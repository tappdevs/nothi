<table class="table table-bordered table-hover tableadvance">
    <thead>
        <tr class="heading">
            <th  class="text-center"> উৎস </th>
            <th  class="text-center"> বিষয় </th>
            <th  class="text-center"> তারিখ </th>
            <th  class="text-center"> ধরন </th>
            <th  class="text-center"> <?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody >
        <?php
        if (!empty($allDakData)) {
            foreach($allDakData as $data){
                $rcv_dt =new \Cake\I18n\Time($data['receive_date']);
                $dak_type = (empty($data['dak_type_id'])?'Daptorik':'Nagorik');
            ?>
            <tr>
                <td class="text-left"><?= ($data['Utsho']) ?> </td>
                <td class="text-left"><?= ($data['dak_subject']) ?> </td>
                <td class="text-left"><?=  $rcv_dt->i18nFormat(null, null, 'bn-BD'); ?> </td>
                <td class="text-center"><?php if(empty($data['dak_type_id'])){ ?>
                    <span title="" data-original-title="দাপ্তরিক ডাক" data-title="দাপ্তরিক ডাক" class="glyphicon glyphicon-home"></span>
                <?php }else{?>
                    <span title="" data-original-title="নাগরিক ডাক" data-title="নাগরিক ডাক" class="glyphicon glyphicon-user"></span> </td>
                <?php } ?>
                <td class="text-center">
                    <button title="" data-dak-type="<?=$dak_type?>" data-toggle="tooltip" data-placement="top" type="button" class="btn btn-xs btn-primary showDetailsNothiJato" data-messageid="<?=$data['id']?>" data-original-title="বিস্তারিত" onclick="showNothijato('<?=$dak_type?>','<?=$data['id']?>','<?=$nothi_part_no_encrypted?>')">
                            <i class=" a2i_gn_details2" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
            <?php
            }
        }
        else {
            ?>
            <tr><td colspan="5" class="danger text-center"> কোন তথ্য পাওয়া যায়নি। </td></tr>
            <?php
        }
        ?>
    </tbody>
</table>
<div class="actions text-center">
    <ul class="pagination pagination-sm nothijatoPagination">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>

</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal-title"></h4>
      </div>
        <div class="modal-body" id="modal-body" style="height: 500px;overflow: auto">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script>
//    $(document).ready(inialize);
//    function inialize(){
//        $(".showDetailsNothiJato").on('click',showNothijato);
//    }
    function showNothijato(dak_type,dak_id,nothi_part_no_encrypted){
          Metronic.blockUI({
                    target: '.nothijatopotroview_div',
                    boxed: true,
                    message: 'অপেক্ষা করুন'
                });
        PROJAPOTI.ajaxSubmitDataCallback('<?=$this->Url->build(['controller' => 'DakMovements','action' =>'viewNothiJatoDakSingle'])?>',{dak_type:dak_type,dak_id:dak_id,part_no:nothi_part_no_encrypted,'nothi_office':<?=$nothi_office?>},'html',function(response){
            $("#myModal").modal('show');
            $("#modal-title").val(' নথিজাত ডাক ');
            $("#modal-body").html(response);
              Metronic.unblockUI('.nothijatopotroview_div');
        });
    }
jQuery(document).ready(function () {
        $(document).off('click','.nothijatoPagination a').on('click','.nothijatoPagination a',function(ev){
            ev.preventDefault();
           if($(this).closest('li').hasClass('disabled')){
                return false;
              }
             Metronic.blockUI({
                    target: '.nothijatopotroview_div',
                    boxed: true,
                    message: 'অপেক্ষা করুন'
                });
            PROJAPOTI.ajaxLoadCallback($(this).attr('href'),function(response){
                $('.nothijatopotroview_div').html(response);
                Metronic.unblockUI('.nothijatopotroview_div');
            });
        });
    });
</script>