<style>
    .inbox .inbox-nav li.active b {
      display: none !important;
    }

    .tooltip-inner {
      white-space: pre-line;
    }

    h3 {
      margin-top: 0px;
      margin-bottom: 0px;
    }

    select.input-xsmall {
      height: 25px !important;
      line-height: 25px !important;
      font-size: 10pt !important;
      padding: 2px;
    }

    .nav-tabs > li > a, .nav-pills > li > a {
      font-size: 11pt;
    }

    .portlet-body img {
      max-width: 100%;
      height: auto;
    }

    div.DraftattachmentsRecord {
      display: none;
    }

    div.DraftattachmentsRecord.active {
      display: block;
    }

    .pager li > a, .pager li > span {
      display: inline-block;
      padding: 5px 8px;
      background-color: #fff;
      border: 1px solid #ddd;
      border-radius: 0;
    }

    .potroview {
      margin: 0 auto;
    }

    a {
      cursor: pointer;
    }

    .editable {
      border: none;
      word-break: break-word;
    }

    table {
      width: 100% !important;
    }

    .bangladate {
      border-bottom: 1px solid #000;
    }

    .portlet.light > .portlet-title > .actions {
      padding: 0px;
    }

    .pagination {
      margin: 5px 0;
    }

    @media print {
      a[href]:after {
        content: none !important;
      }

      @page {
        margin-top: 1in;
        margin-left: 1.2in;
        margin-bottom: .75in;
        margin-right: .75in;
      }

      .row > div {
        padding: 0px !important;
      }

      .row {
        margin-right: 0px !important;
        margin-left: 0px !important;
      }
    }

    .A4 {
      background: white;
      max-width: 21cm;
      height: auto;
      display: block;
      margin: 0 auto;
      padding: 10px 25px;
      margin-bottom: 0.5cm;
      box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
      overflow-y: auto;
      box-sizing: border-box;
      font-size: 12pt;
    }

    .glyphicon.glyphicon-copy {
      font-size: 11px;
    }

    #div-date-range div:first-child {
      padding-top: 2px !important;
      padding-bottom: 2px !important;
    }
</style>
<?php
$bookMarkLi = '';
$currentLi = array();
?>
<div class="portlet light">
    <?php if (!empty($potro)): ?>

  <div class="portlet-title">
        <div class="caption">
            মাস্টার ফাইল
        </div>
        <div class="actions">
            <ul class="pagination pagination-sm">
                <?php
                echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
                echo $this->Paginator->next('<<', array('tag' => 'li', 'escape' => false),
                    '<a class="btn btn-sm blue " href="#">&raquo;</a>',
                    array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true,
                    'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => true, 'modulus' => 10));
                echo $this->Paginator->prev('>>', array('tag' => 'li', 'escape' => false),
                    '<a class="btn btn-sm blue" href="#">&laquo;</a>',
                    array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
                echo $this->Paginator->first(__('প্রথম', true),
                    array('class' => 'number-first'));
                ?>
            </ul>
        </div>
    </div>

  <?php if ($potro): ?>
  <div class="portlet-body">
            <div class="col-md-12 col-sm-12 ">


                <div class="col-md-3 col-sm-3">
                    <?php echo $this->Form->select('potrojari_template',$templete_options,['class'=>'input-sm form-control','empty'=>false,'id'=>'potrojari-template']);?>
                </div>
                <div class="col-md-3 col-sm-3" style='width:21%'>
                        <?php
                            if (!empty($search)) {
                                echo '<input type="text" class="form-control input-sm" placeholder="নথি নম্বর, বিষয় দিয়ে  খুঁজুন" name="search" id="search" value="' . h($search) . '">
                  ';
                            } else {
                                echo ' <input class="form-control input-sm" id="search"
                                              name="search" placeholder="নথি নম্বর, বিষয় দিয়ে  খুঁজুন" type="text">';
                            }
                        ?>
 
                </div>
                <div class="col-md-3 col-sm-3" style='width:20%'>
                        <!--  Date Range Begain -->

                  <?php
                                    echo $this->Form->hidden('start_date', ['class' => 'startdate','id'=>'start_date']);
                                    echo $this->Form->hidden('end_date', ['class' => 'enddate','id'=>'end_date']);
                                ?>
                                    
                                        <div class="hidden-print page-toolbar pull-right portlet-title"
                                             id="div-date-range" style="font-size:13px !important">
                                            <div class=" tooltips btn btn-sm btn-default" data-container="body"
                                                 data-original-title="তারিখ নির্বাচন করুন"
                                                 data-placement="bottom" id="dashboard-report-range">
                                                <i class="icon-calendar"></i>&nbsp; <span
                                                    class="thin uppercase visible-sm-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </div>
                                        </div>


                  <!--  Date Range End  -->
                </div>
                <div class="col-md-1 col-sm-1 ">
                        <button class="btn btn-sm green-haze " id="filter_submit_btn1" title="খুঁজুন" type="button">
                                        &nbsp; <i aria-hidden="true" class="fa fa-search"></i>
                        </button>
                </div>
                <div class="col-md-3 col-sm-3">
                        <div class="input-group  ">
                            


                            <span class="input-group-btn">
                                    
                                    <a class="btn btn-sm red filter-cancel"
                                       data-title="<?php echo __(RESET) ?>" href="<?= $this->request->webroot ?>nothiMasters/masterFile?nothi_master=<?= !empty($selected_nothi_master_id) ? $selected_nothi_master_id : 0 ?>&nothi_part=<?= !empty($selected_nothi_part_id) ? $selected_nothi_part_id : 0 ?>"
                                       title="<?php echo __(RESET) ?>"><i class="fs1 a2i_gn_reset2"></i>
                                    </a>
                                    <a class="btn btn-sm green" data-title-orginal="জারিকৃত পত্রের অবস্থা" href="javascript;"
                                       id="potrojari_dak_tracking_button"
                                       title="জারিকৃত পত্রের অবস্থা"><i
                                            class="fs1 a2i_nt_nothigotibidhi3"></i> </a>
                                    <a class="btn btn-sm btn-info  filter-cancel btn-print" data-toggle="tooltip"
                                       title="প্রিন্ট করুন"><i aria-hidden="true" class="fa fa-print"></i>
                                    </a>
                                    <a class="btn btn-sm btn-warning" target="_blank"
                                       data-toggle="tooltip" href="<?= $this->request->webroot ?>potrojari/potrojariPrintPreview/<?= !empty($selected_nothi_part_id) ? $selected_nothi_part_id : 0 ?>"
                                       title="প্রিন্ট করুন 2"><i aria-hidden="true" class="fa fa-print"></i>
                                    </a>
                                    <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                       onclick="browserPrint()" title="ব্রাউজার প্রিন্ট করুন"><i
                                            aria-hidden="true" class="fs1 a2i_gn_print1"></i>
                                    </a>
                              <?php
                                if (!empty($selected_nothi_master_id) && !empty($selected_nothi_part_id)):
                                    ?>
                              <button class="btn btn-sm btn-warning" data-toggle="tooltip"
                                      onclick="createClonePotrojariForSelectedNothi(0,<?= $selected_nothi_master_id ?>)"
                                      title="পত্রজারি ক্লোন করুন" type="button">
                                        <span class="glyphicon glyphicon-copy"></span></button>
                              <?php
                                else:
                                    ?>
                              <button class="btn btn-sm btn-warning" data-toggle="tooltip" onclick="selectNote()"
                                      title="পত্রজারি ক্লোন করুন" type="button">
                                        <span class="glyphicon glyphicon-copy"></span></button>
                              <?php endif;
                                ?>
                              <button class="btn btn-sm purple portal-publish-button" data-toggle="tooltip"
                                      title="পোর্টালে প্রকাশ করুন"
                                      type="button">
                                        <span <i aria-hidden="true" class="fa fa-share-alt-square"></i></span>
                          </button>
                          </span>
                        </div>
                </div>

                
                
                
            </div>

    <?php
            if (!empty($selected_nothi_master_id) && !empty($selected_nothi_part_id)):
                ?>
    <a class="btn   purple btn-sm" href="<?= $this->Url->build(['_name' => 'noteDetail', $selected_nothi_part_id]) ?>"
       type="button">
                    নথিতে ফেরত যান
                </a>
                <br><br>
                <div class="alert alert-info font-lg text-center">
                    <i aria-hidden="true" class="fa fa-info-circle "></i>
                    যে কোন পত্রকে
                    নির্বাচিত <?= isset($nothi_subject) ? '<b>' . h($nothi_subject) . '</b> বিষয়ের ' : '' ?> নোটে
                    অন্তর্ভুক্ত করতে "পত্রজারি ক্লোন করুন" বাটনে ক্লিক করুন।
                </div>
    <?php
            endif;
            ?>
    <br>

            <div class="row divcenter margin-top-20">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php if(count($potro)>0): ?>
                        <div class="alert alert-danger A4">মোট ফাইল: <?php echo enTobn($this->
                          Paginator->params()['count']);?></div>
                  <?php endif ?>
                  <div class="A4">
                        <div class="potroview">

                            <?php if(count($potro)<1): ?>
                          <div class="alert alert-danger">কোনো মাষ্টার ফাইল পাওয়া যায় নি</div>
                          <?php endif ?>


                          <?php
                            $potro_subject = '';
                            $potrojari_date = '';
                            $i = 0;
                            foreach ($potro as $index => $row) {
                                echo '<div Y-m-d data-potrojari-date="' . $row['potrojari_date']->format(" h:i:s") . '" data-subject="' . h($row['potro_subject']) . '" data-id=' . $row['id'] . ' id_en="' . $row['nothi_potro_page'] . '"  id_bn="' . $row['nothi_potro_page_bn'] . '" class="DraftattachmentsRecord ' . ($i == 0 ? 'active first' : ($i == (count($potro) - 1) ? 'last' : '')) . '">';
                                $potro_subject = h($row['potro_subject']);
                                $potrojari_date = $row['potrojari_date']->format("Y-m-d h:i:s");
                                ?>
                                <div style="background-color: #a94442; padding: 5px; color: #fff;word-wrap: break-word;">

                                    নথি নম্বর: <?= h($row['NothiParts']['nothi_no']) ?>,
                                    নথি বিষয়: <?= h($row['NothiParts']['subject']) ?>,
                                    শাখা: <?= h($allUnits[$row['NothiParts']['office_units_id']]) ?>,
                                    ধরন: <?= h($row['NothiTypes']['type_name']) ?>
                                  <?= $this->Form->hidden('master_id', ['value' => (isset($selected_nothi_master_id) ? $selected_nothi_master_id : $row['nothi_master_id']), 'id' => 'master_id']) ?>
                                  <?= $this->Form->hidden('nothi_office', ['value' => $nothi_office, 'id' => 'nothi_office']) ?>
                                  <?= $this->Form->hidden('potrojari_id', ['value' => $row['id'], 'id' => 'potrojari_id']) ?>
                                  <?= $this->Form->hidden('current_potrojari_id', ['value' => $row['nothi_part_no'], 'id' => 'current_potrojari_id']) ?>
                                  <?= $this->Form->hidden('nothi_part_no', ['value' => (isset($selected_nothi_part_id) ? $selected_nothi_part_id : 0), 'id' => 'nothi_part_no']) ?>
                                  <?= $this->Form->hidden('nothi_subject', ['value' => (isset($nothi_subject) ? h($nothi_subject) : ''), 'id' => 'nothi_subject']) ?>
                                </div>
                          <?php
                                $header = jsonA($row['meta_data']);
                                echo '<div id="template-body" style="background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto;">
                          ' . (!empty($header['potro_header_banner']) ? ('<div
                                style="text-align: ' . ($header['banner_position']) . '!important;"><img
                                src="' . $this->request->webroot . 'getContent?file=' . base64_decode($header['potro_header_banner']) . '&token=' . sGenerateToken(['file' => base64_decode($header['potro_header_banner'])], ['exp' => time() + 60 * 300]) . '"
                                style="width:' . ($header['banner_width']) . ';height: 60px;max-width: 100%;"/></div>') : '') . '<div
                                class="showimageforce">' . html_entity_decode($row['attached_potro']) . '</div><br/>' . html_entity_decode($row['potro_description']) . "</div>";
                                ?>
                                <div class="table">
                                    <table class="table table-striped clearfix" role="presentation">
                                        <tbody class="files">
                                        <?php
                                        if (isset($potrojariAttachments) && count($potrojariAttachments) > 0) {
                                            foreach ($potrojariAttachments as $key => $single_data) {

                                                if ($single_data['attachment_type'] == 'text' || $single_data['attachment_type'] == 'text/html') {
                                                    continue;
                                                }

                                                $fileName = explode('/', $single_data['file_name']);
                                                $attachmentHeaders = get_file_type($single_data['file_name']);
                                                $value = array(
                                                    'name' => urldecode($fileName[count($fileName) - 1]),
                                                    'thumbnailUrl' => $this->request->webroot . 'content/' . $single_data['file_name'],
                                                    'url' => $this->request->webroot . 'content/' . $single_data['file_name'],
                                                    'user_file_name' => h($single_data['user_file_name']),
                                                );


                                                echo '<tr class="template-download fade in">
    
    <td>
    
    <p class="name">
    ' . (!empty($value['name']) ? '<a
            class="showforPopup"
            href="' . $single_data['id'] . '/showPotroAttachment/' . $row['nothi_part_no'] . '" title="' . h($row['potro_subject']) . '">' . (!empty($value['user_file_name']) ? h(urldecode($value['user_file_name'])) : h(urldecode($value['name']))) . '</a>' : '') . '
    </p>
    </td>
    </tr>';
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                    <?php
                                echo "</div>";
                                $i++;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php else: ?>
        <div class="alert alert-danger">কোনো মাষ্টার ফাইল পাওয়া যায় নি</div>
<?php endif ?>

</div>
<?php endif; ?>
<div aria-hidden="true" class="modal fade modal-purple" data-backdrop="false" data-keyboard="false" id="responsiveModal"
     tabindex="-1">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" type="button">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" type="button"><?= __('Close') ?></button>
            </div>
        </div>
      <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<div aria-hidden="true" class="modal fade modal-purple" data-backdrop="false" data-keyboard="false" id="responsiveOnuccedModal"
     tabindex="-1">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" type="button">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button class="btn btn-primary noteCreateButton" onclick="createClonePotrojari(<?= !empty($potrojari_id) ? $potrojari_id : 0 ?>)"
                        type="button"><?= __('Submit') ?></button>
                <button class="btn btn-danger" data-dismiss="modal" type="button"><?= __('Close') ?></button>
            </div>
        </div>
      <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<style>
    .modal-open {
      overflow: hidden !important;
    }
</style>
<!-- পোর্টালে প্রকাশ করুন Modal -->
<div class="modal fade modal-purple" data-backdrop="static" data-keyboard="false" id="portalPublishModal" role="dialog">
    <div class="modal-dialog modal-full">

        <!-- Modal content-->
        <div class="modal-content" style="height: 100vh; overflow: scroll">
            <div class="modal-header">
                <button data-dismiss="modal" type="button">&times;</button>
                <h4 class="modal-title">পোর্টালে প্রকাশের অপশন নির্বাচন করুন</h4>
            </div>
            <div class="modal-body">
                <form action="" id="portalPublishForm" method="post">
                    <input name="potrojari_id" type="hidden" value="">
                    <div class="form-group">
                        <label for="potrojari_subject">পত্রের বিষয়</label>
                        <input class="form-control" id="potrojari_subject" name="potrojari_subject" type="text"
                               value="<?= $potro_subject ?>">
                    </div>
                    <div class="form-group">
                        <label for="type" id="type_loader">ধরন</label>
                        <select class="form-control" id="type" name="type">
                            <option value="">নির্বাচন করুন</option>
                          <?php if ($layer_level !=1 || $layer_level != 2 || $layer_level != 3) { ?>
                          <option value="notices">নোটিশ</option>
                                <option value="news">খবর</option>
                          <?php } ?>

                        </select>
                    </div>
                    <div class="form-group" id="sub-type" style="display: none">
                        <label for="sub-type">উপ-ধরন</label>
                        <select class="form-control" id="select-sub-type" name="category">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="description">বিবরণ</label>
                        <textarea class="form-control required" id="description" name="description" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="potrojari_date">পত্রজারির তারিখ</label>
                        <input class="form-control required" id="potrojari_date" name="potrojari_date"
                               readonly type="text" value="<?= $potrojari_date ?>">
                    </div>
                    <div class="form-group">
                        <label for="archive_date">আর্কাইভের তারিখ</label>
                        <input class="form-control date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+1d"
                               id="archive_date" name="archive_date" type="text">
                    </div>
                  <?php if($layer_level == 1 || $layer_level == 2 || $layer_level == 3) {
                        $office_domain = str_replace("http://", "https://", $office_domain);
                    }
                    ?>
                  <div class="form-group">
                        <label for="domain">ডোমেইন</label>
                        <input class="form-control" id="domain" name="domain" readonly type="text"
                               value="<?= $office_domain; ?>">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info pull-left" id="portalPublishSubmit">প্রকাশ করুন</button>
                <button class="btn btn-danger pull-right" data-dismiss="modal">বন্ধ করুন</button>
            </div>
        </div>

    </div>
</div>
<!-- পোর্টালে প্রকাশ করুন Modal End -->

<?= $this->element('preview_ele');
?>

<?php echo $this->Html->script('assets/global/scripts/printThis.js'); ?>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/master_file_related.js?v=<?= js_css_version ?>"
        type="text/javascript"></script>
<!-- <script type="text/javascript"
        src="<?= CDN_PATH ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
        src="<?= CDN_PATH ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> -->
<script src="<?php echo CDN_PATH; ?>js/client.min.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>



<script>
jQuery(document).ready(function () {
  DateRange.init();
  DateRange.initDashboardDaterange();
  emptyDateValue();
});

function browserPrint() {
  showPrintView($("#template-body"));
}

$(document).off('click', '.showforPopup').on('click', '.showforPopup', function (e) {
  e.preventDefault();
  var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
  getPopUpPotro($(this).attr('href'), title);
});

function isEmpty(value) {
  if (value == '' || value == null || typeof (value) == 'undefined' || value == 0) {
    return true;
  }
  return false;
}

function getPopUpPotro(href, title) {
  $('#responsiveModal').find('.modal-title').text('');
  $('#responsiveModal').find('.modal-body').html('');

  $.ajax({
    url: '<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + href + '?nothi_part=' + '<?= !empty($row['
    nothi_part_no'])?$row['
    nothi_part_no']:'
    ' ?>'+'&token=' + '<?= sGenerateToken(['
    file' => !empty($row['
    nothi_part_no'])?$row['
    nothi_part_no']:'
    '], ['exp
    ' => time() + 60 * 300]) ?>',
    dataType: 'html',
    data: {'nothi_office': < ? php echo
    $nothi_office ? >
},
  type: 'post',
    success
:

  function (response) {
    $('#responsiveModal').modal('show');
    $('#responsiveModal').find('.modal-title').text(title);

    $('#responsiveModal').find('.modal-body').html(response);
  }
})
  ;
}

$(document).off('click', '#filter_submit_btn1');
$(document).on('click', '#filter_submit_btn1', function () {
  window.location = "<?=$this->request->webroot?>nothiMasters/masterFile?search=" + $("#search").val() + '&potrojari_template=' + $("#potrojari-template").val() + '&start_date=' + $("#start_date").val() + '&end_date=' + $("#end_date").val() + '&nothi_master=<?= !empty($selected_nothi_master_id) ? $selected_nothi_master_id : 0 ?>&nothi_part=<?= !empty($selected_nothi_part_id) ? $selected_nothi_part_id : 0 ?>';
});

$('.btn-print').click(function () {
  var id = $('.DraftattachmentsRecord.active').data('id');
  showPdf('পত্র প্রিন্ট প্রিভিউ', '<?= $this->Url->build(['
  controller
  ' => '
  Potrojari
  ', '
  action
  ' => '
  getPdfById
  ']) ?>/' + id + '/<?= $nothi_office ?>' + '/' + 0
)
  ;
  $(".btn-pdf-margin").click();
});

$('#portalPublishSubmit').on('click', function () {
  if (isEmpty($("#potrojari_subject").val())) {
    toastr.error("দুঃখিত! পত্রের বিষয় দেয়া হয়নি");
    return false;
  }
  if (isEmpty($("#type option:selected").val())) {
    toastr.error("দুঃখিত! ধরন দেয়া হয়নি");
    return false;
  }
  if (isEmpty($('#archive_date').val())) {
    toastr.error("দুঃখিত! আর্কাইভের তারিখ দেয়া হয়নি");
    return false;
  }
  var id = $('.DraftattachmentsRecord.active').data('id');
  if (isEmpty(id)) {
    toastr.error("দুঃখিত! সকল তথ্য সঠিকভাবে দেওয়া হয়নি।");
    return false;
  }
  $('input[name="potrojari_id"]').val(id);
  $('#portalPublishForm').attr('action', "<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'portalPublish']) ?>");
  $('#portalPublishForm').submit();
});

$('.date-picker').datepicker({
  orientation: "left",
  autoclose: true
});
$('#potrojari_dak_tracking_button').on('click', function (e) {
  e.preventDefault();
  e.stopPropagation();
  var url = "<?= $this->Url->build(['controller' => 'potrojari', 'action' => 'potrojariDakTracking']) ?>";
  var nothi_office = $("#nothi_office").val();
  var potrojari_id = $("#potrojari_id").val();
  var nothi_part_no = $("#current_potrojari_id").val();
  var new_url = url + "/" + nothi_office + "/" + potrojari_id + "/" + nothi_part_no;
  window.location = new_url;
});
<
  ? php
if ($layer_level == 1 || $layer_level == 2 || $layer_level == 3):
  ?
>
$(".portal-publish-button").on("click", function () {
  $("#sub-type").hide();
  $("#select-sub-type").html('');
  $("#select-sub-type").select2();
  $("#type").html('<option value="">নির্বাচন করুন</option>');
  $("#type").select2();


  $("#portalPublishSubmit").prop('disabled', true);
  $('#type_loader').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; ধরন লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

  $.ajax({
    url: '<?= $office_domain ?>/api/v1/content-type?api_key=7NCnGnrYAAjrz1yScbZL',
    dataType: 'json',
    data: {},
    type: 'get',
    success: function (response) {
      if (response._meta.status == "SUCCESS") {
        var options = '<option value="">নির্বাচন করুন</option>';
        $.each(response.records, function (i, v) {

          if ($.inArray($.trim(v.name), < ? = PORTAL_PUBLISH_TYPE ?>
        ) !=
          -1
        )
          {
            options += '<option value="' + v.name + '">' + v.human_name + '</option>';
          }
        });
        $("#type").html(options);
        $("#type").select2();
        $("#type_loader").html('ধরন');
        $("#portalPublishSubmit").prop('disabled', false);
      } else {
        $("#portalPublishSubmit").prop('disabled', false);
      }
    }
  });

  $("#portalPublishModal").modal('show');
});

$("#type").on('change', function () {
  $("#sub-type").hide();
  $("#select-sub-type").html('');
  $("#select-sub-type").select2();
  var value = $("#type option:selected").val();
  if (isEmpty(value)) {
    return;
  }

  $("#portalPublishSubmit").prop('disabled', true);
  $('#type_loader').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; ধরন সেটিংস লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

  $.ajax({
    url: '<?= $office_domain ?>/api/v1/content-type/' + value + '/structure?api_key=7NCnGnrYAAjrz1yScbZL',
    dataType: 'json',
    data: {},
    type: 'get',
    success: function (response) {
      if (response._meta.status == "SUCCESS") {

        $.each(response._meta.defs.flds, function (i, v) {

          if (v.type == 'lookuptbl') {
            options = '';
            $.each(v.lookup_data, function (j, k) {
              options += '<option value="' + k.id + '">' + k.title + '</option>';
            });
            $("#select-sub-type").html(options);
            $("#select-sub-type").select2();
            $("#sub-type").show();

          }

        });
        $('#type_loader').html('ধরন');
        $("#portalPublishSubmit").prop('disabled', false);
      } else {
        $('#type_loader').html('ধরন');
        $("#portalPublishSubmit").prop('disabled', false);
      }

    }
  });
});

<
  ? php endif;
  ?
>
<
  ? php
if ($layer_level != 1 || $layer_level != 2 || $layer_level != 3):
  ?
>
$(".portal-publish-button").on("click", function () {
  $("#portalPublishModal").modal('show');
});
<
  ? php endif;
  ?
>


var SortingDate = function () {

  var content = $('.inbox-content');

  var loadDraft = function (el, startDate, endDate) {

    var url = js_wb_root + "Performance/newOfficeContent/" + startDate + "/" + endDate;
    content.html('');
    $('.startdate').val(startDate);
    $('.enddate').val(endDate);


    var toggleButton = function (el) {
      if (typeof el == 'undefined') {
        return;
      }
      if (el.attr("disabled")) {
        el.attr("disabled", false);
      } else {
        el.attr("disabled", true);
      }
    };
  };

  return {
    //main function to initiate the module
    init: function (startDate, endDate) {
      loadDraft($(this), startDate, endDate);
    }

  };

}();

function emptyDateValue() {
  document.getElementById('start_date').value = '';
  document.getElementById('end_date').value = '';
}
       
</script>
