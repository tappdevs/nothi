<?php
$lastNoteNo = -1;
$i = 0;
if (!empty($notesquery)) {
    foreach ($notesquery as $key => $row) {

        if ($row->note_no >= 0) {
            $lastNoteNo = $this->Number->format($row->note_no);
        }
        if(!empty($row->digital_sign)){
                                     //verify digital sign
            $sign_info = json_decode($row->sign_info,true);
            $is_vefied_signature = verifySign($row['note_description'],'html',$sign_info['publicKey'],$sign_info['signature']);
            if(empty($is_vefied_signature)){
                $is_vefied_signature = 1;
            }
        }
        $style = '';
        if (!empty($row->subject)) {
            $style = 'page-break-before:always;';
        }
        echo '<div ' . ($row['note_status'] == 'DRAFT' ? 'class="noteDetails"' : '') . ' id="note_' . $row->id . '" style="' . $style . '">';
        echo '<div class="noteContent noMarginPadding" id="' . $this->Number->format($row->note_no) . '">';
        if ($row['note_status'] == 'DRAFT' && ($row['potrojari_status'] != 'Sent')) {
            echo "<div class='pull-right  hidden-print'></div>";
        }

        echo "<div class='printArea'>";
        if (!empty($row->subject)) {
            echo '<div class="noteNo noteSubject"  notesubjectid=' . $row->id . '> বিষয়: <b>' . ($row->subject) . '</b></div>';
        }
        if ($row->is_potrojari == 0) {
            if(!empty($row->digital_sign)){
                //verify digital sign
                if(empty($is_vefied_signature)){
                    echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . $this->Number->format($row->note_no) . '<i class="fa fa-times"  data-original-title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।"  title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।" style="position:  relative;background-color: red;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;"></i></span>';
                }else{
                    echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . $this->Number->format($row->note_no) . '<span style="color: green"><i class="fs0 a2i_gn_onumodondelevery2"  data-original-title="ডিজিটাল সাইনকৃত।"  title="ডিজিটাল সাইনকৃত।" style="padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;"></i></span></span>';
                }
            }else{
                echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . $this->Number->format($row->note_no) . '</span>';
            }
        }
        echo "<div class='noteDescription' id=" . $row->id . "  " . ($row->is_potrojari ? "style='color:red;' title='".$this->Time->format($row->created, "d-MM-y H:m:ss", null, null)."' " : '') . " >" . base64_decode($row->note_description) . "</div>";

        if (!empty($noteAttachmentsmap[$row->id])) {
            echo "<div class='table' style='font-size:11px;'><table class='table table-bordered table-stripped'><tr><th colspan='4' class='text-left'>" . __(SHONGJUKTI) . "</th></tr>";

            foreach ($noteAttachmentsmap[$row->id] as $attachmentKey => $attachment) {

                $fileName = explode('/', $attachment['file_name']);
                echo "<tr>";
                echo "<td style='width:10%;'  class='text-center'>" . $this->Number->format($attachmentKey + 1) . "</td>";
                $url_file_name = "{$attachment['id']}/showAttachment/{$row['nothi_part_no']}";
                $user_file_name = htmlspecialchars($attachment['user_file_name']);
                echo "<td style='width:70%;' class='preview_attachment'><span class='preview'>
".(($attachment['attachment_type'] == 'text' ||  $attachment['attachment_type'] == 'text/html' ||
                        substr($attachment['attachment_type'],0,5) == 'image' ||
                        $attachment['attachment_type'] == 'application/pdf') ? "<a class='showforPopup' data-gallery user_file_name='{$user_file_name}' type='{$attachment['attachment_type']}'  
download='{$attachment['file_name']}' href='{$url_file_name}' title='" . urldecode($fileName[count($fileName) - 1]) . "'>" . urldecode($fileName[count($fileName) - 1]) . "</a>" : urldecode($fileName[count($fileName)- 1])) ."
</span></td>";
                echo "<td style='width:20%;' class='text-center download-attachment'><a class='hidden-print' href='" . $this->Url->build([
                        "controller" => "nothiNoteSheets",
                        "action" => "downloadNoteAttachment",
                        "?" => ["id" => $attachment['id'], 'nothimaster' => $row['nothi_part_no'], 'nothi_office' => $nothi_office]]) . "' title='ডাউনলোড'><i class='fa fa-download'></i></a> ".(($attachment['digital_sign'] ==1)?'<span><i class="fa fa-check" style="background-color: green;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;" data-original-title="ডিজিটাল সাইনকৃত।" title="ডিজিটাল সাইনকৃত।"></i></span>':'').
"</td>";
                echo "</tr>";
            }
            echo "</table></div>";
        }

        if (isset($signatures[$row['nothi_part_no']][$row['id']])) {
            $n = count($signatures[$row['nothi_part_no']][$row['id']]);
            $col = 4;

            $data = $signatures[$row['nothi_part_no']][$row['id']];
            $finalArray = array();
            $finalArray[0][0] = $data[0];

            $designationSeqPrev = array();

            $j = 0;
            $rowC = 0;
            $i = 1;

            if (!isset($data[$i]['office_organogram_id'])) {
                $designationSeq = null;
            } else {
                $designationSeq = isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']]) ? $employeeOfficeDesignation[$data[$i]['office_organogram_id']] : null;
            }

            if(isset($employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']])) {
                $designationSeqPrev = $employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']];
            }else{
                $designationSeqPrev[0]  = 0;
            }

            while ($i < $n) {

                if ($designationSeq[0] <= $designationSeqPrev[0]) {
                    if ($j < $col - 1) {
                        $finalArray[$rowC][++$j] = $data[$i];
                    } else {
                        $finalArray[++$rowC][$j] = $data[$i];
                    }
                } else {
                    $rowC++;
                    if ($j > 0) {
                        $finalArray[$rowC][--$j] = $data[$i];
                    } else {
                        $finalArray[$rowC][$j] = $data[$i];
                    }
                }

                $i++;
                if (isset($data[$i]['office_organogram_id']) && isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']]))
                    $designationSeq = $employeeOfficeDesignation[$data[$i]['office_organogram_id']];
                if (isset($data[$i]['office_organogram_id']) && isset($employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']]))
                    $designationSeqPrev = $employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']];
            }

            if (!empty($finalArray)) {
                for ($findex = 0; $findex < sizeof($finalArray); $findex++) {
                    echo "<div class='row' style='margin:0!important;padding:0!important'>";
                    for ($fj = 0; $fj < $col; $fj++) {
                        echo '<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 text-center" style="padding:2px;"><div style="line-height: 1.2!important;font-size: 10px; color: darkviolet; display: block; ' . (!empty($finalArray[$findex][$fj]['cross_signature']) ? ' text-decoration: line-through;' : '') . ' vertical-align: bottom;page-break-inside:auto!important;margin:0!important;padding:0!important; ">';
                        if (isset($finalArray[$findex][$fj])) {
                            echo "<div class='btn btn-block ' style='height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;'>";
                            if (!empty($finalArray[$findex][$fj]['is_signature']) && empty($finalArray[$findex][$fj]['cross_signature'])) {
                                $englishdate = new \Cake\I18n\Time($finalArray[$findex][$fj]['signature_date'], "d-MM-y H:m:ss", null, null);
                                if(!empty($finalArray[$findex][$fj]['digital_sign'])){
                                    if(empty($is_vefied_signature)){
                                        echo '<i class="fa fa-times" style="left:5px;position: absolute;background-color: red;color: white;padding:  0px;border-radius:  5px;margin: 0px 1px;border: none;" data-original-title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।" title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।"></i>';
                                    }else{
                                        echo '<i class="fa fa-check" style="left:5px;position: absolute;background-color: green;color: white;padding:  0px;border-radius:  5px;margin: 0px 1px;border: none;" data-original-title="ডিজিটাল সাইনকৃত।" title="ডিজিটাল সাইনকৃত।"></i>';
                                    }
                                }
                                echo '<img class="signatures" style="height: 40px!important;padding:0!important;margin:0!important;" data-signdate="'. $englishdate->i18nFormat("Y-M-d H:m:s", null, 'en-US') .'" data-id="' . $finalArray[$findex][$fj]['userInfo'] . '" data-token_id="' . sGenerateToken(['file'=>$finalArray[$findex][$fj]['userInfo']],['exp'=>time() + 60*300]) . '" alt="সাইন যোগ করা হয়নি" />';
                            } else {
                                echo '<img class="img-responsive" style="height: 40px!important;padding:0!important;margin:0!important; visibility: hidden;" />';
                            }
                            echo "</div>";

                            echo "<span style='border-top:1px solid #9400d3;font-size: 8pt;display: block;'>" . $this->Time->format(
                                    $finalArray[$findex][$fj]['signature_date'], "d-MM-y H:m:ss", null, null) . "</span><span style='font-size: 8pt;display: block;'>" . h($finalArray[$findex][$fj]['name']) . "</span><span style='font-size: 8pt;display: block;'>" . $finalArray[$findex][$fj]['employee_designation'] . "</span>";
                        } else
                            echo "<div class='btn btn-block ' style='height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;'>&nbsp;</div>";
                        echo '</div></div>';
                    }
                    echo "</div>";
                }
            }
        }
        echo '</div>';
        echo '</div>';
        echo '</div>';
    }
    die;
}

if ($lastNoteNo == -1) {
    echo "<p class='text-center text-danger'>এই পেজ এ কোন অনুচ্ছেদ দেয়া হয়নি</p>";
}
?>

<script>
    $(document).ready(function() {
        $(document).find('[title]').tooltip({'placement':'bottom', 'container': 'body'});
    })
</script>

