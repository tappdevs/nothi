<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>projapoti-nothi/css/styles.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"
      type="text/javascript">
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2-bootstrap.css"
      type="text/javascript">
<link href="<?php echo CDN_PATH; ?>projapoti-nothi/css/ekko-lightbox.css" rel="stylesheet">
<style>
    .btn-forward-nothi, .btn-sar-nothi-draft, .btn-othi-list {
        padding: 3px 5px !important;

    }

    .portlet-body .notesheetview img {
        width: 100% !important;
        height: 50px !important;
    }

    .btn-icon-only {

    }

    .portlet.box.green > .portlet-title > .actions > a.btn {
        background-color: #fff !important;
        color: green !important;;
    }

    .portlet.box.green > .portlet-title > .actions > a.btn > i {

        color: green !important;;
    }

    #potrodraft_list #sovapoti_signature, #potrodraft_list #sender_signature, #potrodraft_list #sender_signature2, #potrodraft_list #sender_signature3 {
        visibility: hidden;
    }

    #potrodraft_list #sovapoti_signature_date, #potrodraft_list #sender_signature_date, #potrodraft_list #sender_signature2_date, #potrodraft_list #sender_signature3_date {
        visibility: hidden;
    }
</style>

<div class="row">
    <div class="col-md-1 col-sm-1 col-xs-1 notecount">
        <div class="modal fade modal-purple height-auto" id="searchBoxModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">নোট অনুসন্ধান করুন</h4>
                    </div>
                    <div class="modal-body">
                        <div id="search_box">
                            <div class="input-group">
                                <select name="onucchedSearch" class="btn-group bootstrap-select bs-select form-control dropup">
                                    <?php
                                        if(!empty($allNothiParts)){
                                            foreach($allNothiParts as $key=>$value){
                                                ?>
                                                <option value="<?= $value['id'] ?>"><?= mb_strlen(strip_tags($value['NothiNotes']['subject']))>100?(mb_substr(strip_tags($value['NothiNotes']['subject']),0,100) . ' ...'):strip_tags($value['NothiNotes']['subject'])  ?></option>
                                    <?php
                                            }
                                        }
                                    ?>
                                </select>
                                <div class="input-group-btn">
                                    <button class="btn purple" style="padding: 6px 17px;"
                                            onclick="searchNoteOnnucched($('[name=onucchedSearch] option:selected').val());">
                                        <i class="fs1 efile-search3"></i> খুঁজুন
                                    </button>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div id="search_result">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn  btn-danger">বন্ধ করুন</button>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" data-target="#searchBoxModal" data-toggle="modal" class="btn grey" style="display: block; padding:5px; font-size: 10pt;">
            <i class="fs1 efile-search3"></i> খুঁজুন</a>
        <?php
        if (!empty($allNothiParts) && count($allNothiParts) >= 1) {
            echo "<a href='" . $this->Url->build(['_name' => 'nothiDetail', $allNothiParts[0]['id'], $nothi_office]) . "' title='' style='display: block; padding:5px; font-size: 10pt;' class='btn purple'>সকল নোট</a>";
            if ($otherNothi == false) {
                if ($archive['level'] != 2) {
                    echo "<a href='javascript:void(0)' title='' style='display: block; padding:5px; font-size: 10pt;' class='btn btn-default newPartCreate'>নতুন নোট</a>";
                }
            }

            foreach ($allNothiParts as $key => $parts) {
                echo "<a href='" . $this->Url->build(['_name' => 'noteDetail', $parts['id'], $nothi_office]) . "' title='".strip_tags($parts['NothiNotes']['subject'])."' style='display: block;padding:5px;font-size:10pt;' class='btn " . (!empty($nothiMasterPriviligeType[$parts['id']]) ? (($parts['is_finished'] == 1) ? 'btn-default' : 'btn-danger') : 'btn-default') . " '>" . $parts['nothi_part_no_bn'] . "</a>";
            }
        }else{
            if ($otherNothi == false) {
                echo "<a href='javascript:void(0)' title='' style='display: block; padding:5px; font-size: 10pt;' class='btn btn-default newPartCreate'>নতুন নোট</a>";
            }
        }
        ?>
    </div>
    <div class="col-md-11 col-sm-11 col-xs-11">
        <div class="portlet box green-seagreen full-height-content full-height-content-scrollable">
            <div class="portlet-title">
                <div class="caption">
                    <?php
                    if (!empty($note_priority) && $note_priority == 1) {
                        echo '<i  class="glyphicon glyphicon glyphicon-star" ' . ($note_priority == 1 ? 'style="color:red;"' : '') . ' data-title="' . $note_priority_text . '"  title="' . $note_priority_text . '" > </i>';
                    }
                    ?>
                    <?php echo "শাখা: " . $nothiUnit . "; নথি নম্বর: " . $nothiRecord['nothi_no'] . '; বিষয়: ' . $nothiRecord['subject']; ?>
                </div>
                <div class="actions">
                    <a title=" নথি তালিকায় ফেরত যান" class=" btn   btn-primary "
                       href="<?php echo $this->request->webroot; ?>">
                        <i class="fs1 a2i_gn_details1"></i> নথিসমূহ
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="full-height-content-body">
                    <?php if (!empty($allNothiParts) && count($allNothiParts) >= 1) { ?>
                        <div class="row ">
                            <div class="col-md-6 ">
                                <div class="portlet collapsable  box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            নোটাংশ
                                        </div>

                                        <div class="actions">
                                            <a href="javascript:;"
                                               class="btn btn-default btn-sm btn-maximize btn-minmax" title="">
                                                <i class="glyphicon glyphicon-chevron-right"></i>
                                            </a>

                                        </div>
                                    </div>
                                    <div class="portlet-body" id="notesShow"
                                         style="min-height: 40px;overflow-y: auto;overflow-x: hidden">

                                    </div>
                                    <div class="portlet-body" id="khosrapotrobody" style="display: none;">

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="portlet collapsable  box green">
                                    <div class="portlet-title">

                                        <div class="actions pull-left">
                                            <a href="javascript:;"
                                               class="btn btn-default btn-sm btn-maximize btn-minmax" title="">
                                                <i class="glyphicon glyphicon-chevron-left"></i>
                                            </a>
                                        </div>
                                        <div class="caption pull-right">
                                            পত্রাংশ
                                        </div>

                                    </div>
                                    <div class="portlet-body" id="potroShow">

                                    </div>

                                </div>
                            </div>

                        </div>
                    <?php } else echo "<div class='text-center text-primary bold'>দুঃখিত! কোনো নোট এখনো ডেস্ক এ আসেনি। কার্যক্রমের জন্য নতুন নোট ব্যবহার করুন</div>"; ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<div class="modal fade modal-purple" id="responsiveOnuccedModal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-markdown/lib/markdown.js"
        type="text/javascript"></script>

<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/nothi_master_movements.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/ekko-lightbox.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js" type="text/javascript"></script>
<script>
    $('body').addClass('page-quick-sidebar-over-content page-full-width');
    jQuery.ajaxSetup({
        cache: true
    });
    $(document).on('click', '#notesShow .pagination a', function (e) {
        e.preventDefault();
        if ($(this).parent('li').hasClass('disabled') == false)
            noteShow(this.href);

        return false;
    });


    $(document).ready(function () {

        Metronic.init();
        NothiMasterMovement.init('nothing');
        $('li').tooltip({placement: 'bottom'});
        $('#notesShow').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        $('#potroShow').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        noteShow('<?php echo $this->request->webroot; ?>NothiNoteSheets/notesheetPageAll/<?php echo $nothiRecord['id'] ?>/<?= $nothi_office ?>');

        potroPageShow('<?php echo $this->request->webroot; ?>nothiMasters/potroPageAll/<?php echo $nothiRecord['id'] ?>/<?= $nothi_office ?>');

        $('[title]').tooltip({placement: 'bottom'});
        $('li').tooltip({placement: 'bottom'});

        $(document).on('click', '.btn-minmax', function () {
            $('.collapsable').show();
            $('.collapsable').parent('div').removeClass('col-md-12').addClass('col-md-6');
            $('.btn-minmax').addClass('btn-maximize').removeClass('btn-minimized');

        });

        $(document).on('click', '.btn-minmax', function () {
            $('.collapsable').show();
            $('.collapsable').parent('div').removeClass('col-md-12').addClass('col-md-6');
            $('.btn-minmax').addClass('btn-maximize').removeClass('btn-minimized');
            //$(this).find('i').toggleClass('fa-chevron-right').toggleClass('fa-chevron-left');
        });

        $(document).on('click', '.btn-minimized', function () {
            $(this).find('i').toggleClass('fa-chevron-right').toggleClass('fa-chevron-left');
        });

        $(document).on('click', '.btn-maximize', function () {
            var ind = $('.btn-maximize').index(this);

            if (ind == 1) {
                ind = 0;
            } else {
                ind = 1;
            }
            $('.collapsable').eq(ind).hide();
            $('.collapsable').parent('div').removeClass('col-md-6').addClass('col-md-12');
            $('.btn-minmax').removeClass('btn-maximize').addClass('btn-minimized');
            $(this).find('i').toggleClass('fa-chevron-right').toggleClass('fa-chevron-left');

        });

    });


    function getUrlVars(href) {
        var vars = [], hash;
        var hashes = href.slice(href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }


    function noteShow(href) {
        if (href == '' || typeof (href) == 'undefined')
            return;
        $('#notesShow').html('<img src="<?php echo $this->request->webroot; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(href, {}, 'html', function (response) {
            $('#notesShow').html(response);
//            Metronic.initSlimScroll('.scroller');
            $('[data-title-orginal]').tooltip({placement: 'bottom'});
            $('[title]').tooltip({placement: 'bottom'});
            $('li').tooltip({placement: 'bottom'});

            $.each($('.signatures'), function (i, v) {
                var imgurl = $(v).data('id');
                var token = $(v).data('token_id');
                var signDate = $(v).data('signdate');
                PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'getSignature/' + imgurl + '/1/' + signDate+'?token='+token, {}, 'html', function (signature) {
                    $(v).attr('src', signature);
                });

            });
            heightlimit();
        });
    }

    $(document).on('click', '#noteLeftMenuToggle', function () {
        $('.noteLeftMenu').toggle().toggleClass('col-lg-2 col-md-2 col-sm-2 col-xs-2');

        if ($('.noteLeftMenu').css('display') == 'none') {
            $('.noteContentSide').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12')
            $('.noteContentSide').removeClass('col-lg-10 col-md-10 col-sm-10 col-xs-10')

        } else {
            $('.noteContentSide').addClass('col-lg-10 col-md-10 col-sm-10 col-xs-10')
            $('.noteContentSide').removeClass('col-lg-12 col-md-12 col-sm-12 col-xs-12')

        }
    })
    $(document).on('click', '#potroLeftMenuToggle', function () {
        $('.potroLeftMenu').toggle().toggleClass('col-lg-2 col-md-2 col-sm-2 col-xs-2');

        if ($('.potroLeftMenu').css('display') == 'none') {
            $('.potroContentSide').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').css({
                'min-height': '40px',
                'overflow-y': 'auto',
                'overflow-x': 'hidden'
            });
            $('.potroContentSide').removeClass('col-lg-10 col-md-10 col-sm-10 col-xs-10')

        } else {
            $('.potroContentSide').addClass('col-lg-10 col-md-10 col-sm-10 col-xs-10').css({
                'min-height': '40px',
                'overflow-y': 'auto',
                'overflow-x': 'hidden'
            });
            $('.potroContentSide').removeClass('col-lg-12 col-md-12 col-sm-12 col-xs-12')

        }
    });


    function potroPageShow(href, pageno) {
        $('#potroShow').html('<img src="<?php echo $this->request->webroot; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(href, {}, 'html', function (response) {
            $('#potroShow').html(response);

//            Metronic.initSlimScroll('.scroller');
            if (typeof (pageno) != 'undefined') {
                goToPage(pageno);
                getpotropage(pageno);
            }
            $('[data-title-orginal]').tooltip({placement: 'bottom'});
            $('[title]').tooltip({placement: 'bottom'});
            $('li').tooltip({placement: 'bottom'});
            heightlimit();
        });
    }

    function getpotropage(potroPage) {
        $('.potroNo').val(potroPage);
    }

    function refPotro(potrono) {
        if (typeof (potrono) != 'undefined') {
            goToPage(potrono);
            getpotropage(potrono);
        }
    }


    function getPopUpPotro(href, title) {
        $('#responsiveOnuccedModal').find('.modal-title').text('');
        $('#responsiveOnuccedModal').find('.modal-body').html('');
        $.ajax({
            url: '<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + href+'?nothi_part='+'<?= $part_id ?>'+'&token='+'<?= sGenerateToken(['file' => $part_id], ['exp' => time() + 60 * 300]) ?>',
            dataType: 'html',
            data: {'nothi_office':<?php echo $nothi_office ?>},
            type: 'post',
            success: function (response) {
                $('#responsiveOnuccedModal').modal('show');
                $('#responsiveOnuccedModal').find('.modal-title').text(title);

                $('#responsiveOnuccedModal').find('.modal-body').html(response);
            }
        });
    }


    $(document).on('click', '.showforPopup', function (e) {
        e.preventDefault();
        var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
        getPopUpPotro($(this).attr('href'), title);
    })


    $(document).on('click', '.btn-notesheet-print', function () {
        $(document).find('.notesheetview').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: true,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    });


    $(document).ready(function ($) {
        // delegate calls to data-toggle="lightbox"

        $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
            event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function () {

                },
                onNavigate: function (direction, itemIndex) {

                }
            });
        });
    });

    <?php if ($otherNothi == false): ?>
    $('.newPartCreate').on('click', function () {

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right"
        };

        var nothimasterid = <?php echo $nothiRecord['nothi_masters_id']; ?>;

        if (nothimasterid > 0) {
        } else {
            return false;
        }

        var selectedPriviliges = {};

        bootbox.dialog({
            message: "আপনি কি নতুন নোট তৈরি করতে চান?",
            title: "নতুন নোট",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        Metronic.blockUI({
                            target: '.page-container',
                            boxed: true
                        });
                        $('.nothiCreateButton').attr('disabled', 'disabled');
                        $.ajax({
                            url: '<?php echo $this->Url->build(['controller' => 'NothiMasters',
                                'action' => 'add'])
                                ?>',
                            data: {formPart: {nothi_master_id: nothimasterid}, priviliges: selectedPriviliges},
                            type: "POST",
                            dataType: 'JSON',
                            success: function (response) {
                                $('div.error-message').remove();
                                if (response.status == 'error') {
                                    Metronic.unblockUI('.page-container');
                                    $('.nothiCreateButton').removeAttr('disabled');
                                    toastr.error(response.msg);

                                    if (typeof (response.data) != 'undefined') {
                                        var errors = response.data;
                                        $.each(errors, function (i, value) {
                                            $.each(value, function (key, val) {
                                                if (i != 'office_units_organogram_id') {
                                                    $('[name=' + i + ']').focus();
                                                    $('[name=' + i + ']').after('<div class="error-message">' + val + '</div>');
                                                }
                                            });
                                        });
                                    }
                                } else if (response.status == 'success') {
                                    window.location.href = js_wb_root + 'noteDetail/' + response.id;
                                }
                            },
                            error: function (status, xresponse) {

                            }
                        });
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                        Metronic.unblockUI('.page-container');
                    }
                }
            }
        });
        return false;
    });
    <?php endif; ?>

    function heightlimit() {
        var resBreakpointMd = Metronic.getResponsiveBreakpoint('md');
        var handle100HeightContent = function () {

            var target = $('.full-height-content');
            var height;

            height = Metronic.getViewPort().height -
                $('.page-header').outerHeight(true) -
                $('.page-footer').outerHeight(true) -
                $('.page-title').outerHeight(true) -
                $('.page-bar').outerHeight(true);

            if (target.hasClass('portlet')) {
                var portletBody = target.find('.portlet-body');

                if (Metronic.getViewPort().width < resBreakpointMd) {
                    Metronic.destroySlimScroll(portletBody.find('.full-height-content-body')); // destroy slimscroll
                    return;
                }

                height = height -
                    target.find('.portlet-title').outerHeight(true) -
                    parseInt(target.find('.portlet-body').css('padding-top')) -
                    parseInt(target.find('.portlet-body').css('padding-bottom')) - 2;

                if (target.hasClass("full-height-content-scrollable")) {
                    height = height - 15;
                    portletBody.find('.full-height-content-body').css('height', height);
//                    Metronic.initSlimScroll(portletBody.find('.full-height-content-body'));
                } else {
                    portletBody.css('min-height', height);
                }
            } else {
                if (Metronic.getViewPort().width < resBreakpointMd) {
                    Metronic.destroySlimScroll(target.find('.full-height-content-body')); // destroy slimscroll
                    return;
                }

                if (target.hasClass("full-height-content-scrollable")) {
                    height = height - 15;
                    target.find('.full-height-content-body').css('height', height);
//                    Metronic.initSlimScroll(target.find('.full-height-content-body'));
                } else {
                    target.css('min-height', height);
                }
            }
            $('.notecount').css({"max-height": height});
            $('.notecount').css({"overflow": 'auto'});
            $('.slimScrollBar').css({'top': '40px'});
        };
        handle100HeightContent();
        Metronic.addResizeHandler(handle100HeightContent);
        var maxhe = Math.max($('.noteContentSide').height() + 80, $('.potroContentSide').height() + 80);
        $('#notesShow').height(maxhe);
        $('#potroShow').height(maxhe);
    }

    $(document).off('click', '.showAttachmentOfContent').on('click', '.showAttachmentOfContent', function () {
        $('#responsiveOnuccedModal').modal('show');
        var file = $(this).text();
        $('#responsiveOnuccedModal').find('.modal-title').html(file);

        var url = $(this).data('url');
        var attachment_type = $(this).data('type');

        if (typeof(url) != 'undefined') {

            if (attachment_type == 'text') {
                $('#responsiveOnuccedModal').find('.modal-body').html('<div style="background-color: #fff; max-width:950px; min-height:815px; max-height: 815px; margin:0 auto; page-break-inside: auto;">"' + file + '<br/>" + file + "</div>"');
            } else if ((attachment_type.substring(0, 15)) == 'application/vnd' || (attachment_type.substring(0, 15)) == 'application/ms' || (attachment_type.substring(0, 15)) == 'application/pdf') {
                $('#responsiveOnuccedModal').find('.modal-body').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + url + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

            } else if ((attachment_type.substring(0, 5)) == 'image') {
                $('#responsiveOnuccedModal').find('.modal-body').html('<embed src="' + url + '" style=" width:100%; height: 700px;" type="' + attachment_type + '"></embed>');
            } else {
                $('#responsiveOnuccedModal').find('.modal-body').html('দুঃখিত! ডাটা পাওয়া যায়নি');
            }
        }

    });

</script>