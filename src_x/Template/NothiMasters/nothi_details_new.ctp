<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>projapoti-nothi/css/styles.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"
      type="text/javascript">
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2-bootstrap.css"
      type="text/javascript">
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!--<link href="--><?php //echo CDN_PATH; ?><!--projapoti-nothi/css/ekko-lightbox.css" rel="stylesheet">-->
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/Sortable.js"></script>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jQuery-Mask/jquery.mask.min.js"></script>
<style>
    @media print {
        a[href]:after {
            content: none !important;
        }
    }

    .btn-forward-nothi, .btn-sar-nothi-draft, .btn-othi-list, .btn-nisponno-nothi {
        padding: 3px 5px !important;
        border-width: 1px;

    }

    .portlet-body .notesheetview img {
        width: 100% !important;
        height: 50px !important;
    }

    .btn-icon-only {

    }

    .portlet.box.green > .portlet-title > .actions > a.btn {
        background-color: #fff !important;
        color: green !important;;
    }

    .portlet.box.green > .portlet-title > .actions > a.btn > i {

        color: green !important;;
    }

    /*#responsivePermissionEdit .modal, #responsivePermissionEdit .modal-body {
        max-height: 420px;
        overflow-y: auto;
    }*/

    #potrodraft_list #sovapoti_signature, #potrodraft_list #sender_signature, #potrodraft_list #sender_signature2, #potrodraft_list #sender_signature3 {
        visibility: hidden;
    }

    #potrodraft_list #sovapoti_signature_date, #potrodraft_list #sender_signature_date, #potrodraft_list #sender_signature2_date, #potrodraft_list #sender_signature3_date {
        visibility: hidden;
    }

    [id^=dropdown-menu-noteDecision] ul li {
        max-width: 350px;
        overflow: auto;
    }

    .bootstrap-switch-handle-off {
        font-size: 11px !important;
    }

    .A4-max {
	    background: white;
	    max-width: 21cm;
	    min-height: 29.7cm;
	    display: block;
	    margin: 0 auto;
	    padding-left: 0.75in;
	    padding-right: 0.75in;
	    padding-top: 1in;
	    padding-bottom: 0.75in;
	    margin-bottom: 0.5cm;
	    box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
	    overflow-y: auto;
	    box-sizing: border-box;
	    font-size: 12pt;
    }
    .noteSubject {
	    background: white;
	    position: absolute;
	    top: 0;
	    /*min-width: 550px;*/
	    z-index: 999;
	    width: calc(100% - 33px) !important;
	    margin-bottom:0!important;
    }
	.noteSubject>div:first-child {
		border: solid 1px silver;
		padding: 10px;
		width: 100%;
		background: lightgrey;
	}
</style>
<script>
    var noteOrderingType = 'ASC';
    if (typeof $.cookie('noteOrderingType') == 'undefined') {
        $.cookie('noteOrderingType', noteOrderingType);
    } else {
        noteOrderingType = $.cookie('noteOrderingType');
    }
    function changeNoteOrder(order) {
        noteOrderingType = order;
        $.cookie('noteOrderingType', noteOrderingType);
        if (noteOrderingType == 'DESC') {
            $("#asc_label").removeClass('active');
            $("#desc_label").addClass('active');
        } else {
            $("#asc_label").addClass('active');
            $("#desc_label").removeClass('active');
        }
        var noteSheetPageUrl = '<?php echo $this->request->webroot; ?>NothiNoteSheets/notesheetPage/<?php echo $id ?>/<?php echo $nothi_office ?>/'+noteOrderingType+permitted_by;
        noteShow(noteSheetPageUrl, 0);
    }
    var permitted_by = '';
	$(document).ready(function() {
		<?php if(isset($_GET['permitted_by']) && $_GET['permitted_by'] != '') { ?>
        permitted_by = '?permitted_by=<?=$_GET['permitted_by']?>';
		<?php } ?>

		if (noteOrderingType == 'DESC') {
            $("#asc_label").removeClass('active');
            $("#desc_label").addClass('active');
        } else {
            $("#asc_label").addClass('active');
            $("#desc_label").removeClass('active');
        }
	})
</script>
<?php
if (isset($archive['level']) && $archive['level'] > 0) {
    $nothi_class_defination = json_decode(NOTHI_CLASS, true);
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                <a href="" style="text-decoration: none;" title="সচিবালয়ের নির্দেশমালা" data-target="#SecretariatGuidelinesModal" data-toggle="modal">সচিবালয়ের নির্দেশমালা</a> অনুযায়ী
                <?php if ($archive['level'] == 1): ?>
                    <?= $nothi_class_defination[$archive['nothi_class']] ?> শ্রেণির এই নথির মেয়াদ <?= enTobn($archive['nothi_alive_days'] - $archive['nothi_age']) ?> দিনের মধ্যে শেষ হয়ে যাবে।
                <?php elseif ($archive['level'] == 2): ?>
                    <?= $nothi_class_defination[$archive['nothi_class']] ?> শ্রেণির এই নথির মেয়াদ ইতোমধ্যে শেষ হয়ে গেছে, অতিসত্তর সকল কার্যক্রম সম্পন্ন করার অনুরোধ করা হল।
                <?php endif; ?>

                <div id="SecretariatGuidelinesModal" class="modal fade modal-purple" data-backdrop="static"
                     role="dialog">
                    <div class="modal-dialog" style="width: 99%;">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">সচিবালয়ের নির্দেশমালা</h4>
                            </div>
                            <div class="modal-body text-center">
                                <img src="<?= $this->request->webroot ?>img/secretariat_guidelines.png" alt="Image">
                            </div>
                            <div class="modal-footer">
	                            <div class="btn-group btn-group-round">
	                                <button type="button" class="btn btn-default red" data-dismiss="modal"><i class="fs1 a2i_gn_close2"></i> বন্ধ করুন
	                                </button>
	                            </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
<div class="row">
    <div class="col-md-1 col-sm-1 col-xs-1 notecount">
        <div class="modal fade modal-purple height-auto" id="searchBoxModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">নোট অনুসন্ধান করুন</h4>
                    </div>
                    <div class="modal-body">
                        <div id="search_box">
                            <div class="input-group">
                                <select name="onucchedSearch" class="btn-group bootstrap-select bs-select form-control dropup">
                                    <?php
                                    if (!empty($allNothiParts)) {
                                        foreach ($allNothiParts as $key => $value) {
                                            ?>
                                            <option value="<?= $value['id'] ?>"><?= mb_strlen(strip_tags($value['NothiNotes']['subject'])) > 100 ? (mb_substr(strip_tags($value['NothiNotes']['subject']), 0, 100) . ' ...') : strip_tags($value['NothiNotes']['subject']) ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="input-group-btn">
                                    <button class="btn purple" style="padding: 6px 17px;" onclick="searchNoteOnnucched($('[name=onucchedSearch] option:selected').val());">
                                        <i class="fs1 efile-search3"></i> খুঁজুন
                                    </button>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div id="search_result">

                        </div>
                    </div>
                    <div class="modal-footer">
	                    <div class="btn-group btn-group-round">
                            <button type="button" data-dismiss="modal" class="btn  btn-danger"><i class="fs1 a2i_gn_close2"></i> বন্ধ করুন</button>
	                    </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" data-target="#searchBoxModal" data-toggle="modal" class="btn grey"
           style="display: block; padding:5px; font-size: 10pt;"><i class="fs1 efile-search3"></i> খুঁজুন</a>
        <?php
        $permitted_by = '';
		if(isset($_GET['permitted_by']) && $_GET['permitted_by'] != '') {
			$permitted_by = "?permitted_by=".$_GET['permitted_by'];
		}
        echo "<a href='" . $this->Url->build(['_name' => 'nothiDetail', $id, $nothi_office]) . $permitted_by. "' title='' style='display: block; padding:5px; font-size: 10pt;' class='btn purple'>সকল নোট</a>";
        if ($otherNothi == false) {
            if ($archive['level'] != 2) {
            	if ($permitted_by == '') {
					echo "<a href='javascript:void(0)' title='' style='display: block; padding:5px; font-size: 10pt;' class='btn btn-default newPartCreate'>নতুন নোট</a>";
				}
            }
        }
        $currentnote = 0;
        if (!empty($allNothiParts)) {
            echo $this->Form->hidden('all_nothi_parts',['id'=>'all_nothi_parts','value'=>json_encode($allNothiParts)]);
            
            foreach ($allNothiParts as $key => $parts) {
                if ($parts['id'] == $nothiRecord['id']) {
                    $currentnote = 1;
                } elseif ($currentnote == 0 && $parts['id'] > $nothiRecord['id']) {
                    echo "<a href='" . $this->Url->build(['_name' => 'noteDetail', $nothiRecord['id'], $nothi_office]) . $permitted_by. "' title='".strip_tags($nothiRecord['subject'])."' style='display: block;padding:5px;font-size:10pt;' class='btn nothipartdiv btn-success '>" . h($nothiRecord['nothi_part_no_bn']) . "</a>";
                    $currentnote = 1;
                }
//                $color = ($id == $parts['id'] ? 'btn-success' : 'btn-danger');
                $color = ($id == $parts['id'] ? 'btn-success' : (($parts['is_finished'] == 1 || !isset($current_nothi_list[$parts['id']])) ? 'btn-default' : 'btn-danger'));
//                    $color = ($id == $parts['id'] ? 'btn-primary' : (!empty($nothiMasterPriviligeType[$parts['id']])? 'green' : 'btn-default'));
                echo "<a href='" . $this->Url->build(['_name' => 'noteDetail', $parts['id'], $nothi_office]) . "' title='".strip_tags($parts['NothiNotes']['subject'])."' style='display: block;padding:5px;font-size:10pt;' class='btn nothipartdiv " . ($color) . " '>" . h($parts['nothi_part_no_bn']) . "</a>";
            }
        }
        ?>
    </div>

    <div class="col-md-11 col-sm-11 col-xs-11">
        <div class="portlet box green-seagreen full-height-content full-height-content-scrollable">
            <div class="portlet-title">
                <div class="caption">
                    <?php
                    if (!empty($note_priority) && $note_priority == 1) {
                        echo '<i  class="glyphicon glyphicon glyphicon-star" ' . ($note_priority == 1 ? 'style="color:red;"' : '') . ' data-title="' . $note_priority_text . '"  title="' . $note_priority_text . '" > </i>';
                    }
                    ?>
                    <?php echo "শাখা: " . h($nothiUnit) . "; নথি নম্বর: " . h($nothiRecord['nothi_no']) . '; বিষয়: ' . h($nothiRecord['subject']); ?>
                </div>
                <div class="actions btn-group btn-group-round">
                    <?php if ($privilige_type >= 1): ?>
                        <?php
                        if ($canSummary == 1 && $draftSummary == 0 && $otherNothi == false):
                            ?>
                            <a title="সার-সংক্ষেপ তৈরি করুন " class=" btn   btn-info btn-sar-nothi-draft"
                               href="javascript:void(0);">
                                <i class="fs1 a2i_gn_applicationsubmit3"></i> সার-সংক্ষেপ তৈরি করুন
                            </a>
                        <?php endif; ?>
                        <a title=" প্রেরণ  করুন" nothi_office="<?php echo $nothi_office ?>"
                           nothi_master_id="<?php echo $id ?>" class=" btn   purple btn-forward-nothi">
                            <i class="fs1 a2i_gn_send2"></i> &nbsp; প্রেরণ করুন
                        </a>
<!--                        <a title=" অনুমতি সংশোধন" nothi_master_id="--><?php //echo $id ?><!--"-->
<!--                           class=" btn   btn-danger btn-edit-permission">-->
<!--                            <i class="fs1 a2i_gn_secrecy3"></i> অনুমতি সংশোধন-->
<!--                        </a>-->
                        <?php
                        if (isset($canDelete) && $otherNothi == false && $canDelete
                            && $nothiRecord['nothi_part_no'] > 1
                        ):
                            ?>
                            <a title=" মুছে ফেলুন" nothi_master_id="<?php echo $id ?>"
                               class="btn  btn-danger btn-delete-nothi">
                                <i class="fa fa-minus-circle"></i> মুছে ফেলুন
                            </a>
                        <?php endif; ?>
                    <?php endif ?>
					<?php if(isset($_GET['permitted_by']) && $_GET['permitted_by'] != '') { ?>
		                <a title=" নথি তালিকায় ফেরত যান" class=" btn yellow-gold" href="<?php echo $this->request->webroot; ?>permitted_nothi">
			                <i class="fs1 a2i_gn_details1"></i> শেয়ারকৃত নথিসমূহ
		                </a>
					<?php } else { ?>
	                    <a title=" নথি তালিকায় ফেরত যান" class=" btn yellow-gold" href="<?php echo $this->request->webroot; ?>">
	                        <i class="fs1 a2i_gn_details1"></i> নথিসমূহ
	                    </a>
					<?php } ?>

                    <?php if (isset($nothi_back) && $nothi_back == 1) { ?>
                        <a title=" নথি ফেরত আনুন" nothi_master_id="<?php echo $id ?>"
                           class="btn  btn-danger btn-revert-nothi">
                            <i class="glyphicon glyphicon-retweet"></i>
                        </a>
                    <?php }
                    ?>

                </div>
            </div>
            <div class="portlet-body">
                <div class="full-height-content-body">
                    <div class="row ">
                        <div class="col-md-6" style="display:flex" >
                            <div class="portlet collapsable box green" style="margin-bottom:0!important;flex:1;width: 590px !important;">
                                <div class="portlet-title">
                                    <div class="caption">
                                        নোটাংশ
                                    </div>

                                    <div class="actions">
                                        <button class="btn btn-xs grey btn-maximize btn-minmax"
                                           title="">
                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                        </button>
                                        <!--<a href="" class="fullscreen btn btn-default btn-sm "><i class="fa fa-expand"></i></a>-->
                                    </div>
                                    <div class="tools btn-group btn-group-round margin-right-10">
                                        <button class="btn btn-xs grey" id="noteLeftMenuToggle" title="সব অনুচ্ছেদ দেখুন" data-original-title="সব অনুচ্ছেদ দেখুন"><i class="fs1 a2i_gn_view1"></i>
                                        </button>
                                        <button class="btn btn-xs grey" title="প্রিন্ট করুন" data-target="#printAdvanceModal" onclick="$('#advanceSearchForm').submit()" data-toggle="modal"><i class="fs1 efile-print"></i>
                                        </button>
                                    </div>
	                                <div class="tools btn-group btn-group-round margin-right-10">
		                                <label class="btn btn-xs grey active" id="asc_label" title="ঊর্ধ্বগামী" onclick="changeNoteOrder('ASC')"><i class="fa fa-sort-amount-asc"></i></label>
		                                <label class="btn btn-xs grey" id="desc_label" title="নিম্নগামী" onclick="changeNoteOrder('DESC')"><i class="fa fa-sort-amount-desc"></i></label>
	                                </div>
                                </div>
                                <div class="portlet-body" id="onucchedBody">
                                    <?php
                                    $bookMarkLi = '';
                                    $currentLi = array();
                                    ?>

                                    <style>
                                        .nothiGroupView.dropdown-menu {
                                            width: 50px !important;
                                            min-width: 55px;
                                            box-shadow: none;
                                        }

                                        .MsoTableGrid {
                                            margin-left: 0pt !important;
                                        }

                                        @media print {
                                            a[href]:after {
                                                content: none !important;
                                            }

                                            .notesheetview {
                                                line-height: 1.4;
                                                margin: 0px !important;
                                                padding: 0px !important;
                                            }

                                            .notesheetview .noteContent {
                                                padding: 2px !important;
                                            }

                                            @page {
                                                margin-top: 1in;
                                                margin-left: 1.2in;
                                                margin-bottom: .75in;
                                                margin-right: .75in;
                                            }

                                            body {
                                                margin-top: 1in;
                                                margin-left: 1.2in;
                                                margin-bottom: .75in;
                                                margin-right: .75in;
                                            }

                                            .notesheetview p {
                                                margin: 0px 0px 5px !important;
                                            }

                                            .notesheetview .btn-block {
                                                margin-bottom: 2px !important;
                                            }

                                            .row > div {
                                                padding: 0px !important;
                                            }
                                        }

                                        .showsubject {
                                            word-wrap: break-word;
                                            text-align: left;
                                            font-size: 11pt
                                        }
                                    </style>
                                    <div class="portlet  light">
                                        <div class="portlet-body">
                                            <div class="row">
                                                <div class="col-lg-0 col-md-0 col-sm-0 col-xs-0 noteLeftMenu"
                                                     style="display: none;">
                                                    <?php
                                                    $lastListedNote = -1;
                                                    if (!empty($noteNos)) {
                                                        echo '<div class="scroller" style="height: 700px;" data-always-visible="1" data-rail-visible1="1">';
                                                        echo '<div class="btn-group-vertical">';
                                                        $noteGroupArray = array();

                                                        foreach ($noteNos as $key => $value) {

                                                            if ($value['nothi_part_no'] == $id) {
                                                                $lastListedNote = $value['note_no_en'] > $lastListedNote
                                                                    ? $value['note_no_en'] : $lastListedNote;
                                                            }

                                                            $noteGroupArray[$value['nothi_part_no_en']][] = '<li>
				<a href=' . $value['id'] . ' title="অনুচ্ছেদ" class="nothiOnucched showforPopup btn btn-default " partno=' . $value['nothi_part_no_bn'] . ' nothiMasterId = ' . $value['nothi_notesheet_id'] . '  nothisheetsId = ' . $value['nothi_notesheet_id'] . ' notenoen=' . $value['note_no_en'] . ' nothi_part=' . $value['nothi_part_no'] . ' noteno=' . $value['note_no'] . '>' . $value['nothi_part_no_bn'] . '.' . $value['note_no'] . '</a>
			</li>';
                                                        }

                                                        foreach ($noteGroupArray as $ky => $v) {

                                                            echo '<div class="btn-group">';
                                                            echo '<button id="btnGroupVerticalDrop' . $ky . '" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> ' . $this->Number->format($ky) . ' <i class="glyphicon glyphicon-chevron-down"></i> </button>';
                                                            echo '<ul class="nothiGroupView dropdown-menu" role="menu" aria-labelledby="btnGroupVerticalDrop' . $ky . '">';
                                                            foreach ($v as $groupNoteKey => $groupNoteValue) {
                                                                echo $groupNoteValue;
                                                            }
                                                            echo '</ul> </div>';
                                                        }

                                                        echo "</div></div>";
                                                    }
                                                    ?>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noteContentSide">
                                                    <div class="nothiDetailsPage ">
                                                        <div style="padding-top: 44px !important;">
	                                                        <div class="noteNo noteSubject" notesubjectid="<?=($nothi_note_subject)?$nothi_note_subject->id:''?>">
		                                                        <?php if($nothi_note_subject):?><div>বিষয়: <b><?=$nothi_note_subject->subject?></b></div><?php endif;?>
	                                                        </div>
	                                                        <div class="notesheetview" id="notesShow_new" style="height:290px;"></div>
	                                                        <div id="notangso_footer" style="<?php if(!$nothi_note_subject){ echo 'background:white!important';} else {echo 'padding: 10px 0;';} ?>">
																<?php if ($privilige_type == 1):?>
			                                                        <div class="btn-group btn-group-round">
				                                                        <a data-original-title="নতুন অনুচ্ছেদ তৈরি করুন" title="নতুন অনুচ্ছেদ তৈরি করুন" class="btn green margin-right-10 addNewNote"><i class="fs1 a2i_gn_note2"></i> নতুন অনুচ্ছেদ</a>
																		<?php if ($otherNothi == false && $nisponno == 0 && count($nothimovement) > 1 && $employee_office['office_unit_organogram_id'] == $nothiInformation['office_units_organogram_id']): ?>
					                                                        <a title=" নোট নিষ্পন্ন " nothi_master_id="<?php echo $id ?>" nothi_office="<?php echo $nothi_office ?>" class="btn blue btn-nisponno-nothi"><i class="fs1 a2i_gn_onumodondelevery3"></i> নোট নিষ্পন্ন </a>
																		<?php endif; ?>
			                                                        </div>
			                                                        <div class="portlet box purple" id="responsiveNoteEdit" style="display: none;">
				                                                        <div class="portlet-body">
					                                                        <div class="removeOnuchaddiv col-md-12 text-left">
						                                                        <span class="noteNo"> অনুচ্ছেদ: <?php echo '<span id="poro"></span>' . '.' . '<span id="notno"></span>'; ?> </span>
					                                                        </div>
					                                                        <input type="hidden" id="noteId"/>
					                                                        <input type="hidden" id="sendsaveModal"/>
					                                                        <div class="row">
						                                                        <div class="col-md-12 form-group">

						                                                        </div>
					                                                        </div>

					                                                        <div class="row notesubjectdiv <?php echo (count($noteNos) == 0) ? '' : 'hidden' ?>">
						                                                        <div class="col-md-2 col-sm-3 form-group notesubjectdiv" style="padding-right: 0px">
							                                                        <label>বিষয়</label>
							                                                        <input title="আন্ডারলাইন" type="checkbox" id="underline" name="underline" value="underline" class="pull-right hidden" />
						                                                        </div>
						                                                        <div class="col-md-10 col-sm-7 form-group " style="padding-left: 0px
			">

							                                                        <div class="input-group">
								                                                        <input type="text" name="subject"
								                                                               id="notesubject"
								                                                               class="form-control input-sm"
								                                                               maxlength="1000"/>
								                                                        <label for="underline" id="underline_label" title="আন্ডারলাইন" class="input-group-addon" style="background-color: white;border: solid 1px #cacaca;padding: 0;color: black;cursor:pointer;"><span><i class="fa fa-underline"></i></span></label>
							                                                        </div>
						                                                        </div>
						                                                        <div class="col-md-12 col-sm-12 form-group showsubject">

						                                                        </div>
					                                                        </div>

					                                                        <div class="row">
						                                                        <div class="col-md-12 form-group">
			                                                                            <textarea class="noteComposer"
			                                                                                      id="noteComposer"
			                                                                                      name="description"></textarea>
						                                                        </div>
					                                                        </div>

					                                                        <div class="row">
						                                                        <div class="col-md-12 form-group">
							                                                        <form class="inbox-compose form-horizontal"
							                                                              id="fileupload"
							                                                              action="<?= $this->Url->build(['_name'=>'tempUpload']) ?>"
							                                                              method="POST"
							                                                              enctype="multipart/form-data">
								                                                        <input type="hidden" name="module_type" value="Nothi"/>
								                                                        <input type="hidden" name="module" value="Note" />
								                                                        <div class="inbox-compose-attachment">
									                                                        <!--The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload-->
									                                                        <span class="btn blue btn-mini fileinput-button round-corner-5">
			                                                    <i class="fa fa-clipboard"></i>
			                                                    <span>
			    <?php echo __(SHONGJUKTI) ?> </span>
			                                                    <input type="file" name="files[]" multiple>
			                                                </span>
									                                                        <!--The table listing the files available for upload/download-->
									                                                        <table role="presentation"
									                                                               class="table table-striped margin-top-10">
										                                                        <tbody class="files">
										                                                        </tbody>
									                                                        </table>
								                                                        </div>

								                                                        <script id="template-upload"
								                                                                type="text/x-tmpl">
			                                                {% for (var i=0, file; file=o.files[i]; i++) { %}
			                                                <tr class="template-upload fade">
			                                                <td class="name" width="30%"><span>{%=file.name%}</span></td>
			                                                <td class="size" width="40%"><span>{%=o.formatFileSize(file.size)%}</span></td>
			                                                {% if (file.error) { %}
			                                                <td class="error" width="20%" colspan="2"><span class="label label-danger">ত্রুটি</span> {%=file.error%}</td>
			                                                {% } else if (o.files.valid && !i) { %}
			                                                <td>
			                                                <p class="size">{%=o.formatFileSize(file.size)%}</p>
			                                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
			                                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
			                                                </div>
			                                                </td>
			                                                {% } else { %}
			                                                <td colspan="2"></td>
			                                                {% } %}
			                                                <td class="cancel" width="10%" align="right">{% if (!i) { %}
			                                                <button class="btn btn-sm red cancel">
			                                                <i class="fa fa-ban"></i>
			                                                <span>বাতিল করুন</span>
			                                                </button>
			                                                {% } %}</td>
			                                                </tr>
			                                                {% } %}





			                                                                                </script>
								                                                        <!--The template to display files available for download-->
								                                                        <script id="template-download"
								                                                                type="text/x-tmpl">
			                                                {% for (var i=0, file; file=o.files[i]; i++) { %}
			                                                <tr class="template-download fade">
			                                                {% if (file.error) { %}
			                                                <td class="name" width="30%"><span>{%=file.name%}</span></td>
			                                                <td class="size" width="20%"><span>{%=o.formatFileSize(file.size)%}</span></td>
			                                                <td class="error" width="30%" colspan="2"><span class="label label-danger">ত্রুটি</span> {%=file.error%}</td>
			                                                {% } else { %}
			                                                <td class="name" width="40%">
			                                                <a href="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" {%=file.thumbnailUrl?'':''%} download="{%=file.name%}">{%=file.name%}</a>
			                                                </td>
			                                                <td class="size" width="15%"><span>{%=o.formatFileSize(file.size)%}</span></td>
			                                                <td width="40%" style ="min-width: 100px!important;">

			                                                <input type="text" class="form-control note-attachment-input" id="note-attachment-input" onkeyup="check_title()" image="{%=file.url%}" file-type="{%=file.type%}">

			                                                </td>
			                                                {% } %}
			                                                <td class="delete" width="5%" align="right">
			                                                <button id="deletethis" onchange="check_title()"
			                                                class="btn red btn-sm round-corner-5" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
			                                                <i class="fa fa-times"></i>
			                                                </button>
			                                                </td>
			                                                </tr>
			                                                {% } %}</script>
							                                                        </form>
						                                                        </div>
					                                                        </div>

					                                                        <div class="row">
						                                                        <div class="col-md-12 btn-group btn-group-round">
							                                                        <button class="btn btn-sm green saveNote"
							                                                                hasnotorno="<?php echo $lastListedNote; ?>">
								                                                        <i class="fa fa-save"></i> সংরক্ষণ
							                                                        </button>
							                                                        <button class="btn btn-sm blue saveAndNewNote"
							                                                                hasnotorno="<?php echo $lastListedNote; ?>">
								                                                        <i class="fs1 a2i_gn_note2"></i> নতুন অনুচ্ছেদ
							                                                        </button>
							                                                        <button class="btn btn-sm purple btn-save-forward-nothi"
							                                                                nothi_office="<?php echo $nothi_office ?>"
							                                                                nothi_master_id="<?php echo $id ?>"
							                                                                hasnotorno="<?php echo $lastListedNote; ?>">
								                                                        <i class="fs1 a2i_gn_send2"></i> প্রেরণ
							                                                        </button>
							                                                        <button class="btn btn-sm red btn-closeNote" onclick="editBoxClose();"
							                                                                data-original-title="বন্ধ করুন"
							                                                                title="বন্ধ করুন" >
								                                                        <i class="fs1 a2i_gn_close2"></i> মুছে ফেলুন
							                                                        </button>
						                                                        </div>
					                                                        </div>
				                                                        </div>
			                                                        </div>
																<?php endif; ?>
	                                                        </div>
                                                        </div>
                                                        <div id="noteShowLoading"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="portlet-footer" style="border-top: 1px solid #eee;">
                                            <div class="row">

                                                <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-10 col-md-10 col-sm-10 text-right">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body" id="khosrapotrobody" style="display: none;">

                                </div>

                            </div>
                        </div>
                        <div class="col-md-6" style="display:flex" >
                            <div class="portlet collapsable box green" style="margin-bottom:0!important;flex:1;width: 580px !important;">
                                <div class="portlet-title">

                                    <div class="actions pull-left">
                                        <button href="javascript:;" class="btn btn-sm grey btn-maximize btn-minmax "
                                           title="">
                                            <i class="glyphicon glyphicon-chevron-left"></i>
                                        </button>
                                    </div>
                                    <div class="caption pull-right">
                                        পত্রাংশ
                                    </div>

                                </div>
                                <div class="portlet-body" id="potroShow" style="overflow-x: hidden;height: calc(100vh - 211px);overflow-y: scroll;">

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-purple" id="printAdvanceModal">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal"
                        aria-hidden="true">×
                </button>
                <h4 class="modal-title" style="float: left;">
                    প্রিন্ট করুন</h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->create('advanceSearchForm', ['id' => 'advanceSearchForm', 'class' => 'form']) ?>
                <input type="hidden" id="nothi_master_id" name="nothi_master_id"
                       value="<?= $nothiRecord['nothi_masters_id'] ?>"/>
                <input type="hidden" id="note_start" name="note_start" value="<?= $nothiRecord['id'] ?>"/>
                <input type="hidden" id="note_end" name="note_end" value="<?= $nothiRecord['id'] ?>"/>

                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2 form-group">
                        <?= $this->Form->input('margin_top', ['class' => 'form-control input-sm ', 'label' => 'উপর', 'default' => '1.0', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2 form-group">
                        <?= $this->Form->input('margin_bottom', ['class' => 'form-control input-sm', 'label' => 'নিচ', 'default' => '.75', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2 form-group">
                        <?= $this->Form->input('margin_left', ['class' => 'form-control input-sm', 'label' => 'বাম', 'default' => '0.75', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2 form-group">
                        <?= $this->Form->input('margin_right', ['class' => 'form-control input-sm', 'label' => 'ডান', 'default' => '0.75', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2 form-group">
                        <?= $this->Form->input('orientation', ['class' => 'form-control input-sm', 'label' => 'ধরন', 'type' => 'select', 'options' => ['portrait' => 'Portrait', 'landscape' => 'Landscape']]) ?>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-1 col-lg-1 form-group">
                        <?= $this->Form->input('potro_header', ['class' => 'form-control input-sm', 'label' => __("Banner"), 'type' => 'select', 'options' => [0 => 'হ্যাঁ', 1 => 'না']]) ?>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-1 col-lg-1 form-group">
                        <br/>
                        <input value="প্রিভিউ"
                               type="submit"
                               class="btn btn-primary btn-sm btn-pdf"
                               type="button"/>
                    </div>

                </div>
                <?= $this->Form->end() ?>

                <div class="row">
                    <div class="col-md-12">
                        <embed class="showPreview" src="" style=" width:100%; height: 600px;"
                               type="application/pdf"></embed>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
	            <div class="btn-group btn-group-round">
	                <button type="button" data-dismiss="modal"
	                        class="btn  btn-danger"><i class="fs1 a2i_gn_close2"></i>  বন্ধ করুন
	                </button>
	            </div>
            </div>
        </div>
    </div>
</div>
<!--dictionary input-->
<div>
    <input type="hidden" id="word" value="">
    <input type="hidden" id="space" value="">
</div>
<div id="responsiveBookmarkDescription" class="modal fade modal-purple height-auto" tabindex="-1" aria-hidden="true"
     data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title">পতাকা</div>
            </div>
            <div class="modal-body">
                <div class="scrollerr" style="height:100%" data-always-visible="1" data-rail-visible1="1">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 form-group">
                            <label>রঙ</label>
                            <select name="color" class="form-control" id="colorBookmark">
                                <option class="label label-warning" value="warning" selected="selected">কমলা
                                </option>
                                <option class="label label-success" value="success">সবুজ</option>
                                <option class="label label-info" value="info">নীল</option>
                                <option class="label label-danger" value="danger">লাল</option>
                                <option class="label label-default" value="default">ছাই</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6 form-group pdfPageOption" style="display: none;">
                            <label>পিডিএফ ফাইলের জন্য পেজ নাম্বার</label>
                            <input type="text" name="bookmarkPage" class="form-control" id="bookmarkPage"/>

                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>পতাকা শিরোনাম </label>
                            <input type="hidden" id="bookmark_potro"/>
                            <input type="text" class="form-control bookmarkDes" id="bookmarkDes" name="description"/>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
	            <div class="btn-group btn-group-round">
	                <button type="button" class="btn green saveBookmark" data-dismiss="modal">পতাকা সংযুক্তি</button>
	                <button type="button" data-dismiss="modal" class="btn  btn-danger"
	                        onclick="$('.bookmarkDes').text('');$('.bookmarkPage').val('')">
		                <i class="fs1 a2i_gn_close2"></i> বন্ধ করুন
	                </button>
	            </div>
            </div>
        </div>
    </div>
</div>

<div id="responsiveNothiUsers" class="modal fade modal-purple" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title">পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন</div>
            </div>
            <div class="modal-body">
                <div>
                    <input type="hidden" name="nothimasterid"/>
                    <div class="user_list">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
	            <div class="btn-group btn-group-round">
	                <button type="button" class="btn purple sendNothi"><i class="fs1 a2i_gn_send2"></i> প্রেরণ করুন</button>
	                <button type="button" data-dismiss="modal" class="btn  btn-danger">
		                <i class="fs1 a2i_gn_close2"></i> বন্ধ করুন
	                </button>
	            </div>
            </div>
        </div>
    </div>
</div>

<div id="responsiveSendNothi" class="modal fade modal-purple" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title">পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন</div>
            </div>
            <div class="modal-body">
                <div>
                    <input type="hidden" name="nothimasterid"/>

                    <div class="user_list">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
	            <div class="btn-group btn-group-round">
	                <button type="button" class="btn purple sendSaveNothi"><i class="fs1 a2i_gn_send2"></i> প্রেরণ করুন</button>
	                <button type="button" data-dismiss="modal" onclick="$('#sendsaveModal').val(0);"
	                        class="btn  btn-danger"><i class="fs1 a2i_gn_close2"></i> বন্ধ করুন
	                </button>
	            </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-purple" id="responsivePermissionEdit" data-backdrop="static" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-full">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title"></div>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
	            <div class="btn-group btn-group-round">
	                <button type="button" class="btn btn-md green nothiPermissionUpdate"
	                        aria-hidden="true"><?php echo __(SAVE); ?></button>
	                <button type="button" class="btn btn-md btn-danger" data-dismiss="modal"
	                        aria-hidden="true"><i class="fs1 a2i_gn_close2"></i> <?php echo __("বন্ধ করুন"); ?></button>
	            </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade modal-purple UIBlock" id="responsiveModal" data-backdrop="static" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <div class="">
                    <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="modal-title">
                        গার্ড ফাইল
                        <div type="button" class="btn btn-success pull-right btn-sm" id="portalGuardFileImport"
                             aria-hidden="true">পোর্টালের গার্ড ফাইল
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body ">
                <div class="row form-group">
                    <label class="col-md-1 control-label">ধরন</label>
                    <div class="col-md-5">
                        <?= $this->Form->input('guard_file_category_id', ['label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => ["0" => "সকল"] + $guarfilesubjects]) ?>
                    </div>
                </div>
                <br/>

                <table class='table table-striped table-bordered table-hover' id="filelist">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 10%">ক্রম</th>
                        <th class="text-center" style="width: 30%">ধরন</th>
                        <th class="text-center" style="width: 40%">নাম</th>
                        <th class="text-center" style="width: 20%">কার্যক্রম</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
	            <div class="btn-group btn-group-round">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fs1 a2i_gn_close2"></i> <?= __('বন্ধ করুন') ?></button>
	            </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade modal-purple" id="responsiveOnuccedModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
             <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade modal-purple" id="portalResponsiveModal" data-backdrop="static" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title">পোর্টালের গার্ড ফাইল</div>
            </div>
            <div class="modal-body ">
                <div class="row form-group">
                    <label class="col-md-2 control-label text-right">অফিসের ধরন</label>
                    <div class="col-md-4">
                        <?= $this->Form->input('layer_ids', ['label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => ["0" => "--", "2" => "মন্ত্রণালয়/বিভাগ", "3" => "অধিদপ্তর/সংস্থা"]]) ?>
                    </div>
                    <label class="col-md-1 control-label">অফিস</label>
                    <div class="col-md-5">
                        <?= $this->Form->input('portal_guard_file_types', ['label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => ["0" => "--"]]) ?>
                    </div>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-6 pull right" style="padding-bottom: 5px">
                    <input type="text" class="form-control" id="search" placeholder=" খুজুন">
                </div>
                <table class='table table-striped table-bordered table-hover' id="portalfilelist">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 10%">ক্রম</th>
                        <th class="text-center" style="width: 30%">ধরন</th>
                        <th class="text-center" style="width: 40%">নাম</th>
                        <th class="text-center" style="width: 20%">কার্যক্রম</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
	            <div class="btn-group btn-group-round">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fs1 a2i_gn_close2"></i> <?= __('বন্ধ করুন') ?></button>
	            </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade modal-purple" id="portalDownloadModal" role="dialog" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">পোর্টাল গার্ড ফাইল ডাউনলোড করুন</h4>
            </div>
            <div class="modal-body ">
                <form class="form-horizontal" id="portalDownloadForm">
                    <div class="row">
                        <div class="col-md-10">
                            <?php
                            echo $this->Form->hidden('uploaded_attachments');
                            ?>
                            <div class="form-group">
                                <label class="control-label col-md-2">শিরোনাম <span class="required"> * </span></label>

                                <div class="col-md-10">
                                    <input type="text" name="name_bng" class="form-control" data-required="1"
                                           id="name-bng">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="control-label col-md-2">ধরন <span class="required"> * </span></label>

                                <div class="col-md-10">
                                    <?= $this->Form->input('guard_file_category_id', ['id' => 'portal_guard_file_category_id', 'label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => $guarfilesubjects, 'empty' => 'ধরন নির্বাচন করুন']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <span class="pull-left WaitMsg"></span>
	            <div class="btn-group btn-group-round">
	                <button data-bb-handler="success" type="button" onclick="PORTAL_GUARD_FORM.submit()"
	                        class="btn green submitbutton">সংরক্ষণ করুন
	                </button>
	                <button data-bb-handler="danger" type="button" class="btn red" data-dismiss="modal"><i class="fs1 a2i_gn_close2"></i> বন্ধ করুন</button>
	            </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"
        type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/nothi_master_movements.js?v=<?= time() ?>"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/nothi_details.js?v=<?= time() ?>"></script>
<!--<script src="--><?php //echo CDN_PATH; ?><!--projapoti-nothi/js/ekko-lightbox.js"></script>-->
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/note-fileupload.js" type="text/javascript"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript"
        src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_guard_files.js"></script>
<?= $this->element('froala_editor_js_css') ?>
<script src="<?php echo CDN_PATH; ?>js/client.min.js" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/3rd-party-integration.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<?php (!empty($potroAttachmentRecord) ? $potroAttachmentRecord : []) ?>

<script>
    (function () {
        var numbers = {
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯',
            0: '০'
        };

        function replaceNumbers(input) {
            var output = [];
            for (var i = 0; i < input.length; ++i) {
                if (numbers.hasOwnProperty(input[i])) {
                    output.push(numbers[input[i]]);
                } else {
                    output.push(input[i]);
                }
            }
            return output.join('');
        }
        $(window).load(function() {
            var db = ProjapotiOffline.createDatabase('offline_note_draft');
            var docid = '<?php echo $employee_office['office_id'] . '_' . $employee_office['officer_id'] . '_' . $employee_office['office_unit_organogram_id'] . '_' . $id ?>';
            db.get(docid).then(function (doc) {
                setTimeout(function () {
                    initiatefroala();
                    $('#noteComposer').froalaEditor('html.set', doc.body);
                    setTimeout(function () {
                        $.contextMenu('destroy');
                        iniDictionary();
                        $('#noteComposer').froalaEditor('events.focus', true);
                        getCaretPosition();
                        getAutolist();
                    }, 200);
                }, 300);
                NoteFileUpload.init();

                $('#noteId').val(doc.noteid);
                $('#notesubject').val(doc.subject);
                $('.showsubject').text(doc.subject);
                $('#bibeccoPotro').val(0);
                $('.addNewNote').hide();
                $('.btn-forward-nothi').hide();
                $('.btn-sar-nothi-draft').hide();
                $('.btn-nisponno-nothi').hide();
                $('#responsiveNoteEdit').show();
                var value = $.trim($(".nothipartdiv.btn-success").text());
                $("#poro").text(value);
                var lastnot = 0;
                if (value != '' && value != null && value != 'undefined') {
                    $.each($('.nothiOnucched[partno=' + value + ']'), function (i) {
                        lastnot = Math.max(lastnot, $(this).attr('notenoen'));
                    });
                }
                var lastnoteno = -1;
                if (lastnot > 0) {
                    lastnoteno = lastnot;
                }
                var bn = replaceNumbers("'" + parseInt(parseInt(lastnoteno) + 1) + "'");
                bn = bn.replace("'", '');
                bn = bn.replace("'", '');
                $('#notno').text(bn);
            }).then(function (response) {

            }).catch(function (err) {

                <?php if ($lastListedNote == -1): ?>
                setTimeout(function () {
                    initiatefroala();
                    setTimeout(function () {
                        $.contextMenu('destroy');
                        iniDictionary();
                        $('#noteComposer').froalaEditor('events.focus', true);
                        getCaretPosition();
                        getAutolist();
                    }, 200);
                }, 300);
                NoteFileUpload.init();

                $('.addNewNote').hide();
                $('.btn-forward-nothi').hide();
                $('.btn-sar-nothi-draft').hide();
                $('.btn-nisponno-nothi').hide();
                $('.btn-closeNote').hide();
                $('#responsiveNoteEdit').show();
                var value = $.trim($(".nothipartdiv.btn-success").text());
                $("#poro").text(value);
                var lastnot = 0;
                if (value != '' && value != null && value != 'undefined') {
                    $.each($('.nothiOnucched[partno=' + value + ']'), function (i) {
                        lastnot = Math.max(lastnot, $(this).attr('notenoen'));
                    });
                }
                var lastnoteno = -1;
                if (lastnot > 0) {
                    lastnoteno = lastnot;
                }
                var bn = replaceNumbers("'" + parseInt(parseInt(lastnoteno) + 1) + "'");
                bn = bn.replace("'", '');
                bn = bn.replace("'", '');
                $('#notno').text(bn);
                <?php endif; ?>

            });
        });

    }(jQuery));

    function check_title() {
        $('[id^=dropdown-menu-attachment-ref] ul').html('');
        $('[id^=dropdown-menu-attachment-ref] ul').append($('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="attachment-ref" data-param1="" title="" aria-selected="false">--</a></li>'));

        $('#fileupload .note-attachment-input').each(function () {
            var tx = $(this).val();
            var file_handle = $(this).attr('image');
            var file_type = $(this).attr('file-type');

            $('[id^=dropdown-menu-attachment-ref] ul').append('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="attachment-ref" data-param1="' + file_handle + '" ft="' + file_type + '" title="" aria-selected="false">' + tx + '</a></li>');

//            $('#note-attachment-title').append($("<option value='" + file_handle + "' ft='" + file_type + "'></option>").text(tx));
        });
        if (parseInt($('#noteId').val()) > 0) {
            $.each($('#responsiveNoteEdit').next().next().find('.preview_attachment a'), function (i, v) {
                var tx = '';
                var d_text = 'No_user_input_file';
                if ($(this).attr('user_file_name') == d_text) {
                    tx = v.text;
                } else {
                    tx = $(this).attr('user_file_name');
                }
                var file_type = v.type;
                var file_handle = ' <?= FILE_FOLDER ?>' + v.download;

                $('[id^=dropdown-menu-attachment-ref] ul').append('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="attachment-ref" data-param1="' + file_handle + '" ft="' + file_type + '" title="" aria-selected="false">' + tx + '</a></li>');

//                $('#note-attachment-title').append($("<option value='" + file_handle + "' ft='" + file_type + "'></option>").text(tx));
            });
        }
    }

    $('.preview_attachment a').on('click', function (e) {
        e.preventDefault();
    });
</script>
<script>
    var options = {
        translation:{
            A: {pattern: /[\u09E6-\u09EF0-9,]/},
        },
        placeholder: "পৃষ্ঠা সংখ্যার মাঝে কমা ব্যবহার করুন (একাধিক পৃষ্ঠা রেফারেন্সের ক্ষেত্রে)। যেমনঃ ১,২,৩...",
        onChange: function (cep) {
            $('#selectpage').val(cep.replace(/,+/g,','));
        }
    };
    $('#selectpage').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
        ,options);
    var signature = '<?=!empty($employee_office['default_sign'])?$employee_office['default_sign']:0?>';
    var part_no = '<?=$id?>';
    function callGuardFile() {
        $('#selectpage').val('');
        var grid = new Datatable();

        grid.init({
            "destroy": true,
            src: $('#filelist'),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function (grid) {
                // execute some code on ajax data load

            },
            loadingMessage: 'লোড করা হচ্ছে...',
            dataTable: {
                "destroy": true,
                "scrollY": "500",
                "dom": "<'row'<'col-md-8 col-sm-12'li><'col-md-4 col-sm-12'<'table-group-actions pull-right'p>>r>t",
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 500],
                    ["১০", "২০", "৫০", "১০০", "৫০০"] // change per page values here
                ],
                "pageLength": 50, // default record count per page
                "ajax": {
                    "url": js_wb_root + 'GuardFiles/officeGuardFile', // ajax source
                },
                "bSort": false,
                "ordering": false,
                "language": {// language settings
                    // metronic spesific
                    "metronicGroupActions": "_TOTAL_ রেকর্ড নির্বাচন করা হয়েছে:  ",
                    "metronicAjaxRequestGeneralError": "অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না।",
                    // data tables spesific
                    "lengthMenu": "<span class='seperator'></span>দেখুন _MENU_ ধরন",
                    "info": "<span class='seperator'>|</span>মোট _TOTAL_ টি পাওয়া গেছে",
                    "infoEmpty": "",
                    "emptyTable": "কোনো রেকর্ড  নেই",
                    "zeroRecords": "কোনো রেকর্ড  নেই",
                    "paginate": {
                        "previous": "প্রথমটা",
                        "next": "পরবর্তীটা",
                        "last": "শেষেরটা",
                        "first": "প্রথমটা",
                        "page": "পাতা",
                        "pageOf": "এর"
                    }
                }
            }
        });
        $('#guard-file-category-id').off('click').on('click', function () {
            grid.setAjaxParam('guard_file_category_id', $('#guard-file-category-id').val());
            $('.filter input, .filter select').each(function () {
                var $item = $(this);
                grid.setAjaxParam($item.attr('name'), $item.val());
            });
            grid.getDataTable().ajax.url = js_wb_root + 'GuardFiles/officeGuardFile';
            grid.getDataTable().ajax.reload();
        });
        $(document).off('click', '.showDetailAttach').on('click', '.showDetailAttach', function (ev) {
            ev.preventDefault();
            getPopUpPotro($(this).attr('href'), $(this).attr('title'));
        });
        $(document).off('click', '.tagfile').on('click', '.tagfile', function (ev) {
            ev.preventDefault();
            var href = $(this).closest('div').find('.showDetailAttach').attr('href');
            var value = $(this).closest('div').find('.showDetailAttach').attr('data-title');

            bootbox.dialog({
                message: "বাছাইকৃত ফাইলটির কোন পেজটি রেফারেন্স করতে চান?<br/><div class='form-group'><input type='text' class='input-sm form-control' name='selectpage' id='selectpage' placeholder='সকল পেজ' /></div>",
                title: "গার্ড ফাইল রেফারেন্স",
                buttons: {
                    success: {
                        label: "হ্যাঁ",
                        className: "green",
                        callback: function () {
                            var pages = $('#selectpage').val();
                            if (!isEmpty(pages)) {
                            var single_page = pages.split(',');
                            $.each(single_page, function (i,v) {
                                if(!isEmpty(v)){
                                    pasteHtmlAtCaret(" বিবেচ্য গার্ড ফাইল  <a class='showforPopup'  href='" + href + "/" + bnToen(v) + "' title=' গার্ড ফাইল'>" + value + " (পৃষ্ঠা " + enTobn(v) + ")" + "</a>, &nbsp;");
                                    $('#responsiveModal').modal('hide');
                                    $('#noteComposer').froalaEditor('events.focus', true);
                                    }
                                });

                            // $('#noteComposer').froalaEditor('html.insert', " বিবেচ্য গার্ড ফাইল  <a class='showforPopup'  href='" + href + "/" + page + "' title=' গার্ড ফাইল'>" + value + "</a>, &nbsp;");

                        } else {
                                pasteHtmlAtCaret(" বিবেচ্য গার্ড ফাইল  <a class='showforPopup'  href='" + href + "' title=' গার্ড ফাইল'>" + value + "</a>, &nbsp;");
                                $('#responsiveModal').modal('hide');
                                $('#noteComposer').froalaEditor('events.focus', true);

                            }
                        }
                    },
                    danger: {
                        label: "না",
                        className: "red",
                        callback: function () {
                            $('#selectpage').val('');
                        }
                    }
                }
            });
        });
    }

    $('body').addClass('page-quick-sidebar-over-content page-full-width');

    function initiatefroala() {
        if(typeof(viewDecisionPanel) == 'function'){
            viewDecisionPanel();
        }
        $.FroalaEditor.DefineIcon('bibeccoPotro', {NAME: 'envelope'});
        $.FroalaEditor.RegisterCommand('bibeccoPotro', {
            title: 'বিবেচ্য পত্র বাছাই করুন',
            type: 'dropdown',
            focus: true,
            undo: false,
            refreshAfterCallback: true,
                options: { '0':'--' },
            callback: function (cmd, val) {
                if (val != '0') {
                    var value = $('[id^=dropdown-menu-bibeccoPotro] ul li a[data-param1="' + val + '"]').attr('nothi_potro_page_bn');
                    $('#noteComposer').froalaEditor('html.insert', " বিবেচ্য পত্র: <a class='showforPopup'  href='" + EngFromBn(val) + "/potro' title='পত্র'>" + value + "</a>, &nbsp;");

                }
            }
        });

        $.FroalaEditor.DefineIcon('bibeccoPotaka', {NAME: 'flag'});
        $.FroalaEditor.RegisterCommand('bibeccoPotaka', {
            title: 'পতাকা বাছাই করুন',
            type: 'dropdown',
            focus: false,
            undo: false,
            refreshAfterCallback: true,
            options: {
                '--': '--'
            },
            callback: function (cmd, val) {
                var value = $('[id^=dropdown-menu-bibeccoPotaka] ul li a[data-param1="' + val + '"]').attr('attachment_id');
                if (!(isEmpty(value)) && value.length > 0 && value != '--') {
                    var masterid = $('[id^=dropdown-menu-bibeccoPotaka] ul li a[data-param1="' + val + '"]').attr('nothi_master_id');
                    var page = $('[id^=dropdown-menu-bibeccoPotaka] ul li a[data-param1="' + val + '"]').attr('page');
                    var text = $('[id^=dropdown-menu-bibeccoPotaka] ul li a[data-param1="' + val + '"]').text();

                    $('#noteComposer').froalaEditor('html.insert', " বিবেচ্য পতাকা <a class='showforPopup' value=" + value + "  href='" + value + "/potaka/" + masterid + "/" + page + "' title='পতাকা'>" + text + "</a>, &nbsp;", false);
                }
            }
        });

        $.FroalaEditor.DefineIcon('bibeccoOnucced', {NAME: 'pencil-square-o'});
        $.FroalaEditor.RegisterCommand('bibeccoOnucced', {
            title: 'অনুচ্ছেদ বাছাই করুন',
            type: 'dropdown',
            focus: false,
            undo: false,
            refreshAfterCallback: true,
            options: {
                '--': '--'
            },
            callback: function (cmd, val) {
                var text_value = $('[id^=dropdown-menu-bibeccoOnucced] ul li a[data-param1="' + val + '"]').text();
                if (text_value.length > 0 && text_value != '--') {
                    if ($('[id^=dropdown-menu-bibeccoOnucced] ul li a[data-param1="' + val + '"]').hasClass('fullNote')) {

                        $('#noteComposer').froalaEditor('html.insert', "অনুচ্ছেদ <a target='__tab' href='<?php
                            echo $this->Url->build(['_name' => 'noteDetail'], true)
                            ?>" + $('[id^=dropdown-menu-bibeccoOnucced] ul li a[data-param1="' + text_value + '"]').attr('nothi_part_no') + "' title='নোট'> " + text_value + "</a>, &nbsp;", false);

                    } else {

                        $('#noteComposer').froalaEditor('html.insert', 'অনুচ্ছেদ <a class="showforPopup" href="' + val + '" title="অনুচ্ছেদ">' + text_value + '</a>, &nbsp;', false);
                    }

                }
            }
        });

        $.FroalaEditor.DefineIcon('attachment-ref', {NAME: 'paperclip'});
        $.FroalaEditor.RegisterCommand('attachment-ref', {
            title: 'সংযুক্ত-রেফ বাছাই করুন',
            type: 'dropdown',
            focus: false,
            undo: false,
            refreshAfterCallback: true,
            options: {
                '--': '--'
            },
            callback: function (cmd, val) {

                if (val.length > 0 && val != '--') {

                    var value_select = $('[id^=dropdown-menu-attachment-ref] ul li a[data-param1="' + val + '"]').text();
                    var file_type = $('[id^=dropdown-menu-attachment-ref] ul li a[data-param1="' + val + '"]').attr('ft');
                    var masterid = <?= $id; ?>;

                    $.ajax({
                        type: 'POST',
                        url: "<?php
                            echo $this->Url->build(['controller' => 'NothiMasters',
                                'action' => 'save_nothi_attachment_refs'])
                            ?>",
                        data: {
                            "id": <?php echo $nothiRecord['nothi_masters_id'] ?>,
                            "type": file_type,
                            "part_no": masterid,
                            "file_name": val,
                            "nothi_office": <?= $nothi_office ?>},
                        success: function (data) {
                            if (data.status == 'success') {
                                var id_nothi_attachment_ref = data.id;
                                $('#noteComposer').froalaEditor('html.insert', "<a  class='showforPopup' value=" + id_nothi_attachment_ref + "  href='" + id_nothi_attachment_ref + "/note-ref/" + masterid + "' title='" + value_select + "'>" + value_select + "</a>, &nbsp;");

                            } else {

                            }
                        }
                    });
                }
            }
        });

        $.FroalaEditor.DefineIcon('noteDecision', {NAME: 'gavel'});
        $.FroalaEditor.RegisterCommand('noteDecision', {
            title: 'সিদ্ধান্ত বাছাই করুন',
            type: 'dropdown',
            focus: false,
            undo: false,
            refreshAfterCallback: true,
            options: {
                '--': '--',
                <?php
                $indx = 0;
                if (!empty($noteDecision)) {
                    foreach ($noteDecision as $key => $value) {
                        if (!($indx == 0)) {
                            echo ',';
                        }
                        echo "'" . h($value) . "' : '" . h($value) . "'";
                        $indx++;
                    }
                }?>
            },
            callback: function (cmd, val) {
                if (val.length > 0 && val != '--') {
                    $('#noteComposer').froalaEditor('html.insert', val + ' ', false);
                }
            }
        });

        $.FroalaEditor.DefineIconTemplate('material_design', '<i class="fs0 [NAME]"></i>');
        $.FroalaEditor.DefineIcon('guardFile', {NAME: 'fs0 efile-guard_file1', template: 'material_design'});
        $.FroalaEditor.RegisterCommand('guardFile', {
            title: 'গার্ড ফাইল',
            focus: true,
            undo: true,
            showOnMobile: true,
            refreshAfterCallback: true,
            callback: function () {
                $('#responsiveModal').modal('show');
                $("#guard-file-category-id ").val(0);
                $("#s2id_guard-file-category-id [id^=select2-chosen]").text('সকল');
                callGuardFile();
            }
        });

        $.FroalaEditor.DefineIcon('spell-checker', {NAME: 'check-circle'});
        $.FroalaEditor.RegisterCommand('spell-checker', {
            title: 'বানান পরীক্ষক',
            focus: true,
            undo: true,
            showOnMobile: true,
            refreshAfterCallback: true,
            callback: function () {
                getWordSeperated();
            }
        });

        $.FroalaEditor.DefineIcon('spell-checker-off', {NAME: 'minus-circle'});
        $.FroalaEditor.RegisterCommand('spell-checker-off', {
            title: 'বানান পরীক্ষক বন্ধ করুন',
            focus: true,
            undo: true,
            showOnMobile: true,
            refreshAfterCallback: true,
            callback: function () {
                removeSpellChecker();
            }
        });

        var buttons = getButtonList();

        var editor = $('#noteComposer').froalaEditor({
            key: 'xc1We1KYi1Ta1WId1CVd1F==',
            toolbarSticky: false,
            wordPasteModal: true,
			wordAllowedStyleProps: ['font-size', 'background', 'color', 'text-align', 'vertical-align', 'background-color', 'padding', 'margin', 'height', 'margin-top', 'margin-left', 'margin-right', 'margin-bottom', 'text-decoration', 'font-weight'],
			wordDeniedTags: ['a','form'],
			wordDeniedAttrs: ['width'],
            tableResizerOffset: 10,
            tableResizingLimit: 20,
            toolbarButtons: buttons,
            toolbarButtonsMD: buttons,
            toolbarButtonsSM: buttons,
            toolbarButtonsXS: buttons,
            placeholderText: '',
            height: 400,
            enter: $.FroalaEditor.ENTER_BR,
            fontSize: ['10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'],
            fontSizeDefaultSelection: '13',
            tableEditButtons: ['tableHeader', 'tableRemove', '|', 'tableRows', 'tableColumns', 'tableCells', '-', 'tableCellBackground', 'tableCellVerticalAlign', 'tableCellHorizontalAlign', 'tableCellStyle'],
            fontFamily: {
                "Nikosh,SolaimanLipi,'Open Sans', sans-serif": "Nikosh",
                "Arial": "Arial",
                "Kalpurush": "Kalpurush",
                "SolaimanLipi": "SolaimanLipi",
                "'times new roman'": "Times New Roman",
                "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
            }
        }).on('froalaEditor.contentChanged', function (e, editor) {

            var db = ProjapotiOffline.createDatabase('offline_note_draft');
            var docid = '<?php echo $employee_office['office_id'] . '_' . $employee_office['officer_id'] . '_' . $employee_office['office_unit_organogram_id'] . '_' . $id ?>';
            db.get(docid).then(function (doc) {
                return db.put({
                    _id: docid,
                    _rev: doc._rev,
                    noteid: $('#noteId').val(),
                    subject: $('#notesubject').val(),
                    body: editor.html.get()
                });
            }).catch(function (err) {

                return db.put({
                    _id: docid,
                    noteid: $('#noteId').val(),
                    subject: $('#notesubject').val(),
                    body: editor.html.get()
                });
            });
        });

        $('#noteComposer').on('froalaEditor.keyup', function (e, editor, keyupEvent) {
            getCaretPosition();
        });
        $('#noteComposer').on('froalaEditor.mouseup', function (e, editor, keyupEvent) {
            getCaretPosition();
        });

        var db = ProjapotiOffline.createDatabase('offline_note_draft');
        var docid = '<?php echo $employee_office['office_id'] . '_' . $employee_office['officer_id'] . '_' . $employee_office['office_unit_organogram_id'] . '_' . $id ?>';
        db.get(docid).then(function (doc) {
            $('#noteComposer').froalaEditor('html.set', doc.body);
            var value = $.trim($(".nothipartdiv.btn-primary").text());
            $("#poro").text(value);
            var lastnot = 0;

            if (value != '' && value != null && value != 'undefined') {
                $.each($('.nothiOnucched[partno=' + value + ']'), function (i) {
                    lastnot = Math.max(lastnot, $(this).attr('notenoen'));
                });
            }
            var lastnoteno = -1;
            if (lastnot > 0) {
                lastnoteno = lastnot;
            }
            var bn = replaceNumbers("'" + parseInt(parseInt(lastnoteno) + 1) + "'");
            bn = bn.replace("'", '');
            bn = bn.replace("'", '');
            $('#notno').text(bn);
        }).then(function (response) {
        }).catch(function (err) {
        });

        $('.nothiDetailsPage').height($('.nothiDetailsPage .noteSubject').innerHeight());
    }

    jQuery.ajaxSetup({
        cache: true
    });

    $(document).on('click', '#notesShow_new .pagination a', function (e) {
        e.preventDefault();
        if ($(this).parent('li').hasClass('disabled') == false) {
            noteShow(this.href);
        }

        return false;
    });

    $(document).off('change', '.preview_attachment').on('change', '.preview_attachment', function () {
        var tx = $('.preview_attachment').text();
        var file_type = $('.preview_attachment').attr('type');
        var file_handle = $('.preview_attachment').attr('href');
        $('#note-attachment-title').append($("<option value='" + file_handle + "' ft='" + file_type + "'></option>").text(tx));
    });
    $(document).off('input', '#notesubject').on('input', '#notesubject', function () {
        showSubjectStyleUpdate();
    });

    $(document).off('change', '#underline').on('change', '#underline', function () {
        showSubjectStyleUpdate();
    });

    function showSubjectStyleUpdate(){
        if($("#underline").is(":checked")){
            $('.showsubject').html('<b><u>'+ $("#notesubject").val()+'</u></b>');
            $('#underline_label').css('background-color', '#c1c1c1');
        } else{
            $('.showsubject').html('<b>'+ $("#notesubject").val()+'</b>');
            $('#underline_label').css('background-color', '#ffffff');
        }
    }
    <?php if ($privilige_type >= 1): ?>
    $(document).on('click', '.btn-onucced-edit', function () {
        if($(".fr-view").length > 0){
            $('#noteComposer').froalaEditor('destroy');
        }
        editBoxClose();

        NoteFileUpload.init();
        $('#noteId').val($(this).closest('.noteDetails').find('.noteDescription').attr('id'));
        var notesubject = $(this).closest('.noteDetails').find('.noteSubject b').text();
        $('#notesubject').val(notesubject);
        if($(this).closest('.noteDetails').find('.noteSubject b').has("u").length >0){
            $('#underline').prop('checked',true);
            $('#underline_label').css('background-color', '#c1c1c1');
            $('.showsubject').html('<b><u>'+ notesubject +'</u></b>');
        } else {
            $('.showsubject').html('<b>'+ notesubject +'</b>');
        }

        if ($(this).closest('.noteDetails').find('.noteSubject b').text().length > 0) {
            $('.notesubjectdiv').show().removeClass('hidden');
        }

        var onucched_html_data = $(this).closest('.noteDetails').find('.noteDescription').html();
        var editDiv = $('#responsiveNoteEdit').detach();
        $(this).closest('.noteDetails').find('.noteDescription').after(editDiv);
        $(this).closest('.noteDetails').find('.noteDescription').hide();
        $(this).closest('.noteDetails').find('.btn-onucced-delete').hide();
        $(this).closest('.noteDetails').find('.btn-onucced-potrojari').hide();
        $(this).closest('.noteDetails').find('.btn-onucced-edit').hide();
        $('#responsiveNoteEdit').show();
        $('.addNewNote').hide();
        $('.btn-sar-nothi-draft').hide();
        $('.btn-forward-nothi').hide();
        $('.btn-sar-nothi-draft').hide();
        $('.btn-nisponno-nothi').hide();
        $('.saveAndNewNote').hide();
        $('#notno').text('');
        $("#poro").text('');

        $('.removeOnuchaddiv').hide();

        setTimeout(function () {
            initiatefroala();
            $('#noteComposer').froalaEditor('html.set', onucched_html_data);
            setTimeout(function () {
                $.contextMenu('destroy');
                check_title();
                iniDictionary();
                $('#noteComposer').froalaEditor('events.focus', true);
                getCaretPosition();
                getAutolist();
            }, 200);
        },300);

        $('#onucchedBody').animate({
            scrollTop: $("#notesShow_new").height() + 70
        }, 'slow');

        $.each($('#responsiveNoteEdit').next().next().find('.preview_attachment a'), function (i, v) {
            var tx = '';
            var d_text = 'No_user_input_file';
            if ($(this).attr('user_file_name') == d_text) {
                tx = v.text;
            } else {
                tx = $(this).attr('user_file_name');
            }
            var file_type = v.type;
            var file_handle = v.href;
            $('#note-attachment-title').append($("<option value='" + file_handle + "' ft='" + file_type + "'></option>").text(tx));
        });
    });

    function editBoxClose() {
        $(".saveNote").removeAttr('disabled');
        $(".saveAndNewNote").removeAttr('disabled');
        $('.removeOnuchaddiv').show();

        // stat new code
        height = Metronic.getViewPort().height - $('.page-header').outerHeight(true) - $('.page-footer').outerHeight(true) - $('.page-title').outerHeight(true) - $('.page-bar').outerHeight(true);
        height = height - $('.full-height-content').find('.portlet-title').outerHeight(true) - parseInt($('.full-height-content').find('.portlet-body').css('padding-top')) - parseInt($('.full-height-content').find('.portlet-body').css('padding-bottom')) - 2;
        $('#notesShow_new').css('overflow', 'auto');
        //$('#notesShow_new').css('height', height - 141);
        // end new code
        if ($('#responsiveNoteEdit').find('#noteId').val() != '' || $('#responsiveNoteEdit').find('#noteId').val() != 0) {

            $('.addNewNote').show();
            $('.btn-sar-nothi-draft').show();
            $('.btn-forward-nothi').show();
            $('.btn-sar-nothi-draft').show();
            $('.btn-nisponno-nothi').show();
            $('.noteDescription').show();
            $('.btn-onucced-delete').show();
            $('.btn-onucced-edit').show();
            $('.btn-onucced-potrojari').show();
            var editDiv = $('#responsiveNoteEdit').detach();
            $('.addNewNote').after(editDiv);
            $('#responsiveNoteEdit').hide();
            $('.notesubjectdiv').hide().addClass('hidden');

            var db = ProjapotiOffline.createDatabase('offline_note_draft');
            var docid = '<?php echo $employee_office['office_id'] . '_' . $employee_office['officer_id'] . '_' . $employee_office['office_unit_organogram_id'] . '_' . $id ?>';

            db.get(docid).then(function (doc) {

                return db.remove(doc);


            }).then(function (response) {
            }).catch(function (err) {


            });

        } else {
            $('#responsiveNoteEdit').hide();
        }
        heightlimit();
    }


    var numbers = {
        1: '১',
        2: '২',
        3: '৩',
        4: '৪',
        5: '৫',
        6: '৬',
        7: '৭',
        8: '৮',
        9: '৯',
        0: '০'
    };

    function replaceNumbers(input) {
        var output = [];
        for (var i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }

    $(document).on('click', '.addNewNote', function () {
        if($(".fr-view").length > 0){
            $('#noteComposer').froalaEditor('destroy');
        }
        editBoxClose();

        $('#noteId').val(0);
        $('#notesubject').val('');
        $('.showsubject').text($('#notesubject').val());
        $('#bibeccoPotro').val(0);
        $('.saveAndNewNote').show();
        NoteFileUpload.init();
        $('#notesShow_new').css('overflow', 'auto');
        //$('#notesShow_new').css('height', 'auto');

        $(document).ready(function () {
            initiatefroala();
            setTimeout(function () {
                $.contextMenu('destroy');
                iniDictionary();
                $('#noteComposer').froalaEditor('events.focus', true);
                getCaretPosition();
                getAutolist();
            }, 200);
        });

        $('.addNewNote').toggle();
        $('.btn-sar-nothi-draft').hide();
        $('.btn-forward-nothi').hide();
        $('.btn-sar-nothi-draft').hide();
        $('.btn-nisponno-nothi').hide();

        if ($('.note_show_details').length > 0) {
            $('.notesubjectdiv').hide().addClass('hidden');
        } else {
            $('.notesubjectdiv').show().removeClass('hidden');
        }

        if ($('.noteDetails').length > 0) {
            $('.notesubjectdiv').hide().addClass('hidden');
        }


        $('#responsiveNoteEdit').toggle();

        $('#onucchedBody').animate({
            scrollTop: $("#notesShow_new").height() + 70
        }, 'slow');

        var value = $.trim($(".nothipartdiv.btn-success").text());
        $("#poro").text(value);

        var lastnot = 0;
        if (value != '' && value != null && value != 'undefined') {
            $.each($('.nothiOnucched[partno=' + value + ']'), function (i) {
                lastnot = Math.max(lastnot, $(this).attr('notenoen'));
            });
        }


        var lastnoteno = 0;
        if (lastnot > 0) {
            lastnoteno = lastnot;
        }
        var bn = replaceNumbers("'" + parseInt(parseInt(lastnoteno) + 1) + "'");
        bn = bn.replace("'", '');
        bn = bn.replace("'", '');


        var last_onucched = 0;
        $(".printArea span.noteNo").each(function(k, v) {
            var text = $(v).text();
            var onucchedNo = bnToen(text.split('অনুচ্ছেদ: ')[1]);
            if (onucchedNo > last_onucched) {
                last_onucched = onucchedNo;
            }
        });
        last_onucched_note = parseInt(last_onucched.split('.')[1]) + 1;

        var bn = enTobn(last_onucched_note);
        $('#notno').text(bn);

    });

    $(document).on('click', '.saveAndNewNote', function () {
        $(this).attr('disabled', 'disabled');
        $(".saveNote").attr('disabled', 'disabled');
        if ($(this).attr('hasnotorno') == -1 && $('#notesubject').val().length == 0) {
            $(".saveNote").removeAttr('disabled');
            $(".saveAndNewNote").removeAttr('disabled');

            toastr.error('বিষয় দেয়া হয়নি');
            $('#notesubject').focus();
            Metronic.unblockUI('#notesShow_new');
            return false;
        }
        if ($('#noteComposer').froalaEditor('html.get') == "" || $('#noteComposer').froalaEditor('html.get') == "<br>") {
            $(".saveNote").removeAttr('disabled');
            $(".saveAndNewNote").removeAttr('disabled');

            toastr.error('কোনো অনুচ্ছেদ দেয়া হয়নি');
            $('#noteComposer').focus();
            Metronic.unblockUI('#notesShow_new');
            return false;
        }
        removeSpellChecker();
        $('.fr-basic table').addClass('class2');
        var id = $('#noteId').val();
        var noteno = 0;
        var sendsave = 0;
        if($("#underline").is(":checked")){
            var notesubject = '<u>'+$('#notesubject').val()+'</u>';
        } else {
            var notesubject = $('#notesubject').val();
        }
        var notesheetid = $('.notesheetview').attr('id');
        var htmlbody = $('#noteComposer').froalaEditor('html.get');
        var notedecision = $('#bibeccoPotro').find('option:selected').val() + '/' + 1;

        var newHeight = $('.notesheetview').outerHeight();
        $('#temporarydiv').remove();

        // start new code
        height = Metronic.getViewPort().height - $('.page-header').outerHeight(true) - $('.page-footer').outerHeight(true) - $('.page-title').outerHeight(true) - $('.page-bar').outerHeight(true);
        height = height - $('.full-height-content').find('.portlet-title').outerHeight(true) - parseInt($('.full-height-content').find('.portlet-body').css('padding-top')) - parseInt($('.full-height-content').find('.portlet-body').css('padding-bottom')) - 2;
        var new_height = height - 140;
        var total_height = 0;
        $.each($(".note_show_details"), function (key, value) {
            total_height += parseInt($(this).height());
        });
        if (total_height < new_height) {
            new_height = total_height;
        }
        $('#notesShow_new').css('overflow', 'auto');
        //$('#notesShow_new').css('height', new_height);
        // end new code
        var attachments = [];
        var names = [];
        $('.nothiDetailsPage').find('.template-download').each(function () {
            var link_td = $(this).find('td:eq(0)');
            var href = $(link_td).find('a').attr('href');
            var name = $(this).find('input[type=text]').val();
            attachments.push(href);
            names.push(name);
        });
        Metronic.blockUI({target: '#notesShow_new', boxed: true});

        addNewNote(notesheetid, id, htmlbody, noteno, notesubject, attachments, sendsave, notedecision, 1, names);

    });

    $(document).on('click', '.saveNote', function () {
        $(this).attr('disabled', 'disabled');
        $(".saveAndNewNote").attr('disabled', 'disabled');
        Metronic.blockUI({target: '#notesShow_new', boxed: true});

        if ($(this).attr('hasnotorno') == -1 && $('#notesubject').val().length == 0) {
            $(".saveNote").removeAttr('disabled');
            $(".saveAndNewNote").removeAttr('disabled');

            toastr.error('বিষয় দেয়া হয়নি');
            $('#notesubject').focus();
            $('#sendsaveModal').val(0);
            $('#responsiveSendNothi').modal('hide');
            $('.saveNothi').removeAttr('disabled');
            $('.sendSaveNothi').removeAttr('disabled');
            Metronic.unblockUI('#notesShow_new');
            return false;
        }
        if ($('#noteComposer').froalaEditor('html.get') == "" || $('#noteComposer').froalaEditor('html.get') == "<br>") {
            $(".saveNote").removeAttr('disabled');
            $(".saveAndNewNote").removeAttr('disabled');

            toastr.error('কোনো অনুচ্ছেদ দেয়া হয়নি');
            $('#noteComposer').focus();
            $('.saveNothi').removeAttr('disabled');
            $('.sendSaveNothi').removeAttr('disabled');
            $('#sendsaveModal').val(0);
            $('#responsiveSendNothi').modal('hide');
            $('.saveNothi').removeAttr('disabled');
            $('.sendSaveNothi').removeAttr('disabled');
            Metronic.unblockUI('#notesShow_new');
            return false;
        }

        removeSpellChecker();
        $('.fr-basic table').addClass('class2');

        var id = $('#noteId').val();
        var sendsave = $('#sendsaveModal').val();

        var noteno = 0;
        if($("#underline").is(":checked")){
            var notesubject = '<u>'+$('#notesubject').val()+'</u>';
        } else {
            var notesubject = $('#notesubject').val();
        }
        var notesheetid = $('.notesheetview').attr('id');
        var htmlbody = $('#noteComposer').froalaEditor('html.get');
        var notedecision = $('#bibeccoPotro').find('option:selected').val() + '/' + 1;

        var newHeight = $('.notesheetview').outerHeight();
        $('#temporarydiv').remove();

        // start new code
        height = Metronic.getViewPort().height - $('.page-header').outerHeight(true) - $('.page-footer').outerHeight(true) - $('.page-title').outerHeight(true) - $('.page-bar').outerHeight(true);
        height = height - $('.full-height-content').find('.portlet-title').outerHeight(true) - parseInt($('.full-height-content').find('.portlet-body').css('padding-top')) - parseInt($('.full-height-content').find('.portlet-body').css('padding-bottom')) - 2;
        var new_height = height - 140;
        var total_height = 0;
        $.each($(".note_show_details"), function (key, value) {
            total_height += parseInt($(this).height());
        });
        if (total_height < new_height) {
            new_height = total_height;
        }
        $('#notesShow_new').css('overflow', 'auto');
        //$('#notesShow_new').css('height', new_height);
        // end new code
        var attachments = [];
        var names = [];
        $('.nothiDetailsPage').find('.template-download').each(function () {
            var link_td = $(this).find('td:eq(0)');
            var href = $(link_td).find('a').attr('href');
            var name = $(this).find('input[type=text]').val();

            if (typeof (name) == 'undefined' || name == '') {
                name = $(this).find('.name a').text();
            }

            attachments.push(href);
            names.push(name);
        });
        if (id == 0) {
            if (newHeight > 842 || (typeof notesheetid == 'undefined' || notesheetid == 0)) {
                addNewNoteSheet(true, id, htmlbody, noteno, notesubject, attachments, notedecision, names, sendsave);
            } else {
                addNewNote(notesheetid, id, htmlbody, noteno, notesubject, attachments, sendsave, notedecision, 0, names);
            }
        } else {
            addNewNote(notesheetid, id, htmlbody, noteno, notesubject, attachments, sendsave, notedecision, 0, names);
        }
    });

    <?php endif; ?>
    function getAutolist() {
        var bibecchoPotroList = '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" attachment_id="--" data-cmd="bibeccoPotro" data-param1="0" title="" aria-selected="false">--</a></li>';

        var bibecchePotroJson = <?= !empty($potroAttachmentRecord)?json_encode($potroAttachmentRecord):'{}'; ?>;
        $.each(bibecchePotroJson, function (i, v) {
            bibecchoPotroList += '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="bibeccoPotro" data-param1="' + v.id + '" title="'+ v.nothi_potro_page_bn +' - '+escapeHtml(v.subject)+'" nothi_potro_page_bn= "'+ v.nothi_potro_page_bn +'" aria-selected="false">'+ v.nothi_potro_page_bn +' - '+escapeHtml(v.subject)+'</a></li>';
        });

        $('[id^=dropdown-menu-bibeccoPotro] ul').html(bibecchoPotroList);

        var potakaList = '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" attachment_id="--" data-cmd="bibeccoPotaka" data-param1="--" title="" aria-selected="false">--</a></li>';

        $('[id^=dropdown-menu-bibeccoPotaka] ul').html(potakaList);

        $.ajax({
            url: js_wb_root + 'nothiMasters/potakaList/<?php echo $id; ?>/<?php echo $nothi_office ?>',
            dataType: 'JSON',
            success: function (response) {

                $.each(response, function (i, v) {

                    potakaList += '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="bibeccoPotaka" data-param1="' + v.id + '" nothi_master_id=' + v.nothi_master_id + ' attachment_id=' + v.attachment_id + ' nothi_part_no=' + v.nothi_part_no + '  page=' + v.page_no + ' title="" aria-selected="false">' + (v.title != '' ? v.title : v.potro_no) + '</a></li>';
                });

                $('[id^=dropdown-menu-bibeccoPotaka] ul').html(potakaList);
            }
        });


        var onuccedList = '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="bibeccoOnucced" data-param1="--" title="" aria-selected="false">--</a></li>';

        $('[id^=dropdown-menu-bibeccoOnucced] ul').html(onuccedList);

        $.ajax({
            url: js_wb_root + 'nothiMasters/onuccedList/<?php echo $id; ?>/<?php echo $nothi_office ?>',
            dataType: 'JSON',
            success: function (response) {

                var allstar = '';
                $.each(response, function (i, v) {
                    if (allstar != v.nothi_part_no) {

                        onuccedList += '<li role="presentation"><a class="fr-command fullNote" tabindex="-1" role="option" data-cmd="bibeccoOnucced" data-param1="' + v.nothi_part_no_bn + " *" + '" nothi_part_no=' + v.nothi_part_no + ' notesheet=' + v.nothi_notesheet_id + ' title="' + v.nothi_part_no_bn + '" aria-selected="false">' + v.nothi_part_no_bn + " *" + '</a></li>';
                        allstar = v.nothi_part_no;
                    }

                    onuccedList += '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="bibeccoOnucced" data-param1="' + v.id + '" nothi_part_no="' + v.nothi_part_no + '" notesheet="' + v.nothi_notesheet_id + '" title="' + v.note_no + '" aria-selected="false">' + v.note_no + '</a></li>';
                });

                $('[id^=dropdown-menu-bibeccoOnucced] ul').html(onuccedList);
            }
        });
    }

    $(function () {
        $(window).load(function () {
            // initiatefroala();
            // setTimeout(function () {
            //     $('#noteComposer').froalaEditor('events.focus', true);
            //     getCaretPosition();
            // },300);
            Metronic.init();
            NothiMasterMovement.init('nothing');
        });

        $('li').tooltip({'placement':'bottom'});
        //$('#notesShow').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        $('#potroShow').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        $('#noteShowLoading').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        var noteSheetPageUrl = '<?php echo $this->request->webroot; ?>NothiNoteSheets/notesheetPage/<?php echo $id ?>/<?php echo $nothi_office ?>/'+noteOrderingType+permitted_by;
        noteShow(noteSheetPageUrl, 0);

        $('#notesShow_new').scroll(function () {
            if ($("#noteShowLoading").css('display') == 'none') {
                if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                    var limitStart = $("#notesShow_new > div").length;
                    noteShow(noteSheetPageUrl, limitStart);
                }
            }
            heightlimit();
        });

        if ($("#noteShowLoading").length != 0) {
            if ($("#notesShow").height() > $("#notesShow_new").height()) {
                var limitStart = $("#notesShow_new > div").length;
                noteShow(noteSheetPageUrl, parseInt(limitStart) + 1);
            }
            heightlimit();
        }

        potroPageShow('<?php echo $this->request->webroot; ?>nothiMasters/potroPage/<?php echo $id ?>/<?php echo $nothi_office ?>'+permitted_by);
        $('[title]').tooltip({'placement':'bottom', 'container': 'body'});
        $('li').tooltip({'placement':'bottom'});
        $(document).on("click", '.saveBookmark', function () {
            var htmlbody = $('#bookmarkDes').val();
            var page = $('#bookmarkPage').val();
            var potro = $('#bookmark_potro').val();
            sendPotroAjaxRequest(htmlbody, potro, page);
            $('.bookmarkDes').val('');
        });

        $(document).on("click", '.bookmarkImgPotro', function () {
            $('#responsiveBookmarkDescription').modal('show');
            var id_ar_val = $('.attachmentsRecord.active').attr('id_ar');
            var content_type = $('.attachmentsRecord.active').filter('[id_ar = ' + id_ar_val + ']').data('content-type');
            $('#bookmark_potro').val(id_ar_val);
            if (content_type == 'application/pdf') {
                $(".pdfPageOption").css("display", "inline");
            } else {
                $(".pdfPageOption").css("display", "none");
            }

        });
        $(document).on("click", '.bookmarkallImgPotro', function () {
            $('#responsiveBookmarkDescription').modal('show');
            var id_ar_val = $('.attachmentsallRecord.active').attr('id_ar');
            var content_type = $('.attachmentsallRecord.active').filter('[id_ar = ' + id_ar_val + ']').data('content-type');
            $('#bookmark_potro').val(id_ar_val);
            if (content_type == 'application/pdf') {
                $(".pdfPageOption").css("display", "inline");
            } else {
                $(".pdfPageOption").css("display", "none");
            }
        });

        $('#colorBookmark').addClass($('#colorBookmark').find("option:selected").attr('class'));

        $(document).on('change', '#colorBookmark', function () {
            $(this).removeAttr('class');
            $(this).addClass($(this).find("option:selected").attr('class'));
        });

        $(document).on('change', '#colorBookmark', function () {
            $(this).removeAttr('class');
            $(this).addClass($(this).find("option:selected").attr('class'));
        });

        $(document).on('click', '.btn-minmax', function () {
            $('.collapsable').show();
            $('.collapsable').parent('div').removeClass('col-md-12').addClass('col-md-6');
            $('.btn-minmax').addClass('btn-maximize').removeClass('btn-minimized');
        });

        $(document).on('click', '.btn-minimized', function () {
            $(this).find('i').toggleClass('fa-chevron-right').toggleClass('fa-chevron-left');
            heightlimit();
        });

        $(document).on('click', '.btn-maximize', function () {
            var ind = $('.btn-maximize').index(this);

            if (ind == 1) {
                $('html, body').animate({
                    scrollTop: $('.collapsable').eq(ind).offset().top + 100
                }, 1000);
                ind = 0;
            } else {
                ind = 1;
            }

//            $('.collapsable').eq(ind).hide();
            $('.collapsable').parent('div').removeClass('col-md-6').addClass('col-md-12');
            $('.btn-minmax').removeClass('btn-maximize').addClass('btn-minimized');
            $(this).find('i').toggleClass('fa-chevron-right').toggleClass('fa-chevron-left');

        });
    });
    <?php if ($otherNothi == false): ?>
    $(document).on('click', '.btn-sar-nothi-draft', function (e) {
        e.preventDefault();
        var client = new ClientJS();
        if (!client.isChrome()) {
            toastr.info('আপনি গুগল ক্রোম ব্যতিরেকে অন্য ব্রাউজার ব্যবহার করছেন, দয়া করে গুগল ক্রোম ব্যবহার করুন, অন্যথায় ফাঁকা পত্র তৈরি হতে পারে। Google Chrome ইন্সটল না থাকলে <a target="_blank" href="https://www.google.com/chrome/"> এখানে ক্লিক করুন </a>।');

        } else {
            showKhosoraPage();
        }

        function showKhosoraPage() {
            $('#onucchedBody').hide();
            $('#khosrapotrobody').show();

            $('#khosrapotrobody').closest('.portlet').find('.caption').text("খসড়া সার-সংক্ষেপ");
            $('#khosrapotrobody').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

            $.ajax({
                url: '<?php
                    echo $this->Url->build(['controller' => 'SummaryNothi',
                        'action' => 'CreateSarNothiDraft', $id])
                    ?>',
                method: 'post',
                success: function (html) {

                    $('#khosrapotrobody').closest('.portlet').find('.caption').text("খসড়া সার-সংক্ষেপ");
                    $('#khosrapotrobody').closest('.portlet').find('.actions').prepend('<a class="btn btn-default btn-xs potroClose" title="খসড়া সার-সংক্ষেপ বাতিল করুন" data-toggle="tooltip"><i class="fs1 a2i_gn_close2"></i></a>');
                    $('#khosrapotrobody').html(html);
                }
            });
        }
    });

    $(document).on('click', '.potroCreate', function () {
        if(!checkBrowserIsChrome()){
            return;
        }
        var thisElm = $(this);
        editBoxClose();
        //$('#khosrapotrobody').parent().find('.btn-minmax').trigger('click');
        var offices2ShowTemplateModals = <?=  json_encode($offices2ShowTemplateModals)?>;
        decideToShowTemplateModals(thisElm, offices2ShowTemplateModals);
    });
    function checkBrowserIsChrome(){
        var client = new ClientJS();
        if (!client.isChrome()) {
            toastr.info('<a target="_blank" href="https://www.google.com/chrome/">Google Chrome ব্যতিত অন্য কোন ব্রাউজার দিয়ে পত্র খসড়া/সংশোধন করা যাবে না। দয়া করে Google Chrome ব্যবহার করুন। Google Chrome ইন্সটল না থাকলে এখানে ক্লিক করুন </a>');
            return false;
        }
        return true;
    }
    function decideToShowTemplateModals(thisElm, offices2ShowTemplateModals) {
        if ($.inArray(<?=$nothi_office?>, offices2ShowTemplateModals) != -1) {
            var oldPrompt = bootbox.prompt;
            bootbox.prompt = function(options) {
                if(options.reorder) {
                    options = $.extend({}, options, {show: false});    // don't show the modal
                    var $dialog = oldPrompt(options),
                        $cancel = $dialog.find('[data-bb-handler="cancel"]');
                    $cancel.parent().append($cancel.detach());        // swap the buttons
                    $dialog.modal('show');
                } else {
                    oldPrompt(options);
                }
            };
            bootbox.prompt($.extend({}, {
                title: "পত্রের ধরন বাছাই করুন",
                inputType: 'select',
                inputOptions:  <?= json_encode($templete_options) ?>,
                buttons: {
                    cancel: {
                        label: 'বন্ধ করুন',
                        className: 'red margin-right-10'
                    },
                    confirm: {
                        label: 'বাছাই করুন',
                        className: 'green margin-right-10'
                    }
                },
                callback: function (reslt) {
                    if (isEmpty(reslt)) {

                    } else if (reslt == 21) {
                        var url = thisElm.attr('url');
                        url = url.replace("potrojari/makeDraft", "makeRm");
                        window.location.href = url;
                    }else if (reslt == 27) {
                        var url = thisElm.attr('url');
                        url = url.replace("potrojari/makeDraft", "make-formal-letter");
                        window.location.href = url;
                    } else {
                        showKhosoraPage(thisElm, reslt);
                    }
                }
            }, {reorder: true}));
            //bootbox.prompt({
            //    title: "পত্রের ধরন বাছাই করুন",
            //    inputType: 'select',
            //    inputOptions:  <?//= json_encode($templete_options) ?>//,
            //    buttons: {
            //        confirm: {
            //            label: 'বাছাই করুন',
            //            className: 'green margin-right-10'
            //        },
            //        cancel: {
            //            label: 'বন্ধ করুন',
            //            className: 'red margin-right-10'
            //        },
            //    },
            //    callback: function (reslt) {
            //        if (isEmpty(reslt)) {
			//
            //        } else if (reslt == 21) {
            //            var url = thisElm.attr('url');
            //            url = url.replace("potrojari/makeDraft", "makeRm");
            //            window.location.href = url;
            //        }else if (reslt == 27) {
            //            var url = thisElm.attr('url');
            //            url = url.replace("potrojari/makeDraft", "make-formal-letter");
            //            window.location.href = url;
            //        } else {
            //            showKhosoraPage(thisElm, reslt);
            //        }
            //    }
            //}, {reorder: true});
        } else {
            showKhosoraPage(thisElm);
        }
    }

    function showKhosoraPage(thisElm, reslt) {
        var client = new ClientJS();
        if (!client.isChrome()) {
            toastr.info('আপনি গুগল ক্রোম ব্যতিরেকে অন্য ব্রাউজার ব্যবহার করছেন, দয়া করে গুগল ক্রোম ব্যবহার করুন, অন্যথায় ফাঁকা পত্র তৈরি হতে পারে। Google Chrome ইন্সটল না থাকলে <a target="_blank" href="https://www.google.com/chrome/"> এখানে ক্লিক করুন </a>।');
        }
        $('#onucchedBody').hide();
        $('#khosrapotrobody').show();
        thisElm.addClass('potroOpened').addClass('potroClose').removeClass('potroCreate').removeClass('green').addClass('red');
        $(".potro_language").remove();
        $('#khosrapotrobody').closest('.portlet').find('.caption').text("পত্রজারি খসড়া");
        $('#khosrapotrobody').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        $.ajax({
            url: thisElm.attr('url'),
            method: 'post',
            success: function (html) {
                $(".potro_language").remove();
                $('#khosrapotrobody').closest('.portlet').find('.caption').text("পত্রজারি খসড়া");
                $('#khosrapotrobody').closest('.portlet').find('.actions').prepend('<a class="btn btn-default btn-xs potroClose" title="খসড়া পত্র বাতিল করুন" data-toggle="tooltip"><i class="fs1 a2i_gn_close2"></i></a>');
                var language_data = '<span class="potro_language"><label class="control-label"> পত্রজারি ভাষাঃ &nbsp;</label><input type="checkbox" id="potrojari_language" checked="checked" class="make-switch" data-on-text = "বাংলা"  data-off-text = "English" data-size="normal" data-off-color="danger"></span>&nbsp;&nbsp;';
                $('#khosrapotrobody').closest('.portlet').find('.actions').prepend(language_data);
                $('#khosrapotrobody').closest('.collapsable').find('.btn-maximize').click();

                $('#khosrapotrobody').html(html);
                $(document).ready(function () {
                    if (!isEmpty(reslt)) {
                        setTimeout(function () {
                            $('#potro-type').val(reslt).trigger('change');
                            $('#potro-type option[value=21]').remove();
                            $('#potro-type option[value=27]').remove();
                        }, 2000);
                    }
                });
            }
        })
    }

    $(document).on('click', '.btn-endorsment', function () {
        var url = $(this).attr('url');
        var client = new ClientJS();
        if (!client.isChrome()) {
            toastr.info('আপনি গুগল ক্রোম ব্যতিরেকে অন্য ব্রাউজার ব্যবহার করছেন, দয়া করে গুগল ক্রোম ব্যবহার করুন, অন্যথায় ফাঁকা পত্র তৈরি হতে পারে।Google Chrome ইন্সটল না থাকলে <a target="_blank" href="https://www.google.com/chrome/"> এখানে ক্লিক করুন </a>।');
        } else {
            // ERROR CODE
            // 001 => Convert Problem
            var imageUrl = $(this).parent().parent().parent().find('embed').attr('src');
            if(typeof imageUrl != 'undefined' && imageUrl.length > 0) {
				var file_name = imageUrl.split('file=')[1].split('&token')[0];
				$('#onucchedBody').hide();
				$('#khosrapotrobody').show();
				$(".potroClose").remove();
				$(".potro_language").remove();
				$('#khosrapotrobody').closest('.portlet').find('.caption').text("পত্রজারি খসড়া");
				$('#khosrapotrobody').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
				$.ajax({
					url: js_wb_root + 'potrojari/convertImageFromPDF',
					data: {'file_name': file_name},
					dataType: 'json',
					type: 'POST',
					success: function (res) {
						if (res.status == 'success') {
							showKhosoraPage(url);
						} else {
							toastr.error('পিডিএফ পৃষ্ঠাঙ্কন এই মূহুর্তে সম্ভব হচ্ছে না। দয়া করে পুনরায় চেষ্টা করুন। ERR#001');
							$('#onucchedBody').show();
							$('#khosrapotrobody').hide();
						}
					}
				});
			}else {
				showKhosoraPage(url);
			}
        }

        function showKhosoraPage(url) {
            $('#onucchedBody').hide();
            $('#khosrapotrobody').show();
            $(".potroClose").remove();
            $(".potro_language").remove();
            $('#khosrapotrobody').closest('.portlet').find('.caption').text("পত্রজারি খসড়া");
            $('#khosrapotrobody').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

            $.ajax({
                url: url,
                method: 'post',
                success: function (html) {
                    $(".potroClose").remove();
                    $(".potro_language").remove();
                    $('#khosrapotrobody').closest('.portlet').find('.caption').text("পত্রজারি খসড়া");
                    $('#khosrapotrobody').closest('.portlet').find('.actions').prepend('<a class="btn btn-default btn-xs potroClose" title="খসড়া পত্র বাতিল করুন" data-toggle="tooltip"><i class="fs1 a2i_gn_close2"></i></a>');
                    var language_data = '<span class="potro_language"><label class="control-label"> পত্রজারি ভাষাঃ &nbsp;</label><input type="checkbox" readonly id="potrojari_language" checked="checked" class="make-switch" data-on-text = "বাংলা"  data-off-text = "English" data-size="normal" data-off-color="danger"></span>&nbsp;&nbsp;';
                    $('#khosrapotrobody').closest('.portlet').find('.actions').prepend(language_data);

                    $('#khosrapotrobody').html(html);
                    setTimeout(function () {
                        $('#potro-type').trigger('change')
                    }, 1000);
                }
            })
        }
    });

    $(document).on('click', '.potroClose', function () {
        bootbox.dialog({
            message: "আপনি কি খসড়া বাতিল করতে চান?",
            title: "খসড়া বাতিল করুন",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        isRestore = 0;
                        $('.btn-forward-nothi').show();
                        $('.potroOpened').addClass('potroCreate').removeClass('potroClose').removeClass('potroOpened').removeClass('red').addClass('green');
                        $('#onucchedBody').closest('.portlet').find('.caption').text("নোটাংশ");
                        $('#onucchedBody').closest('.portlet').find('a.potroClose').remove();
                        $('#onucchedBody').closest('.portlet').find('span.potro_language').remove();
                        $('#khosrapotrobody').html('');
                        $('#khosrapotrobody').hide();
                        $('#onucchedBody').show();
                        $('.note-codable').next('.note-editable').remove();
                        $('.note-codable').after('<div class="note-editable" contenteditable="true" style="height: 200px;"></div>');
                        $('.potro_language').hide();
                        heightlimit();

                        $("#potroShow").css('height', $("#potroShow").css('min-height'));
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                        $('#selectpage').val('');
                    }
                }
            }
        });
    });
    <?php endif; ?>
    $(document).on('click', '.btn-onucced-delete', function () {
        var noteid = $(this).attr('noteid');
        var removeDivId = $(this).parent().parent().parent().attr('id');
        bootbox.dialog({
            message: "আপনি কি অনুচ্ছেদটি বাতিল করতে ইচ্ছুক?",
            title: "অনুচ্ছেদ বাতিল",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        deleteNote(noteid, $(this), removeDivId);
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {

                    }
                }
            }
        });

    });

    <?php if ($privilige_type >= 1): ?>

    function deleteNote(id, el, removeDivId) {
        if (typeof (id) != 'undefined' || id != 0) {
            $.ajax({
                data: {nothimasterid: <?php echo $id; ?>, noteid: id, 'nothi_office':<?php echo $nothi_office ?>},
                method: 'POST',
                dataType: 'JSON',
                url: '<?php
                    echo $this->Url->build(['controller' => 'NothiNoteSheets',
                        'action' => 'deleteNote']);
                    ?>',
                success: function (msg) {

                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-bottom-right"
                    };

                    if (msg.status == 'success') {
                        $("#" + removeDivId).remove()
                        toastr.success(msg.msg);
                        if ($("#notesShow_new > div").length == 0) {
                            location.reload();
                        } else {
                            noteShow('<?php echo $this->request->webroot; ?>NothiNoteSheets/notesheetPage/<?php echo $id ?>/<?php echo $nothi_office ?>/'+noteOrderingType, 0);
                        }
                    } else {
                        toastr.error(msg.msg);
                    }
                }
            });
        } else {
            toastr.error(' দুঃখিত কিছুক্ষণ পর আবার চেষ্টা করুন। ');
        }
    }

    function addNewNoteSheet(newNotesheet, id, htmlbody, noteno, notesubject, attachments, notedecision, names, sendsave) {

        $.ajax({
            data: {nothimasterid: <?php echo $id; ?>, 'nothi_office':<?php echo $nothi_office ?>},
            method: 'POST',
            url: '<?php
                echo $this->Url->build(['controller' => 'NothiNoteSheets',
                    'action' => 'addNoteSheet']);
                ?>',
            success: function (msg) {
                if (msg > 0) {
                    if (newNotesheet) {
                        addNewNote(msg, id, htmlbody, noteno, notesubject, attachments, sendsave, notedecision, 0, names);
                    } else {
                        noteShow('<?php echo $this->request->webroot; ?>NothiNoteSheets/notesheetPage/<?php echo $id ?>/<?php echo $nothi_office ?>/'+noteOrderingType+permitted_by);
                    }
                } else {

                }

                //Metronic.initSlimScroll('.scroller');
            }
        });
    }

    function addNewNote(notesheetid, id, htmlbody, noteno, notesubject, attachments, sendsave, notedecision, rep, names) {

        if (typeof (rep) == 'undefined') {
            rep = 0;
        }

        $.ajax({
            method: 'POST',
            async: false,
            url: '<?php
                echo $this->Url->build(['controller' => 'NothiNoteSheets',
                    'action' => 'saveNote']);
                ?>',
            data: {
                nothimasterid: <?php echo $id; ?>,
                notesheetid: notesheetid,
                id: id,
                body: htmlbody,
                noteno: noteno,
                notesubject: notesubject,
                attachments: attachments,
                notedecision: notedecision,
                user_file_name: names,
                nothi_office: <?php echo $nothi_office ?>
            },
            success: function (msg) {

                if ( !isEmpty(msg) && msg > 0) {

                    var db = ProjapotiOffline.createDatabase('offline_note_draft');
                    var docid = '<?php echo $employee_office['office_id'] . '_' . $employee_office['officer_id'] . '_' . $employee_office['office_unit_organogram_id'] . '_' . $id ?>';

                    db.get(docid).then(function (doc) {

                        return db.remove(doc).then(function () {
                            db.compact();
                        }).catch(function (err) {
                            // console.log(1,err);
                        });

                    }).then(function (response) {

                        if (rep == 1) {

                            return db.put({
                                _id: docid,
                                _rev: response.rev,
                                noteid: 0,
                                subject: '',
                                body: ''
                            });
                        }
                    }).catch(function (err) {
                    });

                    editBoxClose();
                    if ($('#sendsaveModal').val() === "1") {
                        toastr.info('প্রেরণ প্রক্রিয়া চলমান');
                        setTimeout(function(){
                            NothiMasterMovement.sendNothi();
                            Metronic.unblockUI('#notesShow_new');
                        },3000);
                    } else if(!isEmpty($(".saveNote").attr('stop-reload')) && parseInt($(".saveNote").attr('stop-reload'))==1){
                        console.log($(".saveNote").attr('stop-reload'));
                        if(!isEmpty($(".saveNote").attr('reload-url')))
                        {
                            console.log($(".saveNote").attr('reload-url'));
                           window.location.href = $(".saveNote").attr('reload-url');
                        }
                    }else if (notesubject == '') {
                        var noteSheetPageUrl = '<?php echo $this->request->webroot; ?>NothiNoteSheets/notesheetPage/<?php echo $id ?>/<?php echo $nothi_office ?>/'+noteOrderingType;
                        $('#noteComposer').froalaEditor('html.set', '');
                        noteShow(noteSheetPageUrl, 0);
                    }
                    else {
                        window.location.reload();
                    }
                    $('#sendsaveModal').val(0);
                    $('.saveNothi').removeAttr('disabled');
                    $('.sendSaveNothi').removeAttr('disabled');

                }else{
                    toastr.error('দুঃখিত! কার্যক্রম সম্ভব হয়নি। পুনরায় চেষ্টা করুন।');
                }
                //Metronic.initSlimScroll('.scroller');
            }
        });
    }
    <?php endif; ?>

    function getUrlVars(href) {
        var vars = [], hash;
        var hashes = href.slice(href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }


    function sendPotroAjaxRequest(description, potro, page) {
        var imageNo = $('.imageNo').val();
        $.ajax({
            data: 'color=' + $('#colorBookmark').find("option:selected").val() + '&potro_no=' + potro + "&masternoteno=" + <?php echo $id ?> +"&title=" + description + '&nothi_office=' +<?php echo $nothi_office ?> +'&page=' + page,
            method: 'POST',
            url: '<?php
                echo $this->Url->build(['controller' => 'NothiNoteSheets',
                    'action' => 'savePotroFlag']);
                ?>',
            success: function (msg) {
                if (msg == 1) {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-bottom-right"
                    };
                    toastr.success("পতাকা করা হয়েছে");
                    window.location.reload(false);
                   // potroPageShow('<?php echo $this->request->webroot; ?>nothiMasters/potroPage/<?php echo $id ?>/<?php echo $nothi_office ?>', imageNo);
                }
                else if (msg == 2) {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-bottom-right"
                    };
                    toastr.error("দুঃখিত! এই নামে পতাকা আছে এই নথিতে এ।");
                }
                else {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-bottom-right"
                    };
                    toastr.error("পতাকা সংশোধিত হয়েছে।");
                    window.location.reload(false);
                  //  potroPageShow('<?php echo $this->request->webroot; ?>nothiMasters/potroPage/<?php echo $id ?>/<?php echo $nothi_office ?>', imageNo);
                }
                // getAutolist();
                // $('[title]').tooltip({placement: 'bottom'});
                // $('li').tooltip({placement: 'bottom'});
            }
        });
    }


    function noteShow(href, limitStart) {
        if (href == '' || typeof (href) == 'undefined') {
            return;
        }
        $('#noteShowLoading').show();
        $('#noteShowLoading').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        //$('#notesShow_new').html();

        //alert('before request');
        PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(href, {'limitStart': limitStart}, 'html', function (response) {
            if (isEmpty(response)) {
                $('#noteShowLoading').remove();
            } else {
                $('#noteShowLoading').hide();
            }
            if (limitStart == 0) {
                //
            }
            $('#noteShowLoading').hide();
            if (limitStart == '' || typeof (limitStart) == 'undefined') {
                $('#notesShow_new').html(response);
            } else {
                $('#notesShow_new').append(response);
            }

            //Metronic.initSlimScroll('.scroller');
            //$('[data-title-orginal]').tooltip({placement: 'bottom'});
            //$('[title]').tooltip({placement: 'bottom'});
            //$('li').tooltip({placement: 'bottom'});

            $.each($('.signatures'), function (i, v) {
                var imgurl = $(v).data('id');
                var token = $(v).data('token_id');
                var signDate = $(v).data('signdate');
                PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'getSignature/' + imgurl + '/1/' + signDate+'?token='+token, {}, 'html', function (signature) {
                    $(v).attr('src', signature);
                });
            });
        });
        $(document).ajaxStop(function () {
            checkEmptyOnucched();

            height = Metronic.getViewPort().height - $('.page-header').outerHeight(true) - $('.page-footer').outerHeight(true) - $('.page-title').outerHeight(true) - $('.page-bar').outerHeight(true);
            height = height - $('.full-height-content').find('.portlet-title').outerHeight(true) - parseInt($('.full-height-content').find('.portlet-body').css('padding-top')) - parseInt($('.full-height-content').find('.portlet-body').css('padding-bottom')) - 2;
            var new_height = height - 140;
            var total_height = 0;
            $.each($(".note_show_details"), function (key, value) {
                total_height += parseInt($(this).height());
            });
            if (total_height < new_height) {
                if ($("#noteShowLoading").length != 0) {
                    noteShow(href, limitStart+1);
                }
            }
            
        });
        heightlimit();

        $('.nothiDetailsPage').height($('.nothiDetailsPage>div').innerHeight() - 9);
    }

    function checkEmptyOnucched() {
        if ($('.note_show_details').length == 0) {
            $('.notesubjectdiv').show().removeClass('hidden');
            $('#notesShow_new').css('height', 'auto');
            $('#responsiveNoteEdit').show();
            $('.addNewNote').hide();
            $('.btn-closeNote').hide();
        }
    }


    function gotoNotesheet(newpage, noteNo, notenoen) {
        if (typeof (newpage) == 'undefined' || newpage == '')
            return;

        $('#notesShow_new').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        $.ajax({
            url: '<?php echo $this->request->webroot; ?>nothi_note_sheets/notesheetPage/<?php echo $id; ?>?page=' + newpage + '&noteno=' + (typeof (noteNo) != 'undefined' ? noteNo : 0) + '&nothi_office=<?php echo $nothi_office ?>'+permitted_by,
            success: function (response) {

                $('#notesShow_new').html(response);

                if (typeof (noteNo) != 'undefined') {
                    if (noteNo != '') {
                        var dest = 0;
                        if ($('#' + noteNo).offset().top > $(document).height() - $(window).height()) {
                            dest = $(document).height() - $(window).height() - 80;
                        } else {
                            dest = $('#' + noteNo).offset().top - 80;
                        }
                        //go to destination
                        $('html,body').animate({
                            scrollTop: dest
                        }, 2000, 'swing');
                    }

                }

                //$('[data-title-orginal]').tooltip({placement: 'bottom'});
                //$('[title]').tooltip({placement: 'bottom'});
                //$('li').tooltip({placement: 'bottom'});

                //Metronic.initSlimScroll('.scroller');
            }
        });
    }


    $(document).on('click', '#noteLeftMenuToggle', function () {
        $('.noteLeftMenu').toggle().toggleClass('col-lg-2 col-md-2 col-sm-2 col-xs-2');

        if ($('.noteLeftMenu').css('display') == 'none') {
            $('.noteContentSide').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12')
            $('.noteContentSide').removeClass('col-lg-10 col-md-10 col-sm-10 col-xs-10')

        } else {
            $('.noteContentSide').addClass('col-lg-10 col-md-10 col-sm-10 col-xs-10')
            $('.noteContentSide').removeClass('col-lg-12 col-md-12 col-sm-12 col-xs-12')

        }
    })

    $(document).on('click', '#potroLeftMenuToggle', function () {
        $('.potroLeftMenu').toggle().toggleClass('col-lg-2 col-md-2 col-sm-2 col-xs-2');

        if ($('.potroLeftMenu').css('display') == 'none') {
            $('.potroContentSide').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12')
            $('.potroContentSide').removeClass('col-lg-10 col-md-10 col-sm-10 col-xs-10')

        } else {
            $('.potroContentSide').addClass('col-lg-10 col-md-10 col-sm-10 col-xs-10')
            $('.potroContentSide').removeClass('col-lg-12 col-md-12 col-sm-12 col-xs-12')

        }
    });


    function potroPageShow(href, pageno) {
        $('#potroShow').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(href, {}, 'html', function (response) {
            $('#potroShow').html(response);

            //Metronic.initSlimScroll('.scroller');
            if (typeof (pageno) != 'undefined') {
                goToPage(pageno);
                getpotropage(pageno);
            }
            //$('[data-title-orginal]').tooltip({placement: 'bottom'});
            //$('[title]').tooltip({placement: 'bottom'});
            //$('li').tooltip({placement: 'bottom'});
            heightlimit();
            //initiatefroala();
        });
    }


    function getpotropage(potroPage) {
        $('.potroNo').val(potroPage);
    }

    function refPotro(potrono) {
        if (typeof (potrono) != 'undefined') {
            goToPage(potrono);
            getpotropage(potrono);
        }
    }

    function getPopUpPotro(href, title) {
        $('#responsiveOnuccedModal').find('.modal-title').text('');
        $('#responsiveOnuccedModal').find('.modal-body').html('');
        PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + href+'?nothi_part='+'<?= $id ?>'+'&token='+'<?= sGenerateToken(['file' => $id], ['exp' => time() + 60 * 300]) ?>', {'nothi_office':<?php echo $nothi_office ?>}, 'html', function (response) {
            $('#responsiveOnuccedModal').modal('show');
            $('#responsiveOnuccedModal').find('.modal-title').text(title);

            $('#responsiveOnuccedModal').find('.modal-body').html(response);
        });

    }

    $(document).off('click', '.showforPopup').on('click', '.showforPopup', function (e) {
        e.preventDefault();
        var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
        getPopUpPotro($(this).attr('href'), title);
    })


    $(document).on('click', '.deleteAttachment', function () {
        var id = $(this).data('attachment-id');
        var note_id = $(this).data('note-id');
        var note_sheet_id = $(this).data('note-sheet-id');
        $(this).attr('disabled', 'disabled');
        $.ajax({
            url: js_wb_root + 'nothiNoteSheets/deleteAttachment',
            data: {
                id: id,
                note_id: note_id,
                nothimasterid:<?php echo $id; ?>,
                notesheetid: note_sheet_id,
                nothi_office:<?php echo $nothi_office ?>},
            method: "POST",
            dataType: 'JSON',
            success: function (response) {

                if (response.status == 'error') {
                    $(this).removeAttr('disabled');
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-bottom-right"
                    };
                    toastr.error(response.msg);
                } else {
                    $(this).closest('tr').remove();
                    toastr.success(response.msg);
                    gotoNotesheet(note_sheet_id, note_id);
                }
            },
            error: function (status, response) {
                $(this).removeAttr('disabled');
            }
        })
    });


    function PrintElem(el, title) {
        Popup(el, title);
    }

    $(document).on('click', '.btn-notesheet-print', function () {
        $(document).find('.notesheetview').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: false,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    });

    function Popup(data, title) {
        var mywindow = window.open('', title, 'height=842,width=590');
        mywindow.document.write('<html><head><title>' + title + '</title>');

        mywindow.document.write('<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>projapoti-nothi/css/styles.css"/> <link href="<?php echo CDN_PATH; ?>assets/admin/layout4/css/fonts.css" rel="stylesheet"  type="text/css"/> <link href="<?php echo CDN_PATH; ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/> <link href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/> <link href="<?php echo CDN_PATH; ?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/><link href="<?php echo CDN_PATH; ?>assets/global/css/components.css" id="style_components"  rel="stylesheet" type="text/css"/><link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/> <link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/> <link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-tags-input/jquery.tagsinput.css"/> <link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css"> <link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/typeahead/typeahead.css">  <link href="<?php echo CDN_PATH; ?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/><link href="<?php echo CDN_PATH; ?>assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/> <link href="<?php echo CDN_PATH; ?>assets/admin/layout4/css/themes/darkblue.css" rel="stylesheet" ype="text/css"/><link href="<?php echo CDN_PATH; ?>assets/admin/layout4/css/lojjaboti/lojjaboti.css" rel="stylesheet" type="text/css"/><style> *, body{font-size:10pt!important;} .printArea{background-color:#fff!important;} .fa,.download-attachment{display:none;}</style>');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }

    $(document).ready(function ($) {

//        $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
//            event.preventDefault();
//            return $(this).ekkoLightbox({
//                onShown: function () {
//
//                },
//                onNavigate: function (direction, itemIndex) {
//
//                }
//            });
//        });
    });

</script>


<script>
    <?php if ($otherNothi == false): ?>
    $('.newPartCreate').on('click', function () {

        //
        bootbox.dialog({
            message: "আপনি কি নতুন নোট তৈরি করতে চান?",
            title: "নতুন নোট",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        createNoteNew();
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                        Metronic.unblockUI('.page-container');
                    }
                }
            }
        });
        /*
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right"
        };
        */

        var nothimasterid = <?php echo $nothiRecord['nothi_masters_id']; ?>;

        if (nothimasterid > 0) {
        } else {
            return false;
        }


        var selectedPriviliges = {};


        //if (confirm("আপনি কি নতুন নোট তৈরি করতে চান?")) {
        function createNoteNew() {
            Metronic.blockUI({
                target: '.page-container',
                boxed: true
            });
            $('.nothiCreateButton').attr('disabled', 'disabled');
            $.ajax({
                url: '<?php echo $this->Url->build(['controller' => 'NothiMasters', 'action' => 'add']) ?>',
                data: {formPart: {nothi_master_id: nothimasterid}, priviliges: selectedPriviliges},
                type: "POST",
                dataType: 'JSON',
                success: function (response) {
                    $('div.error-message').remove();
                    if (response.status == 'error') {
                        Metronic.unblockUI('.page-container');
                        $('.nothiCreateButton').removeAttr('disabled');
                        toastr.error(response.msg);

                        if (typeof (response.data) != 'undefined') {
                            var errors = response.data;
                            $.each(errors, function (i, value) {
                                $.each(value, function (key, val) {
                                    if (i != 'office_units_organogram_id') {
                                        $('[name=' + i + ']').focus();
                                        $('[name=' + i + ']').after('<div class="error-message">' + val + '</div>');
                                    }
                                });
                            });
                        }
                    } else if (response.status == 'success') {
                        if(!isEmpty(response.extra_message)){
                            bootbox.dialog({
                                message: response.extra_message,
                                title: "নোট তৈরির সময়কার সমস্যা",
                                buttons: {
                                    success: {
                                        label: "নতুন তৈরিকৃত নোটে গমন",
                                        className: "green",
                                        callback: function () {
                                            window.location.href = js_wb_root + 'noteDetail/' + response.id;
                                        }
                                    },
//                                danger: {
//                                    label: "না",
//                                    className: "red",
//                                    callback: function () {
//                                        Metronic.unblockUI('.page-container');
//                                    }
//                                }
                                }
                            });
                            return;
                        }
                        window.location.href = js_wb_root + 'noteDetail/' + response.id;
                    }
                },
                error: function (status, xresponse) {

                }

            });
        }

        return false;
    });
    <?php endif; ?>
    $(document).off('click', ".btn-nisponno-nothi").on('click', ".btn-nisponno-nothi", function (e) {
        Metronic.blockUI({
            target: '.page-container',
            boxed: true
        });

        var nothimasters = $(this).attr('nothi_master_id');
        var message = 'আপনি কি নোট নিষ্পন্ন করতে ইচ্ছুক?';
        if ($('.btn-send-draft-cron').length > 0) {
            message = 'আপনি কি পত্রটি জারি না করে নোট নিষ্পন্ন করতে ইচ্ছুক?';
        }
        bootbox.dialog({
            message: message,
            title: "নোট নিষ্পন্ন",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                         if(signature > 0){
                             getSignatureToken('NoteNisponno',nothimasters);
                         }else{
                              NoteNisponno(nothimasters);
                         }
                       
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                        Metronic.unblockUI('.page-container');
                    }
                }
            }
        });

    });

    function NoteNisponno(nothimasters) {
        var data = {'selectednothi' : nothimasters};
        if(signature > 0){
            data.soft_token = $("#soft_token").val();
        }
        PROJAPOTI.ajaxSubmitDataCallback(((!isEmpty(signature) && signature > 0)?ds_url:js_wb_root)+'NothiMasterMovements/finishNothiMaster', data, 'json',
            function (response) {
            if (response.status == 'success') {
                toastr.success(response.msg);
                $.ajax({
                    url: js_wb_root + 'NothiMasterMovements/nextPendingNoteSequentially',
                    dataType: "JSON",
                    method: "POST",
                    cache: true,
                    success: function (response) {
                        if (response.status == 'success') {
                            window.location.href = js_wb_root + 'noteDetail/' + response.note['nothi_part_no'] + '/' + response.note['nothi_office'];
                        } else {
                            setTimeout(function () {
                                window.location.href = js_wb_root + 'dashboard/dashboard';
                            }, 1000);

                        }
                    }
                });
            } else {
                toastr.error(response.msg);
                Metronic.unblockUI('.page-container');
            }

        });

    }

    $(document).on('click', ".btn-edit-permission", function (e) {
        $('#responsivePermissionEdit').find('.modal-title').text('');
        $('#responsivePermissionEdit').find('.modal-body').html('');

        $.ajax({
            url: js_wb_root + "nothiMasters/nothiMasterPermissionEditList/" + <?php echo $id ?> +'/<?php echo $nothi_office ?>',
            success: function (response) {
                $('#responsivePermissionEdit').modal('show');
                $('#responsivePermissionEdit').find('.modal-title').text('নথির অনুমতি সংশোধন');
                $('#responsivePermissionEdit').find('.modal-body').html(response);
            },
            error: function (status, xresponse) {

            }

        });
    });

    $(document).on('click', ".btn-delete-nothi", function (e) {

        bootbox.dialog({
            message: "আপনি কি নিশ্চিত মুছে ফেলতে চান?",
            title: "নোট মুছে ফেলুন",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        $.ajax({
                            url: js_wb_root + 'NothiMasters/nothiDelete/<?php echo $id; ?>/<?php echo $nothi_office ?>',
                            type: 'post',
                            cache: false,
                            dataType: 'json',
                            success: function (response) {
                                if (response.status == 'success') {
                                    window.location.href = js_wb_root + 'NothiMasters/index';
                                } else {
                                    toastr.error(response.msg);
                                }
                            },
                            error: function (status, xresponse) {

                            }

                        });
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                        Metronic.unblockUI('.page-container');
                    }
                }
            }
        });
    });

    $(document).on('click', ".btn-revert-nothi", function (e) {
        bootbox.dialog({
            message: "আপনি কি নথি ফেরত আনতে ইচ্ছুক?",
            title: "নথি ফেরত",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        Metronic.blockUI({target: '#notesShow_new', boxed: true});
                        $.ajax({
                            url: js_wb_root + 'NothiMasterMovements/backwardNothi/',
                            type: 'post',
                            cache: false,
                            data: {
                                nothi_part_id:   <?php echo $id; ?>,
                                nothi_office:   <?php echo $nothi_office; ?>,
                            },
                            dataType: 'json',
                            success: function (response) {
                                if (response.status == 'success') {
                                    window.location.href = js_wb_root + 'noteDetail/<?= $id ?>/<?= $nothi_office ?>';
                                } else {
                                    toastr.error(response.msg);
                                }
                                Metronic.unblockUI('#ajax-content');
                            },
                            error: function (status, xresponse) {
                                Metronic.unblockUI('#ajax-content');
                            }

                        });
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {

                    }
                }
            }
        });


    });

    $(document).on('click', ".nothiPermissionUpdate", function (e) {
	    $(this).addClass('disabled');
	    $(this).html('সংরক্ষণ হচ্ছে, অপেক্ষা করুন...');
	    var currentElement = this;
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right"
        };


        var selectedPriviliges = {};
        var j = 0;
        $.each($('#ownOfficeSelected').find('.own_office_permission'), function (i, v) {
            selectedPriviliges[j] = {};
            selectedPriviliges[j]['ofc_id'] = $(v).data('office-id');
            selectedPriviliges[j]['unit_id'] = $(v).data('office-unit-id');
            selectedPriviliges[j]['org_id'] = $(v).data('office-unit-organogram-id');
//            selectedPriviliges[j]['designation_level'] = $(v).data('designation-level');
            selectedPriviliges[j]['perm_value'] = 1;//parseInt($(v).val());
            j = j + 1;
        });
        $.each($('#ownOfficeSelected').find('tbody>tr'), function (i, v) {
            if ($(v).find('#designation_level').val() == '') {
                $(v).find('#designation_level').val(0);
            }
            selectedPriviliges[i]['designation_level'] = $(v).find('#designation_level').val();
        });
        $.each($('#otherOfficeSelected').find('tbody>tr'), function (i, v) {
            selectedPriviliges[i + j] = {};
            if ($(v).find('#designation_level').val() == '') {
                $(v).find('#designation_level').val(0);
            }
            selectedPriviliges[i + j]['designation_level'] = $(v).find('#designation_level').val();
        });
        $.each($('#otherOfficeSelected').find('.other_office_permission'), function (i, v) {
//            selectedPriviliges[j] = {};
            selectedPriviliges[j]['ofc_id'] = $(v).data('office-id');
            selectedPriviliges[j]['unit_id'] = $(v).data('office-unit-id');
            selectedPriviliges[j]['org_id'] = $(v).data('office-unit-organogram-id');
//            selectedPriviliges[j]['designation_level'] = $(v).data('designation-level');
            selectedPriviliges[j]['perm_value'] = 1;//parseInt($(v).val());
            j = j + 1;
        });

        $.ajax({
            url: js_wb_root + "nothiMasters/nothiMasterPermissionUpdate/" + <?php echo $id ?> +'/<?php echo $nothi_office ?>',
            data: {priviliges: selectedPriviliges},
            dataType: 'JSON',
            method: 'POST',
            success: function (response) {

                if (response.status == 'error') {

                    toastr.error(response.msg);

                } else if (response.status == 'success') {
                    toastr.success(response.msg);
                    $("a.btn-forward-list").trigger('click');
                    $("a.btn-save-forward-nothi").trigger('click'); 
                    if(!isEmpty(response.extra_message)){
                        bootbox.dialog({
                            message: response.extra_message,
                            title: "অনুমতি সংশোধন সময়কার সমস্যা",
                            buttons: {

                                danger: {
                                    label: "বন্ধ করুন",
                                    className: "red",
                                    callback: function () {
                                    }
                                }
                            }
                        });
                    }
                    $('#responsivePermissionEdit').modal('toggle');
                }

                $(currentElement).removeClass('disabled');
                $(currentElement).html('সংরক্ষণ');
            },
            error: function (status, xresponse) {

            }

        });
    });

    function heightlimit() {
        var resBreakpointMd = Metronic.getResponsiveBreakpoint('md');
        var handle100HeightContent = function () {

            var target = $('.full-height-content');
            var height;

            height = Metronic.getViewPort().height -
                $('.page-header').outerHeight(true) -
                $('.page-footer').outerHeight(true) -
                $('.page-title').outerHeight(true) -
                $('.page-bar').outerHeight(true);

            if (target.hasClass('portlet')) {
                var portletBody = target.find('.portlet-body');

                // new code start
                height = Metronic.getViewPort().height - $('.page-header').outerHeight(true) - $('.page-footer').outerHeight(true) - $('.page-title').outerHeight(true) - $('.page-bar').outerHeight(true);
                height = height - $('.full-height-content').find('.portlet-title').outerHeight(true) - parseInt($('.full-height-content').find('.portlet-body').css('padding-top')) - parseInt($('.full-height-content').find('.portlet-body').css('padding-bottom')) - 2;
                var new_height = height - 145;
                var total_height = 0;
                $.each($(".note_show_details"), function (key, value) {
                    total_height += parseInt($(this).height());
                });
                // if (total_height < new_height) {
                //     new_height = total_height;
                // }
                $('#notesShow_new').css('overflow', 'auto');
                //$('#notesShow_new').css('height', new_height);
                // new code end
                
                // if (Metronic.getViewPort().width < resBreakpointMd) {
                //     Metronic.destroySlimScroll(portletBody.find('.full-height-content-body')); // destroy slimscroll
                //     return;
                // }
                //
                // height = height -
                //     target.find('.portlet-title').outerHeight(true) -
                //     parseInt(target.find('.portlet-body').css('padding-top')) -
                //     parseInt(target.find('.portlet-body').css('padding-bottom')) - 2;
                //
                // if (target.hasClass("full-height-content-scrollable")) {
                //     height = height - 15;
                //
                //     $('#notesShow_new').css('overflow', 'auto');
                //     $('#notesShow_new').css('height', height - 140);
                //
                //     $('#potroShow').css('overflow', 'auto');
                //     $('#potroShow').css('height', height - 28);
                //
                //     //portletBody.find('.full-height-content-body').css('height', height);
                //     //Metronic.initSlimScroll(portletBody.find('.full-height-content-body'));
                // } else {
                //     portletBody.css('min-height', height);
                // }
            } else {
                if (Metronic.getViewPort().width < resBreakpointMd) {
                    Metronic.destroySlimScroll(target.find('.full-height-content-body')); // destroy slimscroll
                    return;
                }

                if (target.hasClass("full-height-content-scrollable")) {
                    height = height - 15;
                    target.find('.full-height-content-body').css('height', height);
                    //Metronic.initSlimScroll(target.find('.full-height-content-body'));
                } else {
                    target.css('min-height', height);
                }
            }
            $('.notecount').css({"max-height": height});
            $('.notecount').css({"overflow": 'auto'});
            $('.slimScrollBar').css({'top': '40px'});
        };
        handle100HeightContent();
        Metronic.addResizeHandler(handle100HeightContent);
        var maxhe = Math.max($('.noteContentSide').height() + 80, $('.potroContentSide').height() + 80);
        //$('#notesShow').height(maxhe);
        //$('#potroShow').height(maxhe);
        $('.nothiDetailsPage').height($('.nothiDetailsPage>div').innerHeight() - 15);
        //$('#notesShow_new').height($("#potroShow").height() - 96);
        $('#notesShow_new').css('height', $("#potroShow").height() - 96);
        $("#onucchedBody").height($("#potroShow").height());
        $("#onucchedBody").css('overflow-y', 'auto').css('overflow-x', 'hidden');
    }


    $('#portalGuardFileImport').on('click', function () {
        $('#search').val('');
        var empty_option = "<option value='0'>--</option>";

        $('#portal-guard-file-types').html('');
        $('#layer-ids').val(0);
        $('#portal-guard-file-types').html(empty_option);
        $('#portal-guard-file-types').select2();
        $('#layer-ids').select2();

        $('#portalfilelist tbody').html('<tr><td colspan="4" class="text-center"><img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span></td></tr>');
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: "<?= $this->Url->build(['controller' => 'GuardFiles',
                'action' => 'guardFileApi']) ?>",
            data: {},
            success: function (data) {
                if (data.status == 'success' || data.status == "SUCCESS") {
                    if (!isEmpty(data.data)) {
                        var list = '';
                        var i = 1;
                        $.each(data.data, function (index, value) {
                            if (isEmpty(value.link)) {
                                return false;
                            }
                            list += '<tr><td>' + BnFromEng(i) + '</td><td>' + escapeHtml(value.type) + '</td><td data-search-term="' + escapeHtml(value.name) + '">' + escapeHtml(value.name) + '</td><td class="text-center"><a class="btn btn-success btn-sm downFile" title="ডাউনলোড করুন" href="' + value.link + '"><i class="fa fa-download"></i></a></td></tr>';
                            i++;
                        });
                        $('#portalfilelist tbody').html(list);
                    } else {
                        $('#portalfilelist tbody').html('<tr><td colspan="4" style="color: red" class="text-center">দয়াকরে অফিস বাছাই করুন।</td></tr>');
                    }
                } else {
                    $('#portalfilelist tbody').html('<tr><td colspan="4" style="color: red" class="text-center">দয়াকরে অফিস বাছাই করুন। </td></tr>');
                }
            }
        });
        $('#portalResponsiveModal').modal('show');
    });

    $(document).off('click', '.downFile').on('click', '.downFile', function (ev) {
        ev.preventDefault();
        $('.WaitMsg').html('');
        $(".submitbutton").removeAttr('disabled');
        var href = $(this).attr('href');
        var value = $(this).closest('td').prev().text();
        $('#name-bng').val(value);
        $('input[name="uploaded_attachments"]').val(href);
        $('#portalDownloadModal').modal('show');
    });
    var PORTAL_GUARD_FORM = {
        submit: function () {

            $("#portalDownloadForm").attr('action', '<?php echo $this->Url->build(['_name' => 'guardFile']) ?>');
            if ($("#name-bng").val().length == 0) {
                toastr.error("দুঃখিত! গার্ড ফাইলের শিরোনাম দেয়া হয়নি।");
                return false;
            }

            if (isEmpty($("select#portal_guard_file_category_id option:selected").val())) {
                toastr.error("দুঃখিত! গার্ড ফাইলের ধরন দেয়া হয়নি।");
                return false;
            }

            if ($("#portalDownloadForm input[name='uploaded_attachments']").val().length == 0) {
                toastr.error("দুঃখিত! গার্ড ফাইল খুজে পাওয়া যায়নি।");
                return false;
            }

            $(".submitbutton").attr('disabled', 'disabled');
            var name_bng = $("#name-bng").val();
            var guard_file_category_id = escapeHtml($("select#portal_guard_file_category_id option:selected").val());
            var guard_file_category_name = escapeHtml($("select#portal_guard_file_category_id option:selected").text());
            var uploaded_attachments = ($("#portalDownloadForm input[name='uploaded_attachments']").val());
            $('.WaitMsg').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; ডাউনলোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

            $.ajax({
                type: 'POST',
                url: "<?php
                    echo $this->Url->build(['_name' => 'guardFile'])
                    ?>",
                data: {
                    "office_id":<?= $employee_office['office_id'] ?>,
                    "name_bng": name_bng,
                    "guard_file_category_id": guard_file_category_id,
                    "uploaded_attachments": uploaded_attachments,
                    "type": 'portal'
                },
                success: function (data) {
                    if (data.status == 'success') {
                        $('.WaitMsg').html('');
                        toastr.success("গার্ড ফাইল ডাউনলোড করা হয়েছে।");
                        $(".submitbutton").removeAttr('disabled');
                        $('#portalDownloadModal').modal('hide');
                        $('#portalResponsiveModal').modal('hide');
                        $('#portalResponsiveModalPotro').modal('hide');
                        if ($('#note').data('froala.editor')) {
                            Metronic.blockUI({
                                target: '.UIBlockPotro',
                                message: 'অপেক্ষা করুন'
                            });
                            callGuardFilePotro();
                            setTimeout(function () {
                                $("#guard_file_category_potro").val(guard_file_category_id);
                                $("#s2id_guard_file_category_potro [id^=select2-chosen]").text(guard_file_category_name);
                                $("#guard-file-category-id").trigger('click');
                                Metronic.unblockUI('.UIBlockPotro');
                            }, 250)
                        } else {
                            Metronic.blockUI({
                                target: '.UIBlock',
                                message: 'অপেক্ষা করুন'
                            });
                            callGuardFile();
                            setTimeout(function () {
                                $("#guard-file-category-id ").val(guard_file_category_id);
                                $("#s2id_guard-file-category-id [id^=select2-chosen]").text(guard_file_category_name);
                                $("#guard-file-category-id").trigger('click');
                                Metronic.unblockUI('.UIBlock');
                            }, 250)
                        }
                    } else {
                        $('.WaitMsg').html('');
                        toastr.error("দুঃখিত! পোর্টালের সাথে যোগাযোগ করা সম্ভব হচ্ছে না।");
                        $(".submitbutton").removeAttr('disabled');
                    }
                }
            });

        }
    }

    $(document).off('click', '.showAttachmentOfContent').on('click', '.showAttachmentOfContent', function () {
        $('#responsiveOnuccedModal').modal('show');
        var file = $(this).text();
        $('#responsiveOnuccedModal').find('.modal-title').html(file);

        var url = $(this).data('url');
        var attachment_type = $(this).data('type');

        if (typeof(url) != 'undefined') {

            if (attachment_type == 'text') {
                $('#responsiveOnuccedModal').find('.modal-body').html('<div style="background-color: #fff; max-width:950px; min-height:815px; max-height: 815px; margin:0 auto; page-break-inside: auto;">"' + file + '<br/>" + file + "</div>"');
            } else if ((attachment_type.substring(0, 15)) == 'application/vnd' || (attachment_type.substring(0, 15)) == 'application/ms' || (attachment_type.substring(0, 15)) == 'application/pdf') {
                $('#responsiveOnuccedModal').find('.modal-body').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + url + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

            } else if ((attachment_type.substring(0, 5)) == 'image') {
                $('#responsiveOnuccedModal').find('.modal-body').html('<embed src="' + url + '" style=" width:100%; height: 700px;" type="' + attachment_type + '"></embed>');
            } else {
                $('#responsiveOnuccedModal').find('.modal-body').html('দুঃখিত! ডাটা পাওয়া যায়নি');
            }
        }

    });

    $(document).off('change', '#layer-ids').on('change', '#layer-ids', function () {
        var layer = $('#layer-ids').val();

        if (!(isEmpty(layer))) {
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: "<?= $this->Url->build(['controller' => 'GuardFiles',
                    'action' => 'getPortalTypesByLayer']) ?>" + "/" + layer,
                data: {},
                success: function (data) {
                    var list = "<option value='0'>--</option>";
                    if (!(isEmpty(data))) {
                        $.each(data, function (index, value) {
                            list += "<option value='" + value + "'>" + value + "</option>"
                        });

                    }
                    $('#portal-guard-file-types').html('');
                    $('#portal-guard-file-types').html(list);
                    $('#portal-guard-file-types').select2();

                }
            });
        }
    });


    $(document).off('change', '#portal-guard-file-types').on('change', '#portal-guard-file-types', function () {
        var type = $('#portal-guard-file-types').val();

        $('#portalfilelist tbody').html('<tr><td colspan="4" class="text-center"><img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span></td></tr>');
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: "<?= $this->Url->build(['controller' => 'GuardFiles',
                'action' => 'guardFileApi']) ?>" + "/" + type,
            data: {},
            success: function (data) {
                if (data.status == 'success' || data.status == "SUCCESS") {
                    if (!isEmpty(data.data)) {
                        var list = '';
                        var i = 1;
                        $.each(data.data, function (index, value) {
                            if (isEmpty(value.link)) {
                                return false;
                            }
                            list += '<tr><td>' + BnFromEng(i) + '</td><td>' + escapeHtml(value.type) + '</td><td data-search-term="' + escapeHtml(value.name) + '">' + escapeHtml(value.name) + '</td><td class="text-center"><a class="btn btn-success btn-sm downFile" title="ডাউনলোড করুন" href="' + value.link + '"><i class="fa fa-download"></i></a></td></tr>';
                            i++;
                        });
                        $('#portalfilelist tbody').html(list);
                    } else {
                        $('#portalfilelist tbody').html('<tr><td colspan="4" style="color: red" class="text-center">দুঃখিত! কোন ডাটা পাওয়া যায়নি।</td></tr>');
                    }
                } else {
                    $('#portalfilelist tbody').html('<tr><td colspan="4" style="color: red" class="text-center">দুঃখিত! কোন ডাটা পাওয়া যায়নি। </td></tr>');
                }
            }
        });
    });
    $("#search").on("keyup", function () {
        var text = $(this).val().toLowerCase();

        $("table tr td").each(function () {
            var data = $(this).attr('data-search-term');
            if (typeof(data) == 'undefined') {
                return;
            }
            if ($(this).filter('[data-search-term *= ' + text + ']').length > 0 || text.length < 1) {
                $(this).closest('tr').show();
            }
            else {
                $(this).closest('tr').hide();
            }
        });
    });
</script>
<script>
    function iniDictionary() {
        $('.fr-wrapper').contextmenu(function (e) {
            var data = $('#noteComposer').froalaEditor('selection.text');
            if (data != "" && data.length > 1) {
                e.preventDefault();
                if ((data[data.length - 1]) == ' ') {
                    var isSpace = ' ';
                } else {
                    var isSpace = '';
                }
                $("#word").val(data);
                $("#space").val(isSpace);
                customContextMenuCall(data);
            } else {
                var data = getWordFromEvent(e);
                if (data != "" && data.length > 1) {
                    e.preventDefault();
                    if ((data[data.length - 1]) == ' ') {
                        var isSpace = ' ';
                    } else {
                        var isSpace = '';
                    }
                    $("#word").val(data);
                    $("#space").val(isSpace);
                    customContextMenuCall(data);
                } else {
                    e.stopPropagation();
                }
            }
        });

        function customContextMenuCall(word) {
            'use strict';
            var errorItems = {"errorItem": {name: "Word Load error"},};
            var loadItems = function () {

                var dfd = jQuery.Deferred();
                setTimeout(function () {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: 'https://ovidhan.tappware.com/search.php',
                        data: {'word': $("#word").val()},
                        success: function (data) {
                            if (data.length > 10) {
                                var subSubItems = {};
                            }
                            var i = 1;
                            var subItems = {};
                            $(data).each(function (j, v) {
                                if (i < 11) {
                                    var name = {};
                                    name['name'] = v;
                                    subItems[v] = name;
                                } else {
                                    var name = {};
                                    name['name'] = v;
                                    subSubItems[v] = name;
                                }
                                i++;
                            });

                            if (data.length > 10) {
                                var seeMoreMenu = {};
                                seeMoreMenu = {
                                    name: "<strong>আরও দেখুন...</strong>",
                                    isHtmlName: true,
                                    items: subSubItems
                                };
                                subItems['আরও দেখুন...'] = seeMoreMenu;
                            }

                            dfd.resolve(subItems);
                        }
                    });
                }, 100);
                return dfd.promise();
            };

            $.contextMenu({
                selector: '.fr-wrapper',
                build: function ($trigger, e) {
                    return {
                        callback: function (key, options) {
                            if (key != "কোন তথ্য পাওয়া যায় নি।") {
                                $('#noteComposer').froalaEditor('html.insert', (key + ($("#space").val())));
                            }
                        },
                        items: {
                            "status": {
                                name: "সাজেশন্স",
                                icon: "fa-check",
                                items: loadItems(),
                            },
                        }
                    };
                }
            });


            var completedPromise = function (status) {
            };

            var failPromise = function (status) {
            };

            var notifyPromise = function (status) {
            };

            $.loadItemsAsync = function () {
                var promise = loadItems();
                $.when(promise).then(completedPromise, failPromise, notifyPromise);
            };
        }
    }

    $("form#advanceSearchForm").submit(function (event) {
        Metronic.blockUI({target: '#advanceSearchForm'});
        event.preventDefault();
        $('.showPreview').attr('src', '');
        PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot('<?= $this->Url->build(['_name' => 'onucchedPdf', $nothi_office]); ?>',
            $(this).serializeArray(), 'json', function (response) {
                if (response.status == 'success') {
                    $('.showPreview').attr('src', response.filename);
                } else {
                    toastr.error(response.msg);
                }
                Metronic.unblockUI('#advanceSearchForm');
            }
        );
    });

    $(document).ready(function () {
        $( document ).ajaxStop(function() {
            // start new code
            height = Metronic.getViewPort().height - $('.page-header').outerHeight(true) - $('.page-footer').outerHeight(true) - $('.page-title').outerHeight(true) - $('.page-bar').outerHeight(true);
            height = height - $('.full-height-content').find('.portlet-title').outerHeight(true) - parseInt($('.full-height-content').find('.portlet-body').css('padding-top')) - parseInt($('.full-height-content').find('.portlet-body').css('padding-bottom')) - 2;
            var new_height = height - 140;
            var total_height = 0;
            $.each($(".note_show_details"), function (key, value) {
                total_height += parseInt($(this).height());
            });
            if ($("#responsiveNoteEdit").css('display') != 'none') {
                if (total_height < new_height) {
                    new_height = total_height;
                }
            }

            $('#notesShow_new').css('overflow', 'auto');
            //$('#notesShow_new').css('height', new_height);
            // end new code
        });
    });


    $(document).ready(function() {
        $(document).find('[title]').tooltip({'placement':'bottom', 'container': 'body'});
        $(".nothiDetailsPage>div").css('padding-top', $(".nothiDetailsPage .noteSubject").height());
    })
</script>
