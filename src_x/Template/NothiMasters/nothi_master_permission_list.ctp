<style>
    input[type=checkbox]{
        height: 16px;
    }
</style>

    <?php echo $this->cell('NothiOwnUnitPermissionList', ['prefix' => '', 'id'=>$id,'nothi_office'=>$nothi_office]); ?>
    <?php // echo $this->cell('NothiOtherOfficePermissionList', ['prefix' => '', 'id'=>$id]); ?>

<script>
      $(document).find('[title]').tooltip({'placement':'bottom'});

      $("#ownOfficeUnitList").on("change", function() {
          var id = $(this).val();
          if (id == 0) {
              $('[id^="collapse_"]').parent().show();
          } else {
              $('[id^="collapse_"]').parent().hide();
              $('[id="collapse_'+id+'"]').parent().show();
              $('[id="collapse_'+id+'"]').parent().find('.accordion-toggle').trigger('click');
          }
      });
</script>