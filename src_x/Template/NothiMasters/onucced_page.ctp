<style>
    .portlet-body .noteContent img {
        width: 100% !important;
        height: 50px !important;
    }
</style>
<div class="portlet  full-height-content full-height-content-scrollable">

    <div class="portlet-body ">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php

                use Cake\I18n\Time;

                $lastNoteNo = 0;
                $i = 0;
                if (!empty($notesquery)) {

                    if (!empty($notesquery->digital_sign)) {
                        //verify digital sign
                        $sign_info = json_decode($notesquery->sign_info, true);
                        $is_vefied_signature = verifySign($notesquery['note_description'], 'html', $sign_info['publicKey'], $sign_info['signature']);
                    }

                    echo '<div ' . ($notesquery['note_status'] == 'DRAFT' ? 'class="noteDetails"' : '') . ' id="note_' . $notesquery->id . '">';
                    echo '<div class="noteContent noMarginPadding" id="' . $this->Number->format($notesquery->note_no) . '">';

                    echo "<div class='printArea'>";
                    if (!empty($notesquery->subject))
                        echo '<div class="noteNo noteSubject"  notesubjectid=' . $notesquery->id . '> বিষয়: <b>' . ($notesquery->subject) . '</b></div>';

                    if ($notesquery->is_potrojari == 0)
                        echo '<span class="noteNo"> অনুচ্ছেদ: ' . $notesquery->nothi_part_no_bn . '.' . $this->Number->format($notesquery->note_no) . '</span>';
                    echo "<div class='noteDescription' id=" . $notesquery->id . "  " . ($notesquery->is_potrojari ? "style='color:red;'" : '') . " >" . base64_decode($notesquery->note_description) . "</div>";

                    if (!empty($noteAttachmentsmap[$notesquery->id])) {

                        echo "<hr/><div class='table' style='font-size:11px;'><table class='table table-bordered table-stripped hidden-print'><tr><th colspan='4' class='text-left'>" . __(SHONGJUKTI) . "</th></tr>";

                        foreach ($noteAttachmentsmap[$notesquery->id] as $attachmentKey => $attachment) {
                            if (empty($attachment['user_file_name'])) {
                                $attachment['user_file_name'] = 'কোন নাম দেওয়া হয়নি';
                            }
                            $fileName = explode('/', $attachment['file_name']);
                            echo "<tr>";
                            echo "<td style='width:10%;'  class='text-center'>" . $this->Number->format($attachmentKey + 1) . "</td>";
                            $user_file_name = htmlspecialchars($attachment['user_file_name']);
                            $url_file_name = "{$attachment['id']}/showAttachment/{$id}";
//                                        $url_file_name=  "{$this->request->webroot}nothiNoteSheets/showAttachment?id={$attachment['id']}&n_id={$id}";
                            echo "<td style='width:35%;' class='preview_attachment' noteid='{$notesquery->note_no}'><span class='preview'><a class='showforPopup' data-gallery user_file_name='{$user_file_name}' type='{$attachment['attachment_type']}'  download='{$attachment['file_name']}' href='{$url_file_name}' title='" . urldecode($fileName[count($fileName) - 1]) . "'>" . urldecode($fileName[count($fileName) - 1]) . "</a></span></td>";
                            echo "<td style='width:35%; text-align:center; font-size:14px;' class='attachment_user_title'><span class='preview user_file_name'>{$attachment['user_file_name']}</span></td>";
                            echo "<td style='width:20%;' class='text-center download-attachment'><a  href='" . $this->Url->build([
                                    "controller" => "nothiNoteSheets",
                                    "action" => "downloadNoteAttachment",
                                    "?" => ["id" => $attachment['id'], 'nothimaster' => $id, 'nothi_office' => $nothi_office]]) . "' title='ডাউনলোড'><i class='fa fa-download'></i></a> &nbsp;&nbsp;
" . ($notesquery['note_status'] == 'DRAFT' ? "<a class='deleteAttachment' data-note-sheet-id='{$nothi_note_sheets_id}' data-note-id='{$notesquery->id}' data-attachment-id='{$attachment['id']}' ><i style='vertical-align:baseline!important;' class='fs1 a2i_gn_delete2 '></i></a>" : "") . "</td>";
                            echo "</tr>";
                        }
                        echo "</table></div>";
                    }

                    if (isset($signatures[$notesquery['id']])) {
                        $n = count($signatures[$notesquery['id']]);
                        $col = 4;

                        $data = $signatures[$notesquery['id']];
                        $finalArray = array();
                        $finalArray[0][0] = $data[0];

                        $designationSeqPrev = array();

                        $j = 0;
                        $notesqueryC = 0;
                        $i = 1;

                        if (!isset($data[$i]['office_organogram_id'])) {
                            $designationSeq = null;
                        } else {
                            $designationSeq = isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']]) ? $employeeOfficeDesignation[$data[$i]['office_organogram_id']] : null;
                        }

                        if (isset($employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']])) {
                            $designationSeqPrev = $employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']];
                        } else {
                            $designationSeqPrev[0] = 0;
                        }


                        while ($i < $n) {

                            if ($designationSeq[0] <= $designationSeqPrev[0]) {
                                if ($j < $col - 1) {
                                    $finalArray[$notesqueryC][++$j] = $data[$i];
                                } else {
                                    $finalArray[++$notesqueryC][$j] = $data[$i];
                                }
                            } else {
                                $notesqueryC++;
                                if ($j > 0) {
                                    $finalArray[$notesqueryC][--$j] = $data[$i];
                                } else {
                                    $finalArray[$notesqueryC][$j] = $data[$i];
                                }
                            }

                            $i++;
                            if (isset($data[$i]['office_organogram_id']) && isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']])) {
                                $designationSeq = $employeeOfficeDesignation[$data[$i]['office_organogram_id']];
                            }
                            if (isset($data[$i]['office_organogram_id']) && isset($employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']])) {
                                $designationSeqPrev = $employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']];
                            }
                        }

                        if (!empty($finalArray)) {

                            for ($findex = 0; $findex < sizeof($finalArray); $findex++) {
                                echo "<div class='row' style='margin:0!important;padding:0!important'>";
                                for ($fj = 0; $fj < $col; $fj++) {
                                    echo '<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 text-center" style="padding:2px;"><div style="line-height: 1.2!important;font-size: 10px; color: darkviolet; display: block; ' . (!empty($finalArray[$findex][$fj]['cross_signature']) ? ' text-decoration: line-through;' : '') . ' vertical-align: bottom;page-break-inside:auto!important;margin:0!important;padding:0!important; ">';
                                    if (isset($finalArray[$findex][$fj])) {
                                        echo "<div class='btn btn-block ' style='height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;'>";
                                        if (!empty($finalArray[$findex][$fj]['is_signature']) && empty($finalArray[$findex][$fj]['cross_signature'])) {
                                            $englishdate = new \Cake\I18n\Time($finalArray[$findex][$fj]['signature_date'], "d-MM-y H:m:ss", null, null);
                                            if (!empty($finalArray[$findex][$fj]['digital_sign'])) {
                                                if (empty($is_vefied_signature)) {
                                                    echo '<i class="fa fa-times" style="left:5px;position: absolute;background-color: red;color: white;padding:  0px;border-radius:  5px;margin: 0px 1px;border: none;" data-original-title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।" title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।"></i>';
                                                } else {
                                                    echo '<i class="fa fa-check" style="left:5px;position: absolute;background-color: green;color: white;padding:  0px;border-radius:  5px;margin: 0px 1px;border: none;" data-original-title="ডিজিটাল সাইনকৃত।" title="ডিজিটাল সাইনকৃত।"></i>';
                                                }
                                            }
                                            echo '<img class="signatures" style="height: 40px!important;padding:0!important;margin:0!important;" src="' . $finalArray[$findex][$fj]['signature_b64'] . '" alt="সাইন যোগ করা হয়নি" alt="সাইন যোগ করা হয়নি" />';
                                        } else {
                                            echo '<img class="img-responsive" style="height: 40px!important;padding:0!important;margin:0!important; visibility: hidden;" />';
                                        }
                                        echo "</div>";

                                        echo "<span style='border-top:1px solid #9400d3;font-size: 8pt;display: block;'>" . $this->Time->format(
                                                $finalArray[$findex][$fj]['signature_date'], "d-MM-y H:m:ss", null, null) . "</span><span style='font-size: 8pt;display: block;'>" . h($finalArray[$findex][$fj]['name']) . "</span><span style='font-size: 8pt;display: block;'>" . $finalArray[$findex][$fj]['employee_designation'] . "</span>";
                                    } else
                                        echo "<div class='btn btn-block ' style='height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;'>&nbsp;</div>";
                                    echo '</div></div>';
                                }
                                echo "</div>";
                            }
                        }
                    }
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';

                } else {
                    echo "<p class='text-center text-danger'>এই পেজ এ কোন অনুচ্ছেদ দেয়া হয়নি</p>";
                }
                ?>

            </div>
        </div>
    </div>
</div>