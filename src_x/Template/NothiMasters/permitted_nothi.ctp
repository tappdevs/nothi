<?php
$session                 = $this->request->session();
$modules                 = $session->read('modules');
$selected_module         = $session->read('module_id');
$selected_office_section = $session->read('selected_office_section');


if (isset($other_organogram_activities_setting[0]['organogram_id'])) {
	$organogram_id = $other_organogram_activities_setting[0]['organogram_id'];
} else {
	$organogram_id = $other_organogram_activities_settings[0]['organogram_id'];
}
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
	<div class="row" style="padding-left: 15px;">
		<div class="portlet box">
			<div class="portlet-body">
				<div class="row" style="padding-left: 5px;">
					<div class="col-md-2">
						<span>পদবি নির্বাচন করুন</span>
					</div>
					<div class="col-md-10">
						<?php echo $this->Form->create('', ['method' => 'post']); ?>
						<select class="form-control select2" name="organogram_id" onchange="submit()">
							<?php foreach ($other_organogram_activities_settings as $other_organogram_activities_setting): ?>
								<?php $designation_info = designationInfo($other_organogram_activities_setting['organogram_id']); ?>
								<option  <?=$organogram_id == $other_organogram_activities_setting['organogram_id'] ? 'selected' : ''; ?> value="<?=$other_organogram_activities_setting['organogram_id']?>"><?= $designation_info['officer_name'] ?>, <?= $designation_info['officer_designation_label'] ?>, <?= $designation_info['office_unit_name'] ?></option>
							<?php endforeach;?>
						</select>
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
				<hr/>

				<div class="content-dak" >
					<ul class="nav nav-tabs nav-justified">
						<li class="inbox bold <?php if ($listtype == 'inbox') echo 'active'; ?>">
							<a href="#inbox-tab" data-toggle="tab" aria-expanded="true" class="font-lg">আগত নথি</a>
						</li>
						<li class="sent bold <?php if ($listtype == 'sent') echo 'active'; ?>">
							<a href="#sent-tab" data-toggle="tab" aria-expanded="false" class="font-lg">প্রেরিত নথি</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade active in" id="inbox-tab">
							<div class="table-container data-table-dak">
								<table class="table table-striped table-bordered table-hover" id="datatable-nothi-masters">
									<thead>

									<tr role="row" class="heading">

										<th style="width: 5%" class='text-center'>
											ক্রম
										</th>


										<?php
										if($listtype == 'sent' || $listtype == 'other_sent'){
											?>
											<th class='text-center' style="width: 15%">
												নথি নম্বর
											</th>
											<th class='text-center' style="width: 20%">
												শিরোনাম
											</th>
											<th class='text-center' style="width: 10%">
												প্রেরণের তারিখ
											</th>
										<?php }else{?>
											<th class='text-center' style="width: 20%">
												নথি নম্বর
											</th>
											<th class='text-center' style="width: 25%">
												শিরোনাম
											</th>
										<?php } ?>
										<?php
										if($listtype !='all' && $listtype !='archiveNothi'):
											?>
											<th class='text-center' style="width: 20%">
												<?php
												if ($listtype != 'inbox' && $listtype != 'other') {
													?>নথির অবস্থান <?php
												} else {
													?>সর্বশেষ নোটের তারিখ<?php }
												?>
											</th>
										<?php endif; ?>

										<th class='text-center' style="width: 20%">
											নথির শাখা
										</th>
										<?php if ($listtype != 'office') : ?>
											<th class='text-center' style="width: 5%">
												বিস্তারিত
											</th>
										<?php endif; ?>
										<?php if ($listtype == 'inbox' && 'archive'=='true') { ?>
											<th style="width: 5%" class='text-center'>
												আর্কাইভ
											</th>
										<?php } ?>
									</tr>
									</thead>
									<tbody id="dbl_click">

									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane fade" id="sent-tab">
							<div class="table-container data-table-dak">
								<table class="table table-striped table-bordered table-hover" id="datatable-nothi-masters">
									<thead>

									<tr role="row" class="heading">

										<th style="width: 5%" class='text-center'>
											ক্রম
										</th>
										<th class='text-center' style="width: 15%">
											নথি নম্বর
										</th>
										<th class='text-center' style="width: 20%">
											শিরোনাম
										</th>
										<th class='text-center' style="width: 10%">
											প্রেরণের তারিখ
										</th>
										<th class='text-center' style="width: 20%">
											নথির অবস্থান
										</th>
										<th class='text-center' style="width: 20%">
											নথির শাখা
										</th>
										<th class='text-center' style="width: 5%">
											বিস্তারিত
										</th>
									</tr>
									</thead>
									<tbody id="dbl_click">

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="clearfix margin-bottom-20">
		</div>
	</div>
</div>
<!-- Modal -->
<div id="detailsModal" class="modal fade modal-purple" data-backdrop="static" role="dialog">
	<div class="modal-dialog  modal-full">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">বিস্তারিত</h4>
			</div>
			<div class="modal-body">
				<div class="details-modal-inbox-content"></div>
			</div>
			<div class="modal-footer">
				<div class="btn-group btn-group-round">
					<a type="button" class="btn btn-primary btn-sokol" href=""><i class="fs1 a2i_gn_note1"></i> সকল নোট</a>
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fs1 efile-close1"></i> <?=__('Close') ?></button>
				</div>
			</div>
		</div>

	</div>
</div>

<!---CDN-->
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_nothi_permitted_masters.js"></script>
<script src="<?php echo $this->request->webroot; ?>projapoti-nothi/js/nothi_master_permitted_movements.js?v=<?= js_css_version ?>"></script>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->request->webroot; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo $this->request->webroot; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?php echo $this->request->webroot; ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/table-advanced.js"></script>

<script type="text/javascript">
    function submitSectionSelectionForm(action, org_id) {
        $("#dak_unit_selection_form_" + org_id).attr("action", action);
        $("#dak_unit_selection_form_" + org_id).submit();
    }
    jQuery.ajaxSetup({
        cache: true
    });
    $("#selectDesignation").on('change', function () {

        var url = '<?= $this->request->webroot ?>permitted_nothi';

        submitSectionSelectionForm(url, $(this).val());

    });

    NothiMasterMovement.init('<?php echo $listtype; ?>', '<?=$organogram_id?>');

    $(function() {
        $(".content-dak ul li").on('click', function() {
            if($(this).hasClass('inbox')) {
                if ( $.fn.dataTable.isDataTable( '#sent-tab #datatable-nothi-masters' ) ) {
                    $("#sent-tab #datatable-nothi-masters").dataTable().fnDestroy();
                }
                if ( $.fn.dataTable.isDataTable( '#inbox-tab #datatable-nothi-masters' ) ) {
                    $("#inbox-tab #datatable-nothi-masters").dataTable().fnDestroy();
                }
                NothiMasterMovement.init('inbox', '<?=$organogram_id?>');
            }
            if($(this).hasClass('sent')) {
                if ( $.fn.dataTable.isDataTable( '#sent-tab #datatable-nothi-masters' ) ) {
                    $("#sent-tab #datatable-nothi-masters").dataTable().fnDestroy();
                }
                if ( $.fn.dataTable.isDataTable( '#inbox-tab #datatable-nothi-masters' ) ) {
                    $("#inbox-tab #datatable-nothi-masters").dataTable().fnDestroy();
                }
                NothiMasterMovement.init('sent', '<?=$organogram_id?>');
            }
        });
    })

    jQuery(document).ready(function () {
        $(".form_date").datepicker({
            autoclose: true,
            isRTL: Metronic.isRTL(),
            format: "yyyy-mm-dd",
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left")
        });
    });
</script>