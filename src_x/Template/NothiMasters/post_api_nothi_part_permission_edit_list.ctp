<style>
    input[type=checkbox]{
        height: 16px;
    }
</style>
<div>
    <?php
        if(!empty($api) && ($api == true || $api == 1) && !empty($employee_office_info)){
            echo $this->cell('NothiOwnUnitPermissionList::updatePermission', ['', $id,$nothi_office,$employee_office_info,true])->render('api_update_permission');
        }else{
            echo $this->cell('NothiOwnUnitPermissionList::updatePermission', ['', $id,$nothi_office]);
        }

    ?>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
<script>
    jQuery(document).ready(function(){
         $(document).find('[title]').tooltip({placement: 'top'});
		$('select').select2('destroy');
    });
</script>
