<?php
if (isset($mobile) && $mobile == true) {
?>
<div class='portlet box blue'>
    <div class="portlet-title">
        <div class="caption">
            <?php
            if (isset($backButton) && !empty($backButton)) {
                echo "<a class='btn btn-danger ' href = '{$backButton}'>ফেরত যান</a>";
            }
            ?>
        </div>
    </div>
    <div class="portlet-body">
        <?php } ?>
        <?php
        if ($row['attachment_type'] == 'text') {

            echo '<div style="background-color: #fff; max-width:950px; min-width:16cm; min-height:815px; max-height: 815px; margin:0 auto; page-break-inside: auto;">' . html_entity_decode($row['content_body']) . "</div>";
        } elseif (!empty($row['file_name'])) {
            $url = urlencode(FILE_FOLDER . $row['file_name'] . "?token=" . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]));
            $down_url = ($this->request->webroot."content/" . $row['file_name'] . "?token=" . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]));
            if (substr($row['attachment_type'], 0, 5) != 'image') {

                if (substr($row['attachment_type'], 0, strlen('application/vnd')) == 'application/vnd' || substr($row['attachment_type'], 0, strlen('application/ms')) == 'application/ms') {
//                    echo "<h4 class='alert alert-danger'>ফাইলটি দেখতে কোন ধরনের সমস্যা হলে, দয়া করে ডাউনলোড করে দেখুন। ". $this->Html->link(__('Download'), ['_name'=>(!empty($name)?$name:'downloadPotro'),$nothi_office,$row['id']]). "</h4>";
                    echo "<h4 class='alert alert-danger'>ফাইলটি দেখতে কোন ধরনের সমস্যা হলে, দয়া করে ডাউনলোড করে দেখুন। <a class='btn btn-sm btn-success' href='" . $down_url . "' download><i class='fa fa-download'></i> ডাউনলোড</a></h4>";
                    echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url . ' &embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';

                } else {
                    if ($row['attachment_type'] == 'application/pdf') {

                        //echo "<h4 class='alert alert-danger'>ফাইলটি দেখতে কোন ধরনের সমস্যা হলে, দয়া করে ডাউনলোড করে দেখুন। ". $this->Html->link(__('Download'), ['_name'=>(!empty($name)?$name:'downloadPotro'),$nothi_office,$row['id']]). "</h4>";
                        echo "<h4 class='alert alert-danger'>ফাইলটি দেখতে কোন ধরনের সমস্যা হলে, দয়া করে ডাউনলোড করে দেখুন। <a class='btn btn-sm btn-success' href='" . $down_url . "' download><i class='fa fa-download'></i> ডাউনলোড</a></h4>";
                        if(isset($isMobile) && $isMobile == true){
                            echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                        } else {
                            if (!empty($page)) {
                                echo '<embed src="' . $down_url . '#page=' . $page . '" style=" width:100%; height: 700px;" type="' . $row['attachment_type'] . '"></embed>';
                            } else {
                                echo '<embed src="' . $down_url . '" style=" width:100%; height: 700px;" type="' . $row['attachment_type'] . '"></embed>';
                            }
                        }
                    } else {
                        echo "<h4 class='alert alert-danger'>ফাইলটি দেখতে কোন ধরনের সমস্যা হলে, দয়া করে ডাউনলোড করে দেখুন। <a class='btn btn-sm btn-success' href='" . $down_url . "' download><i class='fa fa-download'></i> ডাউনলোড</a></h4>";
                        echo '<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
                    }
                }
            } else {
                echo '<div class="text-center"><a href="' . $this->request->webroot . 'content/' . $row['file_name'] . "?token=" . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" data-gallery="multiimages"  data-toggle="lightbox" data-footer="" ><img class="zoomimg img-responsive"  src="' . $this->request->webroot . 'content/' . $row['file_name'] . "?token=" . sGenerateToken(['file' => $row['file_name']], ['exp' => time() + 60 * 300]) . '" alt="ছবি পাওয়া যায়নি"></a></div>';
            }
        } else {
            echo "<p class='text-danger text-center'>পাওয়া যায়নি</p>";
        }
        ?>
        <?php
        if (isset($mobile) && $mobile == true) {
        ?>
    </div>
</div>
<?php
}
?>
