<style>
    .radio-list {
        display: inline;
    }

    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        margin-left: 0px;
        position: relative;
    }

    .checkbox-inline, .radio-inline {
        padding-left: 5px;
        font-size: 12px;
    }

    .checkbox-inline + .checkbox-inline, .radio-inline + .radio-inline {
        margin-left: 2px;
    }

    .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
        margin-left: 0px !important;
    }

    input[type="checkbox"], input[type="radio"] {
        margin-top: 0px;
    }
</style>


<div class="portlet-body form">
    <div class="row" style="display: none;">
        <label class="col-md-2 control-label">তারিখ </label>
        <div class="col-md-10">
            <div class="input-group date form_date">
                <?= $this->Form->input('due_date', ['label' => false, 'class' => 'form-control','type' => 'hidden', 'readonly' => true, 'value' => Date("Y-m-d")]); ?>
                <span class="input-group-btn">
                    <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php echo $this->cell('NothiOwnUnit::getNothiPermittedListForUser',[$nothi_office,1,0, !empty($master_id)?$master_id:0]) ?>
        </div>
    </div>
</div>
