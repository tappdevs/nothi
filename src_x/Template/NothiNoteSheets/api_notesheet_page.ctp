<?php

use Cake\I18n\Time;

if (!defined("CDN_PATH")) {
    define("CDN_PATH", $this->request->webroot);
}
?>
<meta content="initial-scale=0" name="viewport"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>projapoti-nothi/css/styles.css"/>
<style>
    .fr-view{
        overflow: scroll;
    }

    .portlet {
        font-size: 12pt;
    }

    a[href]:after {
        content: "";
    }

    .btn-forward-nothi, .btn-sar-nothi-draft, .btn-othi-list, .btn-nisponno-nothi {
        padding: 3px 5px !important;
        border-width: 1px;

    }

    .portlet-body .notesheetview img {
        width: 100% !important;
        height: 50px !important;
    }

    .btn-icon-only {

    }

    .portlet.box > .portlet-title {
        padding: 0 5px !important;
    }

    .portlet.box.green > .portlet-title > .actions > a.btn {
        background-color: #fff !important;
        color: green !important;;
    }

    .portlet.box.green > .portlet-title > .actions > a.btn > i {
        color: green !important;;
    }

    #addNewNote {
        padding: 5px !important;
    }
</style>
<?php
$bookMarkLi = '';
$currentLi = array();
?>

<style>
    .nothiGroupView.dropdown-menu {
        width: 50px !important;
        min-width: 55px;
        box-shadow: none;
    }

    .nothiDetailsPage {
        overflow-y: auto;
        padding: 0px;
    }

    .nothiDetailsPage {
        height: auto !important;
    }
</style>

<div class="modal fade modal-purple height-auto" id="responsiveOnuccedModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full">

        <div class="modal-content">
            <div class="modal-header">

                <button type="button" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<?php
if (empty($is_new) || $is_new == 0) {
    ?>
    <div class="alert alert-danger font-lg text-center">
        <i class="fa fa-info-circle " aria-hidden="true"></i>
        প্রিয় ব্যবহারকারী, আপনি পুরনো এন্ড্রয়েড অ্যাপ ব্যবহার করছেন। দয়া করে নতুন এন্ড্রয়েড অ্যাপ ইন্সটল করুন।
        এন্ড্রয়েড অ্যাপ ইন্সটল করতে
        <a href=" https://play.google.com/store/apps/details?id=com.tappware.nothipro" target="_blank">
            <img src="<?= $this->request->webroot ?>img/app_image.png" alt="" style="hight:40px;width:40px;">
        </a>
        ছবিতে ক্লিক করুন অথবা Google Play Store থেকে অ্যাপটির নাম (<b>eNothi System</b>) লিখেও খুঁজে নিতে পারেন।
        ধন্যবাদ।
    </div>
    <?php
}
?>
<div id="notesShow">
    <div class="portlet box grey ">
        <div class="portlet-title " style="border-bottom: 1px solid #eee;">
            <div class="caption">
                <a onclick="javascript: window.location.reload();" class="btn btn-lg btn-default"><i
                            class="glyphicon glyphicon-refresh"></i></a>
            </div>
            <div class="actions">
                <ul class="pagination pagination-lg">
                    <?php
                    //                    echo $this->Paginator->last(__('শেষ', true),array('class' => 'number-last'));
                    echo $this->Paginator->next('<<',
                        array('tag' => 'li', 'escape' => false),
                        '<a href="#" class="btn btn-md blue ">&raquo;</a>',
                        array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li',
                        'currentLink' => true, 'modulus' => 2, 'currentClass' => 'active',
                        'currentTag' => 'a',
                        'reverse' => true));
                    echo $this->Paginator->prev('>>',
                        array('tag' => 'li', 'escape' => false),
                        '<a href="#" class="btn btn-md blue">&laquo;</a>',
                        array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li',
                            'escape' => false));
                    //                    echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
                    ?>
                </ul>
            </div>
        </div>

        <div class="portlet-body ">
            <?php
            $lastListedNote = -1;
            if (!empty($noteNos)) {
                $noteGroupArray = array();

                foreach ($noteNos as $key => $value) {

                    if ($value['nothi_part_no'] == $id) {
                        $lastListedNote = $value['note_no_en'] > $lastListedNote
                            ? $value['note_no_en'] : $lastListedNote;
                    }
                }
            }
            ?>
            <div class="nothiDetailsPage " ata-always-visible="1" data-rail-visible1="1">
                <div class="notesheetview" id="<?php echo $nothi_note_sheets_id ?>">
                    <?php
                    $lastNoteNo = 0;
                    $i = 0;
                    $nothiPartNo = 0;
                    $note_count = ($notesquery->count());
                    if (!empty($notesquery->toArray())) {

                        foreach ($notesquery as $ke => $row) {
                            $nothiPartNo = $row['nothi_part_no_bn'];
                            if ($row['note_no'] >= 0) {
                                $lastNoteNo = $row['note_no'];
                            }
                            if(!empty($row->digital_sign)){
                                //verify digital sign
                                $sign_info = json_decode($row->sign_info,true);
                                $is_vefied_signature = verifySign($row['note_description'],'html',$sign_info['publicKey'],$sign_info['signature']);
                                if(empty($is_vefied_signature)){
                                    $is_vefied_signature = 1;
                                }
                            }
                            echo '<div ' . ($row['note_status'] == 'DRAFT' ? 'class="noteDetails"'
                                    : '') . ' id="note_' . $row['id'] . '">';
                            echo '<div class="noteContent noMarginPadding" id="' . $this->Number->format($row['note_no']) . '">';
                            if ($row['note_status'] == 'DRAFT' && ($row['potrojari_status']
                                    != 'Sent')) {
                                ?>
	                            <div class='pull-right  hidden-print'>
		                            <div class="btn-group btn-group-round">
			                            <button noteid='<?=$row['note_no'] ?>' class='btn btn-xs btn-primary btn-onucced-edit' title='অনুচ্ছেদ সম্পাদন করুন' data-title-orginal='অনুচ্ছেদ সম্পাদন করুন'><i class="fa fa-pencil"></i> সম্পাদন </button>
			                            <button noteid='<?=$row->note_no ?>' class='btn btn-xs btn-danger btn-onucced-delete delete' title='মুছে ফেলুন' data-title-orginal='মুছে ফেলুন'><i class="fa fa-trash"></i> মুছে ফেলুন </button>
		                            </div>
	                            </div>
	                            <?php
                            } else {

                            }

                            echo "<div class='printArea'>";
                            if (!empty($row['subject']))
                                echo '<div class="noteNo noteSubject"  notesubjectid=' . $row['id'] . '> বিষয়: <b>' . ($row['subject']) . '</b></div>';

                            if ($row['is_potrojari'] == 0){
                                if(!empty($row->digital_sign)){
                                    //verify digital sign
                                    if(empty($is_vefied_signature)){
                                        echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . enTobn($row->note_no) . '<i class="fa fa-times"  data-original-title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।"  title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।" style="position:  absolute;background-color: red;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;"></i></span>';
                                    }else{
                                        echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . enTobn($row->note_no) . '<span style="color: green"><i class="fs0 a2i_gn_onumodondelevery2"  data-original-title="ডিজিটাল সাইনকৃত।"  title="ডিজিটাল সাইনকৃত।" style="padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;"></i></span></span>';
                                    }
                                }else{
                                    echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . enTobn($row->note_no) . '</span>';
                                }
                            }
                            echo "<div class='noteDescription' id=" . $row['id'] . "  " . ($row['is_potrojari']
                                    ? "style='color:red;text-align:left!important;' " : '') . " >" . base64_decode($row['note_description']) . "</div>";

                            if (!empty($noteAttachmentsmap[$row['id']])) {

                                echo "<hr/><div class='table' style='font-size:11px;'><table class='table table-bordered table-stripped hidden-print'><tr><th colspan='4' class='text-left'>" . __(SHONGJUKTI) . "</th></tr>";

                                foreach ($noteAttachmentsmap[$row['id']] as $attachmentKey => $attachment) {
                                    if (empty($attachment['user_file_name'])) {
                                        $attachment['user_file_name'] = 'কোন নাম দেওয়া হয়নি';
                                    }
                                    $fileName = explode('/',
                                        $attachment['file_name']);
                                    echo "<tr>";
                                    echo "<td style='width:10%;'  class='text-center'>" . $this->Number->format($attachmentKey
                                            + 1) . "</td>";
                                    $user_file_name = htmlspecialchars($attachment['user_file_name']);
                                    $url_file_name = "{$attachment['id']}/showAttachment/{$id}";
//                                        $url_file_name=  "{$this->request->webroot}nothiNoteSheets/showAttachment?id={$attachment['id']}&n_id={$id}";
                                    echo "<td style='width:35%;' class='preview_attachment' noteid='{$row['note_no']}'><span class='preview'><a class='showforPopup' data-gallery user_file_name='{$user_file_name}' type='{$attachment['attachment_type']}'  download='{$attachment['file_name']}' href='{$url_file_name}' title='" . urldecode($fileName[count($fileName)
                                        - 1]) . "'>" . urldecode($fileName[count($fileName)
                                        - 1]) . "</a></span></td>";
                                    echo "<td style='width:35%; text-align:center; font-size:14px;' class='attachment_user_title'><span class='preview user_file_name'>{$attachment['user_file_name']}</span></td>";
                                    echo "<td style='width:20%;' class='text-center download-attachment'><a  href='" . $this->Url->build([
                                            "controller" => "nothiNoteSheets",
                                            "action" => "downloadNoteAttachment",
                                            $user_designation,
                                            "?" => ["id" => $attachment['id'],
                                                'nothimaster' => $id,
                                                'nothi_office' => $nothi_office]]) . "' title='ডাউনলোড'><i class='fa fa-download'></i></a>".(($attachment['digital_sign'] ==1)?'<span><i class="fa fa-check" style="background-color: green;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;" data-original-title="ডিজিটাল সাইনকৃত।" title="ডিজিটাল সাইনকৃত।"></i></span>':'').  "&nbsp;&nbsp;</td>";
                                    echo "</tr>";
                                }
                                echo "</table></div>";
                            }

                            if (isset($signatures[$row['id']])) {
                                $n = count($signatures[$row['id']]);
                                $col = 4;

                                $data = $signatures[$row['id']];
                                $finalArray = array();
                                $finalArray[0][0] = $data[0];

                                $designationSeqPrev = array();

                                $j = 0;
                                $rowC = 0;
                                $i = 1;

                                if (!isset($data[$i]['office_organogram_id'])) {
                                    $designationSeq = null;
                                } else {
                                    $designationSeq = isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']]) ? $employeeOfficeDesignation[$data[$i]['office_organogram_id']] : null;
                                }

                                if(isset($employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']])) {
                                    $designationSeqPrev = $employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']];
                                }else{
                                    $designationSeqPrev[0] = 0;
                                }


                                while ($i < $n) {

                                    if ($designationSeq[0] <= $designationSeqPrev[0]) {
                                        if ($j < $col - 1) {
                                            $finalArray[$rowC][++$j] = $data[$i];
                                        } else {
                                            $finalArray[++$rowC][$j] = $data[$i];
                                        }
                                    } else {
                                        $rowC++;
                                        if ($j > 0) {
                                            $finalArray[$rowC][--$j] = $data[$i];
                                        } else {
                                            $finalArray[$rowC][$j] = $data[$i];
                                        }
                                    }

                                    $i++;
                                    if (isset($data[$i]['office_organogram_id'])
                                        && isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']]))
                                        $designationSeq = $employeeOfficeDesignation[$data[$i]['office_organogram_id']];
                                    if (isset($data[$i]['office_organogram_id'])
                                        && isset($employeeOfficeDesignation[$data[$i
                                            - 1]['office_organogram_id']]))
                                        $designationSeqPrev = $employeeOfficeDesignation[$data[$i
                                        - 1]['office_organogram_id']];
                                }

                                if (!empty($finalArray)) {
                                    for ($findex = 0; $findex < sizeof($finalArray); $findex++) {
                                        echo "<div class='row' style='margin:0!important;padding:0!important'>";
                                        for ($fj = 0; $fj < $col; $fj++) {
                                            echo '<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 text-center" style="padding:2px;"><div style="line-height: 1.2!important;font-size: 10px; color: darkviolet; display: block; ' . (!empty($finalArray[$findex][$fj]['cross_signature']) ? ' text-decoration: line-through;' : '') . ' vertical-align: bottom;page-break-inside:auto!important;margin:0!important;padding:0!important; ">';
                                            if (isset($finalArray[$findex][$fj])) {
                                                echo "<div class='btn btn-block ' style='height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;'>";
                                                if (!empty($finalArray[$findex][$fj]['is_signature']) && empty($finalArray[$findex][$fj]['cross_signature'])) {
                                                    $englishdate = new \Cake\I18n\Time($finalArray[$findex][$fj]['signature_date'], "d-MM-y H:m:ss", null, null);
                                                    if(!empty($finalArray[$findex][$fj]['digital_sign'])){
                                                        if(empty($is_vefied_signature)){
                                                            echo '<i class="fa fa-times" style="left:5px;position: absolute;background-color: red;color: white;padding:  0px;border-radius:  5px;margin: 0px 1px;border: none;" data-original-title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।" title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।"></i>';
                                                        }else{
                                                            echo '<i class="fa fa-check" style="left:5px;position: absolute;background-color: green;color: white;padding:  0px;border-radius:  5px;margin: 0px 1px;border: none;" data-original-title="ডিজিটাল সাইনকৃত।" title="ডিজিটাল সাইনকৃত।"></i>';
                                                        }
                                                    }
                                                    echo '<img class="signatures" style="height: 40px!important;padding:0!important;margin:0!important;" data-signdate="'. $englishdate->i18nFormat("Y-M-d H:m:s", null, 'en-US') .'" data-id="' . $finalArray[$findex][$fj]['userInfo'] . '" data-token_id="' . sGenerateToken(['file'=>$finalArray[$findex][$fj]['userInfo']],['exp'=>time() + 60*300]) . '" alt="সাইন যোগ করা হয়নি" />';
                                                } else {
                                                    echo '<img class="img-responsive" style="height: 40px!important;padding:0!important;margin:0!important; visibility: hidden;" />';
                                                }
                                                echo "</div>";

                                                echo "<span style='border-top:1px solid #9400d3;font-size: 8pt;display: block;'>" . $this->Time->format(
                                                        $finalArray[$findex][$fj]['signature_date'], "d-MM-y H:m:ss", null, null) . "</span><span style='font-size: 8pt;display: block;'>" . h($finalArray[$findex][$fj]['name']) . "</span><span style='font-size: 8pt;display: block;'>" . $finalArray[$findex][$fj]['employee_designation'] . "</span>";
                                            } else
                                                echo "<div class='btn btn-block ' style='height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;'>&nbsp;</div>";
                                            echo '</div></div>';
                                        }
                                        echo "</div>";
                                    }
                                }
                            }
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                        }
                    } else {
                        echo "<p class='text-center text-danger'>এই পেজ এ কোন অনুচ্ছেদ দেয়া হয়নি</p>";
                    }
                    ?>
                </div>
                <?php
                if ($privilige_type == 1):
                    if (count($noteNos) == 0 || (($lastNoteNo) == $lastListedNote) || empty($notesquery->toArray())) {
                        ?>
                        <br/>
	                    <div class="btn-group btn-group-round">
	                        <a id="addNewNote" data-original-title="নতুন অনুচ্ছেদ তৈরি করুন" title="নতুন অনুচ্ছেদ তৈরি করুন"
	                           class="btn green margin-right-10 margin-bottom-10" style="padding: 3px 5px !important;border-width: 1px;">
	                            <i class="fs1 a2i_gn_note2"></i> নতুন অনুচ্ছেদ
	                        </a>
							<?php if ($otherNothi == false && $nisponno == 0 && count($nothimovement) > 1 && $employee_office['office_unit_organogram_id'] == $nothiInformation['office_units_organogram_id']): ?>
			                    <a title=" নোট নিষ্পন্ন "
			                       nothi_master_id="<?php echo $id ?>"
			                       nothi_office="<?php echo $nothi_office ?>"
			                       class="btn blue btn-nisponno-nothi"><i
						                    class="fs1 a2i_gn_onumodondelevery3"></i> নোট
				                    নিষ্পন্ন </a>
							<?php endif; ?>
	                    </div>

                    <?php } ?>
                    <div class="portlet box purple" id="responsiveNoteEdit">
                        <div class="portlet-body">

                            <input type="hidden" id="noteId"/>
                            <input type="hidden" id="sendsaveModal"/>
                            <!--                            <div class="row">
                                                            <div class="col-md-12 form-group">
                                                                <button  class="btn green btn-lg green saveNote" hasnotorno="<?php echo $lastListedNote; ?>">সংরক্ষণ </button>&nbsp;<button class="btn btn-lg btn-closeNote btn-danger" data-original-title="বন্ধ করুন" title="বন্ধ করুন" onclick="editBoxClose();">  মুছে ফেলুন </button>
                                                            </div>
                                                        </div>-->

                            <div class="row notesubjectdiv <?php
                            echo ((!isset($nothi_note_sheets_id) || $nothi_note_sheets_id
                                    == 1) && ($lastListedNote < 0)) ? '' : 'hidden'
                            ?>">
                                <div class="col-md-2 col-sm-3 form-group">
                                    <label>বিষয়</label>
                                </div>
                                <div class="col-md-10 col-sm-7 form-group ">
                                    <input type="text" name="subject" id="notesubject" class="form-control input-sm"/>
                                </div>
                            </div>

                            <div class="row">`
                                <div class="col-md-12 form-group">
                                    <textarea class="noteComposer" id="noteComposer" name="description"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <button class="btn green btn-lg green saveNote"
                                            hasnotorno="<?php echo $lastListedNote; ?>">সংরক্ষণ করুন
                                    </button>&nbsp;<button class="btn btn-lg btn-closeNote btn-danger"
                                                           data-original-title="বন্ধ করুন" title="বন্ধ করুন"
                                                           onclick="editBoxClose();"> বন্ধ করুন
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <hr/>
            </div>
        </div>
    </div>
</div>
<!-- Subject Modal -->
<div id="subjectModal" class="modal fade modal-purple height-auto" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">নোটের বিষয় লিখুন</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2 col-sm-3 form-group">
                        <label>বিষয়</label>
                    </div>
                    <input title="আন্ডারলাইন" type="checkbox" id="underline" name="underline" value="underline" class="pull-right hidden" />
                    <div class="col-md-10 col-sm-9 form-group ">
                        <div class="input-group">
                        <input type="text" name="subject" id="notesubjectnew" class="form-control input-sm"/>
                        <label for="underline" id="underline_label" title="আন্ডারলাইন" class="input-group-addon" style="background-color: white;border: solid 1px #cacaca;padding: 0;color: black;cursor:pointer;"><span><i class="fa fa-underline"></i></span></label>
                        </div>
                    </div>
                </div>
                        <div class="row">
                            <div class="col-md-2 col-sm-3"></div>
                            <div class="col-md-3 col-sm-4">
                                <button type="button" class="btn btn-info" id="saveSubject">পরবর্তী ধাপে যান</button>

                            </div>

                        </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>

    </div>
</div>
<form action="<?= $this->request->webroot ?>apiOnucched/<?= $id ?>/<?= $nothi_office ?>" method="post" id="apionucched">
    <input type="hidden" name="nothimasterid" value="<?= $id; ?>">
    <input type="hidden" name="notesheetid" value="<?= $nothi_note_sheets_id ?>">
    <input type="hidden" name="notesubject" id="nothi-note-subject" value="">
    <input type="hidden" name="user_designation" value="<?= $user_designation ?>">
    <input type="hidden" name="api_key" value="<?= $apikey ?>">
    <input type="hidden" name="data_ref" value="api">
    <input type="hidden" name="noteno" value="0">
    <input type="hidden" name="note-id" id="note-id" value="0">
    <input type="hidden" name="height" id="height" value="<?= $height ?>">
    <input type="hidden" name="width" id="width" value="<?= $width ?>">

</form>

<script>
    var signature = '<?=!empty($employee_office['default_sign'])?$employee_office['default_sign']:0?>';
    var part_no = '<?=$id?>';
    $(document).ready(function(){
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-center"
        };
    });

    var numbers = {
        1: '১',
        2: '২',
        3: '৩',
        4: '৪',
        5: '৫',
        6: '৬',
        7: '৭',
        8: '৮',
        9: '৯',
        0: '০'
    };

    function replaceNumbers(input) {
        var output = [];
        for (var i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }

    $(function () {
        $('select').select2('destroy')

        $.each($('.signatures'), function (i, v) {
            var imgurl = $(v).data('id');
            var token = $(v).data('token_id');
            var signDate = $(v).data('signdate');
            PROJAPOTI.ajaxSubmitAsyncDataCallbackWithoutAbrot(js_wb_root + 'getSignature/' + imgurl + '/1/' + signDate+'?token='+token, {}, 'html', function (signature) {
                $(v).attr('src', signature);
            });

        });
        <?php if ($privilige_type == 1): ?>

        $(document).on('click', '.btn-onucced-edit', function () {
            var notesubject = $(this).closest('.noteDetails').find('.noteSubject b').text();
            $("#nothi-note-subject").val(notesubject);
            // $("#editor_height").val(editor_height);
            // $("#editor_width").val(editor_width);
            $("#note-id").val($(this).closest('.noteDetails').find('.noteDescription').attr('id'));
            $("#apionucched").submit();
            return;
        });

		$(document).on('click', '.btn-onucced-delete', function () {
			var noteid = $(this).attr('noteid');
			bootbox.dialog({
				message: "আপনি কি অনুচ্ছেদটি বাতিল করতে ইচ্ছুক?",
				title: "অনুচ্ছেদ বাতিল",
				buttons: {
					success: {
						label: "হ্যাঁ",
						className: "green",
						callback: function () {
							deleteNote(noteid);
						}
					},
					danger: {
						label: "না",
						className: "red",
						callback: function () {

						}
					}
				}
			}).find('.modal-dialog').css('top', (($(this).offset().top - 210) > 0) ? $(this).offset().top - 210 : 0);

		});

		function deleteNote(id) {
            if (typeof (id) != 'undefined' || id != 0) {
				$.ajax({
					data: {nothimasterid: <?php echo $id; ?>, noteid: id,
                        nothi_office:<?php echo $nothi_office ?>,
						user_designation:<?= $user_designation ?>,
						api_key:'<?= $apikey ?>',
						data_ref: 'api',
                    },
					method: 'POST',
					dataType: 'JSON',
					url: '<?php
                        echo $this->Url->build(['_name' => 'apiNoteDelete']);
                        ?>',
					success: function (msg) {

						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right"
						};

						if (msg.status == 'success') {
							window.location.reload();
						} else {
							toastr.error(msg.msg);
						}
					}
				});
			} else {
				toastr.error(' দুঃখিত কিছুক্ষণ পর আবার চেষ্টা করুন। ');
			}
		}

        $(document).on('click', '#saveSubject', function () {

            var notesubject = $('#notesubjectnew').val();

            if (isEmpty(notesubject)) {
                toastr.error('কোনো বিষয় দেয়া হয়নি');
                $('#notesubjectnew').focus();
                return;
            }
            if($("#underline").is(":checked")){

                $("#nothi-note-subject").val('<u>'+notesubject+'</u>');

            } else{
                $("#nothi-note-subject").val(notesubject);

            }
            // $("#editor_height").val(editor_height);
            // $("#editor_width").val(editor_width);
            $("#apionucched").submit();
            return;
        });

        $(document).on('click', '#addNewNote', function () {
            Metronic.blockUI('.nothiDetailsPage');
            var notesubject = $('.notesubject').val();
            $("#nothi-note-subject").val(notesubject);
            // $("#editor_height").val(editor_height);
            // $("#editor_width").val(editor_width);
            $("#apionucched").submit();
            return;
        });

        Metronic.init();
        $('li').tooltip({'placement':'bottom'});
        <?php endif; ?>
    });

    function getPopUpPotro(href, title) {
        Metronic.blockUI({
            boxed: true
        });

        $('#responsiveOnuccedModal').find('.modal-title').text('');
        $('#responsiveOnuccedModal').find('.modal-body').html('');
        PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + href+'?nothi_part='+'<?= $id ?>'+'&token='+'<?= sGenerateToken(['file' => $id, 'office_id'=>$employee_office['office_id'], 'office_unit_id'=>$employee_office['office_unit_id'], 'office_unit_organogram_id'=>$employee_office['office_unit_organogram_id']], ['exp' => time() + 60 * 300]) ?>', {'nothi_office':<?php echo $nothi_office ?>}, 'html', function (response) {
            $('#responsiveOnuccedModal').modal('show');
            if(title.length > 25){
                title = title.slice(0,21)+'...';
            }
            $('#responsiveOnuccedModal').find('.modal-title').text(title);

            $('#responsiveOnuccedModal').find('.modal-body').html(response);
            Metronic.unblockUI();
        });

    }

    $(document).off('click', '.showforPopup').on('click', '.showforPopup', function (e) {
        e.preventDefault();
        var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
        getPopUpPotro($(this).attr('href'), title);
    });


    $('a[title="নোট"]').click(function (ev) {
        ev.preventDefault();
    });

    function hasUnfinishedOnucched() {
        // return true if "নতুন অনুচ্ছেদ" button exist
        if ($('#addNewNote').is(":visible") == true) {
            // MyAndroid.receiveValueFromJs(false);
            return;
        }
        // return true if "সংরক্ষণ" button exist
        if ($('.saveNote').is(":visible") == true) {
            if ( ($('#noteComposer').froalaEditor('html.get') == "") || ($('#noteComposer').froalaEditor('html.get') == "<br><br>")) {
                // MyAndroid.receiveValueFromJs(false);
                return;
            }
            if ($('.saveNote').attr('hasnotorno') == -1) {
                toastr.error('বিষয় দেয়া হয়নি');
                // MyAndroid.receiveValueFromJs(false);
                return;
            }
        }
        //MyAndroid.receiveValueFromJs(true);
    }
    function emptyOnucchedCheck(){
        if($('.nothiDetailsPage ').length<1){
            //MyAndroid.receiveValueFromJs(true);
        } else
        {
            //MyAndroid.receiveValueFromJs(false);
        }
    }
    var editor_height = 400;
    var editor_width = 300;
    <?php
    if($can_input_new_onucched == 1){
    ?>
    $(document).ready(function () {
        var note_count = <?= $note_count ?>;
        if(isEmpty(note_count)){
            $("#subjectModal").modal('show');
        }
    });

    <?php } ?>

    function updateEditorHeightWidth(height,width) {
        editor_height = height;
        editor_width = width;
    }

    $(document).off('click', ".btn-nisponno-nothi").on('click', ".btn-nisponno-nothi", function (e) {
        Metronic.blockUI({
            target: '.page-container',
            boxed: true
        });

        var nothimasters = $(this).attr('nothi_master_id');
        var message = 'আপনি কি নোট নিষ্পন্ন করতে ইচ্ছুক?';
        if ($('.btn-send-draft-cron').length > 0) {
            message = 'আপনি কি পত্রটি জারি না করে নোট নিষ্পন্ন করতে ইচ্ছুক?';
        }
        bootbox.dialog({
            message: message,
            title: "নোট নিষ্পন্ন",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        if(signature > 0){
                            getSignatureToken('NoteNisponno',nothimasters);
                        }else{
                            NoteNisponno(nothimasters);
                        }

                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                        Metronic.unblockUI('.page-container');
                    }
                }
            }
        }).find('.modal-dialog').css('top', $(this).offset().top - 210);

    });

    function NoteNisponno(nothimasters) {
        var data = {
            'selectednothi' : nothimasters,
	        'api_key' : "<?=$this->request->query['api_key']?>",
	        'user_designation' : <?=$this->request->query['user_designation']?>,
        };
        if(signature > 0){
            data.soft_token = $("#soft_token").val();
        }
        PROJAPOTI.ajaxSubmitDataCallback(((!isEmpty(signature) && signature > 0)?ds_url:js_wb_root)+'NothiMasterMovements/finishNothiMaster', data, 'json',
            function (response) {
            if (response.status == 'success') {
                toastr.success(response.msg);
                window.location.reload();
            } else {
                toastr.error(response.msg);
                Metronic.unblockUI('.page-container');
            }

        });

    }

    $(document).off('change', '#underline').on('change', '#underline', function () {
        showSubjectStyleUpdate();
    });

    function showSubjectStyleUpdate(){
        if($("#underline").is(":checked")){
            $('#underline_label').css('background-color', '#c1c1c1');
        } else{
            $('#underline_label').css('background-color', '#ffffff');
        }
    }

    function getSignatureToken(functionName,el1,el2,el3){
        if(signature > 0)
        {
            $("#soft_token").focus();
            $("#soft_token").val(0);
            var promise = function () {
                return new Promise(function (resolve, reject) {
                    if (signature == 1) {
                        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'employeeRecords/checkUserSoftToken',{'api_key' : '<?= $apikey ?>','employee_record_id' : '<?=$employee_office['officer_id'] ?>'},'json',function(result){
                            if(!isEmpty(result) && !isEmpty(result.status)){
                                if(result.status == 'success'){
                                    var str = '<div class="col-md-12 row"><div class="col-md-2 col-sm-2"><label class="control-label pull-right">সফট টোকেনঃ </label></div><div class="col-md-8 col-sm-8"><input type="password" class="form-control" id="soft_token" readonly></div></div><div class="col-md-12 row text-center font-green">'+(!isEmpty(result.message)?(result.message):'') +'</div>';
                                    setTimeout(function(){
                                        $("#soft_token").val((!isEmpty(result.sign_token)?result.sign_token:''));
                                    },1000);
                                    resolve(str);
                                }else{
                                    var str ='<div class="col-md-12 col-sm-12 col-xs-12"><label class="control-label">নিম্নে সফট টোকেনটি সরবরাহ করুন </label></div><div class="col-md-12 col-sm-12  col-xs-12"><input type="password" class="form-control" id="soft_token" ></div><br><br><hr><br><div class="col-md-12 row">যদি সফট টোকেন দিয়ে সাইন সম্ভব না হয় তবে মোবাইল অ্যাপ এ হোমপেজ থেকে  <b>স্বাক্ষর ধরন</b> পরিবর্তন করে নিন। </div>';
                                    resolve(str);
                                }
                            }else{
                                reject('Something went wrong.Code 1');
                            }
                        });

                    } else {
                        var str ='<div class="col-md-12 col-sm-12  col-xs-12"><b>হার্ড টোকেন</b> নির্বাচন করায় প্রতি স্বাক্ষরের সময় আপনাকে ডিজিটাল <b>হার্ড টোকেন</b> এর সহযোগিতা নিতে হবে। যদি হার্ড টোকেন দিয়ে সাইন সম্ভব না হয় তবে মোবাইল অ্যাপ এ হোমপেজ থেকে  <b>স্বাক্ষর ধরন</b>  পরিবর্তন করে নিন। নিরাপত্তা নিশ্চিতকরণের জন্য দুইবার টোকেন চাওয়া হতে পারে। <input type="hidden" class="form-control" id="soft_token">  </div>';
                        resolve(str);
                    }
                });
            };

            promise().then(function (str) {
                bootbox.dialog({
                    message: str,
                    title: 'ডিজিটাল সিগনেচার',
                    buttons: {
                        success: {
                            label: ((functionName == 'forwardNothi')?'প্রেরণ':(functionName == 'potroApproval')?'খসড়া অনুমোদন':(functionName == 'makePotrojariRequest')?'পত্রজারি':(functionName == 'goForDraftSave')?'খসড়া সংশোধন':(functionName == 'NoteNisponno')?' নোট নিষ্পন্ন':(functionName == 'apiMakePotrojariRequest')?'পত্রজারি':(functionName == 'apiPotroApporval')?'খসড়া অনুমোদন':''),
                            className: "green",
                            callback: function () {
                                if(functionName == 'forwardNothi'){
                                    NothiMasterMovement.forwardNothiFunction(el1);
                                }
                                else if(functionName == 'potroApproval'){
                                    potroApproval(el1,el2);
                                }
                                else if(functionName == 'makePotrojariRequest'){
                                    makePotrojariRequest(el1,el2);
                                }
                                else if(functionName == 'goForDraftSave'){
                                    DRAFT_FORM.goForDraftSave();
                                }
                                else if(functionName == 'NoteNisponno'){
                                    NoteNisponno(el1);
                                }
                                else if(functionName == 'apiMakePotrojariRequest'){
                                    apiMakePotrojariRequest(el1,el2,el3);
                                }
                                else if(functionName == 'apiPotroApporval'){
                                    apiPotroApporval(el1,el2);
                                }
                            }
                        },
                        proceedWithOutSignature: {
                            label: 'ডিজিটাল সাইন ব্যতিত '+((functionName == 'forwardNothi')?'প্রেরণ':(functionName == 'potroApproval')?'খসড়া অনুমোদন':(functionName == 'makePotrojariRequest')?'পত্রজারি':(functionName == 'goForDraftSave')?'খসড়া সংশোধন':(functionName == 'NoteNisponno')?' নোট নিষ্পন্ন':(functionName == 'apiMakePotrojariRequest')?'পত্রজারি':(functionName == 'apiPotroApporval')?'খসড়া অনুমোদন':''),
                            className: "blue",
                            callback: function () {
                                $("#soft_token").val('-1');
                                if(functionName == 'forwardNothi'){
                                    NothiMasterMovement.forwardNothiFunction(el1);
                                }
                                else if(functionName == 'potroApproval'){
                                    potroApproval(el1,el2);
                                }
                                else if(functionName == 'makePotrojariRequest'){
                                    makePotrojariRequest(el1,el2);
                                }
                                else if(functionName == 'goForDraftSave'){
                                    DRAFT_FORM.goForDraftSave();
                                }
                                else if(functionName == 'NoteNisponno'){
                                    NoteNisponno(el1);
                                }
                                else if(functionName == 'apiMakePotrojariRequest'){
                                    apiMakePotrojariRequest(el1,el2,el3);
                                }
                                else if(functionName == 'apiPotroApporval'){
                                    apiPotroApporval(el1,el2);
                                }
                            }
                        },
                        danger: {
                            label: "বন্ধ করুন",
                            className: "red",
                            callback: function () {
                                if(functionName == 'potroApproval'){
                                    if ($(".approveDraftNothi").is(':checked') == false) {
                                        $(".approveDraftNothi").attr('checked', 'checked');
                                        $(".approveDraftNothi").closest('span').addClass('checked');
                                    }
                                    else {
                                        $(".approveDraftNothi").removeAttr('checked');
                                        $(".approveDraftNothi").closest('span').removeClass('checked');
                                    }
                                }
                                Metronic.unblockUI('.page-container');
                                Metronic.unblockUI('#ajax-content');
                            }
                        }
                    }
                });
                return;
            }).catch (function (error) {
                console.log('Error: ', error);
            });

        }else{
            toastr.error('দুঃখিতঃ কোন ফাংশন নির্বাচন করা হয়নি।');
        }
    }
</script>
