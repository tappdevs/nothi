<?php

use Cake\I18n\Time;

$bookMarkLi = '';
$currentLi = array();
?>

<style>
    .nothiGroupView.dropdown-menu {
        width: 50px !important;
        min-width: 55px;
        box-shadow: none;
    }

    .MsoTableGrid {
        margin-left: 0pt !important;
    }

    @media print {
        a[href]:after {
            content: none !important;
        }

        .notesheetview {
            line-height: 1.4;
            margin: 0px !important;
            padding: 0px !important;
        }

        .notesheetview .noteContent {
            padding: 2px !important;
        }

        @page {
            margin-top: 1in;
            margin-left: 1.2in;
            margin-bottom: .75in;
            margin-right: .75in;
        }

        body {
            margin-top: 1in;
            margin-left: 1.2in;
            margin-bottom: .75in;
            margin-right: .75in;
        }

        .notesheetview p {
            margin: 0px 0px 5px !important;
        }

        .notesheetview .btn-block {
            margin-bottom: 2px !important;
        }

        .row > div {
            padding: 0px !important;
        }
    }
</style>
<div class="portlet  light">
    <div class="portlet-title" style="border-bottom: 1px solid #eee;">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <ul class="pager">
                    <li style="cursor: pointer;" id="noteLeftMenuToggle" title="সব অনুচ্ছেদ দেখুন"
                        data-original-title="সব অনুচ্ছেদ দেখুন">
                        <i class="fs0 a2i_gn_view1" style="color:green;"></i>
                    </li>
                    <li>
                        <!--<div class='pull-right'>-->
                        <button data-original-title="প্রিন্ট করুন" title="প্রিন্ট করুন" class='btn-notesheet-print' style="border:none !important;"><i class='fs1 a2i_gn_print2'></i></button>
                        <!--</div>-->
                    </li>
                </ul>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 text-right">
                <ul class="pagination pagination-sm">
                    <?php
                    echo $this->Paginator->last(__('শেষ', true),
                        array('class' => 'number-last'));
                    echo $this->Paginator->next('<<',
                        array('tag' => 'li', 'escape' => false),
                        '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
                        array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li',
                        'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a',
                        'reverse' => true));
                    echo $this->Paginator->prev('>>',
                        array('tag' => 'li', 'escape' => false),
                        '<a href="#" class="btn btn-sm blue">&laquo;</a>',
                        array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li',
                            'escape' => false));
                    echo $this->Paginator->first(__('প্রথম', true),
                        array('class' => 'number-first'));
                    ?>
                </ul>
            </div>
        </div>

    </div>
    <div class="portlet-body ">
        <div class="row">

            <div class="col-lg-0 col-md-0 col-sm-0 col-xs-0 noteLeftMenu" style="display: none;">
                <?php
                $lastListedNote = -1;
                if (!empty($noteNos)) {
                    echo '<div class="scroller" style="height: 700px;" data-always-visible="1" data-rail-visible1="1">';
                    echo '<div class="btn-group-vertical">';
                    $noteGroupArray = array();

                    foreach ($noteNos as $key => $value) {

                        if ($value['nothi_part_no'] == $id) {
                            $lastListedNote = $value['note_no_en'] > $lastListedNote
                                ? $value['note_no_en'] : $lastListedNote;
                        }

                        $noteGroupArray[$value['nothi_part_no_en']][] = '<li>
				<a href=' . $value['id'] . ' title="অনুচ্ছেদ" class="nothiOnucched showforPopup btn btn-default " partno=' . $value['nothi_part_no_bn'] . ' nothiMasterId = ' . $value['nothi_notesheet_id'] . '  nothisheetsId = ' . $value['nothi_notesheet_id'] . ' notenoen=' . $value['note_no_en'] . ' nothi_part=' . $value['nothi_part_no'] . ' noteno=' . $value['note_no'] . '>' . $value['nothi_part_no_bn'] . '.' . $value['note_no'] . '</a>
			</li>';
                    }

                    foreach ($noteGroupArray as $ky => $v) {

                        echo '<div class="btn-group">';
                        echo '<button id="btnGroupVerticalDrop' . $ky . '" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> ' . $this->Number->format($ky) . ' <i class="glyphicon glyphicon-chevron-down"></i> </button>';
                        echo '<ul class="nothiGroupView dropdown-menu" role="menu" aria-labelledby="btnGroupVerticalDrop' . $ky . '">';
                        foreach ($v as $groupNoteKey => $groupNoteValue) {
                            echo $groupNoteValue;
                        }
                        echo '</ul> </div>';
                    }

                    echo "</div></div>";
                }
                ?>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noteContentSide">
                <div class="nothiDetailsPage ">
                    <div class="notesheetview" id="<?php echo $nothi_note_sheets_id ?>">
                        <?php
                        $lastNoteNo = 0;
                        $i = 0;
                        $noteslist = $notesquery->toArray();

                        if (!empty($noteslist)) {

                            foreach ($notesquery as $row) {
                                if ($row->note_no >= 0) {
                                    $lastNoteNo = $row->note_no;
                                }
                                   if(!empty($row->digital_sign)){
                                        //verify digital sign
                                           $sign_info = json_decode($row->sign_info,true);
                                           $is_vefied_signature = verifySign($row['note_description'],'html',$sign_info['publicKey'],$sign_info['signature']);
                                       if(empty($is_vefied_signature)){
                                           $is_vefied_signature = 1;
                                       }
                                    }
                                echo '<div ' . ($row['note_status'] == 'DRAFT' ? 'class="noteDetails"' : '') . ' id="note_' . $row->id . '">';
                                echo '<div class="noteContent noMarginPadding" id="' . $this->Number->format($row->note_no) . '">';
                                if ($row['note_status'] == 'DRAFT' && ($row['potrojari_status'] != 'Sent')) {
                                    echo "<div class='pull-right  hidden-print'>" . ($otherNothi
                                        == false ? ('<button  title="পত্রের খসড়া তৈরি করুন"  url="' . $this->Url->build(['controller' => 'potrojari',
                                                'action' => 'makeDraft', $row['nothi_part_no'],
                                                $row['id'], 'note']) . '" class="btn btn-xs green  btn-onucced-potrojari potroCreate" data-title-orginal="পত্রের খসড়া তৈরি করুন"> <i class="fs1 a2i_nt_dakdraft4"></i> </button> &nbsp;&nbsp;')
                                            : '') . "<button noteid='{$row->note_no}'
                                        class='btn-onucced-edit   btn btn-xs btn-primary' title='অনুচ্ছেদ সম্পাদন করুন' data-title-orginal='অনুচ্ছেদ সম্পাদন করুন'  ><i class='fs1 a2i_gn_edit2'></i> </button> &nbsp;&nbsp;
<button noteid='{$row->note_no}' class='btn-onucced-delete  delete btn btn-xs btn-danger' title='মুছে ফেলুন' data-title-orginal='মুছে ফেলুন'><i style='vertical-align:baseline!important;' class='fs1 a2i_gn_delete2 '></i> </button>
</div>";
                                }

                                echo "<div class='printArea'>";
                                if (!empty($row->subject))
                                    echo '<div class="noteNo noteSubject"  notesubjectid=' . $row->id . '> বিষয়: <b>' . ($row->subject) . '</b></div>';

                                       if ($row->is_potrojari == 0){
                                            if(!empty($row->digital_sign)){
                                                //verify digital sign
                                                if(empty($is_vefied_signature)){
                                                    echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . $this->Number->format($row->note_no) . '<i class="fa fa-times"  data-original-title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।"  title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।" style="position:  absolute;background-color: red;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;"></i></span>';
                                                }else{
                                                    echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . $this->Number->format($row->note_no) . '<i class="a2i_gn_approval2"  data-original-title="ডিজিটাল সাইনকৃত।"  title="ডিজিটাল সাইনকৃত।" style="position:  absolute;background-color: red;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;"></i>ডিজিটাল সাইনকৃত</span>';
                                                }

                                            }else{
                                                 echo '<span class="noteNo"> অনুচ্ছেদ: ' . $row->nothi_part_no_bn . '.' . $this->Number->format($row->note_no) . '</span>';
                                            }
                                        }
                                echo "<div class='noteDescription' id=" . $row->id . "  " . ($row->is_potrojari
                                        ? "style='color:red;'" : '') . " >" . base64_decode($row->note_description) . "</div>";

                                if (!empty($noteAttachmentsmap[$row->id])) {

                                    echo "<hr/><div class='table' style='font-size:11px;'><table class='table table-bordered table-stripped hidden-print'><tr><th colspan='4' class='text-left'>" . __(SHONGJUKTI) . "</th></tr>";

                                    foreach ($noteAttachmentsmap[$row->id] as $attachmentKey => $attachment) {
                                        if (empty($attachment['user_file_name'])) {
                                            $attachment['user_file_name'] = 'কোন নাম দেওয়া হয়নি';
                                        }
                                        $fileName = explode('/',
                                            $attachment['file_name']);
                                        echo "<tr>";
                                        echo "<td style='width:10%;'  class='text-center'>" . $this->Number->format($attachmentKey
                                                + 1) . "</td>";
                                        $user_file_name = htmlspecialchars($attachment['user_file_name']);
                                        $url_file_name = "{$attachment['id']}/showAttachment/{$id}";

                                        echo "<td style='width:35%;' class='preview_attachment' noteid='{$row->note_no}'><span class='preview'><a class='showforPopup' data-gallery user_file_name='{$user_file_name}' type='{$attachment['attachment_type']}'  download='{$attachment['file_name']}' href='{$url_file_name}' title='" . urldecode($fileName[count($fileName)
                                            - 1]) . "'>" . urldecode($fileName[count($fileName)
                                            - 1]) . "</a></span></td>";
                                        echo "<td style='width:35%; text-align:center; font-size:14px;' class='attachment_user_title'><span class='preview user_file_name'>{$attachment['user_file_name']}</span></td>";
                                        echo "<td style='width:20%;' class='text-center download-attachment'><a  href='" . $this->Url->build([
                                                "controller" => "nothiNoteSheets",
                                                "action" => "downloadNoteAttachment",
                                                "?" => ["id" => $attachment['id'], 'nothimaster' => $id,
                                                    'nothi_office' => $nothi_office]]) . "' title='ডাউনলোড'><i class='fa fa-download'></i></a> &nbsp;&nbsp;
" . ($row['note_status'] == 'DRAFT' ? "<a class='deleteAttachment' data-note-sheet-id='{$nothi_note_sheets_id}' data-note-id='{$row->id}' data-attachment-id='{$attachment['id']}' ><i style='vertical-align:baseline!important;' class='fs1 a2i_gn_delete2 '></i></a>"
                                                : "") . "</td>";
                                        echo "</tr>";
                                    }
                                    echo "</table></div>";
                                }

                                if (isset($signatures[$row['id']])) {
                                    $n = count($signatures[$row['id']]);
                                    $col = 4;

                                    $data = $signatures[$row['id']];
                                    $finalArray = array();
                                    $finalArray[0][0] = $data[0];

                                    $designationSeqPrev = array();

                                    $j = 0;
                                    $rowC = 0;
                                    $i = 1;

                                    if (!isset($data[$i]['office_organogram_id'])) {
                                        $designationSeq = null;
                                    } else {
                                        $designationSeq = isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']]) ? $employeeOfficeDesignation[$data[$i]['office_organogram_id']] : null;
                                    }

                                    if(isset($employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']])) {
                                        $designationSeqPrev = $employeeOfficeDesignation[$data[$i - 1]['office_organogram_id']];
                                    }else{
                                        $designationSeqPrev[0] = 0;
                                    }

                                    while ($i < $n) {

                                        if ($designationSeq[0] <= $designationSeqPrev[0]) {
                                            if ($j < $col - 1) {
                                                $finalArray[$rowC][++$j] = $data[$i];
                                            } else {
                                                $finalArray[++$rowC][$j] = $data[$i];
                                            }
                                        } else {
                                            $rowC++;
                                            if ($j > 0) {
                                                $finalArray[$rowC][--$j] = $data[$i];
                                            } else {
                                                $finalArray[$rowC][$j] = $data[$i];
                                            }
                                        }

                                        $i++;
                                        if (isset($data[$i]['office_organogram_id'])
                                            && isset($employeeOfficeDesignation[$data[$i]['office_organogram_id']])
                                        )
                                            $designationSeq = $employeeOfficeDesignation[$data[$i]['office_organogram_id']];
                                        if (isset($data[$i]['office_organogram_id'])
                                            && isset($employeeOfficeDesignation[$data[$i
                                                - 1]['office_organogram_id']])
                                        )
                                            $designationSeqPrev = $employeeOfficeDesignation[$data[$i
                                            - 1]['office_organogram_id']];
                                    }

                                    if (!empty($finalArray)) {
                                        for ($findex = 0; $findex < sizeof($finalArray); $findex++) {
                                            echo "<div class='row' style='margin:0!important;padding:0!important;'>";
                                            for ($fj = 0; $fj < $col; $fj++) {
                                                echo '<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 text-center" style="padding:0px;"><div style="font-size: 11pt; color: darkviolet; display: block; ' . (!empty($finalArray[$findex][$fj]['cross_signature'])
                                                        ? ' text-decoration: line-through;'
                                                        : '') . ' vertical-align: bottom;page-break-inside:auto!important;margin:0!important;padding:0!important;">';
                                                if (isset($finalArray[$findex][$fj])) {
                                                    echo "<div class='btn btn-block ' style='height:45px;margin:0!important;padding:1px 0px!important;'>";
                                                    if (!empty($finalArray[$findex][$fj]['is_signature'])
                                                        && empty($finalArray[$findex][$fj]['cross_signature'])
                                                    ) {
                                                        $englishdate = new Time($finalArray[$findex][$fj]['signature_date']);
                                                          if(!empty($finalArray[$findex][$fj]['digital_sign'])){
                                                                if(empty($is_vefied_signature)){
                                                                      echo '<i class="fa fa-times" style="position:  absolute;background-color: red;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;" data-original-title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।" title="ডিজিটাল সাইন নিশ্চিত সম্ভব হয়নি।"></i>';
                                                                }else{
                                                                      echo '<i class="fa fa-check" style="position:  absolute;background-color: green;color: white;padding:  2px;border-radius:  5px;margin: 2px;border:  solid 1px white;" data-original-title="ডিজিটাল সাইনকৃত।" title="ডিজিটাল সাইনকৃত।"></i>';
                                                                }
                                                          }
                                                        echo '<img class=" signatures" style="height: 40px!important;padding:0!important;margin:0!important;" data-signdate="' . $englishdate->i18nFormat("Y-M-d H:m:s", null, 'en-US') . '" data-id="' . $finalArray[$findex][$fj]['userInfo'] . '" alt="সাইন যোগ করা হয়নি" /><hr style="margin: 0!important;" />';
                                                    } else {
                                                        echo '<img class="img-responsive" style="height: 40px!important;padding:0!important;margin:0!important; visibility: hidden;" /><hr style="margin: 0!important;" />';
                                                    }
                                                    echo "</div>";

                                                    echo $this->Time->format(
                                                            $finalArray[$findex][$fj]['signature_date'],
                                                            "d-M-y H:m:ss", null,
                                                            null
                                                        ) . "<br/>" . h($finalArray[$findex][$fj]['name']) . "<br/>" . $finalArray[$findex][$fj]['employee_designation'];
                                                } else
                                                    echo "<div class='btn btn-block ' style='height:45px;margin:0!important;padding:1px 0px!important;'>&nbsp;</div>";
                                                echo '</div></div>';
                                            }
                                            echo "</div>";
                                        }
                                    }
                                }
                                echo '</div>';
                                echo '</div>';
                                echo '</div>';
                            }
                        } else {
                            if (!isset($lastListedNote) || $lastListedNote != -1) {
                                echo "<p class='text-center text-danger'>এই পেজ এ কোন অনুচ্ছেদ দেয়া হয়নি</p>";
                            }
                        }
                        ?>
                    </div>
                    <?php
                    if ($privilige_type == 1):
                        if (count($noteNos) == 0 || (($lastNoteNo) == $lastListedNote)) {
                            ?>
                            <br/>
                            <a id="addNewNote" data-original-title="নতুন অনুচ্ছেদ তৈরি করুন"
                               title="নতুন অনুচ্ছেদ তৈরি করুন" class="btn btn-default green margin-right-10">
                                <i class="fs1 a2i_gn_note2"></i> নতুন অনুচ্ছেদ
                            </a>
                            <!--<a title=" প্রেরণ  করুন" nothi_master_id="<?php echo $id ?>" nothi_office="<?php echo $nothi_office ?>" class="btn purple btn-forward-nothi margin-right-10" ><i class="fs1 a2i_gn_send2"></i>  প্রেরণ  করুন</a>-->
                            <?php if ($otherNothi == false && $nisponno == 0 && count($nothimovement) > 1 && $employee_office['office_unit_organogram_id'] == $nothiInformation['office_units_organogram_id']): ?>
                                <a title=" নোট নিষ্পন্ন " nothi_master_id="<?php echo $id ?>"
                                   nothi_office="<?php echo $nothi_office ?>" class="btn blue btn-nisponno-nothi"><i
                                            class="fs1 a2i_gn_onumodondelevery3"></i> নোট নিষ্পন্ন </a>
                            <?php endif; ?>
                        <?php } ?>
                        <div class="portlet box purple" id="responsiveNoteEdit">
                            <div class="portlet-body">

                                <div class="removeOnuchaddiv col-md-12 text-left">
                                    <span class="noteNo"> অনুচ্ছেদ: <?php
                                        echo '<span id="poro"></span>' . '.' . '<span id="notno"></span>';
                                        ?> </span>
                                </div>
                                <input type="hidden" id="noteId"/>
                                <input type="hidden" id="sendsaveModal"/>
                                <div class="row">
                                    <div class="col-md-12 form-group">

                                    </div>
                                </div>

                                <div class="row notesubjectdiv <?php echo ((!isset($nothi_note_sheets_id)
                                        || $nothi_note_sheets_id == 1) && ($lastListedNote < 0)) ? '' : 'hidden' ?>">
                                    <div class="col-md-2 col-sm-3 form-group">
                                        <label>বিষয়</label>
                                    </div>
                                    <div class="col-md-10 col-sm-7 form-group ">
                                        <input type="text" name="subject" id="notesubject"
                                               class="form-control input-sm"/>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <textarea class="noteComposer" id="noteComposer" name="description"></textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <form class="inbox-compose form-horizontal" id="fileupload"
                                              action="<?= $this->Url->build(['_name'=>'tempUpload']) ?>"
                                              method="POST" enctype="multipart/form-data">
                                            <input type="hidden" name="module_type" value="Nothi"/>
                                            <input type="hidden" name="module_type" value="Note"/>

                                            <div class="inbox-compose-attachment">
                                                <!--The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload-->
                                                <span class="btn blue btn-mini fileinput-button">
                                                    <i class="fa fa-clipboard"></i>
                                                    <span>
    <?php echo __(SHONGJUKTI) ?> </span>
                                                    <input type="file" name="files[]" multiple>
                                                </span>
                                                <!--The table listing the files available for upload/download-->
                                                <table role="presentation" class="table table-striped margin-top-10">
                                                    <tbody class="files">
                                                    </tbody>
                                                </table>
                                            </div>

                                            <script id="template-upload" type="text/x-tmpl">
                                                {% for (var i=0, file; file=o.files[i]; i++) { %}
                                                <tr class="template-upload fade">
                                                <td class="name" width="30%"><span>{%=file.name%}</span></td>
                                                <td class="size" width="40%"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                {% if (file.error) { %}
                                                <td class="error" width="20%" colspan="2"><span class="label label-danger">ত্রুটি</span> {%=file.error%}</td>
                                                {% } else if (o.files.valid && !i) { %}
                                                <td>
                                                <p class="size">{%=o.formatFileSize(file.size)%}</p>
                                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                                </div>
                                                </td>
                                                {% } else { %}
                                                <td colspan="2"></td>
                                                {% } %}
                                                <td class="cancel" width="10%" align="right">{% if (!i) { %}
                                                <button class="btn btn-sm red cancel">
                                                <i class="fa fa-ban"></i>
                                                <span>বাতিল করুন</span>
                                                </button>
                                                {% } %}</td>
                                                </tr>
                                                {% } %}




                                            </script>
                                            <!--The template to display files available for download-->
                                            <script id="template-download" type="text/x-tmpl">
                                                {% for (var i=0, file; file=o.files[i]; i++) { %}
                                                <tr class="template-download fade">
                                                {% if (file.error) { %}
                                                <td class="name" width="30%"><span>{%=file.name%}</span></td>
                                                <td class="size" width="20%"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                <td class="error" width="30%" colspan="2"><span class="label label-danger">ত্রুটি</span> {%=file.error%}</td>
                                                {% } else { %}
                                                <td class="name" width="40%">
                                                <a href="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" data-gallery="<?= FILE_FOLDER ?>{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
                                                </td>
                                                <td class="size" width="15%"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                <td width="40%" style ="min-width: 100px!important;">

                                                <input type="text" class="form-control note-attachment-input" id="note-attachment-input" onkeyup="check_title()" image="{%=file.url%}" file-type="{%=file.type%}">

                                                </td>
                                                {% } %}
                                                <td class="delete" width="5%" align="right">
                                                <button id="deletethis" onchange="check_title()"
                                                class="btn default btn-sm" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                                                <i class="fa fa-times"></i>
                                                </button>
                                                </td>
                                                </tr>
                                                {% } %}




                                            </script>

                                        </form>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <button class="btn green btn-sm green saveNote"
                                                hasnotorno="<?php echo $lastListedNote; ?>">সংরক্ষণ
                                        </button>
                                        &nbsp;
                                        <button class="btn green btn-sm blue saveAndNewNote"
                                                hasnotorno="<?php echo $lastListedNote; ?>">নতুন অনুচ্ছেদ
                                        </button>
                                        &nbsp;
                                        <button class="btn green btn-sm purple btn-save-forward-nothi"
                                                nothi_office="<?php echo $nothi_office ?>"
                                                nothi_master_id="<?php echo $id ?>"
                                                hasnotorno="<?php echo $lastListedNote; ?>"> প্রেরণ
                                        </button>
                                        &nbsp;
                                        <button class="btn btn-sm btn-closeNote btn-danger"
                                                data-original-title="বন্ধ করুন" title="বন্ধ করুন"
                                                onclick="editBoxClose();"> মুছে ফেলুন
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet-footer" style="border-top: 1px solid #eee;">
        <div class="row">

            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-10 col-md-10 col-sm-10 text-right">
                <ul class="pagination pagination-sm">
                    <?php
                    echo $this->Paginator->last(__('শেষ', true),
                        array('class' => 'number-last'));
                    echo $this->Paginator->next('<<',
                        array('tag' => 'li', 'escape' => false),
                        '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
                        array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li',
                        'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a',
                        'reverse' => true));
                    echo $this->Paginator->prev('>>',
                        array('tag' => 'li', 'escape' => false),
                        '<a href="#" class="btn btn-sm blue">&laquo;</a>',
                        array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li',
                            'escape' => false));
                    echo $this->Paginator->first(__('প্রথম', true),
                        array('class' => 'number-first'));
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>

    (function () {
        var numbers = {
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯',
            0: '০'
        };

        function replaceNumbers(input) {
            var output = [];
            for (var i = 0; i < input.length; ++i) {
                if (numbers.hasOwnProperty(input[i])) {
                    output.push(numbers[input[i]]);
                } else {
                    output.push(input[i]);
                }
            }
            return output.join('');
        }

        Metronic.initSlimScroll('.scroller');
        $('[data-title-orginal]').tooltip({'placement':'bottom'});
        $('li').tooltip({'placement':'bottom'});
        $('.select2').select2();

        $(window).load(function () {
        var db = ProjapotiOffline.createDatabase('offline_note_draft');
        var docid = '<?php echo $employee_office['office_id'] . '_' . $employee_office['officer_id'] . '_' . $employee_office['office_unit_organogram_id'] . '_' . $id ?>';
        db.get(docid).then(function (doc) {
            setTimeout(function () {
                initiatefroala();
                $('#noteComposer').froalaEditor('html.set', doc.body);
                setTimeout(function () {
                    $.contextMenu('destroy');
                    iniDictionary();
                }, 200);
            }, 300);
            NoteFileUpload.init();
            $('#noteId').val(doc.noteid);
            $('#notesubject').val(doc.subject);
            $('#bibeccoPotro').val(0);
            $('#addNewNote').hide();
            $('.btn-forward-nothi').hide();
            $('.btn-sar-nothi-draft').hide();
            $('.btn-nisponno-nothi').hide();
            $('#responsiveNoteEdit').toggle();
            getAutolist();
            var value = $.trim($(".nothipartdiv.btn-success").text());
            $("#poro").text(value);
            var lastnot = 0;
            if (value != '' && value != null && value != 'undefined') {
                $.each($('.nothiOnucched[partno=' + value + ']'), function (i) {
                    lastnot = Math.max(lastnot, $(this).attr('notenoen'));
                });
            }
            var lastnoteno = -1;
            if (lastnot > 0) {
                lastnoteno = lastnot;
            }
            var bn = replaceNumbers("'" + parseInt(parseInt(lastnoteno) + 1) + "'");
            bn = bn.replace("'", '');
            bn = bn.replace("'", '');
            $('#notno').text(bn);
        }).then(function (response) {
            //console.log(response);
            // handle response

        }).catch(function (err) {

            <?php if ($lastListedNote == -1): ?>
            setTimeout(function () {
                initiatefroala();
                setTimeout(function () {
                    $.contextMenu('destroy');
                    iniDictionary();
                }, 200);
            },300);
            NoteFileUpload.init();
            $('#addNewNote').hide();
            $('.btn-forward-nothi').hide();
            $('.btn-sar-nothi-draft').hide();
            $('.btn-nisponno-nothi').hide();
            $('.btn-closeNote').hide();
            $('#responsiveNoteEdit').toggle();
            getAutolist();
            var value = $.trim($(".nothipartdiv.btn-success").text());
            $("#poro").text(value);
            var lastnot = 0;
            if (value != '' && value != null && value != 'undefined') {
                $.each($('.nothiOnucched[partno=' + value + ']'), function (i) {
                    lastnot = Math.max(lastnot, $(this).attr('notenoen'));
                });
            }
            var lastnoteno = -1;
            if (lastnot > 0) {
                lastnoteno = lastnot;
            }
            var bn = replaceNumbers("'" + parseInt(parseInt(lastnoteno) + 1) + "'");
            bn = bn.replace("'", '');
            bn = bn.replace("'", '');
            $('#notno').text(bn);
            <?php endif; ?>

        });
        });

    }(jQuery));
</script>

<script>

    function check_title() {
//        $('#note-attachment-title').find('option').remove().end();
//        $('#note-attachment-title').append($("<option></option>").text('--'));

        $('#dropdown-menu-attachment-ref-1 ul').html('');
        $('#dropdown-menu-attachment-ref-1 ul').append($('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="attachment-ref" data-param1="" title="" aria-selected="false">--</a></li>'));

        $('#fileupload .note-attachment-input').each(function () {
            var tx = $(this).val();
            var file_handle = $(this).attr('image');
            var file_type = $(this).attr('file-type');

            $('#dropdown-menu-attachment-ref-1 ul').append('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="attachment-ref" data-param1="' + file_handle + '" ft="' + file_type + '" title="" aria-selected="false">' + tx + '</a></li>');

//            $('#note-attachment-title').append($("<option value='" + file_handle + "' ft='" + file_type + "'></option>").text(tx));
        });
        if (parseInt($('#noteId').val()) > 0) {
            $.each($('#responsiveNoteEdit').next().next().find('.preview_attachment a'), function (i, v) {
                var tx = '';
                var d_text = 'No_user_input_file';
                if ($(this).attr('user_file_name') == d_text) {
                    tx = v.text;
                } else {
                    tx = $(this).attr('user_file_name');
                }
                var file_type = v.type;
                var file_handle = v.href;

                $('#dropdown-menu-attachment-ref-1 ul').append('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="attachment-ref" data-param1="' + file_handle + '" ft="' + file_type + '" title="" aria-selected="false">' + tx + '</a></li>');

//                $('#note-attachment-title').append($("<option value='" + file_handle + "' ft='" + file_type + "'></option>").text(tx));
            });
        }
    }

    $('.preview_attachment a').on('click', function (e) {
        e.preventDefault();
    });
</script>