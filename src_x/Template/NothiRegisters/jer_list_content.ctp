<?php
$prev = "";
$count = 0;
if (!empty($data)) {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-hover">
            <?php
            foreach ($data as $key => $value) {
                $officeUnitTable = \Cake\ORM\TableRegistry::get("OfficeUnits");
                $unitInformation = $officeUnitTable->get($value['office_units_id']);
                $unitId = $value['office_units_id'];
                if ($prev == $unitId) {
                    
                } else {
                    $prev = $unitId;
                    if ($count) {
                        ?>
                        <tr><td class="text-center" colspan="7"> </td></tr> 

                        <?php
                        $count = 0;
                    }
                    ?>
                    <tr class="heading">
                        <th class="text-center" colspan="7"><?= h($unitInformation['unit_name_bng']) ?><br><?= h($office_name['office_name_bng']) ?></th>
                    </tr>
                    <tr class="heading">
                          <th class="text-center" > ক্রমিক সংখ্যা  </th>
                        <th class="text-center" > কোথা হইতে প্রাপ্ত  </th>
                        <th class="text-center" > প্রপ্তির তারিখ </th>
                        <th class="text-center" > বিষয়ের বিবরণ </th>
                        <th class="text-center" > স্থানান্তরের কারণ  </th>
                        <th class="text-center"> পেশ করিবার পরবর্তী  তারিখ </th>
                        <th class="text-center" > শাখার দায়িত্বপ্রাপ্ত কর্মকর্তার অনুস্বাক্ষর </th>

                    </tr>
                    <?php
                }
                $count++;
                ?>
                <tr>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
    <?php
} else {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-striped">
            <tr class="heading">
                        <th class="text-center" > ক্রমিক সংখ্যা  </th>
                        <th class="text-center" > কোথা হইতে প্রাপ্ত  </th>
                        <th class="text-center" > প্রপ্তির তারিখ </th>
                        <th class="text-center" > বিষয়ের বিবরণ </th>
                        <th class="text-center" > স্থানান্তরের কারণ  </th>
                        <th class="text-center"> পেশ করিবার পরবর্তী  তারিখ </th>
                        <th class="text-center" > শাখার দায়িত্বপ্রাপ্ত কর্মকর্তার অনুস্বাক্ষর </th>

                    </tr>
            <tr><td class="text-center" colspan="7" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr> 
        </table>
    </div>
    <?php
}
?>
<div class="actions text-center">
    <ul class="pagination pagination-sm">
        <?php
//        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
//        echo $this->Paginator->next('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
//        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
//        echo $this->Paginator->prev('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
//        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        ?>
    </ul>

</div>











