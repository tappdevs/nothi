<?php
$prev = "";
$count = 0;
if (!empty($data)) {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="heading">
                <th class="text-center"  style="width: 5%;">ক্রমিক সংখ্যা</th>
                <th class="text-center"  style="width: 20%;">নথির নম্বর</th>
                <th class="text-center"  style="width: 20%;">পূর্ববর্তী নথির নম্বরসমূহ</th>
                <th class="text-center"  style="width: 30%;">ধরন/শিরোনাম</th>
                <th class="text-center" style="width: 45%;">শ্রেণিবিন্যাসসহ নথিভুক্ত করবার তারিখ</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($this->Paginator->params()['page'] > 1) {
                $count = (($this->Paginator->params()['page']-1) * $this->Paginator->params()['perPage']);
            } else {
                $count = 0;
            }
            foreach ($data as $key => $value) {
                $unitInformation = h($value['office_units_name']);
                $unitId = h($value['office_units_id']);
                if ($prev != $unitId) {
                    $prev = $unitId;
                    $count=0;
                    ?>
                    <tr>
                        <th class="text-center" colspan="5"><?= $unitInformation ?><br><?= h($office_name['office_name_bng']) ?></th>
                    </tr>
                    <?php
                }
                $count++;
                ?>
                <tr>
                    <td class="text-center"><?= $this->Number->format($count) ?></td>
                    <td class="text-center"><?= h($value['nothi_no']) ?></td>
                    <td class="text-center"><?= h($value['changes_history']) ?></td>
                    <td class="text-left"><?= h($value['NothiTypes']['type_name']) . ' - ' . h($value['subject']) ?></td>

                    <td class="text-center"><?= $nothiClass[$value['nothi_class']] . ', ' . Cake\I18n\Time::parse($value['nothi_created_date'])->i18nFormat('dd-MM-yyyy') ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <script>
        $("#content-export-button").css('display', 'initial');
        $("#report_pagination").css('display', 'block');
    </script>
    <?php
} else {
    ?>
    <div class="alert alert-danger">
        দুঃখিত কোন তথ্য পাওয়া যায় নি।
    </div>
    <script>
        $("#content-export-button").css('display', 'none');
        $("#report_pagination").css('display', 'none');
    </script>
    <?php
}
?>
<?= customPagination($this->Paginator) ?>
<script>
    jQuery(document).ready(function () {
        $('.table').floatThead({
            position: 'absolute',
            top: jQuery("div.navbar-fixed-top").height()
        });
        $('.table').floatThead('reflow');
    });
</script>






















