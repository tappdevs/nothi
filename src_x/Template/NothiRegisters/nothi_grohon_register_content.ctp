<?php
$prev = "";
$count = 0;
if (!empty($data)) {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="heading">
                <th class="text-center"  width="10%">ক্রমিক সংখ্যা</th>
                <th class="text-center"  width="15%">গৃহীত নথির নম্বর</th>
                <th class="text-center"  width="10%"> গ্রহণের তারিখ</th>
                <th class="text-center"  width="20%">অফিস/শাখার নাম </th>
                <th class="text-center"  width="25%">নথির বিষয়</th>
                <th class="text-center"  width="20%">পূর্ববর্তী প্রেরক </th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $key => $value) {
                $unitInformation = h($value['to_office_unit_name']);
                $unitId = h($value['to_office_unit_id']);
                if ($prev != $unitId) {
                    $prev = $unitId;
                    $count = 0;
                    ?>
                    <tr>
                        <th class="text-center" colspan="6"><?= $unitInformation ?><br><?= h($office_name['office_name_bng']) ?></th>
                    </tr>
                    <?php
                }
                $count++;
                ?>
                <tr>
                    <td class="text-center"><?= $this->Number->format($count) ?></td>
                    <td class="text-center"><?= h($value['NothiParts']['nothi_no']) ?></td>
                    <td class="text-center"><?= Cake\I18n\Time::parse($value['created']) ?></td>
                    <td class="text-center"><?= h($value['from_office_unit_name']) . ', ' . h($value['from_officer_designation_label']) ?></td>
                    <td class="text-center"><?= h($value['NothiParts']['subject']) ?> </td>
                    <td class="text-center"><?= h($value['from_officer_name']) . ', ' . h($value['from_officer_designation_label']) ?></td>

                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <script>
        $("#content-export-button").css('display', 'initial');
        $("#report_pagination").css('display', 'block');
    </script>
    <?php
} else {
    ?>
    <div class="alert alert-danger">
        দুঃখিত কোন তথ্য পাওয়া যায় নি।
    </div>
    <script>
        $("#content-export-button").css('display', 'none');
        $("#report_pagination").css('display', 'none');
    </script>
    <?php
}
?>
<div class="actions text-center" id="report_pagination">
    <?= customPagination($this->Paginator) ?>

</div>
<script>
    jQuery(document).ready(function () {
        $('.table').floatThead({
            position: 'absolute',
            top: jQuery("div.navbar-fixed-top").height()
        });
        $('.table').floatThead('reflow');
    });
</script>



















