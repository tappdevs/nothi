<?php
$prev = "";
$count = 0;
if (isset($_GET['page'])) {
    $count = ($_GET['page'] * 20) - 20;
}
if (!empty($data)) {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-hover">
            <tr class="heading">
                <th class="text-center"  width="10%">ক্রমিক সংখ্যা</th>
                <th class="text-center"  width="15%">গৃহীত নথির নম্বর</th>
                <th class="text-center"  width="10%"> গ্রহণের তারিখ</th>
                <th class="text-center"  width="20%">অফিস/শাখার নাম </th>
                <th class="text-center"  width="25%">নথির বিষয়</th>
                <th class="text-center"  width="20%">পূর্ববর্তী প্রেরক </th>
            </tr>
            <?php
            foreach ($data as $key => $value) {
                $unitInformation = $value['to_office_unit_name'];
                $unitId = $value['to_office_unit_id'];
                $count++;
                ?>
                <tr>
                    <td class="text-center"><?= $this->Number->format($count) ?></td>
                    <td class="text-center"><?= h($value['NothiParts']['nothi_no']) ?></td>
                    <td class="text-center"><?= Cake\I18n\Time::parse($value['created']) ?></td>
                    <td class="text-center"><?= h($value['from_office_unit_name']) . ', ' . h($value['from_officer_designation_label']) ?></td>
                    <td class="text-center"><?= h($value['NothiParts']['subject']) ?> </td>
                    <td class="text-center"><?= h($value['from_officer_name']) . ', ' . h($value['from_officer_designation_label']) ?></td>

                </tr>
                <?php
            }
            ?>
        </table>
    </div>
    <script>
        $("#content-export-button").css('display', 'initial');
        $("#report_pagination").css('display', 'block');
    </script>
    <?php
} else {
    ?>
    <div class="alert alert-danger">
        দুঃখিত কোন তথ্য পাওয়া যায় নি।
    </div>
    <script>
        $("#content-export-button").css('display', 'none');
        $("#report_pagination").css('display', 'none');
    </script>
    <?php
}
?>
<div class="actions text-center" id="report_pagination">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>

</div>



















