<?php
$prev = "";
$count = 0;
if (!empty($data)) {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-hover">
            <?php
            foreach ($data as $key => $value) {
                $officeUnitTable = \Cake\ORM\TableRegistry::get("OfficeUnits");
                $unitInformation = $value['from_office_unit_name'];
                $unitId = $value['from_office_unit_id'];
                if ($prev == $unitId) {
                    
                } else {
                    $prev = $unitId;
                    if ($count) {
                        ?>
                        <tr><td class="text-center" colspan="7"> </td></tr> 

                        <?php
                        $count = 0;
                    }
                    ?>
                    <tr class="heading">
                        <th class="text-center" colspan="7"><?= h($unitInformation) ?><br><?= h($office_name['office_name_bng']) ?></th>
                    </tr>
                    <tr class="heading">
                        <th class="text-center"rowspan="2" > ক্রমিক সংখ্যা  </th>
                        <th class="text-center" rowspan="2">  নথি নম্বর </th>
                        <th class="text-center" rowspan="2">  বিষয়  </th>
                        <th class="text-center" rowspan="2">  নথি প্রেরণের তারিখ </th>
                        <th class="text-center"  colspan="2">  নথির গতিবিধি  </th>
                        <th class="text-center"rowspan="2" > নথি গ্রহণকারীর স্বাক্ষর তারিখ </th>

                    </tr>
                    <tr class="heading">
                        <th class="text-center"> প্রেরক  </th>
                        <th class="text-center"> প্রাপক </th>
                    </tr>

                    <?php
                }
                $count++;
                ?>
                <tr>
                    <td class="text-center"> <?= $this->Number->format($count) ?> </td>
                    <td class="text-center"> <?= $value['NothiMasters']['nothi_no'] ?></td>
                    <td class="text-center"> <?= $value['NothiMasters']['subject'] ?></td>
                    <td class="text-center"> <?= Cake\I18n\Time::parse($value['created']) ?></td>
                    <td class="text-center"> <?= $value['from_officer_name'] . ", " . $value['from_office_name'] ?> </td>
                    <td class="text-center"> <?= $value['to_officer_name'] . ", " . $value['to_office_name'] ?> </td>
                    <td class="text-center"></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
    <?php
} else {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-striped">
            <tr class="heading">
                <th class="text-center"rowspan="2" > ক্রমিক সংখ্যা  </th>
                <th class="text-center" rowspan="2">  নথি নম্বর </th>
                <th class="text-center" rowspan="2">  বিষয়  </th>
                <th class="text-center" rowspan="2">  নথি প্রেরণের তারিখ </th>
                <th class="text-center"  colspan="2">  নথির গতিবিধি  </th>
                <th class="text-center"rowspan="2" > নথি গ্রহণকারীর স্বাক্ষর তারিখ </th>

            </tr>
            <tr class="heading">
                <th class="text-center"> প্রেরক  </th>
                <th class="text-center"> প্রাপক </th>
            </tr>
            <tr><td class="text-center" colspan="7" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr> 
        </table>
    </div>
    <?php
}
?>
<div class="actions text-center">
    <?= customPagination($this->Paginator) ?>

</div>













