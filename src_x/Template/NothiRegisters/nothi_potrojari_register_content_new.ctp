<?php
$prev = "";
$count = 0;
if (isset($_GET['page'])) {
    $count = ($_GET['page'] * 20) - 20;
}
if (!empty($data)) {
    ?>
    <div class="table-container  table-scrollable">
        <table class="table table-bordered table-hover">
            <tr class="heading">
                <th class="text-center"  width="5%">ক্রমিক সংখ্যা</th>
                <th class="text-center"  width="10%">বিষয়</th>
                <th class="text-center"  width="20%">প্রেরক </th>
                <th class="text-center"  width="10%">স্মারক নম্বর</th>
                <th class="text-center"  width="10%"> প্রেরণের তারিখ  </th>
                <th class="text-center"  width="15%">প্রাপকসমূহ  </th>
                <th class="text-center"  width="15%">অনুলিপিসমূহ</th>
                <th class="text-center"  width="5%">পত্র ধরন</th>
            </tr>
            <?php
            foreach ($data as $key => $value) {
                $unitInformation = $value['office_unit_name'];
                $unitId = $value['office_unit_id'];
                $count++;
                ?>
                <tr>
                    <td class="text-center" style="vertical-align: middle"><?= $this->Number->format($count) ?></td>
                    <td class="text-center" style="vertical-align: middle"><?= h($value['potro_subject']) ?></td>
                    <td class="text-center" style="vertical-align: middle"><?= h($value['officer_designation_label']) . ', ' . h($value['office_unit_name']) ?></td>
                    <td class="text-center" style="vertical-align: middle"><?= h($value['sarok_no']) ?></td>
                    <td class="text-center" style="vertical-align: middle"><?= Cake\I18n\Time::parse(($value['potrojari_date'])) ?></td>
                    <td class="text-center" style="vertical-align: middle"><?= !empty($prapoks[$value['id']])?implode("<hr/>", $prapoks[$value['id']]):'' ?></td>
                    <td class="text-center" style="vertical-align: middle"><?= !empty($onulipis[$value['id']])?implode("<hr/>", $onulipis[$value['id']]):'' ?></td>
                    <td class="text-center" style="vertical-align: middle"><?= $potroTypeInfo[$value['potro_type']] ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
    <script>
        $("#content-export-button").css('display', 'initial');
        $("#report_pagination").css('display', 'block');
    </script>
    <?php
} else {
    ?>
    <div class="alert alert-danger">
        দুঃখিত কোন তথ্য পাওয়া যায় নি।
    </div>
    <script>
        $("#content-export-button").css('display', 'none');
        $("#report_pagination").css('display', 'none');
    </script>

    <!--<div class="table-container  table-scrollable">
        <table class="table table-bordered table-striped">
            <tr class="heading">
                <th class="text-center"  width="5%">ক্রমিক সংখ্যা</th>
                <th class="text-center"  width="10%">বিষয়</th>
                <th class="text-center"  width="20%">প্রেরক </th>
                <th class="text-center"  width="10%">স্মারক নম্বর</th>
                <th class="text-center"  width="10%"> প্রেরণের তারিখ  </th>

                <th class="text-center"  width="15%">প্রাপকসমূহ  </th>
                <th class="text-center"  width="15%">অনুলিপিসমূহ</th>
                <th class="text-center"  width="5%">পত্র ধরন</th>

                <th class="text-center"   width="5%">অগ্রাধিকার</th>
                <th class="text-center"   width="5%">গোপনীয়তা</th>
            </tr>

            <tr><td class="text-center" colspan="10" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr> 
        </table>
    </div>-->
    <?php
}
?>
<div class="actions text-center" id="report_pagination">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>

</div>


























