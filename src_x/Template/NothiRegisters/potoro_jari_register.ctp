<?php
	/* $daks = array(array (
		"sharok_no" => "123456",
		"sending_date" => "123456",
		"sender_info" => "123456",
		"subject" => "123456"
	)); */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i>নথি গ্রহণ নিবন্ধন বহি</div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row text-center">

                <h4><?php echo $office['office_records'] [0]['office_name']; ?></h4>
                <h5><?php echo $office['office_records'] [0]['office_address']; ?></h5>

            </div>

            <div class="row">
                <div class="col-md-12">


                    <!-- Tools start  -->

                    <!--<div class="hidden-print btn-group pull-left">
                        <button class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Tools <i
                                class="glyphicon glyphicon-chevron-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;">
                                    Save as PDF </a>
                            </li>
                            <li>
                                <a href="javascript:window.print();">
                                    Print </a>
                            </li>
                        </ul>
                    </div>-->

                    <!--Tools end -->

                    <!--  Date Range Begain -->

                    <div class="hidden-print page-toolbar pull-right portlet-title">
                        <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                             data-container="body"
                             data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                            <i class="icon-calendar"></i>&nbsp; <span
                                class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                            <i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                    </div>

                    <!--  Date Range End  -->

                </div>

            </div>
        </div>
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr class="heading">
                <th class="text-center" rowspan="2" style="width:5%">ক্রমিক সংখ্যা</th>
                <th class="text-center" rowspan="2" style="width:10%">নথির নম্বর</th>
                <th class="text-center" rowspan="2" style="width:10%">পত্রের  স্মারক  নম্বর ও তারিখ</th>
                <th class="text-center" rowspan="2" style="width:30%"> যাহার নিকট পত্র প্রেরিত হইল  তাহাঁর ঠিকানা  </th>
                <th class="text-center" colspan="3" style="width:30%">প্রেরণের বিবরণসহ তারিখ</th>
                <th class="text-center" rowspan="2" style="width:15%">মন্তব্য </th>
                
            </tr>
            <tr>
                <th class="text-center" style="width:10%">ডাকযোগে</th>
                <th class="text-center" style="width:10%">বাহক মারফত</th>
                <th class="text-center" style="width:10%">ইলেক্ট্রনিক মাধ্যমে</th>
            </tr>



            </thead>
            <tbody class="inbox-content">

            </tbody>
        </table>

        <div class="row">
            <div class="col-md-12">
                <a class="btn pull-right btn-sm blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                    <?php echo __("Print") ?> <i class="fs1 a2i_gn_print2"></i> </a>
            </div>
        </div>

    </div>
</div>

<script src="<?php echo CDN_PATH; ?>js/reports/potorojari_register.js"
        type="text/javascript"></script>

<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>

<script>
jQuery(document).ready(function () {
    DateRange.init();
    DateRange.initDashboardDaterange();
});    
</script>
