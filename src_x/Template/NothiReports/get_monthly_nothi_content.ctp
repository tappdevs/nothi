<?php
$prev = "";
$count = 0;
if (!empty($datas)) {
    ?>
    <div class= "table-scrollable">
        <table class="table table-bordered table-hover">
            <thead>
            <tr class="heading">
                <th class="text-center" colspan="6"><?=isset($selected_office_section['office_name'])?($selected_office_section['office_name'].' - এর উপস্থাপিত নথি সংখ্যা'):''?><?= isset($startDate) && isset($endDate)? (' ('.entobn($startDate).' - '.entobn($endDate).' )' ):'' ?></th>
            </tr>
            <tr class="heading">
                <th class="text-center" >ক্রম</th>
                <th class="text-center" >শাখার নাম</th>
                <th class="text-center" >নথি</th>
                <th class="text-center" >নোট উপস্থাপন </th>
                <th class="text-center" >ডাক থেকে সৃজিত নোট </th>
                <th class="text-center" >স্ব-উদ্যোগে নোট </th>
            </tr>
            </thead>
            <tbody>
            <?php
            $total_nothi = 0;
            $total_note = 0;
            $total_srijito = 0;
            $total_swa_uddok = 0;
            foreach ($datas as $data) {
                $count++;
                $total_nothi += isset($data['count'])?$data['count']:0;
                $total_note += isset($data['all_note'])?$data['all_note']:0;
                $total_srijito += isset($data['dak_srijito_note'])?$data['dak_srijito_note']:0;
                $total_swa_uddok += isset($data['swa_uddog_note'])?$data['swa_uddog_note']:0;
                ?>
                <tr>
                    <td class="text-center"><?= enTobn($count) ?></td>
                    <td class="text-center"><?= $data['name'] ?></td>
                    <td class="text-center"><?= enTobn($data['count']) ?></td>
                    <td class="text-center"><?= enTobn($data['all_note']) ?></td>
                    <td class="text-center"><?= enTobn($data['dak_srijito_note']) ?></td>
                    <td class="text-center"><?= enTobn($data['swa_uddog_note']) ?></td>

                </tr>
                <?php
            }
            ?>
            </tbody>
            <tfoot>
            <?php
            if(isset($total_nothi)){
              ?>
                <tr class="heading">
                    <td class="text-center bold" colspan="2"><?= __('Total') ?></td>
                    <td class="text-center bold"><?= enTobn($total_nothi) ?></td>
                    <td class="text-center bold"><?= enTobn($total_note) ?></td>
                    <td class="text-center bold"><?= enTobn($total_srijito) ?></td>
                    <td class="text-center bold"><?= enTobn($total_swa_uddok) ?></td>

                </tr>
                <?php
            }
            ?>
            </tfoot>
        </table> </div>
    <?php
}
if (empty($data)) {
    ?>
    <div class= "table-scrollable">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr class="heading">
                <th class="text-center" colspan="6"><?=isset($selected_office_section['office_name'])?($selected_office_section['office_name'].' - এর উপস্থাপিত নথি সংখ্যা'):''?><?= isset($startDate) && isset($endDate)? (' ('.entobn($startDate).' - '.entobn($endDate).' )' ):'' ?></th>
            </tr>
            <tr class="heading">
                <th class="text-center" >ক্রম</th>
                <th class="text-center" >শাখার নাম</th>
                <th class="text-center" >নথি</th>
                <th class="text-center" >নোট উপস্থাপন </th>
                <th class="text-center" >ডাক থেকে সৃজিত নোট </th>
                <th class="text-center" >স্ব-উদ্যোগে নোট </th>
            </tr>
            </thead>
            <tbody>

            <tr><td class="text-center" colspan="6" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr>
            </tbody> </table> </div>
    <?php
}
?>