<style>
    .child-table > tbody > tr:first-child > td {
        border: none;
    }
    .table>tbody>tr>td{
        padding: 0px;
    }
</style>
<?php
$unit_id=0;
$total = 0;
if (!empty($datas)) {
    ?>
    <div class= "table-scrollable">
        <table class="table table-bordered">
            <tr class="heading">
                <th class="text-center">শাখা</th>
                <th class="text-center">নোট উপস্থাপনকারী নথির নাম</th>
                <th class="text-center"> নথি নং </th>
                <th class="text-center">নোট উপস্থাপন</th>
                <th class="text-center">ডাক থেকে সৃজিত নোট</th>
                <th class="text-center">স্বউদ্যোগে নোট</th>
                <th class="text-center">মোট নিষ্পন্ন</th>
            </tr>

            <?php
            foreach ($datas as $data) {
                if(!empty($data['name'])){
                    $total++;
                    if($unit_id==$data['unit_id']){
                        $data['name']='';
                    } else {
                        $unit_id= $data['unit_id'];
                    }
                    ?>
                <tr>
                    <td class="text-center"><?= $data['name'] ?></td>
                    <td class="text-center"><?= $data['nothi_subject'] ?></td>
                    <td class="text-center"><?= $data['nothi_no'] ?></td>
                    <td class="text-center"><?= $data['all_note'] ?></td>
                    <td class="text-center"><?= $data['dak_srijito_note'] ?></td>
                    <td class="text-center"><?= $data['swa_uddog_note'] ?></td>
                    <td class="text-center"><?= $data['nisponno'] ?></td>

                </tr>
                <?php
            }}
            ?>
            <?php if($total == 0){ ?>
                <tr><td class="text-center" colspan="7" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr>
         <?php   } ?>
        </table> </div>
    <?php
} else {
?>
<div class= "table-scrollable">
    <table class="table table-bordered">
            <tr class="heading">
                <th class="text-center">শাখা</th>
                <th class="text-center">নোট উপস্থাপনকারী নথির নাম</th>
                 <th class="text-center"> নথি নং </th>
                <th class="text-center">নোট উপস্থাপন</th>
                <th class="text-center">ডাক থেকে সৃজিত নোট</th>
                <th class="text-center">স্বউদ্যোগে নোট</th>
                <th class="text-center">মোট নিষ্পন্ন</th>
            </tr>
        <tr><td class="text-center" colspan="7" style="color:red;"> দুঃখিত কোন তথ্য পাওয়া যায় নি।  </td></tr>
    </table> </div>
<?php } ?>
<div class="actions text-center">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue">&laquo;</a>', array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => FALSE));
        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false), '<a href="#" class="btn btn-sm blue ">&raquo;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>

</div>
