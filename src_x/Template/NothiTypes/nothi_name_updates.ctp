
<style>
    #nothi_number_2nd_part::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #3a3a3a !important;
        opacity: 1; /* Firefox */
    }
    #nothi_number_2nd_part:focus{
        border-color: #d1d1d0;
    }
</style>
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i> নথির তথ্য সংশোধন  </div>
    </div>
    <div class="portlet-body">
         <?php echo $this->Form->create('', ['class' => 'searchForm','id' =>'nothiNameUpdate','url' => ['action'=>'nothiNameUpdates'], 'onsubmit' => 'return false;' ]) ?>
        <div class="row">
        <div class="form-horizontal">
            <div class="form-group col-md-3 col-xs-12" style="margin-left:0; margin-right:0;">
                <label class="control-label text-right font-lg">শাখাঃ</label>
                <?php
                    echo $this->Form->input('office_unit_id',
                        array('label' => false, 'type' => 'select', 'class' => 'form-control',
                            'options' =>  !empty($ownChildInfo)?['all'=>'সকল শাখা']+$ownChildInfo:[],'empty'=>__("বাছাই করুন")));
                ?>
            </div>

            <div class="form-group col-md-4 col-xs-12" style="margin-left:0; margin-right:0;">
                <label class="control-label text-right font-lg">শিরোনাম</label>
                <input type="text" class="form-control input-md" placeholder="শিরোনাম" name="searchFor_nothi_subject" id="searchFor_nothi_subject">
            </div>

            <div class="form-group col-md-3 col-xs-12" style="margin-left:0; margin-right:0;">
                <label class="control-label text-right font-lg">নথি নম্বর</label>
                <input type="text" class="form-control form-filter input-md" placeholder="নথি নম্বর" name="searchFor_nothi_no" id="searchFor_nothi_no">
            </div>

            <div class="form-group col-md-2 col-xs-12">
                <label class="control-label" style="display:block">&nbsp;</label>
                <button class="btn btn-success" style="height:35px" onclick="$('#office-unit-id').trigger('change');"><i class="fa fa-search"></i></button>
                <button class="btn btn-danger" style="height:35px" onclick="resetForm()"><i class="fa fa-undo"></i></button>
            </div>
        </div>
        </div>
        <?php echo $this->Form->end() ?>
        <br/>
        <div class="inbox-content">
            <div id="showlist"></div><br>
            <div class="table-container table-responsive">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                        <tr class="heading">
                            <th class="text-center" > নথি নম্বর </th>
                            <th class="text-center" > শাখা </th>
                            <th class="text-center" > শিরোনাম </th>
                            <th class="text-center"> শ্রেণি </th>
                            <th class="text-center"> নথির ধরন </th>
                            <th class="text-center"> তৈরির তারিখ </th>
                            <th class="text-center"> <?= __('Actions') ?> </th>
                        </tr>
                    </thead>
                    <tbody id="addData">
                        
                    </tbody>
                </table>
            </div>
        </div>
        <input type="hidden" value="" id="hidden_type">
        <input type="hidden" value="" id="hidden_type_id">
        <input type="hidden" value="" id="hidden_type_name">
        <input type="hidden" value="" id="hidden_unit">
        <input type="hidden" value="" id="hidden_unit_name">
    </div>
</div>
<div id="myModal" class="modal fade modal-purple " role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" >নথির তথ্য সংশোধন
                    <a type="button" id="nothi_change_history" class="btn btn-sm btn-info pull-right" >পূর্বের পরিবর্তনসমূহ দেখুন</a>
                </h4>
            </div>
            <div class="modal-body" >
                    <div class=" form form-group">
                         <input type="hidden" id="id_up">
                         <label class="control-label"> বর্তমান নাম </label>
                        <input type="text" id="cur_name" class="form-control">
                         <label class="control-label"> নতুন নাম </label>
                        <input type="text" id="new_name" class="form-control">
                        <label class="control-label"> শ্রেণি </label>
                        <select name="class_id" id="class_id" class="form-control">
                            <option value="1">ক</option>
                            <option value="2">খ</option>
                            <option value="3">গ</option>
                            <option value="4">ঘ</option>
                        </select>
                        <span id="class_error_span" style="display: none;"></span>
                        <label class="control-label"> শাখা </label>
                        <?php
                        echo $this->Form->input('office_unit_id_modal',
                            array('label' => false, 'type' => 'select', 'class' => 'form-control',
                                'options' =>  !empty($ownChildInfo)?$ownChildInfo:[],'empty'=>__("বাছাই করুন")));
                        ?>
                        <label class="control-label"> নথির ধরন </label>
                        <select name="type_id" id="type_id" class="form-control">
                            <option value="0">--ধরন নির্বাচন করুন--</option>
                        </select>
                        <label class="control-label"> বর্তমান নথি নম্বর </label>
                        <input type="text" id="old_nothi_number" class="form-control" >
                        <label class="control-label"> নতুন নথি নম্বর </label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group" style="width: 100%;">
                                    <div class="input-group-addon" id="nothi_number_1st_part" style="border-right: none;background:  white;padding-right:  0px;    border-color: #d1d1d0;text-align: left;font-size: 13pt;color: #3a3a3a;">**.**.****.***.**.</div>
                                    <input type="text" id="nothi_number_2nd_part" class="form-control text-left" maxlength="6" style="border-left: none;padding-left: 0;">
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="submitButton"> <?= __('Submit') ?> </button>
                <button type="button" class="btn btn-default red" data-dismiss="modal"><?= __("Close") ?></button>
            </div>
        </div>
        <!-- Modal End -->




    </div>
</div>
<!-- History Modal -->
<div id="nothi_data_history_modal" class="modal fade modal-purple" role="dialog">
    <div class="modal-dialog modal-full">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">নথির পূর্বের পরিবর্তনসমূহ</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                    <tr class="heading">
                        <th class="text-center" > ক্রমিক নম্বর </th>
                        <th class="text-center" > নথি নম্বর </th>
                        <th class="text-center" > শিরোনাম </th>
                        <th class="text-center"> শাখা </th>
                        <th class="text-center"> শ্রেণি </th>
                        <th class="text-center"> নথির ধরন </th>
                        <th class="text-center"> সময় </th>
                    </tr>
                    </thead>
                    <tbody id="addHistoryData">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default red" data-dismiss="modal">বন্ধ করুন</button>
            </div>
        </div>

    </div>
</div>
<!-- History Modal End-->
<script src="<?php echo CDN_PATH; ?>js/reports/nothi_name_update.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jQuery-Mask/jquery.mask.min.js"></script>

<script>
    function resetForm() {
        $("#office-unit-id").select2('val', '');
        $("#searchFor_nothi_subject").val('');
        $("#searchFor_nothi_no").val('');
        $("#addData").text('');
    }
</script>