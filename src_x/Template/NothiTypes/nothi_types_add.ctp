<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>ধরন তৈরি
        </div>
         <div class="tools">
            <a  href="<?=
            $this->Url->build([
                'action' => 'nothiTypes'])
            ?>"><button class="btn purple btn-sm margin-bottom-5 round-corner-5"  > ধরনের তালিকা  </button></a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <?= $this->Form->create($nothiTypes_records, ['type' => 'post', 'class' => 'form-horizontal']); ?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">বিষয়ের ধরন</label>

                <div class="col-md-4">
                    <div class="input-group">
                        <?= $this->Form->input('type_name', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'বিষয়ের ধরন']); ?>
	                    <a href="" style="text-decoration: none;float: right;margin-right: -25px;margin-top: 7px;" title="সচিবালয়ের নির্দেশমালা" data-target="#SecretariatGuidelinesModalPage73" data-toggle="modal"><i class="efile-help4"></i></a>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">কোড</label>

                <div class="col-md-4">
                    <div class="input-group">
                        <?= $this->Form->input('type_code', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => '২ ডিজিটের ধরন কোড','maxlength' => 2,'minlength' => 2]); ?>
                    </div>
                </div>
            </div>
            <div class="form-group hidden">
                <label class="col-md-3 control-label">নথির সর্বশেষ নম্বর</label>

                <div class="col-md-4">
                    <div class="input-group">
                       
                        <?= $this->Form->input('type_last_number', ['label' => false, 'class' => 'form-control input-circle', 'type' => 'text', 'placeholder' => 'নথির সর্বশেষ নাম্বার']); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn blue round-corner-5">সংরক্ষণ করুন</button>
                </div>
            </div>
        </div>
        <?= $this->Form->input('office_id', ['label' => false, 'type' => 'hidden', 'value' => $office_id]); ?>
        <?= $this->Form->input('office_unit_id', ['label' => false, 'type' => 'hidden', 'value' => $office_unit_id]); ?>
        <?= $this->Form->end(); ?>
    </div>
</div>

<div id="SecretariatGuidelinesModalPage73" class="modal fade modal-purple" data-backdrop="static" role="dialog">
	<div class="modal-dialog" style="width: 99%;">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">সচিবালয়ের নির্দেশমালা</h4>
			</div>
			<div class="modal-body text-center">
				<img src="<?= $this->request->webroot ?>img/bangladesh_secretariat_page73.png" alt="Image" style="width:100%">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default red round-corner-5" data-dismiss="modal">বন্ধ করুন</button>
			</div>
		</div>

	</div>
</div>