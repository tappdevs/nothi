<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class=""></i>নথির ধরন
        </div>
         <div class="tools">
             <?php
                if(!empty($for) && $for == 'admin'){
                                ?>
              <a  href="<?=
            $this->Url->build([
                'action' => 'superAdminOption'])
            ?>"><button class="btn   purple btn-sm margin-bottom-5"  > নথির বিষয়ের ধরন তালিকা  </button></a>
                                    <?php
                }else{
                    ?>
                    <a  href="<?=
            $this->Url->build([
                'action' => 'nothiTypes'])
            ?>"><button class="btn   purple btn-sm margin-bottom-5"  > নথির বিষয়ের ধরন তালিকা  </button></a>
            <?php
                }
             ?>
         
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <?= $this->Form->create($nothiTypes_records, ['type' => 'post', 'class' => 'form-horizontal']);

        $nothiTypes_records->type_code = str_pad($nothiTypes_records->type_code,2,'0',STR_PAD_LEFT);

        ?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">বিষয়ের ধরন</label>

                <div class="col-md-4">
                    <div class="input-group">

                        <?= $this->Form->hidden('id') ?>
                        <?= $this->Form->input('type_name', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'বিষয়ের  ধরন']); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">কোড</label>

                <div class="col-md-4">
                    <div class="input-group">

                        <?= $this->Form->hidden('id') ?>
                        <?= $this->Form->input('office_id', ['label' => false, 'type' => 'hidden', 'value' => $office_id]); ?>
                        <?= $this->Form->input('office_unit_id', ['label' => false, 'type' => 'hidden', 'value' => $office_unit_id]); ?>
                        <?php if ($count > 0) { ?>
                            <?= $this->Form->input('type_code', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => '**', 'readonly']); ?>
                        <?php } else { ?>
                                <?= $this->Form->input('type_code', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => '২ ডিজিটের ধরন কোড','maxlength' => 2,'minlength'=>2]); ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="form-group hidden">
                    <label class="col-md-3 control-label">নথির সর্বশেষ নম্বর</label>

                    <div class="col-md-4">
                        <div class="input-group">


                            <?= $this->Form->input('type_last_number', ['label' => false, 'class' => 'form-control input-circle', 'type' => 'text', 'placeholder' => 'নথির সর্বশেষ নাম্বার']); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn   blue">সংরক্ষণ করুন</button>
                        <?php
                            if(empty($for)){
                                ?>
                         <a class="btn   red" href="<?= $this->URL->build(['action' => 'nothiTypes']); ?>">বাতিল করুন</a>
                        <?php
                            }else if(!empty($for) && $for == 'admin'){
                                ?>
                          <a class="btn   red" href="<?= $this->URL->build(['action' => 'superAdminOption']); ?>">বাতিল করুন</a>
                                    <?php
                            }
                        ?>
                       
                    </div>
                </div>
            </div>
            <?= $this->Form->end(); ?>
    </div>
</div>
