<style>
    #nothi_number::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #3a3a3a !important;
        opacity: 1; /* Firefox */
    }
</style>
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i> নথির তথ্য সংশোধন  </div>
    </div>
    <div class="portlet-body">
        <?= $officeSelectionCell = $this->cell('OfficeSelectionByMinistry', ['entity' => '', 'prefix' => '']) ?>
        <div class="row">
            <div class="form-group">
                <div class="col-md-4 form-group form-horizontal">
                    <?php
                    echo $this->Form->input('office_id',
                        array(
                            'label' => __('Office'),
                            'class' => 'form-control',
                            'empty' => '--বাছাই করুন--'
                        ));
                    ?>
                </div>
                <div class="col-md-4 form-group form-horizontal">
                    <?php
                    echo $this->Form->create('', ['class' => 'searchForm','id' =>'nothiNameUpdate','url' => ['action'=>'SuperAdminNothiDetailUpdates'] ])
                    ?>
                    <?php
                    echo $this->Form->input('office_unit_id',
                        array(
                            'label' => 'শাখা',
                            'type' => 'select', 'class' => 'form-control',
                            'options' => ['0' => ' --বাছাই করুন-- ' ]));
                    ?>
                </div>
            </div>
        </div>

        <?php echo $this->Form->end() ?>
        <br/>
        <div class="inbox-content">
            <div id="showlist"></div><br>
            <div class="table-container ">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                        <tr class="heading">
                            <th class="text-center" > নথি নম্বর </th>
                            <th class="text-center" > শিরোনাম </th>
                            <th class="text-center"> শ্রেণি </th>
                            <th class="text-center"> নথির ধরন </th>
                            <th class="text-center"> তৈরির তারিখ </th>
                            <th class="text-center"> <?= __('Actions') ?> </th>
                        </tr>
                    </thead>
                    <tbody id="addData">
                        
                    </tbody>
                </table>
            </div>
        </div>
        <input type="hidden" value="" id="hidden_type">
        <input type="hidden" value="" id="hidden_type_id">
        <input type="hidden" value="" id="hidden_type_name">
        <input type="hidden" value="" id="hidden_unit">
        <input type="hidden" value="" id="hidden_unit_name">
    </div>
</div>
<div id="myModal" class="modal fade modal-purple " role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" >নথির তথ্য সংশোধন
                    <a type="button" id="nothi_change_history" class="btn btn-sm btn-info pull-right" >পূর্বের পরিবর্তনসমূহ দেখুন</a>
                </h4>
            </div>
            <div class="modal-body" >
                    <div class=" form form-group">
                         <input type="hidden" id="id_up">
                         <label class="control-label"> বর্তমান নাম </label>
                        <input type="text" id="cur_name" class="form-control">
                         <label class="control-label"> নতুন নাম </label>
                        <input type="text" id="new_name" class="form-control">
                        <label class="control-label"> শ্রেণি </label>
                        <select name="class_id" id="class_id" class="form-control">
                            <option value="1">ক</option>
                            <option value="2">খ</option>
                            <option value="3">গ</option>
                            <option value="4">ঘ</option>
                        </select>
                        <label class="control-label"> শাখা </label>
                        <?php
                        echo $this->Form->input('office_unit_id_modal',
                            array('label' => false, 'type' => 'select', 'class' => 'form-control',
                                'options' => [0 => ' -- শাখা নির্বাচন করুন -- ' ]));
                        ?>
                        <label class="control-label"> নথির ধরন </label>
                        <select name="type_id" id="type_id" class="form-control">
                            <option value="0">--ধরন নির্বাচন করুন--</option>
                        </select>
                        <label class="control-label"> বর্তমান নথি নম্বর </label>
                        <input type="text" id="old_nothi_number" class="form-control" >
                        <label class="control-label"> নতুন নথি নম্বর </label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group" style="width: 100%;">
                                    <input type="text" id="nothi_number" class="form-control" maxlength="24">
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="submitButton"> <?= __('Submit') ?> </button>
                <button type="button" class="btn btn-default red" data-dismiss="modal"><?= __("Close") ?></button>
            </div>
        </div>
        <!-- Modal End -->




    </div>
</div>
<!-- History Modal -->
<div id="nothi_data_history_modal" class="modal fade modal-purple" role="dialog">
    <div class="modal-dialog modal-full">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">নথির পূর্বের পরিবর্তনসমূহ</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                    <tr class="heading">
                        <th class="text-center" > ক্রমিক নম্বর </th>
                        <th class="text-center" > নথি নম্বর </th>
                        <th class="text-center" > শিরোনাম </th>
                        <th class="text-center"> শাখা </th>
                        <th class="text-center"> শ্রেণি </th>
                        <th class="text-center"> নথির ধরন </th>
                        <th class="text-center"> সময় </th>
                    </tr>
                    </thead>
                    <tbody id="addHistoryData">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default red" data-dismiss="modal">বন্ধ করুন</button>
            </div>
        </div>

    </div>
</div>
<!-- History Modal End-->
<script src="<?php echo CDN_PATH; ?>js/reports/super_admin_nothi_details_update.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jQuery-Mask/jquery.mask.min.js"></script>
<script>
    var prefix = '';

    $("#" + prefix + "office-ministry-id").bind('change', function () {
        OfficeOrgSelectionCell.loadMinistryWiseLayers(prefix);
    });
    $("#" + prefix + "office-layer-id").bind('change', function () {
        OfficeOrgSelectionCell.loadMinistryAndLayerWiseOfficeOrigin(prefix);
    });
    $("#" + prefix + "office-origin-id").bind('change', function () {
        OfficeOrgSelectionCell.loadOriginOffices(prefix);
    });
    $("#" + prefix + "office-id").bind('change', function () {
        OfficeOrgSelectionCell.loadOfficeUnits(prefix);
    });
    $("#" + prefix + "cell-office-unit-id").bind('change', function () {
        // OfficeOrgSelectionCell.loadPermissionList(prefix);
    });

</script>
<script type="text/javascript">
    var OfficeOrgSelectionCell = {

        loadMinistryWiseLayers: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeSettings/loadLayersByMinistry',
                {'office_ministry_id': $("#" + prefix + "office-ministry-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-layer-id", response, "--বাছাই করুন--");
                });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeSettings/loadOfficesByMinistryAndLayer',
                {
                    'office_ministry_id': $("#" + prefix + "office-ministry-id").val(),
                    'office_layer_id': $("#" + prefix + "office-layer-id").val()
                }, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-origin-id", response, "--বাছাই করুন--");
                });
        },
        loadOriginOffices: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/loadOriginOffices',
                {'office_origin_id': $("#" + prefix + "office-origin-id").val()}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", response, "--বাছাই করুন--");
                });
        },
        loadOfficeUnits: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/loadOfficeUnits',
                {'office_id': $("#" + prefix + "office-id").val()}, 'html',
                function (response) {
                    $("#" + prefix + "office-unit-id").html(response);
                    $("#" + prefix + "office-unit-id-modal").html(response);
                    // PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-unit-id", response, "--বাছাই করুন--");

                });
        },
        loadPermissionList: function () {
//
        }
    };
    $(document).ready(function() {
        $("select").select2();
    });
</script>

