<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i><?php echo __("Event List"); ?></div>

    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <?= $this->Html->link(__('Add New Event'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown"><?php echo __("Tools"); ?> <i
                                class="glyphicon glyphicon-chevron-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#"><?php echo __("Print"); ?></a></li>
                            <li><a href="#"><?php echo __("Save as PDF"); ?></a></li>
                            <li><a href="#">Export to Excel</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center"><?php echo __("ID"); ?></th>
                <th class="text-center"><?php echo __("Title (Bangla)"); ?></th>
                <th class="text-center"><?php echo __("Title (English)"); ?></th>
                <th class="text-center"><?php echo __("Template"); ?></th>
                <th class="text-center"><?php echo __("Status"); ?></th>
                <th class="text-center"><?php echo __("Actions"); ?></th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach ($query as $rows):
            ?>
            <tr>
                <td class="text-center"><?php echo $rows['id']; ?></td>
                <td class="text-center"><?php echo $rows['event_name_bng']; ?></td>
                <td class="text-center"><?php echo $rows['event_name_eng']; ?></td>
                <td class="text-center"><?php echo $rows['template_id']; ?></td>
                <td class="text-center"><?php echo $rows['status']; ?></td>
                <td class="text-center">
                    <?= $this->Form->postLink(
                        'Delete',
                        ['action' => 'delete', $rows->id],
                        ['class' => 'btn btn-danger'],
                        ['confirm' => 'Are you sure to delete this item?'])
                    ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rows->id], ['class' => 'btn btn-primary']) ?>
                    <?= $this->Html->link('View', ['action' => 'view', $rows->id], ['class' => 'btn green']) ?>

                </td>
                <?php
                endforeach;
                ?>
            </tr>
            </tbody>
        </table>
    </div>
</div>
