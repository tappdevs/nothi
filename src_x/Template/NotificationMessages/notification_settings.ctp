<script>
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
</script>
<!-- BEGIN PAGE HEAD -->
<?php $i = 0; ?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">নোটিফিকেশন</div>
	    <div class="actions">
		    <a class="btn btn-sm green" href="<?php echo $this->Url->build(['controller' => 'Dashboard', 'action' => 'dashboard']) ?>"><i class="fa fa-home"></i> হোম </a>
	    </div>
    </div>
    <div class="portlet-body">
        <form id="notification_settings" method="post" onsubmit="return false;">
            <table class="table table-bordered table-striped">
                <tr>
                    <td></td>
                    <!--<td>নোটিফিকেশন বার</td>-->
                    <td>ইমেইল</td>
                    <td>এস এম এস</td>
                    <!--<td> মোবাইল অ্যাপ </td>-->
                </tr>

                <?php
                if (!empty($eventTabs[1])) {
                    foreach ($eventTabs[1] as $key => $value):
//                        pr($supermanSetting[$value['id']]);
                        if ($value['notification_type'] == 'Dak.waiting') {
                            continue;
                        }
                        ?>
                        <tr>
                            <td><?php echo $value['event_name_bng'] ?></td>
                            <!--<td><?php //echo $this->Form->input('system_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['system']) ? (true) : (false))]);  ?></td>-->
                            <?php
                            if ( empty($supermanSetting[$value['id']]['email']) || $supermanSetting[$value['id']]['email'] == 0) {
                                $i = 1;
                                ?>
                            <td><div class="hidden"><?php echo $this->Form->input('email_'.$value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes','data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['email'])? (true) : (false))  ]); ?></div></td>
                        <?php
                    }else{
                        ?>
                                <td><?php echo $this->Form->input('email_'.$value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes','data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['email'])? (true) : (false)) ]); ?></td>
                            <?php
                    }
                    ?>
                               <?php
                            if (empty($supermanSetting[$value['id']]['sms']) || $supermanSetting[$value['id']]['sms'] == 0) {
                                $i = 1;
                                ?>
                                <td><div class="hidden"><?php echo $this->Form->input('sms_'.$value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['sms'])? (true) : (false))  ]); ?></div></td>
                        <?php
                    }else{
                        ?>
                               <td><?php echo $this->Form->input('sms_'.$value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes','data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['sms'])? (true) : (false))]); ?></td>
                            <?php
                    }
                    ?>
                               <?php echo $this->Form->hidden('mobileapp_'.$value['id'],[ 'value' => 1]); ?>
<!--                            <td><?php // echo $this->Form->input('mobileapp_'.$value['id'],['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['mobile_app'])? (true) : (false))]); ?></td>-->
                        </tr>
        <?php
    endforeach;
}

if (!empty($eventTabs[2])) {
    foreach ($eventTabs[2] as $key => $value):
        if ($value['notification_type'] == 'Dak.waiting') {
            continue;
        }
        ?>
                        <tr>
                            <td><?php echo $value['event_name_bng'] ?></td>
                            <!--<td><?php // echo $this->Form->input('system_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['system']) ? (true) : (false))]);  ?></td>-->
                                            <?php
                            if (empty($supermanSetting[$value['id']]['email']) || $supermanSetting[$value['id']]['email'] == 0) {
                                $i = 1;
                                ?>
                            <td><div class="hidden"><?php echo $this->Form->input('email_'.$value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes','data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['email'])? (true) : (false))  ]); ?></div></td>
                        <?php
                    }else{
                        ?>
                                <td><?php echo $this->Form->input('email_'.$value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes','data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['email'])? (true) : (false)) ]); ?></td>
                            <?php
                    }
                    ?>
                               <?php
                            if (empty($supermanSetting[$value['id']]['sms']) ||  $supermanSetting[$value['id']]['sms'] == 0) {
                                $i = 1;
                                ?>
                                <td><div class="hidden"><?php echo $this->Form->input('sms_'.$value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['sms'])? (true) : (false))  ]); ?></div></td>
                        <?php
                    }else{
                        ?>
                               <td><?php echo $this->Form->input('sms_'.$value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes','data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['sms'])? (true) : (false))]); ?></td>
                            <?php
                    }
                    ?>
                               <?php echo $this->Form->hidden('mobileapp_'.$value['id'],[ 'value' => 1]); ?>
<!--                            <td><?php // echo $this->Form->input('mobileapp_'.$value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes','data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['mobile_app'])? (true) : (false))]); ?></td>-->
                        </tr>

        <?php
    endforeach;
}
?>
            </table>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-4 col-sm-offset-4 col-xs-offset-4 col-md-8 col-sm-8 col-xs-8">
                        <button type="submit" class="btn blue btn-settings"><i class="fa fa-check"></i> <?php echo __("Submit") ?></button>
                    </div>
                </div>
            </div>
<?php echo $this->Form->end() ?>
        </form>
        <div class="margin-top-20">
            <?php
            if($i > 0){
                echo "<div class=''>   ** <b>কিছু নোটিফিকেশন সেটিং সুপার এডমিন কর্তৃক সংরক্ষিত </b> </div> " ;
            }
            ?>
        </div>
    </div>
</div>

<script>
    $(document).find('.make-switch').bootstrapSwitch();
    $(document).off('click', '.btn-settings');
    $(document).on('click', '.btn-settings', function () {

        $.ajax({
            url: '<?php echo $this->Url->build(['controller' => 'NotificationMessages', 'action' => 'saveSettings']) ?>',
            data: $('#notification_settings').serialize(),
            type: 'post',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.status == 'success') {
                    toastr.success(response.msg);
                } else {
                    toastr.error(response.msg);
                }
            }
        })

    })
</script>