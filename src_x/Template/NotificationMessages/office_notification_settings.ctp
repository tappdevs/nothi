
<!-- BEGIN PAGE HEAD -->

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
           <?= __('Office') . " :<b> " . $officeName . "</b>"?>
        </div>
    </div>
    <div class="portlet-body">
        <form id="notification_settings" method="post" onsubmit="return false;">
            <?= $this->Form->hidden('officeId',[ 'value' =>$officeId]); ?>
            <table class="table table-bordered table-striped">
                <tr>
                    <td></td>
                    <!--<td>নোটিফিকেশন বার</td>-->
                    <td>ইমেইল</td>
                    <td>এস এম এস</td>
                    <td> মোবাইল অ্যাপ </td>
                </tr>

                <?php
                if (!empty($eventTabs[1])) {
                    foreach ($eventTabs[1] as $key => $value):
                        if($value['notification_type'] == 'Dak.waiting'){
                            continue;
                        }
                        ?>
                        <tr>
                            <td><?php echo $value['event_name_bng'] ?></td>
                            <!--<td><?php //echo $this->Form->input('system_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['system']) ? (true) : (false))]); ?></td>-->
                            <td><?php echo $this->Form->input('email_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['email']) ? (true) : (false))]); ?></td>
                            <td><?php echo $this->Form->input('sms_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['sms']) ? (true) : (false))]); ?></td>
                            <td><?php echo $this->Form->input('mobileapp_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['mobile_app']) ? (true) : (false))]); ?></td>
                        </tr>
                        <?php
                    endforeach;
                }

                if (!empty($eventTabs[2])) {
                    foreach ($eventTabs[2] as $key => $value):
                       if($value['notification_type'] == 'Dak.waiting'){
                            continue;
                        }
                        ?>
                        <tr>
                            <td><?php echo $value['event_name_bng'] ?></td>
                            <!--<td><?php // echo $this->Form->input('system_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['system']) ? (true) : (false))]); ?></td>-->
                            <td><?php echo $this->Form->input('email_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['email']) ? (true) : (false))]); ?></td>
                            <td><?php echo $this->Form->input('sms_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['sms']) ? (true) : (false))]); ?></td>
                            <td><?php echo $this->Form->input('mobileapp_' . $value['id'], ['type' => 'checkbox', 'label' => false, 'class' => 'make-switch', 'data-on-text' => 'Yes', 'data-off-text' => 'No', 'value' => 1, 'checked' => (!empty($existingSetting[$value['id']]['mobile_app']) ? (true) : (false))]); ?></td>
                        </tr>

                        <?php
                    endforeach;
                }
                ?>
            </table>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-4 col-sm-offset-4 col-xs-offset-4 col-md-8 col-sm-8 col-xs-8">
                        <button type="submit" class="btn blue btn-settings"><i class="fa fa-check"></i> <?php echo __("Submit") ?></button>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end() ?>
        </form>
    </div>
</div>

<script>
    $(document).find('.make-switch').bootstrapSwitch();
    $(document).off('click', '.btn-settings');
    $(document).on('click', '.btn-settings', function () {

        $.ajax({
            url: '<?php echo $this->Url->build(['controller' => 'NotificationMessages', 'action' => 'officeSaveSettings']) ?>',
            data: $('#notification_settings').serialize(),
            type: 'post',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.status == 'success') {
                    toastr.success(response.msg);
                } else {
                    toastr.error(response.msg);
                }
            }
        })

    })
</script>