<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            অফিসভিত্তিক শাখা ও পদবি সংশোধন সেটিং
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label">মন্ত্রণালয়</label>
                <?php echo $this->Form->input('office_ministry_id', array('empty' => '--বাছাই করুন--', 'options' => $officeMinistries, 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'মন্ত্রণালয়')); ?>
            </div>
            <div class="col-md-4 form-group form-horizontal">
                <label class="control-label">মন্ত্রণালয়/বিভাগ</label>
                <?php echo $this->Form->input('office_layer_id', array('empty' => '--বাছাই করুন--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তরের স্তর  ')); ?>
            </div>
        </div>
        <div class="row" id="showlist">
            
        </div>
    </div>
</div>
<script>
    var EmployeeAssignment = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

            });
        }
    };

    $(function () {

        if ($("#office-layer-id").val() > 0) {
            getNotification($("#office-ministry-id").val(), $("#office-layer-id").val());
        }

        $("#office-ministry-id").bind('change', function () {
            EmployeeAssignment.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            getNotification($("#office-ministry-id").val(), $(this).val());

        });
        function getNotification(ministryId, layerId) {
             $('#showlist').html('<img src="<?= CDN_PATH?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
            $.ajax({
                type: 'POST',
                async: true,
                url: "<?php echo $this->Url->build(['controller' => 'officeSettings', 'action' => 'loadAllOfficesByMinistryAndLayer']) ?>",
                data: {"office_ministry_id": ministryId, "office_layer_id": layerId},
                success: function (data) {
                    $('#showlist').html('<table class="table table-hover table-striped table-bordered"><thead><tr><th>ক্রম</th><th>অফিসের নাম</th><th>শাখা সংশোধন</th><th>পদবি সংশোধন</th><th>কার্যক্রম</th></tr></thead><tbody></tbody></table>');
                    var flag = 0;
                    $.each(data, function(key, value) {
                        flag++;
                        var newTr = '<tr id="tr_'+key+'">' +
                            '<td>'+flag+'</td>' +
                            '<td>'+value['name']+'</td>';
                        if (value['setting'] == 1) {
                            newTr += '<td><input type="checkbox" name="sakhaEdit" class="make-switch" data-on-text = "Yes" data-off-text = "No" value="2" checked></td>' +
                                     '<td><input type="checkbox" name="organogramEdit" class="make-switch" data-on-text = "Yes" data-off-text = "No" value="3"checked></td>';
                        } else if (value['setting'] == 2) {
                            newTr += '<td><input type="checkbox" name="sakhaEdit" class="make-switch" data-on-text = "Yes" data-off-text = "No" value="2" checked></td>' +
                                '<td><input type="checkbox" name="organogramEdit" class="make-switch" data-on-text = "Yes" data-off-text = "No" value="3"></td>';
                        } else if (value['setting'] == 3) {
                            newTr += '<td><input type="checkbox" name="sakhaEdit" class="make-switch" data-on-text = "Yes" data-off-text = "No" value="2"></td>' +
                                '<td><input type="checkbox" name="organogramEdit" class="make-switch" data-on-text = "Yes" data-off-text = "No" value="3" checked></td>';
                        } else {
                            newTr += '<td><input type="checkbox" name="sakhaEdit" class="make-switch" data-on-text = "Yes" data-off-text = "No" value="2"></td>' +
                                '<td><input type="checkbox" name="organogramEdit" class="make-switch" data-on-text = "Yes" data-off-text = "No" value="3"></td>';
                        }
                        newTr += '<td><input type="submit" class="btn btn-sm btn-success" id="btnSave" value="সংরক্ষণ" onclick="submitData('+key+')"></td></tr>';
                        $('#showlist tbody').append(newTr);
                    });
                    $(document).find('.make-switch').bootstrapSwitch();
                }
            });
        }
    });
    function submitData(officeId) {
        var unit_organogram_edit_option = 0;
        if ($("#tr_"+officeId).find('input[name=sakhaEdit]').is(':checked') && $("#tr_"+officeId).find('input[name=organogramEdit]').is(':checked')) {
            unit_organogram_edit_option = 1;
        } else if ($("#tr_"+officeId).find('input[name=sakhaEdit]').is(':checked') && !$("#tr_"+officeId).find('input[name=organogramEdit]').is(':checked')) {
            unit_organogram_edit_option = 2;
        } else if (!$("#tr_"+officeId).find('input[name=sakhaEdit]').is(':checked') && $("#tr_"+officeId).find('input[name=organogramEdit]').is(':checked')) {
            unit_organogram_edit_option = 3;
        }
        //console.log(unit_organogram_edit_option);
        $.ajax({
            type: 'POST',
            async: true,
            url: "<?php echo $this->Url->build(['controller' => 'NotificationOfficeSettings', 'action' => 'supermanOfficeSectionOrganogramSettingsSave']) ?>",
            data: {"office_id": officeId, "unit_organogram_edit_option": unit_organogram_edit_option},
            success: function (data) {
                console.log(data);
                if (data.status == 'success') {
                    toastr.success(data.msg);
                } else {
                    toastr.success(data.msg);
                }
            }
        });
    }
</script>
