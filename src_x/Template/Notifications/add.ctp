<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_edit2"></i><?php echo __('Event') ?> <?php echo __('Create New') ?></div>

    </div>
    <div class="portlet-body form"><br><br>

        
        <?php echo $this->Form->create(); ?>
        <?php echo $this->element('Notifications/add'); ?>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-4 col-md-9">
                    <button class="btn   blue" type="submit"><?php echo __('Submit') ?></button>
                    <button class="btn   default" type="reset"><?php echo __('Reset') ?></button>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

