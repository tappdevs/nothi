<style>
    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio]{
        margin-left: -10px;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i>দপ্তর কর্মকর্তার তালিকা
                </div>
            </div>
            <div class="portlet-body">
                
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th class="text-center"></th>
                            <th class="text-center">নাম</th>
                            <th class="text-center">পদবি</th>
                            <th class="text-center">সেকশন</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        foreach ($results as $rows):
                            ?>
                            <tr>
                                <td class=""><input <?php echo ($rows['is_current']==1?'checked=checked':''); ?> type="radio" name="front_Desk" value="<?php echo $rows['id']; ?>" /></td>
                                <td class=""><?php echo h($rows['name_bng']); ?></td>
                                <td class="text-center"><?php echo $rows['designation']; ?></td>
                                <td class="text-center"><?php echo $rows['unit_name_bng']; ?></td>
                            </tr>
                            <?php
                        endforeach;
                        ?>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4"><input type="button" class="btn btn-primary" id="assign_fron_desk" value="<?php echo __(SAVE) . ' করুন' ?>"/> </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script>

$(document).on('click','#assign_fron_desk', function(){
    var frondeskid = $('[name=front_Desk]:checked').val();
     Metronic.blockUI({
            target: '.portlet.box.green',
            boxed: true
        });
    $.ajax({
       url: '<?php echo $this->Url->build(['controller'=>'OfficeEmployeeMappings','action'=>'assignFronDesk']) ?>/'+frondeskid,
       method: 'post',
       success: function(res){
           Metronic.unblockUI('.portlet.box.green');
           if(res==1){
               toastr.success("সংরক্ষিত হয়েছে");
           }else{
               toastr.error("সংরক্ষণ করা সম্ভব হচ্ছে না");
           }
       }
    });
})
</script>