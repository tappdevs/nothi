<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>Employee Registration</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:;" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body form">
        <?php echo $this->Form->create(); ?>
        <div class="form-body">
            <?php echo $this->element('OfficeEmployees/personal_info'); ?>
            <?php echo $this->element('OfficeEmployees/professional_info'); ?>
            <?php echo $this->element('OfficeEmployees/office_info'); ?>
            <?php echo $this->element('OfficeEmployees/login_info'); ?>
        </div>
        <?php echo $this->element('submit'); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<script>
     $( "#EmployeeRecordForm" ).submit(function( event ) {
        if($("#nid").val().length != 17){
              event.preventDefault();
              toastr.error(' জাতীয় পরিচয়পত্র নম্বর ১৭ সংখ্যার হতে হবে। ');
        }
          if ($("#date-of-birth").val().length == 0) {
            event.preventDefault();
            toastr.error(' জন্ম তারিখ দেওয়া হয়নি । ');
        }
        });
</script>