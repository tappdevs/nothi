<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i>দপ্তর সেকশনের তালিকা
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <tr class="heading">
                        <th></th>
                        <th class="text-center" style="width:40%">পদবি</th>
                        <th class="text-center" style="width:50%">পদবির কার্যক্রম</th>
                    </tr>
                    <tbody>
                    <tr class="odd gradeX">
                        <?php
                        $i = 0;
                        foreach ($allOrganogram as $rows):
                        ?>
                    <tr class="designationLabel">
                        <td class="text-center"><?php echo $this->Number->format(++$i) ?></td>
                        <td class="designation_id" id="<?php echo $rows['id'] ?>"><?php echo $rows['designation_bng'] . ', <b>' . $rows['OfficeUnits']['unit_name_bng'] . "</b>"; ?></td>
                       
                        <td class="text-left"><textarea  class="form-control text-left" name="designation_description"><?php echo ($rows['designation_description']); ?></textarea></td>
                        
                        <?php
                        endforeach;
                        ?>
                    </tr>
                    <tr class="form-actions">
                        <td colspan="4" class="text-center">
                            <input type="button" class="btn green saveDesignation" value="<?php echo __(SAVE); ?>" />
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    
    
    $(document).on('click','.saveDesignation', function(){
        var i =0;
        Metronic.blockUI({
            target: '.portlet.box.green',
            boxed: true
        });
        $.each($('.designationLabel'), function(){
            var data = {
                id:$(this).find('.designation_id').attr('id'),
                designation_description: $(this).find('[name=designation_description]').val(),
            };
            
            $.ajax({
                url: '<?php echo $this->Url->build(['controller'=>'officeEmployees','action'=>'officeEmployeesDesignationUpdate']) ?>',
                data: data,
                method: 'post',
                success: function(res){
                    i++;
                    if($('.designationLabel').length==i){
                        toastr.success("সংরক্ষিত হয়েছে");
                        Metronic.unblockUI('.portlet.box.green');
                    }
                }
            })
        })
        
        
    });
    
 </script>