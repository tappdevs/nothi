                    <table class="table table-striped table-bordered table-hover" >
                        <tr class="heading">
                            <th rowspan="2"></th>
                            <th class="text-center" rowspan="2">নাম </th>
                            <th class="text-center" colspan="2">পদবির নাম </th>
                            <th class="text-center" rowspan="2"> শাখা</th>
                            <th class="text-center" rowspan="2">অফিস </th>
                            <th class="text-center" rowspan="2">কার্যক্রম </th>
                        </tr>
                        <tr class="heading">
                            <th  class="text-center"> বাংলা </th>
                            <th  class="text-center"> ইংরেজি</th>

                        </tr>

                        <tbody class="designationLabel">
                            <?php
                            $i = $page;
                            foreach ($allOrganogram as $rows):

                                $office_name = Cake\ORM\TableRegistry::get('Offices')->getBanglaName($rows['office']);
                                $unit_name   = Cake\ORM\TableRegistry::get('OfficeUnits')->getBanglaName($rows['unit']);
                                ?>
                                <tr >
                                    <td class="text-center"><?php echo $this->Number->format($i++) ?></td>
                                    <td class="text-center">
                                        <?php echo (!empty($rows['EmployeeRecords']['name_bng'])?$rows['EmployeeRecords']['name_bng']:(!empty($rows['EmployeeRecords']['name_eng'])?$rows['EmployeeRecords']['name_eng']:'')); ?>
                                    </td>
                                    <td class="text-center "><span id = "<?= "designation_bn".$rows['id'] ?>"><?php echo $rows['d_bng'] ?></span></td>
                                    <td class="text-center"><span id = "<?= "designation_en".$rows['id'] ?>"><?php echo $rows['d_eng'] ?></span></td>
                                    <td class="text-center" ><?php echo $unit_name['unit_name_bng']; ?></td>
                                    <td class="text-center" ><?php echo $office_name['office_name_bng']; ?></td>
                                    <td class="text-center">
                                        <a class="btn   btn-primary" data-toggle="modal" data-target="#yourModal" onclick="update(<?= $rows['id'] ?>);"><?= __('Edit') ?></a>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>

                        </tbody>
                    </table>

                    <div class="actions text-center">
                        <ul class="pagination pagination-sm">
                            <?php
                            echo $this->Paginator->first(__('প্রথম', true),
                                array('class' => 'number-first'));

                            echo $this->Paginator->prev('<<',
                                array('tag' => 'li', 'escape' => false),
                                '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
                                array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true,
                                'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => false));
                            echo $this->Paginator->next('>>',
                                array('tag' => 'li', 'escape' => false),
                                '<a href="#" class="btn btn-sm blue">&laquo;</a>',
                                array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
                            echo $this->Paginator->last(__('শেষ', true),
                                array('class' => 'number-last'));
                            ?>
                        </ul>

                    </div>