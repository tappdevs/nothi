<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i><?php echo __("Office"); ?>
            <?php echo __("Unit"); ?>
            <?php echo __("Information"); ?>
            <?php echo __("Edit"); ?> </div>
         <div class="tools">
            <a  href="<?=
                $this->Url->build(['controller' => 'OfficeEmployees',
                   'action' => 'officeUnitNothiCodes'])
               ?>"><button class="btn btn-sm   purple margin-bottom-5"  style="margin-top: -5px;">   শাখা তথ্য সংশোধন তালিকা   </button></a>
        </div>

    </div>
    <div class="portlet-body">
       <div class="form">
            <?php
           
            echo $this->Form->create($entity, ['method' => 'post', 'id' => 'OfficeUnitForm','url' => ['controller' => 'OfficeManagement','action' => 'editOfficeUnit']]);

            echo $this->Form->hidden('id');
            
            ?>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">শাখার নাম (বাংলা)</label>
                        <?= $this->Form->input('unit_name_bng', ['label' => false, 'class' => 'form-control', 'type' => 'text','readonly'=>true]); ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">শাখার নাম (ইংরেজি)</label>
<?= $this->Form->input('unit_name_eng', ['label' => false, 'class' => 'form-control', 'type' => 'text','placeholder'=>'Branch name in English']); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">ঊর্ধ্বতন শাখা</label>
                        <?= $this->Form->input('parent_unit_id', ['label' => false, 'class' => 'form-control', 'type' => 'select','options'=>array(0=>'ঊর্ধ্বতন অফিস')+$office_units,'required'=>'required']); ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">ধরন</label>
<?= $this->Form->input('office_unit_category', ['label' => false, 'class' => 'form-control', 'type' => 'select','options'=>$unitCategory]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">শাখা কোড</label>
                        <?= $this->Form->input('unit_nothi_code', ['label' => false, 'class' => 'form-control', 'type' => 'text']); ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">পত্রজারি স্মারক ক্রম </label>
<?= $this->Form->input('sarok_no_start', ['label' => false, 'class' => 'form-control', 'type' => 'text']); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label"> ক্রম </label>
<?= $this->Form->input('unit_level', ['label' => false, 'class' => 'form-control', 'type' => 'text']); ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label"> ইমেইল </label>
<?= $this->Form->input('email', ['label' => false, 'class' => 'form-control', 'type' => 'email','placeholder'=>'ইমেইল']); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label"> ফোন </label>
<?= $this->Form->input('phone', ['label' => false, 'class' => 'form-control', 'type' => 'text','placeholder'=>'ফোন']); ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label"> ফ্যাক্স </label>
<?= $this->Form->input('fax', ['label' => false, 'class' => 'form-control', 'type' => 'text','placeholder'=>'ফ্যাক্স']); ?>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-4 col-md-9">
                    <button class="btn   blue" type="submit"><?php echo __('Submit') ?></button>
                    <button class="btn   default" type="reset"><?php echo __('Reset') ?></button>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
