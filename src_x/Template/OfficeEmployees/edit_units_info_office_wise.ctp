<div class="row">
    <div class="col-md-12" >
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fs1 a2i_gn_edit2"></i>শাখাসমূহ
                    <input type="hidden" id="office-id" name="office_id" value="<?=$office_id?>">
                </div>
            </div>
            <div class="portlet-body">
                <div id="sample_1">
                    <div class="table-scrollable">
                        <table class="table table-bordered">
                            <thead>
                            <tr class="heading">
                                <th><?= __('Sequence')?></th>
                                <th>শাখা নাম ( বাংলা )</th>
                                <th>শাখা নাম ( ইংরেজি )</th>
                                <th><?= __('Actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($OfficeUnitsData)){
                                $ind = 1;
                                foreach($OfficeUnitsData as $val){
                                    ?>
                                    <tr>
                                        <td><?= enTobn($ind++); ?></td>
                                        <td id = "<?= "unit_bn".$val['id'] ?>" ><?= $val['unit_name_bng']?></td>
                                        <td id = "<?= "unit_en".$val['id'] ?>" ><?= $val['unit_name_eng']?></td>
                                        <td> <button class="btn btn-primary" data-toggle="modal" data-target="#yourModal" onclick="update(<?= $val['id'] ?>);"> <?= __('Change') ?> </button> </td>
                                    </tr>
                                    <?php
                                }
                            }else{
                                echo '<tr><td colspan= 4 class="text-center danger"> কোন তথ্য পাওয়া যায়নি। </td></tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>


                </div>
                <div class="modal fade" id="yourModal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">শাখা সংশোধন</h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-group">
                                    <label class="form-cntrol">শাখা নাম ( বাংলা )</label>
                                    <input type="text" class="form-control" id="update_text_bng">
                                </form>
                                <form class="form-group">
                                    <label class="form-cntrol">শাখা নাম ( ইংরেজি )</label>
                                    <input type="text" class="form-control" id="update_text_eng">
                                    <input type="hidden" class="form-control" id="update_id">
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" onclick="update_action();">সংরক্ষণ</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/edit_units_info.js?v=<?= js_css_version ?>" type="text/javascript"></script>

