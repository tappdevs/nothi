<div class="table-scrollable">
    <table class="table table-bordered">
        <thead>
            <tr class="heading">
                <th><?= __('Sequence')?></th>
                <th>শাখা নাম ( বাংলা )</th>
                <th>শাখা নাম ( ইংরেজি )</th>
                <th><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if(!empty($OfficeUnitsData)){
                $ind = 1;
                foreach($OfficeUnitsData as $val){
                    ?>
            <tr>
                <td><?= enTobn($ind++); ?></td>
                <td id = "<?= "unit_bn".$val['id'] ?>" ><?= $val['unit_name_bng']?></td>
                <td id = "<?= "unit_en".$val['id'] ?>" ><?= $val['unit_name_eng']?></td>
                <td> <button class="btn btn-primary" data-toggle="modal" data-target="#yourModal" onclick="update(<?= $val['id'] ?>);"> <?= __('Change') ?> </button> </td>
            </tr>
            <?php
                }
            }else{
                echo '<tr><td colspan= 4 class="text-center danger"> কোন তথ্য পাওয়া যায়নি। </td></tr>';
            }
            ?>
        </tbody>
    </table>
</div>

