<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>New Office (Global)</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create('OfficeHierarchyTemplate', array('id' => 'OfficeHierarchyTemplateForm')); ?>
        <div class="form-body">
            <div class="row">
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label">মন্ত্রণালয়</label>
                    <?php echo $this->Form->input('ministry_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'মন্ত্রণালয়')); ?>
                </div>
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label">মন্ত্রণালয়/বিভাগ</label>
                    <?php echo $this->Form->input('office_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তর / অধিদপ্তর ')); ?>
                </div>
                <div class="col-md-4 form-group form-horizontal">
                    <label class="control-label">দপ্তর / অধিদপ্তর </label>
                    <?php echo $this->Form->input('office_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তর / অধিদপ্তর ')); ?>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">বিভাগ </label>
                    <?php echo $this->Form->input('office_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তর / অধিদপ্তর ')); ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">জেলা </label>
                    <?php echo $this->Form->input('office_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তর / অধিদপ্তর ')); ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">উপজেলা </label>
                    <?php echo $this->Form->input('office_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'দপ্তর / অধিদপ্তর ')); ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">ইউনিয়ন</label>
                    <?php echo $this->Form->input('ministry_id', array('empty' => '--Select--', 'type' => 'select', 'label' => false, 'class' => 'form-control', 'placeholder' => 'মন্ত্রণালয়')); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group form-horizontal">
                    <label class="control-label">ঠিকানা</label>
                    <?php echo $this->Form->input('address', array('type' => 'textarea', 'label' => false, 'class' => 'form-control', 'placeholder' => 'ঠিকানা')); ?>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">ফোন</label>
                    <?php echo $this->Form->input('phone', array('label' => false, 'class' => 'form-control', 'placeholder' => 'ফোন')); ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">মোবাইল</label>
                    <?php echo $this->Form->input('mobile', array('label' => false, 'class' => 'form-control', 'placeholder' => 'মোবাইল')); ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">ফ্যাক্স</label>
                    <?php echo $this->Form->input('fax', array('label' => false, 'class' => 'form-control', 'placeholder' => 'ফ্যাক্স')); ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">ই-মেইল</label>
                    <?php echo $this->Form->input('email', array('label' => false, 'class' => 'form-control', 'placeholder' => 'ই-মেইল')); ?>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label"> রেফারেন্স কোড</label>
                    <?php echo $this->Form->input('ref_code', array('label' => false, 'class' => 'form-control', 'placeholder' => 'রেফারেন্স কোড')); ?>
                </div>
                <div class="col-md-3 form-group form-horizontal">
                    <label class="control-label">সিস্টেম কোড</label>
                    <?php echo $this->Form->input('gen_code', array('label' => false, 'class' => 'form-control', 'placeholder' => 'সিস্টেম কোড')); ?>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-4 col-md-9">
                        <button type="button" onclick="OfficeHierarchy.save()" class="btn   blue ajax_submit">
                            Submit
                        </button>
                        <button type="reset" class="btn   default">Reset</button>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<script type="text/javascript">
    var OfficeHierarchy =
    {
        save: function () {
            TreeView.saveNode($("#OfficeHierarchyTemplateForm").serialize());
        }
    }
</script>
