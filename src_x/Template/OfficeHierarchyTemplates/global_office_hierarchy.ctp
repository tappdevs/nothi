<div class="row">
    <div class="col-md-6">
        <div class="portlet green-meadow box">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i>Global Office Tree</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body">
                <div id="tree_div">

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6" id="update_div">
    </div>

</div>

<script type="text/javascript">
    var TreeView = {
        selected_node: "",
        parent_id: 0,
        parent_node: "",
        newNode: function (input) {
            $("#update_div").show('slow');
            var parent_id = $(input).attr('data-parent');
            var dataid = $(input).attr('data-id');
            TreeView.parent_id = parent_id;
            TreeView.selected_node = dataid;
            TreeView.parent_node = $(input).attr('data-parent-node');
            var url = "<?php echo $this->request->webroot;?>officeHierarchyTemplates/add?parent_id=" + parent_id;
            PROJAPOTI.ajaxLoadCallback(url, function (response) {
                $("#update_div").html(response);
            });
        },
        saveNode: function (formdata) {
            var url = "<?php echo $this->request->webroot;?>officeHierarchyTemplates/add?parent_id=" + TreeView.parent_id;
            PROJAPOTI.ajaxSubmitDataCallback(url, {formdata}, function (response) {
                $("#update_div").hide('slow');
                if (TreeView.parent_id == 0) {
                    location.reload();
                }
                else {
                    $("#tree_div").jstree("load_node", $('#' + TreeView.parent_node));
                }
            });
        }
    };

    $(function () {
        $('#tree_div').jstree({
            "core": {
                "themes": {
                    "variant": "large"
                },
                'data': {
                    'url': function (node) {
                        return node.id === '#' ?
                            'loadGlobalOfficeHierarchy' : 'loadGlobalOfficeHierarchy';
                    },
                    'data': function (node) {
                        return {'id': node.id, 'type': ""};
                    }
                }
            }
        });

    });
</script>