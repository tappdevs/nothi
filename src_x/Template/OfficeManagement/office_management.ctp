<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-check-square-o"></i><?php echo __('Office') ?> <?php echo __('Management') ?>
        </div>
    </div>
    <div class="portlet-window-body">
        <?= $cell = $this->cell('OfficeMinistryCombo', ['field_value' => ""]) ?>
        <hr/>
        <div class="row" id="">
            <div class="col-md-6">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-tree"></i><?php echo __('Office') ?> <?php echo __('Origin') ?>
                        </div>
                    </div>
                    <div class="portlet-window-body">
                        <div id="tree_panel"></div>
                    </div>
                </div>
            </div>
            <div id="data_panel" class="col-md-6">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fs1 a2i_gn_add1-circle"></i><?php echo __('Office') ?> <?php echo __('Create') ?>
                        </div>
                    </div>
                    <div class="portlet-window-body">
                        <div id="tree_panel">
	                        <?php if(Live == 0): ?>
                            <?php echo $this->Form->create('OfficeRecords', array('id' => 'OfficeRecordsForm'));
                            echo $this->Form->hidden('id');
                            echo $this->Form->hidden('office_ministry_id');
                            echo $this->Form->hidden('office_layer_id');
                            echo $this->Form->hidden('office_origin_id'); ?>
                            <div class="form-body">
                                <?php
                                $geo_field_values = array();
                                echo $geo_panel = $this->cell('GeoSelection', ['sorted_field_values' => $geo_field_values]); ?>

                                <div class="row">
                                    <div class="col-md-6 form-group form-horizontal">
                                        <label class="control-label">নাম  <span class="text-danger">*</span></label>
                                        <?= $this->Form->input('office_name_bng', ['label' => false, 'class' => 'form-control typeahead_office_name_bn', 'type' => 'text', 'placeholder' => 'নাম(বাংলায়)']); ?>
                                    </div>
                                    <div class="col-md-6 form-group form-horizontal">
                                        <label class="control-label">&nbsp;</label>
                                        <?= $this->Form->input('office_name_eng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(ইংরেজি)']); ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group form-horizontal">
                                        <label class="control-label">ঠিকানা</label>
                                        <?php echo $this->Form->input('office_address', array('type' => 'textarea', 'label' => false, 'class' => 'form-control', 'placeholder' => 'ঠিকানা')); ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 form-group form-horizontal">
                                        <label class="control-label">ফোন</label>
                                        <?php echo $this->Form->input('office_phone', array('label' => false, 'class' => 'form-control', 'placeholder' => 'ফোন')); ?>
                                    </div>
                                    <div class="col-md-6 form-group form-horizontal">
                                        <label class="control-label">মোবাইল</label>
                                        <?php echo $this->Form->input('office_mobile', array('label' => false, 'class' => 'form-control', 'placeholder' => 'মোবাইল')); ?>
                                    </div>
                                    <div class="col-md-6 form-group form-horizontal">
                                        <label class="control-label">ফ্যাক্স</label>
                                        <?php echo $this->Form->input('office_fax', array('label' => false, 'class' => 'form-control', 'placeholder' => 'ফ্যাক্স')); ?>
                                    </div>
                                    <div class="col-md-6 form-group form-horizontal">
                                        <label class="control-label">ই-মেইল</label>
                                        <?php echo $this->Form->input('office_email', array('label' => false, 'class' => 'form-control', 'placeholder' => 'ই-মেইল')); ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group form-horizontal">
                                        <label class="control-label">অফিস ওয়েবসাইট</label>
                                        <?php echo $this->Form->input('office_web', array('label' => false, 'class' => 'form-control', 'placeholder' => 'অফিস ওয়েবসাইট')); ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 form-group form-horizontal">
                                        <label class="control-label">ডিজিটাল নথি কোড  (প্রথম ৮টি)  <span class="text-danger">*</span></label>
                                        <?php echo $this->Form->input('digital_nothi_code', array('label' => false, 'class' => 'form-control font-bijoy-bangla', 'placeholder' => '**.**.****')); ?>
                                    </div>
                                    <script>
                                        $(document).ready(function() {
                                            $('#digital-nothi-code').mask('AA.AA.AAAA'
                                                ,{
                                                    translation:{
                                                        A: {pattern: /[\u09E6-\u09EF-0-9]/},
                                                    },
                                                    placeholder: "**.**.****"
                                                });
                                        });
                                    </script>
                                    <div class="col-md-6 form-group form-horizontal">
                                        <label class="control-label"> রেফারেন্স কোড</label>
                                        <?php echo $this->Form->input('reference_code', array('label' => false, 'class' => 'form-control', 'placeholder' => 'রেফারেন্স কোড')); ?>
                                    </div>
                                    <div class="col-md-0 col-xs-0 col-sm-0 col-lg-0 form-group form-horizontal">
                                        <!--<label class="control-label">অফিস কোড</label>-->
                                        <?php echo $this->Form->hidden('office_code', array('label' => false, 'class' => 'form-control', 'placeholder' => 'অফিস কোড')); ?>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    <div class=" form-group form-horizontal">
                                        <label class="control-label">উর্ধতন অফিস</label>
                                        <?= $this->Form->input('parent_office_id', ['empty' => '--বাছাই করুন--', 'label' => false, 'class' => 'form-control', 'type' => 'select']); ?>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-4 col-md-9">
                                            <?= $this->Form->button(__('Submit'), ['type' => 'button', 'onclick' => 'OfficeManagement.save()', 'class' => 'btn btn-primary']) ?>
                                            <?php echo $this->Html->link(__('Delete'), array(), array('onclick' => 'return confirm("Are you sure want to delete?"); OfficeManagement.delete()', 'class' => 'btn btn-danger')); ?>
                                            <?= $this->Form->button(__('Cancel'), ['class' => 'btn purple']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo $this->Form->end(); ?>
	                        <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="output_panel">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-tree"></i><?php echo __('Office') ?>
                </div>
            </div>
            <div class="portlet-window-body">
                <div id="tree_panel_output"></div>
            </div>
        </div>
    </div>
</div>

<!-- Start : Office Setup JS -->
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jQuery-Mask/jquery.mask.min.js"></script>
<script type="text/javascript">
    $(function () {
        OfficeManagement.init();
    });
</script>
<script src="/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/assets/admin/layout4/scripts/projapoti_autocomplete.js" type="text/javascript"></script>
<script src="/assets/global/plugins/typeahead/typeahead.bundle.js" type="text/javascript"></script>
<script src="/assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script type="text/javascript">

    var OfficeManagement = {
        checked_node_ids: [],

        selected_ministry_id: "",
        treeView: function () {
            OfficeManagement.selected_ministry_id = $("#office-ministry-id").val();
//            OfficeManagement.treeOfficeOutputView();
            $('#tree_panel').jstree("refresh");
            $('#tree_panel').jstree({
                "core": {
                    "themes": {
                        "variant": "large",
                        "multiple": false
                    },
                    'data': {
                        'url': function (node) {
                            return node.id === '#' ?
                            js_wb_root + 'officeOriginTree/loadRootNode' : js_wb_root + 'officeOriginTree/loadOfficeOriginTree';
                        },
                        'data': function (node) {
                            return {
                                'id': node.id,
                                'type': "checkbox",
                                'office_ministry_id': OfficeManagement.selected_ministry_id
                            };
                        }
                    }
                },
                "checkbox": {
                    "keep_selected_style": false,
                    "three_state": false
                },
                "plugins": ["checkbox"]
            });
        },

        treeOfficeOutputView: function () {
            OfficeManagement.selected_ministry_id = $("#office-ministry-id").val();
            OfficeManagement.parent_office_id = $('#tree_panel').jstree('get_checked');
            $("#tree_panel_output").html("");
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeOriginTree/loadOfficeTreeDataNew',
                {
                    'office_ministry_id': OfficeManagement.selected_ministry_id,
                    'parent_office_id': OfficeManagement.parent_office_id
                }, 'html',
                function (response) {
                    $("#tree_panel_output").html(response);
                });
        },

        save: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/saveOffice',
                {'formdata': $("#OfficeRecordsForm").serialize()}, 'json',
                function (response) {
                    if (response.status == 1) {
                        //load Office tree
                        toastr.success('অফিস তৈরি হয়েছে।');
                        OfficeManagement.treeOfficeOutputView();
                        PROJAPOTI.clearProjapotiForm("OfficeRecordsForm");
                    }
                    else {
                        toastr.error(response.message);
                    }
                });
        },

        delete: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/deleteOffice',
                {'formdata': $("#OfficeRecordsForm").serialize()}, 'json',
                function (response) {
                    if (response == 1) {
                        //load Office tree
                        OfficeManagement.treeOfficeOutputView();
                        $('#data_panel').find('form')[0].reset();
                        $("#data_panel").hide('slow');

                    }
                    else {
                        alert("Failed to delete.");
                    }
                });
        },

        enableEditMode: function (input) {
            var node_id = $(input).data('id');
            var node_id_arr = node_id.split("_");
            var office_id = node_id_arr.pop();
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/editOffice/' + office_id,
                "", 'json', function (response) {
                    if (response != 0) {
                        $.each(response, function (i) {
                            $("[name=" + i + "]").val(response[i]);
                        });
                        GeoSetup.loadDistricts(response.geo_division_id, response.geo_district_id);
                        $('select').select2();
                    }
                });
        },

        loadParentOffices: function () {
            var last_selected_node = OfficeManagement.checked_node_ids.pop();
            var ministry_id = $("#office-ministry-id").val();
            var last_selected_node_arr = last_selected_node.split("_");
            var office_origin_id = last_selected_node_arr.pop();
            $("input[name=office_origin_id]").val(office_origin_id);
            global_office_origin_id = office_origin_id;

            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/loadParentOfficesByMinistry',
                {'office_ministry_id': ministry_id}, 'json',
                function (response) {
                    PROJAPOTI.projapoti_dropdown_map("#parent-office-id", response, "----");
                    OfficeManagement.treeOfficeOutputView();
                });
        },

        init: function () {
            $("#data_panel").hide('slow');
            $("#office-ministry-id").bind('change', function () {
                OfficeManagement.treeView();
            });

            $('#tree_panel').bind("select_node.jstree", function (e) {
                OfficeManagement.checked_node_ids = $('#tree_panel').jstree('get_checked');
                if (OfficeManagement.checked_node_ids.length > 1) {
                    $('#tree_panel').jstree("deselect_node", "#" + OfficeManagement.checked_node_ids[0]);
                }

                OfficeManagement.loadParentOffices();
                $("#data_panel").show('slow');
            });

            $('#tree_panel').bind("deselect_node.jstree", function (e) {
                $("#data_panel").hide('slow');
            });
        }
    };

    var OfficeDataAdapter = new Bloodhound({
        datumTokenizer: function (d) {
            return d.tokens;
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        //remote: getBaseUrl() + 'officeManagement/autocompleteOfficeName?search_key=%QUERY&office_origin_id='+$("input[name=office_origin_id]").val()
        remote: {
            url: getBaseUrl()+'officeManagement/autocompleteOfficeName?office_origin_id=&search_key=%QUERY',
            replace: function () {
                var q = getBaseUrl()+'officeManagement/autocompleteOfficeName?office_origin_id=';

                q += encodeURIComponent($('input[name=office_origin_id]').val());

                var geo_division_id = $("#geo-division-id").val();
                if (geo_division_id > 0) {
                    q += '&geo_division_id='+encodeURIComponent(geo_division_id);
                }
                var geo_district_id = $("#geo-district-id").val();
                if (geo_district_id > 0) {
                    q += '&geo_district_id='+encodeURIComponent(geo_district_id);
                }
                var geo_upazila_id = $("#geo-upazila-id").val();
                if (geo_upazila_id > 0) {
                    q += '&geo_upazila_id='+encodeURIComponent(geo_upazila_id);
                }
                var geo_union_id = $("#geo-union-id").val();
                if (geo_union_id > 0) {
                    q += '&geo_union_id='+encodeURIComponent(geo_union_id);
                }
                q += '&search_key='+encodeURIComponent($('.typeahead_office_name_bn.tt-input').val());
                // console.log(q);
                return q;
            }
        }
    });
    OfficeDataAdapter.initialize();

    $('.typeahead_office_name_bn').typeahead(null, {
            name: 'datypeahead_office_name_bn',
            displayKey: 'value',
            source: OfficeDataAdapter.ttAdapter(),
            hint: (Metronic.isRTL() ? false : true),
            templates: {
                suggestion: Handlebars.compile([
                    '<div class="media">',
                    '<div class="media-body">',
                    '<h4 class="media-heading">{{value}}</h4>',
                    '</div>',
                    '</div>',
                ].join(''))
            }
        }
    )
    .on('typeahead:opened', onOpened)
    .on('typeahead:selected', onAutocompleted)
    .on('typeahead:autocompleted', onSelected);

    function onOpened($e) {
        // console.log('opened');
    }

    function onAutocompleted($e, datum) {
        //DakOffice.setAutoDataIntoHiddenFields(datum, $e.currentTarget.id);
        $("#"+$e.currentTarget.id).val('');
        toastr.success('এই অফিস ইতোমধ্যে যোগ করা হয়েছে।');
    }

    function onSelected($e, datum) {
        // console.log('selected');
    }

    $("#office-name-bng").blur(function () {
        var q = getBaseUrl()+'officeManagement/autocompleteOfficeName?';
        q += '&office_origin_id='+encodeURIComponent($('input[name=office_origin_id]').val());
        var geo_division_id = $("#geo-division-id").val();
        if (geo_division_id > 0) {
            q += '&geo_division_id='+encodeURIComponent(geo_division_id);
        }
        var geo_district_id = $("#geo-district-id").val();
        if (geo_district_id > 0) {
            q += '&geo_district_id='+encodeURIComponent(geo_district_id);
        }
        var geo_upazila_id = $("#geo-upazila-id").val();
        if (geo_upazila_id > 0) {
            q += '&geo_upazila_id='+encodeURIComponent(geo_upazila_id);
        }
        var geo_union_id = $("#geo-union-id").val();
        if (geo_union_id > 0) {
            q += '&geo_union_id='+encodeURIComponent(geo_union_id);
        }
        q += '&search_key='+encodeURIComponent($('.typeahead_office_name_bn.tt-input').val());

        $.ajax({
            url: q
        }).success(function (response) {
            var result = $.parseJSON(response);
            if (result[0].id != 0) {
                $("#office-name-bng").val('');
                $("#office-name-bng").focus();
                toastr.success('এই অফিস ইতোমধ্যে যোগ করা হয়েছে।');
            }
        });
    });
</script>

<!-- End : Office Setup JS -->