<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-check-square-o"></i><?php echo __('Office') ?> <?php echo __('Ogranogram') ?> <?php echo __('with Employees') ?>
        </div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a> <a href="#portlet-config"
                                                           data-toggle="modal" class="config"></a> <a href="javascript:"
                                                                                                      class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-window-body">
        <?= $cell = $this->cell('OfficeSelection', ['entity' => '']) ?>
        <div class="row">
            <div class="form-group">
                <label class="col-sm-2 control-label">অফিস</label>

                <div class="col-sm-6">
                    <?php
                    echo $this->Form->input('office_id', array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => '----'
                    ));
                    ?>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row" id="unit_tree_div">
            <div class="col-md-6">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=""></i>Unit Tree
                        </div>
                    </div>
                    <div class="portlet-window-body">
                        <div id="unit_tree"></div>
                    </div>
                </div>
            </div>
            <div id="unit_tree_expand_view" class="col-md-6">
                <a data-toggle="modal" class="btn blue" id="view_tree" role="button" href="#modal_organogram_tree"
                   style="display: block;">View Tree</a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div id="modal_organogram_tree" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog modal-full">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title"><b>Organogram Tree</b></h4>
                    </div>
                    <div class="modal-body" data-spy="scroll">
                        <div id="office_organogram_tree">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">বন্ধ করুন</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        /* var jsonObjs = "";
         $("#office-origin-id").bind('change', function ()
         {


         }); */

        //$("#unit_tree_div").hide();
    });
</script>

<script type="text/javascript">
    var OfficeUnitManagement = {
        checked_node_ids: [],
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {
                //
            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {
                //
            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

            });
        },

        getUnitOrganogramByOfficeId: function (office_id) {
            $("#office_organogram_tree").html("");
            var office_id = $("#office-id").val();
            var url = "<?php echo $this->request->webroot; ?>officeManagement/showOfficeOrganogramTree?office_id=" + office_id;
            PROJAPOTI.ajaxSubmitDataCallback(url, {'office_id': office_id}, 'json',
                function (response1) {
                    
                    $("#office_organogram_tree").nadyTree({
                        callType: 'json',
                        url: 'myJsonFile.json',
                        structureObj: response1
                    });
                    
                });
            $("#view_tree").css("display", "block");
        },

        selected_organogram: [],

        selectOrganogram: function (input) {
            var organogram_id = $(input).attr('id');
            var unit_id = $(input).data('unit-id');
            var org_unit = "unit_" + unit_id + ":" + "org_" + organogram_id;

            var index = OfficeUnitManagement.selected_organogram.indexOf(org_unit);
            if (index < 0) {
                OfficeUnitManagement.selected_organogram.push(org_unit);
            }
            else {
                OfficeUnitManagement.selected_organogram.splice(index, 1);
            }
        },

        transferOrganogram: function () {
            if (OfficeUnitManagement.selected_organogram.length > 0) {
                $("#office_unit_tree_panel").html("");
                PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/transferOrganogram',
                    {
                        'origin_unit_org_ids': OfficeUnitManagement.selected_organogram,
                        'office_id': $("#office-id").val()
                    }, 'json',
                    function (response) {
                        //
                        OfficeUnitManagement.reloadOfficeUnitOrg($("#office-id").val());
                    });
            }
        },

    };

    $(function () {
        $("#office-ministry-id").bind('change', function () {
            OfficeUnitManagement.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            OfficeUnitManagement.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            OfficeUnitManagement.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            OfficeUnitManagement.getUnitOrganogramByOfficeId($(this).val());
        });
        $("#officeUnitTransfer").bind('click', function () {
            OfficeUnitManagement.transferOrganogram();
        });

    });
</script>