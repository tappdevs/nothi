<?php
$session = $this->request->session();
$logged_user = $session->read('logged_employee_record');
$office_records = $logged_user['office_records'];
$office_unit_array = [];
$office_unit_options = array();
if (!empty($office_records)) {
    foreach ($office_records as $k => $record) {
        if (!in_array($record['office_unit_id'], $office_unit_array)) {
            $office_unit_array[] = $record['office_unit_id'];
            $office_unit_options[] = [
                'text' => $record['unit_name'],
                'value' => $record['office_unit_id'],
                'office-id' => $record['office_id'],
            ];
        }
    }
}

?>

<div class="portlet light">
    <!--<div class="portlet-title">
        <div class="caption">অফিস সিল তৈরি</div>
    </div>-->
    <div class="portlet-body form">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET -->
                <div class="">
                    <div class="portlet-title">
                        <div class="row">
                            <div class="col-md-5 form-group form-horizontal">
                                <lebel>
                                    পদের নাম: <b><?= $currentOffice['designation'].', '.$currentOffice['office_unit_name'] ?></b>
                                </lebel>
                                <input type="hidden" id="office-unit-id" value="<?= $currentOffice['office_unit_id']?>" office-id="<?= $currentOffice['office_id']?>">
                                <input type="hidden" id="prev_value">
                            </div>

                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6 office-seal form-group form-horizontal">
                                <h4>অফিস শাখা</h4>
                                <div id="office_unit_tree_panel">
                                    <img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>
                                </div>


                            </div>
                            <div class="col-md-6 office-seal form-group form-horizontal">
                                <h4>প্রাপকের তালিকা</h4>
                                <div id="office_unit_seal_panel"><img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span></div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">

                </div>
            </div>
            <!-- END PORTLET -->
        </div>
    </div>
</div>
</div>