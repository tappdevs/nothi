<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
            <?php echo __("Office"); ?>
            <?php echo __("Unit"); ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6 col-lg-6 " id="office_unit_tree_panel">
                
            </div>
            <div class="col-md-6 col-lg-6 " id="office_unit_edit_panel">
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var OfficeManagement = {
        enableEditMode: function(attributes){
            
            var unit_id_text = $(attributes).data('id');
            var unit_id_split = unit_id_text.split('_');
            var unit_id =  unit_id_split[unit_id_split.length - 1];
            
            $.ajax({
                url: '<?php echo $this->Url->build(['controller'=>'officeManagement','action'=>'showOfficeUnit']) ?>' ,
                cache: false,
                type: 'post',
                data: {unit_id:unit_id},
                success: function(html){
                    $('#office_unit_edit_panel').html(html);
                }
                
            })
        }
    }
    var OfficeUnitManagement = {
        reloadOfficeUnitTree: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeOriginTree/populateFullOfficeUnitTree',
                    {'office_id': <?php echo $office_id; ?>}, 'json',
                    function (response) {
                        if (response.length > 0) {
                            var data_str = "";
                            $.each(response[0], function (i) {
                                var node = response[0][i];
                                data_str += '{ "id" : "' + node.id + '", "parent" : "' + node.parent + '", "text" : "' + node.text + '", "icon" : "' + node.icon + '" },';
                            });
                            data_str= data_str.replace(/\\n/g, "\\n")
                            .replace(/\\'/g, "\\'")
                            .replace(/\\"/g, '\\"')
                            .replace(/\\&/g, "\\&")
                            .replace(/\\r/g, "\\r")
                            .replace(/\\t/g, "\\t")
                            .replace(/\\b/g, "\\b")
                            .replace(/\\f/g, "\\f");
             // remove non-printable and other non-valid JSON chars
                        data_str = data_str.replace(/[\u0000-\u0019]+/g,"");
                            var edited_data_str = "[" + data_str.replace(/^,|,$/g, '') + "]";

                            $('#office_unit_tree_panel').jstree(true).settings.core.data = JSON.parse(edited_data_str);
                            $('#office_unit_tree_panel').jstree(true).refresh();
                            $('#office_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                                $('#office_unit_tree_panel').jstree("open_all");
                            });

                        } else {
                            $('#office_unit_tree_panel').jstree(true).settings.core.data = '';
                            $('#office_unit_tree_panel').jstree(true).refresh();
                            $('#office_unit_tree_panel').bind("refresh.jstree", function (event, data) {
                                $('#office_unit_tree_panel').jstree("open_all");
                            });
                        }

                    });
        },
        loadOfficeUnitTree: function () {

            $('#office_unit_tree_panel').jstree(
                    {
                        'core': {
                            "themes": {
                                "variant": "large",
                                "multiple": false
                            },
                            'data': ''
                        },
                        
                    }
            ).bind("loaded.jstree", function (event, data) {
                $(this).jstree("open_all");
            });
        }
    };
    
    
    
    
    $(function () {
        OfficeUnitManagement.loadOfficeUnitTree();
        OfficeUnitManagement.reloadOfficeUnitTree();
    });
</script>