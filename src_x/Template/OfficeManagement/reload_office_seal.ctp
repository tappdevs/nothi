<?php


if ($sealOf == 0) {
    echo $this->cell('DakSender', ['params' => $selected_office_section]);
} else {
    if ($sealType == 1) {
        echo $this->cell('DakRecipient', ['params' => $selected_office_section, "type" => 'multiple']);
    } else {
        echo $this->cell('DakRecipient', ['params' => $selected_office_section]);
    }
}

?>