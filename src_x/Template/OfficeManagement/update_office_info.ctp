<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
            <?php echo __("Office"); ?>
            <?php echo __("Information"); ?>
        </div>
    </div>
    <div class="portlet-body">
          <?= $this->Cell('OfficeSelection'); ?>
        <div class="row">
                    <div class="col-md-4 form-group form-horizontal">
                        <label class="control-label"> <?php echo __("Office") ?> </label>
                        <?php
                        echo $this->Form->input('office_id',
                            array(
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => '----',
                        ));
                        ?>
                    </div>
                </div>
    </div>
</div>
<script>
        var EmployeeAssignment = {
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

            });
        },
    };
    $("#office-ministry-id").bind('change', function () {
        EmployeeAssignment.loadMinistryWiseLayers();
    });
    $("#office-layer-id").bind('change', function () {
        EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
    });
    $("#office-origin-id").bind('change', function () {
        EmployeeAssignment.loadOriginOffices();
    });
    $("#office-id").bind('change', function () {
       window.location.href = js_wb_root+  '<?= isset($redirect_url)?$redirect_url:'officeManagement/updateOwnOffice/' ?>'+$("#office-id").val();
    });
</script>