<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
            <?php echo __("Office"); ?>
            <?php echo __("Information"); ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="form">
            <?php
           
            echo $this->Form->create($entity, ['method' => 'post', 'id' => 'OfficeRecordsForm']);

            echo $this->Form->hidden('id');
            echo $this->Form->hidden('office_ministry_id');
            echo $this->Form->hidden('office_layer_id');
            echo $this->Form->hidden('office_origin_id');
            ?>
            <div class="form-body">
                <?php
                $geo_field_values = array();
                echo $geo_panel = $this->cell('GeoSelection', ['sorted_field_values' => $geo_field_values, $entity->geo_division_id, $entity->geo_district_id, $entity->geo_upazila_id, $entity->geo_union_id]);
                ?>

                <div class="row">
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">নাম <span class="text-danger">*</span></label>
                        <?php if($auth_user['user_role_id'] <= 2){ ?>
                        <?= $this->Form->input('office_name_bng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(বাংলায়)','required'=>'required']); ?>
                        <?php }else{ ?>
                        <?= $this->Form->input('office_name_bng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(বাংলায়)','required'=>'required','readonly' =>true]); ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">&nbsp;</label>
                        <?php if($auth_user['user_role_id'] <= 2){ ?>
                       <?= $this->Form->input('office_name_eng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(ইংরেজি)','required'=>'required']); ?>
                        <?php }else{ ?>
                        <?= $this->Form->input('office_name_eng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(ইংরেজি)','required'=>'required','readonly' =>true]); ?>
                        <?php } ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 form-group form-horizontal">
                        <label class="control-label">ঠিকানা</label>
<?php echo $this->Form->input('office_address', array('type' => 'textarea', 'label' => false, 'class' => 'form-control', 'placeholder' => 'ঠিকানা')); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">ফোন</label>
                        <?php echo $this->Form->input('office_phone', array('label' => false, 'class' => 'form-control', 'placeholder' => 'ফোন')); ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">মোবাইল</label>
                        <?php echo $this->Form->input('office_mobile', array('label' => false, 'class' => 'form-control', 'placeholder' => 'মোবাইল')); ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">ফ্যাক্স</label>
                        <?php echo $this->Form->input('office_fax', array('label' => false, 'class' => 'form-control', 'placeholder' => 'ফ্যাক্স')); ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">ই-মেইল</label>
<?php echo $this->Form->input('office_email', array('label' => false, 'class' => 'form-control', 'placeholder' => 'ই-মেইল')); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">অফিস ওয়েবসাইট</label>
<?php echo $this->Form->input('office_web', array('label' => false, 'class' => 'form-control', 'placeholder' => 'অফিস ওয়েবসাইট')); ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal ">
                        <label class="control-label">ডিজিটাল নথি কোড (প্রথম ৮টি)  <span class="text-danger">*</span></label>
                        <?php echo $this->Form->input('digital_nothi_code', array('label' => false, 'class' => 'form-control', 'placeholder' => '**.**.****')); ?>
                    </div>
                </div>
                <script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jQuery-Mask/jquery.mask.min.js"></script>
                <script>
                        $('#digital-nothi-code').mask('AA.AA.AAAA'
                            ,{
                                translation:{
                                    A: {pattern: /[\u09E6-\u09EF-0-9]/},
                                },
                                placeholder: "**.**.****"
                            });
                </script>

                <div class="row ">
                    <!--
                    <div class="col-md-6 form-group form-horizontal">
                        <label class="control-label">অফিস কোড</label>
<?php // echo $this->Form->input('office_code', array('label' => false, 'class' => 'form-control', 'placeholder' => 'অফিস কোড')); ?>
                    </div>
                    <div class="col-md-6 form-group form-horizontal hidden">
                        <label class="control-label"> রেফারেন্স কোড</label>
                        <?php // echo $this->Form->input('ref_code', array('label' => false, 'class' => 'form-control', 'placeholder' => 'রেফারেন্স কোড')); ?>
                    </div>
                    
                </div>-->
                </div>    
                <!--<hr/>-->
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-offset-5 col-md-7">
<?= $this->Form->button(__('Submit'), ['type' => 'submit', 'class' => 'btn btn-primary']) ?>

                        </div>
                    </div>
                </div>
            

<?php echo $this->Form->end() ?>
        </div>
    </div>
</div>
</div>
