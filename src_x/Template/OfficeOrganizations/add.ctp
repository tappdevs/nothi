<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>Add New Office Organization</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create("", array('id' => 'org_form')); ?>
        <?php echo $this->element('OfficeOrganizations/add', array('office_hierarchy_list' => $office_hierarchy_list)); ?>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-4 col-md-9">
                    <button type="button" class="btn   blue" onclick="OfficeOrg.save()">Submit</button>
                    <button type="reset" class="btn   default">Reset</button>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<script type="text/javascript">
    var OfficeOrg = {
        save: function () {
            var id = "";
            var selected_office_text = $("#office-hierarchy").val();
            <?php
                foreach($office_hierarchy_list as $key=>$ohl){
            ?>
            if (selected_office_text == '<?= $ohl;?>') {
                id = '<?= $key?>';
            }
            <?php
             }
             ?>
            $("#office-hierarchy-id").val(id);
            $("#org_form").submit();
        }
    }
</script>

