<div class="portlet box green">
    <div class="portlet-body form">
        <?php echo $this->Form->create("", ['class' => 'form-horizontal']); ?>
        <?= $this->cell('OfficeHierarchyTemplates'); ?>
        <div id="office_organization_div">
            <!-- // -->
        </div>
        <div id="office_mapping_div">
            <div class="row">
                <div class="col-md-6"><h3>Tree View</h3></div>
                <div class="col-md-6"><h3>Flat View</h3></div>
            </div>

            <div class="row">
                <div class="col-md-6" id=orgngrm_tree></div>
                <div class="col-md-6" id="flat_div"></div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row text-center">
                <button type="button" onclick="OfficeEmployeeMapping.save()" class="btn   blue ajax_submit">
                    Save
                </button>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<script type="text/javascript">
    var OfficeOrganization = {
        loadOfficeOrganizations: function () {
            $("#office_organization_div").hide('slow');
            $("#office_mapping_div").hide('slow');

            OfficeOrganization.getSelectedOfficeHiararchyId();

            var office_hierarchy_id = $("#office-hierarchy-id").val();
            var office_hierarchy_name = $("#office-hierarchy").val();

            PROJAPOTI.ajaxLoadCallback('<?= $this->request->webroot ?>officeEmployeeMappings/loadOffices?ofc_hie_id=' + office_hierarchy_id + '&&ofc_hie_name=' + office_hierarchy_name + '&&parent_script_variable=OfficeOrganization', function (html_response) {
                $("#office_organization_div").show('slow');
                $("#office_organization_div").html(html_response);
            });
        },
        getSelectedOfficeHiararchyId: function () {
            var id = "";
            var selected_office_text = $("#office-hierarchy").val();
            <?php
                foreach($office_hierarchy_list as $key=>$ohl){
            ?>
            if (selected_office_text == '<?= $ohl;?>') {
                id = '<?= $key?>';
            }
            <?php
             }
             ?>
            $("#office-hierarchy-id").val(id);
        },
        getSelectedOrganizationId: function (offce_org_list) {
            var id = "";
            var selected_office_text = $("#office-organization").val();
            //var office_list = $.parseJSON(offce_org_list);
            var office_list = offce_org_list;
            $.each(office_list, function (key, value) {
                if (value == selected_office_text) {
                    id = key;

                }
            });
            $("#office-organization-id").val(id);
        },
        loadOfficeOrganograms: function (offce_org_list) {
            $("#office_mapping_div").hide('slow');

            OfficeOrganization.getSelectedOrganizationId(offce_org_list);

            var office_org_id = $("#office-organization-id").val();
            var office_org_name = $("#office-organization").val();

            PROJAPOTI.ajaxLoadCallback('<?= $this->request->webroot ?>officeEmployeeMappings/loadOfficeOrganogram?office_org_id=' + office_org_id + '&&office_org_name=' + office_org_name, function (html_response) {
                $("#office_mapping_div").show('slow');
                $("#tree_div").html(html_response);
                OfficeOrganization.loadTree();
                PROJAPOTI.ajaxLoadCallback('<?= $this->request->webroot ?>officeEmployeeMappings/loadOfficeEmployees', function (html_response) {
                    $("#flat_div").html(html_response);
                });

            });
        },
        loadTree: function () {
            $('#orgngrm_tree').jstree({
                "core": {
                    "themes": {
                        "variant": "large",
                        "theme": "classic",
                        "dots": true,
                    },
                    'data': {
                        'url': function (node) {
                            return node.id === '#' ?
                                '<?php echo $this->request->webroot?>officeOrganograms/loadOfficeOrganogramData' : '<?php echo $this->request->webroot?>officeOrganograms/loadOfficeOrganogramData';
                        },
                        'data': function (node) {
                            return {
                                'type': 'checkbox',
                                'id': node.id,
                                'office_organization_id': $("#office-organization-id").val()
                            };
                        }
                    }
                }
            });
        }
    };
    $(function () {
        $(".office_hierarchy_btn").bind('click', function () {
            OfficeOrganization.loadOfficeOrganizations();
        });
        $("#office_mapping_div").hide('slow');
    })
</script>