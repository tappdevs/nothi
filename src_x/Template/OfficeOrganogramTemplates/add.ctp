<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>Create New Office Organogram Structure</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body form"><br><br>
        <?php echo $this->Form->create("", array('id' => 'office_organogram_form')); ?>
        <div class="form-horizontal">

            <div class="form-group">
                <label class="col-sm-4 control-label">Des Division</label>

                <div class="col-sm-7">
                    <?php echo $this->Form->input('des_div', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
                </div>
            </div>
            <?php echo $this->element('OfficeOrganogram/add', array('parent' => $parent, 'office_hierarchy_id' => $office_hierarchy_id)); ?>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-4 col-md-9">
                        <button type="button" onclick="OfficeOrganogram.save()" class="btn   blue ajax_submit">
                            Submit
                        </button>
                        <button type="reset" class="btn   default">Reset</button>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<script type="text/javascript">
    var OfficeOrganogram = {
        save: function () {
            TreeView.saveNode($("#office_organogram_form").serialize());
        }
    }

</script>

