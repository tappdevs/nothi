<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>শাখার নতুন মৌলিক সাংগঠনিক কাঠামো</div>
        <div class="tools">
            <!--<a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>-->
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body form">
		<?php if(Live == 0): ?>
        <?php
        echo $this->Form->create($entity, array('id' => 'OriginUnitOrgForm'));
        echo $this->Form->hidden('parent', array('value' => $parent));
        echo $this->Form->hidden('office_origin_unit_id', array('value' => $parent));
        echo $this->Form->hidden('status', array('value' => 1)); // 1:active, 2:inactive
        ?>

        <?php
        echo $this->element('/OfficeOriginUnitOrganograms/org_form', ['office_origin_id' => $office_origin_id]);
        ?>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-4 col-md-9">
                    <button type="button" onclick="OfficeUnitOrgView.saveNode()"
                            class="btn   blue ajax_submit round-corner-5">Submit
                    </button>
                    <button type="reset" class="btn   default round-corner-5">Reset</button>
                </div>
            </div>
        </div>
        <?php
        echo $this->Form->end();
        ?>
	    <?php endif; ?>
    </div>
</div>
