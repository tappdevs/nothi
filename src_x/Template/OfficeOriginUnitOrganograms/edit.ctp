<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class=""></i><?= __("Organogram"). ' ' . __("Edit") ?></div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">
		<?php if(Live == 0): ?>
        <?php
        echo $this->Form->create($entity, array('id' => 'OriginUnitOrgForm'));
        echo $this->Form->hidden('parent', array('value' => $entity->office_origin_unit_id,'class'=>'form-control input-sm'));
        echo $this->Form->hidden('office_origin_unit_id', array('value' => $entity->office_origin_unit_id,'class'=>'form-control input-sm'));
        echo $this->Form->hidden('status', array('value' => $entity->status,'class'=>'form-control input-sm')); // 1:active, 2:inactive
        ?>
        <?php
        echo $this->element('/OfficeOriginUnitOrganograms/org_form', ['office_origin_id' => $office_origin_id, 'entity' => $entity]);
        ?>
        <div class="form-actions text-center">
            <button type="button" onclick="OfficeUnitOrgView.editNode('<?php echo $entity->id; ?>')" class="btn btn-sm blue ajax_submit round-corner-5">Update
            </button>
            <button type="reset" class="btn  btn-sm default round-corner-5">Reset</button>
            <button type="button" class="btn btn-sm  btn-danger round-corner-5" onclick="OfficeUnitOrgView.deleteNode('<?php echo $entity->id; ?>')">Delete
            </button>
        </div>
        <?php
        echo $this->Form->end();
        ?>
	    <?php endif; ?>
    </div>
</div>
