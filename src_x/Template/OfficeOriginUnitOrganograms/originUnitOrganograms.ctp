<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class=""></i><?php echo __('Office') ?> <?php echo __('Origin') ?> <?php echo __('Unitr') ?> <?php echo __('Organogram') ?>
        </div>
        <div class="tools">

        </div>
    </div>
    <div class="portlet-window-body">
        <?= $cell = $this->cell('OfficeSelection'); ?>
        <div class="row" id="tree_view_div">
            <div class="col-md-6">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=""></i>অফিস মৌলিক শাখার সাংগঠনিক কাঠামো
                        </div>
                    </div>
                    <div class="portlet-window-body">
                        <div id="content_tree"></div>
                    </div>
                </div>
            </div>
            <div id="content_tree_expand_view" class="col-md-6"></div>
        </div>
    </div>
</div>
<style>
    #content_tree_expand_view{
        position: fixed;
        right:5px;
	    bottom:5px;
	    border:solid 1px silver;
	    background-color:white;
	    overflow:auto;
    }
</style>
<!-- Start : Office Setup JS -->

<script type="text/javascript">
    $(function () {
        OfficeSetup.init();
        OfficeUnitOrgView.init();
        $("#tree_view_div").hide();
    });
</script>
<script type="text/javascript">
    var OfficeUnitOrgView = {
        selected_node: "",
        parent_id: 0,
        parent_node: "",
        selected_ministry_id: 0,
        selected_layer_id: 0,
        selected_office_id: 0,
        newNode: function (input) {
            $("#content_tree_expand_view").hide('slow');
            var dataid = $(input).attr('data-parent-node');
            var parent_id = dataid.split(":");
            parent_id = parent_id[0] + "_designation";

            OfficeUnitOrgView.parent_id = parent_id;
            OfficeUnitOrgView.selected_node = dataid;
            OfficeUnitOrgView.parent_node = $(input).data('parent-node');
            var url = "<?php echo $this->request->webroot;?>officeOrigin/addOriginUnitOrganogram?parent_id=" + parent_id + "&&office_origin_id=" + OfficeUnitOrgView.selected_office_id;
            PROJAPOTI.ajaxLoad(url, "#content_tree_expand_view");
            $("#content_tree_expand_view").show('slow');
        },

        saveNode: function (formdata) {
            $("#OriginUnitOrgForm #office_ministry_id").val(OfficeUnitOrgView.selected_ministry_id);
            $("#OriginUnitOrgForm #office_layer_id").val(OfficeUnitOrgView.selected_layer_id);
            $("#OriginUnitOrgForm #office_origin_id").val(OfficeUnitOrgView.selected_office_id);

            var url = "<?php echo $this->request->webroot;?>officeOrigin/addOriginUnitOrganogram";

            PROJAPOTI.ajaxSubmitDataCallback(url, {'formdata': $("#OriginUnitOrgForm").serialize()}, "json", function (response) {
                if (response == 1) {
                    $("#content_tree_expand_view").hide('slow');
                    OfficeUnitOrgView.unitTreeView();
                }
                else {
                    alert("Failed to add Node.");
                }
            });
        },

        editNode: function (id) {
            var url = "<?php echo $this->request->webroot;?>officeOrigin/editOriginUnitOrganogram/" + id;
            PROJAPOTI.ajaxSubmitDataCallback(url, {'formdata': $("#OriginUnitOrgForm").serialize()}, "json", function (response) {
                if (response == 1) {
                    $("#content_tree_expand_view").hide('slow');
                    if (OfficeUnitOrgView.parent_id == 0) {
                        OfficeUnitOrgView.unitTreeView();
                    }
                    else {
                        $("#content_tree").jstree("load_node", $('#' + OfficeUnitOrgView.parent_node));
                    }
                }
                else {
                    alert("Failed to update Node.");
                }
            });
        },
        deleteNode: function (id) {
            if (confirm("Are you sure want to delete?")) {
            } else {
                return false;
            }
            var url = "<?php echo $this->request->webroot;?>officeOrigin/deleteOriginUnitOrganogram/" + id;
            PROJAPOTI.ajaxSubmitDataCallback(url, {}, "json", function (response) {

                if (response == 1) {
                    $('#OriginUnitOrgForm').find('button[type=reset]').click();
                    $("#content_tree_expand_view").hide('slow');
                    if (OfficeUnitOrgView.parent_id == 0) {
                        OfficeUnitOrgView.unitTreeView();
                    }
                    else {
                        $("#content_tree").jstree("load_node", $('#' + OfficeUnitOrgView.parent_node));
                    }
                }
                else {
                    toastr.error(response);
                }
            });
        },

        gotoEdit: function (input) {
            $("#content_tree_expand_view").hide('slow');
            var id = $(input).attr('data-id');
            var node_id = $(input).attr('data-node-id');
            var url = "<?php echo $this->request->webroot;?>officeOrigin/editOriginUnitOrganogram/" + id + "?office_origin_id=" + OfficeUnitOrgView.selected_office_id;
            var node = 'designation';
            if (node_id.indexOf(node) != -1 && $.isNumeric(id)) {
                PROJAPOTI.ajaxLoad(url, "#content_tree_expand_view");
            } else {
                $(input).parent().parent().find('.jstree-ocl').trigger('click');
	        }
            $("#content_tree_expand_view").show('slow');
        },

        unitTreeView: function () {
            OfficeUnitOrgView.selected_ministry_id = $("#office-ministry-id").val();
            OfficeUnitOrgView.selected_layer_id = $("#office-layer-id").val();
            OfficeUnitOrgView.selected_office_id = $("#office-origin-id").val();

            $('#content_tree').jstree("refresh");
            $('#content_tree').jstree({
                "core": {
                    "themes": {
                        "variant": "large",
                    },
                    'data': {
                        'url': function (node) {
                            return node.id === '#' ?
                                'loadOriginUnitOrganograms' : 'loadOriginUnitOrganograms';
                        },
                        'data': function (node) {
                            return {'id': node.id, 'type': "123", 'office_id': $("#office-origin-id").val()};
                        }
                    }
                }
            });
        },

        init: function () {
            $("#office-origin-id").bind('change', function () {
                $("#tree_view_div").hide('slow');
                OfficeUnitOrgView.unitTreeView();
                $("#tree_view_div").show('slow');
            });
        }
    };

</script>

<!-- End : Office Setup JS -->