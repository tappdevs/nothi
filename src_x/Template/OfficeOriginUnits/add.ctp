<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class=""></i> <?php echo  __("New"). ' ' . __("Unit") . ' ' . __("Create")  ?></div>
        
    </div>
    <div class="portlet-body form"><br><br>
		<?php if(Live == 0): ?>
        <?php
        echo $this->Form->create('', array('id' => 'OfficeOriginUnitForm'));
        echo $this->Form->hidden('parent', array('value' => $parent));
        echo $this->Form->hidden('parent_unit_id', array('value' => $parent));
        echo $this->Form->hidden('office_ministry_id', array('id' => 'office_ministry_id'));
        echo $this->Form->hidden('office_layer_id', array('id' => 'office_layer_id'));
        echo $this->Form->hidden('office_origin_id', array('id' => 'office_origin_id'));
        ?>
        <div class="form-horizontal">

            <!-- Start : Import Office Unit Category View From Cell -->
            <?= $this->cell('OfficeUnitCategory', ['value' => ""]); ?>
            <!-- End : Import Office Unit Category View From Cell -->

            <div class="form-group">
                <label class="col-sm-5 control-label">Name (Bangla)</label>

                <div class="col-sm-7">
                    <?php echo $this->Form->input('unit_name_bng', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-5 control-label">Name (English)</label>

                <div class="col-sm-7">
                    <?php echo $this->Form->input('unit_name_eng', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-5 control-label">Level</label>

                <div class="col-sm-7">
                    <?php echo $this->Form->input('unit_level', array('label' => false, 'type' => 'text', 'class' => 'form-control','default'=>0)); ?>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-4 col-md-9">
                    <button type="button" onclick="OfficeUnitView.saveNode()" class="btn   blue ajax_submit">
                        Submit
                    </button>
                    <button type="reset" class="btn   default">Reset</button>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
	    <?php endif; ?>
    </div>
</div>
