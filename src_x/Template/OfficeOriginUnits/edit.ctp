<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class=""></i> <?php echo __("Unit") . ' ' . __("Edit") ?></div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">
		<?php if(Live == 0): ?>
        <?php
        echo $this->Form->create($entity, array('id' => 'OfficeOriginUnitForm'));
        echo $this->Form->hidden('parent', array('value' => $entity->parent_unit_id));
        echo $this->Form->hidden('id');
        echo $this->Form->hidden('office_ministry_id');
        echo $this->Form->hidden('office_layer_id');
        echo $this->Form->hidden('office_origin_id');
        ?>
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-5 control-label">Parent</label>

                <div class="col-sm-7">
                    <?php echo $this->Form->input('parent_unit_id', array('type' => 'select', 'class' => 'form-control input-sm', 'empty' => '--', 'options' => $parent_id, 'label' => false)); ?>
                </div>
            </div>

            <!-- Start : Import Office Unit Category View From Cell -->
            <?= $this->cell('OfficeUnitCategory', ['value' => $entity->office_unit_category]); ?>
            <!-- End : Import Office Unit Category View From Cell -->
            <div class="form-group">
                <label class="col-sm-5 control-label">Name (Bangla)</label>

                <div class="col-sm-7">
                    <?php echo $this->Form->input('unit_name_bng', array('label' => false, 'type' => 'text', 'class' => 'form-control input-sm')); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-5 control-label">Name (English)</label>

                <div class="col-sm-7">
                    <?php echo $this->Form->input('unit_name_eng', array('label' => false, 'type' => 'text', 'class' => 'form-control input-sm')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Level</label>

                <div class="col-sm-7">
                    <?php echo $this->Form->input('unit_level', array('label' => false, 'type' => 'text', 'class' => 'form-control input-sm')); ?>
                </div>
            </div>
        </div>
        <div class="form-actions text-center">
            <button type="button" onclick="OfficeUnitView.saveNode()" class="btn btn-sm  blue ajax_submit">
                Update
            </button>
            <button type="reset" class="btn btn-sm  default">Reset</button>
            <button type="button" class="btn btn-sm   btn-danger"
                    onclick="OfficeUnitView.deleteNode('<?php echo $entity->id ?>')">Delete
            </button>
        </div>
        <?php echo $this->Form->end(); ?>
	    <?php endif; ?>
    </div>
</div>
