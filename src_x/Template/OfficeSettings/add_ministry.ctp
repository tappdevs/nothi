<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fs1 a2i_gn_edit2"></i><?php echo __('Ministry') ?><?php echo('/') ?><?php echo __('New Division') ?>
        </div>

    </div>
    <div class="portlet-body form">
        <?php echo $this->Form->create($entity, array('id' => 'OfficeMinistryForm')); ?>
        <div class="form-body">
            <div class="row">
                <div class="col-md-6 form-group form-horizontal">
                    <label class="control-label"><?php echo __('Type') ?></label>
                    <?= $this->Form->input('office_type', ['options' => json_decode(TOP_OFFICE_TYPE, true), 'empty' => '--বাছাই করুন--', 'label' => false, 'class' => 'form-control', 'type' => 'select']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group form-horizontal">
                    <label class="control-label">নাম</label>
                    <?= $this->Form->input('name_bng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(বাংলা)']); ?>
                </div>
                <div class="col-md-6 form-group form-horizontal">
                    <label class="control-label">&nbsp;</label>
                    <?= $this->Form->input('name_eng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(ইংরেজি)']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group form-horizontal">
                    <label class="control-label">সংক্ষিপ্ত নাম</label>
                    <?= $this->Form->input('name_eng_short', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'সংক্ষিপ্ত নাম']); ?>
                </div>
                <div class="col-md-6 form-group form-horizontal">
                    <label class="control-label">মন্ত্রণালয়/বিভাগ কোড</label>
                    <?= $this->Form->input('reference_code', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'মন্ত্রণালয়/বিভাগ কোড']); ?>
                </div>
            </div>
        </div>

        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-4 col-md-9">
                    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
                    <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-danger']) ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
