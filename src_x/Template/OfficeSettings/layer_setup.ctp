<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fs1 a2i_gn_details1"></i> <?php echo __('Add New') ?> <?php echo __('Layer') ?>
        </div>
        <!-- <div class="tools">
             <a href="javascript:" class="collapse"></a> <a href="#portlet-config"
                                                            data-toggle="modal" class="config"></a> <a href="javascript:"
                                                                                                       class="reload"></a>
             <a href="javascript:" class="remove"></a>
         </div>-->
    </div>
    <div class="portlet-body">
        <div class="row">
            <div id="data_panel" class="col-md-6">
				<?php if(Live == 0): ?>
                <?php echo $this->Form->create($entity, array('id' => 'OfficeLayerForm'));
                echo $this->Form->hidden('id', array('id' => 'layer-id')); ?>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 form-group form-horizontal">
                            <label class="control-label"><?php echo __('মন্ত্রণালয়/বিভাগ') ?></label>
                            <?= $this->Form->input('office_ministry_id', ['id' => 'layer-form-ministry-id', 'empty' => '--বাছাই করুন--', 'label' => false, 'class' => 'form-control', 'type' => 'select']); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group form-horizontal">
                            <label class="control-label">নাম</label>
                            <?= $this->Form->input('layer_name_bng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম (বাংলা)']); ?>
                        </div>
                        <div class="col-md-6 form-group form-horizontal">
                            <label class="control-label">&nbsp;</label>
                            <?= $this->Form->input('layer_name_eng', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'নাম(ইংরেজি)']); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group form-horizontal">
                            <label class="control-label"><?php echo __('Layer') ?></label>
                            <?= $this->Form->input('layer_level', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => '']); ?>
                        </div>
                        <div class="col-md-6 form-group form-horizontal">
                            <label class="control-label"><?php echo __('Sequence') ?></label>
                            <?= $this->Form->input('layer_sequence', ['label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => '']); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group form-horizontal">
                            <label class="control-label"><?php echo __('Superior') ?> <?php echo __('Layer') ?></label>
                            <?= $this->Form->input('parent_layer_id', ['id' => 'office-layer-id', 'empty' => '--বাছাই করুন--', 'label' => false, 'class' => 'form-control', 'type' => 'select']); ?>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-4 col-md-9">
                            <button type="button" id="submitButton"
                                    class="btn   blue ajax_submit"><?php echo __('Submit') ?>
                            </button>
                            <button type="reset" class="btn   default"><?php echo __('Reset') ?></button>
                            <button type="button" id="deleteButton"
                                    class="btn   btn-danger btn-delete ajax_delete"><?php echo __('Cancel') ?>
                            </button>
                        </div>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
				<?php endif; ?>
            </div>
            <div class="col-md-6">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fs1 a2i_gn_details1"></i> <?php echo __('Office Layer') ?> <?php echo __('List') ?>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="tree_panel">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script
    src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/layer_setup.js"
    type="text/javascript"></script>

<script type="text/javascript">
    $(function () {
        LayerSetup.init();
    });
</script>
