<style>
    .inbox .inbox-nav li.active b {
        display: none !important;
    }

    .tooltip-inner {
        white-space: pre-line;
    }

    .nav-tabs > li > a, .nav-pills > li > a{
        font-size: 12px;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }

</style>
<?php
    function hide_data($string_to_encode,$first_range =1,$last_range = 1) {
    $len = strlen($string_to_encode);
    return substr($string_to_encode, 0, $first_range).str_repeat('*', $len - $last_range).substr($string_to_encode, $len - $last_range, $last_range);
}
?>
<div class="portlet light">
    <div class="portlet-body">
        <table class="table table-bordered table-advance table-stripped table-striped">
            <?php
            echo "<tr class='heading'>
                        <th>মন্ত্রণালয়</th>
                        <th>মন্ত্রণালয়/বিভাগ</th>
                        <th>দপ্তর / অধিদপ্তরের ধরন</th>
                        <th>অফিস</th>
                        <th></th>
                        <th></th>
                  </tr>";
        foreach($offices as $key=>$value){
            echo "<tr class='eachOffice' data-office={$value['office_id']}>
                        <td>".$value['ministry_name']."</td>
                        <td>".$value['layer_name']."</td>
                        <td>".$value['origin_name']."</td>
                        <td>{$value['office_name']} ({$value['office_id']}) </td>
                        <td>".hide_data($value['office_db'],0,4)."</td>
                        <td class='status'>".hide_data($value['domain_host'],2,3)."</td>
                  </tr>";
        }
        ?>
        </table>

    </div>
</div>
<?php echo $this->Html->script('assets/admin/layout4/scripts/projapoti_ajax.js?v='.js_css_version) ?>
<script>

//hitIndi(0);

function hitIndi(i){
    $('.eachOffice').eq(i).removeClass('danger');
    var office_id = $('.eachOffice').eq(i).data('office');
    if(typeof(office_id) != 'undefined') {
        $('.eachOffice').eq(i).find('td.status').text("Loading...")

        PROJAPOTI.ajaxSubmitDataCallbackError('<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getError']) ?>/' + office_id, {}, 'json', function (res) {
            if (res.status != 'success') {
                $('.eachOffice').eq(i).addClass('danger');
                $('.eachOffice').eq(i).find('td.status').text(res.count)
            }
            hitIndi(++i);
        }, function (err) {
            hitIndi(++i);
        })
    }else{
        hitIndi(++i);
    }
}
</script>