<style>
    .inbox .inbox-nav li.active b {
        display: none !important;
    }

    .tooltip-inner {
        white-space: pre-line;
    }

    .nav-tabs > li > a, .nav-pills > li > a {
        font-size: 12px;
    }

    h3 {
        margin-top: 0px;
        margin-bottom: 0px;
    }

</style>
<div class="portlet light">
    <div class="portlet-body">
        <table class="table table-bordered table-advance table-stripped table-striped">

            <tr>
                <th>অফিস নাম</th>
                <th>মোট আগত ডাক</th>
                <th>নথিভুক্ত ডাক</th>
                <th>নথিজাত ডাক</th>
                <th>নথিতে সম্পন্ন</th>
            </tr>


            <?php

            foreach ($offices as $key => $value) {
                echo "<tr class='eachOffice' data-office={$value['office_id']}><td>{$value['office_name']}</td><td></td><td></td><td></td><td></td></tr>";
            }
            ?>

        </table>

    </div>
</div>
<?php echo $this->Html->script('assets/admin/layout4/scripts/projapoti_ajax.js') ?>
<script>


    function hitSummary(i) {

        if (i >= $('.eachOffice').length)return;
        var office_id = $('.eachOffice').eq(i).data('office');
        $('.eachOffice').eq(i).addClass('text-info');

        PROJAPOTI.ajaxSubmitDataCallbackError('<?= $this->Url->build(['controller' => 'Performance', 'action' => 'testPerformanceOneMonth']) ?>/' + office_id + "/<?= $start_date ?>/<?= $end_date ?>", {}, 'json', function (res) {

            $('.eachOffice').eq(i).addClass('text-success').removeClass("text-info");
            $('.eachOffice').eq(i).find('td').eq(1).text(res.totaltodayinbox);
            $('.eachOffice').eq(i).find('td').eq(2).text(res.totaltodaynothivukto);
            $('.eachOffice').eq(i).find('td').eq(3).text(res.totaltodaynothijato);
            $('.eachOffice').eq(i).find('td').eq(4).text(res.totaltodaynisponnonote);

            hitSummary(++i);
        }, function (err) {
            hitSummary(++i);
            $('.eachOffice').eq(i).addClass('text-danger').removeClass('text-info');
            $('.eachOffice').eq(i).find('td').eq(1).text(0);
            $('.eachOffice').eq(i).find('td').eq(2).text(0);
            $('.eachOffice').eq(i).find('td').eq(3).text(0);
            $('.eachOffice').eq(i).find('td').eq(4).text(0);
        });
    }

</script>