<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>Office Unit Category</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body form"><br><br>
        <?php
        echo $this->Form->create($entity);
        echo $this->Form->hidden('id');
        ?>

        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-4 control-label">Category Name (Bangla)</label>

                <div class="col-sm-3">
                    <?php echo $this->Form->input('category_name_bng', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Category Name (English)</label>

                <div class="col-sm-3">
                    <?php echo $this->Form->input('category_name_eng', array('label' => false, 'type' => 'text', 'class' => 'form-control')); ?>
                </div>
            </div>
            <?php echo $this->element('submit'); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
