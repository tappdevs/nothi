<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i
                class=""></i><?php echo __('Office') ?> <?php echo __('Unitr') ?> <?php echo __('Category List') ?>
        </div>
        <!-- <div class="tools">
             <a href="javascript:" class="collapse"></a>
             <a href="#portlet-config" data-toggle="modal" class="config"></a>
             <a href="javascript:" class="reload"></a>
             <a href="javascript:" class="remove"></a>
         </div>-->
    </div>
    <div class="portlet-window-body">
        <?= $this->Html->link(__('Add New'), ['action' => 'addCategory'], ['class' => 'btn btn-default']) ?><br><br>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th><?php echo __('Category') ?><?php echo __('Bangla') ?></th>
                <th><?php echo __('Category') ?><?php echo __('English') ?></th>
                <th><?php echo __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach ($data_items as $item):
            ?>
            <tr>
                <td><?php echo $item['category_name_bng']; ?></td>
                <td><?php echo $item['category_name_eng']; ?></td>
                <td>
                    <?= $this->Html->link('', ['action' => 'editCategory', $item->id], ['title' => 'Edit Office Unit Category', 'class' => 'text-primary fa fa-edit']) ?>
                    <?= $this->Form->postLink(
                        '',
                        ['action' => 'deleteCategory', $item->id],
                        ['class' => 'text-danger fa fa-remove', 'title' => 'Delete Office Unit Category'],
                        ['confirm' => 'Are you sure to delete this item?'])
                    ?>
                </td>
                <?php
                endforeach;
                ?>
            </tr>
            </tbody>
        </table>
    </div>
</div>
