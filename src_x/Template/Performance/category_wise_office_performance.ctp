<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_dashboard1"></i> ক্যাটাগরি ভিত্তিক অফিস কার্যক্রম  </div>

        <div class="actions">
            <a href="<?=$this->Url->build('/report-category-list') ?>" class="btn btn-link btn-primary">
                রিপোর্ট ক্যাটাগরি
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="well text-center">
            <div style = 'font-size: 12pt!important;' class="bold"><u>মার্ক বিন্যাস</u> </div>
            <?php
            if(!empty($allreportMarking) && !empty($columns)){
                foreach($columns as $val){
                    if(empty($allreportMarking[$val])){
                        continue;
                    }
                    ?>
                    <div class="badge badge-primary" style = 'font-size: 12pt!important;margin-right: 2px;'>
                        <?= __($val)?> - <?= enTobn($allreportMarking[$val]) ?>
                    </div>
            <?php
                }
            }
            ?>
        </div>
        <hr>
        <div class="row" id="searchPanel">
            <label class="control-label col-md-2 text-right font-lg">ক্যাটাগরি</label>
            <div class="col-md-4">
                <?php
                echo $this->Form->create('', ['class' => 'searchForm'])
                ?>
                <?= $this->Form->input('category_id',
                    array('label' => false, 'type' => 'select', 'class' => 'form-control',
                          'options' =>  [0 => __("Select"), -1 => __("সকল")]  + $allCategory));
                ?>
                <?php
                echo $this->Form->hidden('start_date', ['class' => 'startdate'])
                ?>
                <?php
                echo $this->Form->hidden('end_date', ['class' => 'enddate']);
                echo $this->Form->hidden('office_ids_length', ['class' => 'office_ids_length'])
                ?>
                <?php echo $this->Form->end() ?>

            </div>
            <!--  Date Range Begain -->
            <div class="col-md-6 col-sm-12 pull-right">
                <div class="col-md-10 col-sm-10">
                    <div class="hidden-print page-toolbar pull-right portlet-title">
                        <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                             data-container="body"
                             data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                            <i class="icon-calendar"></i>&nbsp; <span
                                class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                            <i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2">
                    <button type="button" class="btn  btn-primary btn-md btn-performance-print"> <i class="fa fa-print">&nbsp;<?=__('Print')?></i> </button>
                </div>
            </div>
            <br>
            <br>
            <div class="" id="showlist">
                <img src="<?= CDN_PATH?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে । একটু অপেক্ষা করুন... </span><br>
            </div>
            <div class="">
                <div class="table-container " id="DesignationPerformance">
                    <table class="table table-bordered table-hover tableadvance loadDataTable" >
                        <thead>
                        <tr class="heading">
                            <th colspan="14" class="text-center headText"></th>
                        </tr>
                        <tr class="heading">
                            <th class="text-center" rowspan="2">অফিসের নাম</th>
                            <th class="text-center" rowspan="2"> সর্বশেষ হালনাগাদ</th>
                            <th colspan="2" class="text-center"> ডাক </th>
                            <th colspan="3" class="text-center"> নথি </th>
                            <th colspan="3" class="text-center" > পত্রজারিতে নিষ্পন্ন নোট</th>
                            <th rowspan="2" class="text-center" >পত্রজারির সংখ্যা </th>
                            <th class="text-center" rowspan="2">সময় ব্যবধান</th>
                            <th class="text-center" rowspan="2">রিপোর্ট মার্ক </th>
                            <th class="text-center" rowspan="2">অফিস আইডি </th>

                        </tr>
                        <tr class="heading">
                            <th class="text-center" >গৃহীত </th>
                            <th class="text-center" >নিষ্পন্ন</th>

                            <th class="text-center" > স্ব- উদ্যোগে সৃজিত নোট </th>
                            <th class="text-center" > ডাক থেকে সৃজিত নোট </th>
                            <th class="text-center" > নোটে নিষ্পন্ন </th>

                            <th class="text-center" > আন্তঃসিস্টেম </th>
                            <th class="text-center" > ইমেইল ও অন্যান্য </th>
                            <th class="text-center" >মোট </th>


                        </tr>
                        </thead>
                        <tbody id ="addData">

                        </tbody>
                    </table>
                </div>


                <!--Tools end -->


            </div>
            <br/>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
<!--<script src="https://code.jquery.com/jquery-3.3.1.js"></script>-->
<script src="<?=$this->request->webroot?>assets/global/scripts/printThis.js"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/category_wise_office_performance.js" type="text/javascript"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>-->
<script>
    var mark = <?= json_encode($allreportMarking);?>;
</script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
