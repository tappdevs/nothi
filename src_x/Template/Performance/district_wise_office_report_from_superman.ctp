<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i> জেলা অফিসসমূহের কার্যক্রম বিবরণী  </div>
    </div>
    <div class="portlet-body">
        <div class="row"   id="searchPanel">
            <label class="control-label col-md-2 text-right font-lg">অফিস</label>
            <div class="col-md-4">
                <!--<select class="form-control select2-offscreen">-->
                <?php
                echo $this->Form->create('', ['class' => 'searchForm'])
                ?>
                <?php
                echo $this->Form->hidden('start_date', ['class' => 'startdate'])
                ?>
                <?php
                echo $this->Form->hidden('end_date', ['class' => 'enddate']);
                echo $this->Form->hidden('geo_district_name',['id' => 'geo-district-name']);
                ?>
                <?php
                echo $this->Form->input('geo_district_id',
                    array('label' => false, 'type' => 'select', 'class' => 'form-control',
                        'options' =>  $geo_district_list + [0 => __("All")], 'empty' => '-- বাছাই করুন --'  ));
                ?>
                <?php echo $this->Form->end() ?>
                <!--</select>-->

            </div>


            <!--Tools end -->

            <!--  Date Range Begain -->
            <div class="col-md-6 col-sm-12">
                <div class="col-md-10 col-sm-10">
                    <div class="hidden-print page-toolbar pull-right portlet-title">
                        <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                             data-container="body"
                             data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                            <i class="icon-calendar"></i>&nbsp; <span
                                    class="thin uppercase visible-lg-inline-block">তারিখ নির্বাচন করুন</span>&nbsp;
                            <i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2">
                    <button type="button" class="btn  btn-primary btn-md btn-performance-print"> <i class="fa fa-print">&nbsp;<?=__('Print')?></i> </button>
                </div>
            </div>

            <!--  Date Range End  -->

        </div>
        <br/>
        <div class="inbox-content" id="OfficePerformance">

        </div>
    </div>
</div>

<script src="<?=$this->request->webroot?>assets/global/scripts/printThis.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/new_office_performance_superman.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>

<script>
    jQuery(document).ready(function () {
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        DateRange.init();
        DateRange.initDashboardDaterange();

        $('#geo-district-id').change(function () {
            SortingDate.init($('.startdate').val(), $('.enddate').val())
            $('.table').floatThead({
                position: 'absolute',
                top: jQuery("div.navbar-fixed-top").height()
            });
            $('.table').floatThead('reflow');
        });

    });
</script>