<style>
    .tabletools-dropdown-on-portlet{
        margin: 0px!important;
    }
</style>

<div class="table-container ">
    <table class="table table-bordered table-hover tableadvance"  >
        <thead>
            <tr class="heading">
                <th colspan="15" class="text-center"><q><?=  $office_name?></q> অফিসের কার্যবিবরণী (<?= entobn($startDate)?> - <?= entobn($endDate)?>) </th>
            </tr>
            <tr class="heading">
                <th class="text-center" rowspan="2">অফিসের নাম</th>
                <th class="text-center" rowspan="2"> ব্যবহারকারী</th>
                <th class="text-center" rowspan="2"> সর্বশেষ হালনাগাদ</th>
                <th colspan="3" class="text-center"> ডাক </th>
                <th colspan="4" class="text-center"> নথি </th>
                <th class="text-center" colspan="3"> পত্রজারিতে নিষ্পন্ন নোট</th>
                <th colspan="2" class="text-center"> কর্মকর্তা বিহীন পদবি </th>

            </tr>
            <tr class="heading">
                <th class="text-center" >মোট  গ্রহণ </th>
                <th class="text-center" >মোট নিষ্পন্ন  </th>
                <th class="text-center" >মোট অনিষ্পন্ন </th>
                
                <th class="text-center" >মোট  স্ব- উদ্যোগে নোট </th>
                <th class="text-center" >মোট ডাক থেকে সৃজিত নোট </th>
                <th class="text-center" >মোট নোটে নিষ্পন্ন </th>
                <th class="text-center" >মোট অনিষ্পন্ন নোট </th>

                <th class="text-center" > আন্তঃসিস্টেম </th>
                <th class="text-center" > ইমেইল ও অন্যান্য </th>
                <th class="text-center" >মোট </th>
                
                <th class="text-center" >মোট অনিষ্পন্ন ডাক</th>
                <th class="text-center" >মোট অনিষ্পন্ন নোট </th>

            </tr>
        </thead>
        <tbody>

            <?php if (!empty($data)): ?>
                <?php foreach ($data as $key => $value): ?>
                    <?= "<tr class='text-center'>" ?>
                    <?= "<td >".(!empty($value['name']) ? $value['name'] : '')."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalUser'])."</td>" ?>
                    <?= "<td>".entobn($value['lastUpdate'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalInbox'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalNisponnoDak'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalONisponnoDak'])."</td>" ?>

                    <?= "<td>".\Cake\I18n\Number::format($value['totalSouddog'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalDaksohoNote'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalNisponnoNote'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalONisponnoNote'])."</td>" ?>

                    
                    <?= "<td>".\Cake\I18n\Number::format($value['internal_potrojari'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['external_potrojari'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalNisponnoPotrojari'])."</td>" ?>

                    <?= "<td>".\Cake\I18n\Number::format($value['unassignedPendingDak'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['unassignedPendingNote'])."</td>" ?>
                    <?= "</tr>" ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div>
