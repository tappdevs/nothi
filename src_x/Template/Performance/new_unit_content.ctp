<style>
    .tabletools-dropdown-on-portlet{
        margin: 0px!important;
    }
</style>
<div class="table-container ">
    <table class="table table-bordered table-hover tableadvance">
        <thead>
            <tr class="heading">
                <th class="text-center" rowspan="2">শাখার নাম</th>
                <th class="text-center" colspan="2"> ব্যবহারকারী</th>
                <th colspan="6" class="text-center"> ডাক </th>
                <th colspan="5" class="text-center"> নথি </th>

            </tr>
            <tr class="heading">
                <th class="text-center" >মোট </th>
                <th class="text-center" >গড় </th>
                <th class="text-center" >মোট  গ্রহণ </th>
                <th class="text-center" >মোট  প্রেরণ </th>
                <th class="text-center" > মোট নথিজাত </th>
                <th class="text-center" >মোট নথিতে  উপস্থাপন </th>
                <th class="text-center" >মোট নিষ্পন্ন  </th>
                <th class="text-center" >মোট অনিষ্পন্ন </th>
                <th class="text-center" >মোট  স্ব- উদ্যোগে নোট </th>
                <th class="text-center" >মোট ডাক থেকে সৃজিত নোট </th>
                <th class="text-center" >মোট পত্রজারিতে নিষ্পন্ন </th>
                <th class="text-center" >মোট নোটে নিষ্পন্ন </th>
                <th class="text-center" >মোট অনিষ্পন্ন নোট </th>

            </tr>
        </thead>
        <tbody>
            <?php if (!empty($result)): ?>
                <?php foreach ($result as $key => $value): ?>
                    <?= "<tr class='text-center'>" ?>
                    <?= "<td >".(!empty($value['name']) ? $value['name'] : '')."</td>"
                    ?>
                   <?= "<td>".\Cake\I18n\Number::format($value['totalUser'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalLogin'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalInbox'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalOutbox'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalNothijato'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalNothivukto'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalNisponnoDak'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalONisponnoDak'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalSouddog'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalDaksohoNote'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalNisponnoPotrojari'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalNisponnoNote'])."</td>" ?>
                    <?= "<td>".\Cake\I18n\Number::format($value['totalONisponnoNote'])."</td>" ?>
                    <?= "</tr>" ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div>


<script type="text/javascript" src="<?= $this->request->webroot ?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= $this->request->webroot ?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?= $this->request->webroot ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>


<script>
    var initTable1 = function () {
        var table = $('.tableadvance');

        /* Set tabletools buttons and button container */

        $.extend(true, $.fn.DataTable.TableTools.classes, {
            "container": "btn-group tabletools-dropdown-on-portlet",
            "buttons": {
                "normal": "btn btn-sm default",
                "disabled": "btn btn-sm default disabled"
            },
            "collection": {
                "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
            }
        });

        var oTable = table.dataTable({
            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},
            "language": {// language settings
                sSearch: "খুঁজুন"
            },
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": -1,
            "dom": "<'row' <'col-md-6 col-sm-12'f><'col-md-6 col-sm-12'T>>t", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "tableTools": {
                "sSwfPath": "<?php echo CDN_PATH ?>assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                        "sExtends": "pdf",
                        "sButtonText": "পিডিএফ"
                    }, {
                        "sExtends": "print",
                        "sButtonText": "প্রিন্ট",
                        "sInfo": 'প্রিন্ট করতে Ctrl + P প্রেস করুন',
                        "sMessage": ""
                    }]
            }
        });
    }

    initTable1();
</script>