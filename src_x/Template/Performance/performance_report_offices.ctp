<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            Performance Office Update
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-scrollable">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr class="text-center heading">
                        <th class="text-center"><?= __('Sequence') ?> </th>
                        <th class="text-center"><?= __('Number') ?> </th>
                        <th class="text-center"><?= __('Name') ?></th>
                        <th class="text-center"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($all_offices)):
                        $i = 0;
                        foreach ($all_offices as $key => $val):
                            $i++;
                            ?>
                            <tr class="text-center">
                                <td> <?= $this->Number->format($i) ?> </td>
                                <td> <?= $this->Number->format($key) ?> </td>
                                <td> <?= $val ?>  </td>
                                <td class="offices" id="<?= $key ?>">  </td>
                            </tr>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    offices_ids = [];
    $(document).ready(function () {
        $.each($('.offices'),function (index,val) {
           offices_ids.push(val.id);
        });
       updateOffices(offices_ids[0],0);
    });
function updateOffices(office_id,count){
    if(count >= offices_ids.length){
        return;
    }
    $('#'+office_id).html('<span><i class="fa fa-spin fa-refresh" aria-hidden="true"></i>  Processing  </span>');

    $.ajax({
        type: 'POST',
        url: "<?php echo $this->Url->build(['controller' => 'Performance', 'action' => 'performaceReportofOffices']) ?>",
        data: {"office_id": office_id},
        success: function (data) {
             $('#'+office_id).html();
            if(data.status=='Success'){
            $('#'+office_id).html('<span><i class="fa fa-check" aria-hidden="true"></i>  Done <b>( ' + data.msg + ' )</b></span>');
            }
            else{
                 $('#'+office_id).html('<span><i class="fa fa-times" aria-hidden="true"></i>  Error <b>( ' + data.msg + ' )</b> </span>');
            }
         updateOffices(offices_ids[count+1],count+1);
        },
        error :  function () {
             $('#'+office_id).html();
             $('#'+office_id).html('<span><i class="fa fa-times" aria-hidden="true"></i>  Error  </span>');
              updateOffices(offices_ids[count+1],count+1);
        }
    });

}
</script>

