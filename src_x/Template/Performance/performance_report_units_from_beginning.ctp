<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            Performance Unit Update (<?= date('Y-m-d H')?>)
        </div>
    </div>
    <div class="portlet-body">
        <div class="text-center">
            <div class="easy-pie-chart">
                <div class="number transactions" data-percent="0">
                    <span>0</span>%
                </div>
                <label class="badge badge-info totalCount">Total: <?= count($all_units); ?></label>&nbsp;&nbsp;<label class="badge badge-success doneCount">Completed: 0</label>&nbsp;&nbsp;<label class="badge badge-danger errorCount">Errors: 0</label>
            </div>
        </div>
        <div class="table-scrollable">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr class="text-center heading">
                        <th class="text-center"><?= __('Sequence') ?> </th>
                        <th class="text-center"><?= __('Number') ?> </th>
                        <th class="text-center"><?= __('Name') ?></th>
                        <th class="text-center"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($all_units)):
                        $i = 0;
                        foreach ($all_units as $key => $val):
                            $i++;
                            ?>
                            <tr class="text-center">
                                <td> <?= $this->Number->format($i) ?> </td>
                                <td> <?= $this->Number->format($key) ?> </td>
                                <td> <?= $val ?>  </td>
                                <td class="units" id="<?= $key ?>">  </td>
                            </tr>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="<?= CDN_PATH ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script>
    units_ids = [];
    error = 0;

    $(document).ready(function () {
        if (jQuery().easyPieChart) {
            $('.easy-pie-chart .number.transactions').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: Metronic.getBrandColor('green')
            });
        }

        
        $.each($('.units'),function (index,val) {
           units_ids.push(val.id);
        });
       updateOffices(units_ids[0],0);
    });
function updateOffices(unit_id,count){
    if(count > units_ids.length){
        return;
    }
    $('#'+unit_id).html('<span><i class="fa fa-spin fa-refresh" aria-hidden="true"></i>  Processing  </span>');

    $.ajax({
        type: 'POST',
        url: "<?php echo $this->Url->build(['controller' => 'Performance', 'action' => 'recordFromBeginningforUnits']) ?>",
        data: {"unit_id": unit_id,"date_start" : '<?= $date_start ?>'},
        success: function (data) {
             $('#'+unit_id).html();
            if(data.status=='Success'){
            $('#'+unit_id).html('<span><i class="fa fa-check" aria-hidden="true"></i>  Done <b>( ' + data.msg + ' )</b></span>');
            }
            else{
                $('.errorCount').text("Errors: " + (++error));
                 $('#'+unit_id).html('<span><i class="fa fa-times" aria-hidden="true"></i>  Error <b>( ' + data.msg + ' )</b> </span>');
            }
;
         updateOffices(units_ids[count+1],count+1);

            $('.doneCount').text("Completed: " + (count+1));

            $('.easy-pie-chart .number').each(function () {
                var newValue = Math.floor(100 * ((count+1) / units_ids.length));
                $(this).data('easyPieChart').update(newValue);
                $('span', this).text(newValue);
            });
        },
        error :  function () {
                $('.errorCount').text("Errors: " + (++error));
             $('#'+unit_id).html();
             $('#'+unit_id).html('<span><i class="fa fa-times" aria-hidden="true"></i>  Error  </span>');
              updateOffices(units_ids[count+1],count+1);
              $('.doneCount').text("Completed: " + (count+1));

                $('.easy-pie-chart .number').each(function () {
                    var newValue = Math.floor(100 * ((count+1) / units_ids.length));
                    $(this).data('easyPieChart').update(newValue);
                    $('span', this).text(newValue);
                });
        }
    });

}
</script>

