<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>projapoti-nothi/css/styles.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"
      type="text/javascript">
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-multi-select/css/multi-select.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<style>
    .btn-changelog, .btn-forward-nothi, .btn-nothiback, .btn-print {
        padding: 3px 5px !important;
    }

    .btn-icon-only {

    }

    .editable {
        border: none !important;
        word-break:break-all;
        word-wrap:break-word
    }



    #sovapoti_signature, #sender_signature, #sender_signature2, #sender_signature3 {
        visibility: hidden;
    }

    #sovapoti_signature_date, #sender_signature_date, #sender_signature2_date, #sender_signature3_date {
        visibility: hidden;
    }

    #note {
        overflow: hidden;
        word-break: break-all;
        word-wrap: break-word;
        height: 100%;
    }

    .ms-container {
        width: 100% !important;
    }

    .bangladate {
        border-bottom: 1px solid #000 !important;
    }

    .editable-click, a.editable-click {
        border: none;
        word-break:break-all;
        word-wrap:break-word
    }

    .cc_list {
        white-space: pre-wrap;
    }

    .to_list {
        white-space: pre-wrap;
    }

    .popover-content {
        padding: 10px 30px;
    }

    .mega-menu-dropdown > .dropdown-menu {
        top: 10px !important;
    }

    .tt-dropdown-menu {
        max-height: 100px !important;
        overflow-y: auto;
    }

    .modal-dialog {
        position: absolute!important;
        bottom: 10%;
        right: 25%;
    }
    <?php if($device_type == 'android'){ ?>
    .abc .modal-dialog {
        top: 100px;
    }
    <?php } ?>

</style>
<?php
    $metas = !empty($draftVersion->meta_data)?json_decode($draftVersion->meta_data,true):[];
?>
<?php if ($privilige_type != 1): ?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="portlet box green-seagreen">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="row ">
                        <div class="col-md-12 " style="margin: 0 auto;">
                            <div id="template-body"
                                 style="border:1px solid #aaaaaa; background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto; padding: 2px;">
                                <?php
                                echo $draftVersion->potro_description;
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <h3> <?php echo __(SHONGJUKTI) ?> </h3>
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a href="#tab_prapto_potro" data-toggle="tab" aria-expanded="true">
                                        প্রাপ্ত পত্রসমূহ </a>
                                </li>
                                <li class="">
                                    <a href="#tab_other_potro" data-toggle="tab" aria-expanded="false">
                                        অন্যান্য </a>
                                </li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_prapto_potro">
                                    <?php
                                    $selectmulti = [];

                                    if (!empty($potroAttachmentRecord)) {
                                        foreach ($potroAttachmentRecord as $k => $v) {
                                            $potr = explode(';', $v);
                                            $oth = array_slice($potr, 1);

                                            $selectmulti[$k] = "<a class='previewAt' data-id={$k} > পত্র: 1" . $potr[0] . " - " . implode(' ',
                                                    $oth) . '</a>';
                                        }
                                    }

                                    echo $this->Form->input('prapto_potro',
                                        ['class' => 'multi-select', 'options' => $selectmulti,
                                            'multiple' => 'multiple', 'label' => false,
                                            'default' => $inlineattachments])
                                    ?>
                                </div>
                                <div class="tab-pane" id="tab_other_potro">
                                    <table role="presentation" class="table table-striped">
                                        <thead>
                                        <tr class="text-center">
                                            <td colspan="5"><?=__('Attachments') ?></td>
                                        </tr>
                                        </thead>
                                        <tbody class="files">
                                        <?php
                                        if (isset($attachments) && count($attachments)
                                            > 0
                                        ) {

                                            foreach ($attachments as $single_data) {

                                                if ($single_data['attachment_type']
                                                    == 'text' || $single_data['attachment_type']
                                                    == 'text/html'
                                                ) {
                                                    continue;
                                                }

                                                $fileName = explode('/',
                                                    $single_data['file_name']);
                                                $attachmentHeaders = get_file_type($single_data['file_name']);

                                                $value = array(
                                                    'user_file_name' => !empty($single_data['user_file_name'])?$single_data['user_file_name']:'',
                                                    'name' => urldecode($fileName[count($fileName) - 1]),
                                                    'thumbnailUrl' => (substr($attachmentHeaders,0,5) == 'image'?
                                                        (FILE_FOLDER .$single_data['file_name']) : null),
                                                    'size' => '-',
                                                    'type' => $attachmentHeaders,
                                                    'url' => $single_data['id'].'/showPotroAttachment/'.$nothimasterid,
                                                    'destination' =>FILE_FOLDER .$single_data['file_name'] ,
                                                    'deleteUrl' => $this->Url->build(['_name'=>'secureDelete','?'=>[
                                                        'id'=>$single_data['id'],
                                                        'token'=>$temp_token,
                                                        'user_designation'=> $employee_office['office_unit_organogram_id'],
                                                        'api_key'=> $apikey,
                                                        'data_ref'=>'api',
                                                    ]]),
                                                    'deleteType' => "GET",
                                                    'visibleName' => (!empty($single_data['user_file_name'])?$single_data['user_file_name']:urldecode($fileName[count($fileName) - 1])),
                                                );


                                                echo '<tr class="template-download fade in">
    <td width="20%">
        <span class="preview">
' . (!empty($value['thumbnailUrl']) ? '<a href="' . $value['url'] . '" title="' . $value['name'] . ' " destination="' . $value['destination'] . ' " download="' . $value['url'] . '" data-gallery><img style="height:80px"  src="' . $value['thumbnailUrl'] . '"></a>' : '') . '
</span>
    </td>
    <td width="25%">
    <p class="name">
    ' . (isset($value['name']) ? '<a href="' . $value['url'] . '" title="' . $value['name'] . '" destination="' . $value['destination'] . ' " download="' . $value['name'] . '" '.((!empty($value['thumbnailUrl'])) ?'data-gallery=""':'').' >' . $value['name'] . '</a>' : '') . '
    </p>
    </td>
    <td width="10%">'.$value['size'].'</td>
    <td width="40%"><input type="text" title="' . $value['name'] . '" class="form-control potro-attachment-input" image="' . $single_data['file_name'] . '" onkeyup="check_attachment()" placeholder="সংযুক্তির নাম" value="' . $value['visibleName'].'" ></td>
    <td>
    <button class="btn red delete btn-sm" data-type="' . $value['deleteType'] . '" data-url="' . $value['deleteUrl'] . '" >
                <i class="glyphicon glyphicon-trash"></i>
                <span>মুছে ফেলুন</span>
            </button>
    </td>
    </tr>';
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                </div>
            </div>

        </div>
    </div>
<?php endif; ?>
<script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<?php if ($privilige_type == 1): ?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="portlet box green-seagreen">
                <div class="portlet-body">
                    <span class="potro_language hide"><label class="control-label"> পত্রজারি ভাষাঃ &nbsp;</label><input type="checkbox" id="potrojari_language" class="make-switch" data-on-text = "বাংলা"  data-off-text = "English" data-size="normal" data-off-color="danger" readonly="true"></span>
                    <div class="row ">
                        <div class="col-md-12 " style="margin: 0 auto;">

                            <div class="form">

                                <?php
                                echo $this->Form->create($draftVersion,
                                    array('action' => '#', 'type' => 'file', 'id' => 'potrojariDraftForm'));
                                ?>
                                <?php
                                echo $this->Form->hidden('soft_token');
                                echo $this->Form->hidden('id');
                                echo $this->Form->hidden('can_potrojari');
                                echo $this->Form->hidden('dak_type',
                                    array('label' => false, 'class' => 'form-control',
                                        'value' => DAK_DAPTORIK));
                                echo $this->Form->hidden('dak_status',
                                    array('label' => false, 'class' => 'form-control',
                                        'value' => 1));
                                echo $this->Form->hidden('dak_subject',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sender_sarok_no',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('meta_data',
                                    array('label' => false, 'class' => 'form-control'));

                                //approvalinformation
                                echo $this->Form->hidden('approval_office_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_officer_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_office_unit_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_officer_designation_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_officer_designation_label_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_officer_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_office_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_office_unit_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_visibleName',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_visibleDesignation',
                                    array('label' => false, 'class' => 'form-control'));


                                //sovapotiinformation
                                echo $this->Form->hidden('sovapoti_office_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sovapoti_officer_designation_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sovapoti_officer_designation_label_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sovapoti_officer_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sovapoti_officer_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sovapoti_visibleName',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sovapoti_visibleDesignation',
                                    array('label' => false, 'class' => 'form-control'));

                                //senderinformation
                                echo $this->Form->hidden('sender_office_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sender_officer_designation_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sender_officer_designation_label_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sender_officer_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sender_officer_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sender_officer_visibleName',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sender_officer_visibleDesignation',
                                    array('label' => false, 'class' => 'form-control'));


                                //receiverinformation
                                echo $this->Form->hidden('receiver_group_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_group_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_group_designation_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_group_member_final',
                                    array('label' => false, 'class' => 'form-control'));

                                echo $this->Form->hidden('receiver_office_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_office_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_officer_designation_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_office_unit_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_office_unit_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_officer_designation_label_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_officer_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_officer_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_officer_email_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_office_head_final',
                                    array('label' => false, 'class' => 'form-control'));
                                   echo $this->Form->hidden('receiver_visibleName_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_officer_mobile_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_sms_message_final',
                                    array('label' => false, 'class' => 'form-control'));

                                //onulipiinformation
                                echo $this->Form->hidden('onulipi_group_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_group_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_group_member_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_group_designation_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_office_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_office_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_officer_designation_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_office_unit_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_office_unit_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_officer_designation_label_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_officer_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_officer_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_officer_email_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_office_head_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_visibleName_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_officer_mobile_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_sms_message_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('potro_type',
                                    ['value' => $draftVersion->potro_type,'id' => 'potro-type',]);
                                //potrojari_language
                            echo $this->Form->hidden('potrojari_language',
                                array('label' => false, 'class' => 'form-control','id' => 'potro_language','value' => (isset($draftVersion->potrojari_language) && $draftVersion->potrojari_language == 'eng')?'eng':'bn'));

                        //attension
                    echo $this->Form->hidden('attension_office_id_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_officer_id_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_office_unit_id_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_officer_designation_id_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_officer_designation_label_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_officer_name_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_office_name_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_office_unit_name_final',
                        array('label' => false, 'class' => 'form-control'));
                                ?>
                                <!--dictionary input-->
                                <div>
                                    <input type="hidden"id="word_potro" value="">
                                    <input type="hidden"id="space_potro" value="">
                                </div>
                                <?php

                                echo $this->Cell('Potrojari::apiDisplay',
                                    ['template' => $template_list, 'potrojari' => $draftVersion,'templates'=>isset($templates)?$templates:[], 'is_endorse' => !empty($draftVersion->attached_potro_id)?$draftVersion->attached_potro_id:0, 'employee_office'=>$employee_office])
                                ?>

                                <div class="portlet-body">
                                    <h3> <?php echo __(SHONGJUKTI) ?> </h3>
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs ">
                                            <li class="active">
                                                <a href="#tab_prapto_potro" data-toggle="tab" aria-expanded="true">
                                                    প্রাপ্ত পত্রসমূহ </a>
                                            </li>
                                            <li class="">
                                                <a href="#tab_other_potro" data-toggle="tab" aria-expanded="false">
                                                    অন্যান্য </a>
                                            </li>

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_prapto_potro">
                                                <?php
                                                $selectmulti = [];

                                                if (!empty($potroAttachmentRecord)) {
                                                    foreach ($potroAttachmentRecord as $k => $v) {
                                                        $potr = explode(';', $v);
                                                        $oth = array_slice($potr, 1);

                                                        $selectmulti[$k] = "পত্র: " . $potr[0] . " - " . implode(' ',
                                                                $oth);
                                                    }
                                                }

                                                echo $this->Form->input('prapto_potro',
                                                    ['class' => 'multi-select', 'options' => $selectmulti,
                                                        'multiple' => 'multiple',
                                                        'label' => false, 'default' => $inlineattachments, 'escape' => false])
                                                ?>
                                                <?php echo $this->Form->end(); ?>
                                            </div>
                                            <form action="<?= $this->request->webroot ?>apiPotroDraftView/<?= $nothimasterid ?>/<?= $potrojari_id ?>/<?= $referencetype ?>/<?= $nothi_office ?>" method="post" id="apiPotroDraftViewForm">
                                                <input type="hidden" name="api_key" value="<?= $apikey ?>">
                                                <input type="hidden" name="user_designation" value="<?= $user_designation ?>">
                                                <input type="hidden" name="data_ref" value="api">
                                                <input type="hidden" name="noteViewData" id="noteViewData" value="">
                                                <input type="hidden" name="height" id="height" value="<?= $height ?>">
                                                <input type="hidden" name="width" id="width" value="<?= $width ?>">

                                            </form>
                                            <div class="tab-pane" id="tab_other_potro">
                                                <form id="fileuploadpotrojari"
                                                      action="<?= $this->Url->build(['_name'=>'apiTempUpload']) ?>"
                                                      method="POST" enctype="multipart/form-data">

                                                    <input type="hidden" name="module_type" value="Nothi"/>
                                                    <input type="hidden" name="module" value="Potrojari" />

                                                    <!-- The table listing the files available for upload/download -->
                                                    <table role="presentation" class="table table-striped">
                                                        <tbody class="files">
                                                        <?php
                                                        if (isset($attachments)
                                                            && count($attachments)
                                                            > 0
                                                        ) {

                                                            foreach ($attachments as $single_data) {

                                                                if ($single_data['attachment_type']
                                                                    == 'text'
                                                                    || $single_data['attachment_type']
                                                                    == 'text/html'
                                                                ) {
                                                                    continue;
                                                                }

                                                                $fileName = explode('/', $single_data['file_name']);
                                                                $attachmentHeaders = get_file_type($single_data['file_name']);

                                                                $value = array(
                                                                    'user_file_name' => $single_data['user_file_name'],
                                                                    'name' => urldecode($fileName[count($fileName) - 1]),
                                                                    'thumbnailUrl' => (substr($attachmentHeaders,0,5) == 'image'?
                                                                        (FILE_FOLDER .$single_data['file_name']) : null),
                                                                    'size' => '-',
                                                                    'type' => $attachmentHeaders,
                                                                    'url' => $single_data['id'].'/showPotroAttachment/'.$nothimasterid,
                                                                    'destination' => FILE_FOLDER .$single_data['file_name'],
                                                                    'deleteUrl' => $this->Url->build(['_name'=>'apiSecureDelete','?'=>[
                                                                        'id'=>$single_data['id'],
                                                                        'token'=>$temp_token,
                                                                        'user_designation'=> $employee_office['office_unit_organogram_id'],
                                                                        'api_key'=> $apikey,
                                                                        'data_ref'=>'api',
                                                                    ]]),
                                                                    'deleteType' => "GET",
                                                                    'visibleName' => (!empty($single_data['user_file_name'])?$single_data['user_file_name']:urldecode($fileName[count($fileName) - 1])),
                                                                );


                                                                echo '<tr class="template-download fade in">
    <td width="20%">
        <span class="preview">
' . (!empty($value['thumbnailUrl']) ? '<a class="showforPopup" href="' . $value['url'] . '" title="' . $value['name'] . '" destination="' . $value['destination'] . ' " download="' . $value['url'] . '" data-gallery><img style="height:80px"  src="' . $value['thumbnailUrl'] . '"></a>' : '') . '
</span>
    </td>
    <td width="25%">
    <p class="name">
    ' . (isset($value['name']) ? '<a class="showforPopup" href="' . $value['url'] . '" title="' . $value['name'] . '" destination="' . $value['destination'] . ' " download="' . $value['name'] . '" '.((!empty($value['thumbnailUrl'])) ?'data-gallery=""':'').' >' . $value['name'] . '</a>' : '') . '
    </p>
    </td>
    <td width="10%">'.$value['size'].'</td>
    <td width="40%"><input type="text" title="' . $value['name'] . '" class="form-control potro-attachment-input" image="' . $single_data['file_name'] . '" onkeyup="check_attachment()" placeholder="সংযুক্তির নাম" value="' . $value['visibleName'].'" ></td>
    <td>
    <button class="btn red delete btn-sm" data-type="' . $value['deleteType'] . '" data-url="' . $value['deleteUrl'] . '" >
                <i class="glyphicon glyphicon-trash"></i>
                <span>মুছে ফেলুন</span>
            </button>
    </td>
    </tr>';
                                                            }
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </form>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-actions">
                                    <?php
                                    if ($draftVersion['potro_type'] != 17 && $employee_office['office_unit_organogram_id']
                                        == $draftVersion['officer_designation_id']
                                    ) {
                                        ?>

                                        <input type="checkbox" class="" <?php
                                        echo ($draftVersion['can_potrojari']) ? 'checked'
                                            : ''
                                        ?> id="approve"/> <?php echo __('অনুমোদন ও সংরক্ষণ  ') ?>
                                        <input type="button" class="btn btn-primary saveDraftNothi"
                                               value="<?php echo __(SAVE) ?>"/>
                                        <?php
                                    } else if ($draftVersion['potro_type'] == 17
                                        && $employee_office['office_unit_organogram_id']
                                        == $draftVersion['sovapoti_officer_designation_id']
                                    ) {
                                        ?>
                                        <input type="checkbox" class="" <?php
                                        echo ($draftVersion['can_potrojari']) ? 'checked'
                                            : ''
                                        ?> id="approve"/>  <?php echo __('অনুমোদন ও সংরক্ষণ  ') ?>
                                        <input type="button" class="btn btn-primary saveDraftNothi"
                                               value="<?php echo __(SAVE) ?>"/>
                                    <?php } else { ?>
                                        <input type="button" class="btn btn-primary saveDraftNothi"
                                               value="<?php echo __(SAVE) ?>"/>
                                    <?php } ?>
                                </div>
                                <!--<div class="form-actions">-->

                                <?php //  $this->Form->button(__('অফলাইন খসড়া পত্র মুছে ফেলুন'), ['class' => 'btn red ', 'onclick' => 'offlineRemove();'])         ?>
                                <!--</div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade modal-purple height-auto" id="responsiveOnuccedModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="bottom: auto;right:  auto;position: relative !important;">

            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="closed" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="responsiveNothiUsers" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">
                        পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন
                        <div class="pull-right">
                            <button type="button" class="btn green sendDraftNothi">প্রেরণ করুন</button>
                            <button type="button" data-dismiss="modal" class="btn  btn-danger">
                                বন্ধ করুন
                            </button>
                        </div>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
                        <input type="hidden" name="nothimasterid"/>

                        <div class="user_list">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green sendDraftNothi">প্রেরণ করুন</button>
                    <button type="button" data-dismiss="modal" class="btn  btn-danger">
                        বন্ধ করুন
                    </button>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo $this->Form->hidden('uploaded_attachments');
    echo $this->Form->hidden('uploaded_attachments_names',
        ['id' => 'uploaded_attachments_names'])
    ?>

    <div class="modal fade modal-purple modal-full " id="responsiveModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body ">
                    <table class='table table-striped table-bordered table-hover' id="filelist">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 30%">বিষয়</th>
                            <th class="text-center" style="width: 70%">নাম</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

<form method="get" action="<?= $this->Url->build(['controller'=>'NothiMasters','action'=>'apiPotroPage',$nothimasterid,$nothi_office]) ?>" id="refresh_me">
        <input type="hidden" name="api_key" value="<?= $apikey ?>">
        <input type="hidden" name="data_ref" value="api">
        <input type="hidden" name="user_designation" value="<?= $employee_office['office_unit_organogram_id'] ?>">
</form>
    <input type="hidden" id="note_view_data" value="<?= $noteViewData ?>">
    <link rel="stylesheet" type="text/css"
          href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css"/>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-ui/jquery-ui.min.js"
             type="text/javascript"></script>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
            type="text/javascript"></script>
    <script
            src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
            type="text/javascript"></script>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
            type="text/javascript"></script>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery.blockui.min.js"
            type="text/javascript"></script>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery.cokie.min.js"
            type="text/javascript"></script>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/uniform/jquery.uniform.min.js"
            type="text/javascript"></script>

    <script type="text/javascript"
            src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <?= $this->element('froala_editor_js_css') ?>
    <script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/nothi_master_movements.js"></script>
    <link rel="stylesheet" type="text/css"
          href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>

    <script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/dak_setup.js" type="text/javascript"></script>
    <script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/potrojari_file_update.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery.mockjax.js"></script>
    <script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js"></script>

    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/form-editable.js"></script>
    <script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/potrojari_api_editable.js?v=<?= time() ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
    <script type="text/javascript"
            src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript"
            src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript"
            src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/global/scripts/datatable.js"></script>
    <script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_guard_files.js"></script>
    <script src="<?php echo CDN_PATH; ?>daptorik_preview/js/Sortable.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/potrojari_related.js?v=<?=js_css_version?>" type="text/javascript"></script>

    <script>
        var signature = '<?=!empty($employee_office['default_sign'])?$employee_office['default_sign']:0?>';
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right"
        };
        $(document).ready(function ($) {
            if($('#noteView').length == 0){

                var note_view_data = $("#note_view_data").val();
                var old_data = $('div#note').html();
                if(!isEmpty(note_view_data)){
                    old_data = note_view_data;
                }

                $('div#note').after('<div data-original-title="Enter notes" data-toggle="manual" data-type="wysihtml5" data-pk="1" id="noteView" class="editable" tabindex="-1" style="display:inline;">'+ old_data +'<br/>');
                $('div#note').replaceWith('<textarea name="" id="note" style="display: none;"></textarea>');

            } else {
                var note_view_data = $("#note_view_data").val();
                if(!isEmpty(note_view_data)){
                    $('#noteView').html(note_view_data);
                }
            }
            Metronic.initSlimScroll('.receiver_group_list');
            Metronic.initSlimScroll('.onulipi_group_list');

        });

        var numbers = {
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯',
            0: '০'
        };

        function replaceNumbers(input) {
            var output = [];
            for (var i = 0; i < input.length; ++i) {
                if (numbers.hasOwnProperty(input[i])) {
                    output.push(numbers[input[i]]);
                } else {
                    output.push(input[i]);
                }
            }
            return output.join('');
        }

        var DRAFT_FORM = {
            attached_files: [],
            sender_users: [],
            receiver_users: [],
            onulipi_users: [],
            setform: function () {

                if ($('.fr-toolbar').length > 0) {
                    $("#pencil").click();
                }
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-center"
                };
                var senders = $('.sender_users');
                var approvals = $('.approval_users');
                var receivers = $('.receiver_users');
                var onulipis = $('.onulipi_users');
                var sovapotis = $('.sovapoti_users');

                $('#sovapoti_signature').html("");
                $('#sovapoti_designation').html("");
                $('#sovapotiname').html("");
                $('#sovapoti_designation2').html("");
                $('#sovapotiname2').html("");
                $('#sender_signature').html("");
                $('#sender_signature2').html("");
                $('#sender_designation').html("");
                $('#sender_name').html("");
                $('#sender_designation2').html("");
                $('#sender_designation3').html("");
                $('#sender_name2').html("");
                $('#sender_name3').html("");
                //$('#office_organogram_id').html("");
                $('#address').html("");

                 //LM
            if($('[name=potro_type]').val() == 20){
                check_attachment();
                if($("#reference").length == 0 || $.trim($("#reference").text())=='...'){
                    $(".remove_div_if_empty").hide();
                }
                if($("#attachment_list tbody tr").length == 0){
                    $(".sologni_div").hide();
                }
                 if($(".cc_div_item").length == 0){
                    $(".getarthe").hide();
                }
            }
            if(DRAFT_FORM.setSovapotiForm(sovapotis) == false){
                return false;
            }
            if(DRAFT_FORM.setSenderForm(senders) == false){
                return false;
            }
            if(DRAFT_FORM.setApprovalForm(approvals) == false){
                return false;
            }
            if(DRAFT_FORM.setReceiverForm(receivers) == false){
                return false;
            }
            if(DRAFT_FORM.setOnulipiForm(onulipis) == false){
                return false;
            }
                if($('[name=potro_type]').val() == 11 || $('[name=potro_type]').val() == 24){
                    $('#sender_name2').remove();
                    $('#sender_designation2').remove();
                    $('#sender_signature2_date').remove();
                }
                if ($('#potrojariDraftForm').find('#office_organogram_id').length == 0) {
                    if ($('#potrojariDraftForm').find('#cc_list_div').length != 0) {
                        if (onulipis.length == 0) {
                            toastr.error("দুঃখিত! অনুলিপি/বিতরন দেয়া হয়নি");
                            return false;
                        }
                    }
                } else {
                    if (receivers.length == 0 && $('input[name=potro_type]').val() != 19) {
                        toastr.error("দুঃখিত! প্রাপক দেয়া হয়নি");
                        return false;
                    }
                }

                DRAFT_FORM.attached_files = [];
                DRAFT_FORM.attached_files_names = [];
                $('.template-download').each(function () {
                    var link_td = $(this).find('.name');
                    var href = $.trim($(link_td).find('a').attr('destination'));
                    DRAFT_FORM.attached_files.push(href);
                });
                $('.template-download .potro-attachment-input').each(function () {
                    var name = $(this).val();
                    if(isEmpty(name)){
                        name = $(this).attr('title');
                    }
                    DRAFT_FORM.attached_files_names.push(name);
                });
                $("#uploaded_attachments_names").val(DRAFT_FORM.attached_files_names);
                $("#uploaded_attachments").val(DRAFT_FORM.attached_files);
                $("#file_description").val($("#file_description_upload").val());
                var subject = $('#subject').text();
                var sender_sarok_no = $('#sharok_no').text();

                if ($('input[name=potro_type]').val() == 13) {
                    subject = $('#blank-subject').val();
                }

                if ($('input[name=potro_type]').val() == 13) {
                    sender_sarok_no = $('#blank-sarok').val();
                }

                $('input[name=sender_sarok_no]').val(sender_sarok_no);
                $('input[name=dak_subject]').val(subject);

                var temp = $('#template-body').html();

                if (temp.length == 0 || $('input[name=potro_type]').val() == '') {
                    toastr.error("দুঃখিত! পত্র দেয়া হয়নি");
                    return false;
                }

                if ($('input[name=dak_subject]').val().length == 0) {
                    $('input[name=dak_subject]').val($('#potro-type option:selected').text());
                }

                if ($('input[name=sender_sarok_no]').val().length == 0) {
                    toastr.error("দুঃখিত! স্মারক নম্বর দেয়া হয়নি");
                    return false;
                }

				if ($('#potrojariDraftForm').find('#office_organogram_id').length > 0 && $('.receiver_users').length > 0) {
					if ($('#potrojariDraftForm').find('#cc_list_div').length > 0) {
						if ($('.onulipi_users').length == 0) {
							$('#potrojariDraftForm').find('#cc_list_div').closest('.row').nextAll('.row').hide()
							$('#potrojariDraftForm').find('#cc_list_div').closest('.row').hide();
							$('#potrojariDraftForm').find('#sharok_no2').closest('.row').hide();
							$('#potrojariDraftForm').find('#sending_date_2').closest('.row').hide();
						}
					}
				}

				$('#template-body').find('#pencil').remove();
				$.each($('#potrojariDraftForm').find('a'), function (i, v) {

					if($('[name=potro_type]').val() == 5){
						if($(this).attr('id') == 'subject'){
							if ($(this).text().length == 0 || $(this).text() == '...') {
								$(this).closest('.row').remove();
							}
						}
					}
					if ($(this).hasClass('closethis') ||
						$(this).hasClass('savethis') ||
						$(this).attr('id') == 'sovadate' ||
						$(this).attr('id') == 'sovatime' ||
						$(this).attr('id') == 'sovaplace' ||
						$(this).attr('id') == 'sovapresent' ||
						$(this).attr('id') == 'subject') {
					}
					else if($('[name=potro_type]').val() == 20){
						if ($(this).attr('id') == 'gov_name_noc' || $(this).attr('id') == 'form_name_noc' || $(this).attr('id') == 'ministry_name_noc' || $(this).attr('id') == 'sharok_no' || $(this).attr('id') == 'reference' || $(this).attr('id') == 'sender_unit' || $(this).attr('id') == 'extension' || $(this).attr('id') == 'LM_date_format') {
							if($.trim($(this).text()) == '...') {
								$(this).text('');
							}
						}
						if ($(this).attr('class') == 'lm_songlogni_set ' || $.trim($(this).text()) == '...'){
							$(this).text('');
						}
					}
					else {
						if ($(this).text().length == 0 || $.trim($(this).text()) == '...') {
							if ($(this).attr('id') == 'left_slogan' || $(this).attr('id') == 'right_slogan') {
								$(this).text('');
							}
						}
					}
				});
                //remove empty end


				if($('[name=potro_type]').val() == 13){
					var para1 = $('#note').html();
					var para2 = $("#para1").html();
					var para3 = $("#para2").html();
					var para4 = $("#para3").html();
					var para5 = $("#para4").html();
					var contentbody = '<div style="padding:10px;"><div id="para1Data" style="word-wrap: break-word;">' + para1.replace(/\r?\n/g, '<br />') + '</div><br/><br/><div id="para2Data" style="float:left;width: 45%;word-wrap: break-word;">' + para2.replace(/\r?\n/g, '<br />') +
						'</div><div id="para3Data" style="float: right;width: 45%;text-align: center;word-wrap: break-word;">' + para3.replace(/\r?\n/g, '<br />') + '</div> <div style="clear: both;"></div> <br/><div id="para4Data" style="clear:both;word-wrap: break-word;float:left;width: 45%;">' + para4.replace(/\r?\n/g, '<br />') +
						'</div><div id="para5Data" style="float: right;width: 45%;text-align: center;word-wrap: break-word;">' + para5.replace(/\r?\n/g, '<br />') + '</div></div>';
					$('#contentbody').text(contentbody);
				}else{
					$.each($('#template-body').find('a.editable'), function (i, v) {
						var dataType = $(this).attr('data-type');
						var id = $(this).attr('id');
						if(dataType=='textarea'){
							var txt = $(this).html();
						}else{
							var txt = $(this).text();
						}
						if(id=="to_div_item") {
							$(this).replaceWith("<span class='canedit' style='white-space:pre-wrap;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</span>");
						}else {
							if($(this).hasClass('potro_security')){
								$(this).replaceWith("<span class='canedit potro_security' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</span>");
							}else{
								$(this).replaceWith("<span class='canedit' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</span>");
							}
						}
					});

					$.each($('#potrojariDraftForm').find('.canedit'), function(i,j) {

						if($.trim($(this).text()) == '...' || $.trim($(this).text()).length == 0) {
							if ($(this).attr('id') == 'reference' || $(this).attr('id') == 'sender_phone'
								|| $(this).attr('id') == 'sender_fax' || $(this).attr('id') == 'sender_email') {
								$(this).closest('.row').hide();
							}
							else if($(this).attr('id') == 'gov_name' || $(this).attr('id') == 'office_ministry' || $(this).attr('id') == 'offices' || $(this).attr('id') == 'unit_name_editable' || $(this).attr('id') == 'web_url_or_office_address' || $(this).attr('id') == 'office_address') {
								$(this).prev('br').remove();
								$(this).remove();
							}else if($(this).attr('id') == 'dynamic_header') {
								$(this).next('br').remove();
								$(this).remove();
							}
						}
					});

                    var contentbody = $('#template-body').html();
                    $('#contentbody').text(contentbody);
                    $('#template-body').html(temp);
                }
            },
            setSovapotiForm: function(sovapotis){
                if ($('input[name=potro_type]').val() == 17) {
                    if (sovapotis.length == 0) {
                        toastr.error("দুঃখিত! সভাপতি তথ্য দেয়া হয়নি");
                        return false;
                    } else {

                        $.each($('.sovapoti_users'), function (i, data) {
                            var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
                            if (sendename.length > 0) {
                            } else {
                                sendename[0] = sendename;
                            }
                            $('input[name=sovapoti_office_id_final]').val($(this).attr('ofc_id'));
                            $('input[name=sovapoti_officer_designation_id_final]').val($(this).attr('designation_id'));
                            $('input[name=sovapoti_officer_designation_label_final]').val($(this).attr('designation'));
                            $('input[name=sovapoti_officer_id_final]').val($(this).attr('officer_id'));
                            $('input[name=sovapoti_officer_name_final]').val(sendename[0]);
                            $('input[name=sovapoti_visibleName]').val(isEmpty($(this).attr('visibleName'))?sendename[0]: $.trim($(this).attr('visibleName')));
                            $('input[name=sovapoti_visibleDesignation]').val(isEmpty($(this).attr('visibleDesignation'))?$.trim($(this).attr('designation')): $.trim($(this).attr('visibleDesignation')));
                            var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' +$(this).attr('officer_id');
                            if(!isEmpty($(this).attr('unit_id'))){
                                url =url + '/'+$(this).attr('unit_id');
                            }
                            $.ajax({
                                url: url,
                                type: 'post',
                                dataType: 'json',
                                async: false,
                                success: function (data) {
                                    PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                                        $('#sovapoti_signature').html('<img src="' + src + '" alt="signature" width="100" />');
                                        if($("#potro_language").val() == 'eng'){
                                            $('#sovapoti_signature_date').html('<?= bnToen($signature_date_bangla) ?>');
                                        }else{
                                            $('#sovapoti_signature_date').html('<?= $signature_date_bangla ?>');
                                        }

                                        if(i==0){
                                            $('#sovapoti_signature2').html('<img src="' + src + '" alt="signature" width="100" />');
                                            if($("#potro_language").val() == 'eng'){
                                                $('#sovapoti_signature2_date').html('<?= bnToen($signature_date_bangla) ?>');
                                            }else{
                                                $('#sovapoti_signature2_date').html('<?= $signature_date_bangla ?>');
                                            }

                                        }
                                    });

                                }
                            });

                            if (i == 0) {
                                $('#sovapoti_designation').text($('input[name=sovapoti_visibleDesignation]').val());
                                $('#sovapotiname').text($('input[name=sovapoti_visibleName]').val());
                                $('#sovapoti_designation2').text($('input[name=sovapoti_visibleDesignation]').val());
                                $('#sovapotiname2').text($('input[name=sovapoti_visibleName]').val());
                            }
                        });
                    }
                }
            },
            setSenderForm: function(senders){
                if (senders.length == 0) {
                    if ($('[name=potro_type]').val() == 17) {
                        $('#sender_name').html('');
                        $.each($('.approval_users'), function (i, data) {
                            var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));

                            if (i > 0) {
                                return;
                            }
                            $('input[name=sender_office_id_final]').val($(this).attr('ofc_id'));
                            $('input[name=sender_officer_designation_id_final]').val($(this).attr('designation_id'));
                            $('input[name=sender_officer_designation_label_final]').val($(this).attr('designation'));
                            $('input[name=sender_officer_id_final]').val($(this).attr('officer_id'));
                            $('input[name=sender_officer_name_final]').val(sendename[0]);
                            $('input[name=sender_officer_visibleName]').val(isEmpty($(this).attr('visibleName'))?$.trim(sendename[0]): $.trim($(this).attr('visibleName')));
                            $('input[name=sender_officer_visibleDesignation]').val(isEmpty($(this).attr('visibleDesignation'))?$.trim($(this).attr('designation')):$.trim($(this).attr('visibleDesignation')));
                            $('#sender_name').text($('input[name=sender_officer_visibleName]').val());
                            $('#sender_designation').text($('input[name=sender_officer_visibleDesignation]').val());


                            //$('#unit_name_editable').text(($('#unit_name_editable').text().length == 0 || $('#unit_name_editable').text() == '...') ? $('.sender_users').eq(0).attr('unit_name') : $('#unit_name_editable').text());
                            var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' +$(this).attr('officer_id');
                            if(!isEmpty($(this).attr('unit_id'))){
                                url =url + '/'+$(this).attr('unit_id');
                            }
                            $.ajax({
                                url: url,
                                type: 'post',
                                dataType: 'json',
                                async: false,
                                success: function (data) {
                                    PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                                        $('#sender_signature').html('<img src="' + src + '" alt="signature" width="100" />');
                                        if($("#potro_language").val() == 'eng'){
                                            $('#sender_signature_date').html('<?= bnToen($signature_date_bangla) ?>');
                                        }else{
                                            $('#sender_signature_date').html('<?= $signature_date_bangla ?>');
                                        }

                                    });

                                    if ($.trim($('#potrojariDraftForm').find('#sender_phone').text()).length > 0 && $.trim($('#potrojariDraftForm').find('#sender_phone').text()) != '...' && data.personal_mobile.length > 0) {
                                        if($("#potro_language").val() == 'eng'){
                                            $('#sender_phone').text(EngFromBn(data.personal_mobile));
                                        }else{
                                            $('#sender_phone').text(data.personal_mobile);
                                        }
                                    }
                                }
                            });

                        });
                    } else {
                        toastr.error("দুঃখিত! অনুমোদনকারী তথ্য দেয়া হয়নি");
                        return false;
                    }
                }
                else {
                    $('#sender_name').html('');
                    $.each($('.sender_users'), function (i, data) {
                        if (i > 0) {
                            toastr.error("একাধিক পদবি দেয়া যাবে না। ");
                            return
                        }
                        var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));

                        $('input[name=sender_office_id_final]').val($(this).attr('ofc_id'));
                        $('input[name=sender_officer_designation_id_final]').val($(this).attr('designation_id'));
                        $('input[name=sender_officer_designation_label_final]').val($(this).attr('designation'));
                        $('input[name=sender_officer_id_final]').val($(this).attr('officer_id'));
                        $('input[name=sender_officer_name_final]').val(sendename[0]);
                        $('input[name=sender_officer_visibleName]').val(isEmpty($(this).attr('visibleName'))?$.trim(sendename[0]): $.trim($(this).attr('visibleName')));
                        $('input[name=sender_officer_visibleDesignation]').val(isEmpty($(this).attr('visibleDesignation'))?$.trim($(this).attr('designation')):$.trim($(this).attr('visibleDesignation')));
                        $('#sender_designation').html($(this).attr('designation'));
                        $('#sender_name').html(sendename[0]);
                        var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' +$('.sender_users').eq(0).attr('officer_id');
                        if(!isEmpty($('.sender_users').eq(0).attr('unit_id'))){
                            url =url + '/'+$('.sender_users').eq(0).attr('unit_id');
                        }
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            async:false,
                            success: function (data) {

                                if ($('input[name=potro_type]').val() != 13) {
                                    PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                                        $('#sender_signature').html('<img src="' + src + '" alt="signature" width="100" />');
                                        if($("#potro_language").val() == 'eng'){
                                            $('#sender_signature_date').html('<?= bnToen($signature_date_bangla) ?>');
                                        }else{
                                            $('#sender_signature_date').html('<?= $signature_date_bangla ?>');
                                        }

                                    });
                                    $('#sender_designation').text($.trim($('input[name=sender_officer_visibleDesignation]').val()));
                                    $('#sender_name').text($.trim($('input[name=sender_officer_visibleName]').val()));
                                }

                                if ($.trim($('#potrojariDraftForm').find('#sender_email').text()).length > 0 && $.trim($('#potrojariDraftForm').find('#sender_email').text()) != '...') {
                                } else {
                                    if (data.personal_email.length > 0) {
                                        $('#sender_email').text($.trim(data.personal_email));
                                    }
                                }
                                if ($.trim($('#potrojariDraftForm').find('#sender_phone').text()).length > 0 && $.trim($('#potrojariDraftForm').find('#sender_phone').text()) != '...') {
                                } else {
                                    if (data.personal_mobile.length > 0) {
                                        if($("#potro_language").val() == 'eng'){
                                            $('#sender_phone').text(EngFromBn(data.personal_mobile));
                                        }else{
                                            $('#sender_phone').text(data.personal_mobile);
                                        }

                                    }
                                }
                            }
                        });
                    });
                }
            },
            setApprovalForm: function(approvals){
                if (approvals.length != 0) {
                    $('#potrojariDraftForm').find('#sender_name2').closest('.row').show();
                    $('#potrojariDraftForm').find('#sender_designation2').closest('.row').show();
                    $('#potrojariDraftForm').find('#sender_signature2').closest('.row').show();

                    $.each($('.approval_users'), function (i, data) {
                        if ($('input[name=sender_officer_designation_id_final]').val() == $(this).attr('designation_id') && $('input[name=potro_type]').val() != 4) {
                            $('#potrojariDraftForm').find('#sender_name2').closest('.row').hide();
                            $('#potrojariDraftForm').find('#sender_designation2').closest('.row').hide();
                            $('#potrojariDraftForm').find('#sender_signature2').closest('.row').hide();
                        }
                        if (i > 0) {
                            toastr.error("একাধিক পদবি দেয়া যাবে না। ");
                            return;
                        }
                        var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
                        var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' +$(this).attr('officer_id');
                        if(!isEmpty($(this).attr('unit_id'))){
                            url =url + '/'+$(this).attr('unit_id');
                        }
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            async: false,
                            success: function (data) {
                                PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                                    if($('input[name=potro_type]').val() ==17){
                                        $('#sender_signature').html('<img src="' + src + '" alt="signature" width="100" />');
                                        if($("#potro_language").val() == 'eng'){
                                            $('#sender_signature_date').html('<?= bnToen($signature_date_bangla) ?>');
                                        }else{
                                            $('#sender_signature_date').html('<?= $signature_date_bangla ?>');
                                        }
                                    }
                                    $('#sender_signature2').html('<img src="' + src + '" alt="signature" width="100" />');
                                    if($("#potro_language").val() == 'eng'){
                                        $('#sender_signature2_date').html('<?= bnToen($signature_date_bangla) ?>');
                                    }else{
                                        $('#sender_signature2_date').html('<?= $signature_date_bangla ?>');
                                    }

                                });

                            }
                        });

                        if (i > 0) {
                            $('#sender_designation2').append(', ');
                            $('#sender_name2').append(', ');
                            $('#sender_designation3').append(', ');
                            $('#sender_name3').append(', ');
                        }
                        $('input[name=approval_visibleName]').val(isEmpty($(this).attr('visibleName'))?$.trim(sendename[0]): $.trim($(this).attr('visibleName')));
                        $('input[name=approval_visibleDesignation]').val(isEmpty($(this).attr('visibleDesignation'))?$.trim($(this).attr('designation')): $.trim($(this).attr('visibleDesignation')));
                        $('#sender_designation2').text($('input[name=approval_visibleDesignation]').val());
                        $('#sender_name2').text($('input[name=approval_visibleName]').val());
                        $('#sender_designation3').text($('input[name=approval_visibleDesignation]').val());
                        $('#sender_name3').text($('input[name=approval_visibleName]').val());
                        if($('input[name=potro_type]').val() ==17){
                            $('#sender_designation').text($('input[name=approval_visibleDesignation]').val());
                            $('#sender_name').text($('input[name=approval_visibleName]').val());
                        }
                        $('input[name=approval_office_id_final]').val($(this).attr('ofc_id'));
                        $('input[name=approval_officer_designation_id_final]').val($(this).attr('designation_id'));
                        $('input[name=approval_officer_designation_label_final]').val($(this).attr('designation'));
                        $('input[name=approval_officer_id_final]').val($(this).attr('officer_id'));
                        $('input[name=approval_office_unit_id_final]').val($(this).attr('unit_id'));
                        $('input[name=approval_office_unit_name_final]').val($(this).attr('unit_name'));
                        $('input[name=approval_officer_name_final]').val(sendename[0]);
                        $('input[name=approval_office_name_final]').val($(this).attr('ofc_name'));

                    });
                }
                else {

                    $.each($('.sender_users'), function (i, data) {
                        var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
                        var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' +$(this).attr('officer_id');
                        if(!isEmpty($(this).attr('unit_id'))){
                            url =url + '/'+$(this).attr('unit_id');
                        }
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            async: false,
                            success: function (data) {
                                PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                                    $('#sender_signature2').html('<img src="' + src + '" alt="signature" width="100" />');
                                    if($("#potro_language").val() == 'eng'){
                                        $('#sender_signature2_date').html('<?= bnToen($signature_date_bangla) ?>');
                                    }else{
                                        $('#sender_signature2_date').html('<?= $signature_date_bangla ?>');
                                    }

                                });

                            }
                        });

                        if (i > 0) {
                            $('#sender_designation2').append(', ');
                            $('#sender_name2').append(', ');
                            $('#sender_designation3').append(', ');
                            $('#sender_name3').append(', ');
                        }
                        $('input[name=approval_visibleName]').val(isEmpty($(this).attr('visibleName'))?$.trim(sendename[0]): $.trim($(this).attr('visibleName')));
                        $('input[name=approval_visibleDesignation]').val(isEmpty($(this).attr('visibleDesignation'))?$.trim($(this).attr('designation')): $.trim($(this).attr('visibleDesignation')));
                        $('#sender_designation2').text($('input[name=approval_visibleDesignation]').val());
                        $('#sender_name2').text($('input[name=approval_visibleName]').val());

                        $('#sender_designation3').text($('input[name=approval_visibleDesignation]').val());
                        $('#sender_name3').text($('input[name=approval_visibleName]').val());

                        $('input[name=approval_office_id_final]').val($(this).attr('ofc_id'));
                        $('input[name=approval_officer_designation_id_final]').val($(this).attr('designation_id'));
                        $('input[name=approval_officer_designation_label_final]').val($(this).attr('designation'));
                        $('input[name=approval_officer_id_final]').val($(this).attr('officer_id'));
                        $('input[name=approval_office_unit_id_final]').val($(this).attr('unit_id'));
                        $('input[name=approval_office_unit_name_final]').val($(this).attr('unit_name'));
                        $('input[name=approval_officer_name_final]').val(sendename[0]);
                        $('input[name=approval_office_name_final]').val($(this).attr('ofc_name'));
                        if ($('input[name=sender_officer_designation_id_final]').val() == $(this).attr('designation_id') && $('input[name=potro_type]').val() != 4) {
                            $('#potrojariDraftForm').find('#sender_name2').closest('.row').hide();
                            $('#potrojariDraftForm').find('#sender_designation2').closest('.row').hide();
                            $('#potrojariDraftForm').find('#sender_signature2').closest('.row').hide();
                        }
                    });
                }
            },
            setReceiverForm: function(receivers){
                if (receivers.length == 0) {
                    if ($('input[name=potro_type]').val() != 19 && ($('#potrojariDraftForm').find('#office_organogram_id').length > 0 || $('input[name=potro_type]').val() == 13)){
                        toastr.error("দুঃখিত! প্রাপক তথ্য দেয়া হয়নি");
                        return false;
                    } else {

                        $('input[name=receiver_group_id_final]').val('');
                        $('input[name=receiver_group_name_final]').val('');
                        $('input[name=receiver_group_member_final]').val('');
                        $('input[name=receiver_group_designation_final]').val('');
                        $('input[name=receiver_office_id_final]').val('');
                        $('input[name=receiver_office_name_final]').val('');
                        $('input[name=receiver_officer_designation_id_final]').val('');
                        $('input[name=receiver_officer_designation_label_final]').val('');
                        $('input[name=receiver_office_unit_id_final]').val('');
                        $('input[name=receiver_office_unit_name_final]').val('');
                        $('input[name=receiver_officer_id_final]').val('');
                        $('input[name=receiver_officer_name_final]').val('');
                        $('input[name=receiver_officer_email_final]').val('');
                        $('input[name=receiver_office_head_final]').val('');
                        $('input[name=receiver_visibleName_final]').val('');
                        $('input[name=receiver_officer_mobile_final]').val('');
                        $('input[name=receiver_sms_message_final]').val('');
                    }
                }
                else {
                    if ($('#potrojariDraftForm').find('#office_organogram_id').length > 0) {
                        if (receivers.length == 1) {
                            $.each(receivers, function (i, data) {

                                if (typeof ($(this).attr('group_id')) != 'undefined') {
                                    $('input[name=receiver_group_member_final]').val($(this).attr('group_member'));

                                    $('input[name=receiver_group_id_final]').val($(this).attr('group_id'));
                                    $('input[name=receiver_group_name_final]').val($(this).attr('group_name'));
                                    $('input[name=receiver_group_designation_final]').val($(this).attr('group_designation'));
                                } else {
                                    $('input[name=receiver_office_id_final]').val($(this).attr('ofc_id'));
                                    $('input[name=receiver_office_name_final]').val($(this).attr('ofc_name'));
                                    $('input[name=receiver_officer_designation_id_final]').val($(this).attr('designation_id'));
                                    $('input[name=receiver_officer_designation_label_final]').val($(this).attr('designation'));
                                    $('input[name=receiver_office_unit_id_final]').val($(this).attr('unit_id'));
                                    $('input[name=receiver_office_unit_name_final]').val($(this).attr('unit_name'));
                                    $('input[name=receiver_officer_id_final]').val($(this).attr('officer_id'));
                                    $('input[name=receiver_officer_name_final]').val($(this).attr('officer_name'));
                                    $('input[name=receiver_officer_email_final]').val($(this).attr('officer_email'));
                                    $('input[name=receiver_office_head_final]').val($(this).attr('office_head'));
                                    var visibleName = (!isEmpty($(this).attr('visibleName')))?$(this).attr('visibleName'):'';
                                    var officer_mobile = (!isEmpty($(this).attr('officer_mobile')))?$(this).attr('officer_mobile'):'';
                                    var sms_message = (!isEmpty($(this).attr('sms_message')))?$(this).attr('sms_message'):'';
                                    $('input[name=receiver_visibleName_final]').val(visibleName);
                                    $('input[name=receiver_officer_mobile_final]').val(officer_mobile);
                                    $('input[name=receiver_sms_message_final]').val(sms_message);
                                }
                            });
                        }
                        else {

                            var prevgroupId = '';
                            var prevgroupNm = '';
                            var prevgroupNmMem = '';
                            var prevOfficeId = '';
                            var prevOfficeNm = '';
                            var prevOfficeOrgId = '';
                            var prevOfficeOrgLb = '';
                            var prevOfficeUnitId = '';
                            var prevOfficeUnitLb = '';
                            var prevOfficerId = '';
                            var prevOfficerNm = '';
                            var prevOfficerEm = '';
                            var prevOfficerHd = '';
                            var prevgroupDes = '';
                            var prevOfficerVisibleName = '';
                            var prevOfficerMobile = '';
                            var prevOfficerSMSMessage = '';
                            $.each(receivers, function (i, data) {
                                if (typeof ($(this).attr('group_id')) != 'undefined') {
                                    $('input[name=receiver_group_id_final]').val(prevgroupId + ";" + $(this).attr('group_id'));
                                    prevgroupId = $('input[name=receiver_group_id_final]').val();
                                    $('input[name=receiver_group_name_final]').val(prevgroupNm + ";" + $(this).attr('group_name'));
                                    prevgroupNm = $('input[name=receiver_group_name_final]').val();

                                    $('input[name=receiver_group_designation_final]').val(prevgroupDes + ";" + $(this).attr('group_designation'));
                                    prevgroupDes = $('input[name=receiver_group_designation_final]').val();

                                    $('input[name=receiver_group_member_final]').val(prevgroupNmMem + ";" + $(this).attr('group_member'));
                                    prevgroupNmMem = $('input[name=receiver_group_member_final]').val();
                                }

                                $('input[name=receiver_office_id_final]').val(prevOfficeId + ";" + $(this).attr('ofc_id'));
                                prevOfficeId = $('input[name=receiver_office_id_final]').val();
                                $('input[name=receiver_office_name_final]').val(prevOfficeNm + ";" + $(this).attr('ofc_name'));
                                prevOfficeNm = $('input[name=receiver_office_name_final]').val();
                                $('input[name=receiver_officer_designation_id_final]').val(prevOfficeOrgId + ";" + $(this).attr('designation_id'));
                                prevOfficeOrgId = $('input[name=receiver_officer_designation_id_final]').val();
                                $('input[name=receiver_officer_designation_label_final]').val(prevOfficeOrgLb + ";" + $(this).attr('designation'));
                                prevOfficeOrgLb = $('input[name=receiver_officer_designation_label_final]').val();
                                $('input[name=receiver_office_unit_id_final]').val(prevOfficeUnitId + ";" + $(this).attr('unit_id'));
                                prevOfficeUnitId = $('input[name=receiver_office_unit_id_final]').val();
                                $('input[name=receiver_office_unit_name_final]').val(prevOfficeUnitLb + ";" + $(this).attr('unit_name'));
                                prevOfficeUnitLb = $('input[name=receiver_office_unit_name_final]').val();
                                $('input[name=receiver_officer_id_final]').val(prevOfficerId + ";" + $(this).attr('officer_id'));
                                prevOfficerId = $('input[name=receiver_officer_id_final]').val();
                                $('input[name=receiver_officer_name_final]').val(prevOfficerNm + ";" + $(this).attr('officer_name'));
                                prevOfficerNm = $('input[name=receiver_officer_name_final]').val();
                                $('input[name=receiver_officer_email_final]').val(prevOfficerEm + ";" + $(this).attr('officer_email'));
                                prevOfficerEm = $('input[name=receiver_officer_email_final]').val();
                                $('input[name=receiver_office_head_final]').val(prevOfficerHd + ";" + $(this).attr('office_head'));
                                prevOfficerHd = $('input[name=receiver_office_head_final]').val();
                                var visibleName = (!isEmpty($(this).attr('visibleName')))?$(this).attr('visibleName'):'';
                                var officer_mobile = (!isEmpty($(this).attr('officer_mobile')))?$(this).attr('officer_mobile'):'';
                                var sms_message = (!isEmpty($(this).attr('sms_message')))?$(this).attr('sms_message'):'';
                                $('input[name=receiver_visibleName_final]').val(prevOfficerVisibleName + ";" + visibleName);
                                prevOfficerVisibleName = $('input[name=receiver_visibleName_final]').val();
                                $('input[name=receiver_officer_mobile_final]').val(prevOfficerMobile + ";" + officer_mobile);
                                prevOfficerMobile = $('input[name=receiver_officer_mobile_final]').val();
                                $('input[name=receiver_sms_message_final]').val(prevOfficerSMSMessage + ";" + sms_message);
                                prevOfficerSMSMessage = $('input[name=receiver_sms_message_final]').val();
                            });
                        }
                    } else if ($('input[name=potro_type]').val() == 13) {
                        if (receivers.length == 1) {
                            $.each(receivers, function (i, data) {
                                if (typeof ($(this).attr('group_id')) != 'undefined') {
                                    $('input[name=receiver_group_member_final]').val($(this).attr('group_member'));
                                    $('input[name=receiver_group_id_final]').val($(this).attr('group_id'));
                                    $('input[name=receiver_group_name_final]').val($(this).attr('group_name'));
                                    $('input[name=receiver_group_designation_final]').val($(this).attr('group_designation'));
                                } else {
                                    $('input[name=receiver_office_id_final]').val($(this).attr('ofc_id'));
                                    $('input[name=receiver_office_name_final]').val($(this).attr('ofc_name'));
                                    $('input[name=receiver_officer_designation_id_final]').val($(this).attr('designation_id'));
                                    $('input[name=receiver_officer_designation_label_final]').val($(this).attr('designation'));
                                    $('input[name=receiver_office_unit_id_final]').val($(this).attr('unit_id'));
                                    $('input[name=receiver_office_unit_name_final]').val($(this).attr('unit_name'));
                                    $('input[name=receiver_officer_id_final]').val($(this).attr('officer_id'));
                                    $('input[name=receiver_officer_name_final]').val($(this).attr('officer_name'));
                                    $('input[name=receiver_officer_email_final]').val($(this).attr('officer_email'));
                                    $('input[name=receiver_office_head_final]').val($(this).attr('office_head'));
                                    $('input[name=receiver_visibleName_final]').val('');
                                    $('input[name=receiver_officer_mobile_final]').val('');
                                    $('input[name=receiver_sms_message_final]').val('');
                                }
                            });
                        } else {
                            var prevgroupId = '';
                            var prevgroupNm = '';
                            var prevgroupNmMem = '';
                            var prevgroupDes = '';
                            var prevOfficeId = '';
                            var prevOfficeNm = '';
                            var prevOfficeOrgId = '';
                            var prevOfficeOrgLb = '';
                            var prevOfficeUnitId = '';
                            var prevOfficeUnitLb = '';
                            var prevOfficerId = '';
                            var prevOfficerNm = '';
                            var prevOfficerEm = '';
                            var prevOfficerHd = '';
                            var prevOfficerVisibleName = '';
                            var prevOfficerMobile = '';
                            var prevOfficerSMSMessage = '';

                            $.each(receivers, function (i, data) {
                                if (typeof ($(this).attr('group_id')) != 'undefined') {
                                    $('input[name=receiver_group_id_final]').val(prevgroupId + ";" + $(this).attr('group_id'));
                                    prevgroupId = $('input[name=receiver_group_id_final]').val();
                                    $('input[name=receiver_group_name_final]').val(prevgroupNm + ";" + $(this).attr('group_name'));
                                    prevgroupNm = $('input[name=receiver_group_name_final]').val();
                                    $('input[name=receiver_group_designation_final]').val(prevgroupDes + ";" + $(this).attr('group_designation'));
                                    prevgroupDes = $('input[name=receiver_group_designation_final]').val();

                                    $('input[name=receiver_group_member_final]').val(prevgroupNmMem + ";" + $(this).attr('group_member'));
                                    prevgroupNmMem = $('input[name=receiver_group_member_final]').val();
                                }

                                $('input[name=receiver_office_id_final]').val(prevOfficeId + ";" + $(this).attr('ofc_id'));
                                prevOfficeId = $('input[name=receiver_office_id_final]').val();
                                $('input[name=receiver_office_name_final]').val(prevOfficeNm + ";" + $(this).attr('ofc_name'));
                                prevOfficeNm = $('input[name=receiver_office_name_final]').val();
                                $('input[name=receiver_officer_designation_id_final]').val(prevOfficeOrgId + ";" + $(this).attr('designation_id'));
                                prevOfficeOrgId = $('input[name=receiver_officer_designation_id_final]').val();
                                $('input[name=receiver_officer_designation_label_final]').val(prevOfficeOrgLb + ";" + $(this).attr('designation'));
                                prevOfficeOrgLb = $('input[name=receiver_officer_designation_label_final]').val();
                                $('input[name=receiver_office_unit_id_final]').val(prevOfficeUnitId + ";" + $(this).attr('unit_id'));
                                prevOfficeUnitId = $('input[name=receiver_office_unit_id_final]').val();
                                $('input[name=receiver_office_unit_name_final]').val(prevOfficeUnitLb + ";" + $(this).attr('unit_name'));
                                prevOfficeUnitLb = $('input[name=receiver_office_unit_name_final]').val();
                                $('input[name=receiver_officer_id_final]').val(prevOfficerId + ";" + $(this).attr('officer_id'));
                                prevOfficerId = $('input[name=receiver_officer_id_final]').val();
                                $('input[name=receiver_officer_name_final]').val(prevOfficerNm + ";" + $(this).attr('officer_name'));
                                prevOfficerNm = $('input[name=receiver_officer_name_final]').val();
                                $('input[name=receiver_officer_email_final]').val(prevOfficerEm + ";" + $(this).attr('officer_email'));
                                prevOfficerEm = $('input[name=receiver_officer_email_final]').val();
                                $('input[name=receiver_office_head_final]').val(prevOfficerHd + ";" + $(this).attr('office_head'));
                                prevOfficerHd = $('input[name=receiver_office_head_final]').val();
                                var visibleName = (!isEmpty($(this).attr('visibleName')))?$(this).attr('visibleName'):'';
                                var officer_mobile = (!isEmpty($(this).attr('officer_mobile')))?$(this).attr('officer_mobile'):'';
                                var sms_message = (!isEmpty($(this).attr('sms_message')))?$(this).attr('sms_message'):'';
                                $('input[name=receiver_visibleName_final]').val(prevOfficerVisibleName + ";" + visibleName);
                                prevOfficerVisibleName = $('input[name=receiver_visibleName_final]').val();
                                $('input[name=receiver_officer_mobile_final]').val(prevOfficerMobile + ";" + officer_mobile);
                                prevOfficerMobile = $('input[name=receiver_officer_mobile_final]').val();
                                $('input[name=receiver_sms_message_final]').val(prevOfficerSMSMessage + ";" + sms_message);
                                prevOfficerSMSMessage = $('input[name=receiver_sms_message_final]').val();
                            });
                        }
                    } else {
                        $('input[name=receiver_group_id_final]').val('');
                        $('input[name=receiver_group_name_final]').val('');
                        $('input[name=receiver_group_member_final]').val('');
                        $('input[name=receiver_group_designation_final]').val('');
                        $('input[name=receiver_office_id_final]').val('');
                        $('input[name=receiver_office_name_final]').val('');
                        $('input[name=receiver_officer_designation_id_final]').val('');
                        $('input[name=receiver_officer_designation_label_final]').val('');
                        $('input[name=receiver_office_unit_id_final]').val('');
                        $('input[name=receiver_office_unit_name_final]').val('');
                        $('input[name=receiver_officer_id_final]').val('');
                        $('input[name=receiver_officer_name_final]').val('');
                        $('input[name=receiver_officer_email_final]').val('');
                        $('input[name=receiver_office_head_final]').val('');
                        $('input[name=receiver_visibleName_final]').val('');
                        $('input[name=receiver_officer_mobile_final]').val('');
                        $('input[name=receiver_sms_message_final]').val('');
                        toastr.error("দুঃখিত! পত্রে প্রাপক দেয়া যাবে না!");
                    }
                }
            },
            setOnulipiForm: function(onulipis){
                if (onulipis.length == 0) {
                    $('input[name=onulipi_group_id_final]').val('');
                    $('input[name=onulipi_group_name_final]').val('');
                    $('input[name=onulipi_group_member_final]').val('');
                    $('input[name=onulipi_group_designation_final]').val('');
                    $('input[name=onulipi_office_id_final]').val('');
                    $('input[name=onulipi_office_name_final]').val('');
                    $('input[name=onulipi_officer_designation_id_final]').val('');
                    $('input[name=onulipi_officer_designation_label_final]').val('');
                    $('input[name=onulipi_office_unit_id_final]').val('');
                    $('input[name=onulipi_office_unit_name_final]').val('');
                    $('input[name=onulipi_officer_id_final]').val('');
                    $('input[name=onulipi_officer_name_final]').val('');
                    $('input[name=onulipi_officer_email_final]').val('');
                    $('input[name=onulipi_office_head_final]').val('');
                    $('input[name=onulipi_visibleName_final]').val('');
                    $('input[name=onulipi_officer_mobile_final]').val('');
                    $('input[name=onulipi_sms_message_final]').val('');
                }
                else {

                    if ($('#potrojariDraftForm').find('#cc_list_div').length > 0) {
                        var prevgroupId = '';
                        var prevgroupNm = '';
                        var prevgroupNmMem = '';
                        var prevgroupDes = '';
                        var prevOfficeId = '';
                        var prevOfficeNm = '';
                        var prevOfficeOrgId = '';
                        var prevOfficeOrgLb = '';
                        var prevOfficeUnitId = '';
                        var prevOfficeUnitLb = '';
                        var prevOfficerId = '';
                        var prevOfficerNm = '';
                        var prevOfficerEm = '';
                        var prevOfficerHd = '';
                        var prevOfficerVisibleName = '';
                        var prevOfficerMobile = '';
                        var prevOfficerSMSMessage = '';

                        var totalmember = $.map(onulipis,function(data){
                            return parseInt($(data).attr('group_member'));
                        }).reduce(function(total,num){return total+num; });

                        var bn = replaceNumbers("'" + totalmember + "'");
                        bn = bn.replace("'", '');
                        bn = bn.replace("'", '');

                        if($("#potro_language").val() == 'eng'){
                            $('#sharok_no2').text($('#sharok_no').text() + "/1" + (totalmember > 1 ? ("(" + totalmember + ")") : ""));
                        }else{
                            $('#sharok_no2').text($('#sharok_no').text() + "/" + replaceNumbers('1') + (totalmember > 1 ? ("(" + bn + ")") : ""));
                        }



                        $('#potrojariDraftForm').find('#cc_list_div').closest('.row').nextAll('.row').show()
                        $('#potrojariDraftForm').find('#cc_list_div').closest('.row').show();
                        $('#potrojariDraftForm').find('#sharok_no2').closest('.row').show();
                        $('#potrojariDraftForm').find('#sending_date_2').closest('.row').show();
                        $('#potrojariDraftForm').find('#sender_name2').closest('.row').show();
                        $('#potrojariDraftForm').find('#sender_designation2').closest('.row').show();
                        $('#potrojariDraftForm').find('#sender_signature2').closest('.row').show();

                        $.each(onulipis, function (i, data) {
                            if (typeof ($(this).attr('group_id')) != 'undefined') {
                                $('input[name=onulipi_group_id_final]').val(prevgroupId + ";" + $(this).attr('group_id'));
                                prevgroupId = $('input[name=onulipi_group_id_final]').val();

                                $('input[name=onulipi_group_name_final]').val(prevgroupNm + ";" + $(this).attr('group_name'));
                                prevgroupNm = $('input[name=onulipi_group_name_final]').val();

                                $('input[name=onulipi_group_designation_final]').val(prevgroupDes + ";" + $(this).attr('group_designation'));
                                prevgroupDes = $('input[name=onulipi_group_designation_final]').val();

                                $('input[name=onulipi_group_member_final]').val(prevgroupNmMem + ";" + $(this).attr('group_member'));
                                prevgroupNmMem = $('input[name=onulipi_group_member_final]').val();
                            }

                            $('input[name=onulipi_office_id_final]').val(prevOfficeId + ";" + $(this).attr('ofc_id'));
                            prevOfficeId = $('input[name=onulipi_office_id_final]').val();
                            $('input[name=onulipi_office_name_final]').val(prevOfficeNm + ";" + $(this).attr('ofc_name'));
                            prevOfficeNm = $('input[name=onulipi_office_name_final]').val();
                            $('input[name=onulipi_officer_designation_id_final]').val(prevOfficeOrgId + ";" + $(this).attr('designation_id'));
                            prevOfficeOrgId = $('input[name=onulipi_officer_designation_id_final]').val();
                            $('input[name=onulipi_officer_designation_label_final]').val(prevOfficeOrgLb + ";" + $(this).attr('designation'));
                            prevOfficeOrgLb = $('input[name=onulipi_officer_designation_label_final]').val();
                            $('input[name=onulipi_office_unit_id_final]').val(prevOfficeUnitId + ";" + $(this).attr('unit_id'));
                            prevOfficeUnitId = $('input[name=onulipi_office_unit_id_final]').val();
                            $('input[name=onulipi_office_unit_name_final]').val(prevOfficeUnitLb + ";" + $(this).attr('unit_name'));
                            prevOfficeUnitLb = $('input[name=onulipi_office_unit_name_final]').val();
                            $('input[name=onulipi_officer_id_final]').val(prevOfficerId + ";" + $(this).attr('officer_id'));
                            prevOfficerId = $('input[name=onulipi_officer_id_final]').val();
                            $('input[name=onulipi_officer_name_final]').val(prevOfficerNm + ";" + $(this).attr('officer_name'));
                            prevOfficerNm = $('input[name=onulipi_officer_name_final]').val();
                            $('input[name=onulipi_officer_email_final]').val(prevOfficerEm + ";" + $(this).attr('officer_email'));
                            prevOfficerEm = $('input[name=onulipi_officer_email_final]').val();
                            $('input[name=onulipi_office_head_final]').val(prevOfficerHd + ";" + $(this).attr('office_head'));
                            prevOfficerHd = $('input[name=onulipi_office_head_final]').val();
                            var visibleName = (!isEmpty($(this).attr('visibleName')))?$(this).attr('visibleName'):'';
                            var officer_mobile = (!isEmpty($(this).attr('officer_mobile')))?$(this).attr('officer_mobile'):'';
                            var sms_message = (!isEmpty($(this).attr('sms_message')))?$(this).attr('sms_message'):'';
                            $('input[name=onulipi_visibleName_final]').val(prevOfficerVisibleName + ";" + visibleName);
                            prevOfficerVisibleName = $('input[name=onulipi_visibleName_final]').val();
                            $('input[name=onulipi_officer_mobile_final]').val(prevOfficerMobile + ";" + officer_mobile);
                            prevOfficerMobile = $('input[name=onulipi_officer_mobile_final]').val();
                            $('input[name=onulipi_sms_message_final]').val(prevOfficerSMSMessage + ";" + sms_message);
                            prevOfficerSMSMessage = $('input[name=onulipi_sms_message_final]').val();
                        });

                    } else if ($('input[name=potro_type]').val() == 13) {
                        var prevgroupId = '';
                        var prevgroupNm = '';
                        var prevgroupNmMem = '';
                        var prevgroupDes = '';
                        var prevOfficeId = '';
                        var prevOfficeNm = '';
                        var prevOfficeOrgId = '';
                        var prevOfficeOrgLb = '';
                        var prevOfficeUnitId = '';
                        var prevOfficeUnitLb = '';
                        var prevOfficerId = '';
                        var prevOfficerNm = '';
                        var prevOfficerEm = '';
                        var prevOfficerHd = '';
                        var prevOfficerVisibleName = '';
                        var prevOfficerMobile = '';
                        var prevOfficerSMSMessage = '';

                        $.each(onulipis, function (i, data) {
                            if (typeof ($(this).attr('group_id')) != 'undefined') {
                                $('input[name=onulipi_group_id_final]').val(prevgroupId + ";" + $(this).attr('group_id'));
                                prevgroupId = $('input[name=onulipi_group_id_final]').val();
                                $('input[name=onulipi_group_name_final]').val(prevgroupNm + ";" + $(this).attr('group_name'));
                                prevgroupNm = $('input[name=onulipi_group_name_final]').val();
                                $('input[name=onulipi_group_designation_final]').val(prevgroupDes + ";" + $(this).attr('group_designation'));
                                prevgroupDes = $('input[name=onulipi_group_designation_final]').val();

                                $('input[name=onulipi_group_member_final]').val(prevgroupNmMem + ";" + $(this).attr('group_member'));
                                prevgroupNmMem = $('input[name=onulipi_group_member_final]').val();
                            }
                            $('input[name=onulipi_office_id_final]').val(prevOfficeId + ";" + $(this).attr('ofc_id'));
                            prevOfficeId = $('input[name=onulipi_office_id_final]').val();
                            $('input[name=onulipi_office_name_final]').val(prevOfficeNm + ";" + $(this).attr('ofc_name'));
                            prevOfficeNm = $('input[name=onulipi_office_name_final]').val();
                            $('input[name=onulipi_officer_designation_id_final]').val(prevOfficeOrgId + ";" + $(this).attr('designation_id'));
                            prevOfficeOrgId = $('input[name=onulipi_officer_designation_id_final]').val();
                            $('input[name=onulipi_officer_designation_label_final]').val(prevOfficeOrgLb + ";" + $(this).attr('designation'));
                            prevOfficeOrgLb = $('input[name=onulipi_officer_designation_label_final]').val();
                            $('input[name=onulipi_office_unit_id_final]').val(prevOfficeUnitId + ";" + $(this).attr('unit_id'));
                            prevOfficeUnitId = $('input[name=onulipi_office_unit_id_final]').val();
                            $('input[name=onulipi_office_unit_name_final]').val(prevOfficeUnitLb + ";" + $(this).attr('unit_name'));
                            prevOfficeUnitLb = $('input[name=onulipi_office_unit_name_final]').val();
                            $('input[name=onulipi_officer_id_final]').val(prevOfficerId + ";" + $(this).attr('officer_id'));
                            prevOfficerId = $('input[name=onulipi_officer_id_final]').val();
                            $('input[name=onulipi_officer_name_final]').val(prevOfficerNm + ";" + $(this).attr('officer_name'));
                            prevOfficerNm = $('input[name=onulipi_officer_name_final]').val();
                            $('input[name=onulipi_officer_email_final]').val(prevOfficerEm + ";" + $(this).attr('officer_email'));
                            prevOfficerEm = $('input[name=onulipi_officer_email_final]').val();
                            $('input[name=onulipi_office_head_final]').val(prevOfficerHd + ";" + $(this).attr('office_head'));
                            prevOfficerHd = $('input[name=onulipi_office_head_final]').val();
                            var visibleName = (!isEmpty($(this).attr('visibleName')))?$(this).attr('visibleName'):'';
                            var officer_mobile = (!isEmpty($(this).attr('officer_mobile')))?$(this).attr('officer_mobile'):'';
                            var sms_message = (!isEmpty($(this).attr('sms_message')))?$(this).attr('sms_message'):'';
                            $('input[name=onulipi_visibleName_final]').val(prevOfficerVisibleName + ";" + visibleName);
                            prevOfficerVisibleName = $('input[name=onulipi_visibleName_final]').val();
                            $('input[name=onulipi_officer_mobile_final]').val(prevOfficerMobile + ";" + officer_mobile);
                            prevOfficerMobile = $('input[name=onulipi_officer_mobile_final]').val();
                            $('input[name=onulipi_sms_message_final]').val(prevOfficerSMSMessage + ";" + sms_message);
                            prevOfficerSMSMessage = $('input[name=onulipi_sms_message_final]').val();
                        });
                    } else {
                        $('input[name=onulipi_group_id_final]').val('');
                        $('input[name=onulipi_group_name_final]').val('');
                        $('input[name=onulipi_group_member_final]').val('');
                        $('input[name=onulipi_group_designation_final]').val('');
                        $('input[name=onulipi_office_id_final]').val('');
                        $('input[name=onulipi_office_name_final]').val('');
                        $('input[name=onulipi_officer_designation_id_final]').val('');
                        $('input[name=onulipi_officer_designation_label_final]').val('');
                        $('input[name=onulipi_office_unit_id_final]').val('');
                        $('input[name=onulipi_office_unit_name_final]').val('');
                        $('input[name=onulipi_officer_id_final]').val('');
                        $('input[name=onulipi_officer_name_final]').val('');
                        $('input[name=onulipi_officer_email_final]').val('');
                        $('input[name=onulipi_office_head_final]').val('');
                        $('input[name=onulipi_visibleName_final]').val('');
                        $('input[name=onulipi_officer_mobile_final]').val('');
                        $('input[name=onulipi_sms_message_final]').val('');
                        $('#cc_list_div').html("");
                        toastr.error("দুঃখিত! পত্রে অনুলিপি দেয়া যাবে না!");
                    }
                }
            },
            goForDraftSave: function(){
                if ($('[name=potro_type]').val() == 13 || ($('#potrojariDraftForm').find('#sender_signature img').attr('src') != '' && typeof($('#potrojariDraftForm').find('#sender_signature img').attr('src')) != 'undefined')) {
                    if(signature > 0){
                        //soft_token_checker
                        $('#potrojariDraftForm').find("input[name=soft_token]").val($("#soft_token").val());
                    }
                    if ($('#approve').length > 0 && $('#approve').is(':checked')) {
                        $("#potrojariDraftForm").find('[name=can_potrojari]').val(1);
                        Metronic.blockUI({
                            target: '.page-container'
                        });
                        var url = ((!isEmpty(signature) && signature > 0)?ds_url:js_wb_root) +
                            '<?php
                            echo ('Potrojari/apiPotrojariDraftUpdate/'.$draftVersion->id.'/'.$nothi_office.'/'.$employee_office['office_unit_organogram_id']) ;
                            ?>';
                        $.ajax({
                            url: url,
                            data: $("#potrojariDraftForm").serialize(),
                            method: "post",
                            dataType: 'JSON',
                            cache: false,
                            success: function (response) {
                                if (response.status == 'error') {
//                                    {timeOut: 5000}
                                    toastr.error(response.msg);
                                    Metronic.unblockUI('.page-container');
                                } else {
                                    toastr.success("খসড়া সংশোধিত হয়েছে");
                                    Metronic.unblockUI('.page-container');

                                    setTimeout(function () {
                                        $("#refresh_me").submit();
                                    }, 1000);
                                }
                            },
                            error: function (xhr, status, errorThrown) {
                                Metronic.unblockUI('.page-container');
                            }
                        });
                    }
                    else {
                        $("#potrojariDraftForm").find('[name=can_potrojari]').val(0);
                        bootbox.dialog({
                            message: "<?php
                                if ($draftVersion['potro_type'] != 17 && $employee_office['office_unit_organogram_id']
                                    == $draftVersion['officer_designation_id']
                                ) {
                                    echo "আপনি কি অনুমোদন ব্যতীত সংরক্ষণ করতে ইচ্ছুক?";
                                } else if ($draftVersion['potro_type'] == 17
                                    && $employee_office['office_unit_organogram_id']
                                    == $draftVersion['sovapoti_officer_designation_id']
                                ) {
                                    echo "আপনি কি অনুমোদন ব্যতীত সংরক্ষণ করতে ইচ্ছুক?";
                                } else {
                                    echo "আপনি কি সংরক্ষণ করতে ইচ্ছুক?";
                                } ?>",
                            title: "সংরক্ষণ ",
                            buttons: {
                                success: {
                                    label: "হ্যাঁ",
                                    className: "green",
                                    callback: function () {
                                        Metronic.blockUI({
                                            target: '.page-container'
                                        });
                                        var url = ((!isEmpty(signature) && signature > 0)?ds_url:js_wb_root) +
                                            '<?php
                                            echo ('Potrojari/apiPotrojariDraftUpdate/'.$draftVersion->id.'/'.$nothi_office.'/'.$employee_office['office_unit_organogram_id']) ;
                                            ?>';
                                        $.ajax({
                                            url: '<?php echo $this->Url->build(['controller' => 'Potrojari', 'action' => 'apiPotrojariDraftUpdate', $draftVersion->id, $nothi_office,$employee_office['office_unit_organogram_id']]);?>',
                                            data: $("#potrojariDraftForm").serialize(),
                                            method: "post",
                                            dataType: 'JSON',
                                            cache: false,
                                            success: function (response) {
                                                if (response.status == 'error') {
                                                    toastr.error(response.msg);
                                                    Metronic.unblockUI('.page-container');
                                                } else {
                                                    toastr.success("খসড়া সংশোধিত হয়েছে");
                                                    Metronic.unblockUI('.page-container');

                                                    setTimeout(function () {
                                                        $("#refresh_me").submit();
                                                    }, 1000);
                                                }
                                            },
                                            error: function (xhr, status, errorThrown) {
                                                Metronic.unblockUI('.page-container');
                                            }
                                        });
                                    }
                                },
                                danger: {
                                    label: "না",
                                    className: "red",
                                    callback: function () {
                                        return true;
                                    }
                                }
                            }
                        }).find('.modal-dialog').css('top', $('.saveDraftNothi').offset().top - 210);

                    }
                } else {
                    Metronic.unblockUI('.page-container');
                    toastr.error("দুঃখিত! স্বাক্ষর লোড হয়নি। পুনরায় সংশোধন করে সংরক্ষণ করুন।");
                }
            },
            goForDraftSaveEditorView: function(){
                if ($('[name=potro_type]').val() == 13 || ($('#potrojariDraftForm').find('#sender_signature img').attr('src') != '' && typeof($('#potrojariDraftForm').find('#sender_signature img').attr('src')) != 'undefined')) {

                        $("#potrojariDraftForm").find('[name=can_potrojari]').val(0);

                        Metronic.blockUI({
                            target: '.page-container'
                        });
                        $.ajax({
                            url: '<?php echo $this->Url->build(['controller' => 'Potrojari', 'action' => 'apiPotrojariDraftUpdate', $draftVersion->id, $nothi_office,$employee_office['office_unit_organogram_id']]);?>',
                            data: $("#potrojariDraftForm").serialize(),
                            method: "post",
                            dataType: 'JSON',
                            cache: false,
                            success: function (response) {
                                if (response.status == 'error') {
                                    toastr.error(response.msg);
                                    Metronic.unblockUI('.page-container');
                                } else {
                                    // toastr.success("খসড়া সংরক্ষণ করা হয়েছে");
                                    $("#noteViewData").val($("#noteView").html());
                                    $("#apiPotroDraftViewForm").submit();
                                    Metronic.unblockUI('.page-container');
                                    return;
                                }
                            },
                            error: function (xhr, status, errorThrown) {
                                Metronic.unblockUI('.page-container');
                            }
                        });
                        <?php if($device_type == 'android'){ ?>
                            $(".abc .modal-dialog").css('top', $('#noteView').offset().top - 50);
                        <?php } ?>

                } else {
                    Metronic.unblockUI('.page-container');
                    toastr.error("দুঃখিত! স্বাক্ষর লোড হয়নি। পুনরায় সংশোধন করে সংরক্ষণ করুন।");
                }
            }
        };

        $(function () {
            $(document).on('click', '.mega-menu-dropdown .dropdown-menu', function (e) {
                e.stopPropagation();
            });

            $('select').select2('destroy');

			NothiMasterMovement.init('nothing');
            DakSetup.init();
            PotroJariFileUpload.init();

            var OfficeDataAdapter = new Bloodhound({
                datumTokenizer: function (d) {
                    return d.tokens;
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY&office_id=<?php echo $office_id; ?>',
                limit: 50,
            });
            OfficeDataAdapter.initialize();

            var OfficeDataAdapterRc = new Bloodhound({
                datumTokenizer: function (d) {
                    return d.tokens;
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY',
                limit: 50,
            });
            OfficeDataAdapterRc.initialize();

            var GroupDataAdapter = new Bloodhound({
                datumTokenizer: function (d) {
                    return d.tokens;
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: getBaseUrl() + 'PotrojariGroups/ajaxGetGroupList/<?php echo $office_id; ?>?search_key=%QUERY'
            });
            GroupDataAdapter.initialize();

            $('.typeahead_receiver_group')
                .typeahead({hint: true}, {
                        name: 'datypeahead_receiver_group',
                        displayKey: 'value',
                        limit: 20,
                        sufficient: 20,
                        source: GroupDataAdapter.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media" onclick="setGroupInfo({{id}},{{list}},\'receiver_\')">',
                                '<div class="media-body" >',
                                '<h4 style="font-size:12px;" class="media-heading" >{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', ongroupOpened)
                .on('typeahead:selected', ongroupAutocompleted)
                .on('typeahead:autocompleted', ongroupSelected);

            $('.typeahead_onulipi_group')
                .typeahead({hint: true}, {
                        name: 'datypeahead_onulipi_group',
                        displayKey: 'value',
                        limit: 20,
                        sufficient: 20,
                        source: GroupDataAdapter.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media"  onclick="setGroupInfo({{id}},{{list}},\'onulipi_\')">',
                                '<div class="media-body">',
                                '<h4 style="font-size:12px;" class="media-heading"  >{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', ongroupOpened)
                .on('typeahead:selected', ongroupAutocompleted)
                .on('typeahead:autocompleted', ongroupSelected);

            function ongroupOpened($e, datum) {

            }

            function ongroupSelected($e, datum) {

            }

            function ongroupAutocompleted($e, datum) {
            }

            $('.typeahead_sender')
                .typeahead(null, {
                        name: 'datypeahead_sender',
                        limit: 20,
                        sufficient: 20,
                        displayKey: 'value',
                        source: OfficeDataAdapter.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media">',
                                '<div class="media-body">',
                                '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);

            $('.typeahead_approval')
                .typeahead(null, {
                        name: 'datypeahead_approval',
                        limit: 20,
                        sufficient: 20,
                        displayKey: 'value',
                        source: OfficeDataAdapter.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media">',
                                '<div class="media-body">',
                                '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);

            $('.typeahead_receiver')
                .typeahead(null, {
                        name: 'datypeahead_receiver',
                        limit: 20,
                        sufficient: 20,
                        displayKey: 'value',
                        source: OfficeDataAdapterRc.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media">',
                                '<div class="media-body">',
                                '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);

            $('.typeahead_sovapoti')
                .typeahead(null, {
                        name: 'datypeahead_sovapoti',
                        limit: 20,
                        sufficient: 20,
                        displayKey: 'value',
                        source: OfficeDataAdapterRc.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media">',
                                '<div class="media-body">',
                                '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);

            $('.typeahead_onulipi')
                .typeahead(null, {
                        name: 'datypeahead_onulipi',
                        limit: 20,
                        sufficient: 20,
                        displayKey: 'value',
                        source: OfficeDataAdapterRc.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media">',
                                '<div class="media-body">',
                                '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);
            $('.typeahead_attension').typeahead(null, {
                    name: 'datypeahead_attension',
                    displayKey: 'value',
                    limit: 20,
                    sufficient: 20,
                    source: OfficeDataAdapterRc.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
            )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);

            function onOpened($e, datum) {

            }

            function onSelected($e, datum) {

            }

            function onAutocompleted($e, datum) {
                DakOffice.setAutoDataIntoHiddenFields(datum, $e.currentTarget.id);
            }

            var list_rec = document.getElementById("receiver_ul");
            Sortable.create(list_rec, {
                animation: 150,
                onUpdate: function (evt) {
                    setInformation();
                }
            });

            var list_onu = document.getElementById("onulipi_ul");
            Sortable.create(list_onu, {
                animation: 150,
                onUpdate: function (evt) {
                    setInformation();
                }
            });
        });


        $(function () {
            $('#pencil').remove();
            $.each($('span.canedit'), function (i, v) {
                var id = $(this).attr('id');
                var txt = $(this).text();
                var dataType = $(this).attr('data-type');

                if (id == 'sending_date') {
                    $(this).replaceWith('<a data-placement="right"  data-pk="1" data-viewformat="dd.mm.yyyy" data-type="date" id="sending_date" href="#" class="editable editable-click">' + txt + '</a>');
                } else if (id == 'cc_div_item') {
                    $(this).replaceWith("<a data-type='text' data-pk='1' class='editable editable-click cc_list' href='javascript:;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</a>");
                }else if (id == 'to_div_item') {
                    $(this).replaceWith("<a data-type='text' data-pk='1' class='editable editable-click to_list' href='javascript:;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</a>");
                } else if (dataType == 'textarea') {
                    $(this).replaceWith("<a data-type='textarea' data-pk='1' class='editable editable-click' href='javascript:;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + $(this).html() + "</a>");
                }else {
                    $(this).replaceWith("<a data-type='text' data-pk='1' class='editable editable-click' href='javascript:;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</a>");
                }
            });
            $('#note').before('<a id="pencil" href="javascript;"> <i class="fs1 a2i_gn_edit2"></i>[সম্পাদন  করুন] <br/></a>');

            $('.sovabox').hide();
            $('.sender_list').show();
            if ($('input[name=potro_type]').val() == 13) {
                $('.khalipotro').show();
                $('#blank-sarok').val('<?php echo $nothiMasterInfo['nothi_no'] ?>-<?php echo $totalPotrojari; ?>');
                $('#blank-subject').val("<?php echo htmlspecialchars(trim($draftVersion->potro_subject)); ?>");

                var para1 = $("#para1Data").html();
                var para2 = $("#para2Data").html();
                var para3 = $("#para3Data").html();
                var para4 = $("#para4Data").html();
                var para5 = $("#para5Data").html();
                $("#note").html(para1.replace(/<br\s*[\/]?>/gi, '\n'));
                $("#para1").html(para2.replace(/<br\s*[\/]?>/gi, '\n'));
                $("#para2").html(para3.replace(/<br\s*[\/]?>/gi, '\n'));
                $("#para3").html(para4.replace(/<br\s*[\/]?>/gi, '\n'));
                $("#para4").html(para5.replace(/<br\s*[\/]?>/gi, '\n'));

                $('#tmpbody').remove();
            } else if ($('input[name=potro_type]').val() == 17) {
                $('.sovabox').show();
                $('.sender_list').hide();
                $('.receiver_list').hide();
                $('.attension_list').hide();
            } else if ($('input[name=potro_type]').val() == 19) {
                $('.attentionbox').hide();
            }
            else if ($('input[name=potro_type]').val() == 9) {
                //$('.sender_list').hide();
            }else if($('input[name=potro_type]').val() == 22){
                        $('.approval_list').hide();
                        $('.onulipi_list').hide();
                        $('.attension_list').hide();
                        $('.sender_list').find('a').closest('.potrojariOptions').html('এন.ও.সি প্রদানকারী <i class="glyphicon glyphicon-chevron-down pull-right"></i>');
            }
            else if($('#potro-type').val() == 31){
                $('.attension_list').hide();
            }
            else {
                $('.khalipotro').hide();
            }
            PotrojariFormEditable.init(<?php echo $heading['potrojari_head'] ?>, '<?php echo $nothiMasterInfo['nothi_no'] ?>', "<?php
                echo isset($potroInfo['sarok_no']) ? htmlspecialchars(trim($potroInfo['sarok_no']))
                    : ''
                ?>", <?php echo json_encode($employee_office) ?>, "<?php echo htmlspecialchars(trim($draftVersion->potro_subject)); ?>", '', '<?php echo $totalPotrojari; ?>','<?=$draftVersion->potrojari_language?>');
        });

        $(document).on('click', '.remove_list', function () {
            $(this).closest('li').remove();
            setInformation();
        });

        $(document).on('change', '#potro-priority-level', setInformation);
        $(document).on('change', '#potro-security-level', setInformation);

    </script>

    <!-- End: JavaScript -->
    <!-- The blueimp Gallery widget -->

    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <script id="template-upload2" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload fade">
        <td>
        <span class="preview"></span>
        </td>
        <td>
        <p class="name">{%=file.name%}</p>
        <strong class="error label label-danger"></strong>
        </td>
        <td>
        <p class="size">প্রক্রিয়াকরন চলছে...</p>
        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
        </div>
        </td>
        <td>
        {% if (!i && !o.options.autoUpload) { %}
        <button class="btn blue start" disabled>
        <i class="fa fa-upload"></i>
        <span>আপলোড করুন</span>
        </button>
        {% } %}
        {% if (!i) { %}
        <button class="btn red cancel">
        <i class="fa fa-ban"></i>
        <span>বাতিল করুন</span>
        </button>
        {% } %}
        </td>
        </tr>
        {% } %}





    </script>
    <!-- The template to display files available for download -->
    <script id="template-download2" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade">
         <td width="20%">
        <span class="preview">
        {% if (file.thumbnailUrl) { %}
        <a href="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="<?= FILE_FOLDER ?>{%=file.thumbnailUrl%}"></a>
        {% } %}
        </span>
        </td>
        <td width="25%">
        <p class="name">
        {% if (file.url) { %}
        <a href="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
        {% } else { %}
        <span>{%=file.name%}</span>
        {% } %}
        </p>
        {% if (file.error) { %}
        <div><span class="label label-danger">ত্রুটি:</span> {%=file.error%}</div>
        {% } %}
        </td>
        <td width="10%">
        <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td width="40%" style ="min-width: 100px!important;">

        <input type="text" title="{%=file.name%}" class="form-control potro-attachment-input" image="{%=file.url%}" file-type="{%=file.type%}" onkeyup="check_attachment()" placeholder="সংযুক্তির নাম">

    </td>
    <td width="5%">
        {% if (file.deleteUrl) { %}
        <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
        <i class="fs1 a2i_gn_delete2"></i>
        <span>মুছে ফেলুন</span>
        </button>

        {% } else { %}
        <button class="btn yellow cancel btn-sm">
        <i class="fa fa-ban"></i>
        <span>বাতিল করুন</span>
        </button>
        {% } %}
        </td>
        </tr>
        {% } %}
    </script>
    <script>
        $(document).on('click', '.saveDraftNothi', function () {
            if (DRAFT_FORM.setform() != false) {
                if(!isEmpty(signature) && signature > 0 && $('#approve').length > 0 && $('#approve').is(':checked')){
                    getSignatureToken('goForDraftSave');
                }else{
                    DRAFT_FORM.goForDraftSave();
                }
            }
        });

        $(document).ready(function () {
            $("#approve").on('click', function () {
                if ($('#approve').is(':checked')) {
                    bootbox.dialog({
                        message: "আপনি কি অনুমোদন ও সংরক্ষণ করতে ইচ্ছুক?",
                        title: "অনুমোদন ও সংরক্ষণ ",
                        buttons: {
                            success: {
                                label: "হ্যাঁ",
                                className: "green",
                                callback: function () {
                                    setTimeout(function () {
                                        $('.saveDraftNothi').trigger('click');
                                    }, 1000);
                                }
                            },
                            danger: {
                                label: "না",
                                className: "red",
                                callback: function () {
                                    return true;
                                }
                            }
                        }
                    }).find('.modal-dialog').css('top', $('#approve').offset().top - 210);
                }
            });
        });

        $(document).ready(function () {
			var potro_type = $('[name=potro_type]').val();
			if(potro_type == 4){
				$('.onulipi_list').hide();
				$('.attentionbox').hide();
			} else if(potro_type == 5){
				$('.receiver_list').hide();
				$('.attentionbox').hide();
			}
			else if(potro_type == 9){
				$('.attentionbox').hide();
			}else if(potro_type == 10){
				$('.receiver_list').hide();
				$('.attentionbox').hide();
			} else if(potro_type == 11 || potro_type == 24){
				$('.receiver_list').hide();
				$('.approval_list').hide();
			} else if(potro_type == 12){
				$('.receiver_list').hide();
			} else if($('#potro-type').val() == 20){
				$('.approval_list').hide();
				$('.attension_list').hide();
				$('.sender_list').find('a').closest('.potrojariOptions').html('প্রেরক <i class="glyphicon glyphicon-chevron-down pull-right"></i>');
				$("a[href=#tab_other_potro]").text("সংলগ্নী ");
			} else if($('#potro-type').val() == 22){
				$('.approval_list').hide();
				$('.onulipi_list').hide();
				$('.attension_list').hide();
				$('.sender_list').find('a').closest('.potrojariOptions').html('এন.ও.সি প্রদানকারী <i class="glyphicon glyphicon-chevron-down pull-right"></i>');
			}
        $('#prapto-potro').multiSelect({
            dblClick: true, afterSelect: function (values) {
                var title = "";
                var id = values;
                $('#responsiveOnuccedModal').find('.modal-title').text('');
                $('#responsiveOnuccedModal').find('.modal-body').html('');
                PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + id + '/potro/' + <?= $draftVersion->nothi_master_id ?>+'?nothi_part='+'<?= $nothimasterid ?>'+'&token='+'<?= sGenerateToken(['file' => $nothimasterid, 'office_id'=>$employee_office['office_id'], 'office_unit_id'=>$employee_office['office_unit_id'], 'office_unit_organogram_id'=>$employee_office['office_unit_organogram_id']], ['exp' => time() + 60 * 300]) ?>' , {'nothi_office':<?php echo $nothi_office ?>}, 'html', function (response) {
                    $('#responsiveOnuccedModal').modal('show');
                    $('#responsiveOnuccedModal').find('.modal-title').text(title);
                    $('#responsiveOnuccedModal').find('.modal-body').html(response);
                });
            }
        });

    });

        $(document).on('click', '.showforPopup', function (e) {
            e.preventDefault();
            var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
            getPopUpPotro($(this).attr('href'), title);
        })

        function getPopUpPotro(href, title) {
            $('#responsiveOnuccedModal').find('.modal-title').text('');
            $('#responsiveOnuccedModal').find('.modal-body').html('');
            PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + href+'?nothi_part='+'<?= $nothimasterid ?>'+'&token='+'<?= sGenerateToken(['file' => $nothimasterid, 'office_id'=>$employee_office['office_id'], 'office_unit_id'=>$employee_office['office_unit_id'], 'office_unit_organogram_id'=>$employee_office['office_unit_organogram_id']], ['exp' => time() + 60 * 300]) ?>', {'nothi_office':<?php echo $nothi_office ?>}, 'html', function (response) {
                $('#responsiveOnuccedModal').modal('show');
                $('#responsiveOnuccedModal').find('.modal-title').text(title);

                $('#responsiveOnuccedModal').find('.modal-body').html(response);
            });
        }

        function getSignatureToken(functionName,el1,el2,el3){
            if(signature > 0)
            {
                $("#soft_token").focus();
                $("#soft_token").val(0);
                var promise = function () {
                    return new Promise(function (resolve, reject) {
                        if (signature == 1) {
                            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'employeeRecords/checkUserSoftToken',{'api_key' : '<?= $apikey ?>','employee_record_id' : '<?=$employee_office['officer_id'] ?>'},'json',function(result){
                                if(!isEmpty(result) && !isEmpty(result.status)){
                                    if(result.status == 'success'){
                                        var str = '<div class="col-md-12 row"><div class="col-md-2 col-sm-2"><label class="control-label pull-right">সফট টোকেনঃ </label></div><div class="col-md-8 col-sm-8"><input type="password" class="form-control" id="soft_token" readonly></div></div><div class="col-md-12 row text-center font-green">'+(!isEmpty(result.message)?(result.message):'') +'</div>';
                                        setTimeout(function(){
                                            $("#soft_token").val((!isEmpty(result.sign_token)?result.sign_token:''));
                                        },1000);
                                        resolve(str);
                                    }else{
                                        var str ='<div class="col-md-12 col-sm-12 col-xs-12"><label class="control-label">নিম্নে সফট টোকেনটি সরবরাহ করুন </label></div><div class="col-md-12 col-sm-12  col-xs-12"><input type="password" class="form-control" id="soft_token" ></div><br><br><hr><br><div class="col-md-12 row">যদি সফট টোকেন দিয়ে সাইন সম্ভব না হয় তবে মোবাইল অ্যাপ এ হোমপেজ থেকে  <b>স্বাক্ষর ধরন</b> পরিবর্তন করে নিন। </div>';
                                        resolve(str);
                                    }
                                }else{
                                    reject('Something went wrong.Code 1');
                                }
                            });

                        } else {
                            var str ='<div class="col-md-12 col-sm-12  col-xs-12"><b>হার্ড টোকেন</b> নির্বাচন করায় প্রতি স্বাক্ষরের সময় আপনাকে ডিজিটাল <b>হার্ড টোকেন</b> এর সহযোগিতা নিতে হবে। যদি হার্ড টোকেন দিয়ে সাইন সম্ভব না হয় তবে মোবাইল অ্যাপ এ হোমপেজ থেকে  <b>স্বাক্ষর ধরন</b>  পরিবর্তন করে নিন। নিরাপত্তা নিশ্চিতকরণের জন্য দুইবার টোকেন চাওয়া হতে পারে। <input type="hidden" class="form-control" id="soft_token">  </div>';
                            resolve(str);
                        }
                    });
                };

                promise().then(function (str) {
                    bootbox.dialog({
                        message: str,
                        title: 'ডিজিটাল সিগনেচার',
                        buttons: {
                            success: {
                                label: ((functionName == 'forwardNothi')?'প্রেরণ':(functionName == 'potroApproval')?'খসড়া অনুমোদন':(functionName == 'makePotrojariRequest')?'পত্রজারি':(functionName == 'goForDraftSave')?'খসড়া সংশোধন':(functionName == 'NoteNisponno')?' নোট নিষ্পন্ন':(functionName == 'apiMakePotrojariRequest')?'পত্রজারি':(functionName == 'apiPotroApporval')?'খসড়া অনুমোদন':''),
                                className: "green",
                                callback: function () {
                                    if(functionName == 'forwardNothi'){
                                        NothiMasterMovement.forwardNothiFunction(el1);
                                    }
                                    else if(functionName == 'potroApproval'){
                                        potroApproval(el1,el2);
                                    }
                                    else if(functionName == 'makePotrojariRequest'){
                                        makePotrojariRequest(el1,el2);
                                    }
                                    else if(functionName == 'goForDraftSave'){
                                        DRAFT_FORM.goForDraftSave();
                                    }
                                    else if(functionName == 'NoteNisponno'){
                                        NoteNisponno(el1);
                                    }
                                    else if(functionName == 'apiMakePotrojariRequest'){
                                        apiMakePotrojariRequest(el1,el2,el3);
                                    }
                                    else if(functionName == 'apiPotroApporval'){
                                        apiPotroApporval(el1,el2);
                                    }
                                }
                            },
                            proceedWithOutSignature: {
                                label: 'ডিজিটাল সাইন ব্যতিত '+((functionName == 'forwardNothi')?'প্রেরণ':(functionName == 'potroApproval')?'খসড়া অনুমোদন':(functionName == 'makePotrojariRequest')?'পত্রজারি':(functionName == 'goForDraftSave')?'খসড়া সংশোধন':(functionName == 'NoteNisponno')?' নোট নিষ্পন্ন':(functionName == 'apiMakePotrojariRequest')?'পত্রজারি':(functionName == 'apiPotroApporval')?'খসড়া অনুমোদন':''),
                                className: "blue",
                                callback: function () {
                                    $("#soft_token").val('-1');
                                    if(functionName == 'forwardNothi'){
                                        NothiMasterMovement.forwardNothiFunction(el1);
                                    }
                                    else if(functionName == 'potroApproval'){
                                        potroApproval(el1,el2);
                                    }
                                    else if(functionName == 'makePotrojariRequest'){
                                        makePotrojariRequest(el1,el2);
                                    }
                                    else if(functionName == 'goForDraftSave'){
                                        DRAFT_FORM.goForDraftSave();
                                    }
                                    else if(functionName == 'NoteNisponno'){
                                        NoteNisponno(el1);
                                    }
                                    else if(functionName == 'apiMakePotrojariRequest'){
                                        apiMakePotrojariRequest(el1,el2,el3);
                                    }
                                    else if(functionName == 'apiPotroApporval'){
                                        apiPotroApporval(el1,el2);
                                    }
                                }
                            },
                            danger: {
                                label: "বন্ধ করুন",
                                className: "red",
                                callback: function () {
                                    if(functionName == 'potroApproval'){
                                        if ($(".approveDraftNothi").is(':checked') == false) {
                                            $(".approveDraftNothi").attr('checked', 'checked');
                                            $(".approveDraftNothi").closest('span').addClass('checked');
                                        }
                                        else {
                                            $(".approveDraftNothi").removeAttr('checked');
                                            $(".approveDraftNothi").closest('span').removeClass('checked');
                                        }
                                    }
                                    Metronic.unblockUI('.page-container');
                                    Metronic.unblockUI('#ajax-content');
                                }
                            }
                        }
                    });
                    return;
                }).catch (function (error) {
                    console.log('Error: ', error);
                });

            }else{
                toastr.error('দুঃখিতঃ কোন ফাংশন নির্বাচন করা হয়নি।');
            }
        }
</script>
<?php endif; ?>