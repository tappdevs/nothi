<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>projapoti-nothi/css/styles.css"/>

<style>
    .btn-changelog, .btn-forward-nothi, .btn-nothiback, .btn-print {
        padding: 3px 5px !important;
    }

    .btn-icon-only {

    }

    .editable {
        border: none !important;
        word-break:break-all;
        word-wrap:break-word
    }

    #sovapoti_signature, #sender_signature, #sender_signature2, #sender_signature3 {
        visibility: hidden;
    }

    #sovapoti_signature_date, #sender_signature_date, #sender_signature2_date, #sender_signature3_date {
        visibility: hidden;
    }

    #note {
        overflow: hidden;
        word-break: break-all;
        word-wrap: break-word;
        height: 100%;
    }

    .ms-container {
        width: 100% !important;
    }

    .bangladate {
        border-bottom: 1px solid #000 !important;
    }

    .editable-click, a.editable-click {
        border: none;
        word-break:break-all;
        word-wrap:break-word
    }

    .cc_list {
        white-space: pre-wrap;
    }

    .to_list {
        white-space: pre-wrap;
    }

    .popover-content {
        padding: 10px 30px;
    }

    .mega-menu-dropdown > .dropdown-menu {
        top: 10px !important;
    }

</style>

<?php if ($privilige_type == 1): ?>
    <div class="row">
        <div class="<?php
        echo 'col-lg-12 col-md-12 col-sm-12 col-xs-12'
        ?>">

            <div class="portlet box green-seagreen">
                <div class="portlet-title">
                    <div class="caption">
                         <?php echo " নথি নম্বর: " . $nothiPartInformation['nothi_no'] . '; বিষয়: ' . $nothiPartInformation['subject']; ?>
                    </div>

                    <div class="actions">
                        <a title="নথিতে ফেরত যান" href="<?php
                        echo $this->Url->build(['_name' => 'noteDetail', $nothiPartInformation['id'], $nothi_office])
                        ?>" class="  btn btn-danger  btn-nothiback">
                            <i class="fa fa-list"></i> নথিতে ফেরত যান
                        </a>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="row ">
                        <div class="col-md-12 " style="margin: 0 auto;">
                            <?php
                                echo $this->Form->create($contentInformation,
                                    array( 'id' => 'potrojariDraftForm'));
                                ?>
                                <?php
                                echo $this->Form->hidden('id');

                                ?>
                              <div id="template-body"
                                 style="border:1px solid #aaaaaa; background-color: #fff; max-width:950px; min-height:815px; height: auto; margin:0 auto; padding: 2px;">
                                <?php
                                echo $contentInformation->potro_description;
                                ?>
                            </div>
                            <?= $this->Form->end(); ?>
                        </div>
                        <div class="form-actions text-center">
                            <br>
                            <button class="btn btn-success" onclick="savePotrojari()"><?= SAVE ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php  endif;?>
    <link rel="stylesheet" type="text/css"
          href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>
    <script type="text/javascript"
            src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery.mockjax.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/form-editable.js"></script>
    <script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/custom_potrojari_edit.js?v=<?= time() ?>" type="text/javascript"></script>
    <script>
        $(function(){
             editInfo('<?= $contentInformation['potrojari_language'] ?>');

        });
    </script>