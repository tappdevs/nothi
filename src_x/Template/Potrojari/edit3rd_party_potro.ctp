<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>projapoti-nothi/css/styles.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"
      type="text/javascript">
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-multi-select/css/multi-select.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<style>
    .btn-changelog, .btn-forward-nothi, .btn-nothiback, .btn-print {
        padding: 3px 5px !important;
    }

    .editable {
        border: none !important;
        word-break: break-all;
        word-wrap: break-word
    }

    .bangladate {
        border-bottom: 1px solid #000 !important;
    }

    .editable-click, a.editable-click {
        border: none;
        word-break: break-all;
        word-wrap: break-word
    }

    .cc_list {
        white-space: pre-wrap;
    }

    .to_list {
        white-space: pre-wrap;
    }

    .A4-max {
        background: white;
        max-width: 21cm;
        min-height: 29.7cm;
        display: block;
        margin: 0 auto;
        padding-left: 0.75in;
        padding-right: 0.75in;
        padding-top: 1in;
        padding-bottom: 0.75in;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
        overflow-y: auto;
        box-sizing: border-box;
        font-size: 12pt;
    }

</style>
<?php
$metas = !empty($draftVersion->meta_data) ? json_decode($draftVersion->meta_data, true) : [];
$cannotback = (!empty($action_decisions) && !empty($action_decisions['completable']) && in_array($decision_answer, $action_decisions['completable'])) ? true : false;
?>
<?php if ($privilige_type == 1): ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="portlet box purple">
            <div class="portlet-title">
                <div class="caption">
                    <?php echo "শাখা: " . $officeUnitsName . "; নথি নম্বর: " . $nothiRecord['nothi_no'] . '; বিষয়: ' . $nothiRecord['subject']; ?>
                </div>

                <div class="actions">
                    <?php if (!$cannotback) : ?>
                        <a title="নথিতে ফেরত যান" href="<?php
                        echo $this->Url->build(['_name' => 'noteDetail', $nothiRecord['id'], $nothi_office])
                        ?>" class="btn btn-danger btn-nothiback">
                            <i class="fa fa-list"></i> নথিতে ফেরত যান
                        </a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="portlet-body" id="potrojariDraftForm">
                <textarea id="contentbody" name="contentbody" style="display: none"></textarea>
                <div id="template-body" class="A4-max" style="
                 margin: 20px auto;
                 display: block;
                 background-color: rgb(255, 255, 255);
                 border: 1px solid #E6E6E6;">
                    <?= $draftVersion->content_body ?>
                </div>
            </div>
            <div class="portlet-footer text-center">
                <button class="btn btn-success btn-save" type="button" onclick="DRAFT_FORM.saveForm()"><i
                            class="fa fa-save"></i> <?php if ($cannotback) {
                        echo "অনুমোদন এবং সংরক্ষণ";
                    } else {
                        echo __(SAVE);
                    } ?>
                </button>
            </div>
        </div>
    </div>

    <link rel="stylesheet" type="text/css"
          href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.js" type="text/javascript"></script>
    <script type="text/javascript"
            src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery.mockjax.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/form-editable.js"></script>
    <script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
    <script src="<?php echo CDN_PATH; ?>js/client.min.js" type="text/javascript"></script>
    <script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/potrojari_related.js?v=<?= js_css_version ?>"
            type="text/javascript"></script>
    <script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/potrojari_editable.js?v=<?= js_css_version ?>"
            type="text/javascript"></script>

    <script>

		$('body').addClass('page-sidebar-closed');
		$('.page-sidebar-menu').addClass('page-sidebar-menu-closed');

		var numbers = {
			1: '১',
			2: '২',
			3: '৩',
			4: '৪',
			5: '৫',
			6: '৬',
			7: '৭',
			8: '৮',
			9: '৯',
			0: '০'
		};
		var bDate = new Array("০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯");
		var bMonth = new Array("জানুয়ারি", "ফেব্রুয়ারি", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর");

		function replaceNumbers(input) {
			var output = [];
			for (var i = 0; i < input.length; ++i) {
				if (numbers.hasOwnProperty(input[i])) {
					output.push(numbers[input[i]]);
				} else {
					output.push(input[i]);
				}
			}
			return output.join('');
		}

		var DRAFT_FORM = {
			setForm: function () {
				Metronic.blockUI({
					target: '.portlet.purple',
                    message : 'অপেক্ষা করুন।'
				});
				var temp = $('#template-body').html();
				$.each($('#template-body').find('a.editable'), function (i, v) {
					var dataType = $(this).attr('data-type');
					var dataEditable = $(this).attr('editable');

					var id = $(this).attr('id');
					if (dataType == 'textarea') {
						var txt = $(this).html();
					} else {
						var txt = $(this).text();
					}

					if(txt =='[লিখুন...]'){
					    txt = '...';
                    }

					if(typeof dataEditable !='undefined'){
						$(this).replaceWith("<span editable='true' class='canedit' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</span>");
					}else{
						$(this).replaceWith("<span class='canedit' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</span>");
					}
				});

				var contentbody = $('#template-body').html();
				$('#contentbody').text($.trim(contentbody));
				$('#template-body').html(temp);

				return true;
			},
			saveForm: function () {
				var checkpromise = new Promise(function (resolve, reject) {

                    <?php
                    if($cannotback){
                    ?>
					encodeImage('<?= $user_info['username'] ?>', '<?= $user_info['en_username'] ?>', function (src) {
						$('#potrojariDraftForm').find('#sender_signature').html('<img src="' + src + '" alt="signature" width="100" />');
                        if($('#potrojariDraftForm').find("#qr-code").length > 0){
                            <?php
                            if(isset($qr_code)):
                            ?>
//                            $('#potrojariDraftForm').find('#qr-code').html('<img src="' + (<?//= json_encode($qr_code) ?>//) + '" alt="qr-code" width="100" />');
                            <?php
                            endif;
                            ?>
                        }
                        DRAFT_FORM.setForm();
                        resolve();
					});
                    <?php }else{ ?>
					DRAFT_FORM.setForm();
					if ($('#potrojariDraftForm').find('#contentbody').text().length == 0) {
						reject("দুঃখিত! পুনরায় সংশোধন করে সংরক্ষণ করুন।");
					} else {
						resolve();
					}
                    <?php } ?>

				})
					.then(function () {
						if ($('#potrojariDraftForm').find('#contentbody').text().length == 0) {
							reject("দুঃখিত! পুনরায় সংশোধন করে সংরক্ষণ করুন।");
						} else {
							PROJAPOTI.ajaxSubmitDataCallback('<?= $this->Url->build(['_name' => 'editPotro', $nothi_office, $draftVersion['id']]) ?>', {
								content_body: $('#potrojariDraftForm').find('#contentbody').text().toString(),
                                imeidata: $.map($('#potrojariDraftForm').find('.imei_table_area').find('tbody>tr'),function(that){
									return JSON.stringify($(that).data('json'))
								})
							}, 'json', function (res) {
								if (res.status == 'success') {
//                                    if($('#potrojariDraftForm').find("#qr-code").find("img").length > 0){
                                        toastr.info('অপেক্ষা করুন। সার্টিফিকেট তৈরি হচ্ছে।');
                                        Metronic.unblockUI('.portlet.purple');
                                        Metronic.blockUI({
                                            target: '.portlet.purple',
                                            boxed: true,
                                            message : 'অপেক্ষা করুন। সার্টিফিকেট তৈরি হচ্ছে।ব্যাপারটি সময়সাপেক্ষ।'
                                        });
                                        var url = js_wb_root+'potrojari/'+'getPdfByPotro/';
                                        PROJAPOTI.ajaxSubmitDataCallback(url+<?= $id?>+'/'+<?=$nothi_office?>,{
                                            'margin_top' :'0.5','margin_right' : '0.5', 'margin_bottom' : '0.5', 'margin_left' : 0.5
                                        },'json',function(response){
                                            if(!isEmpty(response.status) && response.status == 'success'){
                                                url = js_wb_root+'nothiMasters/'+'potroDecisionSave/';
                                                PROJAPOTI.ajaxSubmitDataCallback(url+<?=$nothi_office?>+'/'+<?= $nothi_part_no?>,{'potro_id' : <?= $potro_id?>,'certificate_url' : response.src},'json',function(ajax_response){
                                                    if(!isEmpty(ajax_response.status) && ajax_response.status == 'success'){
                                                        setTimeout(function () {
                                                            toastr.success(ajax_response.message);
                                                            window.location.href = '<?= $this->Url->build(['_name' => 'noteDetail', $nothiRecord['id'], $nothi_office]) ?>'
                                                        }, 500);
                                                    }else{
                                                        toastr.error(ajax_response.message);
                                                        Metronic.unblockUI('.portlet.purple');
                                                    }
                                                });
                                            }else{
                                                toastr.error(response.message);
                                                Metronic.unblockUI('.portlet.purple');
                                            }
                                        });
//                                    }else{
//                                        Metronic.unblockUI('.portlet.purple');
//                                        toastr.success(res.message);
//                                        setTimeout(function () {
//                                            window.location.href = '<?//= $this->Url->build(['_name' => 'noteDetail', $nothiRecord['id'], $nothi_office]) ?>//'
//                                        }, 500);
//                                    }

								} else {
									toastr.error(res.message);
                                    Metronic.unblockUI('.portlet.purple');
								}

							})
						}
					}).catch(function (err) {
						toastr.error(err);
						Metronic.unblockUI('.portlet.purple');
					});
			}
		};

		function encodeImage(imgurl, enc_imgurl, callback) {
			$.ajax({
				url: js_wb_root + 'getSignature/' + imgurl + '/1?token=' + enc_imgurl,
				cache: false,
				type: 'post',
				async: false,
				success: function (img) {
					callback(img);
				},
				error: function (res, err) {
					return false;
				}
			});
		}
    </script>

    <script>
		function pad(n, width, z) {
			z = z || '0';
			n = n + '';
			return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
		}

		$(function () {
			$(document).on('click', '.mega-menu-dropdown .dropdown-menu', function (e) {
				e.stopPropagation();
			});

			$('select').select2();
			$('.no-select2').select2('destroy');
			$('select>option[title]').tooltip({'placement': 'top', 'container': 'body'});
			$("select[name='prapto_potro[]'] option").prop('selected', true);

			$('#pencil').remove();
			$.each($('span.canedit'), function (i, v) {
				var id = $(this).attr('id');
				var txt = $(this).text();
				var dataType = $(this).attr('data-type');
				var jsonEditable = $(this).attr('editable');

				if (id == 'sending_date') {
					$(this).replaceWith('<a data-placement="right"  data-pk="1" data-viewformat="dd.mm.yyyy" data-type="date" id="sending_date" href="#" class="editable editable-click">' + txt + '</a>');
				} else if (jsonEditable == 'true') {
					$(this).replaceWith("<a data-type='text' data-pk='1' class='editable editable-click' editable='true' href='javascript:;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</a>");
				} else if (dataType == 'textarea') {
					$(this).replaceWith("<a data-type='textarea' data-pk='1' class='editable editable-click' href='javascript:;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + $(this).html() + "</a>");
				} else {
					if ($(this).hasClass('potro_security')) {
						$(this).replaceWith("<a data-type='text' data-pk='1' class='editable editable-click potro_security' href='javascript:;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</a>");
					} else {
						$(this).replaceWith("<a data-type='text' data-pk='1' class='editable editable-click' href='javascript:;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</a>");
					}
				}
			});

			$.fn.editable.defaults.mode = 'inline';
			jQuery.uniform.update('#inline');
			$.fn.editable.defaults.inputclass = 'form-control';
			$('#potrojariDraftForm').find('#sending_date').editable({
				rtl: Metronic.isRTL(),
				display: function (value) {
					if (value != '' && value != null) {
						var dtb, dtb1, dtb2;
						var dt = value.getDate();
						if (dt >= 10) {
							dtb1 = Math.floor(dt / 10);
							dtb2 = dt % 10;
							dtb = bDate[dtb1] + "" + bDate[dtb2];
						} else {
							dtb = bDate[0] + "" + bDate[dt];
						}

						var mnb;
						var mn = value.getMonth();
						mnb = bMonth[mn];

						var yrb = "", yr1;
						var yr = value.getFullYear();

						for (var i = 0; i < 3; i++) {
							yr1 = yr % 10;
							yrb = bDate[yr1] + yrb;
							yr = Math.floor(yr / 10);
						}

						yrb = bDate[yr] + "" + yrb;

                        $(this).text(" " + dtb + " " + mnb + " " + yrb).attr("style", "position: relative; top: -8px;");

						var that = $(this);
						$('#potrojariDraftForm').find('.bangladate').next('br').remove();

						$('#potrojariDraftForm').find('.bangladate').remove();
						$.ajax({
							url: js_wb_root + "banglaDate",
							data: {date: value.getFullYear() + "-" + (mn + 1) + "-" + value.getDate()},
							type: 'POST',
							cache: false,
							dataType: 'JSON',
							async: false,
							success: function (bangladate) {
								that.before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");
								var day = value.getDate();
								$('.approval_date').text(value.getFullYear() + "-" + pad((mn + 1),2) + "-" + pad(day,2));

								var currentdate = new Date(value.getFullYear() , mn, day);
								var month6  = new Date(currentdate.setMonth(currentdate.getMonth()+6));
								$('.validation_date').text(month6.getFullYear() + "-" + pad((month6.getMonth() + 1),2) + "-" + pad(month6.getDate(),2));
							}
						});
					} else {
						var dtb, dtb1, dtb2;

						value = new Date();
						var dt = value.getDate();
						if (dt >= 10) {
							dtb1 = Math.floor(dt / 10);
							dtb2 = dt % 10;
							dtb = bDate[dtb1] + "" + bDate[dtb2];
						} else {
							dtb = bDate[0] + "" + bDate[dt];
						}

						var mnb;
						var mn = value.getMonth();
						mnb = bMonth[mn];

						var yrb = "", yr1;
						var yr = value.getFullYear();

						for (var i = 0; i < 3; i++) {
							yr1 = yr % 10;
							yrb = bDate[yr1] + yrb;
							yr = Math.floor(yr / 10);
						}

						yrb = bDate[yr] + "" + yrb;
						$(this).text(" " + dtb + " " + mnb + " " + yrb).attr("style", "position: relative; top: -8px;");

						var that = $(this);
						$('#potrojariDraftForm').find('.bangladate').next('br').remove();
						$('#potrojariDraftForm').find('.bangladate').remove();

						$.ajax({
							url: js_wb_root + "banglaDate",
							data: {date: value.getFullYear() + "-" + (mn + 1) + "-" + value.getDate()},
							type: 'POST',
							cache: false,
							dataType: 'JSON',
							async: false,
							success: function (bangladate) {
								that.before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");
								var day = value.getDate();
								$('.approval_date').text(value.getFullYear() + "-" + pad((mn + 1),2) + "-" + pad(day,2));

								var currentdate = new Date(value.getFullYear() , mn, day);
								var month6  = new Date(currentdate.setMonth(currentdate.getMonth()+6));
								$('.validation_date').text(month6.getFullYear() + "-" + pad((month6.getMonth() + 1),2) + "-" + pad(month6.getDate(),2));
							}
						});
					}
				}
			});

			$('#potrojariDraftForm').find('#sharok_no').editable({
				type: 'text',
				pk: 1,
				name: 'sharok_no',
                display: function (value,d) {
                    if(isEmpty(value) || value == '...'){
                        value = '[লিখুন...]';
                    }
                    $(this).text(value);
                }
			});

			$('#potrojariDraftForm').find('[editable]').editable({
				type: 'text',
				pk: 1,
				name: 'data_editable',
				display: function (value,d) {
				    if(isEmpty(value) || value == '...'){
				        value = '[লিখুন...]';
                    }
                    var id = $(this).attr('id').split('__');
                    if(!isEmpty(id) && !isEmpty(id[0])){
                        if(id[0] == "desk_one_qty"){
                            if(isNaN(value) && value != '-'){
                                toastr.error('দয়া করে ইংরেজিতে নাম্বার ইনপুট দিন');
                                value = '-';
                            }else if(!isNaN(value) && value != '-'){
                                value = Math.abs(value);
                            }
                        }
                    }
					var json = $(this).closest('tr').data('json');
					json[id[0]]= value;

					$(this).closest('tr').attr('data-json',JSON.stringify(json));

					$(this).text(value);
				}
			});

            <?php
            if($cannotback){
            ?>
			DRAFT_FORM.saveForm();
            <?php
            }
            ?>
		});
    </script>
<?php endif; ?>