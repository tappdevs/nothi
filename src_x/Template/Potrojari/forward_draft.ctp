<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fs1 a2i_ld_prostabpotro2"></i>FORWARD DRAFT
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a> <a
                href="#portlet-config" data-toggle="modal" class="config"></a> <a
                href="javascript:;" class="reload"></a> <a href="javascript:;"
                                                           class="remove"></a>
        </div>
    </div>
    <div class="portlet-body form">

        <!--Start: Remaining Form elements -->
        <div class="form-body draft-content">

            <h3 class="form-section">
                DRAFT FORWARD
            </h3>

            <div class="form-horizontal">
                <div class="row">
                    <div class="col-md-12">
                        <textarea height="350" class="ckeditor" id="template_content" name="html_content">
                            <?php echo $potrojari_draft["html_content"]; ?>
                        </textarea>
                    </div>
                </div>
            </div>


            <!-- BEGIN PAGE CONTENT-->

            <h3 class="form-section">

            </h3>

            <div class="form-horizontal">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->cell('DakSender', ['params' => $selected_office_section]); ?>
                    </div>
                </div>
            </div>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <?php //echo $this->Form->hidden('dak_id', ['id' => 'dak_id', 'value' => $dak_id]) ?>
                        <input type="hidden" name="potrojari_draft_id" id="potrojari_draft_id"
                               value="<?php echo $potrojari_draft['id'] ?>"/>
                        <input type="hidden" name="draft_version_id" id="potrojari_draft_id"
                               value="<?php echo $potrojari_draft['id'] ?>"/>
                        <button class="btn btn-primary blue" id="forwardSingle" title="<?php echo __("প্রেরণ"); ?>"
                                dak_id="<?php echo $potrojari_draft['dak_id'] ?>"> <?php echo __("প্রেরণ"); ?></button>
                    </div>
                </div>
            </div>


        </div>

        <!--End: Remaining Form elements -->
        <!--Start: Form Buttons -->


    </div>
</div>


<script type="text/javascript" defer="defer">
    jQuery(document).ready(function () {
        CKEDITOR.replace('template_content', {
            height: 800
        });
    });
</script>


<style>
    div.disabled {
        pointer-events: none;
        opacity: 0.2;
    }
</style>


<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/draft_potro_movement.js"
        type="text/javascript"></script>
<script>
    $(function () {

        DraftPotroMovement.init();

        var OfficeDataAdapter = new Bloodhound({
            datumTokenizer: function (d) {
                return d.tokens;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY'
        });
        OfficeDataAdapter.initialize();

    });
</script>