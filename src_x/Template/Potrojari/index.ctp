<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i>খসড়াসমূহ</div>
        <div class="tools">
            <a href="javascript:" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
            <a href="javascript:" class="reload"></a>
            <a href="javascript:" class="remove"></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <?php echo $this->Html->link('নতুন খসড়া তৈরি করুন', ['action' => 'makeDraft'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i
                                class="glyphicon glyphicon-chevron-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#">Print </a></li>
                            <li><a href="#">Save as PDF </a></li>
                            <li><a href="#">Export to Excel </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center">বিষয়</th>
                <th class="text-center">সংস্করণ</th>
                <th class="text-center">জারী নম্বর</th>
                <th class="text-center">স্মারক নম্বর</th>
                <th class="actions text-center">অ্যাকশন</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($query as $rows): ?>
                <tr>
                    <td class="text-center"><?php echo $rows['subject']; ?></td>
                    <td class="text-center"><?php echo $rows['potro_type']; ?></td>
                    <td class="text-center"><?php echo $rows['jari_no']; ?></td>
                    <td class="text-center"><?php echo $rows['sharok_no']; ?></td>
                    <td class="actions text-center">

                        <?= $this->Html->link(__('View'), ['action' => 'view', $rows['id']], ['class' => 'btn btn-primary']) ?>
                        <?= $this->Html->link(__('Edit Draft'), ['action' => 'edit', $rows['id']], ['class' => 'btn btn-primary']) ?>
                        <?= $this->Html->link(__('Forward'), ['action' => 'forwardDraft', $rows['id']], ['class' => 'btn btn-primary']) ?>
                        <!--
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $rows['id']], ['class' => 'btn btn-danger'], ['confirm' => __('Are you sure you want to delete # {0}?', $rows['id'])]) ?>
                            -->
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<script
    src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/dak_setup.js"
    type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        DakSetup.init();
    });
</script>
