<?php $canRepeat = 0; ?>
<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
            পত্রজারি প্রেরণ অবস্থা
        </div>
        <div class="actions">
            <button class="btn btn-sm btn-danger   btn-retry hide"><i class="fa fa-repeat"></i></button>
            <a class="btn btn-sm btn-primary" href="<?=$this->Url->build(['_name' => 'noteDetail',$nothi_part_no,$office_id])?>"><i class="fa fa-arrow-circle-left"></i>&nbsp; নথিতে ফেরত যান </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6 form-group">
                <div class="table-scrollable">
                    <table class="table table-hover table-bordered table-stripped">
                        <thead>
                            <tr class="heading">
                                <th><?= __("No") ?></th>
                                <th><?= __("Receiver") ?></th>
                                <th><?= __("Email") ?></th>
                                <th><?= __("Group") ?></th>
                                <th><?= __("Status") ?></th>
                            </tr>
                        </thead>
                        <?php if (!empty($receiverList)): ?>
                            <?php foreach ($receiverList as $key => $value): if($value['is_sent']==0){$canRepeat = 1 ; } ?>
                                <tr>
                                    <td style="width:10%"><?= $this->Number->format((++$key)) ?></td>
                                    <td style="width:30%"> <?= (!empty($value['receiving_officer_name'])?($value['receiving_officer_name'] . ', '):'').$value['receiving_officer_designation_label'].', '.(!empty($value['receiving_office_unit_name'])?($value['receiving_office_unit_name'] . ', '):'').$value['receiving_office_name'] ?> </td>
                                    <td style="width:20%"> <?= !empty($value['receiving_officer_email']) && $value['receiving_officer_email']!='undefined'?$value['receiving_officer_email']:'' ?> </td>
                                    <td style="width:20%"> <?= !empty($value['group_name']) ?$value['group_name']:'' ?> </td>
                                    <td style="width:20%"> <?= ($value['task_reposponse'] =='Draft')?'Sending...' : (($value['is_sent'] == 0)) ? (substr($value['task_reposponse'],0, 50).'...') : __("Sent")
                                ?> </td>
                                </tr>
                            <?php endforeach; ?>
<?php endif; ?>
                    </table>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6  form-group">
                <div class="table-scrollable">
                    <table class="table table-hover table-bordered table-stripped">
                        <thead>
                            <tr class="heading">
                                <th><?= __("No") ?></th>
                                <th><?= __("Onulipi") ?></th>
                                <th><?= __("Email") ?></th>
                                <th><?= __("Group") ?></th>
                                <th><?= __("Status") ?></th>
                            </tr>
                        </thead>
                        <?php if (!empty($onulipiList)): ?>
                        <?php foreach ($onulipiList as $key => $value): if($value['is_sent']==0){$canRepeat = 1 ; }?>
                                <tr>
                                    <td style="width:10%"><?= $this->Number->format((++$key)) ?></td>
                                    <td style="width:30%"> <?= (!empty($value['receiving_officer_name'])?($value['receiving_officer_name'] . ', '):'').$value['receiving_officer_designation_label'].', '.(!empty($value['receiving_office_unit_name'])?($value['receiving_office_unit_name'] . ', '):'').$value['receiving_office_name'] ?> </td>
                                    <td style="width:20%"> <?= !empty($value['receiving_officer_email']) && $value['receiving_officer_email']!='undefined'?$value['receiving_officer_email']:'' ?> </td>
                                    <td style="width:20%"> <?= !empty($value['group_name']) ?$value['group_name']:'' ?> </td>
                                    <td style="width:20%"> <?= ($value['run_task'] == 1 && $value['is_sent'] == 0) || $value['run_task'] == 0 ? (substr($value['task_reposponse'],0, 50).'...') : __("Sent")
                                ?> </td>
                                </tr>
    <?php endforeach; ?>
<?php endif; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        var canrepeat = <?= $canRepeat ?>;
        if(canrepeat==1){
            $('.btn-retry').removeClass('hide');
        }
        $('.btn-retry').click(function(){
            PROJAPOTI.ajaxSubmitDataCallback('<?= $this->Url->build(['controller'=>'Potrojari','action'=>'retrySent',$office_id,$potrojari_id]) ?>',{},'json',function(response){
                if(response.status=='success'){
                    toastr.success("ব্যাকগ্রাউন্ড প্রসেস চলছে। কিছুক্ষণ পর রিফ্রেশ করলে প্রেরণ অবস্থা জানা যাবে। ধন্যবাদ। ");
                }
            })
        })
    })
</script>
