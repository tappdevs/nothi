<div class="table-scrollable table-responsive">
	<table class="table table-hover table-bordered table-stripped">
		<thead>
		<tr class="heading">
			<th><?= __("No") ?></th>
			<th><?= __("Receiver") ?></th>
			<th><?= __("Email") ?></th>
			<th><?= __("Group") ?></th>
			<th><?= __("Status") ?></th>
		</tr>
		</thead>
		<?php if (!empty($potrojariList)): $canRepeat = 1; $serial = (($page-1)*$limit)+1 ?>
			<tbody>
			<?php foreach ($potrojariList as $key => $value): ?>
				<tr data-potrojari-id="<?= $value['potrojari_id'] ?>">
					<td style="width:10%"><?= $this->Number->format(($serial++)) ?>)&nbsp;<?= $this->Html->link('নথি',['_name'=>'noteDetail',$value['nothi_part_no']],['target'=>'_tab']) ?></td>
					<td style="width:30%"> <?= (!empty($value['receiving_officer_name']) ? (h($value['receiving_officer_name']) . ', ') : '') . $value['receiving_officer_designation_label'] . ', ' . (!empty($value['receiving_office_unit_name']) ? ($value['receiving_office_unit_name'] . ', ') : '') . $value['receiving_office_name'] ?> </td>
					<td style="width:20%"> <?= !empty($value['receiving_officer_email']) && $value['receiving_officer_email'] != 'undefined' ? str_replace(',', '<br/>', h($value['receiving_officer_email'])) : '' ?> </td>
					<td style="width:20%"> <?= !empty($value['group_name']) ? h($value['group_name']) : '' ?> </td>
					<td style="width:20%"> <?= ($value['run_task'] == 1 && $value['is_sent'] == 0) || $value['run_task'] == 0 ? (substr($value['task_reposponse'], 0, 50) . '...') : __("Sent")
						?> </td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		<?php endif; ?>
	</table>
</div>
<?= customPagination($this->Paginator) ?>

<script>
    $(function () {
        var canrepeat = <?= $canRepeat ?>;
        if (canrepeat == 1) {
            $('.btn-retry').removeClass('hide');
        }

        var potrojaridata = [];
        $.map($('tbody>tr'), function (val, i) {
            if (typeof ($(val).data('potrojari-id')) != 'undefined') {
                potrojaridata[$(val).data('potrojari-id')] = $(val).data('potrojari-id');
            }
        });
    })
</script>
