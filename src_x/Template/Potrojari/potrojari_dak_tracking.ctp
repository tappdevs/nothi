<?php $canRepeat = 0;
$i= 1;
?>
<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
            পত্রজারিকৃত ডাকের অবস্থা
        </div>
        <div class="actions">
            <a class="btn btn-sm btn-primary" href="<?=$this->Url->build(['_name' => 'noteDetail',$nothi_part_no,$office_id])?>"><i class="fa fa-arrow-circle-left"></i>&nbsp; নথিতে ফেরত যান </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6 form-group">
                <div class="table-scrollable">
                    <table class="table table-hover table-bordered table-stripped">
                        <thead>
                            <tr class="heading">
                                <th><?= __("No") ?></th>
                                <th><?= __("Receiver") ?></th>
                                <th><?= __("Status") ?></th>
                            </tr>
                        </thead>
                        <?php if (!empty($receiverList)): ?>
                            <?php foreach ($receiverList as $key => $value): if($value['is_sent']==0){$canRepeat = 1 ; }; ?>
                                <tr>
                                    <td style="width:10%"><?= $this->Number->format((++$key)) ?></td>
                                    <td style="width:30%"> <?= (!empty($value['receiving_officer_name'])?($value['receiving_officer_name'] . ', '):'').$value['receiving_officer_designation_label'].', '.(!empty($value['receiving_office_unit_name'])?($value['receiving_office_unit_name'] . ', '):'').$value['receiving_office_name'] ?> </td>
                                    <td class="receiver_status" id="dak_status_<?= $i ?>" style="width:20%" data-office-id="<?= makeEncryptedData($value['receiving_office_id']) ?>" data-dak-id="<?= !empty($value['dak_id'])?makeEncryptedData($value['dak_id']):0 ?>"></td>
                                </tr>
                            <?php $i++; endforeach; ?>
<?php endif; ?>
                    </table>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6  form-group">
                <div class="table-scrollable">
                    <table class="table table-hover table-bordered table-stripped">
                        <thead>
                            <tr class="heading">
                                <th><?= __("No") ?></th>
                                <th><?= __("Onulipi") ?></th>
                                <th><?= __("Status") ?></th>
                            </tr>
                        </thead>
                        <?php if (!empty($onulipiList)): ?>
                        <?php foreach ($onulipiList as $key => $value): if($value['is_sent']==0){$canRepeat = 1 ; }?>
                                <tr>
                                    <td style="width:10%"><?= $this->Number->format((++$key)) ?></td>
                                    <td style="width:30%"> <?= (!empty($value['receiving_officer_name'])?($value['receiving_officer_name'] . ', '):'').$value['receiving_officer_designation_label'].', '.(!empty($value['receiving_office_unit_name'])?($value['receiving_office_unit_name'] . ', '):'').$value['receiving_office_name'] ?> </td>
                                    <td class="receiver_status" id="dak_status_<?= $i ?>" style="width:20%" data-office-id="<?= makeEncryptedData($value['receiving_office_id']) ?>" data-dak-id="<?= !empty($value['dak_id'])?makeEncryptedData($value['dak_id']):0 ?>"></td>
                                </tr>
    <?php $i++; endforeach; ?>
<?php endif; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        var ajaxArray = [];
        $(".receiver_status").each(function() {
            var id = $(this).attr("id");
            var office_id = ($(this).data('office-id'));
            var dak_id = ($(this).data('dak-id'));

            if(dak_id !=0) {
                var ajaxObject = {id : id,office_id : office_id,dak_id : dak_id};
                ajaxArray.push(ajaxObject);
            }
        });
        if(ajaxArray.length > 0){
            processRequest(ajaxArray,0);
        }
       function processRequest (arry,indx){
            if(arry.length <= indx){
                return;
            }
           $("#"+arry[indx].id).html('<img src="'+js_wb_root+'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
           var ajax_url = "<?php
                   echo $this->Url->build(['controller' => 'Potrojari',
                       'action' => 'getDakTracking'])
                   ?>";
           office_id = arry[indx].office_id;
           dak_id = arry[indx].dak_id;
           $.ajax({
              type: 'POST',
              dataType: 'json',
              url: ajax_url,
              data: {office_id:office_id,dak_id:dak_id},
              success: function (data) {
                      if(!isEmpty(data)){
                          $("#"+arry[indx].id).html(data);
                      } else {
                          $("#"+arry[indx].id).html("");
                      }
                  processRequest(arry,++indx);
                  }
           });
       }
    });
</script>
