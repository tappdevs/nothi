<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8"/>
    <title>নথি | অফিস ব্যবস্থাপনা</title>
    <script>
		var numbers = {
			1: '১',
			2: '২',
			3: '৩',
			4: '৪',
			5: '৫',
			6: '৬',
			7: '৭',
			8: '৮',
			9: '৯',
			0: '০'
		};

		function replaceNumbers(input) {
			var output = [];
			for (var i = 0; i < input.length; ++i) {
				if (numbers.hasOwnProperty(input[i])) {
					output.push(numbers[input[i]]);
				} else {
					output.push(input[i]);
				}
			}
			return output.join('');
		}

		function substitutePdfVariables() {

			function getParameterByName(name) {
				var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
				return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
			}

			function substitute(name) {
				var value = getParameterByName(name);
				var elements = document.getElementsByClassName(name);
				var bn = replaceNumbers("'" + value + "'");
				bn = bn.replace("'", '');
				bn = bn.replace("'", '');
				for (var i = 0; elements && i < elements.length; i++) {
					elements[i].textContent = bn;
				}
			}

			['page']
				.forEach(function(param) {
					substitute(param);
				});
		}
    </script>
    <style>
        body{
            font-family: Nikosh, SolaimanLipi, 'Open Sans', sans-serif !important;
        }
    </style>
</head>
<?php
 if(isset($show_pdf_page_number) && $show_pdf_page_number == 1){
     ?>
     <body onload="substitutePdfVariables()" style="text-align: center">
     <?= $title ?>
     <span style="float:left"><?= ($isCloned) ? '.' : '' ?></span>
     <span class="page"></span>
     <?php
 }
 else {
     ?>
     <body style="text-align: center">
     <?= $title ?>
     <span style="float:left"><?= ($isCloned) ? '.' : '' ?></span>
    <?php
 }
?>

</body>
</html>