<meta charset="UTF-8">
<link href="<?= $this->request->webroot ?>potrojariPrintPreview/bootstrap-grid.min.css" rel="stylesheet">
<link href="<?= $this->request->webroot ?>potrojariPrintPreview/style.css" rel="stylesheet">
<style>
/*

    @font-face {
      font-family: 'SolaimanLipi';
      src: url('<?= $this->request->webroot ?>potrojariPrintPreview/SolaimanLipi.ttf') format('truetype');
    }

    @font-face {
      font-family: 'kalpurushANSI';
      src: url('<?= $this->request->webroot ?>potrojariPrintPreview/kalpurushANSI.ttf') format('truetype');
    }

    body, div {
      font-family: kalpurushANSI, SolaimanLipi, sans-serif;
      font-size: 13pt;
    }

    .en {
      font-family: Arial, SolaimanLipi, sans-serif
    }

    .templateWrapper {
      margin: auto;
      padding: 80px;
    }
*/

    /*
    .templateWrapper {
      box-shadow: 0 0 10px;
      margin: auto;
      width: 850px;
      padding: 80px;
      margin-top: 30px;
      max-width: 850px;
    }
    */

</style>
<?php echo($potrojariData->potro_description);?>





<script>
    const setAttributes = (el, attrs) => {
      for (var key in attrs) {
        el.setAttribute(key, attrs[key]);
      }
    }


    document.querySelector('#subject').parentElement.parentElement.setAttribute('style', 'padding:20px 0;');
    var sender_signature = document.querySelector('#sender_signature');
    var sender_signature2 = document.querySelector('#sender_signature2');
    var sender_email = document.querySelector('#sender_email');
    var sharok_no2 = document.querySelector('#sharok_no2');
    var canedit = document.querySelectorAll('.canedit');
    var english = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
    var isUrl = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

    for (var i = 0; i < canedit.length; i++) {
      console.log(canedit[i].textContent)

      if (english.test(canedit[i].textContent) || isUrl.test(canedit[i].textContent)) {
        canedit[i].setAttribute('class', 'canedit en');
      }
    }

    sender_signature2.parentElement.setAttribute('class', 'col-md-4 offset-md-8 text-center');
    sender_signature.parentElement.setAttribute('class', 'col-md-4 offset-md-8 text-center');
    sender_signature.parentElement.parentElement.parentElement.setAttribute('id', 'senderBlock');
    sender_signature.parentElement.parentElement.parentElement.style.cssText = "margin-top:20px;";
    sender_email.setAttribute('class', 'en');
    setAttributes(sharok_no2.parentElement.parentElement, {
      "style": "margin-bottom:50px;",
      "class": "row customFontSize align-items-center"
    });

    var sender_block = document.querySelectorAll('#sender_name, #sender_designation,#sender_phone,#sender_fax,#sender_email');
    for (var i = 0; i < sender_block.length; i++) {
      sender_block[i].parentElement.parentElement.parentElement.setAttribute('class', 'col-md-4 offset-md-2 text-center');
      sender_block[i].parentElement.parentElement.setAttribute('class', 'text-center');
    }

</script>