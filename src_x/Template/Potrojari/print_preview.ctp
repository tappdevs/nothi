<?php $canRepeat = 0; ?>
<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
            পত্রজারি প্রিন্ট প্রিভিউ
        </div>
        <div class="actions">
            <a class="btn btn-sm btn-primary"
               href="<?= $this->Url->build(['_name' => 'noteDetail', $nothi_part_no, $nothi_office]) ?>"><i
                        class="fa fa-arrow-circle-left"></i>&nbsp; নথিতে ফেরত যান </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">
                    মার্জিন
                </div>
            </div>
            <div class="panel-body">

                <?= $this->Form->create(null, ['id' => 'marginform']) ?>
                <?= $this->Form->hidden('reset',['value' => true]) ?>
                <div class="col-md-2 col-sm-3 col-xs-4 col-lg-2 form-group">
                    <?= $this->Form->input('margin_top', ['class' => 'form-control input-sm ', 'label' => 'উপর', 'default' => '0', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-4 col-lg-2 form-group">
                    <?= $this->Form->input('margin_bottom', ['class' => 'form-control input-sm', 'label' => 'নিচ', 'default' => '0', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-4 col-lg-2 form-group">
                    <?= $this->Form->input('margin_left', ['class' => 'form-control input-sm', 'label' => 'বাম', 'default' => '0', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-4 col-lg-2 form-group">
                    <?= $this->Form->input('margin_right', ['class' => 'form-control input-sm', 'label' => 'ডান', 'default' => '0', 'type' => 'number', 'precision' => 2, 'step' => .25, 'min' => 0]) ?>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-4 col-lg-2 form-group">
                    <?= $this->Form->input('orientation', ['class' => 'form-control input-sm', 'label' => 'ধরন', 'type' => 'select', 'options' => ['portrait' => 'Portrait', 'landscape' => 'Landscape']]) ?>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-4 col-lg-2 form-group">
                    <?= $this->Form->input('potro_header', ['class' => 'form-control input-sm', 'label' => __("Banner"), 'type' => 'select', 'options' => [0 => 'হ্যাঁ', 1 => 'না']]) ?>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-4 col-lg-2 form-group">
                    <label class="control-label">&nbsp;</label><br/>
                    <button class="btn btn-primary btn-sm btn-pdf" type="button"><i class="fa fa-binoculars"></i>
                        প্রিভিউ
                    </button>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <embed class="showPreview" src="" style=" width:100%; height: 600px;" type="application/pdf"></embed>
    </div>
</div>

<script>
    $(function () {
        $('.btn-pdf').click(function () {
            $('.showPreview').attr('src', '');
            PROJAPOTI.ajaxSubmitDataCallback('<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfById', $id, $nothi_office]) ?>',
                $('#marginform').serialize(), 'json', function (response) {
                    if (response.status == 'success') {
                        $('.showPreview').attr('src', response.src);
                    } else {
                        toastr.error(response.msg);
                    }
                }
            );
        });
    });
</script>
