<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>projapoti-nothi/css/styles.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"
      type="text/javascript">
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-multi-select/css/multi-select.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<style>
    .btn-changelog, .btn-forward-nothi, .btn-nothiback, .btn-print {
        padding: 3px 5px !important;
    }

    .btn-icon-only {

    }

    .editable {
        border: none !important;
        word-break:break-all;
        word-wrap:break-word
    }

    #sovapoti_signature, #sender_signature, #sender_signature2, #sender_signature3 {
        visibility: hidden;
    }

    #sovapoti_signature_date, #sender_signature_date, #sender_signature2_date, #sender_signature3_date {
        visibility: hidden;
    }

    #note {
        overflow: hidden;
        word-break: break-all;
        word-wrap: break-word;
        height: 100%;
    }

    .ms-container {
        width: 100% !important;
    }

    .bangladate {
        border-bottom: 1px solid #000 !important;
    }

    .editable-click, a.editable-click {
        border: none;
        word-break:break-all;
        word-wrap:break-word
    }

    .cc_list {
        white-space: pre-wrap;
    }

    .to_list {
        white-space: pre-wrap;
    }

    .popover-content {
        padding: 10px 30px;
    }

    .mega-menu-dropdown > .dropdown-menu {
        top: 10px !important;
    }

</style>


<?php if ($privilige_type == 1): ?>
    <div class="row">
        <div class="<?php
        echo count($allOtherDrafts) > 1 ? 'col-lg-2 col-md-2 col-sm-3 col-xs-3' : 'col-lg-0 col-md-0 col-sm-0 col-xs-0 hide'
        ?>"><?php
            if (!empty($allOtherDrafts)) {
                foreach ($allOtherDrafts as $key => $value) {
                    ?>

                    <div class='<?php
                    echo ($value['id'] == $draftVersion->id) ? 'text-success' : ''
                    ?>' id="potrojari_versions"
                         style=" word-break: break-all; word-wrap: break-word; cursor: pointer; border:1px solid #aaaaaa; background-color: #fff; margin:0 auto 5px; padding: 5px;"
                         onclick="window.location.href = '<?php
                         echo $this->Url->build(['controller' => 'potrojari',
                             'action' => 'potroDraft', $value['nothi_part_no'], $value['id'],
                             'potro', $nothi_office])
                         ?>'">
                        <?php
                        echo $this->Number->format($key + 1)
                        ?>)
                        পত্র ধরন: <?php echo $value['PotrojariTemplates']['template_name']; ?><br/>
                        বিষয়: <?php echo htmlspecialchars(trim($value['potro_subject'])); ?>

                    </div>
                    <?php
                }
            }
            ?>

        </div>
        <div class="<?php
        echo count($allOtherDrafts) > 1 ? 'col-lg-10 col-md-10 col-sm-9 col-xs-9'
            : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'
        ?>">

            <div class="portlet box green-seagreen">
                <div class="portlet-body">
                    <div class="row ">
                        <div class="col-md-12 " style="margin: 0 auto;">

                            <div class="form">

                                <?php
                                echo $this->Form->create($draftVersion,
                                    array('action' => '#', 'type' => 'file', 'id' => 'potrojariDraftForm'));
                                ?>
                                <?php
                                echo $this->Form->hidden('id');
                                echo $this->Form->hidden('can_potrojari');
                                echo $this->Form->hidden('dak_type',
                                    array('label' => false, 'class' => 'form-control',
                                        'value' => DAK_DAPTORIK));
                                echo $this->Form->hidden('dak_status',
                                    array('label' => false, 'class' => 'form-control',
                                        'value' => 1));
                                echo $this->Form->hidden('dak_subject',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sender_sarok_no',
                                    array('label' => false, 'class' => 'form-control'));

                                //approvalinformation
                                echo $this->Form->hidden('approval_office_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_officer_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_office_unit_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_officer_designation_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_officer_designation_label_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_officer_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_office_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('approval_office_unit_name_final',
                                    array('label' => false, 'class' => 'form-control'));


                                //senderinformation
                                echo $this->Form->hidden('sovapoti_office_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sovapoti_officer_designation_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sovapoti_officer_designation_label_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sovapoti_officer_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sovapoti_officer_name_final',
                                    array('label' => false, 'class' => 'form-control'));

                                //senderinformation
                                echo $this->Form->hidden('sender_office_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sender_officer_designation_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sender_officer_designation_label_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sender_officer_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('sender_officer_name_final',
                                    array('label' => false, 'class' => 'form-control'));


                                //receiverinformation
                                echo $this->Form->hidden('receiver_group_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_group_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_group_designation_final',
                                    array('label' => false, 'class' => 'form-control'));

                                echo $this->Form->hidden('receiver_office_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_office_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_officer_designation_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_office_unit_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_office_unit_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_officer_designation_label_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_officer_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_officer_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_officer_email_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('receiver_office_head_final',
                                    array('label' => false, 'class' => 'form-control'));

                                //onulipiinformation
                                echo $this->Form->hidden('onulipi_group_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_group_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_group_designation_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_office_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_office_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_officer_designation_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_office_unit_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_office_unit_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_officer_designation_label_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_officer_id_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_officer_name_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_officer_email_final',
                                    array('label' => false, 'class' => 'form-control'));
                                echo $this->Form->hidden('onulipi_office_head_final',
                                    array('label' => false, 'class' => 'form-control'));

                                echo $this->Form->hidden('potro_type',
                                    ['value' => $draftVersion->potro_type]);
                                //potrojari_language
                            echo $this->Form->hidden('potrojari_language',
                                array('label' => false, 'class' => 'form-control','id' => 'potro_language','value' => (isset($draftVersion->potrojari_language) && $draftVersion->potrojari_language == 'eng')?'eng':'bn'));

                        //attension
                    echo $this->Form->hidden('attension_office_id_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_officer_id_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_office_unit_id_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_officer_designation_id_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_officer_designation_label_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_officer_name_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_office_name_final',
                        array('label' => false, 'class' => 'form-control'));
                    echo $this->Form->hidden('attension_office_unit_name_final',
                        array('label' => false, 'class' => 'form-control'));
                                ?>
                                <?php

                                echo $this->Cell('Potrojari',
                                    ['template' => $template_list, 'potrojari' => $draftVersion,'templates'=>isset($templates)?$templates:''])
                                ?>


                                <div class="portlet-body">
                                    <h3> <?php echo __(SHONGJUKTI) ?> </h3>
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs ">
                                            <li class="active">
                                                <a href="#tab_prapto_potro" data-toggle="tab" aria-expanded="true">
                                                    প্রাপ্ত পত্রসমূহ </a>
                                            </li>
                                            <li class="">
                                                <a href="#tab_other_potro" data-toggle="tab" aria-expanded="false">
                                                    অন্যান্য </a>
                                            </li>

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_prapto_potro">
                                                <?php
                                                $selectmulti = [];

                                                if (!empty($potroAttachmentRecord)) {
                                                    foreach ($potroAttachmentRecord as $k => $v) {
                                                        $potr = explode(';', $v);
                                                        $oth = array_slice($potr, 1);

                                                        $selectmulti[$k] = "পত্র: " . $potr[0] . " - " . implode(' ',
                                                                $oth);
                                                    }
                                                }

                                                echo $this->Form->input('prapto_potro',
                                                    ['class' => 'multi-select', 'options' => $selectmulti,
                                                        'multiple' => 'multiple',
                                                        'label' => false, 'default' => $inlineattachments, 'escape' => false])
                                                ?>
                                                <?php echo $this->Form->end(); ?>
                                            </div>

                                            <div class="tab-pane" id="tab_other_potro">
                                                <form id="fileuploadpotrojari" action="<?= $this->Url->build(['_name'=>'tempUpload']) ?>" method="POST" enctype="multipart/form-data">

                                                    <input type="hidden" name="module_type" value="Nothi"/>
                                                    <input type="hidden" name="office_id" value="<?php echo $draftVersion->office_id ?>"/>
                                                    <input type="hidden" name="module" value="Potrojari" />


                                                    <div class="row fileupload-buttonbar">
                                                        <div class="col-lg-12">
                                                            <!-- The fileinput-button span is used to style the file input field as button -->
                                                            <span class="btn green btn-sm fileinput-button">
                                                                <i class="fs1 a2i_gn_add1"></i>
                                                                <span>
                                                                    ফাইল যুক্ত করুন </span>
                                                                <input type="file" name="files[]" multiple="">
                                                            </span>

                                                            <button type="button" class="btn btn-sm red delete">
                                                                <i class="fs1 a2i_gn_delete2"></i>
                                                                <span>
                                                                    সব মুছে ফেলুন </span>
                                                            </button>
                                                            <!--<input type="checkbox" class="toggle">-->
                                                            <!-- The global file processing state -->
                                                            <span class="fileupload-process">
                                                            </span>
                                                        </div>
                                                        <!-- The global progress information -->
                                                        <div class="col-lg-5 fileupload-progress fade">
                                                            <!-- The global progress bar -->
                                                            <div class="progress progress-striped active"
                                                                 role="progressbar"
                                                                 aria-valuemin="0" aria-valuemax="100">
                                                                <div class="progress-bar progress-bar-success"
                                                                     style="width:0%;">
                                                                </div>
                                                            </div>
                                                            <!-- The extended global progress information -->
                                                            <div class="progress-extended">
                                                                &nbsp;
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <!-- The table listing the files available for upload/download -->
                                                    <table role="presentation" class="table table-striped clearfix">
                                                        <tbody class="files">
                                                        <?php
                                                        if (isset($attachments)
                                                            && count($attachments)
                                                            > 0
                                                        ) {

                                                            foreach ($attachments as $single_data) {

                                                                if ($single_data['attachment_type']
                                                                    == 'text'
                                                                    || $single_data['attachment_type']
                                                                    == 'text/html'
                                                                ) {
                                                                    continue;
                                                                }

                                                                $fileName = explode('/', $single_data['file_name']);
                                                                $attachmentHeaders = get_file_type($single_data['file_name']);

                                                                $value = array(
                                                                    'name' => urldecode($fileName[count($fileName) - 1]),
                                                                    'thumbnailUrl' => (substr($attachmentHeaders,0,5) == 'image'?
                                                                        (FILE_FOLDER .$single_data['file_name']) : null),
                                                                    'size' => '-',
                                                                    'type' => $attachmentHeaders,
                                                                    'url' => FILE_FOLDER . $single_data['file_name'],
                                                                    'deleteUrl' => $this->Url->build(['_name'=>'secureDelete','?'=>[
                                                                        'id'=>$single_data['id'],
                                                                        'token'=>$temp_token
                                                                    ]]),
                                                                    'deleteType' => "GET",
                                                                );

                                                                echo '<tr class="template-download fade in">
    <td>
    <input type="checkbox" name="delete" value="1" class="toggle">
   
    </td>
    <td>
    <p class="name">
    ' . (isset($value['name']) ? '<a href="' . $value['url'] . '" title="' . $value['name'] . '" download="' . $value['name'] . '" data-gallery="">' . $value['name'] . '</a>'
                                                                        : '') . '
    </p>
    </td>
    <td>
    ' . (isset($value['size']) ? '<span class="size">' . (is_array($value['size']) ? $value['size'][0]
                                                                            : $value['size']) . '</span>'
                                                                        : '') . '
    </td>
    <td>
    <button class="btn red delete btn-sm" data-type="' . $value['deleteType'] . '" data-url="' . $value['deleteUrl'] . '" >
                <i class="glyphicon glyphicon-trash"></i>
                <span>মুছে ফেলুন</span>
            </button>
    </td>
    </tr>';
                                                            }
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </form>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-actions">
                                        <input type="button" class="btn btn-primary saveDraftNothi"
                                               value="<?php echo __(SAVE) ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="responsiveNothiUsers" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">
                        পরবর্তী কার্যক্রমের জন্য প্রেরণ করুন
                        <div class="pull-right">
                            <button type="button" class="btn green sendDraftNothi">প্রেরণ করুন</button>
                            <button type="button" data-dismiss="modal" class="btn  btn-danger">
                                বন্ধ করুন
                            </button>
                        </div>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
                        <input type="hidden" name="nothimasterid"/>

                        <div class="user_list">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green sendDraftNothi">প্রেরণ করুন</button>
                    <button type="button" data-dismiss="modal" class="btn  btn-danger">
                        বন্ধ করুন
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade modal-purple bs-modal-lg  " id="responsiveModal" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">

            <div class="modal-content">
                <div class="modal-header">
                    <div class="">
                        <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <br>
                    <div class="">
                        <button type="button" class="btn btn-default pull-right" id="portalGuardFileImportPotro" aria-hidden="true">পোর্টালের গার্ড ফাইল</button>
                        <h4 class="modal-title">গার্ড ফাইল</h4>
                    </div>
                </div>
                <div class="modal-body ">
                    <div class="row form-group">
                        <label class="col-md-2 control-label">ধরন</label>
                        <div class="col-md-6">
                            <?= $this->Form->input('guard_file_category_id', ['label' => false, 'type' => 'select', 'class' => 'form-control','id'=> 'guard_file_category_potro', 'options' => ["0" => "সকল"] + $guarfilesubjects]) ?>
                        </div>
                    </div>
                    <br/>

                    <table class='table table-striped table-bordered table-hover' id="filelist">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 10%">ক্রম</th>
                            <th class="text-center" style="width: 30%">ধরন</th>
                            <th class="text-center" style="width: 40%">নাম</th>
                            <th class="text-center" style="width: 20%">কার্যক্রম</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-lg" id="responsiveOnuccedModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">


            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-lg  " id="portalResponsiveModalPotro" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">

            <div class="modal-content">
                <div class="modal-header">
                    <div class="">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    </div>
                    <br>
                    <div class="">
                        <h4 class="modal-title">পোর্টালের গার্ড ফাইল</h4>
                    </div>
                </div>
                <div class="modal-body ">
                    <div class="row form-group">
                        <label class="col-md-2 control-label">ধরন</label>
                        <div class="col-md-6">
                            <?= $this->Form->input('portal_guard_file_types', ['label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => ["0" => "--"] + $guard_file_api_types]) ?>
                        </div>
                    </div>
                    <table class='table table-striped table-bordered table-hover' id="portalfilelistPotro">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 10%">ক্রম</th>
                            <th class="text-center" style="width: 30%">ধরন</th>
                            <th class="text-center" style="width: 40%">নাম</th>
                            <th class="text-center" style="width: 20%">কার্যক্রম</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-lg  " id="portalDownloadModal" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">পোর্টাল গার্ড ফাইল ডাউনলোড করুন</h4>
                </div>
                <div class="modal-body ">
                    <form class="form-horizontal" id="portalDownloadForm">
                        <div class="row">
                            <div class="col-md-10">
                                <?php
                                echo $this->Form->hidden('uploaded_attachments');
                                ?>
                                <div class="form-group">
                                    <label class="control-label col-md-2">শিরোনাম <span class="required"> * </span></label>

                                    <div class="col-md-10">
                                        <input type="text" name="name_bng" class="form-control" data-required="1" id="name-bng">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label class="control-label col-md-2">ধরন <span class="required"> * </span></label>

                                    <div class="col-md-10">
                                        <?= $this->Form->input('guard_file_category_id', ['id'=>'portal_guard_file_category_potro','label' => false, 'type' => 'select', 'class' => 'form-control', 'options' => $guarfilesubjects,'empty'=>'ধরন নির্বাচন করুন']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <span class="pull-left WaitMsg"></span>
                    <button data-bb-handler="success" type="button" onclick="PORTAL_GUARD_FORM.submit()" class="btn green submitbutton">সংরক্ষণ করুন</button>
                    <button data-bb-handler="danger" type="button" class="btn red" data-dismiss="modal">বন্ধ করুন</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <?= $this->element('preview_ele'); ?>
    <?= $this->element('froala_editor_js_css') ?>
    <script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/nothi_master_movements.js"></script>
    <link rel="stylesheet" type="text/css"
          href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.js" type="text/javascript"></script>
    <script type="text/javascript"
            src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/layout4/scripts/dak_setup.js" type="text/javascript"></script>
    <script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/potrojari_file_update.js" type="text/javascript"></script>
    <script type="text/javascript"
            src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery.mockjax.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/form-editable.js"></script>
    <script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/potrojari_editable.js?v=<?= time() ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.min.js"></script>
    <script type="text/javascript"
            src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript"
            src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript"
            src="<?php echo CDN_PATH; ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/global/scripts/datatable.js"></script>
    <script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/tbl_guard_files.js"></script>
    <script src="<?php echo CDN_PATH; ?>daptorik_preview/js/Sortable.js"></script>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/potrojari_related.js?v=<?=js_css_version?>" type="text/javascript"></script>

    <script>
        $('body').addClass('page-sidebar-closed');
        $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        $(document).ready(function ($) {
            if($('#noteView').length == 0){
                $('#note').after('<div data-original-title="Enter notes" data-toggle="manual" data-type="wysihtml5" data-pk="1" id="noteView" class="editable" tabindex="-1" style="display:inline;">'+$('div#note').html() +'<br/>');
                $('#note').replaceWith('<textarea name="" id="note" style="display: none;"></textarea>');
            }
            $('a').tooltip({'placement':'bottom'});
            Metronic.initSlimScroll('.receiver_group_list');
            Metronic.initSlimScroll('.onulipi_group_list');
            // delegate calls to data-toggle="lightbox"
            $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
                event.preventDefault();
                return $(this).ekkoLightbox({
                    onShown: function () {

                    },
                    onNavigate: function (direction, itemIndex) {

                    }
                });
            });
        });

        var numbers = {
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯',
            0: '০'
        };

        function replaceNumbers(input) {
            var output = [];
            for (var i = 0; i < input.length; ++i) {
                if (numbers.hasOwnProperty(input[i])) {
                    output.push(numbers[input[i]]);
                } else {
                    output.push(input[i]);
                }
            }
            return output.join('');
        }

        var DRAFT_FORM = {
            attached_files: [],
            sender_users: [],
            receiver_users: [],
            onulipi_users: [],
            setform: function () {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right"
                };

                if ($('.fr-toolbar').length > 0) {
                    $("#pencil").click();
                }
                
                var senders = $('.sender_users');
                var approvals = $('.approval_users');
                var receivers = $('.receiver_users');
                var onulipis = $('.onulipi_users');
                var sovapotis = $('.sovapoti_users');

                $('#sovapoti_signature').html("");
                $('#sovapoti_designation').html("");
                $('#sovapotiname').html("");
                $('#sovapoti_designation2').html("");
                $('#sovapotiname2').html("");
                $('#sender_signature').html("");
                $('#sender_signature2').html("");
                $('#sender_designation').html("");
                $('#sender_name').html("");
                $('#sender_designation2').html("");
                $('#sender_designation3').html("");
                $('#sender_name2').html("");
                $('#sender_name3').html("");
                //$('#office_organogram_id').html("");
                $('#address').html("");


                if ($('input[name=potro_type]').val() == 17) {
                    if (sovapotis.length == 0) {
                        toastr.error("দুঃখিত! সভাপতি তথ্য দেয়া হয়নি");
                        return false;
                    } else {

                        $.each($('.sovapoti_users'), function (i, data) {
                            var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
                            if (sendename.length > 0) {
                            } else {
                                sendename[0] = sendename;
                            }
                            $('input[name=sovapoti_office_id_final]').val($(this).attr('ofc_id'));
                            $('input[name=sovapoti_officer_designation_id_final]').val($(this).attr('designation_id'));
                            $('input[name=sovapoti_officer_designation_label_final]').val($(this).attr('designation'));
                            $('input[name=sovapoti_officer_id_final]').val($(this).attr('officer_id'));
                            $('input[name=sovapoti_officer_name_final]').val(sendename[0]);
                            var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' +$(this).attr('officer_id');
                            if(!isEmpty($(this).attr('unit_id'))){
                                url =url + '/'+$(this).attr('unit_id');
                            }
                            $.ajax({
                                url: url,
                                type: 'post',
                                dataType: 'json',
                                async: false,
                                success: function (data) {
                                    PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                                        $('#sovapoti_signature').html('<img src="' + src + '" alt="signature" width="100" />');
                                        if($("#potro_language").val() == 'eng'){
                                            $('#sovapoti_signature_date').html('<?= bnToen($signature_date_bangla) ?>');
                                        }else{
                                             $('#sovapoti_signature_date').html('<?= $signature_date_bangla ?>');
                                        }
                                        
                                        if(i==0){
                                            $('#sovapoti_signature2').html('<img src="' + src + '" alt="signature" width="100" />');
                                            if($("#potro_language").val() == 'eng'){
                                                $('#sovapoti_signature2_date').html('<?= bnToen($signature_date_bangla) ?>');
                                            }else{
                                                $('#sovapoti_signature2_date').html('<?= $signature_date_bangla ?>');
                                            }
                                            
                                        }
                                    });

                                }
                            });

                            if (i == 0) {
                                $('#sovapoti_designation').append($(this).attr('designation'));
                                $('#sovapotiname').append(sendename[0]);
                                $('#sovapoti_designation2').append($(this).attr('designation'));
                                $('#sovapotiname2').append(sendename[0]);
                            }
                        });
                    }
                }

                if (senders.length == 0) {
                    if ($('[name=potro_type]').val() == 17) {
                     $('#sender_name').html('');
                        $.each($('.approval_users'), function (i, data) {
                            var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));

                            if (i > 0) {
                                return;
                            }
                            $('input[name=sender_office_id_final]').val($(this).attr('ofc_id'));
                            $('input[name=sender_officer_designation_id_final]').val($(this).attr('designation_id'));
                            $('input[name=sender_officer_designation_label_final]').val($(this).attr('designation'));
                            $('input[name=sender_officer_id_final]').val($(this).attr('officer_id'));
                            $('input[name=sender_officer_name_final]').val(sendename[0]);

                            $('#sender_designation').text($(this).attr('designation'));
                            $('#sender_name').text(sendename[0]);


                            //$('#unit_name_editable').text(($('#unit_name_editable').text().length == 0 || $('#unit_name_editable').text() == '...') ? $('.sender_users').eq(0).attr('unit_name') : $('#unit_name_editable').text());
                             var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' +$(this).attr('officer_id');
                            if(!isEmpty($(this).attr('unit_id'))){
                                url =url + '/'+$(this).attr('unit_id');
                            }
                                $.ajax({
                                    url: url,
                                    type: 'post',
                                    dataType: 'json',
                                    async: false,
                                    success: function (data) {
                                        PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                                            $('#sender_signature').html('<img src="' + src + '" alt="signature" width="100" />');
                                            if($("#potro_language").val() == 'eng'){
                                                 $('#sender_signature_date').html('<?= bnToen($signature_date_bangla) ?>');
                                            }else{
                                                 $('#sender_signature_date').html('<?= $signature_date_bangla ?>');
                                            }
                                           
                                        });

                                        if ($.trim($('#potrojariDraftForm').find('#sender_phone').text()).length > 0 && $.trim($('#potrojariDraftForm').find('#sender_phone').text()) != '...' && data.personal_mobile.length > 0) {
                                            if($("#potro_language").val() == 'eng'){
                                                $('#sender_phone').text(EngFromBn(data.personal_mobile));
                                            }else{
                                                $('#sender_phone').text(data.personal_mobile);
                                            }
                                        }
                                    }
                                });

                        });
                    } else {
                        toastr.error("দুঃখিত! অনুমোদনকারী তথ্য দেয়া হয়নি");
                        return false;
                    }
                } else {
                 $('#sender_name').html('');
                    $.each($('.sender_users'), function (i, data) {
                        if (i > 0) {
                            toastr.error("একাধিক পদবি দেয়া যাবে না। ");
                            return
                        }
                        var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));

                        $('input[name=sender_office_id_final]').val($(this).attr('ofc_id'));
                        $('input[name=sender_officer_designation_id_final]').val($(this).attr('designation_id'));
                        $('input[name=sender_officer_designation_label_final]').val($(this).attr('designation'));
                        $('input[name=sender_officer_id_final]').val($(this).attr('officer_id'));
                        $('input[name=sender_officer_name_final]').val(sendename[0]);

                        $('#sender_designation').text($(this).attr('designation'));
                        $('#sender_name').text(sendename[0]);
                         var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' +$('.sender_users').eq(0).attr('officer_id');
                            if(!isEmpty($('.sender_users').eq(0).attr('unit_id'))){
                                url =url + '/'+$('.sender_users').eq(0).attr('unit_id');
                            }
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            async:false,
                            success: function (data) {

                                if ($('input[name=potro_type]').val() != 13) {
                                    PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                                        $('#sender_signature').html('<img src="' + src + '" alt="signature" width="100" />');
                                        if($("#potro_language").val() == 'eng'){
                                            $('#sender_signature_date').html('<?= bnToen($signature_date_bangla) ?>');
                                        }else{
                                            $('#sender_signature_date').html('<?= $signature_date_bangla ?>');
                                        }
                                        
                                    });
                                    $('#sender_designation').text($(this).attr('designation'));
                                    $('#sender_name').text(sendename[0]);
                                }

                                if ($.trim($('#potrojariDraftForm').find('#sender_email').text()).length > 0 && $.trim($('#potrojariDraftForm').find('#sender_email').text()) != '...') {
                                } else {
                                    if (data.personal_email.length > 0) {
                                        $('#sender_email').text($.trim(data.personal_email));
                                    }
                                }
                                if ($.trim($('#potrojariDraftForm').find('#sender_phone').text()).length > 0 && $.trim($('#potrojariDraftForm').find('#sender_phone').text()) != '...') {
                                } else {
                                    if (data.personal_mobile.length > 0) {
                                     if($("#potro_language").val() == 'eng'){
                                         $('#sender_phone').text(EngFromBn(data.personal_mobile));
                                     }else{
                                         $('#sender_phone').text(data.personal_mobile);
                                     }
                                        
                                    }
                                }
                            }
                        });
                    });
                }

                if (approvals.length != 0) {
                    $('#potrojariDraftForm').find('#sender_name2').closest('.row').show();
                    $('#potrojariDraftForm').find('#sender_designation2').closest('.row').show();
                    $('#potrojariDraftForm').find('#sender_signature2').closest('.row').show();

                    $.each($('.approval_users'), function (i, data) {
                        if ($('input[name=sender_officer_designation_id_final]').val() == $(this).attr('designation_id') && $('input[name=potro_type]').val() != 4) {
                            $('#potrojariDraftForm').find('#sender_name2').closest('.row').hide();
                            $('#potrojariDraftForm').find('#sender_designation2').closest('.row').hide();
                            $('#potrojariDraftForm').find('#sender_signature2').closest('.row').hide();
                        }
                        if (i > 0) {
                            toastr.error("একাধিক পদবি দেয়া যাবে না। ");
                            return;
                        }
                        var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
                        var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' +$(this).attr('officer_id');
                            if(!isEmpty($(this).attr('unit_id'))){
                                url =url + '/'+$(this).attr('unit_id');
                            }
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            async: false,
                            success: function (data) {
                                PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                                    if($('input[name=potro_type]').val() ==17){
                                        $('#sender_signature').html('<img src="' + src + '" alt="signature" width="100" />');
                                        if($("#potro_language").val() == 'eng'){
                                            $('#sender_signature_date').html('<?= bnToen($signature_date_bangla) ?>');
                                        }else{
                                            $('#sender_signature_date').html('<?= $signature_date_bangla ?>');
                                        }
                                    }
                                    $('#sender_signature2').html('<img src="' + src + '" alt="signature" width="100" />');
                                     if($("#potro_language").val() == 'eng'){
                                         $('#sender_signature2_date').html('<?= bnToen($signature_date_bangla) ?>');
                                     }else{
                                         $('#sender_signature2_date').html('<?= $signature_date_bangla ?>');
                                     }
                                    
                                });

                            }
                        });

                        if (i > 0) {
                            $('#sender_designation2').append(', ');
                            $('#sender_name2').append(', ');
                            $('#sender_designation3').append(', ');
                            $('#sender_name3').append(', ');
                        }

                        $('#sender_designation2').text($(this).attr('designation'));
                        $('#sender_name2').text(sendename[0]);

                        $('#sender_designation3').text($(this).attr('designation'));
                        $('#sender_name3').text(sendename[0]);

                        if($('input[name=potro_type]').val() ==17){
                            $('#sender_designation').text($(this).attr('designation'));
                            $('#sender_name').text(sendename[0]);
                        }

                        $('input[name=approval_office_id_final]').val($(this).attr('ofc_id'));
                        $('input[name=approval_officer_designation_id_final]').val($(this).attr('designation_id'));
                        $('input[name=approval_officer_designation_label_final]').val($(this).attr('designation'));
                        $('input[name=approval_officer_id_final]').val($(this).attr('officer_id'));
                        $('input[name=approval_office_unit_id_final]').val($(this).attr('unit_id'));
                        $('input[name=approval_office_unit_name_final]').val($(this).attr('unit_name'));
                        $('input[name=approval_officer_name_final]').val(sendename[0]);
                        $('input[name=approval_office_name_final]').val($(this).attr('ofc_name'));

                    });
                } else {

                    $.each($('.sender_users'), function (i, data) {
                        var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
                        var url =  '<?php echo $this->Url->build(['controller' => 'EmployeeRecords','action' => 'userDetails'])?>/' +$(this).attr('officer_id');
                            if(!isEmpty($(this).attr('unit_id'))){
                                url =url + '/'+$(this).attr('unit_id');
                            }
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            async: false,
                            success: function (data) {
                                PotrojariFormEditable.encodeImage(data.username, data.en_username, function (src) {
                                    $('#sender_signature2').html('<img src="' + src + '" alt="signature" width="100" />');
                                     if($("#potro_language").val() == 'eng'){
                                         $('#sender_signature2_date').html('<?= bnToen($signature_date_bangla) ?>');
                                     }else{
                                         $('#sender_signature2_date').html('<?= $signature_date_bangla ?>');
                                     }
                                    
                                });

                            }
                        });

                        if (i > 0) {
                            $('#sender_designation2').append(', ');
                            $('#sender_name2').append(', ');
                            $('#sender_designation3').append(', ');
                            $('#sender_name3').append(', ');
                        }

                        $('#sender_designation2').text($(this).attr('designation'));
                        $('#sender_name2').text(sendename[0]);

                        $('#sender_designation3').text($(this).attr('designation'));
                        $('#sender_name3').text(sendename[0]);

                        $('input[name=approval_office_id_final]').val($(this).attr('ofc_id'));
                        $('input[name=approval_officer_designation_id_final]').val($(this).attr('designation_id'));
                        $('input[name=approval_officer_designation_label_final]').val($(this).attr('designation'));
                        $('input[name=approval_officer_id_final]').val($(this).attr('officer_id'));
                        $('input[name=approval_office_unit_id_final]').val($(this).attr('unit_id'));
                        $('input[name=approval_office_unit_name_final]').val($(this).attr('unit_name'));
                        $('input[name=approval_officer_name_final]').val(sendename[0]);
                        $('input[name=approval_office_name_final]').val($(this).attr('ofc_name'));
                        if ($('input[name=sender_officer_designation_id_final]').val() == $(this).attr('designation_id') && $('input[name=potro_type]').val() != 4) {
                            $('#potrojariDraftForm').find('#sender_name2').closest('.row').hide();
                            $('#potrojariDraftForm').find('#sender_designation2').closest('.row').hide();
                            $('#potrojariDraftForm').find('#sender_signature2').closest('.row').hide();
                        }
                    });
                }


                if (receivers.length == 0) {
                    if ($('input[name=potro_type]').val() != 19 && ($('#potrojariDraftForm').find('#office_organogram_id').length > 0 || $('input[name=potro_type]').val() == 13)){
                        toastr.error("দুঃখিত! প্রাপক তথ্য দেয়া হয়নি");
                        return false;
                    } else {

                        $('input[name=receiver_group_id_final]').val('');
                        $('input[name=receiver_group_name_final]').val('');
                        $('input[name=receiver_group_designation_final]').val('');
                        $('input[name=receiver_office_id_final]').val('');
                        $('input[name=receiver_office_name_final]').val('');
                        $('input[name=receiver_officer_designation_id_final]').val('');
                        $('input[name=receiver_officer_designation_label_final]').val('');
                        $('input[name=receiver_office_unit_id_final]').val('');
                        $('input[name=receiver_office_unit_name_final]').val('');
                        $('input[name=receiver_officer_id_final]').val('');
                        $('input[name=receiver_officer_name_final]').val('');
                        $('input[name=receiver_officer_email_final]').val('');
                        $('input[name=receiver_office_head_final]').val('');
                    }
                } else {
                    if ($('#potrojariDraftForm').find('#office_organogram_id').length > 0) {
                        if (receivers.length == 1) {
                            $.each(receivers, function (i, data) {

                                if (typeof ($(this).attr('group_id')) != 'undefined') {

                                    $('input[name=receiver_group_id_final]').val($(this).attr('group_id'));
                                    $('input[name=receiver_group_name_final]').val($(this).attr('group_name'));
                                    $('input[name=receiver_group_designation_final]').val($(this).attr('group_designation'));
                                } else {
                                    $('input[name=receiver_office_id_final]').val($(this).attr('ofc_id'));
                                    $('input[name=receiver_office_name_final]').val($(this).attr('ofc_name'));
                                    $('input[name=receiver_officer_designation_id_final]').val($(this).attr('designation_id'));
                                    $('input[name=receiver_officer_designation_label_final]').val($(this).attr('designation'));
                                    $('input[name=receiver_office_unit_id_final]').val($(this).attr('unit_id'));
                                    $('input[name=receiver_office_unit_name_final]').val($(this).attr('unit_name'));
                                    $('input[name=receiver_officer_id_final]').val($(this).attr('officer_id'));
                                    $('input[name=receiver_officer_name_final]').val($(this).attr('officer_name'));
                                    $('input[name=receiver_officer_email_final]').val($(this).attr('officer_email'));
                                    $('input[name=receiver_office_head_final]').val($(this).attr('office_head'));
                                }
                            });
                        }
                        else {

                            var prevgroupId = '';
                            var prevgroupNm = '';
                            var prevOfficeId = '';
                            var prevOfficeNm = '';
                            var prevOfficeOrgId = '';
                            var prevOfficeOrgLb = '';
                            var prevOfficeUnitId = '';
                            var prevOfficeUnitLb = '';
                            var prevOfficerId = '';
                            var prevOfficerNm = '';
                            var prevOfficerEm = '';
                            var prevOfficerHd = '';
                            var prevgroupDes = '';

                            $.each(receivers, function (i, data) {
                                if (typeof ($(this).attr('group_id')) != 'undefined') {
                                    $('input[name=receiver_group_id_final]').val(prevgroupId + ";" + $(this).attr('group_id'));
                                    prevgroupId = $('input[name=receiver_group_id_final]').val();
                                    $('input[name=receiver_group_name_final]').val(prevgroupNm + ";" + $(this).attr('group_name'));
                                    prevgroupNm = $('input[name=receiver_group_name_final]').val();

                                    $('input[name=receiver_group_designation_final]').val(prevgroupDes + ";" + $(this).attr('group_designation'));
                                    prevgroupDes = $('input[name=receiver_group_designation_final]').val();
                                }

                                $('input[name=receiver_office_id_final]').val(prevOfficeId + ";" + $(this).attr('ofc_id'));
                                prevOfficeId = $('input[name=receiver_office_id_final]').val();
                                $('input[name=receiver_office_name_final]').val(prevOfficeNm + ";" + $(this).attr('ofc_name'));
                                prevOfficeNm = $('input[name=receiver_office_name_final]').val();
                                $('input[name=receiver_officer_designation_id_final]').val(prevOfficeOrgId + ";" + $(this).attr('designation_id'));
                                prevOfficeOrgId = $('input[name=receiver_officer_designation_id_final]').val();
                                $('input[name=receiver_officer_designation_label_final]').val(prevOfficeOrgLb + ";" + $(this).attr('designation'));
                                prevOfficeOrgLb = $('input[name=receiver_officer_designation_label_final]').val();
                                $('input[name=receiver_office_unit_id_final]').val(prevOfficeUnitId + ";" + $(this).attr('unit_id'));
                                prevOfficeUnitId = $('input[name=receiver_office_unit_id_final]').val();
                                $('input[name=receiver_office_unit_name_final]').val(prevOfficeUnitLb + ";" + $(this).attr('unit_name'));
                                prevOfficeUnitLb = $('input[name=receiver_office_unit_name_final]').val();
                                $('input[name=receiver_officer_id_final]').val(prevOfficerId + ";" + $(this).attr('officer_id'));
                                prevOfficerId = $('input[name=receiver_officer_id_final]').val();
                                $('input[name=receiver_officer_name_final]').val(prevOfficerNm + ";" + $(this).attr('officer_name'));
                                prevOfficerNm = $('input[name=receiver_officer_name_final]').val();
                                $('input[name=receiver_officer_email_final]').val(prevOfficerEm + ";" + $(this).attr('officer_email'));
                                prevOfficerEm = $('input[name=receiver_officer_email_final]').val();
                                $('input[name=receiver_office_head_final]').val(prevOfficerHd + ";" + $(this).attr('office_head'));
                                prevOfficerHd = $('input[name=receiver_office_head_final]').val();
                            });
                        }
                    } else if ($('input[name=potro_type]').val() == 13) {
                        if (receivers.length == 1) {
                            $.each(receivers, function (i, data) {
                                if (typeof ($(this).attr('group_id')) != 'undefined') {
                                    $('input[name=receiver_group_id_final]').val($(this).attr('group_id'));
                                    $('input[name=receiver_group_name_final]').val($(this).attr('group_name'));
                                    $('input[name=receiver_group_designation_final]').val($(this).attr('group_designation'));
                                } else {
                                    $('input[name=receiver_office_id_final]').val($(this).attr('ofc_id'));
                                    $('input[name=receiver_office_name_final]').val($(this).attr('ofc_name'));
                                    $('input[name=receiver_officer_designation_id_final]').val($(this).attr('designation_id'));
                                    $('input[name=receiver_officer_designation_label_final]').val($(this).attr('designation'));
                                    $('input[name=receiver_office_unit_id_final]').val($(this).attr('unit_id'));
                                    $('input[name=receiver_office_unit_name_final]').val($(this).attr('unit_name'));
                                    $('input[name=receiver_officer_id_final]').val($(this).attr('officer_id'));
                                    $('input[name=receiver_officer_name_final]').val($(this).attr('officer_name'));
                                    $('input[name=receiver_officer_email_final]').val($(this).attr('officer_email'));
                                    $('input[name=receiver_office_head_final]').val($(this).attr('office_head'));
                                }
                            });
                        } else {
                            var prevgroupId = '';
                            var prevgroupNm = '';
                            var prevgroupDes = '';
                            var prevOfficeId = '';
                            var prevOfficeNm = '';
                            var prevOfficeOrgId = '';
                            var prevOfficeOrgLb = '';
                            var prevOfficeUnitId = '';
                            var prevOfficeUnitLb = '';
                            var prevOfficerId = '';
                            var prevOfficerNm = '';
                            var prevOfficerEm = '';
                            var prevOfficerHd = '';

                            $.each(receivers, function (i, data) {
                                if (typeof ($(this).attr('group_id')) != 'undefined') {
                                    $('input[name=receiver_group_id_final]').val(prevgroupId + ";" + $(this).attr('group_id'));
                                    prevgroupId = $('input[name=receiver_group_id_final]').val();
                                    $('input[name=receiver_group_name_final]').val(prevgroupNm + ";" + $(this).attr('group_name'));
                                    prevgroupNm = $('input[name=receiver_group_name_final]').val();
                                    $('input[name=receiver_group_designation_final]').val(prevgroupDes + ";" + $(this).attr('group_designation'));
                                    prevgroupDes = $('input[name=receiver_group_designation_final]').val();
                                }

                                $('input[name=receiver_office_id_final]').val(prevOfficeId + ";" + $(this).attr('ofc_id'));
                                prevOfficeId = $('input[name=receiver_office_id_final]').val();
                                $('input[name=receiver_office_name_final]').val(prevOfficeNm + ";" + $(this).attr('ofc_name'));
                                prevOfficeNm = $('input[name=receiver_office_name_final]').val();
                                $('input[name=receiver_officer_designation_id_final]').val(prevOfficeOrgId + ";" + $(this).attr('designation_id'));
                                prevOfficeOrgId = $('input[name=receiver_officer_designation_id_final]').val();
                                $('input[name=receiver_officer_designation_label_final]').val(prevOfficeOrgLb + ";" + $(this).attr('designation'));
                                prevOfficeOrgLb = $('input[name=receiver_officer_designation_label_final]').val();
                                $('input[name=receiver_office_unit_id_final]').val(prevOfficeUnitId + ";" + $(this).attr('unit_id'));
                                prevOfficeUnitId = $('input[name=receiver_office_unit_id_final]').val();
                                $('input[name=receiver_office_unit_name_final]').val(prevOfficeUnitLb + ";" + $(this).attr('unit_name'));
                                prevOfficeUnitLb = $('input[name=receiver_office_unit_name_final]').val();
                                $('input[name=receiver_officer_id_final]').val(prevOfficerId + ";" + $(this).attr('officer_id'));
                                prevOfficerId = $('input[name=receiver_officer_id_final]').val();
                                $('input[name=receiver_officer_name_final]').val(prevOfficerNm + ";" + $(this).attr('officer_name'));
                                prevOfficerNm = $('input[name=receiver_officer_name_final]').val();
                                $('input[name=receiver_officer_email_final]').val(prevOfficerEm + ";" + $(this).attr('officer_email'));
                                prevOfficerEm = $('input[name=receiver_officer_email_final]').val();
                                $('input[name=receiver_office_head_final]').val(prevOfficerHd + ";" + $(this).attr('office_head'));
                                prevOfficerHd = $('input[name=receiver_office_head_final]').val();
                            });
                        }
                    } else {
                        $('input[name=receiver_group_id_final]').val('');
                        $('input[name=receiver_group_name_final]').val('');
                        $('input[name=receiver_group_designation_final]').val('');
                        $('input[name=receiver_office_id_final]').val('');
                        $('input[name=receiver_office_name_final]').val('');
                        $('input[name=receiver_officer_designation_id_final]').val('');
                        $('input[name=receiver_officer_designation_label_final]').val('');
                        $('input[name=receiver_office_unit_id_final]').val('');
                        $('input[name=receiver_office_unit_name_final]').val('');
                        $('input[name=receiver_officer_id_final]').val('');
                        $('input[name=receiver_officer_name_final]').val('');
                        $('input[name=receiver_officer_email_final]').val('');
                        $('input[name=receiver_office_head_final]').val('');
                        toastr.error("দুঃখিত! পত্রে প্রাপক দেয়া যাবে না!");
                    }
                }

                if (onulipis.length == 0) {
                    $('input[name=onulipi_group_id_final]').val('');
                    $('input[name=onulipi_group_name_final]').val('');
                    $('input[name=onulipi_group_designation_final]').val('');
                    $('input[name=onulipi_office_id_final]').val('');
                    $('input[name=onulipi_office_name_final]').val('');
                    $('input[name=onulipi_officer_designation_id_final]').val('');
                    $('input[name=onulipi_officer_designation_label_final]').val('');
                    $('input[name=onulipi_office_unit_id_final]').val('');
                    $('input[name=onulipi_office_unit_name_final]').val('');
                    $('input[name=onulipi_officer_id_final]').val('');
                    $('input[name=onulipi_officer_name_final]').val('');
                    $('input[name=onulipi_officer_email_final]').val('');
                    $('input[name=onulipi_office_head_final]').val('');
                } else {

                    if ($('#potrojariDraftForm').find('#cc_list_div').length > 0) {
                        var prevgroupId = '';
                        var prevgroupNm = '';
                        var prevgroupDes = '';
                        var prevOfficeId = '';
                        var prevOfficeNm = '';
                        var prevOfficeOrgId = '';
                        var prevOfficeOrgLb = '';
                        var prevOfficeUnitId = '';
                        var prevOfficeUnitLb = '';
                        var prevOfficerId = '';
                        var prevOfficerNm = '';
                        var prevOfficerEm = '';
                        var prevOfficerHd = '';

                        var totalmember = $.map(onulipis,function(data){
                            return parseInt($(data).attr('group_member'));
                        }).reduce(function(total,num){return total+num; });
                        var bn = replaceNumbers("'" + totalmember + "'");
                        bn = bn.replace("'", '');
                        bn = bn.replace("'", '');

                        if($("#potro_language").val() == 'eng'){
                            $('#sharok_no2').text($('#sharok_no').text() + "/1" + (totalmember > 1 ? ("(" + bn + ")") : ""));
                        }else{
                             $('#sharok_no2').text($('#sharok_no').text() + "/" + replaceNumbers('1') + (totalmember > 1 ? ("(" + bn + ")") : ""));
                        }
                        
                       

                        $('#potrojariDraftForm').find('#cc_list_div').closest('.row').nextAll('.row').show()
                        $('#potrojariDraftForm').find('#cc_list_div').closest('.row').show();
                        $('#potrojariDraftForm').find('#sharok_no2').closest('.row').show();
                        $('#potrojariDraftForm').find('#sending_date_2').closest('.row').show();
                        $('#potrojariDraftForm').find('#sender_name2').closest('.row').show();
                        $('#potrojariDraftForm').find('#sender_designation2').closest('.row').show();
                        $('#potrojariDraftForm').find('#sender_signature2').closest('.row').show();

                        $.each(onulipis, function (i, data) {
                            if (typeof ($(this).attr('group_id')) != 'undefined') {
                                $('input[name=onulipi_group_id_final]').val(prevgroupId + ";" + $(this).attr('group_id'));
                                prevgroupId = $('input[name=onulipi_group_id_final]').val();

                                $('input[name=onulipi_group_name_final]').val(prevgroupNm + ";" + $(this).attr('group_name'));
                                prevgroupNm = $('input[name=onulipi_group_name_final]').val();

                                $('input[name=onulipi_group_designation_final]').val(prevgroupDes + ";" + $(this).attr('group_designation'));
                                prevgroupDes = $('input[name=onulipi_group_designation_final]').val();
                            }

                            $('input[name=onulipi_office_id_final]').val(prevOfficeId + ";" + $(this).attr('ofc_id'));
                            prevOfficeId = $('input[name=onulipi_office_id_final]').val();
                            $('input[name=onulipi_office_name_final]').val(prevOfficeNm + ";" + $(this).attr('ofc_name'));
                            prevOfficeNm = $('input[name=onulipi_office_name_final]').val();
                            $('input[name=onulipi_officer_designation_id_final]').val(prevOfficeOrgId + ";" + $(this).attr('designation_id'));
                            prevOfficeOrgId = $('input[name=onulipi_officer_designation_id_final]').val();
                            $('input[name=onulipi_officer_designation_label_final]').val(prevOfficeOrgLb + ";" + $(this).attr('designation'));
                            prevOfficeOrgLb = $('input[name=onulipi_officer_designation_label_final]').val();
                            $('input[name=onulipi_office_unit_id_final]').val(prevOfficeUnitId + ";" + $(this).attr('unit_id'));
                            prevOfficeUnitId = $('input[name=onulipi_office_unit_id_final]').val();
                            $('input[name=onulipi_office_unit_name_final]').val(prevOfficeUnitLb + ";" + $(this).attr('unit_name'));
                            prevOfficeUnitLb = $('input[name=onulipi_office_unit_name_final]').val();
                            $('input[name=onulipi_officer_id_final]').val(prevOfficerId + ";" + $(this).attr('officer_id'));
                            prevOfficerId = $('input[name=onulipi_officer_id_final]').val();
                            $('input[name=onulipi_officer_name_final]').val(prevOfficerNm + ";" + $(this).attr('officer_name'));
                            prevOfficerNm = $('input[name=onulipi_officer_name_final]').val();
                            $('input[name=onulipi_officer_email_final]').val(prevOfficerEm + ";" + $(this).attr('officer_email'));
                            prevOfficerEm = $('input[name=onulipi_officer_email_final]').val();
                            $('input[name=onulipi_office_head_final]').val(prevOfficerHd + ";" + $(this).attr('office_head'));
                            prevOfficerHd = $('input[name=onulipi_office_head_final]').val();
                        });

                    } else if ($('input[name=potro_type]').val() == 13) {
                        var prevgroupId = '';
                        var prevgroupNm = '';
                        var prevgroupDes = '';
                        var prevOfficeId = '';
                        var prevOfficeNm = '';
                        var prevOfficeOrgId = '';
                        var prevOfficeOrgLb = '';
                        var prevOfficeUnitId = '';
                        var prevOfficeUnitLb = '';
                        var prevOfficerId = '';
                        var prevOfficerNm = '';
                        var prevOfficerEm = '';
                        var prevOfficerHd = '';

                        $.each(onulipis, function (i, data) {
                            if (typeof ($(this).attr('group_id')) != 'undefined') {
                                $('input[name=onulipi_group_id_final]').val(prevgroupId + ";" + $(this).attr('group_id'));
                                prevgroupId = $('input[name=onulipi_group_id_final]').val();
                                $('input[name=onulipi_group_name_final]').val(prevgroupNm + ";" + $(this).attr('group_name'));
                                prevgroupNm = $('input[name=onulipi_group_name_final]').val();
                                $('input[name=onulipi_group_designation_final]').val(prevgroupDes + ";" + $(this).attr('group_designation'));
                                prevgroupDes = $('input[name=onulipi_group_designation_final]').val();
                            }
                            $('input[name=onulipi_office_id_final]').val(prevOfficeId + ";" + $(this).attr('ofc_id'));
                            prevOfficeId = $('input[name=onulipi_office_id_final]').val();
                            $('input[name=onulipi_office_name_final]').val(prevOfficeNm + ";" + $(this).attr('ofc_name'));
                            prevOfficeNm = $('input[name=onulipi_office_name_final]').val();
                            $('input[name=onulipi_officer_designation_id_final]').val(prevOfficeOrgId + ";" + $(this).attr('designation_id'));
                            prevOfficeOrgId = $('input[name=onulipi_officer_designation_id_final]').val();
                            $('input[name=onulipi_officer_designation_label_final]').val(prevOfficeOrgLb + ";" + $(this).attr('designation'));
                            prevOfficeOrgLb = $('input[name=onulipi_officer_designation_label_final]').val();
                            $('input[name=onulipi_office_unit_id_final]').val(prevOfficeUnitId + ";" + $(this).attr('unit_id'));
                            prevOfficeUnitId = $('input[name=onulipi_office_unit_id_final]').val();
                            $('input[name=onulipi_office_unit_name_final]').val(prevOfficeUnitLb + ";" + $(this).attr('unit_name'));
                            prevOfficeUnitLb = $('input[name=onulipi_office_unit_name_final]').val();
                            $('input[name=onulipi_officer_id_final]').val(prevOfficerId + ";" + $(this).attr('officer_id'));
                            prevOfficerId = $('input[name=onulipi_officer_id_final]').val();
                            $('input[name=onulipi_officer_name_final]').val(prevOfficerNm + ";" + $(this).attr('officer_name'));
                            prevOfficerNm = $('input[name=onulipi_officer_name_final]').val();
                            $('input[name=onulipi_officer_email_final]').val(prevOfficerEm + ";" + $(this).attr('officer_email'));
                            prevOfficerEm = $('input[name=onulipi_officer_email_final]').val();
                            $('input[name=onulipi_office_head_final]').val(prevOfficerHd + ";" + $(this).attr('office_head'));
                            prevOfficerHd = $('input[name=onulipi_office_head_final]').val();
                        });
                    } else {
                        $('input[name=onulipi_group_id_final]').val('');
                        $('input[name=onulipi_group_name_final]').val('');
                        $('input[name=onulipi_group_designation_final]').val('');
                        $('input[name=onulipi_office_id_final]').val('');
                        $('input[name=onulipi_office_name_final]').val('');
                        $('input[name=onulipi_officer_designation_id_final]').val('');
                        $('input[name=onulipi_officer_designation_label_final]').val('');
                        $('input[name=onulipi_office_unit_id_final]').val('');
                        $('input[name=onulipi_office_unit_name_final]').val('');
                        $('input[name=onulipi_officer_id_final]').val('');
                        $('input[name=onulipi_officer_name_final]').val('');
                        $('input[name=onulipi_officer_email_final]').val('');
                        $('input[name=onulipi_office_head_final]').val('');
                        $('#cc_list_div').html("");
                        toastr.error("দুঃখিত! পত্রে অনুলিপি দেয়া যাবে না!");
                    }
                }

                if ($('#potrojariDraftForm').find('#office_organogram_id').length == 0) {
                    if ($('#potrojariDraftForm').find('#cc_list_div').length != 0) {
                        if (onulipis.length == 0) {
                            toastr.error("দুঃখিত! অনুলিপি/বিতরন দেয়া হয়নি");
                            return false;
                        }
                    }
                } else {
                    if (receivers.length == 0 && $('input[name=potro_type]').val() != 19) {
                        toastr.error("দুঃখিত! প্রাপক দেয়া হয়নি");
                        return false;
                    }
                }

                DRAFT_FORM.attached_files = [];
                $('.template-download').each(function () {
                    var link_td = $(this).find('.name');
                    var href = $(link_td).find('a').attr('href');
                    DRAFT_FORM.attached_files.push(href);
                });

                $("#uploaded_attachments").val(DRAFT_FORM.attached_files);
                $("#file_description").val($("#file_description_upload").val());
                var subject = $('#subject').text();
                var sender_sarok_no = $('#sharok_no').text();

                if ($('input[name=potro_type]').val() == 13) {
                    subject = $('#blank-subject').val();
                }

                if ($('input[name=potro_type]').val() == 13) {
                    sender_sarok_no = $('#blank-sarok').val();
                }

                $('input[name=sender_sarok_no]').val(sender_sarok_no);
                $('input[name=dak_subject]').val(subject);

                var temp = $('#template-body').html();

                if (temp.length == 0 || $('input[name=potro_type]').val() == '') {
                    toastr.error("দুঃখিত! পত্র দেয়া হয়নি");
                    return false;
                }

                if ($('input[name=dak_subject]').val().length == 0) {
                    $('input[name=dak_subject]').val($('#potro-type option:selected').text());
                }

                if ($('input[name=sender_sarok_no]').val().length == 0) {
                    toastr.error("দুঃখিত! স্মারক নম্বর দেয়া হয়নি");
                    return false;
                }

                //remove empty
                if ($('#reference').text().length == 0 || $('#reference').text() == '...') {
                    $('#reference').closest('.row').remove();
                }

                if ($('#office_ministry').text().length == 0 || $('#office_ministry').text() == '...') {
                    $('#office_ministry').prev().remove();
                    $('#office_ministry').remove();
                }

                if ($('#offices').text().length == 0 || $('#offices').text() == '...') {
                    $('#offices').prev().remove();
                    $('#offices').remove();
                }

                if ($('#unit_name_editable').text().length == 0 || $('#unit_name_editable').text() == '...') {
                    $('#unit_name_editable').prev().remove();
                    $('#unit_name_editable').remove();
                }

                if ($('#web_url_or_office_address').text().length == 0 || $('#web_url_or_office_address').text() == '...') {
                    $('#web_url_or_office_address').prev().remove();
                    $('#web_url_or_office_address').remove();
                }

                if ($('#office_address').text().length == 0 || $('#office_address').text() == '...') {
                    $('#office_address').prev().remove();
                    $('#office_address').remove();
                }

                if ($('#sender_phone').text().length == 0 || $('#sender_phone').text() == '...') {
                    $('#sender_phone').closest('div').remove();
                }

                if ($('#sender_fax').text().length == 0 || $('#sender_fax').text() == '...') {
                    $('#sender_fax').closest('div').remove();
                }

                if ($('#sender_email').text().length == 0 || $('#sender_email').text() == '...') {
                    $('#sender_email').closest('div').remove();
                }

                if ($('#potrojariDraftForm').find('#office_organogram_id').length > 0 && $('.receiver_users').length > 0) {
                    if ($('#potrojariDraftForm').find('#cc_list_div').length > 0) {
                        if ($('.onulipi_users').length == 0) {
                            $('#potrojariDraftForm').find('#cc_list_div').closest('.row').nextAll('.row').hide()
                            $('#potrojariDraftForm').find('#cc_list_div').closest('.row').hide();
                            $('#potrojariDraftForm').find('#sharok_no2').closest('.row').hide();
                            $('#potrojariDraftForm').find('#sending_date_2').closest('.row').hide();
                        }
                    }
                }

                $('#template-body').find('#pencil').remove();
                $.each($('#potrojariDraftForm').find('a'), function (i, v) {
                    if ($(this).hasClass('closethis') ||
                        $(this).hasClass('savethis') ||
                        $(this).attr('id') == 'sovadate' ||
                        $(this).attr('id') == 'sovatime' ||
                        $(this).attr('id') == 'sovaplace' ||
                        $(this).attr('id') == 'sovapresent' ||
                        $(this).attr('id') == 'subject') {
                    } else {
                        if ($.trim($(this).text()).length == 0 || $.trim($(this).text()) == '...') {
                            $(this).hide();
                            if($(this).attr('id')=='left_slogan' || $(this).attr('id')=='right_slogan'){
                                $(this).text('');
                            }
                        }
                    }
                });
                //remove empty end


                if($('[name=potro_type]').val() == 13){
                    var para1 = $('#note').html();
                    var para2 = $("#para1").html();
                    var para3 = $("#para2").html();
                    var para4 = $("#para3").html();
                    var para5 = $("#para4").html();
                    var contentbody = '<div style="padding:10px;"><div id="para1Data" style="word-wrap: break-word;">' + para1.replace(/\r?\n/g, '<br />') + '</div><br/><br/><div id="para2Data" style="float:left;width: 45%;word-wrap: break-word;">' + para2.replace(/\r?\n/g, '<br />') +
                        '</div><div id="para3Data" style="float: right;width: 45%;text-align: center;word-wrap: break-word;">' + para3.replace(/\r?\n/g, '<br />') + '</div> <div style="clear: both;"></div> <br/><div id="para4Data" style="clear:both;word-wrap: break-word;float:left;width: 45%;">' + para4.replace(/\r?\n/g, '<br />') +
                        '</div><div id="para5Data" style="float: right;width: 45%;text-align: center;word-wrap: break-word;">' + para5.replace(/\r?\n/g, '<br />') + '</div></div>';
                    $('#contentbody').text(contentbody);
                }else{
                    $.each($('#template-body').find('a.editable'), function (i, v) {
                        var dataType = $(this).attr('data-type');
                        var id = $(this).attr('id');
                        if(dataType=='textarea'){
                            var txt = $(this).html();
                        }else{
                            var txt = $(this).text();
                        }
                        if(id=="to_div_item") {
                            $(this).replaceWith("<span class='canedit' style='white-space:pre-wrap;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</span>");
                        }else {
                            $(this).replaceWith("<span class='canedit' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</span>");
                        }
                    });

                    var contentbody = $('#template-body').html();
                    $('#contentbody').text(contentbody);
                    $('#template-body').html(temp);
                }
            }
        };
    </script>

    <script>
        $(function () {
            $(document).on('click', '.mega-menu-dropdown .dropdown-menu', function (e) {
                e.stopPropagation();
            });

            $('select').select2();
            NothiMasterMovement.init('nothing');
            DakSetup.init();
            PotroJariFileUpload.init();

            var OfficeDataAdapter = new Bloodhound({
                datumTokenizer: function (d) {
                    return d.tokens;
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY&office_id=<?php echo $office_id; ?>',
                limit: 50,
            });
            OfficeDataAdapter.initialize();

            var OfficeDataAdapterRc = new Bloodhound({
                datumTokenizer: function (d) {
                    return d.tokens;
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: getBaseUrl() + 'officeManagement/autocompleteOfficeDesignation?search_key=%QUERY',
                limit: 50,
            });
            OfficeDataAdapterRc.initialize();

            var GroupDataAdapter = new Bloodhound({
                datumTokenizer: function (d) {
                    return d.tokens;
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: getBaseUrl() + 'PotrojariGroups/ajaxGetGroupList?search_key=%QUERY'
            });
            GroupDataAdapter.initialize();

            $('.typeahead_receiver_group')
                .typeahead({hint: true}, {
                        name: 'datypeahead_receiver_group',
                        displayKey: 'value',
                        limit: 20,
                        sufficient: 20,
                        source: GroupDataAdapter.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media" onclick="setGroupInfo({{id}},{{list}},\'receiver_\')">',
                                '<div class="media-body" >',
                                '<h4 style="font-size:12px;" class="media-heading" >{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', ongroupOpened)
                .on('typeahead:selected', ongroupAutocompleted)
                .on('typeahead:autocompleted', ongroupSelected);

            $('.typeahead_onulipi_group')
                .typeahead({hint: true}, {
                        name: 'datypeahead_onulipi_group',
                        displayKey: 'value',
                        limit: 20,
                        sufficient: 20,
                        source: GroupDataAdapter.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media"  onclick="setGroupInfo({{id}},{{list}},\'onulipi_\')">',
                                '<div class="media-body">',
                                '<h4 style="font-size:12px;" class="media-heading"  >{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', ongroupOpened)
                .on('typeahead:selected', ongroupAutocompleted)
                .on('typeahead:autocompleted', ongroupSelected);

            function ongroupOpened($e, datum) {

            }

            function ongroupSelected($e, datum) {

            }

            function ongroupAutocompleted($e, datum) {
            }

            $('.typeahead_sender')
                .typeahead(null, {
                        name: 'datypeahead_sender',
                        limit: 20,
                        sufficient: 20,
                        displayKey: 'value',
                        source: OfficeDataAdapter.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media">',
                                '<div class="media-body">',
                                '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);

            $('.typeahead_approval')
                .typeahead(null, {
                        name: 'datypeahead_approval',
                        limit: 20,
                        sufficient: 20,
                        displayKey: 'value',
                        source: OfficeDataAdapter.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media">',
                                '<div class="media-body">',
                                '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);

            $('.typeahead_receiver')
                .typeahead(null, {
                        name: 'datypeahead_receiver',
                        limit: 20,
                        sufficient: 20,
                        displayKey: 'value',
                        source: OfficeDataAdapterRc.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media">',
                                '<div class="media-body">',
                                '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);

            $('.typeahead_sovapoti')
                .typeahead(null, {
                        name: 'datypeahead_sovapoti',
                        limit: 20,
                        sufficient: 20,
                        displayKey: 'value',
                        source: OfficeDataAdapterRc.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media">',
                                '<div class="media-body">',
                                '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);

            $('.typeahead_onulipi')
                .typeahead(null, {
                        name: 'datypeahead_onulipi',
                        limit: 20,
                        sufficient: 20,
                        displayKey: 'value',
                        source: OfficeDataAdapterRc.ttAdapter(),
                        hint: (Metronic.isRTL() ? false : true),
                        templates: {
                            suggestion: Handlebars.compile([
                                '<div class="media">',
                                '<div class="media-body">',
                                '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                                '</div>',
                                '</div>',
                            ].join(''))
                        }
                    }
                )
                .on('typeahead:opened', onOpened)
                .on('typeahead:selected', onAutocompleted)
                .on('typeahead:autocompleted', onSelected);

            $('.typeahead_attension').typeahead(null, {
                    name: 'datypeahead_attension',
                    displayKey: 'value',
                    limit: 20,
                    sufficient: 20,
                    source: OfficeDataAdapterRc.ttAdapter(),
                    hint: (Metronic.isRTL() ? false : true),
                    templates: {
                        suggestion: Handlebars.compile([
                            '<div class="media">',
                            '<div class="media-body">',
                            '<h4 style="font-size:12px;" class="media-heading">{{value}}</h4>',
                            '</div>',
                            '</div>',
                        ].join(''))
                    }
                }
            )
            .on('typeahead:opened', onOpened)
            .on('typeahead:selected', onAutocompleted)
            .on('typeahead:autocompleted', onSelected);


            function onOpened($e, datum) {

            }

            function onSelected($e, datum) {

            }

            function onAutocompleted($e, datum) {
                DakOffice.setAutoDataIntoHiddenFields(datum, $e.currentTarget.id);
            }

            var list_rec = document.getElementById("receiver_ul");
            Sortable.create(list_rec, {
                animation: 150,
                onUpdate: function (evt) {
                    setInformation();
                }
            });

            var list_onu = document.getElementById("onulipi_ul");
            Sortable.create(list_onu, {
                animation: 150,
                onUpdate: function (evt) {
                    setInformation();
                }
            });
        });


    </script>

    <script>

        $(function () {
            $('#pencil').remove();
            $.each($('span.canedit'), function (i, v) {
                var id = $(this).attr('id');
                var txt = $(this).text();
                var dataType = $(this).attr('data-type');

                if (id == 'sending_date') {
                    $(this).replaceWith('<a data-placement="right"  data-pk="1" data-viewformat="dd.mm.yyyy" data-type="date" id="sending_date" href="#" class="editable editable-click">' + txt + '</a>');
                } else if (id == 'cc_div_item') {
                    $(this).replaceWith("<a data-type='text' data-pk='1' class='editable editable-click cc_list' href='javascript:;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</a>");
                }else if (id == 'to_div_item') {
                    $(this).replaceWith("<a data-type='text' data-pk='1' class='editable editable-click to_list' href='javascript:;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</a>");
                } else if (dataType == 'textarea') {
                    $(this).replaceWith("<a data-type='textarea' data-pk='1' class='editable editable-click' href='javascript:;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + $(this).html() + "</a>");
                }else {
                    $(this).replaceWith("<a data-type='text' data-pk='1' class='editable editable-click' href='javascript:;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</a>");
                }
            });
            $('#note').before('<a id="pencil" href="#"> <i class="fs1 a2i_gn_edit2"></i>[সম্পাদন  করুন] <br/></a>');

            $('.sovabox').hide();
            $('.sender_list').show();
            if ($('input[name=potro_type]').val() == 13) {
                $('.khalipotro').show();
                $('#blank-sarok').val('<?php echo $nothiMasterInfo['nothi_no'] ?>-<?php echo $totalPotrojari; ?>');
                $('#blank-subject').val("<?php echo htmlspecialchars(trim($draftVersion->potro_subject)); ?>");

                var para1 = $("#para1Data").html();
                var para2 = $("#para2Data").html();
                var para3 = $("#para3Data").html();
                var para4 = $("#para4Data").html();
                var para5 = $("#para5Data").html();
                $("#note").html(para1.replace(/<br\s*[\/]?>/gi, '\n'));
                $("#para1").html(para2.replace(/<br\s*[\/]?>/gi, '\n'));
                $("#para2").html(para3.replace(/<br\s*[\/]?>/gi, '\n'));
                $("#para3").html(para4.replace(/<br\s*[\/]?>/gi, '\n'));
                $("#para4").html(para5.replace(/<br\s*[\/]?>/gi, '\n'));

                $('#tmpbody').remove();
            } else if ($('input[name=potro_type]').val() == 17) {
                $('.sovabox').show();
                $('.sender_list').hide();
                $('.receiver_list').hide();
            } else if ($('input[name=potro_type]').val() == 19) {
                $('.attentionbox').hide();
            }
            else if ($('input[name=potro_type]').val() == 9) {
                //$('.sender_list').hide();
            } else {
                $('.khalipotro').hide();
            }
            PotrojariFormEditable.init(<?php echo ($heading['potrojari_head']) ?>, '<?php echo $nothiMasterInfo['nothi_no'] ?>', "<?php
                echo isset($potroInfo['sarok_no']) ? htmlspecialchars(trim($potroInfo['sarok_no']))
                    : ''
                ?>", <?php echo json_encode($employee_office) ?>, "<?php echo htmlspecialchars(trim($draftVersion->potro_subject)); ?>", '', '<?php echo $totalPotrojari; ?>','<?=$draftVersion->potrojari_language?>',<?php echo !empty($heading['write_unit'])?$heading['write_unit']:0 ?>);
        });

        $(document).on('click', '.remove_list', function () {
            $(this).closest('li').remove();
            setInformation();
        });

        $(document).on('change', '#potro-priority-level', setInformation);
        $(document).on('change', '#potro-security-level', setInformation);

    </script>

    <!-- End: JavaScript -->
    <!-- The blueimp Gallery widget -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
        <div class="slides">
        </div>
        <h3 class="title"></h3>
        <a class="prev">
            ÔøΩ </a>
        <a class="next">
            ÔøΩ </a>
        <a class="close white">
        </a>
        <a class="play-pause">
        </a>
        <ol class="indicator">
        </ol>
    </div>
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <script id="template-upload2" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload fade">
        <td>
        <span class="preview"></span>
        </td>
        <td>
        <p class="name">{%=file.name%}</p>
        <strong class="error label label-danger"></strong>
        </td>
        <td>
        <p class="size">প্রক্রিয়াকরন চলছে...</p>
        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
        </div>
        </td>
        <td>
        {% if (!i && !o.options.autoUpload) { %}
        <button class="btn blue start" disabled>
        <i class="fa fa-upload"></i>
        <span>আপলোড করুন</span>
        </button>
        {% } %}
        {% if (!i) { %}
        <button class="btn red cancel">
        <i class="fa fa-ban"></i>
        <span>বাতিল করুন</span>
        </button>
        {% } %}
        </td>
        </tr>
        {% } %}





    </script>
    <!-- The template to display files available for download -->
    <script id="template-download2" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade">
        <td>
        <span class="preview">
        {% if (file.thumbnailUrl) { %}
        <a href="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
        {% } %}
        </span>
        </td>
        <td>
        <p class="name">
        {% if (file.url) { %}
        <a href="<?= FILE_FOLDER ?>{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
        {% } else { %}
        <span>{%=file.name%}</span>
        {% } %}
        </p>
        {% if (file.error) { %}
        <div><span class="label label-danger">ত্রুটি:</span> {%=file.error%}</div>
        {% } %}
        </td>
        <td>
        <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
        {% if (file.deleteUrl) { %}
        <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
        <i class="fs1 a2i_gn_delete2"></i>
        <span>মুছে ফেলুন</span>
        </button>

        {% } else { %}
        <button class="btn yellow cancel btn-sm">
        <i class="fa fa-ban"></i>
        <span>বাতিল করুন</span>
        </button>
        {% } %}
        </td>
        </tr>
        {% } %}





    </script>

    <script>
        $(document).on('click', '.saveDraftNothi', function () {
            if (DRAFT_FORM.setform() != false) {
                var checkpromise = new Promise(function (resolve, reject) {

                    if($('#contentbody').text().length==0){
                        reject("দুঃখিত! পুনরায় সংশোধন করে সংরক্ষণ করুন।");
                    }else if($('[name=potro_type]').val() == 13){
                        resolve();
                    }else {
                        if ($('#sender_signature img').attr('src') == '' || typeof($('#sender_signature img').attr('src')) == 'undefined') {
                            reject("দুঃখিত! প্রেরক/অনুমোদনকারীর স্বাক্ষর লোড হয়নি। ")
                        }

                        else if ($('#sender_signature2').length > 0
                            && ($('#sender_signature2 img').attr('src') == '' ||
                                typeof($('#sender_signature2 img').attr('src')) == 'undefined')) {
                            reject("দুঃখিত! প্রেরক/অনুমোদনকারীর স্বাক্ষর লোড হয়নি। ")
                        }

                        else if ($('#sender_signature3').length > 0
                            && ($('#sender_signature3 img').attr('src') == '' ||
                                typeof($('#sender_signature3 img').attr('src')) == 'undefined')) {
                            reject("দুঃখিত! প্রেরক/অনুমোদনকারীর স্বাক্ষর লোড হয়নি। ")
                        }

                        else if ($('#sovapoti_signature').length > 0
                            && ($('#sovapoti_signature img').attr('src') == '' ||
                                typeof($('#sovapoti_signature img').attr('src')) == 'undefined')) {
                            reject("দুঃখিত! সভাপতির স্বাক্ষর লোড হয়নি। ")
                        }

                        else if ($('#sovapoti_signature2').length > 0
                            && ($('#sovapoti_signature2 img').attr('src') == '' ||
                                typeof($('#sovapoti_signature2 img').attr('src')) == 'undefined')) {
                            reject("দুঃখিত! সভাপতির স্বাক্ষর লোড হয়নি। ")
                        } else {
                            resolve();
                        }
                    }

                }).then(function () {
                    if ($('#approve').length > 0 && $('#approve').is(':checked')) {
                        $("#potrojariDraftForm").find('[name=can_potrojari]').val(1);
                        Metronic.blockUI({
                            target: '.page-container'
                        });
                        $.ajax({
                            url: '<?php
                                echo $this->Url->build(['controller' => 'Potrojari',
                                    'action' => 'PotrojariDraftUpdate', $draftVersion->id, $nothi_office]);
                                ?>',
                            data: $("#potrojariDraftForm").serialize(),
                            method: "post",
                            dataType: 'JSON',
                            cache: false,
                            success: function (response) {
                                if (response.status == 'error') {
                                    toastr.error(response.msg);
                                    Metronic.unblockUI('.page-container');
                                } else {
                                    toastr.success("খসড়া সংশোধিত হয়েছে");
                                    Metronic.unblockUI('.page-container');
                                    setTimeout(function () {
//                                        window.reload();
                                        window.location.reload();
                                    }, 1000);

                                }
                            },
                            error: function (xhr, status, errorThrown) {
                                Metronic.unblockUI('.page-container');
                            }
                        });
                    }
                    else {
                        $("#potrojariDraftForm").find('[name=can_potrojari]').val(0);
                        bootbox.dialog({
                            message: "আপনি কি অনুমোদন সংরক্ষণ করতে ইচ্ছুক?",
                            title: "সংরক্ষণ ",
                            buttons: {
                                success: {
                                    label: "হ্যাঁ",
                                    className: "green",
                                    callback: function () {
                                        Metronic.blockUI({
                                            target: '.page-container'
                                        });
                                        $.ajax({
                                            url: '<?php
                                                echo $this->Url->build(['controller' => 'Potrojari',
                                                    'action' => 'PotrojariDraftUpdate', $draftVersion->id, $nothi_office]);
                                                ?>',
                                            data: $("#potrojariDraftForm").serialize(),
                                            method: "post",
                                            dataType: 'JSON',
                                            cache: false,
                                            success: function (response) {
                                                if (response.status == 'error') {
                                                    toastr.error(response.msg);
                                                    Metronic.unblockUI('.page-container');
                                                } else {
                                                    toastr.success("খসড়া সংশোধিত হয়েছে");
                                                    Metronic.unblockUI('.page-container');

                                                    setTimeout(function () {
                                                        window.location.reload();
                                                    }, 1000);
                                                }
                                            },
                                            error: function (xhr, status, errorThrown) {
                                                Metronic.unblockUI('.page-container');
                                            }
                                        });
                                    }
                                },
                                danger: {
                                    label: "না",
                                    className: "red",
                                    callback: function () {
                                        return true;
                                    }
                                }
                            }
                        });

                    }
                }).catch(function (err) {
                    toastr.error(err);
                    Metronic.unblockUI('#ajax-content');
                });
            }
        });

        $(document).on('click', '.sendDraftNothi', function () {
            if (DRAFT_FORM.setform() != false) {
                var checkpromise = new Promise(function (resolve, reject) {

                    if($('#contentbody').text().length==0){
                        reject("দুঃখিত! পুনরায় সংশোধন করে সংরক্ষণ করুন।");
                    }else if($('[name=potro_type]').val() == 13){
                        resolve();
                    }else {
                        if ($('#sender_signature img').attr('src') == '' || typeof($('#sender_signature img').attr('src')) == 'undefined') {
                            reject("দুঃখিত! প্রেরক/অনুমোদনকারীর স্বাক্ষর লোড হয়নি। ")
                        }

                        else if ($('#sender_signature2').length > 0
                            && ($('#sender_signature2 img').attr('src') == '' ||
                            typeof($('#sender_signature2 img').attr('src')) == 'undefined')) {
                            reject("দুঃখিত! প্রেরক/অনুমোদনকারীর স্বাক্ষর লোড হয়নি। ")
                        }

                        else if ($('#sender_signature3').length > 0
                            && ($('#sender_signature3 img').attr('src') == '' ||
                            typeof($('#sender_signature3 img').attr('src')) == 'undefined')) {
                            reject("দুঃখিত! প্রেরক/অনুমোদনকারীর স্বাক্ষর লোড হয়নি। ")
                        }

                        else if ($('#sovapoti_signature').length > 0
                            && ($('#sovapoti_signature img').attr('src') == '' ||
                            typeof($('#sovapoti_signature img').attr('src')) == 'undefined')) {
                            reject("দুঃখিত! সভাপতির স্বাক্ষর লোড হয়নি। ")
                        }

                        else if ($('#sovapoti_signature2').length > 0
                            && ($('#sovapoti_signature2 img').attr('src') == '' ||
                            typeof($('#sovapoti_signature2 img').attr('src')) == 'undefined')) {
                            reject("দুঃখিত! সভাপতির স্বাক্ষর লোড হয়নি। ")
                        } else {
                            resolve();
                        }
                    }

                }).then(function () {
                    $.ajax({
                        url: '<?php
                            echo $this->Url->build(['controller' => 'Potrojari',
                                'action' => 'PotrojariDraftUpdate', $draftVersion->id, $nothi_office]);
                            ?>',
                        data: $("#potrojariDraftForm").serialize(),
                        method: "post",
                        dataType: 'JSON',
                        cache: false,
                        success: function (response) {
                            if (response.status == 'error') {
                                toastr.error(response.msg);
                            } else {
                                NothiMasterMovement.sendNothi();
                            }
                        },
                        error: function (xhr, status, errorThrown) {
                            toastr.error(xhr);
                        }
                    });
                }).catch(function (err) {
                    toastr.error(err);
                    Metronic.unblockUI('#ajax-content');
                });
            }
        });

    </script>
<?php endif; ?>

</div>
</div>

<div id="responsiveChangeLog" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">খসড়া পত্রের পরিবর্তনসমূহ</h4>
            </div>
            <div class="modal-body" style="background-color: #828282;">
                <div class="scroller" style="height:100%; max-height: 500px;" data-always-visible="1"
                     data-rail-visible1="1">

                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('assets/global/scripts/printThis.js'); ?>
<script>
    $(document).ready(function ($) {
        // delegate calls to data-toggle="lightbox"
        $('body').addClass('page-sidebar-closed');
        $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
            event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function () {

                },
                onNavigate: function (direction, itemIndex) {

                }
            });
        });
    });
    var PORTAL_GUARD_FORM = {
        submit: function () {

            $("#portalDownloadForm").attr('action', '<?php echo $this->Url->build(['_name'=>'guardFile']) ?>');
            if ($("#name-bng").val().length == 0) {
                toastr.error("দুঃখিত! গার্ড ফাইলের শিরোনাম দেয়া হয়নি।");
                return false;
            }

            if (isEmpty($("select#portal_guard_file_category_potro option:selected").val())) {
                toastr.error("দুঃখিত! গার্ড ফাইলের ধরন দেয়া হয়নি।");
                return false;
            }

            if ($("#portalDownloadForm input[name='uploaded_attachments']").val().length == 0) {
                toastr.error("দুঃখিত! গার্ড ফাইল খুজে পাওয়া যায়নি।");
                return false;
            }

            $(".submitbutton").attr('disabled', 'disabled');
            var name_bng = $("#name-bng").val();
            var guard_file_category_id = $("select#portal_guard_file_category_potro option:selected").val();
            var uploaded_attachments = $("#portalDownloadForm input[name='uploaded_attachments']").val();

            $('.WaitMsg').html('<img src="<?php echo CDN_PATH; ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; ডাউনলোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

            $.ajax({
                type: 'POST',
                url: "<?php
                    echo $this->Url->build(['_name' => 'guardFile'])
                    ?>" ,
                data: {
                    "office_id":<?= $nothi_office ?>,
                    "name_bng": name_bng,
                    "guard_file_category_id": guard_file_category_id,
                    "uploaded_attachments": uploaded_attachments,
                    "type": 'portal'},
                success: function (data) {
                    if (data.status == 'success') {
                        $('.WaitMsg').html('');
                        toastr.success("গার্ড ফাইল ডাউনলোড করা হয়েছে।");
                        $(".submitbutton").removeAttr( 'disabled' );
                        $('#portalDownloadModal').modal('hide');
                        $('#portalResponsiveModalPotro').modal('hide');
                        $('#responsiveModal').modal('hide');
                    } else {
                        $('.DownMsg').html('');
                        toastr.error("দুঃখিত! গার্ড ফাইল ডাউনলোড করা সম্ভব হচ্ছে না।");
                        $(".submitbutton").removeAttr( 'disabled' );
                    }
                }
            });

        }
    }
    $(function () {
        $('#prapto-potro').multiSelect({
            dblClick: true, afterSelect: function (values) {
                var title = "";
                var id = values;
                $('#responsiveOnuccedModal').find('.modal-title').text('');
                $('#responsiveOnuccedModal').find('.modal-body').html('');
                PROJAPOTI.ajaxSubmitDataCallback('<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + id + '/potro/' + <?= $draftVersion->nothi_master_id ?> +'?nothi_part='+'<?= $part_id ?>'+'&token='+'<?= sGenerateToken(['file' => $part_id ], ['exp' => time() + 60 * 300]) ?>', {'nothi_office':<?php echo $nothi_office ?>}, 'html', function (response) {
                    $('#responsiveOnuccedModal').modal('show');
                    $('#responsiveOnuccedModal').find('.modal-title').text(title);
                    $('#responsiveOnuccedModal').find('.modal-body').html(response);
                });
            }
        });

    })


    function getPopUpPotro(href, title) {
        $('#responsiveOnuccedModal').find('.modal-title').text('');
        $('#responsiveOnuccedModal').find('.modal-body').html('');
        $.ajax({
            url: '<?php echo $this->request->webroot; ?>NothiMasters/showPopUp/' + href+'?nothi_part='+'<?= $part_id ?>'+'&token='+'<?= sGenerateToken(['file' => $part_id ], ['exp' => time() + 60 * 300]) ?>',
            dataType: 'html',
            data: {'nothi_office':<?php echo $nothi_office ?>},
            type: 'post',
            success: function (response) {
                $('#responsiveOnuccedModal').modal('show');
                $('#responsiveOnuccedModal').find('.modal-title').text(title);

                $('#responsiveOnuccedModal').find('.modal-body').html(response);
            }
        });
    }

    $(document).off('click', '.showforPopup').on('click', '.showforPopup', function (e) {
        e.preventDefault();
        var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
        getPopUpPotro($(this).attr('href'), title);
    })


    $(document).on('click', '.btn-changelog', function () {
        $('#responsiveChangeLog').modal('show');
        $('#responsiveChangeLog').find('.scroller').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
        $.ajax({
            url: '<?php echo $this->Url->build(['controller' => 'Potrojari', 'action' => 'showChangeLog']) ?>',
            data: {
                nothi_master_id:<?php echo $nothimasterid ?>,
                potrojari_id:<?php echo $draftVersion->id ?>,
                nothi_office:<?php echo $nothi_office ?>},
            method: 'post',
            dataType: 'html',
            cache: false,
            success: function (response) {
                $('#responsiveChangeLog').find('.scroller').html(response);
            },
            error: function (err, status, rspn) {
                $('#responsiveChangeLog').find('.scroller').html('');
            }
        });
    });

    $(document).on('click', '.btn-print', function () {
        $('#template-body').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: true,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    });

    $(document).on('click', '.btn-printpreview', function () {
        $('#previewModal').find('.modal-title').text('');
        if ($('#note').attr('contenteditable') == "true") {
            $('#note').removeAttr('contenteditable');
            $('#pencil').html(' <i class="fs1 a2i_gn_edit2"></i> [সম্পাদন  করুন] <br/>');
            $('#note').froalaEditor('destroy');
        }

        var body = $('#template-body').html();
        $('#previewModal').modal('show');
        $('#previewModal').find('.modal-title').text('পত্রজারি প্রিন্ট প্রিভিউ');
        $('#previewModal').find('.showPreview').removeAttr('src');

        $(document).off('click', '.btn-pdf-margin').on('click', '.btn-pdf-margin', function () {
            $('#previewModal').find('.loading').show();
            $('#previewModal').find('.btn-pdf-download').removeAttr('href').addClass('hide');
            PROJAPOTI.ajaxSubmitAsyncDataCallback('<?= $this->Url->build(['controller' => 'Potrojari', 'action' => 'getPdfByBody']) ?>',
                {
                    name: '<?= $nothimasterid . '_draft_' . $nothi_office ?>',
                    body: body,
                    reset: true,
                    margin_top: parseFloat($('#marginform').find('#margin-top').val()),
                    margin_right: parseFloat($('#marginform').find('#margin-right').val()),
                    margin_bottom: parseFloat($('#marginform').find('#margin-bottom').val()),
                    margin_left: parseFloat($('#marginform').find('#margin-left').val()),
                    orientation: $('#marginform').find('#orientation').val(),
                }, 'json', function (response) {
                    if (response.status == 'success') {
                        $('#previewModal').find('.loading').hide();
                        $('#previewModal').find('.btn-pdf-download').attr('href', response.src).removeClass('hide');
                        $('#previewModal').find('.showPreview').attr('src', response.src);
                    } else {
                        $('#previewModal').find('.loading').hide();
                        $('#previewModal').find('.btn-pdf-download').removeAttr('href').addClass('hide');
                        toastr.error(response.msg);
                    }
                }
            );
        });

        $(document).off('click', '.btn-save-pdf-margin').on('click', '.btn-save-pdf-margin', function () {
            var obj = {
                margin_top: parseFloat($('#marginform').find('#margin-top').val()),
                margin_right: parseFloat($('#marginform').find('#margin-right').val()),
                margin_bottom: parseFloat($('#marginform').find('#margin-bottom').val()),
                margin_left: parseFloat($('#marginform').find('#margin-left').val()),
                orientation: $('#marginform').find('#orientation').val()
            };
            $('input[name=meta_data]').val(JSON.stringify(obj));

            toastr.success('মার্জিন যুক্ত করা হয়েছে')
            $('#previewModal').modal('hide');
        });
    });

</script>