<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i>পত্রজারি গ্রুপসমূহ</div>
        <div class="actions"><?=
                        $this->Html->link('<i class="fa fa-plus"></i> নতুন',
                            ['controller' => 'potrojariGroups', 'action' => 'groupMailAdd'],
                            ['class' => 'btn btn-success round-corner-5', 'escape' => false])
                        ?></div>
    </div>
	<script>
        function updateQueryStringParameter(uri, key, value) {
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            }
            else {
                return uri + separator + key + "=" + value;
            }
        }
		function pageRefreshWithPageLimit(pageLimit, searchString) {
            var fullURL = window.location.href;
            var newURL = updateQueryStringParameter(fullURL, 'limit', pageLimit);
            newURL = updateQueryStringParameter(newURL, 'search', searchString);
            window.location.href = newURL;
		}
	</script>
    <div class="portlet-body">
	    <div style="margin-bottom:5px;">
		    <div style="display: inline-block;">
			    <select id="pageLimit" onchange="pageRefreshWithPageLimit(this.value, $('#searchString').val())">
				    <option <?=$limit==5?'selected':''?> value="5"><?=enTobn(5)?> টি দেখাবে</option>
				    <option <?=$limit==10?'selected':''?> value="10"><?=enTobn(10)?> টি দেখাবে</option>
				    <option <?=$limit==20?'selected':''?> value="20"><?=enTobn(20)?> টি দেখাবে</option>
				    <option <?=$limit==50?'selected':''?> value="50"><?=enTobn(50)?> টি দেখাবে</option>
				    <option <?=$limit==100?'selected':''?> value="100"><?=enTobn(100)?> টি দেখাবে</option>
			    </select>
		    </div>
		    <div class="pull-right">
			    <div >
				    <input type="text" class="form-control round-corner-5" value="<?=$search?>" style="float:left;margin-right: 5px;width: 250px;height: 29px;" id="searchString" placeholder="গ্রুপের নাম দিয়ে খুঁজুন" onkeypress="if(event.keyCode == 13) if(this.value.length > 0) pageRefreshWithPageLimit($('#pageLimit').val(), this.value)" />
				    <button title="খুঁজুন" class="btn btn-success btn-sm round-corner-5" onclick="if(searchString.value.length > 0) pageRefreshWithPageLimit($('#pageLimit').val(), searchString.value)"><i class="fa fa-search"></i></button>
				    <button title="রিফ্রেশ" class="btn btn-danger btn-sm round-corner-5" onclick="pageRefreshWithPageLimit($('#pageLimit').val(), '')"><i class="fa fa-rotate-left"></i></button>
			    </div>
		    </div>
	    </div>
        <table class="table table-bordered font-lg">
            <thead>
                <tr>
                    <th class="text-center" >ক্রমিক নং</th>
                    <th class="text-center">গ্রুপের নাম</th>
                    <th class="text-center">গ্রুপে অন্তর্ভুক্ত ব্যক্তিবর্গ</th>
                    <th class="text-center">গ্রুপ তৈরিকারী</th>
                    <th class="text-center"><?php echo __("Actions"); ?></th>
                </tr>
            </thead>
            <tbody>

                <?php
                $i = (($page-1)*$limit)+1;
                $j=1;
                foreach ($all_created_groups as $rows):
                     $own = false;
                    if(  $user_info['office_id'] ==$rows['creator_office_id'] && $user_info['office_unit_id']== $rows['creator_unit_id'] && $user_info['office_unit_organogram_id'] == $rows['creator_office_unit_organogram_id'])
                    {
                        $own = true;
                    }
                    ?>
                    <tr>
                        <td class="text-center"><?php
                            $x    = $this->convertNumberToBengali($i++);
                            echo $x;
                            ?></td>
                        <td class="text-center"><?php echo h($rows['group_name']); ?></td>

                        <td class="text-left" style="padding: 0px">
                            <div class="panel panel-default" style="margin-bottom: 0px;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?= $j ?>" aria-expanded="false">
                                            <b>অন্তর্ভুক্ত ব্যক্তিবর্গ ( <?=$this->Number->format($rows['total_users'])?> )</b></a>
                                    </h4>
                                </div>
                                <div id="collapse_<?= $j ?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body" style="font-size : 12pt!important;">
			                            <?php
			                            $sl=1;
			                            if (!empty($group_users)) {
			                                foreach ($group_users as $k => $v) {
			                                    if ($rows['id'] == $v['group_id']) {
			                                        echo $this->convertNumberToBengali($sl++) . ') ';
			                                        if (!empty($v['employee_id'])) {
			                                            echo h($v['office_unit_organogram_name_bng']) . ", " . h($v['office_unit_name_bng']) . ", " . h($v['office_name_bng']) . "<br>";
			                                        } else {
			                                            if (!empty($v['employee_name_bng'])) {
			                                                echo h($v['employee_name_bng']) . ", " . h($v['office_unit_organogram_name_bng']) . ", " . h($v['office_unit_name_bng']) . ", " .h($v['office_name_bng']) . (!empty($v['officer_email']) ? (" (<b>" . h($v['officer_email']) .(!empty($v['officer_mobile'])?', '.h($v['officer_mobile']):''). " </b>)") : "") . "<br>";
			                                            } else {
			                                                echo h($v['office_unit_organogram_name_bng']) . ", " . (!empty($v['office_unit_name_bng']) ? h($v['office_unit_name_bng']) . ", " : '') . h($v['office_name_bng']) . (!empty($v['officer_email']) ? (" (<b>" . h($v['officer_email']) . " </b>)") : "") . "<br>";
			                                            }
			                                        }

			                                    }
			                                }
			                            }
			                            ?>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="text-center">
                            <?= (!empty($rows['creator_employee_id']))?h($creator_employee_info[$rows['creator_employee_id']]).', '.h($creator_office_unit_organogram_info[$rows['creator_office_unit_organogram_id']]).', '.h($creator_unit_info[$rows['creator_unit_id']]):'Super Admin' ?>
                        </td>
                        <td class="text-center">
	                        <div class="btn-group btn-group-round">
                            <?php
                            if($own == true || ($user_info['is_admin']==1 && $user_info['office_id'] == $rows['creator_office_id'])):
                            ?>
                            <?= $this->Html->link('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', ['action' => 'groupMailEdit', $rows->id],
                                ['class' => 'btn btn-primary btn-sm','escape' => false,'title'=>'সম্পাদনা' ])
                            ?>
                                
                            <button title="মুছুন" class="btn btn-danger btn-sm" onclick="groupMailDelete(<?= $rows->id ?>)"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            <?php
                            endif;
                            ?>
	                        </div>
                        </td>
    <?php
    $j++;
endforeach;
?>
                </tr>
            </tbody>
        </table>
	    <?=customPagination($this->Paginator)?>

    </div>
</div>
<!-- Modal -->
<div id="archiveModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">পত্রজারি গ্রুপ বাতিল </h4>
      </div>
      <div class="modal-body">
          <p class="font-lg">
          আপনি কি গ্রুপটি মুছে ফেলতে চান?
        </p>
        <input type="hidden" id="saveID">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="goforDelete()"> মুছে ফেলুন</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><?=__('Close') ?></button>
      </div>
    </div>

  </div>
</div>
<script>
    function groupMailDelete(id){
        $("#archiveModal").modal('toggle');
        $("#saveID").val(id);
    }
    function goforDelete(){
          var id = $("#saveID").val();
          $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'potrojariGroups', 'action' => 'groupMailDelete']) ?>",
                data: {"id" : id},
                success: function(data){
                            if(data == 'success'){
                                $("#saveID").val('');
                      toastr.success('মুছে ফেলা হয়েছে');
                      window.location.href = '<?= $this->Url->build(['controller' => 'potrojariGroups', 'action' => 'groupMail']) ?>';
                  }
                  else{
                      toastr.error('মুছে ফেলা সম্ভব হয়নি। কিছুক্ষণ পর আবার চেষ্টা করুন');
                  }
              }
        });
    }
</script>