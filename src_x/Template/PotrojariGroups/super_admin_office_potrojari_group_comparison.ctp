<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs0 a2i_ld_namjarikarjakrom2"></i>  স্তর ভিত্তিক অফিস - পত্রজারি গ্রুপ তুলনা   </div>
    </div>
    <div class="portlet-body">
        <?= $officeSelectionCell = $this->cell('OfficeSelectionByMinistry', ['entity' => '', 'prefix' => '']) ?>
        <div class="row">
            <div class="form-group">
                <div class="col-md-4 form-group form-horizontal">
                    <?php
                    echo $this->Form->input('potrojari_group_id',
                        array(
                            'label' =>'পত্রজারি গ্রুপ',
                            'class' => 'form-control',
                            'empty' => '--বাছাই করুন--',
                            'options' => $potrojari_groups_by_super_admin,
                        ));
                    ?>
                </div>

                <div class="col-md-4 form-group form-horizontal margin-top-20">
                    <button class="btn btn-primary btn-md round-corner-5" id="compare" style="display: none;"><i class="fa fa-file-text"></i><i class="fa fa-exchange" style="margin: 0 3px;"></i><i class="fa fa-file-text"></i> তুলনা করুন</button>
                </div>
            </div>
        </div>

        <?php echo $this->Form->end() ?>
        <br/>
        <div class="inbox-content">
            <div id="showlist"></div><br>
            <div class="table-container ">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                    <tr class="heading">
                        <th class="text-center" > অফিসের নাম </th>
                        <th class="text-center" > গ্রুপে অন্তর্ভুক্ত ব্যক্তিবর্গ</th>
                        <th class="text-center"> কার্যক্রম</th>
                    </tr>
                    </thead>
                    <tbody id="addData">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-purple" id="OfficeUserModal" tabindex="-1" aria-hidden="true" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->hidden('office_id_pj_group') ?>
                <?= $this->Form->hidden('id_pj_group') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary addUsersInPotrojariGroup round-corner-5"><?=__('Submit') ?></button>
                <button type="button" class="btn btn-danger round-corner-5" data-dismiss="modal"><?=__('Close') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script src="<?php echo CDN_PATH; ?>js/reports/super_admin_office_potrojari_group_comparison.js" type="text/javascript"></script>
<script>

</script>