<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 a2i_gn_details1"></i>টেমপ্লেটসমূহ</div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-12">
                    <div class="btn-group">
                        <?= $this->Html->link('নতুন টেমপ্লেট তৈরী করুন ', ['action' => 'add'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center">আইডি</th>
                <th class="text-center">টেমপ্লেটের নাম</th>
                <th class="text-center">স্ট্যাটাস</th>
                <th class="text-center">অ্যাকশন</th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach ($query as $rows):
            ?>
            <tr>
                <td class="text-center"><?php echo $rows['id']; ?></td>
                <td class="text-center"><?php echo $rows['template_name']; ?></td>
                <td class="text-center"><?php echo $rows['status']; ?></td>
                <td class="text-center">
                    <?= $this->Form->postLink(
                        'Delete',
                        ['action' => 'delete', $rows->id],
                        ['class' => 'btn btn-danger'],
                        ['confirm' => 'Are you sure to delete this item?'])
                    ?>
                    <?= $this->Html->link('Edit', ['action' => 'edit', $rows->id], ['class' => 'btn btn-primary']) ?>
                    <?= $this->Html->link('View', ['action' => 'view', $rows->id], ['class' => 'btn green']) ?>
                </td>
                <?php
                endforeach;
                ?>
            </tr>
            </tbody>
        </table>
    </div>
</div>
