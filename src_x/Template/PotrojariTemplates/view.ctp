<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class=""></i>View Template</div>
    </div>
    <div class="portlet-body">
		<table class="table table-bordered">
			<tr>
				<td class="text-right">Template Name</td>
				<td><?= h($potrojari_template->template_name) ?></td>
			</tr>
			<tr>
				<td class="text-right">Template</td>
				<td><?= ($potrojari_template->html_content) ?></td>
			</tr>
			<tr>
				<td class="text-right">Status</td>
				<td><?= h($potrojari_template->status) ?></td>
			</tr>
		</table>
    </div>
</div>
