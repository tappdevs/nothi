<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    প্রতিকল্প অবস্থা
                </div>
            </div>
            <div class="portlet-body protikolpo-status">
                <table class="table table-striped table-bordered table-hover">
                    <tr class="heading">
                        <th class="text-center">ক্রম</th>
                        <th class="text-center">প্রতিকল্পকারি ব্যক্তি</th>
                        <th class="text-center">প্রতিকল্পকৃত ব্যক্তি</th>
                        <th class="text-center">প্রতিকল্প শুরু</th>
                        <th class="text-center">প্রতিকল্প শেষ</th>
                    </tr>
                    <?php

                    use Cake\I18n\Number;
                    use Cake\I18n\Time;
                    use Cake\Routing\Route\Route;
                    use Cake\Routing\Router;

                    if(!empty($protikolpo)){

                        foreach($protikolpo as $key=>$value):
                    ?>
                         <tr>
                             <td class="text-center"><?= Number::format($key+1) ?></td>
                             <td><?= $value['employee_office_id_from_name'] ?></td>
                             <td><?= $value['employee_office_id_to_name'] ?></td>
                             <td class="text-center"><?= enTobn($value['protikolpo_start_date']->format("Y-m-d H:i")) ?></td>
                             <td class="text-center"><?= !empty($value['protikolpo_end_date'])?enTobn($value['protikolpo_end_date']->format("Y-m-d H:i")):'' ?></td>

                         </tr>
                    <?php
                        endforeach;;

                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>