<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
           Office Migration Details
        </div>
    </div>
    <div class="portlet-body">
        <div class="row" id="showlist">

        </div>
        <div class="row">
            <div class="table-container ">
                <table class="table table-bordered table-hover tableadvance">
                    <thead>
                        <tr class="heading">
                            <th class="text-center" rowspan="2">অফিসের নাম</th>
                            <th class="text-center" colspan="2">Permission</th>
                            <th class="text-center" colspan="2"> Migrated</th>
                            <th class="text-center" colspan="2">Current User</th>
                            <th class="text-center" colspan="2"> Migrated</th>
                        </tr>
                        <tr class="heading">
                            <th class="text-center">  Own Office</th>
                            <th class="text-center">  Other Office</th>
                            <th class="text-center">  Own Office</th>
                            <th class="text-center">  Other Office</th>
                            <th class="text-center">  Own Office</th>
                            <th class="text-center">  Other Office</th>
                            <th class="text-center">  Own Office</th>
                            <th class="text-center">  Other Office</th>
                        </tr>
                    </thead>
                    <tbody id ="addData">

                    </tbody>
                </table>
            </div>


            <!--Tools end -->


        </div>
        <br/>
        <div class="inbox-content">

        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function () {

        var office_ids = [<?php echo '"'.implode('","', array_keys($all_offices)).'"' ?>];
        var office_name = [<?php echo '"'.implode('","', array_values($all_offices)).'"' ?>];
        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        callOffices(0);
    var TotalOffices = 0;
    var premission_own_office = 0;
    var premission_other_office = 0;
    var premission_migrated_own_office = 0;
    var premission_migrated_other_office = 0;
    var current_own_office = 0;
    var current_other_office = 0;
    var current_migrated_own_office = 0;
    var current_migrated_other_office = 0;
        function callOffices(cnt) {
//        for(var i= 0; i < office_ids.length; i++){
//            console.log( office_ids.length);
//            console.log(cnt);
            if (cnt == office_ids.length) {
                $('#showlist').html('');
                showTotal();
                return;
            }
//     console.log(date_start);
//     console.log(date_end);

            $('#showlist').html('<img src="<?= CDN_PATH ?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে  <b>' + office_name[cnt] + '</b> । একটু অপেক্ষা করুন... </span><br>');
            var office_id = office_ids[cnt];
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(['controller' => 'Report', 'action' => 'checkMigrationStatusContent']) ?>",
                //                dataType: 'json',
                data: {"office_id": office_id},
                success: function (data) {
                    if (data.status == 1)
                    {
                        TotalOffices++;
                        current_own_office = current_own_office + data.current_own_office;
                        current_other_office = current_other_office + data.current_other_office;
                        current_migrated_own_office = current_migrated_own_office +data.current_migrated_own_office;
                        current_migrated_other_office = current_migrated_other_office + data.current_migrated_other_office;
                        premission_migrated_own_office = premission_migrated_own_office + data.premission_migrated_own_office;
                        premission_migrated_other_office = premission_migrated_other_office + data.premission_migrated_other_office;
                        premission_own_office = premission_own_office + data.premission_own_office;
                        premission_other_office = premission_other_office + data.premission_other_office;
                       class_ok = ' ';
                        if((data.premission_own_office != data.premission_migrated_own_office) || (data.premission_other_office != data.premission_migrated_other_office) || (data.current_own_office != data.current_migrated_own_office) || (data.current_other_office != data.current_migrated_other_office)){
                            class_ok = 'danger';
                        }
                        html = '<tr class="text-center '+class_ok+'">' +
                                '<td>' + office_name[cnt] + '</td>' +
                                ' <td>' + BntoEng(data.premission_own_office) + '</td>' +
                                '<td>' + BntoEng(data.premission_other_office) + '</td>' +
                                '<td>' + BntoEng(data.premission_migrated_own_office) + '</td>' +
                                '<td>' + BntoEng(data.premission_migrated_other_office) + '</td>' +
                                '<td>' + BntoEng(data.current_own_office) + '</td>' +
                                '<td>' + BntoEng(data.current_other_office) + '</td>' +
                                '<td>' + BntoEng(data.current_migrated_own_office) + '</td>' +
                                '<td>' + BntoEng(data.current_migrated_other_office) + '</td>' +
                                '</tr>';
                        $('#addData').append(html);
                    }
                    callOffices(cnt + 1);
                },
                error: function(){
                    callOffices(cnt + 1);
                }
            });
//                break;
//        }

        }
        function showTotal(){
            html = '<tr class="text-center bold">' +
                                '<td><b>মোট</b> (' + BntoEng(TotalOffices) + ')</td>' +
                                ' <td>' + BntoEng(premission_own_office) + '</td>' +
                                '<td>' + BntoEng(premission_other_office) + '</td>' +
                                '<td>' + BntoEng(premission_migrated_own_office) + '</td>' +
                                '<td>' + BntoEng(premission_migrated_other_office) + '</td>' +
                                '<td>' + BntoEng(current_own_office) + '</td>' +
                                '<td>' + BntoEng(current_other_office) + '</td>' +
                                '<td>' + BntoEng(current_migrated_own_office) + '</td>' +
                                '<td>' + BntoEng(current_migrated_other_office) + '</td>' +
                                '</tr>';
            $('#addData').append(html);
        }
    });


    function BntoEng(input) {
        var numbers = {
            0: '০',
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯'
        };

        var output = '';
        if (parseInt(input) != 0) {
            while (parseInt(input) != 0) {
                var n = parseInt(input % 10);
                if (!Number.isInteger(n))
                    break;
                input = input / 10;
                output += (numbers[n]);
            }
        } else
        {
            output = '০';
        }
        for (var i = output.length - 1, o = ''; i >= 0; i--)
        {
            o += output[i];
        }
        return o;

    }
</script>



