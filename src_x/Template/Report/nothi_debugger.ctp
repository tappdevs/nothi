<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            নথি মেসেজ ডিবাগার
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="debugger">Message:</label>
                    <textarea class="form-control" rows="5" id="debugger"></textarea>

                </div>
                <div class="form-group">
                    <button class="btn btn-info pull-right" onclick="getMessage()">Submit</button>
                    <br>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-4" style="vertical-align: middle">
                    Output:
                </div>
                <div class="col-md-10 col-sm-10 col-xs-8 well" id="output" style="padding: 20px;">

                </div>
            </div>
        </div>
    </div>
</div>

<script>
   function getMessage() {
       var msg = $('#debugger').val();
       PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'nothi-debugger',
           {'msg': msg}, 'json', function (response) {
               if (response.status == 'success') {
                   $('#output').text(response.data);
               }
               else {
                   toastr.error(response.msg);
               }
           });

   }
</script>