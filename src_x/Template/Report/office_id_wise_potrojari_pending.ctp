<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
           <span aria-hidden="true" class="icon-book-open"></span> অফিসভিত্তিক পত্রজারি পেন্ডিং
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
                <div class="col-md-4 col-sm-8 form-group form-horizontal">
                    <label class="control-label"> <?php echo __("Office").' '.__('ID') ?> </label>
                    <?php
                    echo $this->Form->input('office_id',
                        array(
                        'label' => false,
                        'class' => 'form-control',
                            'type' => 'text'
                    ));
                    ?>
                </div>
            <div class="col-md-4 col-sm-8 form-group form-horizontal margin-top-20">
                <button type="button" class="btn btn-md btn-primary" onclick="generatePendingReport()"> <?= __('Report').' ' . __('Create')?> </button>
            </div>

        </div>
        <div class="row" id="showlist_receiver">

        </div>
        <div class="row" id="showlist_onulipi">

        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function () {

        $(this).find('body').addClass('page-sidebar-closed');
        $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
    });
    function  generatePendingReport() {
         var office_id = $("#office-id").val();
        if(office_id == '' || office_id == null || typeof(office_id) == 'undefined'){
            toastr.error(' Office Id can not be empty ');
            return;
        }
        Metronic.blockUI({target: '.portlet-body', boxed: true});
        $('#showlist_receiver').html('');
        $('#showlist_showlist_onulipi').html('');
       
        $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'Report', 'action' => 'generatePotrojariReceiverPending']) ?>",
            data: {'office_id': office_id},
            success: function (data) {
                Metronic.unblockUI('.portlet-body');
                $('#showlist_receiver').html(data);
                loadOnulipi(office_id);
            },
            error: function () {
                Metronic.unblockUI('.portlet-body');
            }
        });
    }
    function loadOnulipi(office_id){
    Metronic.blockUI({target: '#showlist_onulipi', boxed: true});
    $.ajax({
            type: 'POST',
            url: "<?php echo $this->Url->build(['controller' => 'Report', 'action' => 'generatePotrojariOnulipiPending']) ?>",
            data: {'office_id': office_id},
            success: function (data) {
                 Metronic.unblockUI('#showlist_onulipi');
                $('#showlist_onulipi').html(data);
            },
            error: function () {
                 Metronic.unblockUI('#showlist_onulipi');
            }
        });
    }
</script>


