<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-tachometer"></i>রিপোর্ট ক্যাটাগরি
        </div>
        <div class="actions">
            <a onclick="addCategory()" class="btn btn-primary btn-sm round-corner-5"><i class="fs0 a2i_gn_add1"></i> অন্তর্ভুক্তি </a>
            <a onclick="editCategorySerial()" class="btn btn-danger btn-sm round-corner-5"><i class="fs0 a2i_gn_edit2"></i> ক্রম সংশোধন </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-container ">
            <table class="table table-bordered table-hover ">
                <thead>
                    <tr class="heading">
                        <th class="text-center" ><?=__('Sequence') ?></th>
                        <th class="text-center" > ক্যাটাগরি </th>
                        <th class="text-center" > অফিস সংখ্যা </th>
                        <th class="text-center" > রিপোর্ট ক্রম </th>
                        <th class="text-center" ><?= __('Actions') ?> </th>
                    </tr>
                </thead>
                <tbody>

        <?php
            if(!empty($reportCategoryList)){
                $indx = 0;
                foreach ($reportCategoryList as $data){
                    $office_list = jsonA($data['offices']);
        ?>
                    <tr id="row">
                        <td class="text-center"><?= enTobn($indx+1) ?> </td>
                        <td class="text-center"> <?= $data['name'] ?></td>
                        <td class="text-center"> <?= enTobn(count($office_list)) ?></td>
                        <td class="text-center cat_serial"><input class="category_serial" data-ref="<?=makeEncryptedData($data['id'])?>" type="text" value="<?= enTobn(isset($data['serial'])?$data['serial']:($indx+1)) ?>" style="min-width:10px;" placeholder="ক্রম বাংলা বা ইংলিশ হবে"></td>
                        <td class="text-center">
                            <a class="btn btn-link btn-sm" onclick="editCategory(<?=$data['id']?>,'<?= $data['name']?>')" data-toggle="tooltip" title="সম্পাদনা"><i class="fs1 a2i_gn_edit2"></i> </a>
                            <a class="btn btn-link btn-sm" onclick="deleteCategory(<?=$data['id']?>)"><i class="fs1 efile-delete3" data-toggle="tooltip" title="মুছুন"></i> </a>
                            <a class="btn btn-link btn-sm" href="<?= $this->Url->build('/report-category-office-mapping/'.$data['id'])?>"><i class="fs1 efile-head_of_wing2" data-toggle="tooltip" title="ক্যাটাগরি - অফিস ম্যাপিং"></i> </a>
                        </td>
                    </tr>
        <?php
                $indx++;
                }
            }else{
                echo '<tr class="danger text-center"><td colspan="5">'.__('Sorry no data found').'</td></tr>';
            }
        ?>
                </tbody>
            </table>
    </div>
        <div class="text-center">
            <a onclick="editCategorySerial()" class="btn btn-danger btn-sm round-corner-5"><i class="fs0 a2i_gn_edit2"></i> ক্রম সংশোধন </a>
        </div>
</div>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/report_category.js" type="text/javascript"></script>