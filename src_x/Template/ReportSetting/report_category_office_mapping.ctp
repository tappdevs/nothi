<style>
    fieldset.scheduler-border {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }

    legend.scheduler-border {
        font-size: 1.2em !important;
        font-weight: bold !important;
        text-align: left !important;
        width:auto;
        padding:0 10px;
        border-bottom:none;
    }
</style>
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption"><i class="fs1 efile-head_of_wing2"></i> ক্যাটাগরি - অফিস ম্যাপিং   </div>

        <div class="actions">
            <a href="<?=$this->Url->build('/report-category-list') ?>" class="btn btn-link btn-primary">
                রিপোর্ট ক্যাটাগরি
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row" id="searchPanel">
            <div class="">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">ক্যাটাগরি</legend>
                    <div class="">
                        <?= $this->Form->input('category_id',
                            array('label' => false, 'type' => 'select', 'class' => 'form-control',
                                  'options' =>  [0 => __("Select")]  + $allCategory,'default' => isset($reportCategoryEntity->id) ? $reportCategoryEntity->id : 0  ));
                        ?>
                    </div>
                </fieldset>
            </div>
            <div class="">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border"><?=__('Office').' '.__('Search')?></legend>
                    <div class="tabbable-custom ">
                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#layerWise" data-toggle="tab" aria-expanded="true">স্তরভিত্তিক</a>
                            </li>
                            <li class="">
                                <a href="#ministryWise" data-toggle="tab" aria-expanded="true">মন্ত্রণালয়/দপ্তর ভিত্তিক</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="layerWise">
                                <div class="row">
                                    <label class="control-label col-md-2 text-right font-lg"><?=__('Ministry')?></label>
                                    <div class="col-md-5">
                                        <?= $this->Form->input('ministry_id',
                                            array('label' => false, 'type' => 'select', 'class' => 'form-control',
                                                  'options' =>  [0 => __("Select")]  + $allMinistry));
                                        ?>
                                    </div>
                                    <label class="control-label col-md-1 text-right font-lg"><?=__('Layer')?></label>
                                    <div class="col-md-4">
                                        <?= $this->Form->input('layer_id',
                                            array('label' => false, 'type' => 'select', 'class' => 'form-control',
                                                  'options' =>  [0 => __("Select")]  + $options));
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="ministryWise">
                                <?= $this->Cell('OfficeSelection'); ?>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>


        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-5">
                    <div class="" id="office_list">

                    </div>
                </div>
                <div class="col-md-7">
                    <div class="row" id="office_mapping">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fs0 a2i_gn_details1"></i>ম্যাপিং অফিসসমূহ <badge class="badge badge-danger" id="office_count"><?= !empty($mappedOffices)?enTobn(count($mappedOffices)):0 ?></badge>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class=""  id="mapped_offices" style="">
                                    <?php
                                    if(!empty($mappedOffices)){
                                        $indx = 0;
                                        foreach ($mappedOffices as $key => $val){
                                            $indx++;
                                            echo '<div class="badge badge-primary selectedOffices" style="font-size: 11pt!important;margin-right: 2px;display: inline-block" data-office-id="'.$key.'"> '.$val.'
                        <span class="delete" onclick="unSetOffice($(this));"><i class="fa fa-times-circle"></i>  </span> 
                    </div>';
                                            if($indx % 3 == 0){
                                                echo '<br>';
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                                <hr>
                                <div class="text-center">
                                    <button type="button" class="btn btn-info" onclick="mapOfficesWithCategory()"><?=__('Submit')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/report_category_office_mapping.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
