<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-tachometer"></i>রিপোর্টের মার্ক বিন্যাস
        </div>
    </div>
    <div class="portlet-body form">
        <?= $this->Form->create($reportMarkingEntity, ['type' => 'post', 'class' => 'form-horizontal']); ?>
        <div class="form-body">

                <?php
                if(!empty($columns)){
                    foreach($columns as $col_index => $col_name){
                        if((($col_index+1) % 3) == 0){
                            echo '<div class="form-group">';
                        }
                ?>
                        <label class="col-md-2 col-sm-2 col-xs-6 control-label"><?= __($col_name) ?></label>

                        <div class="col-md-2 col-sm-2 col-xs-6 ">
                                <?=
                                $this->Form->input($col_name, ['label' => false, 'class' => 'form-control', 'type' => 'number',(in_array($col_name,$readOnly))?'readonly':'']);
                                ?>
                        </div>
                <?php
                        if((($col_index+1) % 3) == 0){
                            echo '</div>';
                        }
                    }
                }
                ?>

        </div>
        <div class="form-actions">
            <div class="text-center">
                <div class="well">
                    <span id="showSum" class="bold"></span></br>
                   রিপোর্টের সব গুলো ফিল্ড মিলিয়ে মোট ১০০% মার্ক তৈরি করতে হবে ।
                </div>
                <div class="text-center">
                    <button type="submit" class="btn   blue">সংরক্ষণ করুন</button>
                </div>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>
<script src="<?php echo CDN_PATH; ?>projapoti-nothi/js/report_distribution.js" type="text/javascript"></script>
