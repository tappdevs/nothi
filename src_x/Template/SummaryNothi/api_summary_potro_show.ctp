<link rel="stylesheet" type="text/css"
      href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>projapoti-nothi/css/styles.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.css"/>
<style>
    .portlet-body img {
        max-width: 100%;
        height: auto;
    }

    .potroview {
        margin: 0 auto;
    }

    div.potroAccordianPaginationBody {
        display: none;
    }

    div.potroAccordianPaginationBody.active {
        display: block;
    }

    a {
        cursor: pointer;
    }

    .editable {
        border: none;
        word-break: break-word;
    }

    #potrodraft_list #sovapoti_signature, #potrodraft_list #sender_signature, #potrodraft_list #sender_signature2, #potrodraft_list #sender_signature3 {
        visibility: hidden;
    }

    table {
        width: 100% !important;
    }

    .bangladate {
        border-bottom: 1px solid #000;
    }

    .btn-changelog, .btn-forward-nothi, .btn-nothiback, .btn-print, .btn-approve {
        padding: 3px 5px !important;
        border-width: 1px;
    }

    .editable {
        border: none !important;
        word-break: break-word;
        word-wrap: break-word
    }

    #sovapoti_signature, #sender_signature, #sender_signature2, #sender_signature3 {
        visibility: hidden;
    }

    #note {
        overflow: hidden;
        word-break: break-word;
        word-wrap: break-word;
        height: 100%;
    }
</style>
<style>
    .editable-click, a.editable-click {
        border: none;
        word-break: break-word;
        word-wrap: break-word
    }

    .cc_list {
        white-space: pre-wrap;
    }

    .popover-content {
        padding: 10px 30px;
    }

    .mega-menu-dropdown > .dropdown-menu {
        top: 10px !important;
    }
</style>

<div class="portlet box blue" style="min-width: 7.8in">
    <div class="portlet-title">
        <div class="caption">
            <?php echo "শাখা: " . $officeUnitsName . "; নথি নম্বর: " . $nothiRecord['nothi_no'] . '; বিষয়: ' . $nothiRecord['subject']; ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row ">
            <div class="col-md-12 " style="margin: 0 auto;">
                <?php // if ($privilige_type == 1) {  ?>
                <div class="form">

                    <?php echo $this->Form->create('',
                        array('onsubmit' => 'return false;', 'type' => 'file', 'id' => 'potrojariDraftForm')); ?>
                    <?php
                    echo $this->Form->hidden('id');
                    echo $this->Form->hidden('user_designation', ['value' => $employee_office['office_unit_organogram_id']]);
                    ?>

                    <div class="row form-group">
                        <div class="col-lg-12">

                            <div class="tabbable-custom nav-justified">
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active">
                                        <a href="#tab_1_1_1" data-toggle="tab">
                                            কভার পাতা</a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_1_2" data-toggle="tab">
                                            সার-সংক্ষেপ পাতা </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1_1_1">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="template-body"
                                                     style="max-width: 950px;min-height: 815px;    height: auto;    margin: 20px auto;    padding-bottom: 50px;    display: block;    background-color: rgb(255, 255, 255);    border: 2px solid #E6E6E6; <?php echo !empty($mainpotro)
                                                         ? "" : "display:none;" ?>">
                                                    <?php echo !empty($mainpotro)
                                                        ? $mainpotro->potro_cover : '' ?>
                                                </div>
                                            </div>
                                            <textarea id="contentbody" name="contentbody"
                                                      style="display: none"></textarea>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_1_1_2">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="template-body2"
                                                     style="max-width: 950px;min-height: 815px;    height: auto;    margin: 20px auto;    padding-bottom: 50px;    display: block;    background-color: rgb(255, 255, 255);    border: 2px solid #E6E6E6; <?php echo !empty($mainpotro)
                                                         ? "" : "display:none;" ?>">
                                                    <?php echo !empty($mainpotro)
                                                        ? $mainpotro->content_body : '' ?>
                                                </div>
                                            </div>
                                            <textarea id="contentbody2" name="contentbody2"
                                                      style="display: none"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                    <?php if ($privilige_type == 1 && $can_approve && $approved == 0): ?>
                    <div class="form-actions text-center">
                        <input type="checkbox" class="hide checkbox btn-approve" checked="checked"/>
                        <input type="button" class="btn btn-primary saveDraftNothi" value="<?php echo __(SAVE) ?>"/>
                    </div>
                </div>
            <?php endif; ?>

                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h3> <?php echo __(SHONGJUKTI) ?> </h3>

                            <!-- The table listing the files available for upload/download -->
                            <table role="presentation" class="table table-striped clearfix">
                                <tbody class="files">
                                <?php
                                if (isset($attachments) && count($attachments)
                                    > 0) {
                                    foreach ($attachments as $single_data) {
                                        if ($single_data['attachment_type']
                                            == 'text' || $single_data['attachment_type']
                                            == 'text/html') continue;

                                        $fileName = explode('/',
                                            $single_data['file_name']);
                                        $attachmentHeaders = get_file_type($single_data['file_name']);
                                        $value = array(
                                            'name' => urldecode($fileName[count($fileName)
                                            - 1]),
                                            'thumbnailUrl' => $this->request->webroot . 'content/' . $single_data['file_name'],
                                            'size' => '-',
                                            'type' => $attachmentHeaders,
                                            'url' => $this->request->webroot . 'content/' . $single_data['file_name']
                                        );

                                        echo '<tr class="template-download fade in">
    <td>
    <p class="name" style="width:80%">
    ' . (isset($value['name']) ? '<a href="' . $value['url'] . '" title="' . $value['name'] . '" download="' . $value['name'] . '" data-gallery="">' . $value['name'] . '</a>'
                                                : '') . '
    </p>
    </td>
    <td>
    ' . (isset($value['size']) ? '<span class="size">' . $value['size'] . '</span>' : '') . '
    </td>
  
    </tr>';
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<?php if ($privilige_type == 1): ?>
    <datalist id="selectReason">
        <option value="অনুমোদন করা হলো">অনুমোদন করা হলো</option>
    </datalist>

    <div id="modalSiddhanto" class="modal fade bs-modal-sm"  style="width: 350px; margin: 0 auto;" tabindex="-1" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">সিদ্ধান্ত গ্রহণ</h4>
                </div>
                <div class="modal-body">
                    <input type="text" id="siddhanto" list="selectReason" class="form-control"
                           placeholder="মাননীয় প্রধানমন্ত্রী আপনি কিছু লিখতে চান?">
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary saveDraftNothi" value="<?php echo __(SAVE) ?>"/>
                    <button type="button" data-dismiss="modal" class="btn  btn-danger">
                        বন্ধ করুন
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="responsivepotalaLog" class="modal fade" tabindex="-1" aria-hidden="true" data-backdrop="static" ata-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn btn-xs btn-danger pull-right" data-dismiss="modal" aria-hidden="true">বন্ধ করুন</button>
                    <h4 class="modal-title">সংলাগ</h4>
                </div>
                <div class="modal-body" style="background-color: #828282;">
                    <div class="scroller" style="height:100%;max-width: 500px" data-always-visible="1" data-rail-visible1="1">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo $this->request->webroot; ?>projapoti-nothi/js/nothi_master_movements.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/ui-toastr.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/global/plugins/select2/select2.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo CDN_PATH; ?>assets/global/plugins/jquery.mockjax.js"></script>
    <script src="<?php echo CDN_PATH; ?>assets/admin/pages/scripts/form-editable.js"></script>


    <script>
        var numbers = {
            1: '১',
            2: '২',
            3: '৩',
            4: '৪',
            5: '৫',
            6: '৬',
            7: '৭',
            8: '৮',
            9: '৯',
            0: '০'
        };

        function replaceNumbers(input) {
            var output = [];
            for (var i = 0; i < input.length; ++i) {
                if (numbers.hasOwnProperty(input[i])) {
                    output.push(numbers[input[i]]);
                } else {
                    output.push(input[i]);
                }
            }
            return output.join('');
        }

        var DRAFT_FORM = {
            setform: function () {

                if ($('#siddhanto').val().length == 0) {
                    $('#potrojariDraftForm').find('#ministry_note').remove();
                } else {
                    $('#template-body2').find('#ministry_note').replaceWith("<div id='ministry_note' >" + $('#siddhanto').val() + "</div>");
                }

                var templatebody = $('#template-body2').html();
                $('#contentbody2').text(templatebody);

                var templatebody = $('#template-body').html();
                $('#contentbody').text(templatebody);
            }
        };

        $(function () {
            NothiMasterMovement.init('nothing');
            $('select').select2();
            <?php if ($privilige_type == 1 && $can_approve && $approved == 0): ?>
            sayWhoCan();

            function sayWhoCan() {
                if ($('#potrojariDraftForm').find('#ministry_note').length == 0) {
                    $('#<?= $position_number ?>').before('<div class="col-md-12 col-xs-12 col-sm-12 text-center"> <a href="javascript:;" class="" id="ministry_note" data-type="textarea" data-pk="1" data-original-title="মাননীয় প্রধানমন্ত্রী আপনি কিছু লিখতে চান?" title="মাননীয় প্রধানমন্ত্রী আপনি কিছু লিখতে চান?" style="cursor: pointer">...</a></div>');
                }
            }

            $(document).off('click', '#ministry_note').on('click', '#ministry_note', function (e) {
                e.preventDefault();
                $('#modalSiddhanto').modal('show');
            });
            <?php endif; ?>
        });

        $(document).off('click', '.songlapbutton').on('click', '.songlapbutton', function () {
            $('#responsivepotalaLog').modal('show');
            $('#responsivepotalaLog').find('.scroller').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

            var id = $(this).attr('id');
            var url = $(this).data('url');
            var type = $(this).data('type');
            var officeid = parseInt($(this).data('nothi-office'));

            if(typeof(id)!='undefined'){
                $.ajax({
                    url: '<?php echo $this->Url->build(['controller' => 'NothiMasters', 'action' => 'showSonglap']) ?>/' + id + '/' + officeid,
                    method: 'post',
                    dataType: 'json',
                    cache: false,
                    success: function (response) {

                        var attachmenttype = response.attachment_type;
                        if (response.attachment_type == 'text') {
                            $('#responsivepotalaLog').find('.scroller').html('<div style="background-color: #fff; max-width:950px; min-height:815px; margin:0 auto; page-break-inside: auto;">' + response.potro_cover + "<br/>" + response.content_body + "</div>");
                        } else if ((attachmenttype.substring(0, 15)) == 'application/vnd' || (attachmenttype.substring(0, 15)) == 'application/ms') {
                            $('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + encodeURIComponent('<?php echo FILE_FOLDER ?>' + response.file_name) + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

                        } else if ((attachmenttype.substring(0, 5)) != 'image') {
                            $('#responsivepotalaLog').find('.scroller').html('<embed src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" style=" width:100%; height: 700px;" type="' + response.attachment_type + '"></embed>');
                        } else {
                            $('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a href="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" data-gallery="multiimages"  data-toggle="lightbox" data-title="পত্র নম্বর:  ' + response.nothi_potro_page_bn + '" data-footer="" ><img class="zoomimg img-responsive"  src="' + '<?php echo $this->request->webroot . 'content/' ?>' + response.file_name + '" alt="ছবি পাওয়া যায়নি"></a></div>');
                        }
                    },
                    error: function (err, status, rspn) {
                        $('#responsivepotalaLog').find('.scroller').html('');
                    }
                })
            }
            else if(typeof(url)!='undefined'){
                if ((type.substring(0, 15)) == 'application/vnd' || (type.substring(0, 15)) == 'application/ms') {
                    $('#responsivepotalaLog').find('.scroller').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url=' + url + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

                } else if ((type.substring(0, 5)) != 'image') {
                    $('#responsivepotalaLog').find('.scroller').html('<embed src="' + url + '" style=" width:100%; height: 700px;" type="' + type + '"></embed>');
                } else {
                    $('#responsivepotalaLog').find('.scroller').html('<div class="text-center"><a href="' + url + '" data-gallery="multiimages"  data-toggle="lightbox" data-title="সংলাগ" data-footer="" ><img class="zoomimg img-responsive"  src="' + url + '" alt="সংলাগ পাওয়া যায়নি"></a></div>');
                }
            }
        });

    </script>

    <!-- End: JavaScript -->
    <!-- The blueimp Gallery widget -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
        <div class="slides">
        </div>
        <h3 class="title"></h3>
        <a class="prev">
            ÔøΩ </a>
        <a class="next">
            ÔøΩ </a>
        <a class="close white">
        </a>
        <a class="play-pause">
        </a>
        <ol class="indicator">
        </ol>
    </div>
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <script id="template-upload" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload fade">

        <td>
        <p class="name">{%=file.name%}</p>
        <strong class="error label label-danger"></strong>
        </td>
        <td>
        <p class="size">প্রক্রিয়াকরন চলছে...</p>
        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
        </div>
        </td>
        <td>
        {% if (!i && !o.options.autoUpload) { %}
        <button class="btn blue start" disabled>
        <i class="fa fa-upload"></i>
        <span>আপলোড করুন</span>
        </button>
        {% } %}
        {% if (!i) { %}
        <button class="btn red cancel">
        <i class="fa fa-ban"></i>
        <span>বাতিল করুন</span>
        </button>
        {% } %}
        </td>
        </tr>
        {% } %}




    </script>
    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade">

        <td>
        <p class="name">
        {% if (file.url) { %}
        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
        {% } else { %}
        <span>{%=file.name%}</span>
        {% } %}
        </p>
        {% if (file.error) { %}
        <div><span class="label label-danger"> ত্রুটি: </span> {%=file.error%}</div>
        {% } %}
        </td>
        <td>
        <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>

        </tr>
        {% } %}




    </script>
    <script src="<?php echo CDN_PATH ?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
    <script>
        var deviceType = 'Android';
        function saveOnucced() {

            if ($('#potrojariDraftForm').find('#ministry_note').length > 0) {
                $.ajax({
                    url: '<?= $this->Url->build(['controller' => 'NothiNoteSheets',
                        'action' => 'apiNoteOnuccedAdd',$nothiRecord['id'],$nothi_office]) ?>',
                 data: {
                        id: (typeof ($('#potrojariDraftForm').find('#ministry_note').data("id")) != 'undefined' ? $('#potrojariDraftForm').find('#ministry_note').data("id") : 0),
                        user_designation:<?= $employee_office['office_unit_organogram_id'] ?>,
                        nothimasterid:<?= $nothiRecord['id'] ?>,
                        body: $('#potrojariDraftForm').find('#ministry_note').html(),
                        notesheetid:<?= $notesheedid['nothi_notesheet_id'] ?>,
                        notesubject: '',
                        nothi_office:<?= $nothi_office ?>,
                        notedecision: ''
                    },
                    type: 'POST',
                    async: false,
                    cache: false,
                    success: function (response) {
                        if (response > 0) {
                            $('#potrojariDraftForm').find('#ministry_note').attr("data-id", response);
                        }
                        if(typeof Android !== "undefined" && Android !== null) {
                            Android.showSendDialog();
                        }
                    }
                });
            }
            else {
                if(typeof Android !== "undefined" && Android !== null) {
                    Android.showSendDialog();
                }
            }
        }

        $(document).on('click', '.saveDraftNothi', function () {
            if(typeof Android !== "undefined" && Android !== null) {
                Android.showProgress();
            }
            var summarynothiapprovePromise = new Promise(function (resolve, reject) {
                $('#modalSiddhanto').modal('hide')
                var url = '<?php echo $this->Url->build(['controller' => 'SummaryNothi', 'action' => 'apiDraftApprovedByMinistry']) ?>/' + 1 + '/' +<?= $nothi_office ?>;

                $.ajax({
                    url: url,
                    method: "post",
                    data: {
                        'potro_id':<?php echo $mainpotro->nothi_potro_id ?>,
                        'nothi_part_no': <?= $mainpotro->nothi_part_no ?>,
                        'user_designation':<?= $employee_office['office_unit_organogram_id'] ?>
                    },
                    cache: false,
                    async: false,
                    dataType: 'JSON',
                    success: function (response) {
                        if (response.status == 'error') {
                            $('.btn-approve').closest('.checked').removeClass('checked');
                            $('.btn-approve').removeAttr('checked');
                            reject(response.msg)
                        } else {
                            if (response.data['will_sign'] == 1) {
                                $("#<?= $position_number ?>").find('img').show();
                            } else {
                                $("#<?= $position_number ?>").find('img').hide();
                            }
                            resolve();
                        }
                    },
                    error: function (xhr, status, errorThrown) {
                        reject(xhr)
                    }
                });
            });

            summarynothiapprovePromise.then(function () {
                return DRAFT_FORM.setform();
            }).then(function () {
                $.ajax({
                    url: '<?php echo $this->Url->build(['controller' => 'SummaryNothi',
                        'action' => 'apiDraftUpdateByMinistry', $nothimasterid, $nothipotroid, $nothi_office]) ?>//',
                    data: $("#potrojariDraftForm").serialize(),
                    method: "post",
                    dataType: 'JSON',
                    async: false,
                    cache: false,
                    success: function (response) {

                        if (response.status == 'error') {
                            toastr.error(response.msg);
                        } else {
                            saveOnucced();
                        }
                    },
                    error: function (xhr, status, errorThrown) {

                    }
                });
            }).catch(function (err) {
                toastr.error(err);
            });
        });
    </script>
<?php endif; ?>