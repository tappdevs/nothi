<ul class="page-breadcrumb breadcrumb"><!-- ADD CLASS hide for not showing breadcrumb -->
    <li>
        <a href="<?php echo $this->request->webroot; ?>">Home</a><i class="fa fa-circle"></i>
    </li>
    <li class="active">
        Users
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i>User Table
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-scrollable">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>
                            <?= $this->Paginator->sort('id') ?>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('username') ?>
                        </th>
                        <th>
                            Role
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach ($users as $user):
                        ?>
                        <tr>
                            <td>
                                <?php echo $this->Number->format($user->id) ?>
                            </td>
                            <td>
                                <?= h($user->username) ?>
                            </td>
                            <td>
                                <?= h($user->user_role['name']) ?>
                            </td>
                            <td class="actions">
                                <?= $this->Html->link(__('Edit'),
                                    ['action' => 'edit', $user->id]) ?>
    <?php if ($user->id != 1): ?><?= $this->Form->postLink(__('Delete'),
            ['action' => 'delete', $user->id],
            ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?><?php endif; ?>
                            </td>
                        </tr>
<?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="actions text-center">
            <ul class="pagination pagination-sm">
                <?php
                echo $this->Paginator->first(__('প্রথম', true),
                    array('class' => 'number-first'));
                echo $this->Paginator->prev('<<',
                    array('tag' => 'li', 'escape' => false),
                    '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
                    array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li',
                    'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a',
                    'reverse' => FALSE, 'modulus' => 10));
                echo $this->Paginator->next('>>',
                    array('tag' => 'li', 'escape' => false),
                    '<a href="#" class="btn btn-sm blue">&laquo;</a>',
                    array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li',
                    'escape' => false));

                echo $this->Paginator->last(__('শেষ', true),
                    array('class' => 'number-last'));
                ?>
            </ul>
        </div>
    </div>
</div>

