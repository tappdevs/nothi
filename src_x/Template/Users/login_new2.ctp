<!-- BEGIN LOGIN FORM -->

<noscript>
    <div id="Script-disable-div" class="alert alert-danger">দুঃখিত, আপনার ব্রাউজার জাভাস্ক্রিপ্ট সমর্থন করে না! অনুগ্রহপুর্বক জাভাস্ক্রিপ্ট চালু করুন অথবা ব্রাউজার আপডেট করুন।
    </div>
</noscript>
<!--Need To add nothi-custom-->
<link href="<?php echo CDN_PATH; ?>assets/global/css/nothi-custom.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CDN_PATH; ?>assets/global/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo CDN_PATH; ?>assets/global/scripts/nothi-custom.js?v=<?= js_css_version ?>" type="text/javascript"></script>
<style>
    .tabbable-custom > .nav-tabs > li {
        margin-right: 2px;
        border-top: 2px solid #eeeeee;
        border-right: 1px solid #dddddd;
    }
    .tabbable-custom > .nav-tabs > li:first-child {
        border-left: 1px solid #dddddd;
    }
    .tabbable-custom > .nav-tabs > li:last-child {
        margin-right: 0px;
    }
    .tab-pane{
        padding-top:10pt;
    }

    .tabbable-custom > .nav-tabs > li.active{
        border-top: 3px solid #F3565D;
        border-left: 2px solid #F3565D;
        border-right: 2px solid #F3565D;
        border-bottom:0px;
    }

    .tabbable-custom > .nav-tabs > li:first-child.active{
        border-left: 0px solid #F3565D;
    }
    .tabbable-custom > .nav-tabs > li:last-child.active{
        border-right: 0px solid #F3565D;
    }

    input {
        width: 100% !important;
        padding: 5px !important;
        border: 1px solid #CACACA!important;
        /* border: none #a0a9b4 !important; */
        /* border-bottom: 1px solid !important; */
        color: #000000 !important;
        font-size: 13px !important;
        margin-bottom: 20px !important;
    }
    .inputWithIcon{
        position:relative;
    }

    .inputWithIcon .efile-view1{
        position:absolute;
        right:14px;
        top:0px;
        margin:9px 8px;
        color:#aaa;
        transition:.3s;
        cursor: pointer;
        background-color: white;
    }
     input[name=password]{
        padding-right: 25px!important;
    }
</style>
<?php
$path = $this->request->webroot;
if (defined('CDN') && CDN == 1) {
    $path = 'http://cdn1.nothi.gov.bd/webroot/';
}
?>
<div class="alert alert-danger" id="GChrome" style="display: none;">
    অনুগ্রহপূর্বক নথি অ্যাপ্লিকেশনের সকল কার্যক্রম উপভোগ করতে Google Chrome ব্যবহার করুন। Google Chrome ইন্সটল না থাকলে
    <a target="_blank" href="https://www.google.com/chrome/"> এখানে ক্লিক করুন </a>।
</div>
<div id="form-div">
    <div class="tabbable-custom nav-justified" style="background: #FFFFFF!important">
        <ul class="nav nav-tabs nav-justified">
            <li class="active">
                <a href="#tab_loginname" data-toggle="tab">ইউজার আইডি</a>
            </li>
            <li>
                <a href="#tab_userid" data-toggle="tab">ইউজার নেম</a>
            </li>
            <li>
                <a href="#tab_password" data-toggle="tab">পাসওয়ার্ড <?= __("Reset") ?></a>
            </li>
        </ul>

        <div class="tab-content">
            <!-- >ইউজার আইডি TAB -->
            <div class="tab-pane active" id="tab_loginname">
                <?= $this->Form->create(null, ['class' => 'login-form', 'url' => ['_name' => 'login'], 'autocomplete' => 'off']) ?>
                <?= $this->Form->hidden('fingerprint', ['id' => 'fingerprint']) ?>
                <div class="row">
                    <div class="col-xs-6">
                        <?= $this->Form->input('username', ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'ইউজার আইডি', 'label' => false]) ?>
                    </div>
                    <div class="col-xs-6 inputWithIcon">
                        <?= $this->Form->input('password', ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'পাসওয়ার্ড', 'label' => false, 'type' => 'password']) ?><span class="efile-view1 "></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6"><button type="button" class="btn btn-sm btn-danger" onclick="clearCache()"><i class="fs0 efile-history3"></i>ব্রাউজার ক্যাশ মুছুন</button></div>
                    <div class="col-xs-6 text-right">
                        <button class="btn btn-sm blue" type="submit">প্রবেশ <i class="fs0 efile-login2"></i></button>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
            <!-- END >ইউজার আইডি TAB -->
            <!-- ইউজার নেম TAB -->
            <div class="tab-pane" id="tab_userid">
                <?= $this->Form->create(null, ['class' => 'login-form', 'url' => ['_name' => 'login']]) ?>
                <div class="row">
                    <div class="col-xs-6">
                        <?= $this->Form->input('alias', ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'ইউজার নেম', 'label' => false]) ?>
                    </div>
                    <div class="col-xs-6 inputWithIcon">
                        <?= $this->Form->input('password', ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'পাসওয়ার্ড', 'id' => 'password2', 'label' => false, 'type' => 'password']) ?><span class="efile-view1 "></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <button class="btn btn-sm blue" type="submit">প্রবেশ <i class="fs0 efile-login2"></i></button>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
            <!-- END ইউজার নেম TAB -->
            <!-- পাসওয়ার্ড ভুলে গেছেন TAB -->
            <div class="tab-pane" id="tab_password">
                <?= $this->Form->create(null, ['class' => 'login-form forget-form', 'action' => 'javascript:;']) ?>
                <div class="row">
                    <div class="col-xs-6">
                        <input type="text" placeholder="ইউজার আইডি" class="form-control"
                               autocomplete="off" name="username" autocomplete='off'/></div>
                    <div class="col-xs-6">
                        <input type="text" placeholder="ইমেইল" autocomplete='off' class="form-control"
                               name="email"/></div>
                </div>
                <div class="row">

                    <div class="col-xs-12 text-right">
                        <button class="btn btn-sm green" type="button">অনুরোধ করুন <i class="fs1 a2i_gn_send1"></i>
                        </button>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
            <!-- END পাসওয়ার্ড ভুলে গেছেন TAB -->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-6 text-left">
            <a href="#" onclick="showhelpdeskPanel()" class="btn btn-link"> হেল্প ডেস্ক <i class="glyphicon glyphicon-chevron-up icon-changer"></i></a>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
            <a href="<?php
            echo $this->Url->build(['controller' => 'DakNagoriks',
                                    'action' => 'NagorikAbedon'])
                ?>" class="btn btn-link" style="text-decoration: none;"><i class="efile-citizen1"></i>&nbsp;নাগরিক কর্নার&nbsp;</a>
        </div>
    </div>
</div>
<div class="create-help helpdeskPanel" style="
    display: block;
    position: absolute;
    background: rgb(255, 255, 255);
    padding: 10px;
    z-index: 100;
    width: 470px;
    border-top: 1px solid rgb(85, 119, 119);
">
    <div class="row">
        <div class="col-sm-7 col-md-7 col-xs-7">
            <div class="form-group ">
                <i class="glyphicon glyphicon-phone"></i> +৮৮ ০১৭৫২-০২৬৫২১
                <br/>
                <i class="glyphicon glyphicon-phone"></i> +৮৮ ০১৭১২-৪৮৫৭৪৭
                <br/>
                <span class="glyphicon glyphicon-envelope"></span> <a href="mailto:support@nothi.org.bd" style="
font-size: 14px">support@nothi.org.bd</a><br/>
                <img src="<?= $this->request->webroot ?>img/fb.png" style="margin-left:-2px;"/>
                <a target="__tab" class="btn-link bold font-md" style="text-decoration: none;"
                   href="https://fb.com/groups/nothi/">নথি (ফেসবুক গ্রুপ)</a><br/>
                <small style=" vertical-align: bottom!important;">
                    <img src="<?= $this->request->webroot ?>img/img_tapp_logo.png" style="height: 18px;">
                    কারিগরি সহায়তায় <a style="vertical-align: bottom!important;" href="http://tappware.com" target="_tab">ট্যাপওয়ার</a>
                </small>
            </div>
        </div>
        <div class="col-sm-5 col-md-5 col-xs-5">
            <div class="form-group ">
                <icon class="glyphicon glyphicon-info-sign"></icon>
                <a target="__tab" class="btn-link bold font-md" style="text-decoration: none;"
                   href="<?= $this->request->webroot ?>FAQ">আপনার জিজ্ঞাসা</a><br/>
                <icon class="glyphicon glyphicon-bullhorn"></icon>
                <a target="__tab" class="btn-link bold font-md" style="text-decoration: none;"
                   href="<?= \Cake\Routing\Router::url(['_name' => 'release-note']); ?>">আপডেট </a><br/>
                <icon class="glyphicon glyphicon-book"></icon>
                <a target="__tab" class="btn-link bold font-md" href="<?= $this->Url->build(['_name' => 'showUserManualList']) ?>"> ব্যবহার সহায়িকা (ভার্সনঃ ১৬)</a><br/>
                <icon class="glyphicon glyphicon-play-circle"></icon>
                <a class="btn-link bold font-md" style="text-decoration: none;" href="#" id="dropdown_video" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">টিউটোরিয়াল <span class="caret"></span></a><br/>

	            <ul class="dropdown-menu">
		            <li><a href="#" data-toggle="modal" data-target="#NothiVideoTutorial">ভিডিও টিউটোরিয়াল</a></li>
		            <li class="dropdown-submenu">
			            <a href="#">ই নথি অনলাইন কোর্স</a>
			            <ul class="dropdown-menu">
				            <li><a href="http://www.muktopaath.gov.bd/#/elPortal/showSignUpPage?role=student&isStudent=true" target="_blank">কোর্সে নিবন্ধন</a></li>
				            <li><a href="http://www.muktopaath.gov.bd/login/goToHomePage#/elM2Portal/showCourseDetails?courseId=276" target="_blank">কোর্সে শুরু করুন</a></li>
			            </ul>
		            </li>

	            </ul>


            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div id="NothiVideoTutorial" class="modal modal-purple" aria-hidden="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header"><button data-dismiss="modal">×</button><h4 style="color: white;font-size: 22px !important;margin-top: 0px;">নথির ভিডিও টিউটোরিয়াল</h4></div>
			<div class="modal-body">
				<div class="panel-group accordion" id="accordion1">
				<?php foreach ($video_tutorial_links as $key => $video_tutorial_link): ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?=$key?>">
								<img src="https://img.youtube.com/vi/<?=$video_tutorial_link['id']?>/2.jpg" style="width: 50px;" />
								<?=$video_tutorial_link['name']?>
							</a>
						</h4>
					</div>
					<div id="collapse_<?=$key?>" class="panel-collapse collapse">
						<div class="panel-body" style="font-size : 12pt!important;">
							<iframe style="width:100%;height:calc(100vh - 204px)" src="<?=$video_tutorial_link['url']?>" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
				</div>

			</div>
			<div class="modal-footer">
				<div class="btn-group btn-group-round"><span class="btn red" data-dismiss="modal">বন্ধ করুন</span>
				</div>
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" href="<?= $this->request->webroot ?>css/persistant_header.css">
<script src="//wmh.github.io/jquery-scrollbox/jquery.scrollbox.js"></script>
<style>
	.floatingHeader {
		position: absolute;
		top: 0;
		visibility: hidden;
		padding: 12px 5px;
	}
</style>
<div style="font-size:9.5pt!important;height: 270px;overflow: hidden;" id="page-wrap" class="scroll-text">
	<ul>
    <?php
    if (!empty($office_messages['notice'])) {
        foreach ($office_messages['notice'] as $officeNotices) {
        	echo '<li class="some-other-area persist-area">';
            echo '<h3 class="persist-header">'.$officeNotices["title"].'</h3>';
			echo '<p>'.$officeNotices["description"].'</p>';
            echo '</li>';
        }
    }
    ?>
	</ul>
</div>
<script type="text/javascript">
    function UpdateTableHeaders() {
        $(".persist-area").each(function() {
            var el = $(this),
                offset         = el.offset(),
                scrollTop      = $("#page-wrap").scrollTop(),
                floatingHeader = $(".floatingHeader", this)

            if (offset.top <= $("#page-wrap").offset().top + 0) {
                floatingHeader.css({
                    "visibility": "visible",
                    "top" : $("#page-wrap").offset().top
                });
            } else {
                floatingHeader.css({
                    "visibility": "hidden"
                });
            };
        });
    }
    // DOM Ready
    $(function() {
        var clonedHeaderRow;
        $(".persist-area").each(function() {
            clonedHeaderRow = $(".persist-header", this);
            clonedHeaderRow
                .before(clonedHeaderRow.clone())
                .css("width", clonedHeaderRow.width() + 10)
                .addClass("floatingHeader");
        });

        $("#page-wrap")
            .scroll(UpdateTableHeaders)
            .trigger("scroll");
    });

    $('#page-wrap').scrollbox({
        linear: true,
        step: 1,
        delay: 0,
        speed: 20
    });
    setTimeout(function(){ showhelpdeskPanel(); }, 1000);

    function videoModal() {
        var modalId = 'NothiVideoTutorial';
        var header = 'নথির ব্যবহার সহায়িকা';
        var content = '<iframe style="width:100%;height:calc(100vh - 204px)" src="https://www.youtube.com/embed/hmUUBt7G8Gs?rel=0&list=PLVCEN663EzMFwATrFC6ULiM64U1FRyeAu" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
        doModal(modalId, header, content, '', '');
        $('#'+modalId).modal('show');
    }
</script>

<script src="<?php echo CDN_PATH; ?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/npm/jquery.marquee@1.5.0/jquery.marquee.min.js" type="text/javascript"></script>
<script>

	function pad(str, max) {
		str = str.toString();
		return str.length < max ? pad("0" + str, max) : str;
	}

	$(function () {
		$('.forget-form').show()
		$('.marquee').marquee({
			speed: 50,
			gap: 50,
			delayBeforeStart: 500,
			direction: 'up',
			pauseOnHover: true
		});
	})

	function showhelpdeskPanel() {
		$('.helpdeskPanel').toggle(500);
        $('.icon-changer').toggleClass("glyphicon-chevron-up glyphicon-chevron-down");
	}

	$(document).on('change', '[name=username]', function () {
		var loginid = $(this).val();
		var start = loginid.substr(0, 1);
		var restof = loginid.substr(1);
		loginid = start + pad(restof, 11);
		$(this).val(loginid);
	});

	var client = new ClientJS();
	var fingerprint = client.getFingerprint(); // Calculate Device/Browser
	if (!client.isChrome()) {
		$("#GChrome").show();
		$(".GChrome").remove();
	} else {
		$(".GChrome").show();
	}
	$("#fingerprint").val(fingerprint);

	function clearCache(){
//	    window.location.href = 'chrome://settings/clearBrowserData';
//	    return;
//        chrome://settings/clearBrowserData
//        if((client.isChrome())){
//            url = "chrome.tabs.create({url:'chrome://settings/clearBrowserData'});";
//            url = '<a href="'+url+'" onclick="'+url+'"> ক্লিক করুন। </a>';
//        }else{
            url = '<code>SHIFT</code> + <code>CTRL</code> + <code>DELETE</code> বাটন একসাথে প্রেস করুন এবং ব্রাউজার কর্তৃক পরবর্তী নির্দেশনা অনুসরণ করুন।';
//        }
		bootbox.dialog({
			message: "<h4>প্রিয় ব্যবহারকারী,<br/>ব্রাউজার ক্যাশ মুছে ফেলতে "+url+"</h4>",
			title: "ব্রাউজার ক্যাশ মুছে ফেলুন",
			buttons: {
				danger: {
					label: "বন্ধ করুন",
					className: "red",
					callback: function () {

					}
				}
			}
		});
    }
    $(".efile-view1").click(function () {
        passwordView();
    });
    function passwordView() {
        $("input[name=password]").each(function(key, value) {
            if ($(value).attr('type') == 'password') {
                $(value).attr('type', 'text');
                $(".efile-view1").css('color', 'black');
            } else {
                $(value).attr('type', 'password');
                $(".efile-view1").css('color', '#aaa');
            }
        });
    }
</script>