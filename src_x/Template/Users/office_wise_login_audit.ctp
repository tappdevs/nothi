<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            কর্মকর্তার লগইন বর্ণনা
        </div>

    </div>
    <div class="portlet-body">
        <?= $cell = $this->cell('OfficeSelection', ['entity' => '']) ?>
        <div class="row">

            <div class="col-sm-4 form-group form-horizontal">
                <?php
                    echo $this->Form->input('office_id', array(
                        'label' => __('Office'),
                        'class' => 'form-control',
                        'empty' => '--বাছাই করুন--'
                    ));

                ?>
            </div>
            <div class="col-sm-4 form-group form-horizontal">
                <?php
                echo $this->Form->input('office_unit_id', array(
                    'label' => __('Unit'),
                    'class' => 'form-control',
                    'empty' => '--বাছাই করুন--'
                ));
                ?>
            </div>
            <div class="col-sm-4 form-group form-horizontal">
                <?php
                echo $this->Form->input('office_unit_organogram_id', array(
                    'label' => __('Organogra'),
                    'class' => 'form-control',
                    'empty' => '--বাছাই করুন--'
                ));
                ?>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 form-group form-horizontal">
                <div class="hidden-print ">
                        <div id="dashboard-report-range" class=" tooltips btn btn-sm btn-default"
                             data-container="body"
                             data-placement="bottom" data-original-title="তারিখ নির্বাচন করুন">
                            <i class="icon-calendar"></i>&nbsp; <span
                                class="thin uppercase block">তারিখ নির্বাচন করুন</span>&nbsp;
                            <i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                    </div>
            </div>
        </div>


        <div class="table-scrollable showHistory">

        </div>

    </div>
</div>


<script src="<?php echo CDN_PATH; ?>daptorik_preview/js/report_date_range.js"></script>
<script src="<?php echo CDN_PATH; ?>js/reports/office_wise_login_audit.js?v=<?=js_css_version?>"></script>


