<table class="table table-bordered table-advance table-hover">
    <thead>
        <tr>
            <th class="text-center"><?php echo __("No") ?></th>
            <th class="text-center"><?php echo __("Name") ?></th>
            <th class="text-center"><?php echo __("Office") ?></th>
            <th class="text-center"><?php echo __("Unit") ?></th>
            <th class="text-center"><?php echo __("Organogra") ?></th>
             <th class="text-center"><?php echo __("Login").' '.__("Information") ?></th>
            <th class="text-center"><?php echo __("Total").' '.__("Login") ?></th>
            <th class="text-center"><?php echo __("Login").' '.__("Time") ?></th>
            <th class="text-center"><?php echo __("Logout").' '.__("Time") ?></th>
            <th class="text-center"><?php echo __("Actions") ?></th>
        </tr>
    </thead>
    <tbody >


        <?php
        if (!empty($loginHistory)) {
            foreach ($loginHistory as $key => $value) {
                ?>
                <tr>
                    <td class="text-center"><?= Cake\I18n\Number::format((($page - 1) * $limit) + $key + 1) ?></td>
                    <td class="text-center"><?= h($value['employee_name']) ?></td>
                    <td class="text-center"><?= $value['office_name'] ?></td>
                    <td class="text-center"><?= $value['unit_name'] ?></td>
                    <td class="text-center"><?= $value['organogram_name'] ?></td>
                    <td class="text-center"><?= __('IP').': '.$value['client_ip'] ?></td>
                    <td class="text-center"><?= $value['countIP'] ?></td>
                    <td class="text-center"><?= $value['login_time'] ?></td>
                    <td class="text-center"><?= $value['logout_time'] ?></td>
                    <td class="text-center"><button class="btn btn-primary" onclick="showLoginDetails('<?=$value['employee_name'] . ', '.$value['organogram_name'].', '.$value['unit_name'].', '.$value['office_name']?>','<?= $value['office_unit_organogram'] ?>','<?=$value['client_ip']?>','<?= $start_time?>','<?=$end_time?>')"> <?= __('Details') ?> </button> </td>
                </tr>

                <?php
            }
        }
        ?>
    </tbody>
</table>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="header"><?php echo __("Login").' '.__("Information") ?></h4>
      </div>
        <div class="modal-body" id="loginDetails" style="height: 500px;overflow: auto;">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
      </div>
    </div>

  </div>
</div>
<div class="actions text-center">
    <ul class="pagination pagination-sm">
        <?php
        echo $this->Paginator->first(__('প্রথম', true), array('class' => 'number-first'));
        echo $this->Paginator->prev('<<', array('tag' => 'li', 'escape' => false),
            '<a href="#" class="btn btn-sm blue">&laquo;</a>',
            array('class' => 'prev disabled btn btn-sm blue', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true,
            'currentClass' => 'active', 'currentTag' => 'a', 'reverse' => false));

        echo $this->Paginator->next('>>', array('tag' => 'li', 'escape' => false),
            '<a href="#" class="btn btn-sm blue ">&raquo;</a>',
            array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
        echo $this->Paginator->last(__('শেষ', true), array('class' => 'number-last'));
        ?>
    </ul>

</div>