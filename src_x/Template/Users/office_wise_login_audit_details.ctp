<div class="table-container ">
    <table class="table table-bordered table-advance table-hover">
        <thead>
            <?php
            if (!empty($info)) {
               $start = new \Cake\I18n\Time($start);
               $end =new \Cake\I18n\Time($end);
                
                ?>
                <tr class="heading">
                    <th class="text-center" colspan="8"><?php echo $info.' এর '.__("Login").' '.__("Information") ?></th>
                </tr>
                <tr class="heading">
                    <th class="text-center" colspan="8"><?php echo ' সময় ব্যবধানঃ  '.$start->i18nFormat('Y-MM-d', null, 'bn-BD') .' - '.$end->i18nFormat('Y-MM-d', null, 'bn-BD')  ?></th>
                </tr>
                <tr class="heading">
                    <th class="text-center" colspan="8"><?php echo 'লগইন আইপিঃ '.$ip ?></th>
                </tr>
                <?php
            }
            ?>
            <tr class="heading">
                <th class="text-center"><?php echo __("No") ?></th>
                <th class="text-center"><?php echo __("Name") ?></th>
                <th class="text-center"><?php echo __("Office") ?></th>
                <th class="text-center"><?php echo __("Unit") ?></th>
                <th class="text-center"><?php echo __("Organogra") ?></th>
                <th class="text-center"><?php echo __("Login").' '.__("Information") ?></th>
                <th class="text-center"><?php echo __("Login").' '.__("Time") ?></th>
                <th class="text-center"><?php echo __("Logout").' '.__("Time") ?></th>
            </tr>
        </thead>
        <tbody >


            <?php
            if (!empty($response) && $response['status'] == 'success') {
                $browser_ids = [];
                if (!empty($response['data'])) {
                    $loginHistory = $response['data'];
                    $indx         = 1;
                    foreach ($loginHistory as $key => $value) {
                        $network_information = json_decode($value['network_information']);
                        if (!empty($network_information->DeviceId)) {
                            if (isset($browser_ids[$network_information->DeviceId])) {
                                $browser_ids[$network_information->DeviceId] ++;
                            }
                            else {
                                $browser_ids[$network_information->DeviceId] = 1;
                            }
                        }
                        ?>
                        <tr>
                            <td class="text-center"><?= Cake\I18n\Number::format($indx++) ?></td>
                            <td class="text-center"><?= h($value['employee_name']) ?></td>
                            <td class="text-center"><?= $value['office_name'] ?></td>
                            <td class="text-center"><?= $value['unit_name'] ?></td>
                            <td class="text-center"><?= $value['organogram_name'] ?></td>
                            <td class="text-center"><?= __('IP').': '.$value['client_ip']."<br/>".__('Device').': '.$value['device_name']."<br/>".__('Browser').': '.$value['browser_name']."<br/>".__('Browser').' আইডি : '.$network_information->DeviceId ?></td>
                            <td class="text-center"><?= $value['login_time'] ?></td>
                            <td class="text-center"><?= $value['logout_time'] ?></td>
                        </tr>

                        <?php
                    }
                    if (!empty($browser_ids)) {
                        $html = ' <tr><th class="text-center" colspan="8">যেসকল ব্রাউজার বব্যবহার হয়েছে </th></tr>';
                        echo $html;
                        echo '<tr><th class="text-center" colspan="4"> ব্রাউজার আইডি </th>';
                        echo '<th class="text-center" colspan="4">ব্যবহার সংখ্যা</th></tr>';
                        foreach ($browser_ids as $b_key => $b_val) {
                            echo '<tr><th class="text-center" colspan="4">'.$b_key.'</th>';
                            echo '<th class="text-center" colspan="4">'.Cake\I18n\Number::format($b_val).'</th></tr>';
                        }
                    }
                }
                else {
                    ?>
                    <tr><td colspan="8"> <?= __('No Data Found') ?></td></tr>
                    <?php
                }
            }
            else {
                ?>
                <tr><td colspan="8"> <?= (!empty($response['msg'])) ? $response['msg'] : __('No Data Found') ?></td></tr>
                <?php
            }
            ?>
        </tbody>
    </table>
</div>
