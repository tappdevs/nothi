<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class NothiOwnUnitCell extends Cell
{
    public function display($dak_subject = "", $nothijato=0,$selectedMeta = null)
    {
        $nothi_id = 0;
        $note_id = 0;
        if(!empty($selectedMeta)){
            if(count($selectedMeta)>1){

            }else{
                $note_id = $selectedMeta[0];
            }
        }
        $session = $this->request->session();
        $employee = $session->read('selected_office_section');

        $this->set('employee_id', $employee['office_unit_organogram_id']);
        $this->set('office_id', $employee['office_id']);
        $this->set('dak_subject',$dak_subject);
        TableRegistry::remove('NothiMasterPermissions');
        $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
        $nothiMastersTable = TableRegistry::get('NothiMasters');

        $nothiMastersPermitted = $nothiPriviligeTable->getMasterNothiList($employee['office_id'],$employee['office_id'], $employee['office_unit_id'], $employee['office_unit_organogram_id'],0,$note_id);
        
        $nothiMastersId = "";
        if (!empty($nothiMastersPermitted)) {
            foreach ($nothiMastersPermitted as $nothiKey => $nothiId) {
                if(!empty($nothi_id)){
                    if($nothiId['nothi_masters_id']!=$nothi_id){
                        continue;
                    }
                }
                $nothiMastersId .= $nothiId['nothi_masters_id'] . ',';
            }
            $condition = '(NothiMasters.is_archived is NULL OR NothiMasters.is_archived = 0) AND NothiMasters.id IN (' . substr($nothiMastersId, 0, -1) . ') ';
        } else {
            $condition = " 0 ";
        }

        $nothiPermissionListArray = $nothiMastersTable->getAll($condition,0,0,"NothiMasters.id DESC")->toArray();

        $this->set('nothiPermissionList', $nothiPermissionListArray);
        $this->set('nothijato',$nothijato);
        $this->set('selectedMeta',$selectedMeta);
    }
    public function displayForSummeryNote($dak_subject = "", $nothijato=0, $sarok_no)
    {
        $session = $this->request->session();
        $employee = $session->read('selected_office_section');

        $summary_nothi_users_table = TableRegistry::get('SummaryNothiUsers');
        $summary_nothi_users = $summary_nothi_users_table->find()->where(['tracking_id' => $sarok_no, 'sequence_number' => 1])->order(['id' => 'desc'])->first();
        //pr($sarok_no);die;
        if ($summary_nothi_users) {
            $nothi_master_id = $summary_nothi_users->nothi_master_id;
            $nothi_part_no = $summary_nothi_users->nothi_part_no;
        } else {
            $nothi_master_id = false;
            $nothi_part_no = false;
        }

        $this->set('employee_id', $employee['office_unit_organogram_id']);

        $this->set('dak_subject',$dak_subject);
        TableRegistry::remove('NothiMasterPermissions');
        $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');
        $nothiMastersTable = TableRegistry::get('NothiMasters');

        $nothiMastersPermitted = array();
        $nothiMastersPermitted = $nothiPriviligeTable->getMasterNothiList($employee['office_id'],$employee['office_id'], $employee['office_unit_id'], $employee['office_unit_organogram_id'], $nothi_master_id, $nothi_part_no);

        $nothiMastersId = "";
        if (!empty($nothiMastersPermitted)) {
            foreach ($nothiMastersPermitted as $nothiKey => $nothiId) {
                $nothiMastersId .= $nothiId['nothi_masters_id'] . ',';
            }
            $condition = ' NothiMasters.id IN (' . substr($nothiMastersId, 0, -1) . ') ';
        } else {
            $condition = " 0 ";
        }


        $nothiPermissionListArray = $nothiMastersTable->getAll($condition,0,0,"NothiMasters.id DESC")->toArray();
        //pr($nothiPermissionListArray);die;

        $this->set('nothiPermissionList', $nothiPermissionListArray);

        $this->set('nothijato',$nothijato);
    }
    public function getNothiPermittedListForUser($nothi_office =0,$dont_show_new_nothi_create_option =1,$dont_show_new_note_option = 1, $id = null){

        $session = $this->request->session();
        $employee = $session->read('selected_office_section');

        $this->set('employee_id', $employee['office_unit_organogram_id']);

        TableRegistry::remove('NothiMasterPermissions');
        $nothiPriviligeTable = TableRegistry::get('NothiMasterPermissions');

        $nothiMastersTable = TableRegistry::get('NothiMasters');

         if(empty($nothi_office)){
            $nothi_office = $employee['office_id'];
        }

//        if(empty($id)) {
            $nothiMastersPermitted = array();
            $nothiMastersPermitted = $nothiPriviligeTable->getMasterNothiList($nothi_office, $employee['office_id'], $employee['office_unit_id'], $employee['office_unit_organogram_id']);

            $nothiMastersId = "";
            if (!empty($nothiMastersPermitted)) {
                foreach ($nothiMastersPermitted as $nothiKey => $nothiId) {
                    $nothiMastersId .= $nothiId['nothi_masters_id'] . ',';
                }
                $condition = ' NothiMasters.id IN (' . substr($nothiMastersId, 0, -1) . ') ';
            } else {
                $condition = " 0 ";
            }
//        }else{
//            $condition = ' NothiMasters.id =' . $id;
//        }


        $nothiPermissionListArray = $nothiMastersTable->getAll($condition,0,0,"NothiMasters.id DESC")->toArray();

        $this->set('nothiPermissionList', $nothiPermissionListArray);
        $this->set(compact('dont_show_new_note_option','dont_show_new_nothi_create_option'));

    }
}
