<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class NothiPermittedCell extends Cell
{
    public function display($prefix = "")
    {
        $session = $this->request->session();
        $employee = $session->read('selected_office_section');
        $nothiList = $nothiMastersCurrentList = array();

        $nothiMasterTable = TableRegistry::get('NothiMasters');
        TableRegistry::remove('NothiMasterCurrentUsers');
        TableRegistry::remove('NothiMasterPermissions');
        $nothiMasterCurrentUserTable = TableRegistry::get('NothiMasterCurrentUsers');
        $nothiMasterPermissionTable = TableRegistry::get('NothiMasterPermissions');

        $permittedNothi = $nothiMasterPermissionTable->find()->where(['office_unit_organograms_id' => $employee['office_unit_organogram_id'], 'office_unit_id' => $employee['office_unit_id'], 'privilige_type > 0','nothi_office'=>$employee['employee']])->toArray();


        if (!empty($permittedNothi)) {

            $permittedNothiList = '';
            foreach ($permittedNothi as $key => $list) {
                $permittedNothiList .= $list['nothi_masters_id'] . ',';
            }

            if ($permittedNothiList != '') {
                $condition = "NothiMasters.office_units_id != {$employee['office_unit_id']} AND NothiMasters.id IN (" . substr($permittedNothiList, 0, -1) . ')';
                $nothiList = $nothiMasterTable->getAll($condition)->toArray();

                $nothiMastersCurrentList = $nothiMasterCurrentUserTable->find('list', [
                    'keyField' => 'nothi_master_id',
                    'valueField' => 'office_unit_organogram_id',
                ])->where(['office_unit_id' => $employee['office_unit_id'], 'office_unit_organogram_id' => $employee['office_unit_organogram_id'],'nothi_office'=>$employee['office_id']])->toArray();

            }

        }
        $this->set(compact('nothiList', 'nothiMastersCurrentList'));
        $this->set('employee_id', $employee['office_unit_organogram_id']);
    }

}
