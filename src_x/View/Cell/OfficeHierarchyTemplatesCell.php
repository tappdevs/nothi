<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class OfficeHierarchyTemplatesCell extends Cell
{

    public function display()
    {
        $office_hierarchy_table = TableRegistry::get('OfficeHierarchyTemplates');
        $office_hierarchys = $office_hierarchy_table->find()->select(['id', 'title_eng', 'title_bng'])->toArray();
        $office_hierarchy_list = array();
        foreach ($office_hierarchys as $oh) {
            $office_hierarchy_list[$oh['id']] = $oh['title_bng'] . "[" . $oh['title_eng'] . "]";
        }

        $this->set('office_hierarchy_list', $office_hierarchy_list);
    }
}
