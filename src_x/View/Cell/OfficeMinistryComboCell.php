<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class OfficeMinistryComboCell extends Cell
{
    public function display($field_value)
    {
        $table_instance = TableRegistry::get('OfficeMinistries');
        $data_items = $table_instance->find('list')->toArray();
        $this->set('data_items', $data_items);
        $this->set('value', $field_value);
    }
}
