<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class OfficeSealCell extends Cell {

    public function display($params) {
        $seal_info = array();
        $session = $this->request->session();
        $params = $session->read('selected_office_section');
        if (!empty($params)) {

            $table_instance_designation_seal = TableRegistry::get('OfficeDesignationSeals');
            $table_employee_office = TableRegistry::get('EmployeeOffices');
            $seal_designations = $table_instance_designation_seal->getSealArray($params);

            foreach ($seal_designations as $designation) {
                $employeeInfo = $table_employee_office->getDesignationDetailsInfo($designation->office_unit_organogram_id);

                if (!empty($employeeInfo)) {
                    $seal_info[] = array(
                        'id' => $designation->id,
                        'employee_office_id' => $employeeInfo['id'],
                        'designation_description' => $employeeInfo['designation_description'],
                        'designation_name_bng' => $designation->designation_name_bng,
                        'unit_name_bng' => $designation->unit_name_bng,
                        'office_unit_organogram_id' => $designation->office_unit_organogram_id,
                        'office_unit_id' => $designation->office_unit_id,
                        'employee_record_id' => $employeeInfo['employee_record_id'],
                        'employee_name' => $employeeInfo['EmployeeRecords']['name_bng'],
                    );
                }
            }
        }
        
        $this->set('query', $seal_info);
    }

}
