<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class OriginUnitCell extends Cell
{
    public function display($office_origin_id, $data_entity)
    {
        $table_instance = TableRegistry::get('OfficeOriginUnits');
        $data_items = $table_instance->find('list')->where(['office_origin_id' => $office_origin_id,'active_status'=>1])->toArray();
        $this->set('officeOriginUnits', $data_items);
        $this->set('data_entity', $data_entity);
    }
}
