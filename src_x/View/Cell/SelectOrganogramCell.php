<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * App View class
 */
class SelectOrganogramCell extends Cell
{
    public function display($prefix = "")
    {
        $table = TableRegistry::get('EmployeeRecords');
        $data = $table->find()->select(['id', 'name_eng', 'name_bng'])->toArray();
        $data_array = array();
        foreach ($data as $row) {
            $data_array[$row['id']] = $row['name_bng'] . "[" . $row['name_eng'] . "]";
        }
        $this->set('office_employees', $data_array);
        $this->set('prefix', $prefix);
    }
}
