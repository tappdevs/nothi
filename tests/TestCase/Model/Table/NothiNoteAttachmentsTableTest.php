<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NothiNoteAttachmentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NothiNoteAttachmentsTable Test Case
 */
class NothiNoteAttachmentsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nothi_note_attachments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NothiNoteAttachments') ? [] : ['className' => 'App\Model\Table\NothiNoteAttachmentsTable'];
        $this->NothiNoteAttachments = TableRegistry::get('NothiNoteAttachments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NothiNoteAttachments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getAllAttachments method
     *
     * @return void
     */
    public function testGetAllAttachments()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
