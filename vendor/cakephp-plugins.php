<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'ACake3Upload' => $baseDir . '/plugins/ACake3Upload/',
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'Cake3Upload' => $baseDir . '/plugins/Cake3Upload/',
        'CakePdf' => $baseDir . '/vendor/friendsofcake/cakepdf/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'ProjapotiAuditTrait' => $baseDir . '/plugins/ProjapotiAuditTrait/',
        'ProjapotiFileUploader' => $baseDir . '/plugins/ProjapotiFileUploader/'
    ]
];
