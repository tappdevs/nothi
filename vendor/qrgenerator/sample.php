<?php    
/*
 * PHP QR Code encoder
 *
 * Exemplatory usage
 *
 * PHP QR Code is distributed under LGPL 3
 * Copyright (C) 2010 Dominik Dzienia <deltalab at poczta dot fm>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */
    
    echo "<h1>PHP QR Code</h1><hr/>";
    
    //set it to writable location, a place for temp generated PNG files
    $file_dir = 'lib'.DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
    
    //html PNG location prefix
    // $PNG_WEB_DIR = 'lib/temp/';

    include "lib/qrlib.php";    
    
    
   



    if (isset($_REQUEST['data'])) { 
    
        //it's very important!
        if (trim($_REQUEST['data']) == '')
            die('data cannot be empty! <a href="?">back</a>');
            
        // user data
        $data 					= 	$_REQUEST['data'];
		$filename 				= 	QRcode::generateFileName($file_dir);
		$errorCorrectionLevel 	= 	'H';
		$matrixPointSize 		= 	4;
        $file_path 				= 	QRcode::png($data, $filename, $errorCorrectionLevel, $matrixPointSize, 2);    
        $file_string 			= 	QRcode::genBase64String($filename);    
        $file_src 				= 	QRcode::genBase64Src($filename);    
        echo '<img src="'.$file_path.'" /><hr/>';  
		?>
		<table style="width:850px" border='1'>
			<tr>
				<td width='15%'>File path</td>
				<td><?php echo $file_path ?></td>
			</tr>
			<tr>
				<td>Base64 String</td>
				<td><?php echo $file_string ?></td>
			</tr>
			<tr>
				<td>Base64 Src</td>
				<td><?php echo $file_src ?></td>
			</tr>
		</table>
		<?php

		
		echo "<br>";
		//echo '<img src="'.$base64.'">';
    } else {    
    
        //default data
        echo 'You can provide data in GET parameter: <a href="?data=like_that">like that</a><hr/>';    
        
        
    }    
        
    //display generated file
    
    //config form
        
    // benchmark
    //QRtools::timeBenchmark();    
?>

		<form action="sample.php" method="post">
			<table>
				<tr>
					<td>Data:</td>
					<td><textarea rows=6 name="data" value="'.(isset($_REQUEST['data'])?htmlspecialchars($_REQUEST['data']):'Write some data...').'"></textarea>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" value="GENERATE"></td>
				</tr>
			</table>
        </form><hr/>
    