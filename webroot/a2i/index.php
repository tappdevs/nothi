<!DOCTYPE html>
<html>
<head>
	<title>A2I Icon Pack</title>
	<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<?php
	$file_content = file_get_contents('style.css');

	// $icon_set_data_string = end(explode("/*Icon Set*/", $file_content));
	// echo "<pre>";var_dump($icon_set_data_string);die;


	$css = $file_content;
	$icon_classes = [];
	$css = str_replace("\r", "", $css); // get rid of new lines
	$css = str_replace("\n", "", $css); // get rid of new lines

	$first = explode('}', $css);

	if($first) {
	    foreach($first as $v) {
	        // explode() on the opening curly brace and the ZERO index should be the class declaration or w/e
	        $second = explode('{', $v);

	        // The final item in $first is going to be empty so we should ignore it
	        if(isset($second[0]) && $second[0] !== '') {
	            $icon_classes[] = trim($second[0]);
	        }
	    }
	}

	foreach ($icon_classes as $key => $icon_class) {
		if ($key > 4) {
			$icon_name = explode(':', $icon_class);
			$icon_name = $icon_name[0];

			$icon_name = explode('.', $icon_name);
			// if ($icon_class == '.a2i_gn_onumodondelevery1 .path1:before') {
			// 	echo "<pre>";var_dump($icon_name);die;
			// } else {
			// 	echo $icon_class."<br>";
			// 	continue;
			// }

			if (count($icon_name) == 3) {
				?><div class="<?php echo $icon_name[1]; ?>" style="font-size:20px;width:250px;text-align:center;padding:10px;border:solid 0.5px #f3f3f3;float:left;">
					<i style="font-size:40px;" class="<?php echo $icon_name[2]; ?>"></i><br/><?php echo $icon_name[1]; ?> <?php echo $icon_name[2]; ?>
				</div><?php
			}
			if (count($icon_name) == 2) {
				?><div style="font-size:20px;width:250px;text-align:center;padding:10px;border:solid 0.5px #f3f3f3;float:left;">
					<i style="font-size:40px;" class="<?php echo $icon_name[1]; ?>"></i><br/><?php echo $icon_name[1]; ?>
				</div><?php
			}
			$icon_name = explode('#', $icon_name);
			if (count($icon_name) > 1) {
				?><div style="font-size:20px;width:250px;text-align:center;padding:10px;border:solid 0.5px #f3f3f3;float:left;">
					<i style="font-size:40px;" id="<?php echo $icon_name[1]; ?>"></i><br/><?php echo $icon_name[1]; ?>
				</div><?php
			}
		}
	}
	?>
</body>
</html>