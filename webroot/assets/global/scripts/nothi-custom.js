/**
 * Created by Hasan Sayeed on 11/20/2017.
 */
$( document ).ajaxStop(function() {
    changeBootBoxStyle();
});
$( document ).ready(function() {
    changeBootBoxStyle();

    window.addEventListener("DOMSubtreeModified", function () {
        if ($(".bootbox").length > 0) {
            if (!$(".bootbox").hasClass('modal-purple')) {
                changeBootBoxStyle();
            }
        }
    }, false);
});
$( document ).click(function () {
    changeBootBoxStyle();
});
//for get last clicked event
$(document).click(function(e) {
    e = e || event;
    $.lastClicked = e.target || e.srcElement;
    setTimeout(function() {
        changeBootBoxStyle();
    }, 1000);
});
function changeBootBoxStyle() {
        setTimeout(function () {
            $(".bootbox").each(function (index) {
                var bootbox = this;
                $(this).addClass('modal-purple height-auto');
                $(this).find('button').removeClass('close');
                if ($(bootbox).find('.modal-footer').find('.btn-group').length == 0) {
                    setTimeout(function() {
                        var total_width = 0;
                        $(bootbox).find(".modal-footer").find(".btn").each(function () {
                            total_width += $(this).outerWidth();
                        });
                        if ($(bootbox).find(".modal-footer").innerWidth() < total_width) {
                            $(bootbox).find(".modal-footer").find('.btn-group.btn-group-round').removeClass('btn-group').removeClass('btn-group-round');
                            $(bootbox).find(".modal-footer").find(".btn").addClass('btn-block');
                        } else {
                            $(bootbox).find('.modal-footer').html('<div class="btn-group btn-group-round">' + $(bootbox).find('.modal-footer').html() + '</div>');
                        }
                    }, 100);
                }
            });
        }, 100);
}
function waitForElement(elementPath, callBack){
    window.setTimeout(function(){
        if($(elementPath).length){
            callBack(elementPath, $(elementPath));
        }else{
            waitForElement(elementPath, callBack);
        }
    },500)
}


function BrowserDetection() {
    var broserName = null;
    //Check if browser is IE
    if (navigator.userAgent.search("MSIE") >= 0) {
        // insert conditional IE code here
        broserName = 'Internet Explorer';
    }
    //Check if browser is Chrome
    else if (navigator.userAgent.search("Chrome") >= 0) {
        // insert conditional Chrome code here
        broserName = 'Chrome';
    }
    //Check if browser is Firefox
    else if (navigator.userAgent.search("Firefox") >= 0) {
        // insert conditional Firefox Code here
        broserName = 'Firefox';
    }
    //Check if browser is Safari
    else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
        // insert conditional Safari code here
        broserName = 'Safari';
    }
    //Check if browser is Opera
    else if (navigator.userAgent.search("Opera") >= 0) {
        // insert conditional Opera code here
        broserName = 'Opera';
    }
    return broserName;
}
function alertOtherBrowser(requestedBrowserName, requestedElement) {
    requestedElement.hide();
    var browserName = BrowserDetection();
    if (browserName != requestedBrowserName) {
        bootbox.dialog({
            message: "আপনি গুগল ক্রোম ব্যতিরেকে অন্য ব্রাউজার ব্যবহার করছেন, দয়া করে গুগল ক্রোম ব্যবহার করুন, অন্যথায় পত্রটি ভেঙ্গে যেতে পারে। আপনি কি এই ব্রাউজারে কার্যক্রম চালিয়ে যেতে চান?",
            title: "দয়া করে গুগল ক্রোম ব্যবহার করুন",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function (result) {
                        //return true;
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function (result) {
                        $('#potroClose').trigger('click');
                    }
                }
            }
        });
        return false;
    }
}
function getDownloadFile(fileUrl) {
    window.open(fileUrl);
    // var http = new XMLHttpRequest();
    // http.open('HEAD', fileUrl, false);
    // http.send();
    // if (http.status!=404) {
    //     var fileNameIndex = fileUrl.lastIndexOf("/") + 1;
    //     var filename = fileUrl.substr(fileNameIndex);
    //     var link = document.createElement("a");
    //     link.download = filename;
    //     link.href = fileUrl;
    //     link.click();
    //     return;
    // } else {
    //     toastr.error('ফাইলটি খুঁজে পাওয়া যায়নি');
    //     return http.status!=404;
    // }
}

function doModal(modalId, heading, formContent, submitBtn , isHeightAuto ) {
    if (typeof(isHeightAuto)!= 'undefined') {
        isHeightAuto = '';
    }
    $("#"+modalId).remove();
    var html =  '<div id="'+modalId+'" class="modal modal-purple '+isHeightAuto+'">';
    html += '<div class="modal-dialog modal-lg">';
    html += '<div class="modal-content">';
    html += '<div class="modal-header">';
    html += '<button data-dismiss="modal">×</button>';
    html += '<h4>'+heading+'</h4>';
    html += '</div>';
    html += '<div class="modal-body">';
    html += '<p>';
    html += formContent;
    html += '</div>';
    html += '<div class="modal-footer">';
    html += '<div class="btn-group btn-group-round">';
    if (typeof(submitBtn)!= 'undefined') {
        html += submitBtn;
    }
    html += '<span class="btn red" data-dismiss="modal">';
    html += 'বন্ধ করুন';
    html += '</span>'; // close button
    html += '</div>';  // footer
    html += '</div>';  // footer
    html += '</div>';  // content
    html += '</div>';  // dialog
    html += '</div>';  // modalWindow
    $('body').append(html);
    $("#"+modalId).modal('show');
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function getParams (url) {
    var params = {};
    var parser = document.createElement('a');
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
};