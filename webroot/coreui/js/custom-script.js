(function (e) {
  /*datepicker*/
  $('[data-toggle="datepicker"]').datepicker({
    autoHide: true,
    format: 'dd-mm-yyyy'
  });
  
  
  /*image uploader*/
  $(document).off('click', ".btn-close").on('click', ".btn-close", function () {
    $(this).closest('.up_item').remove();
  })
  
  var imagesPreview = function (input, placeToInsertImagePreview) {
    
    if (input.files) {
      
      for (i = 0; i < input.files.length; i++) {
        var reader = new FileReader();
        var extension = input.files[i].name.replace(/^.*\./, '');
        
        if (extension == 'docx' || extension == 'xlsx' || extension == 'xls' || extension == 'doc') {
          reader.onload = function (event) {
            $(placeToInsertImagePreview).append(
              '<div class="docx up_item mb-2 mr-2"><i class="fa fa-close"></i><i class="fa fa-file-word-o"></i> </div>'
            );
          }
        }
        
        if (extension == 'pdf') {
          reader.onload = function (event) {
            $(placeToInsertImagePreview).append(
              '<div class="pdf up_item mb-2 mr-2"><i class="fa fa-close"></i><i class="fa fa-file-pdf-o"></i> </div>'
            );
          }
        }
        
        if (extension == 'zip') {
          reader.onload = function (event) {
            $(placeToInsertImagePreview).append(
              '<div class="zip up_item mb-2 mr-2"><i class="fa fa-close"></i><i class="fa fa-file-zip-o"></i> </div>'
            );
          }
        }
        
        if (extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'svg') {
          reader.onload = function (event) {
            $(placeToInsertImagePreview).append(
              // '<div class="image up_item mb-2 mr-2"><i class="fa fa-close"></i><img src="' + event.target.result + '" /> </div>'
              
              '<div class="image up_item col-sm-6">' +
              '<div class="border mb-3 d-flex align-items-center pr-3">' +
              '<div class="p-2"><input type="radio" name="item"></div>' +
              '<div class="file_wrap shadow-sm">' +
              '<img src="' + event.target.result + '" />' +
              '</div> ' +
              '<input type="text" class="form-control mx-3" placeholder="সংযুক্তির নাম ">' +
              '<div><button class="btn btn-danger btn-xs fa fa-trash btn-close"></button></div>' +
              '</div>' +
              '</div>'
            );
          }
        }
        
        reader.readAsDataURL(input.files[i]);
      }
    }
    
  };
  
  $(document).off('change', ".file_uploader").on('change', ".file_uploader", function () {
    imagesPreview(this, 'div.file_uploader_gallery div.upload_wrap');
  });
  
  
  /*image uploader*/
  
  
  $('.sidebar-minimizer').click(function (e) {
    $('.update-notice').toggleClass('hide');
  });
  
  $('.daak_sender_btn').on('click', function (e) {
    $('.daak_sender_info').slideToggle(100);
    e.stopPropagation();
  })
  $(".daak_sender_info").click(function (e) {
    e.stopPropagation();
  });
  
  $(document).click(function () {
    $(".daak_sender_info").slideUp(100);
  });
  
  
  /*$('.daak_forward').click(function (e) {
    e.preventDefault();
    
    
    $('.daak_btns').slideToggle(100);
    $('.daak_movement').slideToggle(100);
    
    $('.daak_detail').animate({
      scrollTop: $("div.daak_movement").offset().top - 300
    }, 1000)
    return false;
  })*/
  
  $(".style-changer button").click(function (e) {
    e.preventDefault();
    $('body').removeClass('style-1').removeClass('style-2').removeClass('style-3');
    console.log($(this).data('style'));
    $('body').toggleClass($(this).data('style'));
  });
  
  
  $("textarea").focus(function (e) {
    // noteTopBottom.setSizes([60, 40]);
  });
  
  // $('.btn-show-hide').unbind('click');
  $("#dbl_click").on("click", ".btn-show-hide", function (e) {
    e.preventDefault();
    var nothi_master_id = $(this).data("nothi-master-id");
    console.log("nothi master id: ", nothi_master_id);
    
    fetch("/tappware/nothi/NothiMasterMovements/getAllNothiNotesV2", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify({nothi_master_id: nothi_master_id})
    })
    .then(response => {
      return response.json();
    })
    .then(data => {
      if (data.status == "success") {
        let nothi_list = data.data;
        console.log("Nothi data response : ", nothi_list);
        let table_body = $("#table-nothi-notes-body");
        table_body.find("tr").remove();
        nothi_list.forEach(function (value) {
          let tr_tmp =
            "<tr role=row class=even><td style='font-family:nikosh'>" +
            value.subject +
            "</td><td>" +
            value.created +
            "</td><td>" +
            value.description_bk +
            "</td><td><div class='text-center'><a class='btn btn-success btn-sm btn-show-hide' data-nothi-master-id=" +
            value.nothi_master_id +
            " href='javascript;'><i class='fa fa-search-plus'></i></a></div></td></tr>";
          table_body.append(tr_tmp);
          console.log(value.office_id);
        });
      }
    })
    .catch(err => {
      console.log("Error: ", err);
    });
    
    $(".content-slide").toggleClass("show");
  });
  
  $('[data-toggle="tooltip"]').tooltip();
  
  $(".datepicker").datepicker({
    clearBtn: true,
    format: "dd-mm-yyyy",
    autoclose: true,
    todayHighlight: true
  });
  
  $("select").select2({
    width: "100%"
  });
  
  // $('.ql-toolbar select').select2('close');
  
  $(".pScroll,.fr-wrapper").each(function (e) {
    const ps = new PerfectScrollbar(this, {
      wheelSpeed: 2,
      wheelPropagation: true,
      minScrollbarLength: 20,
    });
  });
  
  /*window.addEventListener('resize', function() {
    console.log('not');
    console.log(this.width());
  });*/
  
  console.log("custom-script running...");
})(jQuery);
