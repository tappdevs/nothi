(function (e) {
  
  
  $('textarea').focus(function (e) {
    // noteTopBottom.setSizes([60, 40]);
  })
  
  /*$('.btn-show-hide').click(function (e) {
    e.preventDefault();
    $('.content-slide').toggleClass('show');
  })*/
  
  $('#dbl_click').on('click', '.btn-show-hide', function (e) {
    e.preventDefault();
    var nothi_master_id = $(this).data('nothi-master-id');
    console.log('nothi master id: ', nothi_master_id);
    
    fetch('/nothi/NothiMasterMovements/getAllNothiNotesV2',
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({nothi_master_id: nothi_master_id})
      }).then(response => {
      return response.json();
    }).then(data => {
      if (data.status == "success") {
        let nothi_list = data.data;
        console.log("Nothi data response : ", nothi_list);
        let table_body = $('#table-nothi-notes-body');
        table_body.find('tr').remove();
        nothi_list.forEach(function (value) {
          let tr_tmp = "<tr role=row class=even><td style='font-family:nikosh'>" + value.id + "</td><td>" + value.nothi_no + "</td><td>" + value.subject + "</td><td>" + value.created +
            "</td><td>" + value.from_office_unit_name +
            "</td><td><div class='text-center'><a class='btn btn-success btn-sm btn-show-hide' data-nothi-master-id=" + value.nothi_master_id +
            " href='javascript;'><i class='fa fa-search-plus'></i></a></div></td></tr>";
          table_body.append(tr_tmp);
          console.log(value.office_id);
        });
        
      }
    }).catch(err => {
      console.log('Error: ', err);
    });
    
    
    $('[data-toggle="tooltip"]').tooltip();
    
    $('.datepicker').datepicker({
      clearBtn: true,
      format: "dd-mm-yyyy",
      autoclose: true,
      todayHighlight: true
    });
    
    $('select').select2({
      width: '100%'
    });
    
    // $('.ql-toolbar select').select2('close');
    
    $('.pScroll,.fr-wrapper').each(function (e) {
      const ps = new PerfectScrollbar(this, {
        wheelSpeed: 2,
        wheelPropagation: true,
        minScrollbarLength: 20
      });
    })
    
    /*window.addEventListener('resize', function() {
      console.log('not');
      console.log(this.width());
    });*/
    
    
    console.log('custom-script running...');
  })(jQuery)
/*


console.log(document.getElementById("note-bottom"));

document.getElementById("note-bottom").onresize = function() {
  console.log('not');
  console.log(this.width());
};*/
