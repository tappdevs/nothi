$(function (e) {
  $('input.sameAsPresent').change(function (e) {
    if(this.checked) {
      $('.permanentAddress').val($('.presentAddress').val());
      $(this).parents('.form-group').find('textarea').prop('disabled', true);
    }else{
      $('.permanentAddress').val('');
      $(this).parents('.form-group').find('textarea').prop('disabled', false);
    }
  })
  /*office_unit_dropdown*/
  $('.office-seal input[type="checkbox"]').on('change', function (e) {
    var receiverName = $(this).siblings('label').text();
    var receiverOffice = $(this).attr('title');
    
    console.log(receiverOffice);
    
    var prapokRow = '<tr>' +
      '<td>' +
      '<span>' + receiverName + ' , ' + receiverOffice + '</span>' +
      '</td>' +
      '</tr>';
    $('#office_unit_seal_panel table tbody').append(prapokRow);
  })
  
  
  $('#office_unit_dropdown').on('change', function (e) {
    var thisData = $(this).val();
    if (thisData != 'all') {
      $('.office-seal .card').hide();
      $('.office-seal .card[data-id="' + thisData + '"]').show();
    } else {
      $('.office-seal .card').show();
    }
    /*if($('.office-seal .card').data('id') == thisData){
      $(this).show();
    }*/
    
  })
  /*office_unit_dropdown*/
  
  /*daak area collupse*/
  $('.office-seal .card-header').on('click', function (e) {
    $(this).find('i.fa').toggleClass('fa-plus-circle').toggleClass('fa-minus-circle');
  })
  $('.card-header .toogle-icon').on('click', function (e) {
    $(this).toggleClass('fa-angle-down').toggleClass('fa-angle-right');
  })
  /*daak area collupse*/
  
  /*daptorik daak officer filter*/
  $('.office_search').on('click', function () {
    $(this).toggleClass('fa-chevron-circle-down').toggleClass('fa-chevron-circle-up');
    $('.search_specific').toggle();
    $('.search_advanced').toggle();
  })
  /*daptorik daak officer filter*/
  
})