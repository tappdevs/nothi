(function (e) {
  
  console.log('onucched');
  
  
  $('.onucched_toggle').click(function (e) {
    $('.onucched_number_list').toggle();
  })
  
  
  $('.side-pop-btn,.btn-close').click(function (e) {
    $('.side-pop').removeClass('show');
    $(this).parents('.side-pop-parent').find('.side-pop').toggleClass('show');
    e.stopPropagation();
  })
  $('.side-pop').click(function (e) {
    e.stopPropagation();
  });
  $(document).click(function (e) {
    $('.side-pop').removeClass('show');
  })
  
  
  $('button.expand').on('click', function (e) {
    $(this).parents('.card').addClass('full-screen');
    $(this).parents('.card').find('.card-footer button span').show();
  })
  
  $('button.notansho_fullscreen').on('click', function (e) {
    $('#notansho').toggleClass('full-screen');
    $(this).find('i.fa').toggleClass('fa-expand').toggleClass('fa-compress');
  })
  
  $('button.compress').on('click', function (e) {
    console.log('eleme');
    $(this).parents('.card').removeClass('full-screen');
    if ($(this).parents('.card').width() < 500) {
      $(this).parents('.card').find('.card-footer button span').hide();
    }
  })
  
  
  $('#note-bottom textarea.froala').froalaEditor({
    placeholderText: 'এখানে অনুচ্ছেদ লিখুন...'
  });
  
  
  /*file uploader*/
  Object.defineProperty(Number.prototype, 'fileSize', {
    value: function (a, b, c, d) {
      return (a = a ? [1e3, 'k', 'B'] : [1024, 'K', 'iB'], b = Math, c = b.log,
          d = c(this) / c(a[0]) | 0, this / b.pow(a[0], d)).toFixed(2)
        + ' ' + (d ? (a[1] + 'MGTPEZY')[--d] + a[2] : 'Bytes');
    }, writable: false, enumerable: false
  });
  
  console.log((186457865).fileSize(1));
  
  function formatNumber(num) {
    return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }
  
  
  $('.fr-element').focus(function (e) {
    $('.attachment_upload').hide();
  })
  $('.onucched_add_attachment').click(function (e) {
    $('.attachment_upload').toggle();
  })
  
  $(document).off('click', ".trash_attached").on('click', ".trash_attached", function () {
    $(this).closest('.up_item').remove();
  })
  
  
  var imagesPreview = function (input, placeToInsertImagePreview) {
    
    if (input.files) {
      
      for (i = 0; i < input.files.length; i++) {
        var reader = new FileReader();
        var extension = input.files[i].name.replace(/^.*\./, '');
        
        if (extension == 'docx' || extension == 'xlsx' || extension == 'xls' || extension == 'doc') {
          reader.onload = function (event) {
            $(placeToInsertImagePreview).append(
              '<div class="docx up_item mb-2 col-2"><i class="fa fa-close"></i><i class="fa fa-file-word-o"></i> </div>'
            );
          }
        }
        
        if (extension == 'pdf') {
          reader.onload = function (event) {
            $(placeToInsertImagePreview).append(
              '<div class="pdf up_item mb-2 col-2"><i class="fa fa-close"></i><i class="fa fa-file-pdf-o"></i> </div>'
            );
          }
        }
        
        if (extension == 'zip') {
          reader.onload = function (event) {
            $(placeToInsertImagePreview).append(
              '<div class="zip up_item mb-2 col-2"><i class="fa fa-close"></i><i class="fa fa-file-zip-o"></i> </div>'
            );
          }
        }
        
        if (extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'svg') {
          reader.onload = function (event) {
            // console.log(formatNumber(event.total / 1024));
            // console.log((event.total).fileSize(1));
            $(placeToInsertImagePreview).append(
              '<div class="image up_item mb-2 d-flex justify-content-between align-items-center px-2 alert-warning">' +
              '<i class="fa fa-image fa-2x"></i>' +
              '<div class="px-2" style="width:200px">' + (event.total).fileSize(1) + '</div>' +
              '<input class="form-control form-control-sm" style="width:200px" type="text" placeholder="type label...">' +
              '<button class="btn btn-danger trash_attached btn-sm">' +
              '<i class="fa fa-close"></i>' +
              '</button>' +
              '</div>'
            );
          }
        }
        
        
        reader.readAsDataURL(input.files[i]);
      }
    }
    
  };
  
  $('.file_uploader').on('change', function () {
    imagesPreview(this, 'div.file_uploader_gallery div.attach_list');
  });
  
  
  $('#mulpotro_upload').on('change', function () {
    imagesPreview(this, 'div.mulpotro_upload_gallery div.attach_list');
  });
  $('#shonjukti_upload').on('change', function () {
    imagesPreview(this, 'div.shonjukti_upload_gallery div.attach_list');
  });
  
  /*file uploader*/
  
  
  function onucchedButtonLabel() {
    if ($('.add_onucched').width() < 500) {
      $('.add_onucched .card-footer button span').hide();
    } else {
      $('.add_onucched .card-footer button span').show();
    }
  }
  
  $(window).resize(function (e) {
    onucchedButtonLabel();
  })
  
  if ($('#notansho, #potransho').length) {
    var notePotro = Split(['#notansho', '#potransho'], {
      minSize: [300, 300],
      onDrag: function (sizes) {
        onucchedButtonLabel();
      }
    });
  }
  if ($('#note-top, #note-bottom').length) {
    var noteTopBottom = Split(['#note-top', '#note-bottom'], {
      sizes: [100, 0],
      minSize: [100, 0],
      direction: 'vertical'
    });
    // noteTopBottom.collapse(1);
    
  }
  
  
  
  $('.write_onucched').click(function (e) {
    onucchedButtonLabel();
    noteTopBottom.setSizes([50, 50]);
    $('#note-top').removeClass('h-100');
    $('.gutter-vertical').show();
    $(this).hide();
  })
  
  $('.cancle_onucched').click(function (e) {
    noteTopBottom.setSizes([100, 0]);
    $(this).parents('.card').removeClass('full-screen');
    $('#note-top').addClass('h-100');
    $('.gutter-vertical').hide();
    $('.write_onucched').show();
  })
  
  
})(jQuery)
