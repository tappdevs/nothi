(function (e) {
  
  
  $(document).off('click', '.btn_external').on('click', '.btn_external', function (e) {
    // alert('working');
    var tabID = $(this).attr('data-id');
    var externalWindow = window.open('', tabID, 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=0,height=0');
  
    externalWindow.document.write($("#"+tabID).html());
    externalWindow.document.write('<html><head><title>'+tabID+'</title><link href="/tappware/nothi/coreui/css/font-icons.css" rel="stylesheet">\n' +
      '  <link href="/tappware/nothi/coreui/node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">\n' +
      '  <link href="/tappware/nothi/coreui/node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">\n' +
      '  <link href="/tappware/nothi/coreui/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">\n' +
      '  <link href="/tappware/nothi/coreui/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">\n' +
      '  <link href="/tappware/nothi/coreui/vendors/select2/dist/css/select2.min.css" rel="stylesheet">\n' +
      '  <link href="/tappware/nothi/coreui/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"\n' +
      '      rel="stylesheet">\n' +
      '  <link href="/tappware/nothi/coreui/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">\n' +
      '  <!-- Include Editor style. -->\n' +
      '  <link href=\'/tappware/nothi/coreui/node_modules/froala-editor/css/froala_editor.min.css\' rel=\'stylesheet\' type=\'text/css\' />\n' +
      '  <link href=\'/tappware/nothi/coreui/node_modules/froala-editor/css/froala_style.min.css\' rel=\'stylesheet\' type=\'text/css\' />\n' +
      '\n' +
      '\n' +
      '  <!-- Main styles for this application-->\n' +
      '  <link href="/tappware/nothi/coreui/css/style.genius.css" rel="stylesheet">\n' +
      '  <link href="/tappware/nothi/coreui/vendors/pace-progress/css/pace.min.css" rel="stylesheet">\n' +
      '  <link href="/tappware/nothi/coreui/css/nothi_style.css" rel="stylesheet">\n<!-- Bootstrap and necessary plugins-->\n' +
      '  <script src="/tappware/nothi/coreui/node_modules/jquery/dist/jquery.min.js"></script>\n' +
      '  <script src="/tappware/nothi/coreui/node_modules/popper.js/dist/umd/popper.min.js"></script>\n' +
      '  <script src="/tappware/nothi/coreui/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>\n' +
      '\n' +
      '\n' +
      '  <script src="/tappware/nothi/coreui/node_modules/pace-progress/pace.min.js"></script>\n' +
      '  <script src="/tappware/nothi/coreui/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>\n' +
      '  <script src="/tappware/nothi/coreui/node_modules/@coreui/coreui-pro/dist/js/coreui.min.js"></script>\n' +
      '  <script src="/tappware/nothi/coreui/vendors/select2/dist/js/select2.min.js"></script>\n' +
      '  <script src="/tappware/nothi/coreui/node_modules/moment/min/moment.min.js"></script>\n' +
      '  <script src="/tappware/nothi/coreui/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>\n' +
      '\n' +
      '\n' +
      '  <script src="/tappware/nothi/coreui/node_modules/datatables.net/js/jquery.dataTables.js"></script>\n' +
      '  <script src="/tappware/nothi/coreui/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>\n' +
      '  <script src="/tappware/nothi/coreui/js/datatables.js"></script></head><body>');
  
    e.stopPropagation();
    e.preventDefault();
  })
  
  
  $(document).off('click', '.note_list .list-group-item .note_label')
  .on('click', '.note_list .list-group-item .note_label', function () {
    var checkbox = $(this).parents('.list-group-item').find('input[type=checkbox]');
    
    $('.notansho_note_view_tab nav .nav-tabs, .notansho_note_view_tab .tab-content').html('');
    
    $(this).parents('.list-group').find('input[type=checkbox]').prop("checked", false);
    $(this).parents('.list-group-item').siblings('.list-group-item').removeClass('active');
    $(this).parents('.list-group-item').addClass('active');
    
    checkbox.prop("checked", true);
    
    showCheckedNotes(checkbox);
    
    $(this).parents('.list-group').removeClass('checked');
    
  })
  
  
  $(document).off('change', '.note_list .list-group-item input[type=checkbox]')
  .on('change', '.note_list .list-group-item input[type=checkbox]', function () {
    
    $('.notansho_note_view_tab nav .nav-tabs, .notansho_note_view_tab .tab-content').html('');
    
    $('.note_list .list-group-item').each(function (e) {
      
      $(this).find('input[type=checkbox]:checked').each(function (name) {
        
        showCheckedNotes($(this));
        
        $(this).parents('.list-group').addClass('checked').find('.list-group-item').removeClass('active');
      })
    })
  });
  
  
  function showCheckedNotes(checkbox) {
    
    var tab_id = checkbox.val();
    var tab_title = checkbox.attr('data-title');
    var tab_date = checkbox.attr('data-date');
    var tab_id = checkbox.val();
    var tab_nav_title = checkbox.parents('.list-group-item').find('.note_label').text();
    
    console.log(tab_title);
    
    /*var tab_title = '<li class="nav-item"><a class="nav-item nav-link" id="noteNavLink_' + tab_id + '" data-toggle="tab" href="#noteNav_' + tab_id + '">' + tab_nav_title + '<button class="ml-3 btn-sm btn btn-transparent text-dark"><i class="fa fa-external-link"></i></button></a> </li>';
    var tab_content = '<div class="tab-pane p-0 h-100 fade" id="noteNav_' + tab_id + '" role="tabpanel"><div class="card border-0 h-100 mb-0">' + tab_nav_title + '</div></div>';*/
    var tab_note_no = '<li class="nav-item"><a class="nav-item nav-link" id="noteNavLink_' + tab_id + '" data-toggle="tab" href="#noteNav_' + tab_id + '">' + tab_nav_title + '<button data-id="noteNav_' + tab_id + '" class="btn_external ml-3 btn-sm btn btn-transparent text-dark"><i class="fa fa-external-link"></i></button></a> </li>';
    $.ajax({
      type: "POST",
      url: "/tappware/nothi/Dashboard/getNote",
      data: {'tab_id': tab_id, 'tab_title': tab_title, 'tab_date': tab_date},
      async: false,
      success: function (htmlResponse) {
        // console.log(htmlResponse);
        $('.notansho_note_view_tab nav .nav-tabs').append(tab_note_no);
        $('.notansho_note_view_tab .tab-content').append('<div class="tab-pane p-0 h-100 fade" id="noteNav_' + tab_id + '" role="tabpanel"><div class="card border-0 h-100 mb-0">' + htmlResponse + '</div></div>');
        
        $('.pScroll').each(function (e) {
          const ps = new PerfectScrollbar(this, {
            wheelSpeed: 2,
            wheelPropagation: true,
            minScrollbarLength: 20
          });
        })
      },
      error: function (tab) {
        alert('error');
      }
    });
    
    // ('/tappware/nothi/Dashboard/getNote');//append(tab_content);
    
    $('.nav-tabs a[href="#noteNav_' + tab_id + '"]').tab('show');
    
    
  }
  
  /*$('.note_list .list-group-item input[type=checkbox]').on('change', function (e) {
    e.stopPropagation();
    
    var tab_id = $(this).val();
    var tab_nav_title = $(this).parents('.list-group-item').find('.note_label').text();
    
    if ($(this).is(':checked')) {
      
      $(this).parents('.list-group').addClass('checked');
      $('.notansho_note_view').hide();
      $('.notansho_note_view_tab').show();
      
      var tab_title = '<li class="nav-item"><a class="nav-item nav-link" id="noteNavLink_' + tab_id + '" data-toggle="tab" href="#noteNav_' + tab_id + '">' + tab_nav_title + '<button class="ml-3 btn-sm btn btn-transparent text-dark"><i class="fa fa-external-link"></i></button></a> </li>';
      var tab_content = '<div class="tab-pane p-0 h-100 fade" id="noteNav_' + tab_id + '" role="tabpanel"><div class="card border-0 h-100 mb-0">' + tab_nav_title + '</div></div>';
      
      $('.notansho_note_view_tab nav .nav-tabs').append(tab_title);
      $('.notansho_note_view_tab .tab-content').append(tab_content);
      
      $('.nav-tabs a[href="#noteNav_' + tab_id + '"]').tab('show');
      
    }else{
      $(this).parents('.list-group').removeClass('checked');
  
      $('#noteNav_' + tab_id).remove();
      $('#noteNavLink_' + tab_id).remove();
      
      $('.nav-tabs a:first-child').tab('show');
    }*/
})
(jQuery)