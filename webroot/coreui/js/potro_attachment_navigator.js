(function (e) {
  $(document).off('click', '.note_left').on('click', '.note_left', function (e) {
    var toolbars = $(this).parents('.slides').find('.btn-toolbar');
    var barPosition = toolbars.position().left;
    var barLastChildPosition = toolbars.children().last().position().left;
    var barRealWidth = toolbars.children().last().width() + barLastChildPosition;
    var scrollWidth = barRealWidth - toolbars.width();
    
    var remainingSpace = scrollWidth - Math.abs(barPosition);
    
    if (barRealWidth > toolbars.width()) {
      if (remainingSpace < 200) {
        // $(this).prop('disabled', true);
        toolbars.animate({left: barPosition - remainingSpace});
      } else {
        toolbars.animate({left: barPosition - 200});
      }
    }
    
  })
  
  $(document).off('click', '.note_right').on('click', '.note_right', function (e) {
    var toolbars = $(this).parents('.slides').find('.btn-toolbar');
    var barPosition = toolbars.position().left;
    var barLastChildPosition = toolbars.children().last().position().left;
    var barRealWidth = toolbars.children().last().width() + barLastChildPosition;
    var scrollWidth = barRealWidth - toolbars.width();
    
    if (barPosition < 42) {
      toolbars.animate({left: barPosition + 100});
    } else {
      toolbars.animate({left: 0});
    }
  })
})(jQuery)
