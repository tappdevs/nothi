var app = angular.module('nothiServiceApp', []);
app.controller('nothiServiceCtrl', function($scope) {
    $scope.topinp = {value:20};
    $scope.rightinp = {value:20};
    $scope.bottominp = {value:20};
    $scope.leftinp = {value:20};
});


$(function (e) {

    $("input[type=number]").keydown(function(e) {
        if(e.which == 37) {
            $(this).prevAll("input:first").focus();
        }
        if(e.which == 39) {
            $(this).nextAll("input:first").focus();
        }
    });

    var previousHtml = jQuery(".message-append").html();

    jQuery(".page .subpage").append(previousHtml);

    $('div.split-pane').splitPane();

    $('#bottom-component .note-editable').on('click', function(e) {
        $('div.horizontal-percent').splitPane('lastComponentSize', 300);
        $('.btn-area,.note-toolbar-wrapper').show();
        e.stopPropagation();

    });


    $('.cancel ').on('click', function(e) {
        $('div.horizontal-percent').splitPane('lastComponentSize', 70);
        $('.btn-area,.note-toolbar-wrapper').hide();
        e.stopPropagation();
    });


    /*$(window).on('click', function() {
        $('div.horizontal-percent').splitPane('lastComponentSize', 100);
    });*/

    $(".btn-print").click(function () {
        $("#section-to-print").printThis();
    });


    $('.add-message').click(function (e) {
        alert('hi');
        var docHtml = $('.note-editable').html();
        $('.message-append').append(docHtml);
    })


    $('.pScroll').perfectScrollbar();

})





function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
}

//== Class definition

var ActionsDemo = function () {
    //== Private functions

    return {
        // public functions
        init: function() {
            $('.summernote').summernote({
                height: 100,
            });
        }
    };
}();
