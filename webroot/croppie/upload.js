function demoUpload(canvasWidth, canvasHeight) {
    var $uploadCrop;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('.upload-demo').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    console.log('jQuery bind complete');
                });
                
            }
            
            reader.readAsDataURL(input.files[0]);
        } else {
            //alert("Sorry - you're browser doesn't support the FileReader API");
        }
        setTimeout(function() {
            $("#upload-result").trigger('click');
            $(".submitbutton").show();
        }, 1000);
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: canvasWidth,
            height: canvasHeight,
            type: 'square'
        },
        enableExif: true
    });

    $('#upload').on('change', function () { readFile(this); });
    $('.upload-result').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            $("#cropped_image").attr('src', resp);
            // popupResult({
            //     src: resp
            // });
        });
    });

}
function popupResult(result) {
    var html;
    if (result.html) {
        html = result.html;
    }
    if (result.src) {
        html = '<img src="' + result.src + '" />';
    }
    console.log(html);
}