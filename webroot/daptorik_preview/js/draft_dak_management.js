var DraftDakMovement = function () {

    var content = $('.inbox-content');
    var loading = $('.inbox-loading');
    var listListing = '';

    var loadDraft = function (el, name) {
    	var url = js_wb_root + "dakDaptoriks/inboxContent/"+name;
        var title = $('.nav-tabs > li.' + name + ' a').attr('data-title');
        listListing = name;

        loading.show();
        content.html('');
        toggleButton(el);

        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            success: function(res) 
            {
                toggleButton(el);

                $('.nav-tabs > li.active').removeClass('active');
                $('.nav-tabs > li.' + name).addClass('active');

                loading.hide();
                content.html(res);

                if (Layout.fixContentHeight) {
                    Layout.fixContentHeight();
                }
                Metronic.initUniform();

                $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
            },
            error: function(xhr, ajaxOptions, thrownError)
            {
                toggleButton(el);
            },
            async: false
        });

        // handle group checkbox:
        jQuery('body').on('change', '.mail-group-checkbox', function () {
            var set = jQuery('.mail-checkbox');
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                $(this).attr("checked", checked);
            });
            jQuery.uniform.update(set);
        });
    };

    var loadMessage = function (el, name, resetMenu) {
        var url = js_wb_root + "dakMovements/viewDak";

        loading.show();
        content.html('');
        toggleButton(el);

        var message_id = el.parent('tr').attr("data-messageid");  
        
        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            data: {'message_id': message_id},
            success: function(res) 
            {
                toggleButton(el);

                if (resetMenu) {
                    $('.nav-tabs > li.active').removeClass('active');
                }

                loading.hide();
                content.html(res);
                Layout.fixContentHeight();
                Metronic.initUniform();
                $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
               // ProjapotiCanvas.init('.img-src', false);

            },
            error: function(xhr, ajaxOptions, thrownError)
            {
                toggleButton(el);
            },
            async: false
        });
    };

    var initWysihtml5 = function () {
        $('.inbox-wysihtml5').wysihtml5({
            "stylesheets": [js_wb_root + "assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
        });
    };

    var initFileupload = function () {

        $('#fileupload').fileupload({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: js_wb_root + 'server/php/',
            autoUpload: true
        });

        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: js_wb_root + 'server/php/',
                type: 'HEAD'
            }).fail(function () {
                $('<span class="alert alert-error"/>')
                    .text('দুঃখিত! আপলোড সার্ভারে এখন সংযোগ দেয়া সম্ভব হচ্ছে না ')
                    .appendTo('#fileupload');
            });
        }
    };

    var loadDraftAfterSent = function (el, name) {
    	var messageid = $("#selected_dak_ids").val();
    	var url = js_wb_root + "dakDaptoriks/sendDak";
        
        loading.show();
        content.html('');
        toggleButton(el);

        $.ajax({
            type: "post",
            cache: false,
            url: url,
            dataType: "json",
            data:{'message_id':messageid},
            success: function(res) 
            {
            	loadDraft($(this), 'draft');
            },
            error: function(xhr, ajaxOptions, thrownError)
            {
                toggleButton(el);
            }
        });
    };
    var loadDetailMessgae = function (el, resetMenu) {
        var url = js_wb_root + "dakDaptoriks/viewDak";

        loading.show();
        content.html('');
        toggleButton(el);

        var message_id = el.attr("data-messageid");  
        
        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            data: {'message_id': message_id},
            success: function(res) 
            {
                toggleButton(el);

                if (resetMenu) {
                    $('.nav-tabs > li.active').removeClass('active');
                }

                loading.hide();
                content.html(res);
                Layout.fixContentHeight();
                Metronic.initUniform();
                $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
                //ProjapotiCanvas.init('.img-src', false);
            },
            error: function(xhr, ajaxOptions, thrownError)
            {
                toggleButton(el);
            }
        });
    };


    var toggleButton = function(el) {
        if (typeof el == 'undefined') {
            return;
        }
        if (el.attr("disabled")) {
            el.attr("disabled", false);
        } else {
            el.attr("disabled", true);
        }
    };

    return {
        //main function to initiate the module
        init: function (url) {

            // handle compose btn click
            $('.draft').on('click', '.compose-btn a', function () {
                //loadCompose($(this));
            });

            // handle discard btn
            $('.draft').on('click', '.inbox-discard-btn', function(e) {
                e.preventDefault();
                //loadDraft($(this), listListing);
            });

            // handle reply and forward button click

            // handle ডাক দেখুন
            $('.inbox-content').on('click', '.view-message', function () {
                loadMessage($(this));
            });
            
            
            $('.inbox-content').on('click', '.dak_draft_send_btn_single', function () {
            	$("#selected_dak_ids").val($(this).data('dak-id'));
            	loadDraftAfterSent($(this), 'draft');
            });
            
            $('.inbox-content').on('click', '.button-forward-all', function () {
            	loadDraftAfterSent($(this), 'draft');
            });

            
            $('.inbox-content').on('click', '.nav-tabs > li.new > a', function () {
                loadDraft($(this),'new');
            });
            
            $('.inbox-content').on('click', ' .nav-tabs > li.draft > a', function () {
                loadDraft($(this),'draft');
            });
            
            $('.inbox-content').on('click', ' .nav-tabs > li.my-draft > a', function () {
                loadDraft($(this),'my-draft');
            });

            
            $(document).on('click','.showDetailsDak',function(){
                loadDetailMessgae($(this));
            });

            // handle draft listing
            $('.inbox-nav > li.dispatched > a').click(function () {
                loadDraft($(this), 'dispatched');
            });

            // handle trash listing
            $('.inbox-nav > li.trash > a').click(function () {
                loadDraft($(this), 'trash');
            });

            $('.inbox-content').on('click', '.draft-discard-btn', function(e) {
                e.preventDefault();
                loadDraft($(this), "draft");
            });

            //handle loading content based on URL parameter
            if (Metronic.getURLParameter("a") === "view") {
                loadMessage();
            } else if (Metronic.getURLParameter("a") === "compose") {
                loadCompose();
            } else {

                if(typeof(url) !='undefined' && url != ''){
                    loadDraft($(this), url);
                }else{
                    loadDraft($(this), "draft");
                }
            }

        }

    };

}();