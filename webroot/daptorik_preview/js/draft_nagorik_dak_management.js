var DraftDakMovement = function () {

    var content = $('.inbox-content');
    var loading = $('.inbox-loading');
    var listListing = '';

    var loadDraft = function (el, name) {
    	var url = js_wb_root + "dakNagoriks/inboxContent/"+name;
        var title = $('.nav-tabs > li.' + name + ' a').attr('data-title');
        listListing = name;

        loading.show();
        content.html('');
        toggleButton(el);

        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            success: function(res) 
            {
                toggleButton(el);

                $('.nav-tabs > li.active').removeClass('active');
                $('.inbox-nav > li.' + name).addClass('active');
//                $('.inbox-header > h1').text(title);

                //alert(res);
                loading.hide();
                content.html(res);

                if (Layout.fixContentHeight) {
                    Layout.fixContentHeight();
                }
                Metronic.initUniform();

                $('[data-toggle="tooltip"]').tooltip();
            },
            error: function(xhr, ajaxOptions, thrownError)
            {
                toggleButton(el);
            },
            async: false
        });

        // handle group checkbox:
        jQuery('body').on('change', '.mail-group-checkbox', function () {
            var set = jQuery('.mail-checkbox');
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                $(this).attr("checked", checked);
            });
            jQuery.uniform.update(set);
        });
    };

    var loadMessage = function (el, name, resetMenu) {
        var url = js_wb_root + "dakMovements/viewDak";

        loading.show();
        content.html('');
        toggleButton(el);

        var message_id = el.parent('tr').attr("data-messageid");  
        
        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            data: {'message_id': message_id},
            success: function(res) 
            {
                toggleButton(el);

                if (resetMenu) {
                    $('.nav-tabs > li.active').removeClass('active');
                }
//                $('.inbox-header > h1').text('ডাক দেখুন');

                loading.hide();
                content.html(res);
                Layout.fixContentHeight();
                Metronic.initUniform();
                $('[data-toggle="tooltip"]').tooltip();
                ProjapotiCanvas.init('.img-src', false);
            },
            error: function(xhr, ajaxOptions, thrownError)
            {
                toggleButton(el);
            },
            async: false
        });
    };

    var initWysihtml5 = function () {
        $('.inbox-wysihtml5').wysihtml5({
            "stylesheets": ["../../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
        });
    };

    var initFileupload = function () {

        $('#fileupload').fileupload({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: js_wb_root + 'server/php/',
            autoUpload: true
        });

        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: js_wb_root + 'server/php/',
                type: 'HEAD'
            }).fail(function () {
                $('<span class="alert alert-error"/>')
                    .text('দুঃখিত! আপলোড সার্ভারে এখন সংযোগ দেয়া সম্ভব হচ্ছে না ')
                    .appendTo('#fileupload');
            });
        }
    };

    var loadCompose = function (el) {
        var url = 'inbox_compose.html';

        loading.show();
        content.html('');
        toggleButton(el);

        // load the form via ajax
        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            success: function(res) 
            {
                toggleButton(el);

                $('.nav-tabs > li.active').removeClass('active');
//                $('.inbox-header > h1').text('ডাক উত্তোলন করুন');

                loading.hide();
                content.html(res);

                initFileupload();
                initWysihtml5();

                $('.inbox-wysihtml5').focus();
                Layout.fixContentHeight();
                Metronic.initUniform();
            },
            error: function(xhr, ajaxOptions, thrownError)
            {
                toggleButton(el);
            },
            async: false
        });
    };
    
    var loadDraftAfterSent = function (el, name) {
    	var messageid = $("#selected_dak_ids").val();
    	var url = js_wb_root + "dakNagoriks/sendDak";
        var title = $('.nav-tabs > li.' + name + ' a').attr('data-title');
        listListing = name;

        loading.show();
        content.html('');
        toggleButton(el);

        $.ajax({
            type: "post",
            cache: false,
            url: url,
            dataType: "json",
            data:{'message_id':messageid},
            success: function(res) 
            {
            	loadDraft($(this), 'draft');
            },
            error: function(xhr, ajaxOptions, thrownError)
            {
                toggleButton(el);
            },
            async: false
        });
    };

    var loadReply = function (el) {
        var messageid = $(el).attr("data-messageid");
        var url = js_wb_root + 'dakMovements/dakReply?message_id=' + messageid;
        
        loading.show();
        content.html('');
        toggleButton(el);

        // load the form via ajax
        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            success: function(res) 
            {
                toggleButton(el);

                $('.nav-tabs > li.active').removeClass('active');
//                $('.inbox-header > h1').text('উত্তর দিন');

                loading.hide();
                content.html(res);
                $('[name="message"]').val($('#reply_email_content_body').html());

                handleCCInput(); // init "CC" input field

                //initFileupload();
                //initWysihtml5();
                Layout.fixContentHeight();
                Metronic.initUniform();
            },
            error: function(xhr, ajaxOptions, thrownError)
            {
                toggleButton(el);
            },
            async: false
        });
    };

    var loadSearchResults = function (el, name) {
        var url = 'inbox_search_result.html';

        loading.show();
        content.html('');
        toggleButton(el);

        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            success: function(res) 
            {
                toggleButton(el);

                $('.nav-tabs > li.active').removeClass('active');
                $('.inbox-header > h1').text('খুঁজুন');

                loading.hide();
                content.html(res);
                Layout.fixContentHeight();
                Metronic.initUniform();
            },
            error: function(xhr, ajaxOptions, thrownError)
            {
                toggleButton(el);
            },
            async: false
        });
    };
    
    var loadDetailMessgae = function (el, resetMenu) {
        var url = js_wb_root + "dakNagoriks/viewDak";

        loading.show();
        content.html('');
        toggleButton(el);

        var message_id = el.attr("data-messageid");  
        
        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            data: {'message_id': message_id},
            success: function(res) 
            {
                toggleButton(el);

                if (resetMenu) {
                    $('.nav-tabs > li.active').removeClass('active');
                }
                $('.inbox-header > h1').text('ডাক দেখুন');

                loading.hide();
                content.html(res);
                Layout.fixContentHeight();
                Metronic.initUniform();
                $('[data-toggle="tooltip"]').tooltip();
                ProjapotiCanvas.init('.img-src', false);
            },
            error: function(xhr, ajaxOptions, thrownError)
            {
                toggleButton(el);
            },
            async: false
        });
    };

    var handleCCInput = function () {
        var the = $('.inbox-compose .mail-to .inbox-cc');
        var input = $('.inbox-compose .input-cc');
        the.hide();
        input.show();
        $('.close', input).click(function () {
            input.hide();
            the.show();
        });
    };

    var handleBCCInput = function () {

        var the = $('.inbox-compose .mail-to .inbox-bcc');
        var input = $('.inbox-compose .input-bcc');
        the.hide();
        input.show();
        $('.close', input).click(function () {
            input.hide();
            the.show();
        });
    };

    var toggleButton = function(el) {
        if (typeof el == 'undefined') {
            return;
        }
        if (el.attr("disabled")) {
            el.attr("disabled", false);
        } else {
            el.attr("disabled", true);
        }
    };

    return {
        //main function to initiate the module
        init: function () {

            // handle compose btn click
            $('.draft').on('click', '.compose-btn a', function () {
                //loadCompose($(this));
            });

            // handle discard btn
            $('.draft').on('click', '.inbox-discard-btn', function(e) {
                e.preventDefault();
                //loadDraft($(this), listListing);
            });

            // handle reply and forward button click

            // handle ডাক দেখুন
            $('.inbox-content').on('click', '.view-message', function () {
                loadMessage($(this));
            });
            
            
            $('.inbox-content').on('click', '.dak_draft_send_btn_single', function () {
            	$("#selected_dak_ids").val($(this).data('dak-id'));
            	loadDraftAfterSent($(this), 'draft');
            });
            
            $('.inbox-content').on('click', '.button-forward-all', function () {
            	loadDraftAfterSent($(this), 'draft');
            });

            
            $('.inbox-content').on('click', '.nav-tabs > li.new > a', function () {
                loadDraft($(this),'new');
            });
            
            $('.inbox-content').on('click', '.nav-tabs > li.draft > a', function () {
                loadDraft($(this),'draft');
            });
            
            $('.inbox-content').on('click', '.nav-tabs > li.my-draft > a', function () {
                loadDraft($(this),'my-draft');
            });

            
            $(document).on('click','.showDetailsDak',function(){
                loadDetailMessgae($(this));
            });

            // handle draft listing
            $('.nav-tabs > li.dispatched > a').click(function () {
                loadDraft($(this), 'dispatched');
            });

            // handle trash listing
            $('.nav-tabs > li.trash > a').click(function () {
                loadDraft($(this), 'trash');
            });

            //handle compose/reply cc input toggle
            $('.inbox-content').on('click', '.mail-to .inbox-cc', function () {
                handleCCInput();
            });

            //handle compose/reply bcc input toggle
            $('.inbox-content').on('click', '.mail-to .inbox-bcc', function () {
                handleBCCInput();
            });
            
            $('.inbox-content').on('click', '.draft-discard-btn', function(e) {
                e.preventDefault();
                loadDraft($(this), "draft");
            });

            //handle loading content based on URL parameter
            if (Metronic.getURLParameter("a") === "view") {
                loadMessage();
            } else if (Metronic.getURLParameter("a") === "compose") {
                loadCompose();
            } else {
               //$('.inbox-nav > li.draft > a').click();
                loadDraft($(this), "draft");
            }

        }

    };

}();