var InboxDakMovement = function () {
	var content = $('.inbox-content');
	var detailscontent = $('.inbox-details');
	var loading = $('.inbox-loading');
	var conditions = '';
	var listListing = '';

	var loadInbox = function (el, name) {

		var url = js_wb_root + "dakMovements/inboxContent/" + name;
		var title = $('.nav-tabs > li.' + name + ' a').attr('data-title');

		conditions = "";
		loading.show();
		content.show();
		detailscontent.html('');
		detailscontent.hide();
		content.html('');
		toggleButton(el);

		$.ajax({
			type: "GET",
			cache: true,
			url: url,
			dataType: "text",
			success: function (res) {
				toggleButton(el);

				$('.nav-tabs > li.active').removeClass('active');
				$('.nav-tabs > li.' + name).addClass('active');
				$('.inbox-header > h1').text(title);

				loading.hide();
				content.html(res);
				if (Layout.fixContentHeight) {
					Layout.fixContentHeight();
				}
				Metronic.initUniform();
				$('.date-picker').datepicker({
					rtl: Metronic.isRTL(),
					orientation: "left",
					autoclose: true
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
				toggleButton(el);
			},
			async: false
		});

		// handle group checkbox:
		jQuery('body').on('change', '.mail-group-checkbox', function () {
			var set = jQuery('.mail-checkbox');
			var checked = jQuery(this).is(":checked");
			jQuery(set).each(function () {
				$(this).attr("checked", checked);
			});
			jQuery.uniform.update(set);
		});
	};

	var loadInboxDetailMessgae = function (el, name) {
		loading.show();
		content.hide();
		detailscontent.show();
		detailscontent.html('');

		var url = '';
		var dak_type = el.attr("data-dak-type");
		var archive = el.attr("data-archive");

		var url = "";

		if (dak_type == "Daptorik") {
			if (isEmpty(archive)) {
				url = js_wb_root + "dakMovements/viewDakDaptorik";
			} else {
				url = js_wb_root + "dakMovements/viewDakDaptorik/archive";
			}
		} else {
			if (isEmpty(archive)) {
				url = js_wb_root + "dakMovements/viewDakNagorik";
			} else {
				url = js_wb_root + "dakMovements/viewDakNagorik/archive";
			}
		}

		toggleButton(el);
		var message_id = el.attr("data-messageid");

		$.ajax({
			type: "GET",
			cache: true,
			url: url,
			dataType: "text",
			data: {
				'message_id': message_id,
				conditions: conditions,
				si: el.data("si"),
				totalRec: $('table#datatable_dak').children('tbody').children('tr').length
			},
			success: function (res) {
				toggleButton(el);

				$('.inbox-header > h1').text('ডাক দেখুন');

				loading.hide();
				detailscontent.html(res);
				Layout.fixContentHeight();
				Metronic.initUniform();
				$('[data-toggle="tooltip"]').tooltip({'placement': 'bottom'});
				// if ($('.projapoticanvas').length == 1) {
				//     ProjapotiCanvas.init('.img-src');
				// }
				scrollToDakDetail();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				toggleButton(el);
				loadInbox($(this), 'inbox');
			},
			async: false
		});
	};

	var removeInboxMessgae = function (el, name) {
		loading.show();

		var url = '';
		var dak_type = el.attr("data-dak-type");

		var url = js_wb_root + "dakMovements/deleteDak/" + dak_type;

		toggleButton(el);
		var message_id = el.attr("data-messageid");

		$.ajax({
			type: "post",
			cache: true,
			url: url,
			dataType: 'json',
			data: {'message_id': message_id},
			success: function (res) {
				toggleButton(el);

				loading.hide();

				if (res.status == 'success') {
					toastr.success(res.msg);
					loadInbox(el, 'inbox');
				} else {
					toastr.error(res.msg);
				}

			},
			error: function (xhr, ajaxOptions, thrownError) {
				toggleButton(el);
				loadInbox($(this), 'inbox');
			},
			async: false
		});
	};

	var loadSentDetailMessgae = function (el, name) {

		var message_id = el.attr("data-messageid");
		var dak_type = el.attr("data-dak-type");

		if (dak_type == "Nagorik")
			var url = js_wb_root + "dakMovements/viewDakNagorikSent";
		else
			var url = js_wb_root + "dakMovements/viewDakDaptorikSent";

		loading.show();
		content.hide();
		detailscontent.show();
		detailscontent.html('');

		toggleButton(el);

		$.ajax({
			type: "GET",
			cache: true,
			url: url,
			dataType: "text",
			data: {
				'message_id': message_id,
				conditions: conditions,
				si: el.data("si"),
				totalRec: $('table#datatable_dak').children('tbody').children('tr').length
			},
			success: function (res) {
				toggleButton(el);

				$('.inbox-header > h1').text('ডাক দেখুন');

				loading.hide();
				detailscontent.html(res);

				Layout.fixContentHeight();
				Metronic.initUniform();
				$('[data-toggle="tooltip"]').tooltip({'placement': 'bottom'});
				// if ($('.projapoticanvas').length == 1) {
				//     ProjapotiCanvas.init('.img-src', false);
				// }
				scrollToDakDetail();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				toggleButton(el);
			},
			async: false
		});
	};

	var loadNotiVuktoDetailMessgae = function (el, name) {

		loading.show();
		content.hide();
		detailscontent.show();
		detailscontent.html('');

		toggleButton(el);

		var url = '';

		var dak_type = el.attr("data-dak-type");
		if (dak_type == "Nagorik")
			url = js_wb_root + "dakMovements/viewDakNagorikNothiVukto";
		else
			url = js_wb_root + "dakMovements/viewNothiVukto";

		var message_id = el.attr("data-messageid");

		$.ajax({
			type: "GET",
			cache: true,
			url: url,
			dataType: "text",
			data: {
				'message_id': message_id,
				conditions: conditions,
				si: el.data("si"),
				totalRec: $('table#datatable_dak').children('tbody').children('tr').length
			},
			success: function (res) {
				toggleButton(el);
				$('.inbox-header > h1').text('ডাক দেখুন');
				loading.hide();
				detailscontent.html(res);

				Layout.fixContentHeight();
				Metronic.initUniform();
				$('[data-toggle="tooltip"]').tooltip({'placement': 'bottom'});
				// if ($('.projapoticanvas').length == 1) {
				//     ProjapotiCanvas.init('.img-src', false);
				// }
				scrollToDakDetail();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				toggleButton(el);
			},
			async: false
		});
	};

	var loadNotiJatoDetailMessgae = function (el, name) {

		loading.show();
		content.hide();
		detailscontent.show();
		detailscontent.html('');

		toggleButton(el);

		var url = '';

		var dak_type = el.attr("data-dak-type");
		if (dak_type == "Nagorik")
			url = js_wb_root + "dakMovements/viewDakNagorikNothiJato";
		else
			url = js_wb_root + "dakMovements/viewNothiJato";

		var message_id = el.attr("data-messageid");

		$.ajax({
			type: "GET",
			cache: true,
			url: url,
			dataType: "text",
			data: {
				'message_id': message_id,
				conditions: conditions,
				si: el.data("si"),
				totalRec: $('table#datatable_dak').children('tbody').children('tr').length
			},
			success: function (res) {
				toggleButton(el);
				$('.inbox-header > h1').text('ডাক দেখুন');
				loading.hide();
				detailscontent.html(res);

				Layout.fixContentHeight();
				Metronic.initUniform();
				$('[data-toggle="tooltip"]').tooltip({'placement': 'bottom'});
				// if ($('.projapoticanvas').length == 1) {
				//     ProjapotiCanvas.init('.img-src', false);
				// }
				scrollToDakDetail();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				toggleButton(el);
			},
			async: false
		});
	};

	var initWysihtml5 = function () {
		$('.inbox-wysihtml5').wysihtml5({
			"stylesheets": [js_wb_root + "assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
		});
	};

	var initFileupload = function () {

		$('#fileupload').fileupload({
			// Uncomment the following to send cross-domain cookies:
			//xhrFields: {withCredentials: true},
			url: js_wb_root + 'server/php/',
			autoUpload: true
		});

		// Upload server status check for browsers with CORS support:
		if ($.support.cors) {
			$.ajax({
				url: js_wb_root + 'server/php/',
				type: 'HEAD'
			}).fail(function () {
				$('<span class="alert alert-error"/>')
					.text('দুঃখিত! আপলোড সার্ভারে এখন সংযোগ দেয়া সম্ভব হচ্ছে না')
					.appendTo('#fileupload');
			});
		}
	};

	var loadInboxAfterSent = function (el, name) {
		var messageid = $(el).attr("data-message-id");
		var to_id = $(el).attr("data-dak-to");
		var from_id = $(el).attr("data-dak-from");
		var url = js_wb_root + "dakMovements/sendDak";
		var title = $('.nav-tabs > li.' + name + ' a').attr('data-title');
		listListing = name;

		loading.show();
		content.html('');
		toggleButton(el);

		$.ajax({
			type: "post",
			cache: true,
			url: url,
			dataType: "json",
			data: {'message_id': messageid, 'to': to_id, 'from': from_id},
			success: function (res) {
				loadInbox($(this), 'sent');
			},
			error: function (xhr, ajaxOptions, thrownError) {
				toggleButton(el);
			},
			async: false
		});
	};

	var dakRevert = function (el) {
		var messageid = $(el).data("message-id");
		var messagetype = $(el).data("message-type");
		var url = js_wb_root + "dakMovements/dakRevert";


		loading.show();
		content.html('');
		toggleButton(el);

		$.ajax({
			type: "post",
			cache: true,
			url: url,
			dataType: "json",
			data: {'message_id': messageid, dak_type: messagetype},
			success: function (res) {
				if (res == 1) {
//                    loadInbox($(this), 'inbox');
					if ($('.showPaginateDetailsDakInbox').length > 0) {
						$('.showPaginateDetailsDakInbox').click();
					} else if ($('.showPaginateDetailsDakInbox').length > 0) {
						$('.showPaginateDetailsDakInbox').click();
					} else {
						loadInbox($(this), 'inbox');
					}
				} else {
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right"
					};
					toastr.error("দুঃখিত! ডাকটি ফেরত আনা সম্ভব হচ্ছে না");

				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				toggleButton(el);
			},
			async: false
		});
	};

	var dakRevertNothi = function (el) {
		var messageid = $(el).data("message-id");
		var messagetype = $(el).data("message-type");
		var url = js_wb_root + "dakMovements/dakRevertNothi";

		loading.show();
		content.html('');
		toggleButton(el);

		$.ajax({
			type: "post",
			cache: true,
			url: url,
			dataType: "json",
			data: {'message_id': messageid, dak_type: messagetype},
			success: function (res) {
				if (res == 1) {
					toastr.success("ডাকটি ফেরত আনা সম্ভব হয়েছে");
					loadInbox($(this), 'inbox');
//                    if ($('.showPaginateDetailsDakInbox').length > 0)
//                    {
//                        $('.showPaginateDetailsDakInbox').click();
//                    } else if ($('.showPaginateDetailsDakInbox').length > 0) {
//                        $('.showPaginateDetailsDakInbox').click();
//                    } else {
//                        loadInbox($(this), 'inbox');
//                    }
				} else {
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right"
					};
					toastr.error("দুঃখিত! ডাকটি ফেরত আনা সম্ভব হচ্ছে না");
					loading.hide();

				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				toggleButton(el);
				loading.hide();
			},
			async: false
		});
	};

	var dakForwardFromList = function (comment, selectedMessageId, to_officer_id, to_office_name, priority_level, to_officer_level, dak_type, currentShape, attachmentid, type) {

		if (typeof (currentShape) == "undefined")
			currentShape = "";
		if (typeof (type) == "undefined")
			type = "";

		if (typeof (attachmentid) == "undefined")
			attachmentid = "";

		if (typeof (priority_level) == "undefined")
			priority_level = "";
		if (typeof (to_officer_level) == "undefined")
			to_officer_level = "";
		if (typeof (dak_type) == "undefined")
			dak_type = "Daptorik";

		var messageid = selectedMessageId;

		var url = '';
		if (dak_type == "Daptorik") {
			url = js_wb_root + "dakMovements/forwardSelectedDaptorikDak";
		}
		else {
			url = js_wb_root + "dakMovements/forwardSelectedNagorikDak";
		}

		$.ajax({
			type: "post",
			cache: true,
			url: url,
			dataType: "json",
			data: {
				'messageId': messageid,
				'comment': comment,
				toOfficerId: to_officer_id,
				to_office_name: to_office_name,
				to_priority_level: priority_level,
				to_officer_level: to_officer_level,
				currentShape: {
					x: currentShape.x,
					y: currentShape.y,
					font: currentShape.font,
					color: currentShape.color
				},
				attachmentid: attachmentid
			},
			success: function (res) {
				toastr.options = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-bottom-right"
				};
				if (res != 0) {

					toastr.success("ডাক ফরোয়ার্ড করা হয়েছে");
					Metronic.unblockUI('.page-content-wrapper');
					Metronic.unblockUI('.dak_sender_cell_list');

					if (type == '') {
						if ($('.showPaginateDetailsDakInbox').length > 0) {
							$('.showPaginateDetailsDakInbox').click();
							var current_val = EngFromBn($('.btn-group-circle .red .badge-info').text());
							if (res.id != 1 && parseInt(current_val) > 0) {
								current_val--;
							}
							$('.btn-group-circle .red .badge-info').text(BnFromEng(current_val));
						} else if ($('.showPaginateDetailsDakInbox').length > 0) {
							$('.showPaginateDetailsDakInbox').click();
							var current_val = EngFromBn($('.btn-group-circle .red .badge-info').text());
							if (res.id != 1 && parseInt(current_val) > 0) {
								current_val--;
							}
							$('.btn-group-circle .red .badge-info').text(BnFromEng(current_val));
						} else {
							var current_val = EngFromBn($('.btn-group-circle .red .badge-info').text());
							if (res.id != 1 && parseInt(current_val) > 0) {
								current_val--;
							}
							$('.btn-group-circle .red .badge-info').text(BnFromEng(current_val));
							loadInbox($(this), 'inbox');
						}
					} else {
						var current_val = EngFromBn($('.btn-group-circle .red .badge-info').text());
						if (res.id != 1 && parseInt(current_val) > 0) {
							current_val--;
						}
						$('.btn-group-circle .red .badge-info').text(BnFromEng(current_val));
						loadInbox($(this), 'inbox');
					}
				} else {
					toastr.error("দুঃখিত! ডাক ফরোয়ার্ড করা সম্ভব হয়নি");
					Metronic.unblockUI('.page-content-wrapper');
					Metronic.unblockUI('.dak_sender_cell_list');
				}

			},
			error: function (xhr, ajaxOptions, thrownError) {
				toastr.options = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-bottom-right"
				};
				toastr.error("দুঃখিত! ডাক ফরোয়ার্ড করা সম্ভব হয়নি");
				Metronic.unblockUI('.page-content-wrapper');
				Metronic.unblockUI('.dak_sender_cell_list');
				loadInbox($(this), 'inbox');
			},
			async: true
		});
	};

	var toggleButton = function (el) {
		if (typeof el == 'undefined') {
			return;
		}
		if (el.attr("disabled")) {
			el.attr("disabled", false);
		} else {
			el.attr("disabled", true);
		}
	};

	$(document).off('click', '.btnNothiVukto');
	$(document).on('click', '.btnNothiVukto', function () {

		$('#selected_dak_id').val('');
		$('#selected_dak_type').val('');
		var selectedVal = '';
		var dak_type = '';
		var meta_data = '';

		$.each($('.dak_list_checkbox_to_select:checked'), function () {

			var meta = $(this).data('meta');
			if (typeof  meta.note_id != 'undefined' && meta.note_id > 0) {
				meta_data += parseInt(meta.note_id) + ',';
			}

			selectedVal += $(this).val() + ',';
			dak_type += $(this).data('dak-type') + ',';
		});

		if (selectedVal.length === 0) {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right"
			};
			toastr.error("দুঃখিত! ডাক বাছাই করা হয়নি।");
			return false;
		} else if (meta_data.length > 0 && $('.dak_list_checkbox_to_select:checked').length > 1) {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right"
			};
			toastr.error("দুঃখিত! ৩য় পক্ষ কর্তৃক পুনরায় আবেদনকৃত ডাক বাছাই করা হয়েছে।");
			return false;
		} else {
			$('#nothiVuktoKoron-modal').modal('show');
			$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
			$('.nothiVuktoContent').find('.modal-body').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
			$.ajax({
				url: js_wb_root + 'nothiMasters/userNothiList',
				dataType: (meta_data.length > 0 ? 'json' : 'html'),
				method: 'post',
				data: {
					'dak_id': selectedVal,
					'dak_type': dak_type,
					meta_data: (meta_data.length > 0 ? meta_data.substring(0, meta_data.length - 1) : meta_data)
				},
				success: function (response) {

					$('#selected_dak_id').val(selectedVal);
					$('#selected_dak_type').val(dak_type);
					$('#nothijato_input').val(0);

					if (meta_data.length > 0) {
						if (response.status == 'error') {
							toastr.options = {
								"closeButton": true,
								"debug": false,
								"positionClass": "toast-bottom-right"
							};
                            var message = '৩য় পক্ষ কর্তৃক পুনরায় আবেদনকৃত ডাক এর জন্য উপস্থাপিত নোট আপনার ডেস্ক এ নেই।';
							 message += (!isEmpty(response.details)?(' দয়া করে ডাকটি '+ response.details.EmployeeRecords.name_bng + ', ' + response.details.designation_label   + " ডেস্ক এ ফরোয়ার্ড করুন।"):'');
							toastr.error(message);
							$('#nothiVuktoKoron-modal').modal('hide');
							return false;
						}
						else {
							$('#selected_nothi_master_id').val(response.data.nothi_master_id);
							$('#selected_nothi_part_id').val(response.data.nothi_part_no);

							var url = js_wb_root + "nothiMasters/nothiVuktoKoron";
							$.ajax({
								url: url,
								data: $('#NothiVuktoKoronForm').serializeArray(),//Previous:  $(this).serializeArray(),
								type: "POST",
								async: false,
								dataType: 'JSON',
								success: function (response2) {
									if (response2.status === 'error') {
										toastr.options = {
											"closeButton": true,
											"debug": false,
											"positionClass": "toast-bottom-right"
										};
										toastr.error(response2.msg);
										$('#nothiVuktoKoron-modal').modal('hide');
										return false;

									} else if (response2.status === 'success') {
										toastr.options = {
											"closeButton": true,
											"debug": false,
											"positionClass": "toast-bottom-right"
										};
										toastr.success(response2.msg);
										$(this).find('span').removeClass('checked');

										var url2 = js_wb_root + 'noteDetail/' + response.data.nothi_part_no;
										window.location.href = url2;
									}
								},
								error: function (status, xresponse) {
									$('#nothiVuktoKoron-modal').modal('hide');
								}
							});
						}
					} else {
						$('.nothiVuktoContent').find('.modal-body').html(response);
					}

					Metronic.init();
					Metronic.initSlimScroll('.scroller');
				}
			});
		}
	});

	$(document).off('click', '.newNothiCreate');
	$(document).on('click', '.newNothiCreate', function () {
		$('.nothiCreateContent').show();
		$('.nothiVuktoContent').find('.modal-body').html('');
		$('.nothiVuktoContent').hide();

		var dak_subject = $(this).attr('dak_subject');
		var nothijato = $(this).attr('data-nothijato');

		$('.nothiCreateContent').find('.scroller').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
		$.ajax({
			url: js_wb_root + 'nothiMasters/add/0/add/' + dak_subject + '/' + nothijato,
			method: 'post',
			success: function (response) {
				$('.nothiCreateContent').find('.scroller').html(response);
				$('.nothi-add-menu').hide();
				$('.nothiCreateButton').hide();
				$('#nothi-types-id').select2();

				$('.bntNothiListShow').show();

				$('.dataTables_filter').css('float', 'left');
				$('.permissiondiv').show();

				Metronic.init();
				Metronic.initSlimScroll('.scroller');
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth() + 1; //January is 0!
				var yyyy = today.getFullYear();

				if (dd < 10) {
					dd = '0' + dd
				}

				if (mm < 10) {
					mm = '0' + mm
				}

				today = yyyy + '-' + mm + '-' + dd;
				$('.date-picker').datepicker({
					rtl: Metronic.isRTL(),
					orientation: "left",
					format: 'yyyy-mm-dd',
					endDate: today,
					autoclose: true
				});
			}
		});
	});

	$(document).off('click', '.bntNothiListShow');
	$(document).on('click', '.bntNothiListShow', function () {
		$('.nothiCreateContent').hide();
		$('.nothiCreateContent').find('.scroller').html('');
		$('.nothiVuktoContent').show();

		var nothijato = $(this).attr('data-nothijato');

		$('.nothiVuktoContent').find('.modal-body').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

		$.ajax({
			url: js_wb_root + 'nothiMasters/userNothiList',
			data: {nothijato: nothijato},
			type: 'post',
			success: function (response) {
				$('.nothiVuktoContent').find('.modal-body').html(response);
				Metronic.init();
				Metronic.initSlimScroll('.scroller');
			}
		});
	});

	$(document).off('click', '.nothiVuktoKoronSingle');
	$(document).on('click', '.nothiVuktoKoronSingle', function () {

		var selectedVal = $(this).data('id');
		var dak_type = $(this).data('dak-type');
		var meta_data = !isEmpty($(this).data('meta'))?$(this).data('meta'):0;
		if (selectedVal.length === 0) {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right"
			};
			toastr.error("দুঃখিত! ডাক বাছাই করা হয়নি।");
			return false;
		} else {
			$('#nothiVuktoKoron-modal').modal('show');
			$('.nothiVuktoContent').find('.modal-body').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
			$.ajax({
				url: js_wb_root + 'nothiMasters/userNothiList',
				dataType: (parseInt(meta_data) > 0 ? 'json' : 'html'),
				method: 'post',
				data: {'dak_id': selectedVal, 'dak_type': dak_type, meta_data: parseInt(meta_data)},
				success: function (response) {
					$('#selected_dak_id').val(selectedVal);
					$('#selected_dak_type').val(dak_type);
					$('#nothijato_input').val(0);

					if (parseInt(meta_data) > 0) {
						if (response.status == 'error') {
							toastr.options = {
								"closeButton": true,
								"debug": false,
								"positionClass": "toast-bottom-right"
							};
                            var message = '৩য় পক্ষ কর্তৃক পুনরায় আবেদনকৃত ডাক এর জন্য উপস্থাপিত নোট আপনার ডেস্ক এ নেই।';
                            message += (!isEmpty(response.details)?(' দয়া করে ডাকটি '+ response.details.EmployeeRecords.name_bng + ', ' + response.details.designation_label   + " ডেস্ক এ ফরোয়ার্ড করুন।"):'');
                            toastr.error(message);
							$('#nothiVuktoKoron-modal').modal('hide');
							return false;
						}
						else {
							$('#selected_nothi_master_id').val(response.data.nothi_master_id);
							$('#selected_nothi_part_id').val(response.data.nothi_part_no);

							var url = js_wb_root + "nothiMasters/nothiVuktoKoron";
							$.ajax({
								url: url,
								data: $('#NothiVuktoKoronForm').serializeArray(),
								type: "POST",
								async: false,
								dataType: 'JSON',
								success: function (response2) {
									if (response2.status === 'error') {
										toastr.options = {
											"closeButton": true,
											"debug": false,
											"positionClass": "toast-bottom-right"
										};
										toastr.error(response2.msg);
										$('#nothiVuktoKoron-modal').modal('hide');
										return false;

									} else if (response2.status === 'success') {
										toastr.options = {
											"closeButton": true,
											"debug": false,
											"positionClass": "toast-bottom-right"
										};
										toastr.success(response2.msg);
										$(this).find('span').removeClass('checked');

										var url2 = js_wb_root + 'noteDetail/' + response.data.nothi_part_no;
										window.location.href = url2;
									}
								},
								error: function (status, xresponse) {
									$('#nothiVuktoKoron-modal').modal('hide');
								}
							});
						}
					} else {
						$('.nothiVuktoContent').find('.modal-body').html(response);
					}

					Metronic.init();
					Metronic.initSlimScroll('.scroller');
				}
			});
		}
	});

	$(document).off('click', '.sourceNothiVuktoKoronSingle');
	$(document).on('click', '.sourceNothiVuktoKoronSingle', function () {

		var selectedVal = $(this).data('id');
		var dak_type = $(this).data('dak-type');
		var sarok_no = $(this).data('sarok-no');

		if (selectedVal.length === 0) {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right"
			};
			toastr.error("দুঃখিত! ডাক বাছাই করা হয়নি।");
			return false;
		} else {
			//$('#sourceNothiVuktoKoronSingle-modal').modal('show');
			//$('.sourceNothiVuktoKoronSingleContent').find('.modal-body').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
			$.ajax({
				url: js_wb_root + 'nothiMasters/userNothiListSelectedForSummeryNote',
				data: {'dak_id': selectedVal, 'dak_type': dak_type, 'sarok_no': sarok_no},
				method: 'post',
				dataType: 'html',
				success: function (return_response) {
					response = $.parseJSON(return_response);
					var nothi_part_no = response.msg.nothi_part_no;
					if (response.status == 'success') {
						$.ajax({
							url: js_wb_root + 'nothiMasters/nothiVuktoKoron',
							data: response.msg,
							method: 'post',
							dataType: 'text',
							success: function (return_response2) {
								response2 = $.parseJSON(return_response2);

								if (response2.status == 'success') {
									$.ajax({
										url: js_wb_root + 'nothiMasters/addNothiNotesTableForSummeryNote',
										data: {'sarok_no': sarok_no},
										method: 'post',
										dataType: 'text',
										success: function (return_response3) {
											response3 = $.parseJSON(return_response3);
											if (response3.status == 'success') {
												toastr.success(response3.msg);
												window.location.href = js_wb_root + 'noteDetail/' + nothi_part_no;
											} else {
												toastr.error(response3.msg);
											}
										}
									});
								} else {
									toastr.error(response2.msg);
								}
							}
						});
					} else {
						toastr.error(response.msg);
					}
				}
			});
		}
	});

	$(document).off('click', '#NothiVuktoKoronForm');
	$(document).on('submit', '#NothiVuktoKoronForm', function (e) {
		e.preventDefault();
		var that = this;
		if ($('#NothiVuktoKoronForm').find('.nothipermittedlist.active').length > 0 || $('#nothijato_input').val() == 1) {
			if ($('#NothiVuktoKoronForm').find('.selectPartNo.active').length > 0 || $('#nothijato_input').val() == 1) {
				if ($('#nothijato_input').val() == 1) {
					var nothi_id = $("#selected_nothi_part_id").val();
					var nothi_master_id = $("#selected_nothi_master_id").val();
				} else {
					var nothi_id = $('#NothiVuktoKoronForm').find('.selectPartNo.active').find('td').eq(0).attr('nothi_parts_id');
					var nothi_master_id = $('#NothiVuktoKoronForm').find('.selectPartNo.active').find('td').eq(0).attr('nothi_masters_id');
				}
				var goForAdvance = false;
				if ($('#nothijato_input').val() == 1) {
					bootbox.dialog({
						message: "আপনি কি ডাকটি নথিজাত করতে চান?",
						title: "ডাক নথিজাত",
						buttons: {
							success: {
								label: "হ্যাঁ",
								className: "green",
								callback: function () {
									Metronic.blockUI({
										target: '.nothiprmittedlst',
										boxed: true,
										message: 'অপেক্ষা করুন'
									});
									bootbox.hideAll();
									$(that).find('input[type=submit]').attr('disabled', 'disabled');
									setTimeout(function () {
										goForAdvance = true;
										if (goForAdvance == true) {
											if (nothi_id == 0) {

												if (nothi_master_id == 0 || typeof (nothi_master_id) == 'undefined') {
													toastr.options = {
														"closeButton": true,
														"debug": false,
														"positionClass": "toast-bottom-right"
													};
													Metronic.unblockUI('.nothiprmittedlst');
													$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
													toastr.error("দুঃখিত! নথি বাছাই করা হয়নি।");
													return false;
												} else {


													$('#NothiVuktoKoronForm').find('#selected_nothi_master_id').val(nothi_master_id);

													$.ajax({
														url: js_wb_root + "nothiMasters/add",
														data: {
															formData: $('#NothiVuktoKoronForm').serializeArray(),
															priviliges: []
														},
														type: "POST",
														async: false,
														dataType: 'JSON',
														success: function (response) {

															if (response.status == 'error') {

																toastr.error(response.msg);
																$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
																return false;
															} else if (response.status == 'success') {
																nothi_id = response.id;
																$('#NothiVuktoKoronForm').find('#selected_nothi_part_id').val(response.id);
															}
															Metronic.unblockUI('.nothiprmittedlst');
														},
														error: function (status, xresponse) {
															Metronic.unblockUI('.nothiprmittedlst');
															$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
														}

													});

												}
											} else {
												$('#NothiVuktoKoronForm').find('#selected_nothi_master_id').val(nothi_master_id);
												$('#NothiVuktoKoronForm').find('#selected_nothi_part_id').val(nothi_id);

											}


											if (nothi_id <= 0) {
												$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
												toastr.options = {
													"closeButton": true,
													"debug": false,
													"positionClass": "toast-bottom-right"
												};
												Metronic.unblockUI('.nothiprmittedlst');
												toastr.error("দুঃখিত! নথি বাছাই করা হয়নি।");
												return false;
											}

											var url = js_wb_root + "nothiMasters/nothiVuktoKoron";
											$.ajax({
												url: url,
												data: $('#NothiVuktoKoronForm').serializeArray(),
												type: "POST",
												async: false,
												dataType: 'JSON',
												success: function (response) {

													if (response.status === 'error') {
														Metronic.unblockUI('.nothiprmittedlst');
														//$('#nothijato_input').val(0);
														$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
														toastr.options = {
															"closeButton": true,
															"debug": false,
															"positionClass": "toast-bottom-right"
														};
														toastr.error(response.msg);
														return false;

													} else if (response.status === 'success') {
														toastr.options = {
															"closeButton": true,
															"debug": false,
															"positionClass": "toast-bottom-right"
														};
														toastr.success(response.msg);
														$(this).find('span').removeClass('checked');
														$('#NothiVuktoKoronForm')[0].reset();
														$('#nothiVuktoKoron-modal').modal('toggle');

														if ($('#nothijato_input').val() == 1) {
															var current_val = EngFromBn($('.btn-group-circle .red .badge-info').text());
															if (parseInt(current_val) > 0) {
																current_val--;
															}
															$('.btn-group-circle .red .badge-info').text(BnFromEng(current_val));
															loadInbox($(this), 'inbox');
														} else {
															var url = js_wb_root + 'noteDetail/' + nothi_id;

															window.location.href = url;
														}
														Metronic.unblockUI('.nothiprmittedlst');
													}
												},
												error: function (status, xresponse) {
													$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
													Metronic.unblockUI('.nothiprmittedlst');
												}
											});
										}
										else {
											$(that).find('input[type=submit]').removeAttr('disabled');
										}
									}, 500);
								}
							},
							danger: {
								label: "না",
								className: "red",
								callback: function () {
									return true;
//                                    Metronic.unblockUI('.page-container');
								}
							}
						}
					});
				}
				else {
					bootbox.dialog({
						message: "আপনি কি ডাকটি নথিতে পেশ/উপস্থাপন করতে চান?",
						title: "ডাক নথিতে পেশ/উপস্থাপন",
						buttons: {
							success: {
								label: "হ্যাঁ",
								className: "green",
								callback: function () {
									bootbox.hideAll();
									Metronic.blockUI({
										target: '.nothiprmittedlst',
										boxed: true,
										message: 'অপেক্ষা করুন'
									});
									$(that).find('input[type=submit]').attr('disabled', 'disabled');
									// $('#nothiVuktoKoron-modal').modal('hide');
									setTimeout(function () {
										goForAdvance = true;
										if (goForAdvance == true) {
											if (nothi_id == 0) {

												if (nothi_master_id == 0 || typeof (nothi_master_id) == 'undefined') {
													toastr.options = {
														"closeButton": true,
														"debug": false,
														"positionClass": "toast-bottom-right"
													};
													Metronic.unblockUI('.nothiprmittedlst');
													$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
													toastr.error("দুঃখিত! নথি বাছাই করা হয়নি।");
													return false;
												} else {


													$('#NothiVuktoKoronForm').find('#selected_nothi_master_id').val(nothi_master_id);

													$.ajax({
														url: js_wb_root + "nothiMasters/add",
														data: {
															formData: $('#NothiVuktoKoronForm').serializeArray(),
															priviliges: []
														},
														type: "POST",
														async: false,
														dataType: 'JSON',
														success: function (response) {

															if (response.status == 'error') {
																if (typeof(response.id) != 'undefined') {
																	nothi_id = response.id;
																	$('#NothiVuktoKoronForm').find('#selected_nothi_part_id').val(nothi_id);
																}
																else {
																	toastr.error(response.msg);
																	$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
																	return false;
																}
															} else if (response.status == 'success') {
																nothi_id = response.id;
																$('#NothiVuktoKoronForm').find('#selected_nothi_part_id').val(response.id);
															}
															Metronic.unblockUI('.nothiprmittedlst');
														},
														error: function (status, xresponse) {
															Metronic.unblockUI('.nothiprmittedlst');
															$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
														}

													});

												}
											} else {
												$('#NothiVuktoKoronForm').find('#selected_nothi_master_id').val(nothi_master_id);
												$('#NothiVuktoKoronForm').find('#selected_nothi_part_id').val(nothi_id);
											}


											if (nothi_id <= 0) {
												$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
												toastr.options = {
													"closeButton": true,
													"debug": false,
													"positionClass": "toast-bottom-right"
												};
												Metronic.unblockUI('.nothiprmittedlst');
												toastr.error("দুঃখিত! নথি বাছাই করা হয়নি।");
												return false;
											}

											var url = js_wb_root + "nothiMasters/nothiVuktoKoron";
											$.ajax({
												url: url,
												data: $('#NothiVuktoKoronForm').serializeArray(),//Previous:  $(this).serializeArray(),
												type: "POST",
												async: false,
												dataType: 'JSON',
												success: function (response) {

													if (response.status === 'error') {
														Metronic.unblockUI('.nothiprmittedlst');
														//$('#nothijato_input').val(0);
														$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
														toastr.options = {
															"closeButton": true,
															"debug": false,
															"positionClass": "toast-bottom-right"
														};
														toastr.error(response.msg);
														return false;

													} else if (response.status === 'success') {
														toastr.options = {
															"closeButton": true,
															"debug": false,
															"positionClass": "toast-bottom-right"
														};
														toastr.success(response.msg);
														$(this).find('span').removeClass('checked');
														$('#NothiVuktoKoronForm')[0].reset();
														$('#nothiVuktoKoron-modal').modal('toggle');

														if ($('#nothijato_input').val() == 1) {
															var current_val = EngFromBn($('.btn-group-circle .red .badge-info').text());
															if (parseInt(current_val) > 0) {
																current_val--;
															}
															$('.btn-group-circle .red .badge-info').text(BnFromEng(current_val));
															loadInbox($(this), 'inbox');
														} else {
															var url = js_wb_root + 'noteDetail/' + nothi_id;
															window.location.href = url;
														}
														Metronic.unblockUI('.nothiprmittedlst');
													}
												},
												error: function (status, xresponse) {
													$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
													Metronic.unblockUI('.nothiprmittedlst');
												}
											});
										} else {
											$(that).find('input[type=submit]').removeAttr('disabled');
										}
									}, 500);
								}
							},
							danger: {
								label: "না",
								className: "red",
								callback: function () {
									return true;
//                                    Metronic.unblockUI('.page-container');
								}
							}
						}
					});
				}

			}
			else {
				Metronic.unblockUI('.nothiprmittedlst');
				$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
				toastr.options = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-bottom-right"
				};
				toastr.error("দুঃখিত! নথি বাছাই করা হয়নি।");
				return false;
			}

		} else {
			Metronic.unblockUI('.nothiprmittedlst');
			$('#NothiVuktoKoronForm').find('input[type=submit]').removeAttr('disabled');
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right"
			};
			toastr.error("দুঃখিত! নথি বাছাই করা হয়নি।");
			return false;
		}
		Metronic.unblockUI('.nothiprmittedlst');

		return false;
	});

	$(document).off('click', '.btnNothiJato')
	$(document).on('click', '.btnNothiJato', function () {

		$('#selected_dak_id').val('');
		$('#selected_dak_type').val('');
		var selectedVal = '';
		var dak_type = '';
		var meta_data = '';
		$.each($('.dak_list_checkbox_to_select:checked'), function () {
			selectedVal += $(this).val() + ',';
			dak_type += $(this).data('dak-type') + ',';
			var meta = $(this).data('meta');
			if (typeof  meta.note_id != 'undefined' && meta.note_id > 0) {
				meta_data += parseInt(meta.note_id) + ',';
			}
		});
		if (selectedVal.length === 0) {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right"
			};
			toastr.error("দুঃখিত! ডাক বাছাই করা হয়নি।");
			return false;
		} else if (meta_data.length > 0) {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right"
			};
            toastr.error("দুঃখিত! ৩য় পক্ষ কর্তৃক পুনরায় আবেদনকৃত ডাক বাছাই করা হয়েছে।");
			return false;
		}
		else {
			$('#nothiVuktoKoron-modal').modal('show');
			$('.nothiVuktoContent').find('.modal-body').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
			$.ajax({
				url: js_wb_root + 'nothiMasters/userNothiList',
				dataType: 'html',
				method: 'post',
				data: {'dak_id': selectedVal, 'dak_type': dak_type, 'nothijato': 1, meta_data: meta_data},
				success: function (response) {
					$('.nothiVuktoContent').find('.modal-body').html(response);
					$('#selected_dak_id').val(selectedVal);
					$('#selected_dak_type').val(dak_type);
					$('#nothijato_input').val(1);

					Metronic.init();
					Metronic.initSlimScroll('.scroller');
				}
			});
		}
	});

	$(document).off('click', '.nothiJatoKoronSingle')
	$(document).on('click', '.nothiJatoKoronSingle', function () {

		var selectedVal = $(this).data('id');
		var dak_type = $(this).data('dak-type');
		var meta_data = !isEmpty($(this).data('meta'))?$(this).data('meta'):0;
		if (selectedVal.length === 0) {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right"
			};
			toastr.error("দুঃখিত! ডাক বাছাই করা হয়নি।");
			return false;
		} else {
			$('#nothiVuktoKoron-modal').modal('show');
			$('.nothiVuktoContent').find('.modal-body').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
			$.ajax({
				url: js_wb_root + 'nothiMasters/userNothiList',
				data: {'dak_id': selectedVal, 'dak_type': dak_type, 'nothijato': 1, meta_data: meta_data},
				method: 'post',
				dataType: 'html',
				success: function (response) {
					$('.nothiVuktoContent').find('.modal-body').html(response);
					$('#selected_dak_id').val(selectedVal);
					$('#nothijato_input').val(1);
					$('#selected_dak_type').val(dak_type);

					Metronic.init();
					Metronic.initSlimScroll('.scroller');
				}
			});
		}
	});

	return {
		//main function to initiate the module
		init: function (page) {
			if (typeof (page) != 'undefined' && page != '') {
				if (page == 'inbox')
					loadInbox($(this), "inbox");
				else if (page == 'sent')
					loadInbox($(this), "sent");
				else if (page == 'archive')
					loadInbox($(this), "archive");
			} else {
				loadInbox($(this), "inbox");
			}

			$('.inbox-details').on('click', '.inbox-discard-btn', function (e) {
				e.preventDefault();
				loadInbox($(this), "inbox");
			});

			$('.inbox-details').on('click', '.sent-discard-btn', function (e) {
				e.preventDefault();
				loadInbox($(this), "sent");
			});

			$('.inbox-details').on('click', '.nothivukto-discard-btn', function (e) {
				e.preventDefault();
				loadInbox($(this), "nothivukto");
			});

			$('.inbox-details').on('click', '.nothijato-discard-btn', function (e) {
				e.preventDefault();
				loadInbox($(this), "nothijato");
			});

			// handle reply and forward button click

			$('.inbox-content').on('click', '.dak_forward_btn', function () {
				loadInboxAfterSent($(this), 'sent');
			});

			// handle view message
			$('.inbox-details').on('click', '.dak_revert', function () {
				dakRevert($(this));
			});

			$('.inbox-details').on('click', '.dak_revert_nothi', function () {
				dakRevertNothi($(this));
			});

			$(document).on("click", '.showDetailsDakInbox', function () {
				conditions = $('.filter input').map(function () {
					var obj = {};
					var $item = $(this);
					obj[$item.attr('name')] = $item.val();
					return obj;
				}).get();

				loadInboxDetailMessgae($(this));
			});

			$(document).on("click", '.deleteThisDak', function () {
				conditions = $('.filter input').map(function () {
					var obj = {};
					var $item = $(this);
					obj[$item.attr('name')] = $item.val();
					return obj;
				}).get();
				var that = $(this);
				bootbox.dialog({
					message: "আপনি কি ডাকটি আর্কাইভ করতে ইচ্ছুক?",
					title: "ডাক আর্কাইভ",
					buttons: {
						success: {
							label: "হ্যাঁ",
							className: "green",
							callback: function () {
								removeInboxMessgae(that);
							}
						},
						danger: {
							label: "না",
							className: "red",
							callback: function () {
							}
						}
					}
				});
			});

			$(document).off("click", '.showPaginateDetailsDakInbox');
			$(document).on("click", '.showPaginateDetailsDakInbox', function () {

				if (typeof (js_paginate_type) != 'undefined') {
					if (js_paginate_type == 1) {
						var si = $(this).data('si');

						if ($('table#datatable_dak').children('tbody').children('tr').length < si) {
							si = 1;
						}

						si = si - 1;

						$('.inbox-content').find('.showDetailsDakInbox').eq(si).click();
					} else {
						conditions = $('.filter input').map(function () {
							var obj = {};
							var $item = $(this);
							obj[$item.attr('name')] = $item.val();
							return obj;
						}).get();

						loadInboxDetailMessgae($(this));
					}
				} else {
					var si = $(this).data('si');

					if ($('table#datatable_dak').children('tbody').children('tr').length < si) {
						si = 1;
					}

					si = si - 1;

					$('.inbox-content').find('.showDetailsDakInbox').eq(si).click();
				}

			});

			$(document).off('click', '.showDetailsDakSent');
			$(document).on('click', '.showDetailsDakSent', function () {
				conditions = $('.filter input').map(function () {
					var obj = {};
					var $item = $(this);
					obj[$item.attr('name')] = $item.val();
					return obj;
				}).get();
				loadSentDetailMessgae($(this));
			});

			$(document).off('click', '.showDetailsNothiVukto');
			$(document).on('click', '.showDetailsNothiVukto', function () {
				conditions = $('.filter input').map(function () {
					var obj = {};
					var $item = $(this);
					obj[$item.attr('name')] = $item.val();
					return obj;
				}).get();
				loadNotiVuktoDetailMessgae($(this));
			});

			$(document).off('click', '.showDetailsNothiJato');
			$(document).on('click', '.showDetailsNothiJato', function () {
				conditions = $('.filter input').map(function () {
					var obj = {};
					var $item = $(this);
					obj[$item.attr('name')] = $item.val();
					return obj;
				}).get();
				loadNotiJatoDetailMessgae($(this));
			});

			$(document).off('click', '.showPaginateDetailsDakSent');
			$(document).on('click', '.showPaginateDetailsDakSent', function () {

				if (typeof (js_paginate_type) != 'undefined') {
					if (js_paginate_type == 1) {
						var si = $(this).data('si');

						if ($('table#datatable_dak').children('tbody').children('tr').length < si) {
							si = 1;
						}

						si = si - 1;

						$('.inbox-content').find('.showDetailsDakSent').eq(si).click();

					} else {
						conditions = $('.filter input').map(function () {
							var obj = {};
							var $item = $(this);
							obj[$item.attr('name')] = $item.val();
							return obj;
						}).get();
						loadSentDetailMessgae($(this));
					}
				} else {
					var si = $(this).data('si');

					if ($('table#datatable_dak').children('tbody').children('tr').length < si) {
						si = 1;
					}

					si = si - 1;

					$('.inbox-content').find('.showDetailsDakSent').eq(si).click();

				}
			});

			$(document).off('click', '.showPaginateDetailsNothiVuktoDak');
			$(document).on('click', '.showPaginateDetailsNothiVuktoDak', function () {

				if (typeof (js_paginate_type) != 'undefined') {
					if (js_paginate_type == 1) {
						var si = $(this).data('si');

						if ($('table#datatable_dak').children('tbody').children('tr').length < si) {
							si = 1;
						}

						si = si - 1;

						$('.inbox-content').find('.showDetailsNothiVukto').eq(si).click();

					} else {
						conditions = $('.filter input').map(function () {
							var obj = {};
							var $item = $(this);
							obj[$item.attr('name')] = $item.val();
							return obj;
						}).get();

						loadNotiVuktoDetailMessgae($(this));
					}
				} else {
					var si = $(this).data('si');

					if ($('table#datatable_dak').children('tbody').children('tr').length < si) {
						si = 1;
					}

					si = si - 1;

					$('.inbox-content').find('.showDetailsNothiVukto').eq(si).click();

				}
			});

			$(document).off('click', '.showPaginateDetailsNothiJatoDak');
			$(document).on('click', '.showPaginateDetailsNothiJatoDak', function () {

				if (typeof (js_paginate_type) != 'undefined') {
					if (js_paginate_type == 1) {
						var si = $(this).data('si');

						if ($('table#datatable_dak').children('tbody').children('tr').length < si) {
							si = 1;
						}

						si = si - 1;

						$('.inbox-content').find('.showDetailsNothiJato').eq(si).click();

					} else {
						conditions = $('.filter input').map(function () {
							var obj = {};
							var $item = $(this);
							obj[$item.attr('name')] = $item.val();
							return obj;
						}).get();

						loadNotiVuktoDetailMessgae($(this));
					}
				} else {
					var si = $(this).data('si');

					if ($('table#datatable_dak').children('tbody').children('tr').length < si) {
						si = 1;
					}

					si = si - 1;

					$('.inbox-content').find('.showDetailsNothiJato').eq(si).click();

				}
			});


			$('.inbox-content').on('click', '.nav-tabs > li.new > a', function (e) {
				e.preventDefault();
				loadInbox($(this), "new");
			});
			$('.inbox-content').on('click', ' li.archive > a', function (e) {
				e.preventDefault();
				loadInbox($(this), "archive");
			});

			$('.inbox-content').on('click', '.nav-tabs > li.inbox > a', function (e) {
				e.preventDefault();
				loadInbox($(this), "inbox");
			});

			$('.inbox-content').on('click', '.nav-tabs > li.sent > a', function (e) {
				e.preventDefault();
				loadInbox($(this), "sent");
			});

			$('.inbox-content').on('click', 'li.nothivukto > a', function (e) {
				e.preventDefault();
				loadInbox($(this), "nothivukto");
			});
			$('.inbox-content').on('click', 'li.nothijato > a', function (e) {
				e.preventDefault();
				loadInbox($(this), "nothijato");
			});

			$('.inbox-content').on('click', ' li.dispatched > a', function (e) {
				e.preventDefault();
				loadInbox($(this), "dispatched");
			});


			$('.inbox-content').on('change', '.extramenu', function (e) {
				e.preventDefault();
				var val = $(this).find('option:selected').val();
				if (val != '--')
					loadInbox($(this), val);
			});


			$('.nav-tabs > li.new > a').click(function () {
				loadInbox($(this), 'new');//prev->inbox
			});
			// handle inbox listing
			$('.nav-tabs > li.inbox > a').click(function () {
				loadInbox($(this), 'inbox');//prev->inbox
			});

			// handle sent listing
			$('.nav-tabs > li.sent > a').click(function () {
				loadInbox($(this), 'sent');
			});


			$('.inbox-content').on('click', '#forwardAllSelected', function () {
				$(this).tooltip('destroy');
				setTimeout(function () {
					$("#forwardAllSelected").tooltip();
				},1000);

				Metronic.blockUI({
					target: '.page-content-wrapper',
					boxed: true
				});
				var i = 0;
				var dakaction = $('.dak_list_checkbox_to_action').find('input[name=dak_actions]').val();
				var toofficer = $('.dak_list_checkbox_to_action input[name=to_officer_id]').val();
				var toofficername = $('.dak_list_checkbox_to_action input[name=to_officer_name]').val();
				var mainofficer = $('.dak_list_checkbox_to_action input[name=to_officer_level]').val();

				if (toofficer.length == 0) {
					error = true;
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right"
					};
					toastr.error("দুঃখিত! প্রাপক বাছাই করা হয়নি");
					Metronic.unblockUI('.page-content-wrapper');
					return;

				}
				if (mainofficer.length == 0) {
					error = true;
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right"
					};
					toastr.error("দুঃখিত! মুল প্রাপক বাছাই করা হয়নি");
					Metronic.unblockUI('.page-content-wrapper');
					return;

				}

				$('.dak_list_checkbox_to_select:checked').each(function () {
					var selectedVal = '';
					var dak_type = '';
					selectedVal = $(this).val();
					dak_type = $(this).data('dak-type');
					var error = false;

					if (selectedVal.length === 0) {
						error = true;
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right"
						};
						toastr.error("দুঃখিত! কোনো ডাক বাছাই করা হয়নি");
						Metronic.unblockUI('.page-content-wrapper');
						return;

					}
					dakForwardFromList(
						dakaction,
						selectedVal,
						toofficer,
						toofficername,
						$('.dak_list_checkbox_to_action select[name=dak_priority_level]').val(),
						mainofficer, dak_type, '', '', 'multi'
					);
					// var current_val = EngFromBn($('.btn-group-circle .red .badge-info').text());
					// if(parseInt(current_val)>0){
					// 	current_val--;
					// }
					// $('.btn-group-circle .red .badge-info').text(BnFromEng(current_val));

					$(this).closest('tr').children('td').attr('style', 'background-color: #f8fbfd;');
				});
				loadInbox($(this), 'inbox');
			});

			$('.inbox-content').on('click', '.forwardSingle', function () {
				Metronic.blockUI({
					target: '.dak_sender_cell_list ',
					boxed: true
				});

				var selectedVal = '';
				var dakType = '';
				selectedVal = $(this).closest('tr').parent().parent().closest('tr').find('.dak_list_checkbox_to_select').val();
				dakType = $(this).closest('tr').parent().parent().closest('tr').find('.dak_list_checkbox_to_select').attr('data-dak-type');

				var error = false;


				if ($(this).closest('table').parent().parent().find('input[name=to_officer_id]').val().length == 0) {
					error = true;
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right"
					};
					toastr.error("দুঃখিত! প্রাপক বাছাই করা হয়নি");
					Metronic.unblockUI('.dak_sender_cell_list');
					return;
				}

				if ($(this).closest('table').parent().parent().find('input[name=to_officer_level]').val().length == 0) {
					error = true;
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right"
					};
					toastr.error("দুঃখিত! মুল প্রাপক বাছাই করা হয়নি");
					Metronic.unblockUI('.dak_sender_cell_list');
					return;
				}

				if (selectedVal.length == 0) {
					error = true;
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right"
					};
					toastr.error("দুঃখিত! কোনো ডাক বাছাই করা হয়নি");
					Metronic.unblockUI('.dak_sender_cell_list');
					return;
				}
				/*
				 if ($(this).closest('table').find('input[name=dak_actions]').val().length == 0) {
				 error = true;
				 }*/

				if (error) {
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right"
					};
					toastr.error("দুঃখিত! দয়া করে প্রয়োজনীয় তথ্য দিন");
					Metronic.unblockUI('.dak_sender_cell_list');
					return;
				}

				dakForwardFromList($(this).closest('table').find('input[name=dak_actions]').val(), selectedVal,
					$(this).closest('table').parent().parent().find('input[name=to_officer_id]').val(),
					$(this).closest('table').parent().parent().find('input[name=to_officer_name]').val(),
					$(this).closest('table').find('select[name=dak_priority_level]').val(),
					$(this).closest('table').parent().parent().find('input[name=to_officer_level]').val(), dakType
				);
			});

			$('.inbox-details').on('click', '.forwardSingleBtn', function () {
				Metronic.blockUI({
					target: '.page-content-wrapper',
					boxed: true
				});

				var selectedVal = '';
				selectedVal = $(this).attr('dak_id');

				var dakType = '';
				dakType = $(this).attr('dak_type');

				var error = false;
				if ($(this).parents('.inbox-details').find('input[name=to_officer_level]').val().length == 0) {
					error = true;
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right"
					};
					Metronic.unblockUI('.page-content-wrapper');
					toastr.error("দুঃখিত! মুল প্রাপক বাছাই করা হয়নি");
					return;
				}

				if ($(this).parents('.inbox-details').find('input[name=to_officer_id]').val().length == 0) {
					error = true;
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right"
					};
					toastr.error("দুঃখিত! প্রাপক বাছাই করা হয়নি");
					Metronic.unblockUI('.page-content-wrapper');
					return;
				}

				if (selectedVal.length == 0) {
					error = true;
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right"
					};
					toastr.error("দুঃখিত! কোনো ডাক বাছাই করা হয়নি");
					Metronic.unblockUI('.page-content-wrapper');
					return;
				}

				if (error) {
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right"
					};
					toastr.error("দুঃখিত! দয়া করে প্রয়োজনীয় তথ্য দিন");
					Metronic.unblockUI('.page-content-wrapper');
					return;
				}

				dakForwardFromList(
					$(this).parents('.inbox-details').find('input[name=dak_actions]').last().val(), selectedVal,
					$(this).parents('.inbox-details').find('input[name=to_officer_id]').val(),
					$(this).parents('.inbox-details').find('input[name=to_officer_name]').val(),
					$(this).parents('.inbox-details').find('select[name=dak_priority_level]').val(),
					$(this).parents('.inbox-details').find('input[name=to_officer_level]').val(), dakType
				);

			});

			$('.inbox-content').on('click', '#forwardAllSelectedWithMultipleComment', function () {


				$('.dak_list_checkbox_to_select:checked').each(function () {
					var selectedVal = '';
					selectedVal = $(this).val();

					var dak_type = $(this).data('dak-type');
					var error = false;

					if ($(this).closest('tr').find('.dak_sender_cell_list').find('input[name=to_officer_id]').val().length == 0) {
						error = true;
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right"
						};
						toastr.error("দুঃখিত! প্রাপক বাছাই করা হয়নি");
						return;

					}

					if ($(this).closest('tr').find('.dak_sender_cell_list').find('input[name=to_officer_level]').val().length == 0) {
						error = true;
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right"
						};
						toastr.error("দুঃখিত! মুল প্রাপক বাছাই করা হয়নি");
						return;
					}

					if (selectedVal.length == 0) {
						error = true;
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right"
						};
						toastr.error("দুঃখিত! কোনো ডাক বাছাই করা হয়নি");
						return;
					}


					if (error) {
						$(this).closest('tr').children('td').attr('style', 'background-color:rgba(255, 144, 144, 0.8)');
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right"
						};
						toastr.error("লাল চিহ্নিত ডাক ফরোয়ার্ড করা সম্ভব হচ্ছে না");
						return;
					} else {
						dakForwardFromList($(this).closest('tr').find('.dak_sender_cell_list').find('input[name=dak_actions]').val(), selectedVal,
							$(this).closest('tr').find('.dak_sender_cell_list').find('input[name=to_officer_id]').val(),
							$(this).closest('tr').find('.dak_sender_cell_list').find('input[name=to_officer_name]').val(),
							$(this).closest('tr').find('.dak_sender_cell_list').find('select[name=dak_priority_level]').val(),
							$(this).closest('tr').find('.dak_sender_cell_list').find('input[name=to_officer_level]').val(), dak_type
						);
						$(this).closest('tr').children('td').attr('style', 'background-color: #f8fbfd;');
					}

				});
			});

			//handle loading content based on URL parameter
			if (Metronic.getURLParameter("a") === "view") {
				loadMessage();
			}
		}

	};


}();

function scrollToDakDetail() {
	if ($("#ajax-content").is(':visible')) {
		$('html, body').animate({
			scrollTop: $("#ajax-content").offset().top - 50
		}, 500);
	} else if ($("#selectDesignation").is(':visible')) {
		$('html, body').animate({
			scrollTop: $("#selectDesignation").offset().top - 50
		}, 500);
	}
	else {
		$('html, body').animate({
			scrollTop: detailscontent.offset().top - 50
		}, 500);
	}

}