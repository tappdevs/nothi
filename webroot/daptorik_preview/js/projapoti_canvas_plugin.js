var ProjapotiCanvas = function () {
    var curImg = 0;
    var commentShape = [];
    
    return {
        init : function (imgel, optionHide) {
            $(imgel).eq(curImg).find('img').clone().appendTo('.projapoticanvas');

            $('.image-prev').attr('disabled', 'disabled');

            $(document).on('click', '.image-prev', function (e) {
                $('.projapoticanvas').html('');
                curImg--;
                $('.image-next').removeAttr('disabled');
                if (curImg <= 0) {
                    $(this).attr('disabled', 'disabled');
                    curImg = 0;
                }
                $(imgel).eq(curImg).find('img').clone().appendTo('.projapoticanvas');
            });

            $(document).on('click', '.image-next', function (e) {
                $('.projapoticanvas').html('');
                var totaldiv = $(imgel).length;
                curImg++;
                $('.image-prev').removeAttr('disabled');
                if (curImg >= totaldiv) {
                    $(this).attr('disabled', 'disabled');
                    curImg = totaldiv-1;
                }
                $(imgel).eq(curImg).find('img').clone().appendTo('.projapoticanvas');
            });
        },
        getCurImg : function(){
            return curImg;
        },
        getcommentShape : function(){
            return commentShape;
        },
        setAttachmentsComment : function (comments){
            this.comments = comments ;
        }
    };
}();