var ProjapotiCanvas = function () {
    var curImg = 0;
    var commentShape = [];
    var curImgId = -1;
    var countShape = 0;
    var comments = [];

    var MyTool = function (lc) {  // take lc as constructor arg
        var self = this;
        return {
            usesSimpleAPI: false,  // DO NOT FORGET THIS!!!
            name: 'Decision',
            iconName: 'decision',
            optionsStyle: null,
            didBecomeActive: function (lc) {
                var onPointerDown = function (pt) {
                    self.currentShape = LC.createShape('Text', {
                        x: pt.x,
                        y: pt.y,
                        text: $('.image-text').parents('.inbox-details').find('#dak-actions').val() + "\n" + $('.image-text').eq(0).val(),
                        color: '#D314D8',
                        font: 'italic 16px "Nikosh"',
                        v: 1
                    });
                    lc.currentShapeState = 'selected';
                    lc.setShapesInProgress([self.currentShape]);
                    lc.repaintLayer('main');
                    commentShape = self.currentShape;
                };

                var onPointerDrag = function (pt) {
                    self.currentShape.x = pt.x;
                    self.currentShape.y = pt.y;
                    lc.setShapesInProgress([self.currentShape]);
                    lc.repaintLayer('main');
                };

                var onPointerUp = function (pt) {
                    self.currentShape.x = pt.x;
                    self.currentShape.y = pt.y;

                    lc.setShapesInProgress([]);
                    lc.saveShape(self.currentShape);
                    countShape++;
                };

                var onPointerMove = function (pt) {
                    // console.log("Mouse moved to", pt);
                };

                var onRedo = function (lc) {
                    countShape++;
                }

                var onUndo = function (lc) {
                    countShape--;
                    if(countShape<=0) {
                        self.currentShape = [];
                        commentShape = [];
                    }
                }

                // lc.on() returns a function that unsubscribes us. capture it.
                self.unsubscribeFuncs = [
                    lc.on('lc-pointerdown', onPointerDown),
                    lc.on('lc-pointerdrag', onPointerDrag),
                    lc.on('lc-pointerup', onPointerUp),
                    lc.on('lc-pointermove', onPointerMove),
                    lc.on('redo', onRedo),
                    lc.on('undo', onUndo)
                ];
            },

            willBecomeInactive: function (lc) {
                // call all the unsubscribe functions
                self.unsubscribeFuncs.map(function (f) {
                    f()
                });
            }
        }
    };

    return {
        init : function (imgel, optionHide) {

            $('div.lc-drawing').html('');

            var lc = LC.init(
                document.getElementsByClassName('projapoticanvas')[0], {
                    imageURLPrefix: js_wb_root + 'webroot/daptorik_preview/img',
                    tools: LC.defaultTools.concat([MyTool]),
                }
            );  
    
            if(typeof(optionHide) != 'undefined' && optionHide === false){
                $('.lc-picker').remove();
                $('.lc-options').remove();
            }else{
                $('.lc-picker').append($('.lc-options'));
            }

            var newImage = new Image();
            newImage.src = $(imgel).find('img').eq(0).attr('src');
            curImgId =  $(imgel).find('img').eq(0).attr('id');

            lc.saveShape(LC.createShape('Image', {x: 0, y: 0, image: newImage}));
            comments = this.comments;

//            if(typeof comments != 'undefined' && typeof comments[curImgId] != 'undefined'){
//                $.each(comments[curImgId], function(i, ob){
//                    lc.saveShape(LC.createShape(
//                        'Text', {
//                            x: ob.position_x,
//                            y: ob.position_y,
//                            text: ob.comments + '\n' + ob.designation_label,
//                            font: ob.fonts,
//                            color: ob.color
//                       }));
//                });
//            }

            $('.image-prev').attr('disabled', 'disabled');

            $(document).on('click', '.image-prev', function (e) {
                curImg--;
                $('.image-next').removeAttr('disabled');
                if (curImg < 0) {
                    $(this).attr('disabled', 'disabled');
                    curImg = 0;
                }
                lc.clear();
                var newImage = new Image();
                newImage.src = $(imgel).find('img').eq(curImg).attr('src');
                curImgId =  $(imgel).find('img').eq(curImg).attr('id');
                lc.saveShape(LC.createShape('Image', {x: 0, y: 0, image: newImage}));
//
//                if(typeof comments[curImgId] != 'undefined'){
//                    $.each(comments[curImgId], function(i, ob){
//                        lc.saveShape(LC.createShape(
//                            'Text', {
//                                x: ob.position_x,
//                                y: ob.position_y,
//                                text: ob.comments + '\n' + ob.designation_label,
//                                font: ob.fonts,
//                                color: ob.color
//                            }));
//                    });
//                }

                countShape = 0;
                commentShape = [];
            });

            $(document).on('click', '.image-next', function (e) {
                var totaldiv = $(imgel).find('img').length;
                curImg++;
                $('.image-prev').removeAttr('disabled');
                if (curImg >= totaldiv) {
                    $(this).attr('disabled', 'disabled');
                    curImg = totaldiv - 1;
                }
                lc.clear();

                var newImage = new Image();
                newImage.src = $(imgel).find('img').eq(curImg).attr('src');
                curImgId =  $(imgel).find('img').eq(curImg).attr('id');
                lc.saveShape(LC.createShape('Image', {x: 0, y: 0, image: newImage}));

//                if(typeof comments[curImgId] != 'undefined'){
//                    $.each(comments[curImgId], function(i, ob){
//                        lc.saveShape(LC.createShape(
//                            'Text', {
//                                x: ob.position_x,
//                                y: ob.position_y,
//                                text: ob.comments + '\n' + ob.designation_label,
//                                font: ob.fonts,
//                                color: ob.color
//                            }));
//                    });
//                }

                countShape = 0;
                commentShape = [];
            });
        },
        getCurImg : function(){
            return curImg;
        },
        getcommentShape : function(){
            return commentShape;
        },
        getCurImgId : function(){
            return curImgId;
        },
        setAttachmentsComment : function (comments){
            this.comments = comments ;
        }
    };
}();