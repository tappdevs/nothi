//$(document).ready(function () {
    $('div.lc-drawing').html('');
    var lc = LC.init(
        document.getElementsByClassName('literally')[0], {imageURLPrefix: js_wb_root +'webroot/daptorik_preview/img'}
    );
    
    var curImg = 0;
    var newImage = new Image();
    newImage.src = $('.img-src').find('img').eq(0).attr('src');

    lc.saveShape(LC.createShape('Image', {x: 5, y: 5, image: newImage}));

    $('.lc-picker').append($('.lc-options'));
    $('.image-prev').attr('disabled','disabled');

    $(document).on('click', '.image-text', function (e) {
        e.preventDefault();
        var text = $('.image-text').parents().find('#dak-actions').val() +"\n"+$('.image-text').prev().find('input').eq(0).val()+","+$('.image-text').prev().find('input').eq(1).val();
        if (text.length > 0) {
            $('.text-tool-input').val(text);
//            lc.saveShape(LC.createShape('Text', {y: $('.text-tool-input').offset().top, x: $('.text-tool-input').offset().left ,v:1, text: text}));
            
        }
    });
    
    $(document).on('click', '.image-prev', function (e) {
        curImg--;
        $('.image-next').removeAttr('disabled');
        if (curImg < 0) {
            $(this).attr('disabled','disabled');
            curImg = 0;
        }
        lc.clear();
        var newImage = new Image();
        newImage.src = $('.img-src').find('img').eq(curImg).attr('src');
        lc.saveShape(LC.createShape('Image', {x: 5, y: 5, image: newImage}));
    });

    $(document).on('click', '.image-next', function (e) {
        var totaldiv = $('.img-src').find('img').length;
        curImg++;
        $('.image-prev').removeAttr('disabled');
        if (curImg >= totaldiv) {
            $(this).attr('disabled','disabled');
            curImg = totaldiv - 1;
        }
        lc.clear();
        
        var newImage = new Image();
        newImage.src = $('.img-src').find('img').eq(curImg).attr('src');
        lc.saveShape(LC.createShape('Image', {x: 5, y: 5, image: newImage}));
    });
   

