var DateRange = function () {

    return {

        //main function
        init: function () {
            Metronic.addResizeHandler(function () {
                jQuery('.vmaps').each(function () {
                    var map = jQuery(this);
                    map.width(map.parent().width());
                });
            });
        },

        initCalendar: function () {
            if (!jQuery().fullCalendar) {
                return;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var h = {};

            if ($('#calendar').width() <= 400) {
                $('#calendar').addClass("mobile");
                h = {
                    left: 'title, prev, next',
                    center: '',
                    right: 'today,month,agendaWeek,agendaDay'
                };
            } else {
                $('#calendar').removeClass("mobile");
                if (Metronic.isRTL()) {
                    h = {
                        right: 'title',
                        center: '',
                        left: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                } else {
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }
            }

            $('#calendar').fullCalendar('destroy'); // destroy the calendar
            $('#calendar').fullCalendar({ //re-initialize the calendar
                disableDragging: false,
                header: h,
                editable: true,
                events: [{
                    title: 'All Day',
                    start: new Date(y, m, 1),
                    backgroundColor: Metronic.getBrandColor('yellow')
                }, {
                    title: 'Long Event',
                    start: new Date(y, m, d - 5),
                    end: new Date(y, m, d - 2),
                    backgroundColor: Metronic.getBrandColor('blue')
                }, {
                    title: 'Repeating Event',
                    start: new Date(y, m, d - 3, 16, 0),
                    allDay: false,
                    backgroundColor: Metronic.getBrandColor('red')
                }, {
                    title: 'Repeating Event',
                    start: new Date(y, m, d + 6, 16, 0),
                    allDay: false,
                    backgroundColor: Metronic.getBrandColor('green')
                }, {
                    title: 'Meeting',
                    start: new Date(y, m, d + 9, 10, 30),
                    allDay: false
                }, {
                    title: 'Lunch',
                    start: new Date(y, m, d, 14, 0),
                    end: new Date(y, m, d, 14, 0),
                    backgroundColor: Metronic.getBrandColor('grey'),
                    allDay: false
                }, {
                    title: 'Birthday',
                    start: new Date(y, m, d + 1, 19, 0),
                    end: new Date(y, m, d + 1, 22, 30),
                    backgroundColor: Metronic.getBrandColor('purple'),
                    allDay: false
                }, {
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    backgroundColor: Metronic.getBrandColor('yellow'),
                    url: 'http://google.com/'
                }]
            });
        },

        initDashboardDaterange: function (startdate) {

            if (!jQuery().daterangepicker) {
                return;
            }

            if(typeof startdate == 'undefined'){
                startdate = 6;
            }

            moment.locale('bn');
            $('#dashboard-report-range').daterangepicker({
                    opens: (Metronic.isRTL() ? 'right' : 'left'),
                    startDate: moment().subtract(startdate,'d'),
                    endDate: moment(),
                    minDate: '01/01/2016',
                    maxDate: moment().endOf('month'),
                    dateLimit: {
                        days: 365 * 5
                    },
                    showDropdowns: false,
                    showWeekNumbers: true,
                    timePicker: false,
                    timePickerIncrement: 1,
                    timePicker12Hour: true,
                    ranges: {
                        'আজ': [moment(), moment()],
                        'গতকাল': [moment().subtract( 1,'d'), moment().subtract( 1,'d')],
                        'শেষ ৭ দিন': [moment().subtract( 6,'d'), moment()],
                        'সর্বশেষ ৩০ দিন': [moment().subtract( 29,'d'), moment()],
                        'এই মাস': [moment().startOf('month'), moment().endOf('month')],
                        'গত মাসে': [moment().subtract( 1,'month').startOf('month'), moment().subtract( 1,'month').endOf('month')]
                    },
                    buttonClasses: ['btn btn-sm'],
                    applyClass: ' blue',
                    cancelClass: 'default',
                    format: 'MM/DD/YYYY',
                    separator: ' to ',
                    locale: {
                        applyLabel: 'প্রয়োগ করুন ',
                        cancelLabel: 'বাতিল',
                        fromLabel: 'From',
                        toLabel: 'To',
                        customRangeLabel: 'কাস্টম বিন্যাস',
                        daysOfWeek: ['রবি', 'সোম', 'মঙ্গল', 'বুধ', 'বৃহ', 'শুক্র', 'শনি'],
                        monthNames: ['জানুয়ারি', 'ফেব্রুয়ারি', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর'],
                        firstDay: 1
                    }
                },
                function (start, end) {
                    
                    $('#dashboard-report-range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    startDate = moment(start).unix();
                    endDate = moment(end).unix();
                    moment.locale('en');
                    SortingDate.init(moment.unix(startDate).format("YYYY-MM-DD"), moment.unix(endDate).format("YYYY-MM-DD"));
                    moment.locale('bn');
                }
            );

            $('#dashboard-report-range span').html(moment().subtract(startdate,'d').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            $('#dashboard-report-range').show();
            moment.locale('en');
            SortingDate.init(moment().subtract(startdate,'d').format("YYYY-MM-DD"), moment().format("YYYY-MM-DD"));
            moment.locale('bn');

        }
    };

}();