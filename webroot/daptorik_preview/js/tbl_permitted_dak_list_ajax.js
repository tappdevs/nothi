var TableAjax = function () {

    var handleRecords = function (name, urls) {

        if (name == 'inbox') {
            $("#inbox #datatable_dak").dataTable().fnDestroy();
            $("#sent #datatable_dak").dataTable().fnDestroy();
        } else {
            $("#sent #datatable_dak").dataTable().fnDestroy();
            $("#inbox #datatable_dak").dataTable().fnDestroy();
        }

        setTimeout(function() {
            //do something special
        }, 2000);

        var grid = new Datatable();
        var url = js_wb_root + urls + name;
          
        grid.init({
            src: $("#"+name+" #datatable_dak"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function (grid) {
                // execute some code on ajax data load
                $('#forwardAllSelected').parents('tfoot').hide();

                if(name == 'draft' || name == 'my-draft'){

                    $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
                }else{
                    $('[data-toggle="tooltip"]').tooltip({container:'body','placement':'bottom'});
                }

                $('.dak_sender_cell_list').children('div').removeAttr('class').addClass('col-md-12');
                $('div.dak_sender_cell_list').hide();
                $('.forward-message').on('click', function () {
                    //$(this).find('.dak-recepiant-seal').html($('.dak_list_checkbox_to_action').html());
                    $(this).closest('div').parent().find('.dak_sender_cell_list').html('');
                    $(this).closest('div').parent().find('.dak_sender_cell_list').html($('.dak_list_checkbox_to_action').html()).toggle();
                    $(document).find('.dak_sender_cell_list').find('button#forwardAllSelected').removeAttr('id').addClass('forwardSingle');
                    $(document).find('.dak_sender_cell_list').find('button.forwardSingle').after('<button class="btn btn-sm btn-danger" onclick="javascript:$(this).parents(\'.dak_sender_cell_list\').toggle();" title="বন্ধ করুন" data-original-title="বন্ধ করুন"><i class="fs1 a2i_gn_close2"></i>বন্ধ করুন</button>');
                });

                $('.dak_list_checkbox_to_select').on('click', function () {
                    var check = false;
                    if ($('.dak_list_checkbox_to_select:checked').length > 0) {
                        check = true;
                    }

                    if (check == false) {
                        $('#forwardAllSelected').parents('tfoot').hide();
                    } else {
                        $('#forwardAllSelected').parents('tfoot').show();
                    }

                });
                $('.dak_draft_list_checkbox_to_select').on('click', function () {
                    var selected_daks = [];
                    $('.dak_draft_list_checkbox_to_select').each(function () {
                        if ($(this).is(':checked')) {
                            selected_daks.push($(this).val());
                        }
                    });
                    $("#selected_dak_ids").val(selected_daks);
                });
            },
            loadingMessage: 'লোড করা হচ্ছে...',
            dataTable: {
                "dom": "<'row'<'col-md-8 col-sm-12'li><'col-md-4 col-sm-12'<'table-group-actions pull-right'p>>r>t",

                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "bDestroy": true,
                "lengthMenu": [
                    [10, 20, 50, 100, 500],
                    ["১০", "২০", "৫০", "১০০", "৫০০"] // change per page values here
                ],
                "pageLength": (typeof ($.cookie("page_limit_dak")) != 'undefined'?$.cookie("page_limit_dak"):10), // default record count per page
                "ajax": {
                    "url": url // ajax source
                },
                //"columnDefs":sortarray,
                //"order":[sortdefault],
                "language": { // language settings
                    // metronic spesific
                    "metronicGroupActions": "_TOTAL_ রেকর্ড নির্বাচন করা হয়েছে:  ",
                    "metronicAjaxRequestGeneralError": "অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না।",

                    // data tables spesific
                    "lengthMenu": " _MENU_ ",
                    "info": "<span class='seperator'> </span>মোট _TOTAL_ টি ডাক আছে",
                    "infoEmpty": "",
                    "emptyTable": "কোনো ডাক নেই",
                    "zeroRecords": "কোনো ডাক নেই",
                    "paginate": {
                        "previous": "প্রথমটা",
                        "next": "পরবর্তীটা",
                        "last": "শেষেরটা",
                        "first": "প্রথমটা",
                        "page": "পাতা",
                        "pageOf": "এর"
                    }
                }
            }
        });

        $('.filter-cancel').click(function () {
             $('#filter_input').val('');
             $('#receiving_officer_name').val('');
             $('#search_date_from').val('');
             $('#search_date_to').val('');
            grid.resetFilter();
        });

        $('#filter_submit_btn1').off('click').on('click',function () {
            grid.setAjaxParam('dak_subject',$('#filter_input').val()) ;
            $('.filter input, .filter select').each(function () {
                var $item = $(this);
                grid.setAjaxParam($item.attr('name'),$item.val()) ;
            });
            grid.getDataTable().ajax.url = url; 
            grid.getDataTable().ajax.reload();
        });

        $('[name=datatable_dak_length]').on('change', function () {
            $.cookie('page_limit_dak',$(this).val());
        });

    };


    return {

        //main function to initiate the module
        init: function (name, url) {
            handleRecords(name, url);
        }

    };

}();