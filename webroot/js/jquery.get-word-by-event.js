	function getWordFromEvent(event) {
		var range, word;
		if (event.rangeParent && document.createRange) {
			range = document.createRange();
			range.setStart(event.rangeParent, event.rangeOffset);
			range.setEnd(event.rangeParent, event.rangeOffset);
			expandRangeToWord(range);
			word = range.toString();
			return word;
		// Webkit
		} else if (document.caretRangeFromPoint) {
			range = document.caretRangeFromPoint(event.clientX, event.clientY);
			expandRangeToWord(range);
			word = range.toString();
			return word;
		// Firefox for events without rangeParent
		} else if (document.caretPositionFromPoint) {
			var caret = document.caretPositionFromPoint(event.clientX, event.clientY);
			range = document.createRange();
			range.setStart(caret.offsetNode, caret.offset);
			range.setEnd(caret.offsetNode, caret.offset);
			expandRangeToWord(range);
			word = range.toString();
			range.detach();
			return word;
		} else {
			return null;
		}
	}

	function expandRangeToWord(range) {
		while (range.startOffset > 0) {
			if (range.toString().indexOf(' ') === 0) {
				range.setStart(range.startContainer, range.startOffset + 1);
				break;
			}
			range.setStart(range.startContainer, range.startOffset - 1);
		}
		while (range.endOffset < range.endContainer.length && range.toString().indexOf(' ') == -1) {
			range.setEnd(range.endContainer, range.endOffset + 1);
		}
        s = window.getSelection();
        s.removeAllRanges();
        s.addRange(range);
		return range.toString().trim();
	}