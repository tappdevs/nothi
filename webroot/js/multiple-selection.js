$(function() {
    $('#btn_leftSelected').click(function() {
        // pass id select lists as parameters
        moveItemsToLeft('#multiselect_left', '#multiselect_right');
    });
    $('#btn_rightSelected').on('click', function() {
        moveItemsToRight('#multiselect_left', '#multiselect_right');
    });
    $('#btn_leftAll').on('click', function() {
        moveAllItemsToSource('#multiselect_left', '#multiselect_right');
    });

    $('#btn_rightAll').on('click', function() {
        moveAllItemsToDest('#multiselect_left', '#multiselect_right');
    });

    $('#btn_move_up').click(function() {
        moveUp('#multiselect_right');
    });

    $('#btn_move_down').click(function() {
        moveDown('#multiselect_right');
    });
});

function moveItemsToRight(sourseSelect, destSelect) { // move selected items from left to right select list
    $(destSelect).append($(sourseSelect + ' option:selected').clone());
    $(sourseSelect + ' option:selected').css("display", "none").removeAttr("selected");
    $(destSelect + ' option').prop('selected', true);
}

function moveItemsToLeft(sourseSelect, destSelect) { // move back selected items from right to left select list
    $(destSelect + " option:selected").each(function() {
        $(sourseSelect + ' option[value=' + $(this).val() + ']').show().removeAttr("selected");
        $(this).remove();
    });
    $(destSelect + ' option').prop('selected', true);
}

function moveAllItemsToDest(sourseSelect, destSelect) { // move all items from left to right select list
    $(destSelect).append($(sourseSelect + ' option').clone());
    $(sourseSelect + ' option').css("display", "none").removeAttr("selected");
    $(destSelect + ' option').filter(function() {
        if ($(this).css("display") == "none") {
            $(this).remove();
        }

    });
    $(destSelect + ' option').prop('selected', true);
}

function moveAllItemsToSource(sourseSelect, destSelect) { // move back all available items from right to left select list
    $(sourseSelect + ' option').show().removeAttr("selected");
    $(destSelect + ' option').remove();
}

function moveUp(destSelect) { // move selected items one step up in right select list
    var op = $(destSelect + ' option:selected');
    op.first().prev().before(op);
}

function moveDown(destSelect) { // move selected items one step down in right select list
    var op = $(destSelect + ' option:selected');
    op.last().next().after(op);
}