var prefix = '';

$("#" + prefix + "office-ministry-id").bind('change', function () {
    OfficeOrgSelectionCell.loadMinistryWiseLayers(prefix);
});
$("#" + prefix + "office-layer-id").bind('change', function () {
    OfficeOrgSelectionCell.loadMinistryAndLayerWiseOfficeOrigin(prefix);
});
$("#" + prefix + "office-origin-id").bind('change', function () {
    OfficeOrgSelectionCell.loadOriginOffices(prefix);
});
$("#" + prefix + "office-id").bind('change', function () {
    OfficeOrgSelectionCell.loadOfficeUnits(prefix);
});

var OfficeOrgSelectionCell = {

    loadMinistryWiseLayers: function () {
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeSettings/loadLayersByMinistry',
            {'office_ministry_id': $("#" + prefix + "office-ministry-id").val()}, 'json',
            function (response) {
                PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-layer-id", response, "--বাছাই করুন--");
            });
    },
    loadMinistryAndLayerWiseOfficeOrigin: function () {
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeSettings/loadOfficesByMinistryAndLayer',
            {
                'office_ministry_id': $("#" + prefix + "office-ministry-id").val(),
                'office_layer_id': $("#" + prefix + "office-layer-id").val()
            }, 'json',
            function (response) {
                PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-origin-id", response, "--বাছাই করুন--");
            });
    },
    loadOriginOffices: function () {
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/loadOriginOffices',
            {'office_origin_id': $("#" + prefix + "office-origin-id").val()}, 'json',
            function (response) {
                PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-id", response, "--বাছাই করুন--");
            });
    },
    loadOfficeUnits: function () {
        $('#addData').html('');
        if(isEmpty($("#" + prefix + "office-id").val())){
            return;
        }
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/officeUnitsList',
            {'office_id': $("#" + prefix + "office-id").val()}, 'json',
            function (response) {
                Metronic.blockUI({target: '.portlet-body', boxed: true});
                var units =[];
                for (indx in response){
                    units.push(indx);
                }
                $('#changeNothiNumber').hide();

                OfficeOrgSelectionCell.loadAllNothiOfOffice(units,0);
            });
    },
    loadAllNothiOfOffice: function(response,indx){
       if(response.length <= indx){
           Metronic.unblockUI('.portlet-body');
           OfficeOrgSelectionCell.getOfficeDigitalNothiCode();
           return;
       }
       if(isEmpty(response[indx])){
           Metronic.unblockUI('.portlet-body');
           return;
       }
       var office_id =$("#" + prefix + "office-id").val();
       var url = js_wb_root+'SuperAdminNothiDetailUpdates?office_id='+office_id+'&unit_id='+response[indx];
        PROJAPOTI.ajaxLoadCallback(url,function(data){
            if (data.nothies.length > 0) {
                $.each(data.nothies, function (i, v) {
                    if (v.nothi_created_date == null || typeof (v.nothi_created_date) == 'undefined' || v.nothi_created_date == '') {
                        var dt = '';
                    } else {
                        var dt = v.nothi_created_date.substring(0, 10);
                    }
                    if(v.nothi_class==1){
                        n_class = 'ক';
                    } else if(v.nothi_class==2){
                        n_class = 'খ';
                    } else if(v.nothi_class==3){
                        n_class = 'গ';
                    } else {
                        n_class = 'ঘ';
                    }
                    html = '<tr class="text-center">' +
                        '<td id="no'+v.id+'" data-nothi="'+v.nothi_no+'">' + v.nothi_no + '</td>' +
                        '<td id="' + v.id + '">' + v.subject + '</td>' +
                        '<td id="c'+v.id+'" data-class="'+v.nothi_class+'">' + n_class + '</td>' +
                        '<td id="t'+v.id+'" data-type="'+v.nothi_types_id+'">' + v.nothi_types_name + '</td>' +
                        '<td>' + BnFromEng(dt) + '</td>' +
                        '</tr>';
                    $('#addData').append(html);
                });
            }
            OfficeOrgSelectionCell.loadAllNothiOfOffice(response,indx+1);
        });
    },
    getOfficeDigitalNothiCode: function () {
        Metronic.blockUI({target: '.portlet-body', boxed: true});
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeSettings/getOfficeDigitalNothiCode',
            {'office_id': $("#" + prefix + "office-id").val()}, 'json',
            function (response) {
                if(!isEmpty(response.data)){
                    $('#digital-nothi-code').val(response.data);
                    $('#changeNothiNumber').show();
                }else{
                    toastr.error('দুঃখিত! ডিজিটাল কোড পাওয়া যায়নি।');
                }
                Metronic.unblockUI('.portlet-body');
            });
    },
    gatherNothiNumber: function(){
        var nothis = [];
        $('.inbox-content table tbody tr').each(function(){
            nothis.push($(this).find('td:first').attr('id'));
        });
        OfficeOrgSelectionCell.changeNothiNumber(nothis,0);
    },
    changeNothiNumber: function(nothis,indx){
        if(nothis.length <= indx){
            toastr.info('সকল নথির তথ্য হালনাগাদ করার চেষ্টা করা হয়েছে। যেসব নথির তথ্য হালনাগাদ সম্ভব হয়নি,লাল রং দ্বারা চিহ্নিতকরণ এবং ত্রুটির কারণ নথি নম্বর এর সাথে যুক্ত করা হয়েছে।');
            Metronic.unblockUI('.portlet-body');
            return;
        }
        if(isEmpty(nothis[indx])){
            Metronic.unblockUI('.portlet-body');
            return;
        }
        var id = nothis[indx];
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'NothiMasters/changeNothiNumberAccordingNothiDigitalCode',
            {'nothi_id': id.replace('no',''),'office_id': $("#" + prefix + "office-id").val()}, 'json',
            function (response) {
                if(isEmpty(response) || response.status == 'error'){
                    $('#'+id).append(!isEmpty(response.message)?(' ('+response.message+' )'):'');
                    $('#'+id).closest('tr').addClass('danger');
                }else{
                    $('#'+id).append(!isEmpty(response.message)?(' ('+response.message+' )'):'');
                    $('#'+id).closest('tr').addClass('success');
                }
                OfficeOrgSelectionCell.changeNothiNumber(nothis,indx+1);
            });

    }
};
$(document).ready(function() {
    $("select").select2();
    $('#changeNothiNumber').on('click',function(){
        bootbox.dialog({
            message: "পরবর্তী কার্যক্রমের জন্য অগ্রসর হলে নির্বাচিত অফিসের সকল নথি নাম্বারের প্রথম ৮ ডিজিট, অফিসের ডিজিটাল নথি কোড দিয়ে পরিবর্তন হয়ে যাবে।আপনি কি সকল নথি  নাম্বার পরিবর্তন করতে ইচ্ছুক?",
            title: "সকল নথি নাম্বার পরিবর্তন",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        Metronic.blockUI({target: '.portlet-body', boxed: true,'message' : 'সকল নথি নাম্বার পরিবর্তন সময়সাপেক্ষ ব্যাপার।দয়া করে অপেক্ষা করুন।'});
                        OfficeOrgSelectionCell.gatherNothiNumber();
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                    }
                }
            }
        });
    });
});
