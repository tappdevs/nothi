jQuery(document).ready(function () {
    $("#showlist").html('');
    // $('.table').floatThead({
    //     position: 'absolute',
    //     top: jQuery("div.navbar-fixed-top").height()
    // });
    // $('.table').floatThead('reflow');
});
var office_ids = [];
var office_name = [];
jQuery(document).ready(function () {
    $(this).find('body').addClass('page-sidebar-closed');
    $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
    DateRange.init();
    DateRange.initDashboardDaterange();
    $("#category-id").bind('change', function () {
        var tables = $.fn.dataTable.fnTables();
        var tables2destroy = [];
        $(tables).each(function () {
            var dtId = $(this).attr('id');
            if(!isEmpty(dtId)){
                tables2destroy.push(dtId);
            }
        });
        tables2destroy = tables2destroy.reverse();
        for(dtId in tables2destroy){
            if ( $.fn.DataTable.isDataTable( '#'+dtId ) ) {
                $('#'+dtId).dataTable().fnDestroy();
            }
        }
        $('.excel-export').remove();
        $('#DesignationPerformance').find('table:not(:first)').remove();
        $('#DesignationPerformance').find('table:first').find('tbody').html('');

        if ($(this).val() == -1) {
            $.each($('#category-id option[value!=0][value!=-1]'), function(key, value) {
                var tableId = 'categoryTable_'+$(value).val();
                if (key == 0) {
                    $('#DesignationPerformance').find('table').attr('id', tableId).css('margin-bottom', '20px');
                } else {
                    $('#DesignationPerformance').find('table').first().clone().attr('id', tableId).css('margin-bottom', '20px').appendTo('#DesignationPerformance');
                }
                getOfficesId($(value).val(), tableId);
            });
        } else if (!isEmpty($(this).val())) {
            var tableId = 'categoryTable_'+$(this).val();
            $('#DesignationPerformance').find('table').attr('id', tableId);
            getOfficesId($(this).val(), tableId);
        }
        //$('#addData').html('');
    });
});
function getOfficesId(category_id, tableId) {
    $('#showlist').html('<img src="'+js_wb_root+'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
    $.ajax({
        type: 'POST',
        url: js_wb_root+"category-wise-office-performance",
        dataType: 'json',
        data: {"category_id": category_id},
        // async: false,
        success: function (response) {
            if(!isEmpty(response) && response.status == 'success'){
                office_ids[category_id] = [];
                office_name[category_id] = [];
                var ind = 0;
                $.each(response.data, function (i, v) {
                    office_ids[category_id].push(i);
                    office_name[category_id].push(v);

                });
                // $(".office_ids_length").val(office_ids[category_id].length);
                SortingDate.init($('.startdate').val(), $('.enddate').val(), tableId, category_id);
                $('#showlist').html('');
            }else{
                if(!isEmpty(response.message)){
                    toastr.error(response.message);
                }else{
                    toastr.error('টেকনিক্যাল ত্রুটি হয়েছে।');
                }
                $('#showlist').html('');
            }
        }
    });
}

function callOffices(cnt, date_start, date_end, tableId, category_id) {
    if (cnt == office_ids[category_id].length) {
        $('#showlist').html('');
        Metronic.unblockUI('#searchPanel');
        loadDatatable(tableId,category_id);
        return false;
    }else if(cnt==0){
        if ( $.fn.DataTable.isDataTable( '#'+tableId ) ) {
            $('#'+tableId).DataTable().clear();
            $('#'+tableId).dataTable().fnDestroy();
        }
        // var table = $('.loadDataTable').DataTable();
        // table.clear();
        // table.destroy();
    }

    $('#showlist').html('<img src="'+js_wb_root+'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে  <b>' + office_name[category_id][cnt] + '</b> । একটু অপেক্ষা করুন... </span><br>');
    var office_id = office_ids[category_id][cnt];
    $.ajax({
        type: 'POST',
        url: js_wb_root+"Cron/officesReportsContent",
        //                dataType: 'json',
        data: {"office_id": office_id, "type": 'dak', 'date_start': date_start, 'date_end': date_end},
        success: function (data_dak) {
            if (data_dak == 'false')
            {
                callOffices(cnt + 1, date_start, date_end, tableId, category_id);
            } else {
                $.ajax({
                    type: 'POST',
                    url: js_wb_root+"Cron/officesReportsContent",
                    //                dataType: 'json',
                    data: {"office_id": office_id, "type": 'nothi', 'date_start': date_start, 'date_end': date_end},
                    cache: false,
                    success: function (data_nothi) {
                        var totalMark = generateMark($.extend({}, data_dak, data_nothi));
                        toAdd = '<tr>' +
                            '<td class="text-center" ><b>' + office_name[category_id][cnt] + '</b></td> ' +
                            '<td class="text-center"> <b>' + enTobn(data_nothi.lastUpdate) + '</b></td>' +
                            '<td class="text-center"> <b>' + enTobn(data_dak.totalInbox) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(parseInt(data_dak.totalNothijato) + parseInt(data_dak.totalNothivukto)) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn( parseInt(data_nothi.selfNote)) + '</b></td> ' +
                            '<td class="text-center"><b>' + enTobn(parseInt(data_nothi.dakNote) ) + '</b></td> ' +
                            '<td class="text-center"><b>' + enTobn(data_nothi.NisponnoNote) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(data_nothi.potrojari_nisponno_internal) + '</b></td> ' +
                            '<td class="text-center"><b>' + enTobn(data_nothi.potrojari_nisponno_external) + '</b></td> ' +
                            '<td class="text-center"><b>' + enTobn(data_nothi.NisponnoPotrojari) + '</b></td>' +
                            '<td class="text-center"><b>' + enTobn(data_nothi.Potrojari) + '</b></td> ' +
                            '<td class="text-center"><b>' + enTobn(data_nothi.date_range) + '</b></td> ' +
                            '<td class="text-center"><b>' + (totalMark) + '</b></td> ' +
                            '<td class="text-center"><b>' + (office_id) + '</b></td> ' +
                            '</tr>';
                        $('table#'+tableId).find('#addData').append(toAdd);

                        callOffices(cnt + 1, date_start, date_end, tableId, category_id);
                    }
                });
            }

        }
    });
//                break;
//        }

}
function duration(start,end){
   var startDate = start.split('-');
   var endDate = end.split('-');
    // 0 - Y, 1 - M, 2 - D
    if(isEmpty(startDate) || isEmpty(endDate) || startDate.length < 2 || endDate.length < 2 ){
       return BnFromEng(start)+" - "+BnFromEng(end);
    }
    var monthNames= ['জানুয়ারি', 'ফেব্রুয়ারি', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর'];
    if(startDate[0] == endDate[0] && startDate[1] == endDate[1]){
        return BnFromEng(startDate[2])+' - '+BnFromEng(endDate[2])+' '+monthNames[parseInt(endDate[1]-1)]+', '+BnFromEng(endDate[0]);
    }
    else if(startDate[0] == endDate[0] ){
        return BnFromEng(startDate[2])+' '+monthNames[parseInt(startDate[1] -1)]+' - '+BnFromEng(endDate[2])+' '+monthNames[parseInt(endDate[1] -1)]+', '+BnFromEng(endDate[0]);
    }
    else{
        return BnFromEng(startDate[2])+' '+monthNames[parseInt(startDate[1] -1)]+', '+BnFromEng(startDate[0])+' - '+BnFromEng(endDate[2])+' '+monthNames[parseInt(endDate[1] -1)]+', '+BnFromEng(endDate[0]);
    }
}
function generateMark(data){
    if(isEmpty(mark)){
       return -1;
    }
    var total = 0;
    if(!isEmpty(data.totalNothijato) || !isEmpty(data.totalNothivukto)){
        total += parseInt((data.totalNothijato + data.totalNothivukto )* mark.dak_nisponno );
    }
    if(!isEmpty(data.totalInbox)){
        total += parseInt(data.totalInbox * mark.dak_inbox );
    }
    if(!isEmpty(data.selfNote)){
        total += parseInt(data.selfNote * mark.nothi_self_note );
    }
    if(!isEmpty(data.dakNote)){
        total += parseInt(data.dakNote * mark.nothi_dak_note );
    }
    if(!isEmpty(data.NisponnoNote)){
        total += parseInt(data.NisponnoNote * mark.nothi_note_niponno );
    }
    if(!isEmpty(data.potrojari_nisponno_internal)){
        total += parseInt(data.potrojari_nisponno_internal * mark.potrojari_nisponno_internal );
    }
    if(!isEmpty(data.potrojari_nisponno_external)){
        total += parseInt(data.potrojari_nisponno_external * mark.potrojari_nisponno_external );
    }
    if(!isEmpty(data.Potrojari)){
        total += parseInt(data.Potrojari * mark.potrojari );
    }
    if(total > 0){
        total = total/100;
    }
    return total;
}
var SortingDate = function () {

    var content = $('.inbox-content');

    var loadDraft = function (el, startDate, endDate, tableId, category_id) {
        if(!isEmpty(startDate) && !isEmpty(endDate)){
            $('.startdate').val(startDate);
            $('.enddate').val(endDate);
        }
        if(isEmpty(category_id)){
            if(!isEmpty($("#category-id :selected").val())){
                $("#category-id").trigger('change');
            }
            return;
        }


        var categoryName = $('select#category-id option[value='+category_id+']').text();
        $("#"+tableId).find('.headText').html("<q>" + categoryName +"</q> এর ই-নথি কার্যক্রমের বিস্তারিত রিপোর্ট ("+duration(startDate,endDate)+")");

        if (typeof category_id == 'undefined') {
            return;
        }
        if(office_ids[category_id].length == 0 || office_ids[category_id].length == ''){
            return;
        }
        Metronic.blockUI({
            target: '#searchPanel',
            boxed: true,
            message: 'অপেক্ষা করুন'
        });
        callOffices(0,startDate,endDate, tableId, category_id);
        var toggleButton = function (el) {
            if (typeof el == 'undefined') {
                return;
            }
            if (el.attr("disabled")) {
                el.attr("disabled", false);
            } else {
                el.attr("disabled", true);
            }
        };
    };

    return {
        //main function to initiate the module
        init: function (startDate, endDate, tableId, category_id) {
            $('#addData').html('');

            if(isEmpty(startDate) ){
                startDate =   $('.startdate').val();
            }
            if(isEmpty(endDate)){
                endDate =   $('.enddate').val();
            }

            loadDraft($(this), startDate, endDate, tableId, category_id);

        }

    };

}();
function loadDatatable(tableId,category_id) {
    var table_header = $("#"+tableId).find(".headText").text();
    //table_header,cloumns_header
    $("#"+tableId).DataTable( {
        "searching": false,
        "info": false,
        dom: 'Bfrtip',
        "destroy": true,
        buttons: [
            {
                extend: 'excelHtml5',
                className: 'btn green round-corner-5 margin-bottom-10 margin-top-10',
                text: '<i class="fa fa-file-excel-o"></i> এক্সেল ' +$('#category-id option[value='+category_id+']').text(),
                filename : table_header.replace('এর ই-নথি কার্যক্রমের বিস্তারিত রিপোর্ট','এর ই-নথি কার্যক্রম'),
                title : '',
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                    $(node).addClass('excel-export');
                    $(node).attr('onclick', 'changeButtonColor(this)');
                },
                customize: function (xlsx) {
                    // console.log(xlsx);
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    var downrows = 2;
                    var clRow = $('row', sheet);
                    //update Row
                    clRow.each(function (i,v) {
                        var attr = $(this).attr('r');
                        var ind = parseInt(attr);
                        ind = ind + downrows;
                        $(this).attr("r",ind);
                    });

                    // Update  row > c
                    $('row c ', sheet).each(function (i,v) {
                        var attr = $(this).attr('r');
                        var pre = attr.substring(0, 1);
                        var ind = parseInt(attr.substring(1, attr.length));
                        ind = ind + downrows;
                        $(this).attr("r", pre + ind);
                    });

                    function Addrow(index,data) {
                        msg='<row r="'+index+'">'
                        for(i=0;i<data.length;i++){
                            var key=data[i].k;
                            var value=data[i].v;
                            msg += '<c t="inlineStr" r="' + key + index + '" s="32">';
                            msg +=  '<is>';
                            msg +=   '<t>'+value+'</t>';
                            msg +=  '</is>';
                            msg += '</c>';
                        }
                        msg += '</row>';
                        return msg;
                    }


                    //insert
                    var r1 = Addrow(1, [
                        { k: 'A', v: table_header }, { k: 'B', v: '' }, { k: 'C', v: '' },
                        { k: 'D', v: '' }, { k: 'E', v: '' }, { k: 'F', v: '' },
                        { k: 'G', v: '' }, { k: 'H', v: '' }, { k: 'I', v: '' },
                        { k: 'J', v: '' }, { k: 'K', v: '' }, { k: 'L', v: '' },{ k: 'M', v: '' },{ k: 'N', v: '' }
                        ]);

                    var r2 = Addrow(2, [
                        { k: 'A', v: '' }, { k: 'B', v: '' }, { k: 'C', v: 'ডাক' },
                        { k: 'D', v: '' }, { k: 'E', v: 'নথি' }, { k: 'F', v: '' },
                        { k: 'G', v: '' }, { k: 'H', v: 'পত্রজারিতে নিষ্পন্ন নোট' }, { k: 'I', v: '' },
                        { k: 'J', v: '' }, { k: 'K', v: '' }, { k: 'L', v: '' },{ k: 'M', v: '' },{ k: 'N', v: '' }
                        ]);

                    sheet.childNodes[0].childNodes[1].innerHTML = r1+r2+ sheet.childNodes[0].childNodes[1].innerHTML;

                },
            }
        ],
        "columnDefs": [
            { "orderable": false, "targets": [0,1,2,3,4,5,6,7,8,9,10,11,13]}
        ],
        "order": [[ 12, "desc" ]],
        "paging": false
    } );

    $.each($("#"+tableId).find("tbody>tr").find('td:last-child'), function(key, value) {
        $(value).find('b').text(enTobn($(value).find('b').text()));
    })

}
$(document).on('click', '.btn-performance-print', function () {
    $(document).find('#DesignationPerformance').printThis({
        importCSS: true,
        debug: false,
        importStyle: true,
        printContainer: false,
        pageTitle: "",
        removeInline: false,
        header: null
    });
});

function changeButtonColor(element) {
    $(element).removeClass('green').addClass('red');
};