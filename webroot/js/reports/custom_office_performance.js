var SortingDate = function () {

    var content = $('.inbox-content');
    var loadDraft = function (el, startDate, endDate, officeId) {

        var url = js_wb_root + "Performance/newOfficeContent/" + startDate + "/" + endDate;
        $('.startdate').val(startDate);
        $('.enddate').val(endDate);
        $("#office-name").val($('#office-id :selected').text());
        Metronic.blockUI({
            target: '#searchPanel',
            boxed: true,
            message: 'অপেক্ষা করুন'
        });
        if (officeId == 0) {
            var data = $('.searchForm').serialize();
        } else {
            var data = $('.searchForm').serialize() + '&office_id=' + officeId;
        }
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: data,
            dataType: "html",
            success: function (res) {
                content.append(res);
                Metronic.unblockUI('#searchPanel');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toggleButton(el);
                Metronic.unblockUI('#searchPanel');
            },
            async: false
        });
        var toggleButton = function(el) {
            if (typeof el == 'undefined') {
                return;
            }
            if (el.attr("disabled")) {
                el.attr("disabled", false);
            } else {
                el.attr("disabled", true);
            }
        };
    };

    return {
        //main function to initiate the module
        init: function (startDate, endDate, officeId) {
            if (typeof officeId == 'undefined') {
                officeId = $('#office-id').val();
            }
            loadDraft($(this), startDate, endDate, officeId);
        }

    };

}();
$(document).on('click', '.btn-performance-print', function () {
    $(document).find('#OfficePerformance').printThis({
        importCSS: true,
        debug: false,
        importStyle: true,
        printContainer: false,
        pageTitle: "",
        removeInline: false,
        header: null
    });
});