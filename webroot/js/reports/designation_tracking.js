var SortingDate = function () {

    var content = $('.inbox-content');

    var loadDraft = function (el, startDate, endDate) {

        $('.startdate').val(startDate);
        $('.enddate').val(endDate);
        var office_id = $('#office-id').val(); 
        var office_unit_id = $('#office-unit-id').val(); 
        var office_designation_id = $('#office-unit-organogram-id').val(); 
         loadHistory(office_id,office_unit_id,office_designation_id,startDate,endDate);
        var toggleButton = function (el) {
            if (typeof el == 'undefined') {
                return;
            }
            if (el.attr("disabled")) {
                el.attr("disabled", false);
            } else {
                el.attr("disabled", true);
            }
        };
    };

    return {
        //main function to initiate the module
        init: function (startDate, endDate) {
              $('#showlist').html('');
                if(startDate == '' || typeof(startDate) == 'undefined' ){
                    startDate =   $('.startdate').val();
                }
                if(endDate == '' || typeof(endDate) == 'undefined' ){
                    endDate =   $('.enddate').val();
                }
               
                loadDraft($(this), startDate, endDate);
            
        }

    };

}();