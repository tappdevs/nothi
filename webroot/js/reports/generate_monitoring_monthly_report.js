jQuery(document).ready(function () {
    var tbl_index = 1;
    var datesArray = [];
    $("#export").on("click", exportMonitorReport);
    function exportMonitorReport() {
        $('.inbox-content').html('');
        $('#showlist').html('<img src="' + js_wb_root + 'webroot/assets/global/img/loading-spinner-grey.gif" alt="" class="loading" > <span>&nbsp;&nbsp; লোড হচ্ছে  <b>' + '</b> । একটু অপেক্ষা করুন... </span><br>');


        var year = $("#year :selected").val();
        var month = $("#month :selected").val();
        var month_name = $("#month :selected").text();

        var content = '<div class="row" >\n\
    <div class ="col-md-3 col-sm-3 col-xs-3 pull-left"><img src="' + js_wb_root + 'webroot/img/a_a2i-logo.jpg" alt="" style="max-height: 50px;max-width: 100px;"></div>\n\
<div class ="col-md-6 col-sm-6 col-xs-6 text-center" style="font-size:12px!important;">Monitoring Dashbord<br>Monthly Status Report of Offices<br><b style="font-size:12px!important;">' + month_name + ' ' + year + '</b></div>\n\
<div class ="col-md-3 col-sm-3 col-xs-3"><div class="pull-right"><img src="' + js_wb_root + 'webroot/img/ban-gov_logo.png" alt="" style="max-height: 50px;max-width: 100px;"></div></div>\n\
</div> ';
        var add = '<tr  class="heading"><td colspan = 5 >' + content + '</td></tr><tr class="heading"><th class="text-center" >ক্রম </th><th class="text-center" >তারিখ </th><th class="text-center" >সর্বমোট অফিস </th><th class="text-center" >ব্যর্থ হয়েছে </th><th class="text-center" > বিস্তারিত </th></tr>';
        $('#addHeader').html(add);

        var reportStart = '' + year + '-' + month + '-01';
        if ($.inArray(month, ['01', '03', '05', '07', '08', '10', '12']) != -1) {
            var reportEnd = '"' + year + '-' + month + '-31"';
        } else if (month == '02') {
            if ((parseInt(month) % 100) != 0 && (parseInt(month) % 4 == 0) && (parseInt(month) % 400) == 0) {
                var reportEnd = '' + year + '-' + month + '-29';
            } else {
                var reportEnd = '' + year + '-' + month + '-28';
            }
        } else {
            var reportEnd = '' + year + '-' + month + '-30';
        }
        datesArray = getDates(reportStart, new Date(reportEnd));
        var length = datesArray.length;
        getData(0, length);
    }
    function getDates(startDate, stopDate) {

        var dateArray = [];
        var currentDate = moment(new Date(startDate));
        while (currentDate <= stopDate) {
            dateArray.push(moment(currentDate).format('YYYY-MM-DD'))
            currentDate = moment(currentDate).add(1, 'days');
        }
        return dateArray;
    }
    function getData(indx, cnt) {
        if (indx == cnt) {
           var dt =moment(Date.now()).format('YYYY-MM-DD');
           if(tbl_index == 1){
               var add = '<tr  class="heading"><td colspan = 5 class="text-center danger"> দুঃখিত কোন তথ্য পাওয়া যায়নি। </td></tr>';
                $('.inbox-content').append(add);
           }
            tbl_index = 1;
            $('.inbox-content ul').addClass('list-group');
            $('.inbox-content ul li').addClass('list-group-item');
             var content = '<div class="row" >\n\
    <div class ="col-md-4 col-sm-4 col-xs-4 pull-left" style="font-size:8px!important;margin-top: 8px;">Report genereted from e-Nothi </div>\n\
<div class ="col-md-4 col-sm-4 col-xs-4 text-center" style="font-size:8px!important;margin-top: 8px;"> Date: '+ dt+' </div>\n\
<div class ="col-md-4 col-sm-4 col-xs-4"><div class="pull-right" style="font-size:8px!important;"><a href="http://tappware.com"><img src="' + js_wb_root + 'webroot/img/tappware_final_logo.png" alt="" style="max-height: 10px;max-width: 30px;"></a>&nbsp;Developed by: Tappware Solutions Ltd.</div></div>\n\
</div> ';
            var add = '<tr  class="heading"><td colspan = 5 >'+content+'</td></tr>';
            $('.inbox-content').append(add);
            $('#showlist').html(' <a class="btn pull-right btn-sm blue hidden-print btn-print pull-right">  <i class="fs1 a2i_gn_print2"></i> প্রিন্ট </a><br>');
            return;
        }
         $('#showlist').html('<img src="' + js_wb_root + 'webroot/assets/global/img/loading-spinner-grey.gif" alt="" class="loading" > <span>&nbsp;&nbsp; লোড হচ্ছে  <b>' +datesArray[indx]+ ' তারিখের রিপোর্ট</b> । একটু অপেক্ষা করুন... </span><br>');
        $.ajax({
            type: 'POST',
            url: js_wb_root + 'report/generateMonitoringDashboardMonthlyReport',
            data: {'date': datesArray[indx]},
            success: function (res) {
                if (res.status == 'success') {
                    var dt = res.data.operation_date;
                    var add = '<tr ><td class="text-right">' + En2Bn(tbl_index++) + '</td><td class="text-right">' + En2Bn(dt.slice(0, 10)) + '</td><td class="text-right">' + En2Bn(res.data.total_offices) + '</td><td class="text-right">' + En2Bn(res.data.total_errors) + '</td><td style="font-size:12px!important;">' + res.data.error_details + '</td></tr>';
                    $('.inbox-content').append(add);
                }
                  getData(indx + 1, cnt);
            },
            error: function () {
                  getData(indx + 1, cnt);
            }
        });
    }
      $(document).on('click', '.btn-print', function () {
        $(document).find('.print-portion').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: false,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    });
});

