var SortingDate = function () {

    var content = $('.inbox-content');

    var loadDraft = function (el, startDate, endDate) {
        var url = js_wb_root + "Performance/districtWiseOfficeListByGeoDistrictId/" + startDate + "/" + endDate;
        var officeUrl = js_wb_root + "Performance/newOfficeContent/" + startDate + "/" + endDate;
        content.html('');
        $('.startdate').val(startDate);
        $('.enddate').val(endDate);
        if ($("#geo-district-id").val() != "") {
            $("#geo-district-name").val($('#geo-district-id :selected').text());
            Metronic.blockUI({
                target: '#searchPanel',
                boxed: true,
                message: 'অপেক্ষা করুন'
            });
            $.ajax({
                type: "POST",
                cache: false,
                url: url,
                data: $('.searchForm').serialize(),
                dataType: "html",
                async: true,
                success: function (res) {
                    var totalData = Object.keys($.parseJSON(res)).length;
                    var flag = 0;
                    $.each($.parseJSON(res), function(officeId, officeName) {
                        $.ajax({
                            type: "POST",
                            cache: false,
                            url: officeUrl,
                            data: $('.searchForm').serialize() + '&office_id=' + officeId,
                            dataType: "html",
                            async: true,
                            success: function (result) {
                                flag++;
                                content.append(result);
                                if (totalData == flag) {
                                    Metronic.unblockUI('#searchPanel');
                                }
                            }
                        })
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toggleButton(el);
                    Metronic.unblockUI('#searchPanel');
                }
            });
            var toggleButton = function (el) {
                if (typeof el == 'undefined') {
                    return;
                }
                if (el.attr("disabled")) {
                    el.attr("disabled", false);
                } else {
                    el.attr("disabled", true);
                }
            };
        }
    };

    return {
        //main function to initiate the module
        init: function (startDate, endDate) {
            loadDraft($(this), startDate, endDate);
        }

    };

}();
$(document).on('click', '.btn-performance-print', function () {
    $(document).find('#OfficePerformance').printThis({
        importCSS: true,
        debug: false,
        importStyle: true,
        printContainer: false,
        pageTitle: "",
        removeInline: false,
        header: null
    });
});
