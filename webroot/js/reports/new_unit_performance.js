var SortingDate = function () {

    var content = $('.inbox-content');

    var loadDraft = function (el, startDate, endDate) {

//        var url = js_wb_root + "Performance/newUnitContent/" + startDate + "/" + endDate;
        content.html('');
        $('.startdate').val(startDate);
        $('.enddate').val(endDate);
//            if($('.unit_ids_length').val() == 0 || $('.unit_ids_length').val() == ''){
//                    return;
//                }
        callUnits(0,startDate,endDate);
        
//        $.ajax({
//            type: "POST",
//            cache: false,
//            url: url,
//            data: $('.searchForm').serialize(),
//            dataType: "html",
//            success: function (res) {
//                content.html(res);
//
//            },
//            error: function (xhr, ajaxOptions, thrownError) {
//                toggleButton(el);
//            },
//            async: false
//        });
        var toggleButton = function(el) {
            if (typeof el == 'undefined') {
                return;
            }
            if (el.attr("disabled")) {
                el.attr("disabled", false);
            } else {
                el.attr("disabled", true);
            }
        };
    };

    return {
        //main function to initiate the module
        init: function (startDate, endDate) {
               $('#addData').html('');
        
              
                if(startDate == '' || typeof(startDate) == 'undefined' ){
                    startDate =   $('.startdate').val();
                }
                if(endDate == '' || typeof(endDate) == 'undefined' ){
                    endDate =   $('.enddate').val();
                }
            loadDraft($(this), startDate, endDate);
        }

    };

}();
$(document).on('click', '.btn-performance-print', function () {
        $(document).find('#UnitPerformance').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: false,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    });