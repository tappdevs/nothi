$(document).ready(function () {
    $("#office-unit-id").on('change', function () {
        $("#hidden_unit_name").val($("#office-unit-id :selected").text());
        srcDt();
    });
    $("#office-unit-id-modal").on('change', function () {
        $("#nothi_number").val('');
        var office_unit_id = $("#office-unit-id-modal :selected").val();
        if (!(office_unit_id > 0)) {
            toastr.error('কোন শাখা নির্বাচন করা হয় নি।');
            return;
        }
        getNothiTypesByUnit(office_unit_id);
        $("#s2id_type_id [id^=select2-chosen]").text('--ধরন নির্বাচন করুন--');
    });
    $("#submitButton").on("click", function () {
        var id = $("#id_up").val();
        var name = $("#new_name").val();
        var class_id = $("#class_id").val();
        var type_id = $("#type_id").val();
        var unit_it = $("#office-unit-id-modal :selected").val();
        var nothi_no = ($("#nothi_number_1st_part").text())+($("#nothi_number_2nd_part").val().replace(/[\*]/g, ''));
        if(isEmpty(class_id)){
            toastr.error("নথির শ্রেণি নির্বাচন করুন।");
            return;
        }
        if(isEmpty(unit_it)){
            toastr.error("কোন শাখা নির্বাচন করা হয় নি।");
            return;
        }
        if(isEmpty(type_id)){
            toastr.error("নথির ধরন নির্বাচন করুন।");
            return;
        }
        var old_nothi_number = $("#old_nothi_number").val();
        if(nothi_no==old_nothi_number){
            nothi_no='';
        }
        if(isEmpty(name)){
            name = $("#cur_name").val();
        }
        if(!(isEmpty(nothi_no))){
            if ((nothi_no.replace(/[\.]/g, '').length !=18) && (nothi_no.replace(/[\.]/g, '').length !=0)) {

                toastr.error('দুঃখিত! নথি নম্বর সঠিক নয়। নথি নম্বর ১৮ ডিজিটের হতে হবে ');
                return;
            }
        }

        var prev_unit_name =  $("#hidden_unit_name").val();
        var prev_type_name =  $("#hidden_type_name").val();

        $.ajax({
            type: 'POST',
            url: $("#nothiNameUpdate").attr('action'),
            data: {'id': btoa(id), "name": name,"class_id":class_id,"type_id":type_id,"unit_it":unit_it,"nothi_no":nothi_no,'prev_type_name':prev_type_name,'prev_unit_name':prev_unit_name},
            success: function (data) {
                if (data.status == 'success') {
                    toastr.success(data.msg);
                    srcDt();
                    $("#myModal").modal('toggle');
                } else {
                    toastr.error(data.msg);
                    srcDt();
                }

            },
            error: function (data) {
                toastr.error(data.msg);
                $("#myModal").modal('toggle');
            }
        });
    });
    $("#type_id").on('change', function () {
        var office_unit_id = $("#office-unit-id-modal :selected").val();
        var type_id = $("#type_id :selected").val();
        $("#nothi_number").val('');

        if(isEmpty(office_unit_id)) {
            toastr.error("কোন শাখা নির্বাচন করা হয় নি।");
            return;
        };
        if(isEmpty(type_id)) {
            toastr.error("নথির ধরন নির্বাচন করুন।");
            return;
        };

        var prev_unit = $("#hidden_unit").val();
        var prev_type = $("#hidden_type_id").val();

        if(office_unit_id==prev_unit && type_id==prev_type){
            var old_number = $("#old_nothi_number").val();
                var nothi_number_1st_part = old_number.substring(0, 18);
                var nothi_number_2nd_part = old_number.substring(18, 25);
                $("#nothi_number_1st_part").text(nothi_number_1st_part);
                $("#nothi_number_2nd_part").val(nothi_number_2nd_part);
            return;
        }

        $.ajax({
            type: 'POST',
            url: js_wb_root+'nothiMasters/generateNothiNumber/',
            data: {'id':type_id,'office_unit_id':office_unit_id},
            success: function (data) {
                    var nothi_number_1st_part = data.substring(0, 18);
                    var nothi_number_2nd_part = data.substring(18, 25);
                    $("#nothi_number_1st_part").text(nothi_number_1st_part);
                    $("#nothi_number_2nd_part").val(nothi_number_2nd_part);

            }
        })

    });
    $('#nothi_number_2nd_part').mask('AAA.AA'
        ,{
        translation:{
                    A: {pattern: /[\u09E6-\u09EF-0-9]/},
                },
        placeholder: "***.**"
    });
});
function getNothiTypesByUnit(office_unit_id){
    $.ajax({
        type: 'POST',
        url: js_wb_root+'NothiTypes/getNothiTypeByUnit/'+office_unit_id,
        data: {},
        success: function (data) {
            $('#type_id').html('');
            $('#type_id').append('<option value="0">--ধরন নির্বাচন করুন--</option>');
            $.each(data, function (i, v) {
                $('#type_id').append('<option value="'+i+'">'+v+'</option>');
            });
        }
    });
}
function getNothiTypesByUnitOptions(office_unit_id){
    $.ajax({
        type: 'POST',
        url: js_wb_root+'NothiTypes/getNothiTypeByUnit/'+office_unit_id,
        data: {},
        success: function (data) {
            $('#hidden_type').val('');
            var options = '<option value="0">--ধরন নির্বাচন করুন--</option>';

            $('#type_id').append('<option value="0">--ধরন নির্বাচন করুন--</option>');
            $.each(data, function (i, v) {
                options +='<option value="'+i+'">'+v+'</option>';
            });
            $('#hidden_type').val(options);
        }
    });
}

$("#nothi_change_history").on("click", function () {
    $("#nothi_data_history_modal").modal('show');
    var id = $("#id_up").val();
    $.ajax({
        type: 'POST',
        url: js_wb_root+'NothiTypes/getNothiDataChangeHistory/'+btoa(id),
        data: {},
        success: function (data) {
            $('#addHistoryData').html('');
            if(data.status=='success'){
            var history = '';
            var sl = 1;
            $.each(data.h_data, function (j, v) {
               var n_class='';
                if(v.nothi_class==1){
                    n_class = 'ক';
                } else if(v.nothi_class==2){
                    n_class = 'খ';
                } else if(v.nothi_class==3){
                    n_class = 'গ';
                } else {
                    n_class = 'ঘ';
                }
                    history = '<tr class="text-center">' +
                        ' <td>' + BnFromEng(sl++) + '</td>' +
                        ' <td>' + v.nothi_no + '</td>' +
                        ' <td>' + v.subject + '</td>' +
                        ' <td>' + v.unit_name + '</td>' +
                        ' <td>' + n_class + '</td>' +
                        ' <td>' + v.type_name + '</td>' +
                        ' <td>' + v.time + '</td>' +
                        '</tr>';
                    $('#addHistoryData').append(history);
            });
        } else {
                $('#addHistoryData').append('<tr><td colspan="7" class="text-center" style="color: red">কোন তথ্য পাওয়া যায় নি।</td></tr>');
            }
        }
    });

});

function srcDt() {
    var office_unit_id = $("#office-unit-id :selected").val();
    var nothi_subject = $("#searchFor_nothi_subject").val();
    var nothi_no = $("#searchFor_nothi_no").val();
    var srch = $("#search-sub").val();
    if (!isEmpty(office_unit_id)) {
        $('#addData').html('');
    } else {
        toastr.error(' কোন শাখা নির্বাচন করা হয় নি ');
        $("#office-unit-id").focus();
        return;
    }
    Metronic.blockUI({target: '.portlet-body', boxed: true});
    getNothiTypesByUnitOptions(office_unit_id);
    $.ajax({
        type: 'GET',
        url: $("#nothiNameUpdate").attr('action'),
        data: {'unit_id': office_unit_id, 'srch': srch, 'nothi_subject':nothi_subject, 'nothi_no':nothi_no},
        success: function (data) {
            Metronic.unblockUI('.portlet-body');
            if (data.length > 0) {
                $.each(data, function (i, v) {
                    if (v.nothi_created_date == null || typeof (v.nothi_created_date) == 'undefined' || v.nothi_created_date == '') {
                        var dt = '';
                    } else {
                        var dt = v.nothi_created_date.substring(0, 10);
                    }
                    if(v.nothi_class==1){
                        n_class = 'ক';
                    } else if(v.nothi_class==2){
                        n_class = 'খ';
                    } else if(v.nothi_class==3){
                        n_class = 'গ';
                    } else {
                        n_class = 'ঘ';
                    }
                    html = '<tr class="text-center">' +
                        '<td id="no'+v.id+'" data-unit-id="'+v.office_units_id+'" data-nothi="'+v.nothi_no+'">' + v.nothi_no + '</td>' +
                        '<td style="display:none;" id="e' + v.id + '">' + v.expire_level + '</td>' +
                        '<td >' + v.office_units_name + '</td>' +
                        '<td id="' + v.id + '">' + v.subject + '</td>' +
                        '<td id="c'+v.id+'" data-class="'+v.nothi_class+'">' + n_class + '</td>' +
                        '<td id="t'+v.id+'" data-type="'+v.nothi_types_id+'">' + v.nothi_types_name + '</td>' +
                        '<td>' + BnFromEng(dt) + '</td>' +
                        '<td><button type="button" class="btn btn-sm btn-warning" onclick="upMe(' + v.id + ')"> সংশোধন করুন</button></td>' +
                        '</tr>';
                    $('#addData').append(html);
                });

            } else {
                html = '<tr>' +
                    '<td colspan="7" class="text-center red"> দুঃখিত কোন তথ্য পাওয়া যায়নি। </td>' +
                    '</tr>';
                $('#addData').append(html);
            }
        },
        error: function () {
            Metronic.unblockUI('.portlet-body');
        }
    });
}
$(document).ajaxStart(function () {
    Metronic.blockUI({target: '.modal-lg', boxed: true});
});
$(document).ajaxStop(function () {
    Metronic.unblockUI('.modal-lg');
});


function upMe(id) {
    var old_nothi_no = $("#no" + id).data('nothi');
    var selected_unit = $("#no" + id).data('unit-id');

    var nothi_number_1st_part = old_nothi_no.substring(0, 18);
    var nothi_number_2nd_part = old_nothi_no.substring(18, 25);
    $("#nothi_number_1st_part").text(nothi_number_1st_part);
    $("#nothi_number_2nd_part").val(nothi_number_2nd_part);
    $("#new_name").val('');

    $("#type_id").html('');
    $("#type_id").html($('#hidden_type').val());

    $("#myModal").modal('show');


    $("#id_up").val(id);
    $("#cur_name").val($("#" + id).text());
    $("#office-unit-id-modal").val(selected_unit);
    $("#old_nothi_number").val(old_nothi_no);
    $("#class_id").val($("#c" + id).data('class'));
    $("#type_id").val($("#t" + id).data('type'));

    // if ($("#e" + id).text() == 2) {
    //     $("#class_id").attr("disabled", true);
    //     $("#class_error_span").html('নথিটির মেয়াদ শেষ হয়ে যাওয়ায় নথির শ্রেণি পরিবর্তন করতে পারবেন না।');
    //     $("#class_error_span").css('display', 'block');
    //     $("#class_error_span").css('color', 'red');
    // } else {
        $("#class_id").removeAttr("disabled");
        $("#class_error_span").css('display', 'none');
    // }

    $("#hidden_unit").val($("#office-unit-id").val());
    $("#hidden_type_id").val($("#t" + id).data('type'));
    $("#hidden_type_name").val($("#t" + id).text());


    $('select').select2();
    $("#cur_name").attr("readonly", true);
    $("#old_nothi_number").attr("readonly", true);
}
