var SortingDate = function () {

    var content = $('.inbox-content');

    var loadDraft = function (el, startDate, endDate) {
        var per_page = $('#reports_page_limit').val();
        var unit_id = $("#units_list").val();
        $("#start_date").val(startDate);
        $("#end_date").val(endDate);
        var url = js_wb_root + "DakReports/nothiVuktoDakListContetnt/" + startDate + "/" + endDate+"/1/"+per_page+"/"+unit_id;
        content.html('');

        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            dataType: "html",
            success: function (res) {
                $('#expDate').val(startDate+'/'+endDate);
                content.html(res);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toggleButton(el);
            },
            async: false
        });
        
        var toggleButton = function(el) {
            if (typeof el == 'undefined') {
                return;
            }
            if (el.attr("disabled")) {
                el.attr("disabled", false);
            } else {
                el.attr("disabled", true);
            }
        };
    };

    return {
        //main function to initiate the module
        init: function (startDate, endDate) {
            loadDraft($(this), startDate, endDate);
        }

    };

}();