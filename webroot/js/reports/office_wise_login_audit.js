
    var EmployeeLoginHistory = {
        checked_node_ids: [],
        start_time:'',
        end_time:'',
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function () {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function () {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function () {

            });
        },
        loadOfficeUnits: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/loadOfficeUnits',
                    {'office_id': $("#office-id").val()}, 'html',
                    function (response) {
                        $("#office-unit-id").html(response);
                    });
        },
        loadOfficeUnitDesignations: function () {
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/loadOfficeUnitOrganogramsWithName',
                    {'office_unit_id': $("#office-unit-id").val()}, 'json',
                    function (response) {
                        // PROJAPOTI.projapoti_dropdown_map("#office-unit-organogram-id", response, "--বাছাই করুন--");
                        PROJAPOTI.projapoti_dropdown_map_with_title("#office-unit-organogram-id", response, "--বাছাই করুন--",'',{'key' : 'id','value' : 'designation','title' : 'name'  });
                    });
        },
        loadHistory: function() {
            PROJAPOTI.ajaxLoadWithRequestData(js_wb_root + 'users/officeWiseLoginAuditAjax',
                    {'office_id':$("#office-id").val(),office_unit_id:$("#office-unit-id").val(),office_unit_organogram_id:$("#office-unit-organogram-id").val(),start_time:this.start_time, end_time: this.end_time},'.showHistory');
        },
        loadPagination: function(url) {
            PROJAPOTI.ajaxLoad(url,'.showHistory');
        }
    };

    var SortingDate = function () {

        return {
            //main function to initiate the module
            init: function (startDate, endDate) {
                EmployeeLoginHistory.start_time = startDate;
                EmployeeLoginHistory.end_time = endDate;
                EmployeeLoginHistory.loadHistory();
            }

        };

    }();

    $(function () {
        DateRange.init();
        DateRange.initDashboardDaterange();
        $("#office-ministry-id").bind('change', function () {
            EmployeeLoginHistory.loadMinistryWiseLayers();
        });
        $("#office-layer-id").bind('change', function () {
            EmployeeLoginHistory.loadMinistryAndLayerWiseOfficeOrigin();
        });
        $("#office-origin-id").bind('change', function () {
            EmployeeLoginHistory.loadOriginOffices();
        });
        $("#office-id").bind('change', function () {
            EmployeeLoginHistory.loadOfficeUnits();
            EmployeeLoginHistory.loadHistory();
        });
        $("#office-unit-id").bind('change', function () {
            EmployeeLoginHistory.loadOfficeUnitDesignations();
            EmployeeLoginHistory.loadHistory();
        });
        $("#office-unit-organogram-id").bind('change', function () {
            EmployeeLoginHistory.loadHistory();
        });

        $(document).off('click','.pagination a').on('click','.pagination a',function(ev){
            ev.preventDefault();
            EmployeeLoginHistory.loadPagination($(this).attr('href'));
        });
    });

function showLoginDetails(info,designation,ip,start,end){
    $("#myModal").modal('show');
      $('#loginDetails').html('<img src="'+js_wb_root+'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে  <b>' + '</b> । একটু অপেক্ষা করুন... </span><br>');
      PROJAPOTI.ajaxLoadWithRequestData(js_wb_root + 'users/officeWiseLoginAuditDetails',
                    {designation:designation,ip:ip,start:start,end:end,info : info},'#loginDetails');
}