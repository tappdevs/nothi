var SortingDate = function () {

    var content = $('.inbox-content');

    var loadDraft = function (el, startDate, endDate) {

        $('.startdate').val(startDate);
        $('.enddate').val(endDate);
          if($('.office_ids_length').val() == 0 || $('.office_ids_length').val() == ''){
                    return;
                }
//        console.log(startDate,endDate);
//                  $("#headText").html("<q>" + $("#office-origin-id :selected").text() +"</q> অফিসসমূহের কার্যবিবরণী ( "+BnFromEng(startDate)+" - "+BnFromEng(endDate) +")");
        $("#headText").html("<q>" + $("#office-origin-id :selected").text() +"</q> এর ই-নথি কার্যক্রমের বিস্তারিত রিপোর্ট ("+duration(startDate,endDate)+")");
            Metronic.blockUI({
               target: '#searchPanel',
               boxed: true,
               message: 'অপেক্ষা করুন'
           });
         callOffices(0,startDate,endDate);
//        $.ajax({
//            type: "POST",
//            cache: false,
//            url: url,
//            data: $('.searchForm').serialize(),
//            dataType: "html",
//            success: function (res) {
//                content.html(res);
//
//            },
//            error: function (xhr, ajaxOptions, thrownError) {
//                toggleButton(el);
//            },
//            async: false
//        });
        var toggleButton = function (el) {
            if (typeof el == 'undefined') {
                return;
            }
            if (el.attr("disabled")) {
                el.attr("disabled", false);
            } else {
                el.attr("disabled", true);
            }
        };
    };

    return {
        //main function to initiate the module
        init: function (startDate, endDate) {
              $('#addData').html('');
        
              
                if(startDate == '' || typeof(startDate) == 'undefined' ){
                    startDate =   $('.startdate').val();
                }
                if(endDate == '' || typeof(endDate) == 'undefined' ){
                    endDate =   $('.enddate').val();
                }
//                var origin_id = $("#office-origin-id :selected").val();
//                 if(origin_id == null || origin_id =='' || typeof(origin_id) == 'undefined'){
//                   return false;
//                }
             
                loadDraft($(this), startDate, endDate);
            
        }

    };

}();
$(document).on('click', '.btn-performance-print', function () {
        $(document).find('#DesignationPerformance').printThis({
            importCSS: true,
            debug: false,
            importStyle: true,
            printContainer: false,
            pageTitle: "",
            removeInline: false,
            header: null
        });
    });
function loadDatatable(table_header,cloumns_header) {
    $('.loadDataTable').DataTable( {
        dom: 'Bfrtip',
        "destroy": true,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'এক্সেল',
                filename : table_header.replace('এর ই-নথি কার্যক্রমের বিস্তারিত রিপোর্ট','এর ই-নথি কার্যক্রম'),
                title : '',
                customize: function (xlsx) {
                    // console.log(xlsx);
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    var downrows = 2;
                    var clRow = $('row', sheet);
                    //update Row
                    clRow.each(function (i,v) {
                        var attr = $(this).attr('r');
                        var ind = parseInt(attr);
                        ind = ind + downrows;
                        $(this).attr("r",ind);
                    });

                    // Update  row > c
                    $('row c ', sheet).each(function (i,v) {
                        var attr = $(this).attr('r');
                        var pre = attr.substring(0, 1);
                        var ind = parseInt(attr.substring(1, attr.length));
                        ind = ind + downrows;
                        $(this).attr("r", pre + ind);
                    });

                    function Addrow(index,data) {
                        msg='<row r="'+index+'">'
                        for(i=0;i<data.length;i++){
                            var key=data[i].k;
                            var value=data[i].v;
                            msg += '<c t="inlineStr" r="' + key + index + '" s="32">';
                            msg += '<is>';
                            msg +=  '<t>'+value+'</t>';
                            msg+=  '</is>';
                            msg+='</c>';
                        }
                        msg += '</row>';
                        return msg;
                    }


                    //insert
                    // var r1 = Addrow(1, [
                    //     { k: 'A', v: table_header }, { k: 'B', v: '' }, { k: 'C', v: '' },
                    //     { k: 'D', v: '' }, { k: 'E', v: '' }, { k: 'F', v: '' },
                    //     { k: 'G', v: '' }, { k: 'H', v: '' }, { k: 'I', v: '' },
                    //     { k: 'J', v: '' }, { k: 'K', v: '' }, { k: 'L', v: '' },
                    //     ]);
                    var r2 = Addrow(2, [
                        { k: 'A', v: '' }, { k: 'B', v: '' }, { k: 'C', v: 'ডাক' },
                        { k: 'D', v: '' }, { k: 'E', v: 'নথি' }, { k: 'F', v: '' },
                        { k: 'G', v: '' }, { k: 'H', v: 'পত্রজারিতে নিষ্পন্ন নোট' }, { k: 'I', v: '' },
                        { k: 'J', v: '' }, { k: 'K', v: '' }, { k: 'L', v: '' },{ k: 'M', v: '' },
                        { k: 'N', v: '' },
                    ]);

                    sheet.childNodes[0].childNodes[1].innerHTML = r2+ sheet.childNodes[0].childNodes[1].innerHTML;
                    // $.fn.DataTable.ext.buttons.excelHtml5.action.mergeCells('B2',2);


                },
            }
        ],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 1 },
            { "orderable": false, "targets": 2 },
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 },
            { "orderable": false, "targets": 7 },
            { "orderable": false, "targets": 8 },
            { "orderable": false, "targets": 9 },
            { "orderable": false, "targets": 10 },
            { "orderable": false, "targets": 11 },
            { "orderable": false, "targets": 13 },
        ],
        "order": [[ 12, "desc" ]],
        "paging": false
    } );

}
function duration(start,end){
    var startDate = start.split('-');
    var endDate = end.split('-');
    // 0 - Y, 1 - M, 2 - D
    if(isEmpty(startDate) || isEmpty(endDate) || startDate.length < 2 || endDate.length < 2 ){
        return BnFromEng(start)+" - "+BnFromEng(end);
    }
    var monthNames= ['জানুয়ারি', 'ফেব্রুয়ারি', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর'];
    if(startDate[0] == endDate[0] && startDate[1] == endDate[1]){
        return BnFromEng(startDate[2])+' - '+BnFromEng(endDate[2])+' '+monthNames[parseInt(endDate[1])]+', '+BnFromEng(endDate[0]);
    }
    else if(startDate[0] == endDate[0] ){
        return BnFromEng(startDate[2])+' '+monthNames[parseInt(startDate[1])]+' - '+BnFromEng(endDate[2])+' '+monthNames[parseInt(endDate[1])]+', '+BnFromEng(endDate[0]);
    }
    else{
        return BnFromEng(startDate[2])+' '+monthNames[parseInt(startDate[1])]+', '+BnFromEng(startDate[0])+' - '+BnFromEng(endDate[2])+' '+monthNames[parseInt(endDate[1])]+', '+BnFromEng(endDate[0]);
    }
}
function generateMark(data){
    if(isEmpty(mark)){
        return -1;
    }
    var total = 0;
    if(!isEmpty(data.totalNothijato) || !isEmpty(data.totalNothivukto)){
        total += parseInt((data.totalNothijato + data.totalNothivukto )* mark.dak_nisponno );
    }
    if(!isEmpty(data.totalInbox)){
        total += parseInt(data.totalInbox * mark.dak_inbox );
    }
    if(!isEmpty(data.selfNote)){
        total += parseInt(data.selfNote * mark.nothi_self_note );
    }
    if(!isEmpty(data.dakNote)){
        total += parseInt(data.dakNote * mark.nothi_dak_note );
    }
    if(!isEmpty(data.NisponnoNote)){
        total += parseInt(data.NisponnoNote * mark.nothi_note_niponno );
    }
    if(!isEmpty(data.potrojari_nisponno_internal)){
        total += parseInt(data.potrojari_nisponno_internal * mark.potrojari_nisponno_internal );
    }
    if(!isEmpty(data.potrojari_nisponno_external)){
        total += parseInt(data.potrojari_nisponno_external * mark.potrojari_nisponno_external );
    }
    if(!isEmpty(data.Potrojari)){
        total += parseInt(data.Potrojari * mark.potrojari );
    }
    if(total > 0){
        total = total/100;
    }
    return total;
}