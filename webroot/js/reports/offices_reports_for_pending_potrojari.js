var SortingDate = function () {


    var loadDraft = function (el, startDate, endDate) {

        $('.startdate').val(startDate);
        $('.enddate').val(endDate);
        if( $("#office-origin-id").val() == 0 ||  $("#office-origin-id").val() == ''){
            return;
        }
        //Need to initiate this function in view file for handle inputs
        getOfficesId( $("#office-origin-id").val(),startDate,endDate);
        var toggleButton = function (el) {
            if (typeof el == 'undefined') {
                return;
            }
            if (el.attr("disabled")) {
                el.attr("disabled", false);
            } else {
                el.attr("disabled", true);
            }
        };
    };

    // var content = $('.inbox-content');
    return {
        //main function to initiate the module
        init: function (startDate, endDate) {
              $('#addData').html('');
        
              
                if(startDate == '' || typeof(startDate) == 'undefined' ){
                    startDate =   $('.startdate').val();
                }
                if(endDate == '' || typeof(endDate) == 'undefined' ){
                    endDate =   $('.enddate').val();
                }
            loadDraft($(this), startDate, endDate);
        }

    };

}();