var SortingDate = function () {

    var content = $('.inbox-content');

    var loadDraft = function (el, startDate, endDate) {

        $('.startdate').val(startDate);
        $('.enddate').val(endDate);
          if($('#office-id').val() == 0 || $('#office-id').val() == ''){
                    return;
                }
         loadPotrojari($('#office-id').val(),startDate,endDate);
        var toggleButton = function (el) {
            if (typeof el == 'undefined') {
                return;
            }
            if (el.attr("disabled")) {
                el.attr("disabled", false);
            } else {
                el.attr("disabled", true);
            }
        };
    };

    return {
        //main function to initiate the module
        init: function (startDate, endDate) {
              $('#showlist').html('');
              $('.inbox-content').html('');
              
                if(startDate == '' || typeof(startDate) == 'undefined' ){
                    startDate =   $('.startdate').val();
                }
                if(endDate == '' || typeof(endDate) == 'undefined' ){
                    endDate =   $('.enddate').val();
                }
               
                loadDraft($(this), startDate, endDate);
            
        }

    };

}();