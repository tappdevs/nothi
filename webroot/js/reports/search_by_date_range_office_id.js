var SortingDate = function () {

    var loadDraft = function (el, startDate, endDate) {

        $('.startdate').val(startDate);
        $('.enddate').val(endDate);
          if($('#office-id').val() == 0 || $('#office-id').val() == ''){
                    return;
                }
                //Need to initiate this function in view file for handle inputs
         loadData($('#office-id').val(),startDate,endDate);
        var toggleButton = function (el) {
            if (typeof el == 'undefined') {
                return;
            }
            if (el.attr("disabled")) {
                el.attr("disabled", false);
            } else {
                el.attr("disabled", true);
            }
        };
    };

    return {
        //main function to initiate the module
        init: function (startDate, endDate) {
                if(startDate == '' || typeof(startDate) == 'undefined' ){
                    startDate =   $('.startdate').val();
                }
                if(endDate == '' || typeof(endDate) == 'undefined' ){
                    endDate =   $('.enddate').val();
                }
                loadDraft($(this), startDate, endDate);
        }
    };
}();