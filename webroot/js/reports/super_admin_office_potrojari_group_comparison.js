var prefix = '';
var offices_ids =[];
$("#" + prefix + "office-ministry-id").bind('change', function () {
    OfficeOrgSelectionCell.loadMinistryWiseLayers(prefix);
});
$("#" + prefix + "office-layer-id").bind('change', function () {
    OfficeOrgSelectionCell.loadMinistryAndLayerWiseOfficeOrigin(prefix);
});
$("#" + prefix + "office-origin-id").bind('change', function () {
    OfficeOrgSelectionCell.loadOriginOffices(prefix);
});
// compareOfficeWithPJGroups

var OfficeOrgSelectionCell = {

    loadMinistryWiseLayers: function () {
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeSettings/loadLayersByMinistry',
            {'office_ministry_id': $("#" + prefix + "office-ministry-id").val()}, 'json',
            function (response) {
                PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-layer-id", response, "--বাছাই করুন--");
            });
    },
    loadMinistryAndLayerWiseOfficeOrigin: function () {
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeSettings/loadOfficesByMinistryAndLayer',
            {
                'office_ministry_id': $("#" + prefix + "office-ministry-id").val(),
                'office_layer_id': $("#" + prefix + "office-layer-id").val()
            }, 'json',
            function (response) {
                PROJAPOTI.projapoti_dropdown_map("#" + prefix + "office-origin-id", response, "--বাছাই করুন--");
            });
    },
    loadOriginOffices: function () {
        Metronic.blockUI({target: '.portlet-body', boxed: true, message: 'অপেক্ষা করুন...'});
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'officeManagement/loadOriginOffices',
            {'office_origin_id': $("#" + prefix + "office-origin-id").val()}, 'json',
            function (response) {
                offices_ids = [];
                for(var i in response){
                    offices_ids.push(i);
                    $('#addData').append('<tr class="text-center" id="row_'+i+'"> <td class="office" data-office-id="'+i+'">'+response[i]+'</td> <td class="users"></td> <td class="action"></td> </tr>');
                }
                $("#compare").show();
                Metronic.unblockUI('.portlet-body');
            });
    },
    compareOfficeWithPJGroups: function(response,indx){
        if(response.length <= indx){
            Metronic.unblockUI('.portlet-body');
            return;
        }
        if(isEmpty(response[indx])){
            Metronic.unblockUI('.portlet-body');
            return;
        }
        var office_id =response[indx];
        var pj_group_id =$("#" + prefix + "potrojari-group-id").val();
        var url = js_wb_root+'super-admin-get-pj-group-users';
        var data = {};
        if(isEmpty(office_id) || isEmpty(pj_group_id)){
            toastr.error('প্রয়োজনীয় তথ্য দেওয়া হয়নি।');
            return;
        }else{
            data.office_id = office_id;
            data.potrojari_group_id = pj_group_id;
        }
        PROJAPOTI.ajaxSubmitDataCallback(url,data,'json',function(res){
            if(res.status =='success'){
                $("#row_"+office_id).find('.users').html('');
                if (res.data.length > 0) {
                    $("#row_"+office_id).removeClass('danger');
                    $.each(res.data, function (i, v) {
                        $("#row_"+office_id).find('.users').css('text-align', 'left');
                        $("#row_"+office_id).find('.users').append(BnFromEng(i+1)+'। ', v.employee_name_bng + ', ' + v.office_unit_organogram_name_bng + ', ' + v.office_unit_name_bng + '<br>');
                    });
                }else{
                    $("#row_"+office_id).addClass('danger');
                }
                $("#row_"+office_id).find('.action').html('<button class="btn btn-primary round-corner-5" onclick="showDesignationsOfAnOffice('+office_id+','+pj_group_id+')">পত্রজারি গ্রুপে পদবী অন্তর্ভুক্তিকরণ </button>');
            }else{
                toastr.error(res.message);
            }
            OfficeOrgSelectionCell.compareOfficeWithPJGroups(response,indx+1);
        });
    },
};
$(document).ready(function() {
    $("select").select2();
    $('#compare').on('click',function(){
        Metronic.blockUI({target: '.portlet-body', boxed: true, message: 'অপেক্ষা করুন...'});
        OfficeOrgSelectionCell.compareOfficeWithPJGroups(offices_ids,0);
    });

    $('.addUsersInPotrojariGroup').on('click',function(){
        var element = $(this);
        Metronic.blockUI({target: element.closest('.modal').find('.modal-body'), boxed: true, message: 'সংরক্ষণ হচ্ছে...'});
        var officeId = element.closest('.modal').find('.modal-title').attr('office_id');
        var organogramIds = [];
        $(".checker:checkbox:checked").each(function () {
            organogramIds.push($(this).val());
        });
        // if (organogramIds.length == 0) {
        //     toastr.error('কোনো ব্যবহারকারী নির্বাচন করা নাই। দয়া করে ব্যবহারকারী নির্বাচন করুন।');
        //     return;
        // }
        var potrojariGroupId = $("#potrojari-group-id").val();
        var data = {
            'officeId':officeId,
            'organogramIds':organogramIds,
            'potrojariGroupId':potrojariGroupId
        };
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'update-potrojari-group',data,'json',function(response){
            if (response.status == 'success') {
                toastr.success(response.msg);
            } else {
                toastr.success(response.msg);
            }
            Metronic.unblockUI(element.closest('.modal').find('.modal-body'));
            element.closest('.modal').modal('hide');
            $("#compare").trigger('click');
        });

    });
});
function showDesignationsOfAnOffice(office_id,pj_group_id){
    if(isEmpty(office_id) || isEmpty(pj_group_id)){
        toastr.error('প্রয়োজনীয় তথ্য দেওয়া হয়নি।');
        return;
    }
    var url = js_wb_root+'getPJGroupMappedUserOfAnOffice';
    var data = {};
    data.office_id = office_id;
    data.potrojari_group_id = pj_group_id;
    $("#OfficeUserModal").find('.modal-title').html($("#row_"+office_id+" td.office").html());
    $("#OfficeUserModal").find('.modal-title').attr('office_id', office_id);
    PROJAPOTI.ajaxSubmitDataCallback(url,data,'html',function(res){
        $("#OfficeUserModal").modal('show');
        $("#OfficeUserModal").find('.modal-body').html(res);
    });
}
