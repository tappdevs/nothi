var SortingDate = function () {

    var content = $('.inbox-content');

    var loadDraft = function (el, startDate, endDate) {

        var url = js_wb_root + "Performance/superadminOfficeContent/" + startDate + "/" + endDate;
        content.html('');
        $('.startdate').val(startDate);
        $('.enddate').val(endDate);
        $('.officeids').val($('#office_ids_list').text());
//        console.log($('#office_ids_list').text());

        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: $('.searchForm').serialize(),
            dataType: "html",
            success: function (res) {
                content.html(res);

            },
            error: function (xhr, ajaxOptions, thrownError) {
                toggleButton(el);
            },
            async: false
        });
        var toggleButton = function (el) {
            if (typeof el == 'undefined') {
                return;
            }
            if (el.attr("disabled")) {
                el.attr("disabled", false);
            } else {
                el.attr("disabled", true);
            }
        };
    };

    return {
        //main function to initiate the module
        init: function (startDate, endDate) {
//            console.log(startDate);
            if ($("#office_ids_list").text() == '' || $("#office_ids_list").text() == undefined) {
                return;
            }else{
                if(startDate == '' || typeof(startDate) == 'undefined' ){
                    startDate =   $('.startdate').val();
                }
                if(endDate == '' || typeof(endDate) == 'undefined' ){
                    endDate =   $('.enddate').val();
                }
                loadDraft($(this), startDate, endDate);
            }
        }

    };

}();