var SortingDate = function () {

    var content = $('.inbox-content');

    var loadDraft = function (el, startDate, endDate) {
           if(isEmpty($('#office-unit-id').val())){
                return;
            }
        var url = js_wb_root + "Performance/UnitContent/" + startDate + "/" + endDate;
        content.html('');
        $('.startdate').val(startDate);
        $('.enddate').val(endDate);
        
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: $('.searchForm').serialize(),
            dataType: "html",
            success: function (res) {
                content.html(res);

            },
            error: function (xhr, ajaxOptions, thrownError) {
                toggleButton(el);
            },
            async: false
        });
        var toggleButton = function(el) {
            if (typeof el == 'undefined') {
                return;
            }
            if (el.attr("disabled")) {
                el.attr("disabled", false);
            } else {
                el.attr("disabled", true);
            }
        };
    };

    return {
        //main function to initiate the module
        init: function (startDate, endDate) {
            loadDraft($(this), startDate, endDate);
        }

    };

}();