$(document).ready(function () {
    $("#svDT").on("click", function () {
        var designations = [];
        Metronic.blockUI({target: '.portlet-body', boxed: true});
        $(".permitted").each(function (i, v) {
            if (v.checked) {
                designations.push($(v).data('designation'));
            }
        });
        $.ajax({
            type: 'POST',
            url: $("#addData").data('url'),
            data: {"designation": designations},
            success: function (ans) {
                 if(ans.status == 'success'){
                    toastr.success(ans.msg);
                    window.location.reload();
                }else{
                     toastr.error(ans.msg);
                }
                Metronic.unblockUI('.portlet-body');
//                $('#showlist').html(data);
            },
            error: function(ans){
                 toastr.error(ans.msg);
                Metronic.unblockUI('.portlet-body');
            }
        });
    });
});
