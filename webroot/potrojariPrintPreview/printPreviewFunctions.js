'use strict';
// print();
const printSpace = (form) => {
  var json = JSON.parse(toJSONString(form));
  var printWrapper = document.querySelectorAll('.templateWrapper,.containerx');
  for (var i = 0; i < printWrapper.length; ++i) {

    printWrapper[i].style.cssText = 'padding-top:' + json.top + 'in;' +
      'padding-right:' + json.right + 'in;' +
      'padding-bottom:' + json.bottom + 'in;' +
      'padding-left:' + json.left + 'in;';
  }
}

document.addEventListener('DOMContentLoaded', function () {
  var form = document.querySelector('form#print_margin');
  printSpace(form);
  form.addEventListener('submit', function (e) {
    e.preventDefault();
    printSpace(form);
  }, false);
});

const toJSONString = (form) => {
  var obj = {};
  var elements = form.querySelectorAll('input, select, textarea');
  for (var i = 0; i < elements.length; ++i) {
    var element = elements[i];
    var name = element.name;
    var value = element.value;

    if (name) {
      obj[name] = value;
    }
  }
  return JSON.stringify(obj);
}

const setAttributes = (el, attrs) => {
  for (var key in attrs) {
    el.setAttribute(key, attrs[key]);
  }
}

const replaceClass = (classToAdd, classToRemove) => {
  var element = document.querySelectorAll('.' + classToRemove);
  if (element) {
    for (i = 0; i < element.length; i++) {
      element[i].classList.add(classToAdd);
      element[i].classList.remove(classToRemove);
    }
  }
}
const removeClass = (classToRemove) => {
  var element = document.querySelectorAll('.' + classToRemove);
  if (element) {
    for (i = 0; i < element.length; i++) {
      // console.log(element[i].classList)
      element[i].classList.remove(classToRemove);
    }
  }
}


var prevSibling = document.querySelectorAll('table');
for (var i = 0; i < prevSibling.length; i++) {
  if (prevSibling[i].previousElementSibling !== null) {
    prevSibling[i].previousElementSibling.classList.add('text-center');
  }
}


