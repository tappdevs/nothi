function viewDecisionPanel(){
    if($("#potroview_list").find('.attachmentsRecord.first').find('.showDecisionModal').length > 0){
        $.FroalaEditor.DefineIcon('3erd-party-decision', {NAME: 'fs0 a2i_jd_adeshnamatoiri2', template: 'material_design'});
        $.FroalaEditor.RegisterCommand('3erd-party-decision', {
            title: 'পত্র সিদ্ধান্ত',
            focus: true,
            undo: true,
            showOnMobile: true,
            refreshAfterCallback: true,
            callback: function () {
                $("#potroview_list").find('.attachmentsRecord.first').find('.showDecisionModal').trigger('click');
            }
        });
    }
}
function getButtonList(){
    if($("#potroview_list").find('.attachmentsRecord.first').find('.showDecisionModal').length > 0){
        var buttons = ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertLink', 'insertTable', '|', 'insertHR', 'clearFormatting', '|', 'print', 'undo', 'redo', 'guardFile', 'bibeccoPotro', 'bibeccoPotaka', 'bibeccoOnucced', 'attachment-ref', 'noteDecision', 'spell-checker', 'spell-checker-off','3erd-party-decision'];
    }else{
        var buttons = ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertLink', 'insertTable', '|', 'insertHR', 'clearFormatting', '|', 'print', 'undo', 'redo', 'guardFile', 'bibeccoPotro', 'bibeccoPotaka', 'bibeccoOnucced', 'attachment-ref', 'noteDecision', 'spell-checker', 'spell-checker-off'];
    }
    return buttons;
}
$(document).off('click', '.showDecisionModal').on('click', '.showDecisionModal', function () {
    Integration.showDecisionOptions($(this));
    $(document).off('click','.decisionSave').on('click','.decisionSave',function () {
        Integration.saveDecision($(this));
    });
});

var Integration = function(){
    var integration_info = {'nothi_office': '', 'part_id' : '', 'potro_id' : '','row_id' : '','actions':'','subject' : ''};
    return{
        setRequiredInfo: function(data){
            if(!isEmpty(data.nothi_office)){
                integration_info.nothi_office =data.nothi_office;
            }
            if(!isEmpty(data.part_id)){
                integration_info.part_id =data.part_id;
            }
            if(!isEmpty(data.potro_id)){
                integration_info.potro_id =data.potro_id;
            }
            if(!isEmpty(data.row_id)){
                integration_info.row_id =data.row_id;
            }
            if(!isEmpty(data.actions)){
                integration_info.actions =data.actions;
            }
            if(!isEmpty(data.subject)){
                integration_info.subject =data.subject;
            }

			$(document).off('click', '#potroDecision .decisions').on('click', '#potroDecision .decisions', function () {
				var decision_answer = parseInt($('.decisions:checked').val());
				if(typeof data.actions.decision_presentation_date != 'undefined') {
					if (data.actions.decision_presentation_date.includes(decision_answer)) {
						$('.presentation_date').removeClass('hide');
					}else{
						$('.presentation_date').addClass('hide')
					}
				}
			})
        },
        setNoteWithDecision: function(decision,redirect){
            if($(".addNewNote").length > 0 && $(".addNewNote").is(":visible")){
                $(".addNewNote").trigger('click');
                setTimeout(function () {
                    $('#noteComposer').froalaEditor('html.insert', (decision));
                }, 1000);
            }
            else if($(".fr-view").length > 0)
            {
                $('#noteComposer').froalaEditor('html.insert', ('<p>' + decision+'</p>'));
            }
            if($(".saveNote").length > 0 && $(".saveNote").is(":visible")){
                setTimeout(function () {
                    if ($('.saveNote').attr('hasnotorno') == -1 && $('#notesubject').val().length == 0) {
                        $('#notesubject').val(integration_info.subject);
                    }
                    if(!isEmpty(redirect)){
                        $(".saveNote").attr('stop-reload',1);
                        $(".saveNote").attr('reload-url',redirect);
                    }
                    $(".saveNote").trigger('click');
                }, 2000);
            }
        },
        saveDecision: function (that) {
            var decision_answer = parseInt($('.decisions:checked').val());
            var decision_answer_comment = $('.decisions_comment').val();
            var presentation_date = $('.presentation_date').val();
            var decision_text = $.trim($('.decisions:checked').closest('label').text());
            var nothi_office = parseInt(that.data('nothi-office'));
            var part_id = parseInt(that.data('part-id'));
            var potro_id = parseInt(integration_info.potro_id);
            var row_id = parseInt(integration_info.row_id);
            var actions = integration_info.actions;
            var subject = integration_info.subject;
            var setText = '';

            if($('.presentation_date').is(":visible") && isEmpty(presentation_date)){
                //check presentation date
                toastr.error('নির্বাচিত সিদ্ধান্তের সাথে উপস্থাপনের সময় দেওয়া আবশ্যক।');
                $('.presentation_date').focus();
                return false;
            }

            if(!isEmpty(decision_text)){
                setText += 'সিদ্ধান্তঃ <b>'+decision_text +' </b><br>';

            }
            if(!isEmpty(decision_answer_comment)){
                setText += '</br>'+decision_answer_comment +'</br>';

            }
            if(!isEmpty(presentation_date) && $('.presentation_date').is(":visible")){
                setText += '</br>উপস্থাপনের সময়ঃ <b>'+enTobn(presentation_date) +'</b></br>';

            }

            if($("#potroview_list").find('.attachmentsRecord.first').find('.showDecisionModal').length > 0){
                if($("#potroview_list").find('.attachmentsRecord.first').find('.imei_table_area').length > 0){
                    var clone = $("#potroview_list").find('.attachmentsRecord.first').find('.imei_table_area').outer();
                    setText += '<br>'+ clone +' </br>';
                }
            }

            $.ajax({
                url: js_wb_root+'apiPotroDecisionSave/'+nothi_office+'/'+part_id,
                data: {
                    potro_id: potro_id, decision_answer: decision_answer, comment: $.trim(decision_answer_comment).toString(),
					presentation_date: presentation_date
                },
                method: 'post',
                cache: false,
                dataType: 'JSON',
                success: function (response) {

                    if (response.status == 'error') {
                        toastr.error(response.message);
                        Metronic.unblockUI('.page-container');
                    } else {
                        toastr.success(response.message);
                        if(typeof actions.completable != 'undefined'){
                            if(actions.completable.includes(decision_answer)){
                                Integration.setNoteWithDecision(setText,js_wb_root+ 'editPotro/' +nothi_office + '/' + row_id);
                            }else {
                                Integration.setNoteWithDecision(setText);
                            }
                        }else {
                            Integration.setNoteWithDecision(setText);
                        }
                        setTimeout(function(){
                            $('#potroDecision').modal('toggle');
                        },2000);
                    }
                },
                error: function (xhr, status, errorThrown) {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-bottom-right"
                    };
                    Metronic.unblockUI('.page-container');
                    toastr.error("দুঃখিত! ইন্টারনেট সংযোগ বিঘ্নিত হয়েছে। কিছুক্ষণ পর আবার চেষ্টা করুন। ধন্যবাদ");
                }
            });
        },
        showDecisionOptions:function (that) {
            $('#potroDecision').find('.modal-body').html('');
            var decision = that.data('decision');
            var presentation_date_value = that.data('presentation_date');
            var comment = !isEmpty(that.data('comment'))?that.data('comment').toString():'';
            var actions = that.data('action-decisions');
            var decision_id = that.data('decision-id');
            var decision_locked = 0;//parseInt($(this).data('locked'));
            var decision_answer = -1;
            var row_id = parseInt(that.data('id'));
            var nothi_office = parseInt(that.data('nothi-office'));
            var part_id = parseInt(that.data('part-id'));
            var potro_id = that.data('potro-id');
            var subject = that.data('potro-subject');

            Integration.setRequiredInfo({'nothi_office':nothi_office,'part_id' : part_id,'potro_id' : potro_id,'row_id' : row_id,'actions' : actions,'subject':subject});
            if (that.data('decision-answer') >= 0) {
                decision_answer = that.data('decision-answer')
            }

            if(decision_locked){
                $('.decisionSave').addClass('hide')
            }else{
                $('.decisionSave').removeClass('hide')
            }

            $('#potroDecision').modal('show');
            $.each(decision, function (i, v) {
                var step = v.step;
                $('#potroDecision').find('.modal-body').append('<h4 class="bold underscore">' + step + '</h4>');
                $.each(v.decision, function (ind, dec) {

                    $('#potroDecision').find('.modal-body').append(
                        '<div class="form-group input-group"><label><input ' + (decision_locked ? 'disabled="disabled"' : (ind<decision_answer ?'disabled="disabled"' : '')) + ' class="decisions" type=radio name="decisions" value=' + ind + ' ' + (ind == decision_answer ? 'checked' : (decision_id >= 0 && decision_id == ind ? 'checked' : '')) + ' />  ' + dec + '</label></div>'
                    )
                })
            })

            var hideClass = 'hide';

			if(typeof actions.decision_presentation_date != 'undefined') {
				if (actions.decision_presentation_date.includes(decision_answer)) {
					hideClass = '';
				}
			}

			if(presentation_date_value==''){
			    var date = new Date();
				presentation_date_value = date.getFullYear() + '-' + parseInt(date.getMonth()+1) + '-' + date.getDate() + ' '+date.getHours() + ':'+date.getMinutes() + ':'+date.getSeconds()  ;
            }
            var presentation_date = '<input ' + (decision_locked ? 'disabled="disabled"' : '') + ' class="presentation_date form-control datepicker '+hideClass+'" type=text name="presentation_date" value="" placeholder="উপস্থাপনের সময় / Presentation Date" /><br/>';


            $('#potroDecision').find('.modal-body').append('<div class="form-group">' + presentation_date + '<input ' + (decision_locked ? 'disabled="disabled"' : '') + ' class="decisions_comment form-control" type=text name="decisions_comment" value="" placeholder="সিদ্ধান্ত লিখুন" /> </div>');

			$('.datepicker').datetimepicker({
                pickerPosition: "top-right",
				format: 'yyyy-mm-dd HH:mm:ss',
				autoclose: true,
                startDate: presentation_date_value
			});
        }
    }
}();

