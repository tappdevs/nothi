$(document).ready(function(){
    $(".cs-body-approve").click(function(){
        if(isEmpty($("#cs-body-final").html())){
            toastr.error('দুঃখিত! প্রতিবেদন পত্র দেওয়া হয়নি। দয়া করে পুনরায় সম্পাদনা করুন।');
            return;
        }
        Metronic.blockUI({
            target: '.page-container',
            boxed: true,
            message: 'অপেক্ষা করুন'
        });
        csSettings.setInfo(this);
    });
    $(".saveDraftNothiWithSignature").click(function(){
        Metronic.blockUI({
            target: '.page-container',
            boxed: true,
            message: 'অপেক্ষা করুন'
        });
        csSettings.setSignatureAndSave();
    });
    $(".saveDraftNothiCS").click(function(){
        $(".saveDraftNothi").click();
    });
});
var csSettings = function () {
    var user_info = {'name': '', 'designation' : '', 'office_name' : '','unit_name' : ''};
    return {
        init :function (employee_info) {
            user_info.name = (!isEmpty(employee_info.name_bng)?employee_info.name_bng:(!isEmpty(employee_info.officer_name)?employee_info.officer_name:''));
            user_info.designation = (!isEmpty(employee_info.designation)?employee_info.designation:'')
                + (!isEmpty(employee_info.incharge_label)?(' (' +employee_info.incharge_label + ')'):'');
            user_info.office_name = (!isEmpty(employee_info.office_name)?employee_info.office_name:'');
            user_info.unit_name = (!isEmpty(employee_info.office_unit_name)?employee_info.office_unit_name:'');
            user_info.designation_id = (!isEmpty(employee_info.office_unit_organogram_id)?employee_info.office_unit_organogram_id:'');
        },
        setInfo : function (that) {
            var cs_id = $(that).data('cs');
            if(isEmpty(cs_id)){
                return;
            }

            var last_signatured_user = $(".tab-pane .active #csBody-"+cs_id).find('#csSignatures').find('.cs-signatures').last().data('id');
            var last_signature_designation = $('.cs-signatures').last().data('designation-id');
            if(!isEmpty(last_signatured_user) && !isEmpty($('#username').val()) && last_signatured_user == $('#username').val() && !isEmpty(last_signature_designation) && last_signature_designation == user_info.designation_id){
                Metronic.unblockUI('.page-container');
                return;
            }

            var date = new Date();
            var current_date_time = date.getDate()+'-'+date.getMonth()+'-'+date.getFullYear()+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();
            var csSignaturesDom = $(".tab-pane .active #csBody-"+cs_id).find('#csSignatures');
            var csDom = $(".tab-pane .active #csBody-"+cs_id).find('#template-body');
            PROJAPOTI.ajaxSubmitAsyncDataCallback(js_wb_root + 'getSignature/' + $('#username').val() + '/1/'+'?token='+$('#user_signature_token').val(), {}, 'html', function (signature) {
                var html2add = '<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 text-center" style="padding:2px;"><div style="line-height: 1.2!important;font-size: 10px; color: darkviolet; display: block;  vertical-align: bottom;page-break-inside:auto!important;margin:0!important;padding:0!important; "><div class="btn btn-block " style="height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;"><img class="cs-signatures" style="height: 40px!important;padding:0!important;margin:0!important;" data-signdate="'+current_date_time+'" data-id="'+$('#username').val()+'" alt="সাইন যোগ করা হয়নি" src="'+signature+'"></div>' +
                    '<span style="border-top:1px solid #9400d3;font-size: 8pt;display: block;">'+enTobn(current_date_time)+'</span>' +
                    '<span style="font-size: 8pt;display: block;">'+user_info.name+'</span>' +
                    '<span style="font-size: 8pt;display: block;">'+user_info.designation + (!isEmpty(user_info.unit_name)?(', '+user_info.unit_name):'')+'</span>' +
                    '<span style="font-size: 8pt;display: block;">'+user_info.office_name+'</span>' +
                    '</div></div>';
                bootbox.dialog({
                    message: "আপনি কি প্রতিবেদনটি স্বাক্ষর করতে ইচ্ছুক?",
                    title: "প্রতিবেদন অনুমোদন ও স্বাক্ষর",
                    buttons: {
                        success: {
                            label: "হ্যাঁ",
                            className: "green",
                            callback: function () {
                                $(csSignaturesDom).append(html2add);
                                csSettings.setSignature(cs_id,$(csDom).html());
                            }
                        },
                        danger: {
                            label: "না",
                            className: "red",
                            callback: function () {
                                $('[data-cs='+cs_id+']').closest('span').removeClass('checked');
                                Metronic.unblockUI('.page-container');
                            }
                        }
                    }
                });

            });
        },
        setSignature : function(cs_id,html){
            if(!isEmpty(html) && !isEmpty(cs_id)){
                PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'potrojari/updateCsBody',{cs_id :cs_id,cs_body : html,is_signature : 1 },'json',function(response){
                    if(!isEmpty(response) && response.status == 'success'){
                        toastr.success(response.message);
                        setTimeout(window.location.reload(),2000);
                    }
                    else if(!isEmpty(response)){
                        toastr.error(response.message);
                    }else{
                        toastr.error('যান্ত্রিক ত্রুটি হয়েছে');
                    }
                    Metronic.unblockUI('.page-container');
                });
            }
        },
        setSignatureAndSave: function(){
            var last_signature_user = $('.cs-signatures').last().data('id');
            var last_signature_designation = $('.cs-signatures').last().data('designation-id');
            if(!isEmpty(last_signature_user) && !isEmpty($('#username').val()) && last_signature_user == $('#username').val() && !isEmpty(last_signature_designation) && last_signature_designation == user_info.designation_id){
                Metronic.unblockUI('.page-container');
                $('.saveDraftNothiCS').click();
                return;
            }
            var date = new Date();
            var current_date_time = date.getDate()+'-'+date.getMonth()+'-'+date.getFullYear()+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();
            var csSignaturesDom = $("#div-cs-body").find('#csSignatures');
            PROJAPOTI.ajaxSubmitAsyncDataCallback(js_wb_root + 'getSignature/' + $('#username').val() + '/1/'+'?token='+$('#user_signature_token').val(), {}, 'html', function (signature) {
                var html2add = '<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 text-center" style="padding:2px;"><div style="line-height: 1.2!important;font-size: 10px; color: darkviolet; display: block;  vertical-align: bottom;page-break-inside:auto!important;margin:0!important;padding:0!important; "><div class="btn btn-block " style="height: 40px;margin:0!important;padding: 0px!important;margin-bottom: 2px!important;"><img class="cs-signatures" style="height: 40px!important;padding:0!important;margin:0!important;" data-signdate="'+current_date_time+'" data-id="'+$('#username').val()+'" data-designation-id="'+user_info.designation_id+'" alt="সাইন যোগ করা হয়নি" src="'+signature+'"></div>' +
                    '<span style="border-top:1px solid #9400d3;font-size: 8pt;display: block;">'+enTobn(current_date_time)+'</span>' +
                    '<span style="font-size: 8pt;display: block;">'+user_info.name+'</span>' +
                    '<span style="font-size: 8pt;display: block;">'+user_info.designation + (!isEmpty(user_info.unit_name)?(', '+user_info.unit_name):'')+'</span>' +
                    '<span style="font-size: 8pt;display: block;">'+user_info.office_name+'</span>' +
                    '</div></div>';
                $(csSignaturesDom).append(html2add);

                var meta_data = $('input[name=meta_data]').val();
                if(!isEmpty(meta_data)){
                    meta_data = JSON.parse(meta_data);
                    meta_data.has_signature = user_info.designation_id;
                }else{
                    meta_data = {
                        has_signature : user_info.designation_id
                    };
                }
                meta_data = JSON.stringify(meta_data);
                $('input[name=meta_data]').val(meta_data);

                Metronic.unblockUI('.page-container');
                $('.saveDraftNothiCS').click();
            });
        }
    }
}();