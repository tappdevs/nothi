function editInfo(potrojari_language){
    console.log('init calendar');
    $.fn.editable.defaults.mode = 'inline';

//        jQuery.uniform.update('#inline');

        //global settings
        $.fn.editable.defaults.inputclass = 'form-control';
//      $.fn.editable.defaults.url = '/post';

        $.each($('span.canedit'), function (i, v) {
            var id = $(this).attr('id');
            var txt = $(this).text();
            var dataType = $(this).attr('data-type');

            if (id == 'sending_date') {
                $(this).replaceWith('<a data-placement="right"  data-pk="1" data-viewformat="dd.mm.yyyy" data-type="date" id="sending_date" href="#" class="editable editable-click">' + txt + '</a>');
            }
            if (id == 'sharok_no') {
                $(this).replaceWith('<a class="editable editable-click" id="sharok_no" data-type="text" data-pk="1">' + txt + '</a>');
            }
        });
        $('#sending_date').editable({
            rtl: Metronic.isRTL(),
            display: function (value) {
                var bDate = new Array("০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯");
//                var bMonth = new Array("০১", "০২", "০৩", "০৪", "০৫", "০৬", "০৭", "০৮", "০৯", "১০", "১১", "১২");
                var bMonth = new Array("জানুয়ারি", "ফেব্রুয়ারি", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর");
                var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

                if (value != '' && value != null) {
                    var dtb, dtb1, dtb2;
                    var dt = value.getDate();
                    if (dt >= 10) {
                        dtb1 = Math.floor(dt / 10);
                        dtb2 = dt % 10;
                        dtb = bDate[dtb1] + "" + bDate[dtb2];
                    } else {
                        dtb = bDate[0] + "" + bDate[dt];
                    }

                    var mnb;
                    var mn = value.getMonth();
                    mnb = bMonth[mn];

                    var yrb = "", yr1;
                    var yr = value.getFullYear();

                    for (var i = 0; i < 3; i++) {
                        yr1 = yr % 10;
                        yrb = bDate[yr1] + yrb;
                        yr = Math.floor(yr / 10);
                    }

                    yrb = bDate[yr] + "" + yrb;

                    if(potrojari_language == 'eng'){
                        var day = (value.getDate().length == 1)?'0'+value.getDate():value.getDate();
                         $(this).text(day + "/" + (mn + 1) + "/" + value.getFullYear());
                    }else{
                         $(this).text(" " + dtb + " " + mnb + " " + yrb).attr("style","position: relative; top: -8px;");
                    }


                    var that = $(this);
                    $('#potrojariDraftForm').find('.bangladate').next('br').remove();

                    $('#potrojariDraftForm').find('.bangladate').remove();
                      if(potrojari_language == 'eng'){
//                        that.before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" +value.getDate() + "/" + (mn + 1) + "/" + value.getFullYear() + "</span><br/>");
//                            $('#potrojariDraftForm').find("#sending_date_2");
//                            $('#potrojariDraftForm').find("#sending_date_2").before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" +value.getDate() + "/" + (mn + 1) + "/" + value.getFullYear() + "</span><br/>");
                    }else{
                        $.ajax({
                            url: js_wb_root + "banglaDate",
                            data: {date: value.getFullYear() + "-" + (mn + 1) + "-" + value.getDate()},
                            type: 'POST',
                            cache: false,
                            dataType: 'JSON',
                            async: false,
                            success: function (bangladate) {

                                that.before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");

                                $('#potrojariDraftForm').find("#sending_date_2").attr('style', 'position: relative; top: -8px;');
                                $('#potrojariDraftForm').find("#sending_date_2").before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");
                            }
                        });
                    }

                    if ($('#potrojariDraftForm').find("#sending_date_2")) {
                        $('#potrojariDraftForm').find("#sending_date_2").text($(this).text()).attr('readDate', value);
                    }
                } else {
                    var dtb, dtb1, dtb2;

                    value = new Date();
                    var dt = value.getDate();
                    if (dt >= 10) {
                        dtb1 = Math.floor(dt / 10);
                        dtb2 = dt % 10;
                        dtb = bDate[dtb1] + "" + bDate[dtb2];
                    } else {
                        dtb = bDate[0] + "" + bDate[dt];
                    }

                    var mnb;
                    var mn = value.getMonth();
                    mnb = bMonth[mn];

                    var yrb = "", yr1;
                    var yr = value.getFullYear();

                    for (var i = 0; i < 3; i++) {
                        yr1 = yr % 10;
                        yrb = bDate[yr1] + yrb;
                        yr = Math.floor(yr / 10);
                    }

                    yrb = bDate[yr] + "" + yrb;

                     if(potrojari_language == 'eng'){
                          var day = (value.getDate().length == 1)?'0'+value.getDate():value.getDate();
                         $(this).text(day + "/" + (mn + 1) + "/" + value.getFullYear());
                     }else{
                         $(this).text(" " + dtb + " " + mnb + " " + yrb ).attr("style","position: relative; top: -8px;");
                     }

//                    $(this).text(" " + dtb + " " + mnb + ", " + yrb + " খ্রিষ্টাব্দ");

                    var that = $(this);
                    $('#potrojariDraftForm').find('.bangladate').next('br').remove();
                    $('#potrojariDraftForm').find('.bangladate').remove();
                    if(potrojari_language == 'eng'){
//                        that.before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" +value.getDate() + "/" + (mn + 1) + "/" + value.getFullYear() + "</span><br/>");
//                            $('#potrojariDraftForm').find("#sending_date_2");
//                            $('#potrojariDraftForm').find("#sending_date_2").before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" +value.getDate() + "/" + (mn + 1) + "/" + value.getFullYear() + "</span><br/>");
                    }else{
                            $.ajax({
                            url: js_wb_root + "banglaDate",
                            data: {date: value.getFullYear() + "-" + (mn + 1) + "-" + value.getDate()},
                            type: 'POST',
                            cache: false,
                            dataType: 'JSON',
                            async: false,
                            success: function (bangladate) {
                                that.before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");

                                $('#potrojariDraftForm').find("#sending_date_2").attr('style', 'position: relative; top: -8px;');
                                $('#potrojariDraftForm').find("#sending_date_2").before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");
                            }
                        });
                    }


                    if ($('#potrojariDraftForm').find("#sending_date_2")) {
                        $('#potrojariDraftForm').find("#sending_date_2").text($(this).text()).attr('readDate', value);
                    }
                }
            }
        });
        $('#sharok_no').editable({
            name: "sharok_no",
            value: $('#potrojariDraftForm').find('#sharok_no').text() != '...' ? $('#potrojariDraftForm').find('#sharok_no').text() : ($nothino + ('.' + $totalPotrojari)),
            validate: function (value) {
                if ($.trim(value) == '')
                    return 'এই তথ্য দিতে হবে';
            },
            display: function (value) {
                $(this).text(value);
                var onu = $('#potrojariDraftForm').find('.onulipi_users').length > 0 ? $.map($('#potrojariDraftForm').find('.onulipi_users'),function(data){
                    return parseInt($(data).attr('group_member'));
                }).reduce(function(total,num){return total+num; }) : 0;

                var bn = replaceNumbers("'" + (onu) + "'");
                bn = bn.replace("'", '');
                bn = bn.replace("'", '');
                if(potrojari_language == 'eng'){
                    $('#potrojariDraftForm').find("#sharok_no2").text(value + (onu > 0 ? ('/1(' + onu + ')') : ''));
                }else{
                    $('#potrojariDraftForm').find("#sharok_no2").text(value + (onu > 0 ? ('/১(' + bn + ')') : ''));
                }
            }
        });
      
}
var numbers = {
    1: '১',
    2: '২',
    3: '৩',
    4: '৪',
    5: '৫',
    6: '৬',
    7: '৭',
    8: '৮',
    9: '৯',
    0: '০'
};
function replaceNumbers(input) {
    var output = [];
    for (var i = 0; i < input.length; ++i) {
        if (numbers.hasOwnProperty(input[i])) {
            output.push(numbers[input[i]]);
        } else {
            output.push(input[i]);
        }
    }
    return output.join('');
}
function savePotrojari(){
   // var id = $("#id").val();
    removAllEditable();
    var body = $("#template-body").html();
    var sharokNo = $("#sharok_no").html();
    PROJAPOTI.ajaxSubmitDataCallback($("#potrojariDraftForm").attr('action'),{'body' : body, 'sharok_no' : sharokNo},'json',function(res){
        if(res.status == 'success'){
            toastr.success(res.msg);
            setTimeout(function(){
                window.location.href = $('.btn-nothiback').attr('href');
            },1000);
        }else{
            toastr.error(res.msg);
        }
    });
}
function removAllEditable(){
     $.each($('#template-body').find('a.editable'), function (i, v) {
                    var id = $(this).attr('id');
                    var dataType = $(this).attr('data-type');
                    if (dataType == 'textarea') {
                        var txt = $(this).html();
                    } else {
                        var txt = $(this).text();
                    }
                    if(id=="to_div_item") {
                        $(this).replaceWith("<span class='canedit' style='white-space:pre-wrap;' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + dataType + "'>" + txt + "</span>");
                    }else {
                        $(this).replaceWith("<span class='canedit' id='" + ((typeof (id) != 'undefined') ? id : '') + "' data-type='" + (dataType == 'date' || dataType == 'textarea' ? dataType : 'text') + "'>" + txt.replace(/(?:\r\n|\r|\n)/g, '<br />') + "</span>");
                    }
                });
}
  