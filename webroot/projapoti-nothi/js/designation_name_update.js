var url_new = js_wb_root + 'officeEmployees/designationNameSearch';
var EmployeeAssignment = {
	loadMinistryWiseLayers: function () {
		OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

		});
	},
	loadMinistryAndLayerWiseOfficeOrigin: function () {
		OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

		});
	},
	loadOriginOffices: function () {
		OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

		});
	},
	loadOfficeUnits: function () {
		OfficeSetup.loadOfficeUnits($("#office-id").val(), function (response) {

		});
	},
};
$("#office-ministry-id").bind('change', function () {
	EmployeeAssignment.loadMinistryWiseLayers();
});
$("#office-layer-id").bind('change', function () {
	EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
});
$("#office-origin-id").bind('change', function () {
	EmployeeAssignment.loadOriginOffices();
});
$("#office-id").bind('change', function () {
	EmployeeAssignment.loadOfficeUnits();
});
$("#office-unit-id").bind('change', function () {
//            console.log($(this).val());
	PROJAPOTI.ajaxLoadWithRequestData(url_new,
		{
			'ministry_id': $("#office-ministry-id").val(),
			'layer_id': $("#office-layer-id").val(),
			'origin_id': $("#office-origin-id").val(),
			'office_id': $("#office-id").val(),
			'unit_id': $(this).val()
		},
		'#sample_1');
//            getNotification($(this).val());
});

function getNotification() {
	PROJAPOTI.ajaxLoadWithRequestData(url_new,
		{
			'ministry_id': $("#office-ministry-id").val(),
			'layer_id': $("#office-layer-id").val(),
			'origin_id': $("#office-origin-id").val(),
			'office_id': $("#office-id").val(),
			'unit_id': $("#office-unit-id").val()
		},
		'#sample_1');
}


$(document).on('click', '.saveDesignation', function () {
	var i = 0;
	Metronic.blockUI({
		target: '.portlet.box.green',
		boxed: true
	});
	$.each($('.designationLabel'), function () {
		var data = {
			id: $(this).find('.designation_id').attr('id'),
			designation_level: $(this).find('[name=designation_level]').val(),
			designation_sequence: $(this).find('[name=designation_sequence]').val()
		};

		$.ajax({
			url: js_wb_root + 'officeEmployees/officeEmployeesDesignationUpdate',
			data: data,
			method: 'post',
			success: function (res) {
				i++;
				if ($('.designationLabel').length == i) {
					toastr.success("সংরক্ষিত হয়েছে");
					Metronic.unblockUI('.portlet.box.green');
				}
			}
		})
	})


});

$(document).ready(function () {
	//PROJAPOTI.ajaxLoad(url_new, '#sample_1');
});

$(document).off('click', '.pagination a').on('click', '.pagination a', function (ev) {
	ev.preventDefault();
	PROJAPOTI.ajaxLoad($(this).attr('href'), '#sample_1');
});
$(document).off('click', '#searchDesignation').on('click', '#searchDesignation', function () {
	PROJAPOTI.ajaxLoadWithRequestData(url_new, {
		'search': $('#search-designation').val(),
		'ministry_id': $("#office-ministry-id").val(),
		'layer_id': $("#office-layer-id").val(),
		'origin_id': $("#office-origin-id").val(),
		'office_id': $("#office-id").val(),
		'unit_id': $('#office-unit-id').val()
	}, '#sample_1');
});
$(document).off('click', '#refresh').on('click', '#refresh', function () {
	PROJAPOTI.ajaxLoadWithRequestData(url_new, {'reset': true}, '#sample_1');
});

function update(id) {
	var bn_text = $('#designation_bn' + id + '').text();
	var en_text = $('#designation_en' + id + '').text();
	$('#update_text_bng').val(bn_text);
	$('#update_text_eng').val(en_text);
	$('#update_id').val(id);
}

function update_action() {
	var bn_text = $('#update_text_bng').val();
	var en_text = $('#update_text_eng').val();
	var id = $('#update_id').val();
	if (bn_text == '' || typeof (bn_text) == 'undefined' || bn_text == 'undefined' || bn_text == null) {
		toastr.error('পদবির নাম খালি রাখা যাবে না।');
		return;
	}
//       console.log(text+':'+id);
	$.ajax({
		type: 'POST',
		url: js_wb_root + 'OfficeEmployees/designationNameSave',
		data: {"bn_text": bn_text, "en_text": en_text, "id": id},
		success: function (data) {
			if (data.status == 'success') {
				toastr.success(data.msg);
				if ($(".pagination .active a").attr('href') == '' || typeof ($(".pagination .active a").attr('href')) == 'undefined' || $(".pagination .active a").attr('href') == 'undefined' || $(".pagination .active a").attr('href') == null) {
					getNotification();
				} else {
					$(".pagination .active a").click();
				}
				$("#yourModal").modal('toggle');
			} else {
				toastr.error(data.msg);
			}
		}
	});
}
