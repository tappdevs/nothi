$(document).ready(function(){
    if(!isEmpty(api)) {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-center"
        };
    }
});


         $(".default_sign").on('change',function(){
             if(isEmpty(api)){
                 var selectedSign = ($("#electronic").is(' :checked'))?0:($("#soft_signature").is(' :checked'))?1:($("#hard_signature").is(' :checked')?2:-1);

             }else{
                 var selectedSign = $('#default_sign_options :selected').val();
                 $("#availableSignatureBody").html('');
             }
            if(selectedSign == -1){
                return;
            }
            var msg = (selectedSign ==1 || selectedSign ==2)?' ডিজিটাল সাইন নির্বাচন করলে এরপর প্রতি স্বাক্ষরের সময় আপনাকে '+((selectedSign==1)?'<b>সফট টোকেন</b>':'<b>হার্ড টোকেন</b>')+' এর সহযোগিতা নিতে হবে।':'';
              bootbox.dialog({
                        message: msg+" আপনি কি স্বাক্ষরের ধরন পরিবর্তন করতে ইচ্ছুক?",
                        title: "স্বাক্ষরের ধরন পরিবর্তন",
                        buttons: {
                            success: {
                                label: "হ্যাঁ",
                                className: "green",
                                callback: function () {
                                    if(selectedSign ==1 || selectedSign ==2){
                                        if(isEmpty(api)){
                                            $("#signatureSetting").modal('toggle');
                                            $("#showSignOptions").hide();

                                            $('#issueDate').datepicker({
                                                rtl: Metronic.isRTL(),
                                                autoclose: true,
                                                format: "mm/dd/yyyy"
                                            });
                                        }else {
                                            $("#signatureSetting").show();
                                        }

                                    }else{
                                        digitalSignature.setUserSignature(selectedSign);
                                    }
                                }
                            },
                            danger: {
                                label: "না",
                                className: "red",
                                callback: function () {
                                    Metronic.unblockUI('.page-container');
                                }
                            }
                        }
                    });
        });
        $("#ProofType").on('change',function(){
            var val= $("#ProofType :selected").val();
            $("#ProofNo").val('');
            if(!isEmpty(val) && val == 1){
                $("#ProofNo").val($("#nid").val());
            }
        });
    var digitalSignature = {
        setUserSignature: function(selectedSign,el){
            var data = {sign : selectedSign,id: $("#rcrd_id").val()};
            if(!isEmpty(el)){
                data.ca = $(el).data('ca');
                data.certSerialNumber = $(el).data('cert-serial-number');
                data.ProofType =$("#ProofType :selected").val();
                data.ProofNo = $('#ProofNo').val();
                data.tokenType = $(el).data('token-type');
            }
            if(!isEmpty(api)){
                data.api_key = $('#api_key').val();
                data.data_ref = 'api';
            }
             Metronic.blockUI({
                target: '.page-container',
                boxed: true,
                message: 'অপেক্ষা করুন'
            });
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'employeeRecords/setDefaultSignature', data, 'json', function (res) {
                        if(res.status == 'success'){
                            if(selectedSign){
                                if(isEmpty(api)){
                                    $("#signatureSetting").modal('toggle');
                                    // if(!isEmpty(MyAndroid)){
                                    //    MyAndroid.receiveValueFromJs(true);
                                    // }
                                }else{
                                    $("#signatureSetting").hide();
                                }
                            }
                            toastr.success(res.msg);
                        }else{
                             toastr.error(res.msg);
                        }
                        Metronic.unblockUI('.page-container');
                    });
        },
        getUserSignature: function(){
            $("#showSignOptions").show();
            $("#availableSignatureBody").html('<tr><td colspan=4> লোড হচ্ছে...  </td></tr>');
            var data ={};
            data.ProofType= $("#ProofType :selected").val();
            data.ProofNo= $("#ProofNo").val();
                if(isEmpty(api)){
                    var selectedSign = ($("#electronic").is(' :checked'))?0:($("#soft_signature").is(' :checked'))?1:($("#hard_signature").is(' :checked')?2:-1);

                }else{
                    var selectedSign = $('#default_sign_options :selected').val();
                    data.api_key = $('#api_key').val();
                    data.data_ref = 'api';
                }
                data.id = $("#rcrd_id").val();
                PROJAPOTI.ajaxSubmitDataCallback(ds_url+'employeeRecords/getDigitalSignatureSettings', data, 'json', function (res) {
                if(res.status == 'success'){
                   if(!isEmpty(res.data) && res.data.length > 0){
                        var html = '';
                       $.each(res.data,function(i,v){
                           if((selectedSign == 1 && v.tokenType == 'Soft') || (selectedSign == 2 && v.tokenType == 'Hard') ){
                                  html += '<tr><td>'+v.ca+'</td><td>'+v.certSerialNumber+'</td><td>'+v.tokenType+'</td><td><button class="btn btn-primary btn-xs" data-ca='+v.ca+' data-cert-serial-number='+v.certSerialNumber+' data-token-type='+v.tokenType+'  onclick="digitalSignature.setUserSignature('+selectedSign+',this)">ব্যবহার করুন</button></td></tr>';
                           }else{
                               // html += '<tr><td>'+v.ca+'</td><td>'+v.certSerialNumber+'</td><td>'+v.tokenType+'</td><td></td></tr>';
                           }
                       });
                       if(html.length ==0){
                           html ='<tr class="text-center"><td colspan=4 class="danger">'+ ((selectedSign == 1)?'সফট টোকেন':'হার্ড টোকেন') +' সম্পর্কিত কোন তথ্য পাওয়া যায়নি। </td></tr>'
                       }
                       $("#availableSignatureBody").html(html);
                   }else{
                        $("#availableSignatureBody").html('<tr class="text-center"><td colspan=4 class="danger"> কোন তথ্য পাওয়া যায়নি। </td></tr>');
                   }
                }else{
                     toastr.error(res.msg);
                }
            });
        }
    };
    $(document).ready(function(){
        $("#cert-file").on('click',function(){
           $("#cert-file-use").show();
        });
    });

 var certificateContent = '';

function getBase64(file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        return  reader.result;
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
        return false;
    };
}

    function setSignatureByCertFile(){
        var promise = function () {
            return new Promise(function (resolve, reject) {
                var file = $('#cert-file').prop('files')[0];
                if(!isEmpty(file)) {

                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        resolve(reader.result);
                    };
                } else {
                    reject('সার্টিফিকেট ফাইল নির্বাচন করুন!')
                }

            });
        };
        promise().then(function (file_base64) {
            setSignatureByCertFileReq(file_base64);
        }).catch (function (error) {
            return toastr.error(error);
        });
    }
    function setSignatureByCertFileReq(file_base64){
    file_base64 = file_base64.split('base64,');

        if(!isEmpty($('#identityProfType').val())){
            var identityProfType = $('#identityProfType').val();
        } else {
            return toastr.error('স্বাক্ষরের মাধ্যম নির্বাচন করুন!');
        }

        if(!isEmpty($('#identityProofNo').val())){
            var identityProofNo = $('#identityProofNo').val();
        } else {
            return toastr.error('স্বাক্ষরের মাধ্যম নং লিখুন!');
        }

        if(!isEmpty($('#caName').val())){
            var caName = $('#caName').val();
        } else {
            return toastr.error('সার্টিফিকেট সরবরাহকারী প্রতিষ্ঠানের নাম লিখুন!');
        }

        if(!isEmpty($('#serialNumber').val())){
            var serialNumber = $('#serialNumber').val();
        } else {
            return toastr.error('সিরিয়াল নাম্বার লিখুন!');
        }

        if(!isEmpty($('#certificateValidity').val())){
            var certificateValidity = $('#certificateValidity').val();
        } else {
            return toastr.error('মেয়াদউত্তীর্ণের সাল নির্বাচন করুন!');
        }

        if(!isEmpty($('#issueDate').val())){
            var issueDate = $('#issueDate').val();
        } else {
            return toastr.error('ইস্যুর তারিখ নির্বাচন করুন!');
        }

        if(!isEmpty($('#password').val())){
            var password = $('#password').val();
        } else {
            return toastr.error('পাসওয়ার্ড লিখুন!');
        }

        if(!isEmpty($('#cnfm_password').val()) && ($('#cnfm_password').val() == password)){
            var cnfm_password = $('#cnfm_password').val();
        } else {
            return toastr.error('পাসওয়ার্ড নিশ্চিত করুন!');
        }

        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'employeeRecords/dsCertificateUpload', {
            'identityProfType':identityProfType,
            'identityProofNo':identityProofNo,
            'caName': caName,
            'serialNumber': serialNumber,
            'certificateValidity':certificateValidity,
            'issueDate':issueDate,
            'password':password,
            'cnfm_password':cnfm_password,
            'certificateContent':file_base64[1]
        }, 'json', function (res) {
            if(res.status == 'success'){
                if(!isEmpty(res.data)){
                    toastr.success(res.data);
                } else{
                    toastr.success(res.message);

                }

                $('#ProofType').val(identityProfType);
                $('#ProofNo').val(identityProofNo);
                $(".DS-file-attach").click();
                digitalSignature.getUserSignature();

            }else{
                if(!isEmpty(res.data)){
                    toastr.error(res.data);
                } else{
                    toastr.error(res.message);

                }
            }
        });
    }
