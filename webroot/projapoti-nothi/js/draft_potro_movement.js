
var DraftPotroMovement = function () {


    var draftForward = function (el, name, form_id)
    {
        var my_data = $("#" + form_id).serialize();
        var dak_id = $(el).attr("dak_id");
        var potrojari_draft_id = $("#potrojari_draft_id").val();
        var url = js_wb_root + "potrojari/send_draft";
        var template_content = $("#template_content").val();

        $.ajax({
            type: "post",
            cache: false,
            url: url,
            dataType: "json",
            data: {'potrojari_draft_id': potrojari_draft_id, 'my_data': my_data, 'template_content': template_content},
            success: function (res)
            {
                toastr.success("ড্রাফট ফরোয়ার্ড করা হয়েছে").delay(800);
                //res.delay(800);
                //window.location.href = js_wb_root + "potrojari/received_drafts";
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                toastr.success("দুঃখিত! ড্রাফট ফরোয়ার্ড করা সম্ভব হয়নি।");
            },
            async: false
        });
    }
    
    var getTemplate = function ()
    {

        var template_id = $("#potro-type").val();
        var url = js_wb_root + "potrojari/get_potrojari_template";

        $.ajax({
            type: "post",
            cache: false,
            url: url,
            dataType: "json",
            data: {'template_id': template_id},
            success: function (res)
            {
                CKEDITOR.instances.template_content.setData(res.html_content);
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(thrownError);
            },
            async: false
        });
    }

    return {
        //main function to initiate the module
        init: function () {

            $('.draft-content').on('click', '#forwardSingle', function () {
                draftForward($(this), 'inbox', 'decision_form');
            });
            
            $("#potro-type").change(function(){
                if($("#potro-type").val() != ""){
                    
                    getTemplate();
                }
            });

        }

    };

}();



