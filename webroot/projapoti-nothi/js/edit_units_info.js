var url_new = js_wb_root+  'officeEmployees/getUnitInfoForEdit';
    var EmployeeAssignment = {
        loadMinistryWiseLayers: function () {
            OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

            });
        },
        loadMinistryAndLayerWiseOfficeOrigin: function () {
            OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

            });
        },
        loadOriginOffices: function () {
            OfficeSetup.loadOriginOffices($("#office-origin-id").val(), function (response) {

            });
        },
    };
    $("#office-ministry-id").bind('change', function () {
        EmployeeAssignment.loadMinistryWiseLayers();
    });
    $("#office-layer-id").bind('change', function () {
        EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
    });
    $("#office-origin-id").bind('change', function () {
        EmployeeAssignment.loadOriginOffices();
    });
    $("#office-id").bind('change', function () {
//            console.log($(this).val());
       getNotification();
//            getNotification($(this).val());
    });
    function getNotification() {
                    PROJAPOTI.ajaxLoadWithRequestData(url_new,
                {
                    'ministry_id': $("#office-ministry-id").val(),
                    'layer_id': $("#office-layer-id").val(),
                    'origin_id': $("#office-origin-id").val(),
                    'office_id': $("#office-id").val(),
                },
                '#sample_1');
    }

    function update(id)
    {
        var bn_text = $('#unit_bn' + id + '').text();
        var en_text = $('#unit_en' + id + '').text();
        $('#update_text_bng').val(bn_text);
        $('#update_text_eng').val(en_text);
        $('#update_id').val(id);
    }
    function update_action()
    {
        var bn_text = $('#update_text_bng').val();
        var en_text = $('#update_text_eng').val();
        var id = $('#update_id').val();
        if (bn_text == '' || typeof (bn_text) == 'undefined' || bn_text == 'undefined' || bn_text == null) {
            toastr.error('শাখার নাম খালি রাখা যাবে না।');
            return;
        }
         $("#yourModal").modal('toggle');
//       console.log(text+':'+id);
        $.ajax({
            type: 'POST',
            url: js_wb_root +'OfficeEmployees/unitInfoUpdate',
            data: {"bn_text": bn_text, "en_text": en_text, "id": id},
            success: function (data) {
                if (data.status == 'success') {
                    toastr.success(data.msg);
                      getNotification();
                  
                } else {
                    toastr.error(data.msg);
                     $("#yourModal").modal('toggle');
                }
            }
        });
    }
