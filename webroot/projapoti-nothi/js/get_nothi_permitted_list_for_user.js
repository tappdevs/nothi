    $(document).off('click', '.selectPartNo');
    $(document).on('click', '.selectPartNo', function () {

        $(this).toggleClass('active');
        $(this).siblings().removeClass('active');
    });

    $(document).off('click', '.nothipermittedlist');
    $(document).on('click', '.nothipermittedlist', function () {
        $(this).toggleClass('active');
        $(this).siblings().removeClass('active');
        $("#nothi_list_datatable").dataTable().fnDestroy();
        $('#nothi_list_datatable').find('tbody').html('');
        $('.selectedNothiName').text('নথি বাছাই করুন');


        if ($(this).hasClass('active')) {
             $('.selectedNothiName').text("নথি নম্বর: " + $(this).find('td').eq(3).text());
            $.ajax({
               url: js_wb_root+ 'NothiMasters/getPart/' + $(this).find('td').eq(0).attr('nothi_id') + '/' + $("#dont_show_new_note_option").val(),
               method: 'post',
               success: function(response){
                   $('#nothi_list_datatable').find('tbody').html(response);

                   initTable1();
               }
            });
        }
    });

    var initTable1 = function () {


        var table = $('#nothi_list_datatable');

        var oTable = table.dataTable({
             destroy: true,
            "bLengthChange": false,
             "scrollY":        "500px",
            "scrollCollapse": true,
            loadingMessage: 'লোড করা হচ্ছে...',
            "language": {
                "emptyTable": "কোনো তথ্য নেই",
                "info": "<span class='seperator'>|</span>মোট _TOTAL_ টি তথ্য পাওয়া গেছে",
                "infoEmpty": "কোনো তথ্য নেই",
                "infoFiltered": "<span class='seperator'>|</span>মোট _TOTAL_ টি তথ্য পাওয়া গেছে",
                "search": "খুঁজুন  ",
                "zeroRecords": "কোনো তথ্য নেই"
            },

            "serverSide": false,
            "paging": false,
            "ordering": false,
            "info": false,
            "bStateSave": false,
            // set the initial value
            "pageLength": -1,
            "dom": "<'table-scrollable't>"
        });

        $(document).on('keyup change','#nothisubject', function(){
            oTable.api().search($(this).val()).draw();
        });

    }

    var initTable2 = function () {


        var table = $('.nothiprmittedlst');

        var oTable = table.dataTable({

            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
//            "scrollY": "100",
            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 50, 100, 500],
                ["১০", "২০", "৫০", "১০০", "৫০০"] // change per page values here
            ],
            "pageLength": 10, // default record count per page

            loadingMessage: 'লোড করা হচ্ছে...',
            "language": { // language settings
                    // metronic spesific
                    "metronicGroupActions": "_TOTAL_ রেকর্ড নির্বাচন করা হয়েছে:  ",
                    "metronicAjaxRequestGeneralError": "অনুরোধ সম্মন্ন করা সম্ভব হচ্ছে না।",

                    // data tables spesific
                    "lengthMenu": "<span class='seperator'></span>দেখুন _MENU_ ধরন",
                    "info": "মোট _TOTAL_ টি পাওয়া গেছে",
                    "infoEmpty": "",
                    "sSearch": "খুঁজুন: ",
                    "emptyTable": "কোনো রেকর্ড  নেই",
                    "zeroRecords": "কোনো রেকর্ড  নেই",
                    "paginate": {
                        "previous": "প্রথম",
                        "next": "পরবর্তী",
                        "last": "শেষের",
                        "first": "প্রথম",
                        "page": "পাতা",
                        "pageOf": "এর"
                    }
                },
            "serverSide": false,
            "ordering": false,
             "infoFiltered": "( খোঁজা হয়েছে _MAX_ টি তথ্য থেকে)",
        });

    }
    initTable2();