//function selectNote(){
//      var  master_id = $("#master_id").val();
//      var  nothi_office = $("#nothi_office").val();
//      $('#responsiveOnuccedModal').find('.modal-title').text('');
//    $('#responsiveOnuccedModal').find('.modal-body').html('');
//        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'NothiMasters/getNothiPartListByMasterID',
//            {'master_id': master_id, 'nothi_office': nothi_office}, 'json', function (response) {
//                
//        if (response.status == 'success') {
//            if(response.data){
//                var html = '<table class="table table-bordered table-hover">';
//                html += '<thead><tr class="heading" ><th colspan="2">নথির বিসয়ঃ '+response.data.name+'</th></tr><tr class="heading"><th> পার্ট</th><th> নির্বাচন</th> </tr></thead><tbody>';
//                $.each(response.data.part_list,function(i,v){
//                    html += '<tr><th class="text-center">'+v+'</th>';
//                    html += '<th class="text-center">'+i+'</th></tr>';
//                    html += '<th class="text-center">'
//                        +'<div class="input-group"><span class="input-group-addon"><input type="radio" aria-label="Radio button for following text input">'
//                        +'</span><input type="text" class="form-control" aria-label="Text input with radio button"></div>'
//                                +'</th></tr>';
//                    
//                });
//                html += '</tbody></table>';
//            }
//            $('#responsiveOnuccedModal').find('.modal-title').text('পত্রজারি ক্লোন করার জন্য  নথির পার্ট নির্বাচন ');
//    $('#responsiveOnuccedModal').find('.modal-body').html(html);
//    $('#responsiveOnuccedModal').modal('show');
////            toastr.success(response.msg);
////            window.location.href = js_wb_root +'dashboard/dashboard';
//        } else {
//            toastr.error(response.msg);
//        }
////        Metronic.unblockUI('#ajax-content');
//    });
////    nothiMasters/userNothiList
//}
function selectNote(potrojari_id){
    if(!checkBrowserIsChrome()){
        return;
    }
      var  master_id = $("#master_id").val();
      var  nothi_office = $("#nothi_office").val();
      if(typeof(potrojari_id) == 'undefined'){
          potrojari_id = 0;
      }

      $('#responsiveOnuccedModal').find('.modal-title').text('');
    $('#responsiveOnuccedModal').find('.modal-body').html('');
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'NothiMasters/showAllPermittedNothiMasterList',
            {'master_id': master_id, 'nothi_office': nothi_office,'potrojari_id':potrojari_id}, 'html', function (response) {
        if (response) {
            $('#responsiveOnuccedModal').find('.modal-title').text('পত্রজারি ক্লোন করার জন্য  নথির নোট  নির্বাচন ');
    $('#responsiveOnuccedModal').find('.modal-body').html(response);
    $('#responsiveOnuccedModal').modal('show');
//            toastr.success(response.msg);
//            window.location.href = js_wb_root +'dashboard/dashboard';
        } else {
            toastr.error('Something went wrong.');
        }
//        Metronic.unblockUI('#ajax-content');
    });
}
function checkBrowserIsChrome(){
    var client = new ClientJS();
    if (!client.isChrome()) {
        toastr.info('<a target="_blank" href="https://www.google.com/chrome/">Google Chrome ব্যতিত অন্য কোন ব্রাউজার দিয়ে পত্র খসড়া/সংশোধন করা যাবে না। দয়া করে Google Chrome ব্যবহার করুন। Google Chrome ইন্সটল না থাকলে এখানে ক্লিক করুন </a>');
        return false;
    }
    return true;
}
    function createClonePotrojari(potrojari_id, nothi_master_id,bootbox_skip){
        if(!checkBrowserIsChrome()){
            return;
        }
    $('.noteCreateButton').attr("disabled","disabled");
         var  nothi_office = $("#nothi_office").val();
         if(potrojari_id == 0){
             potrojari_id = $("#potrojari_id").val();
         }
        if(nothi_master_id == 0 || typeof(nothi_master_id) == 'undefined' || isNaN(nothi_id)){
            nothi_master_id = $('#responsiveOnuccedModal').find('.selectPartNo.active').find('td').eq(0).attr('nothi_masters_id');
        }

        var  nothi_id = $('#responsiveOnuccedModal').find('.selectPartNo.active').find('td').eq(0).attr('nothi_parts_id');
        if(nothi_id == 0 || typeof(nothi_id) == 'undefined' || isNaN(nothi_id)) {
           nothi_id = parseInt($('#nothi_part_no').val());
        }

         if((isEmpty(nothi_id) && ($('#responsiveOnuccedModal').find('.selectPartNo.active').find('td').eq(0).hasClass('newPartCreate')== false)) && isEmpty(nothi_master_id)){
                 toastr.error(' দয়া করে নথি ও নোট বাছাই করে পুনরায় চেষ্টা করুন।  ')
             $('.noteCreateButton').removeAttr('disabled'); 
            return false;
             }
             
         if(isEmpty(potrojari_id)){
               toastr.error("দুঃখিত! সকল তথ্য সঠিকভাবে দেওয়া হয়নি।");
                $('.noteCreateButton').removeAttr('disabled'); 
               return false;
             }
             
         
        if (!isEmpty(bootbox_skip)) {
             var data = [potrojari_id, nothi_master_id, nothi_id, nothi_office];
             var data_url = js_wb_root + 'potrojari/potrojariClone/' + data[0] + '/' + data[1] + '/' + data[2] + '/' + data[3];
            window.location.href = data_url;
            return;
        }
            bootbox.dialog({
                message: "আপনি কি পত্রজারি ক্লোন করতে ইচ্ছুক?",
                title: "পত্রজারি ক্লোন",
                buttons: {
                    success: {
                        label: "হ্যাঁ",
                        className: "green",
                        callback: function () {
                            var promise = new Promise(function (resolve, reject) {

                                if($('#responsiveOnuccedModal').find('.selectPartNo.active').find('td').eq(0).hasClass('newPartCreate')== true){
                                    $.ajax({
                                        url : js_wb_root + 'nothiMasters/add',
                                        data: {formPart: {nothi_master_id: nothi_master_id}},
                                        type: "POST",
                                        dataType: 'JSON',
                                        success: function (response) {
                                            $('div.error-message').remove();
                                            if (response.status == 'error') {
                                                $('.noteCreateButton').removeAttr('disabled');
                                                reject(response.msg);
                                            } else if (response.status == 'success') {
                                                nothi_id=  response.id;
                                                var data = [potrojari_id, nothi_master_id, nothi_id, nothi_office];
                                                resolve(data);
                                            }
                                        }
                                    });
                                } else {
                                    var data = [potrojari_id, nothi_master_id, nothi_id, nothi_office];
                                    resolve(data);
                                }
                            }).then(function (data) {
                                var data_url = js_wb_root + 'potrojari/potrojariClone/' + data[0] + '/' + data[1] + '/' + data[2] + '/' + data[3];
                                if (!isEmpty(bootbox_skip)) {
                                    window.location.href = data_url;
                                    return;
                                }
                                window.location.href = data_url;
                                return;
                            }).catch(function (err) {
                                toastr.error(err);
                            });
                        }
                    },
                    danger: {
                        label: "না",
                        className: "red",
                        callback: function () {
                            $('.noteCreateButton').removeAttr('disabled'); 
                        }
                    }
                }
            });

    }
    function createClonePotrojariForSelectedNothi(potrojari_id, nothi_master_id){
        if(!checkBrowserIsChrome()){
            return;
        }
        bootbox.dialog({
                    message: "আপনি কি নির্বাচিত নোটে" +(!isEmpty($("#nothi_subject").val())?'<b> ( বিষয়ঃ  '+$("#nothi_subject").val()+' )</b> ':' ')+"পত্রজারি ক্লোন করতে ইচ্ছুক?",
                    title: "পত্রজারি ক্লোন",
                    buttons: {
                        success: {
                            label: "হ্যাঁ",
                            className: "green",
                            callback: function () {
                               createClonePotrojari(potrojari_id, nothi_master_id,1);
                            }
                        },
                        danger: {
                            label: "না",
                            className: "red",
                            callback: function () {
//                                $('#nothi_part_no').val(0);
                                selectNote();
                            }
                        }
                    }
                });
    }