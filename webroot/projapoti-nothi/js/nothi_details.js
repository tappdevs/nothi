function checkKhosraAndSignatureDate(language,url,potro_type){
    $('.dateChange').hide();
    if(potro_type == 30){
        var signature_date =  $.trim($('.DraftattachmentsRecord.active .tab-pane.csCover:last').find("#sender_signature_date").text());
        var sending_date  = $.trim($('.DraftattachmentsRecord.active .tab-pane.csCover:last').find("#sending_date").text());
    }
    else{
        var signature_date =  $.trim($('.DraftattachmentsRecord.active #template-body:last').find("#sender_signature_date").text());
        var sending_date  = $.trim($('.DraftattachmentsRecord.active #template-body:last').find("#sending_date").text());
    }
    if(isEmpty(signature_date)){
         signature_date =  $.trim($('.DraftattachmentsRecord.active #template-body:last').find("#sender_signature2_date").text());
    }
    if(isEmpty(signature_date) || isEmpty(sending_date)){
        return;
    }
    var pad = '০০';
    var signature_date_array = signature_date.split('-');
    var signature_date_day = pad.substring('০', 2 - signature_date_array[0].length) + signature_date_array[0];
    var signature_date_month = pad.substring('০', 2 - signature_date_array[1].length) + signature_date_array[1];
    var signature_date_year = signature_date_array[2];

    if(language == 'eng'){
        if (sending_date.split('/').length == 3) {
            var sending_date_array = sending_date.split('/');
        } else {
            var sending_date_array = sending_date.split('-');
        }

        var sending_date_day = sending_date_array[0];
        var sending_date_month = sending_date_array[1];
        var sending_date_year = sending_date_array[2];

        // var sending_date_len = sending_date.length;
        // var sending_date_day =($.isNumeric(sending_date[1])) ?sending_date.substring(0, 2):sending_date.substring(0, 1);
        // var sending_date_year = sending_date.substring(sending_date_len - 4, sending_date_len);
        // var sending_date_month = ($.isNumeric(sending_date[1])) ?sending_date.substring(3, 5):sending_date.substring(2, 4);
    }else{
        var sending_date_len = sending_date.length;
        var sending_date_day = sending_date.substring(0, 2);
        var sending_date_year = sending_date.substring(sending_date_len - 4, sending_date_len);
        var sending_date_month_text = sending_date.substring(3, sending_date_len - 5);

        var bMonth = {"১":"জানুয়ারি", "২":"ফেব্রুয়ারি", "৩":"মার্চ", "৪":"এপ্রিল", "৫":"মে", "৬":"জুন", "৭":"জুলাই", "৮":"আগস্ট", "৯":"সেপ্টেম্বর", "১০":"অক্টোবর", "১১":"নভেম্বর", "১২":"ডিসেম্বর"};
        var sending_date_month = jQuery.map( bMonth, function( n, i ) {
            if(sending_date_month_text==n) return pad.substring('০', 2 - i.length) + i;
        });
    }
    var sending_date_real = sending_date_year + '-' + sending_date_month+ '-' +sending_date_day;
    var signature_date_real = signature_date_year+ '-' + signature_date_month+ '-' + signature_date_day;
    var timestamp1=Date.parse(EngFromBn(sending_date_real));
    var timestamp2=Date.parse(EngFromBn(signature_date_real));
    if (isNaN(timestamp1)==false && isNaN(timestamp2) == false)
    {
         var date1=new Date(EngFromBn(sending_date_real));
         var date2=new Date(EngFromBn(signature_date_real));
         if(date1.getYear()+date1.getMonth()+date1.getDate() != date2.getYear()+date2.getMonth()+date2.getDate()){
             //console.log(date1,date2);
             $('.dateChange').show();
             //$("#potroKhosraEditUrl").attr('href',url);
         }
    }
}
function verifySoftTokenForOnucched(){
    var token = $("#soft_token").val();
     PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'nothiNoteSheets/verifySoftTokenForOnucched', {'token':token,part_no:part_no}, 'json', function (response) {
         
    });
}