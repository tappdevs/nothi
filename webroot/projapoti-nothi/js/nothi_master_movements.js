var NothiMasterMovement = function () {

	var nothimasterid = 0;
	var content = $('.inbox-content');
	var loading = $('.inbox-loading');


	var loadInbox = function (el, name) {

		var title = $('.nav-tabs > li.' + name + ' a').attr('data-title');
//        var listviewtype = $('.view_nothi_list').val();
		var url = js_wb_root + "nothiMasters/inboxContent/" + name;
//        if(listviewtype=='folder'){
//            var url = js_wb_root + "nothiMasters/folderContent/" + name;
//        }else{
//            var url = js_wb_root + "nothiMasters/inboxContent/" + name;
//        }

		loading.show();
		content.html('');
		toggleButton(el);

		$.ajax({
			type: "POST",
			cache: true,
			url: url,
			dataType: "html",
			success: function (res) {
				toggleButton(el);

				$('.inbox-nav > li.active').removeClass('active');
				$('.inbox-nav > li.' + name).addClass('active');

				$('.inbox-header > h1').text(title);

				loading.hide();
				content.html(res);
				if (Layout.fixContentHeight) {
					Layout.fixContentHeight();
				}
				Metronic.initUniform();
				$('.date-picker').datepicker({
					rtl: Metronic.isRTL(),
					orientation: "left",
					autoclose: true
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
				toggleButton(el);
			},
			async: false
		});
	};

	var forwardNothi = function (el) {

		var to_office_id = el.attr('to_office_id');
		var to_office_unit_id = el.attr('to_unit_id');
		var to_office_unit_name = el.attr('to_unit_name');
		var to_officer_id = el.attr('to_officer_id');
		var to_officer_name = el.attr('to_officer_name');
		var to_officer_designation_id = el.attr('to_org_id');
		var to_officer_designation_label = el.attr('to_org_name');
		var nothi_office = el.attr('nothi_office');
		var incharge_label = el.attr('incharge-label');
		var priority = $("#priority :selected").val();
		var soft_token = $("#soft_token").val();


		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right"
		};
		if (typeof (to_officer_designation_label) === 'undefined' || to_officer_designation_label <= 0) {
			$('.sendNothi').removeAttr('disabled');
			toastr.error('প্রাপক বাছাই করা হয়নি');
			return;
		}
		Metronic.blockUI({
			target: '.page-container',
			boxed: true
		});

		$.ajax({
			url: ((!isEmpty(signature) && signature > 0)?ds_url:js_wb_root) + 'NothiMasterMovements/forwardNothiMaster',
			data: {
				nothi_office: nothi_office,
				to_office_id: to_office_id,
				to_office_unit_name: to_office_unit_name,
				to_officer_name: to_officer_name,
				to_officer_designation_label: to_officer_designation_label,
				nothi_master_id: nothimasterid,
				to_office_unit_id: to_office_unit_id,
				to_officer_id: to_officer_id,
				to_officer_designation_id: to_officer_designation_id,
				to_officer_incharge_label: incharge_label,
				'priority': priority,
				'soft_token': soft_token
			},
			dataType: "JSON",
			method: "POST",
			cache: true,
			success: function (response) {
				if (response.status == 'error') {
					toastr.error(response.msg);
					$('.sendNothi').removeAttr('disabled');
					$('.saveNothi').removeAttr('disabled');
					$('.sendSaveNothi').removeAttr('disabled');
					Metronic.unblockUI('.page-container');
					Metronic.unblockUI('#ajax-content');
				} else {
					toastr.success(response.msg);
                    if(typeof(deleteAllOfflineDraftOfNote) == 'function'){
                        deleteAllOfflineDraftOfNote(nothimasterid);
                    }
//                    window.location.reload();
					//ajax to test next pending
					$.ajax({
						url: js_wb_root + 'NothiMasterMovements/nextPendingNoteSequentially',
						dataType: "JSON",
						method: "POST",
						cache: true,
						success: function (response) {
							if (response.status == 'success') {
								window.location.href = js_wb_root + 'noteDetail/' + response.note['nothi_part_no'] + '/' + response.note['nothi_office'];
							} else {
								toastr.error(response.msg);
								setTimeout(function () {
									window.location.href = js_wb_root + 'nothiMasters/index';
								}, 1000);

							}
						}
					})
					//end
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			}
		});
	};

	var archiveNothi = function () {

		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right"
		};

		if ($('.checkarchive:checked').length == 0) {
			$('#archiveModal').modal('toggle');
			toastr.error('কোন নথি বাছাই করা হয় নি।');
			return;
		}


		var selectednothi = [];

		$.each($('.checkarchive:checked'), function (i, v) {
			selectednothi.push($(this).data('nothimasterid'));
		});

		$.ajax({
			url: js_wb_root + 'NothiMasterMovements/archiveNothiMaster',
			data: {selectednothi: selectednothi},
			dataType: "JSON",
			method: "POST",
			cache: true,
			success: function (response) {
				$('#archiveModal').modal('toggle');
				if (response.status == 'error') {
					toastr.error(response.msg);
				} else {
					toastr.success(response.msg);
					loadInbox($(this), "inbox");
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			}
		});
	}
	// var showPartNothis = function (id,nothipartno,type,search_text) {
	var showPartNothis = function (id, nothipartno, type) {

		$('.details-modal-inbox-content').html('');
		Metronic.blockUI({
			target: '#detailsModal>.modal-dialog>.modal-content>.modal-body'
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right"
		};

		if ($('.checkdetails').length == 0) {
			$('#detailsModal').modal('toggle');
			toastr.error('কোন নথি বাছাই করা হয় নি।');
			return;
		}

		$('#detailsModal').find('.newPartCreate').attr('nothi_masters_id', id);
		$('#detailsModal').find('.newPartCreate').attr('nothi_parts_id', nothipartno);
		$('#detailsModal').find('.btn-sokol').attr('href', js_wb_root + 'nothiDetail/' + nothipartno + '/');

		$.ajax({
			url: js_wb_root + 'apiParts/' + type,
			data: {selected_part: id},
			// data: {selected_part:id,search:search_text},
			dataType: "HTML",
			method: "GET",
			cache: true,
			success: function (response) {
				Metronic.unblockUI(
                    '#detailsModal>.modal-dialog>.modal-content>.modal-body'
				);
				$('.details-modal-inbox-content').html(response);
			},
			error: function (xhr, ajaxOptions, thrownError) {

			}
		});
	}
	var toggleButton = function (el) {
		if (typeof el == 'undefined') {
			return;
		}
		if (el.attr("disabled")) {
			el.attr("disabled", false);
		} else {
			el.attr("disabled", true);
		}
	};

	return {
		//main function to initiate the module
		init: function (page) {
			if (page == 'nothing') {

			} else if (page == 'inbox') {
				loadInbox($(this), "inbox");
			} else if (page == 'sent') {
				loadInbox($(this), "sent");
			} else if (page == 'all') {
				loadInbox($(this), "all");
			} else if (page == 'office') {
				loadInbox($(this), "office");
			} else if (page == 'other') {
				loadInbox($(this), "other");
			} else if (page == 'other_sent') {
				loadInbox($(this), "other_sent");
			} else if (page == 'archiveNothi') {
				loadInbox($(this), "archiveNothi");
			} else {
				loadInbox($(this), "inbox");
			}

			$('.inbox-content').on('click', '.nav-tabs > li.inbox > a', function (e) {
				e.preventDefault();
				loadInbox($(this), "inbox");
			});
			$('.inbox-content').on('click', '.nav-tabs > li.sent > a', function (e) {
				e.preventDefault();
				loadInbox($(this), "sent");
			});
			$('.inbox-content').on('click', '.nav-tabs > li.all > a', function (e) {
				e.preventDefault();
				loadInbox($(this), "all");
			});
			$('.inbox-content').on('click', '.nav-tabs > li.other_office > ul > li.other', function (e) {
				e.preventDefault();
				loadInbox($(this), "other");
			});
			$('.inbox-content').on('click', '.nav-tabs > li.other_office > ul > li.other_sent', function (e) {
				e.preventDefault();
				loadInbox($(this), "other_sent");
			});
			$('.inbox-content').on('click', '.nav-tabs > li.office > a', function (e) {
				e.preventDefault();
				loadInbox($(this), "office");
			});
			$('.inbox-content').on('click', '.nav-tabs > li.archiveNothi > a', function (e) {
				e.preventDefault();
				loadInbox($(this), "archiveNothi");
			});

			function showSenderList(masterid, nothi_office) {
				$('.sendNothi').removeAttr('disabled');
				$('#responsiveNothiUsers').modal('show');
				$('.user_list').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
				nothimasterid = masterid;
				$('#responsiveNothiUsers').find('.nothimasterid').val(masterid);

				$.ajax({
					url: js_wb_root + 'nothiMasters/showForwaredUsers/' + masterid + '/' + nothi_office,
					success: function (response) {
						$('#responsiveNothiUsers').find('.user_list').html(response);
                        $(".user_list").find('[title]').tooltip({placement: 'bottom'});

						$('#responsiveNothiUsers').find('#user_tree').jstree({
							"core": {
								"themes": {
									"responsive": true
								}
							},
							"types": {
								"default": {
									"icon": "fa fa-user icon-state-primary icon-lg"
								},
								"file": {
									"icon": "fa fa-user icon-state-primary icon-lg"
								}
							},
							"plugins": ["types"]
						});
					}
				});
			}

			$(document).on('click', '.btn-forward-nothi', function () {
				var masterid = $(this).attr('nothi_master_id');
				var nothi_office = $(this).attr('nothi_office');

				if ($('.btn-send-draft-cron').length > 0 || $('.approveDraftNothi:not(:checked)').length > 0) {
					var warn_message = 'আপনার কাছে ';
					if ($('.btn-send-draft-cron').length > 0) {
						warn_message += BnFromEng($('.btn-send-draft-cron').length) + ' টি পত্র পত্রজারির ';
					}
					if ($('.approveDraftNothi:not(:checked)').length > 0) {
						if ($('.btn-send-draft-cron').length > 0) {
							warn_message += ' এবং ';
						}
						warn_message += BnFromEng($('.approveDraftNothi:not(:checked)').length) + ' টি পত্র অনুমোদনের ';
					}
					warn_message += 'জন্য অপেক্ষমাণ। ';
					bootbox.dialog({
						message: warn_message + "আপনি কি পরবর্তী কার্যক্রম না করে নথি পাঠাতে ইচ্ছুক?",
						title: "পরবর্তী কার্যক্রম ব্যতিত নথি প্রেরণ",
						buttons: {
							success: {
								label: "হ্যাঁ",
								className: "green",
								callback: function () {
									$('#responsiveNothiUsers').modal('hide');
									setTimeout(function () {
										showSenderList(masterid, nothi_office);
									}, 500);
								}
							},
							danger: {
								label: "না",
								className: "red",
								callback: function () {

								}
							}
						}
					});
				}
				else {
					showSenderList(masterid, nothi_office);
				}
			});
			$(document).on('click', '.btn-forward-list', function () {
				var masterid = $(this).attr('nothi_master_id');
				var nothi_office = $(this).attr('nothi_office');
				showSenderList(masterid, nothi_office);
			});

			$(document).off('click', '.btn-save-forward-nothi').on('click', '.btn-save-forward-nothi', function () {
				$('.sendSaveNothi').removeAttr('disabled');
				$('#responsiveSendNothi').modal('show');
				$('#sendsaveModal').val(1);
				var that = this;
				$('.user_list').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');
				var masterid = $(this).attr('nothi_master_id');
				var nothi_office = $(this).attr('nothi_office');
				nothimasterid = masterid;
				$('#responsiveSendNothi').find('.nothimasterid').val(masterid);

				$.ajax({
					url: js_wb_root + 'nothiMasters/showForwaredUsers/' + masterid + '/' + nothi_office + '/' + 1,
					success: function (response) {
						$('#responsiveSendNothi').find('.user_list').html(response);

						$('#responsiveSendNothi').find('#user_tree').jstree({
							"core": {
								"themes": {
									"responsive": true
								}
							},
							"types": {
								"default": {
									"icon": "fa fa-user icon-state-primary icon-lg"
								},
								"file": {
									"icon": "fa fa-user icon-state-primary icon-lg"
								}
							},
							"plugins": ["types"]
						});
					}
				});
			});

			$(document).off('click', '.sendNothi');
			$(document).on('click', '.sendNothi', function () {
				$('.sendNothi').attr('disabled', 'disabled');
				var el = $('.jstree-anchor.jstree-clicked');
				var officer_name = el.attr('to_officer_name');
				var officer_designation_label = el.attr('to_org_name');
				if (officer_name == '' || officer_name == null || typeof(officer_name) == 'undefined') {
					$('.sendNothi').removeAttr('disabled');
					toastr.error(' দুঃখিত কোন কর্মকর্তা বাছায় করা হয়নি। ');
					return;
				}
				if (officer_designation_label == '' || officer_designation_label == null || typeof(officer_designation_label) == 'undefined') {
					$('.sendNothi').removeAttr('disabled');
					toastr.error(' দুঃখিত কর্মকর্তার পদবি পাওয়া যায়নি। ');
					return;
				}
                if (signature > 0) {
                    Metronic.blockUI({
                        target: '#ajax-content',
                        boxed: true,
                        message: 'অপেক্ষা করুন'
                    });
                    $('#responsiveNothiUsers').modal('hide');
                    setTimeout(function () {
                        if (signature > 0) {
                            getSignatureToken('forwardNothi', el);
                        }
                    }, 500);
                    return;
                }
				bootbox.dialog({
					message: "আপনি কি নথিটি <b>জনাব " + officer_name + ", " + officer_designation_label + "</b> কে প্রেরণ করতে চান?",
					title: "নথি প্রেরণ",
					buttons: {
						success: {
							label: "হ্যাঁ",
							className: "green",
							callback: function () {
								Metronic.blockUI({
									target: '#ajax-content',
									boxed: true,
									message: 'অপেক্ষা করুন'
								});
								$('#responsiveNothiUsers').modal('hide');
								setTimeout(function () {
									if (signature > 0) {
										getSignatureToken('forwardNothi', el);
									} else {
										forwardNothi(el);
									}
								}, 500);

							}
						},
						danger: {
							label: "না",
							className: "red",
							callback: function () {
								$('.sendNothi').removeAttr('disabled');
							}
						}
					}
				});

			});


			$(document).off('click', '.sendSaveNothi');
			$(document).on('click', '.sendSaveNothi', function () {
				$('.sendSaveNothi').attr('disabled', 'disabled')
				var el = $('.jstree-anchor.jstree-clicked');
				var officer_name = el.attr('to_officer_name');
				var officer_designation_label = el.attr('to_org_name');
				if (officer_name == '' || officer_name == null || typeof(officer_name) == 'undefined') {
					$('.sendSaveNothi').removeAttr('disabled')
					toastr.error(' দুঃখিত কোন কর্মকর্তা বাছায় করা হয়নি। ');
					return;
				}
				if (officer_designation_label == '' || officer_designation_label == null || typeof(officer_designation_label) == 'undefined') {
					$('.sendSaveNothi').removeAttr('disabled')
					toastr.error(' দুঃখিত কর্মকর্তার পদবি পাওয়া যায়নি। ');
					return;
				}
				if(signature > 0){
                    Metronic.blockUI({
                        target: '#ajax-content',
                        boxed: true,
                        message: 'অপেক্ষা করুন'
                    });
                    $('#responsiveNothiUsers').modal('hide');
                    setTimeout(function () {
                        $('#sendsaveModal').val(1);
                        $('.saveNote').removeAttr('disabled');
                        $('.saveNote').eq(0).trigger('click');
                        $('.saveNothi').attr('disabled', 'disabled');
                        $('.sendSaveNothi').attr('disabled', 'disabled');
                        $('#responsiveSendNothi').modal('hide');
                        Metronic.unblockUI('#ajax-content');
                    }, 500);
                    return;
				}
				bootbox.dialog({
					message: "আপনি কি নথিটি <b>জনাব " + officer_name + ", " + officer_designation_label + "</b> কে প্রেরণ করতে চান?",
					title: "নথি প্রেরণ",
					buttons: {
						success: {
							label: "হ্যাঁ",
							className: "green",
							callback: function () {
								Metronic.blockUI({
									target: '#ajax-content',
									boxed: true,
									message: 'অপেক্ষা করুন'
								});
								$('#responsiveNothiUsers').modal('hide');
								setTimeout(function () {
									$('#sendsaveModal').val(1);
                                    $('.saveNote').removeAttr('disabled');
                                    $('.saveNote').eq(0).trigger('click');
                                    $('.saveNothi').attr('disabled', 'disabled');
                                    $('.sendSaveNothi').attr('disabled', 'disabled');
                                    $('#responsiveSendNothi').modal('hide');
                                    Metronic.unblockUI('#ajax-content');
								}, 500);
							}
						},
						danger: {
							label: "না",
							className: "red",
							callback: function () {
								$('.sendSaveNothi').removeAttr('disabled')

							}
						}
					}
				});

			});

			$(document).off('click', '.saveNothi');
			$(document).on('click', '.saveNothi', function () {
				$('#sendsaveModal').val(0);
				$('.saveNothi').attr('disabled', 'disabled');
				$('.sendSaveNothi').attr('disabled', 'disabled');
				$('.saveNote').eq(0).trigger('click');

			});

			$(document).on('click', '#goforArchive', function () {
				archiveNothi();
			});

			$(document).on('click', '.movetoArchive', function () {
				$("#archiveModal").modal();

			});

			$(document).on('click', '.checkdetails', function () {
				// $('#sub_search').val('');
				$('#detailsModal').modal();
				// $('#data-nothimasterid').val($(this).data('nothimasterid'));
				// $('#data-type').val($(this).data('type'));
				showPartNothis($(this).data('nothimasterid'), $(this).data('nothipartno'), $(this).data('type'));
			});
			// $(document).on('click', '#filter_submit_btn1', function () {
			//     var search_text = $('#sub_search').val();
			//     showPartNothis($('#data-nothimasterid').val(),$('#data-type').val(),search_text);
			// });

			$(document).on('change', '.view_nothi_list', function () {
				loadInbox($(this), $('.inbox-nav > li.inbox > a').data('title'));
			});
		},
		sendNothi: function () {
			if (signature > 0) {
				getSignatureToken('forwardNothi', $('.jstree-anchor.jstree-clicked'));
			} else {
				forwardNothi($('.jstree-anchor.jstree-clicked'));
			}
		},
		forwardNothiFunction: function (el) {
			forwardNothi(el);
		}
	}
}();

function getSignatureToken(functionName, el1, el2, el3) {
	if (signature > 0) {
		var promise = function () {
            return new Promise(function (resolve, reject) {
                if (signature == 1) {
                	PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'employeeRecords/checkUserSoftToken',{},'json',function(result){
                		if(!isEmpty(result) && !isEmpty(result.status)){
                			if(result.status == 'success'){
                                var str = '<div class="col-md-12 row"><div class="col-md-2 col-sm-2"><label class="control-label pull-right">সফট টোকেনঃ </label></div><div class="col-md-8 col-sm-8"><input type="password" class="form-control" id="soft_token" readonly></div></div><div class="col-md-12 row text-center font-green">'+(!isEmpty(result.message)?(result.message):'') +'</div>';
                                setTimeout(function(){
                                		$("#soft_token").val((!isEmpty(result.sign_token)?result.sign_token:''));
									},1000);
                                resolve(str);
							}else{
                                var str = '<div class="col-md-12 row"><div class="col-md-2 col-sm-2"><label class="control-label pull-right">সফট টোকেনঃ </label></div><div class="col-md-8 col-sm-8"><input type="password" class="form-control" id="soft_token" ></div></div><div class="col-md-12 row">যদি সফট টোকেন দিয়ে সাইন সম্ভব না হয় তবে <a href="' + js_wb_root + 'employee_records/myProfile">প্রোফাইল ব্যবস্থাপনা থেকে <b>স্বাক্ষর সেটিং</b> </a> হতে <b>স্বাক্ষর ধরন</b>  পরিবর্তন করে নিন। </div>';
                                resolve(str);
							}
						}else{
                            reject('Something went wrong.Code 1');
						}
					});

                } else {
                    var str = '<div class="col-md-12"><b>হার্ড টোকেন</b> নির্বাচন করায় প্রতি স্বাক্ষরের সময় আপনাকে ডিজিটাল <b>হার্ড টোকেন</b> এর সহযোগিতা নিতে হবে। যদি হার্ড টোকেন দিয়ে সাইন সম্ভব না হয় তবে <a href="' + js_wb_root + 'employee_records/myProfile">প্রোফাইল ব্যবস্থাপনা থেকে <b>স্বাক্ষর সেটিং</b> </a> হতে <b>স্বাক্ষর ধরন</b>  পরিবর্তন করে নিন। নিরাপত্তা নিশ্চিতকরণের জন্য দুইবার টোকেন চাওয়া হতে পারে। <input type="hidden" class="form-control" id="soft_token">  </div>';
                    resolve(str);
                }
            });
        };
        promise().then(function (str) {
            $("#soft_token").focus();
            bootbox.dialog({
                message: str,
                title: 'ডিজিটাল সিগনেচার',
                buttons: {
                    success: {
                        label: "ডিজিটাল সাইন ও " + ((functionName == 'forwardNothi') ? 'প্রেরণ' : (functionName == 'potroApproval') ? 'খসড়া অনুমোদন' : (functionName == 'makePotrojariRequest') ? 'পত্রজারি' : (functionName == 'goForDraftSave') ? 'খসড়া তৈরি' : (functionName == 'NoteNisponno') ? ' নোট নিষ্পন্ন' : ''),
                        className: "green",
                        callback: function () {
                            if (functionName == 'forwardNothi') {
                                NothiMasterMovement.forwardNothiFunction(el1);
                            }
                            else if (functionName == 'potroApproval') {
                                potroApproval(el1, el2);
                            }
                            else if (functionName == 'makePotrojariRequest') {
                                makePotrojariRequest(el1, el2);
                            }
                            else if (functionName == 'goForDraftSave') {
                                DRAFT_FORM.goForDraftSave();
                            }
                            else if (functionName == 'NoteNisponno') {
                                NoteNisponno(el1);
                            }
                            else if (functionName == 'apiMakePotrojariRequest') {
                                apiMakePotrojariRequest(el1, el2, el3);
                            }
                        }
                    },
                    proceedWithOutSignature:{
                        label: "ডিজিটাল সাইন ব্যতীত " + ((functionName == 'forwardNothi') ? 'প্রেরণ' : (functionName == 'potroApproval') ? 'খসড়া অনুমোদন' : (functionName == 'makePotrojariRequest') ? 'পত্রজারি' : (functionName == 'goForDraftSave') ? 'খসড়া তৈরি' : (functionName == 'NoteNisponno') ? ' নোট নিষ্পন্ন' : ''),
                        className: "blue",
                        callback: function () {
                            $("#soft_token").val('-1');
                            if (functionName == 'forwardNothi') {
                                NothiMasterMovement.forwardNothiFunction(el1);
                            }
                            else if (functionName == 'potroApproval') {
                                potroApproval(el1, el2);
                            }
                            else if (functionName == 'makePotrojariRequest') {
                                makePotrojariRequest(el1, el2);
                            }
                            else if (functionName == 'goForDraftSave') {
                                DRAFT_FORM.goForDraftSave();
                            }
                            else if (functionName == 'NoteNisponno') {
                                NoteNisponno(el1);
                            }
                            else if (functionName == 'apiMakePotrojariRequest') {
                                apiMakePotrojariRequest(el1, el2, el3);
                            }
                        }
                    },
                    danger: {
                        label: "বন্ধ করুন",
                        className: "red",
                        callback: function () {
                            if (functionName == 'potroApproval') {
                                if ($(".approveDraftNothi").is(':checked') == false) {
                                    $(".approveDraftNothi").attr('checked', 'checked');
                                    $(".approveDraftNothi").closest('span').addClass('checked');
                                }
                                else {
                                    $(".approveDraftNothi").removeAttr('checked');
                                    $(".approveDraftNothi").closest('span').removeClass('checked');
                                }
                            }
                            Metronic.unblockUI('.page-container');
                            Metronic.unblockUI('#ajax-content');
                        }
                    },

                }
            });
            return;
        }).catch (function (error) {
            console.log('Error: ', error);
        });

	} else {
		toastr.error('দুঃখিতঃ কোন ফাংশন নির্বাচন করা হয়নি।');
	}
}

function searchNoteOnnucched(val) {

	if (parseInt(val) > 0) {
		window.location.href = js_wb_root + 'noteDetail' + '/' + val;
	}
}

function findObjectByKey(array, key, value) {
	var return_array = [];
	for (var i = 0; i < array.length; i++) {
		var keyArr = key.split('.');
		if (keyArr.length == 1) {
			if (array[i][key] == value) {
				return_array.push(array[i]);
			}
		} else if (keyArr.length == 2) {
			var array_value = array[i][keyArr[0]][keyArr[1]].toLowerCase();
			if (JSON.stringify(array_value).indexOf(value.toLowerCase()) >= 0) {
				return_array.push(array[i]);
			}
		} else if (keyArr.length == 3) {
			var array_value = array[i][keyArr[0]][keyArr[1]][keyArr[2]].toLowerCase();
			if (JSON.stringify(array_value).indexOf(value.toLowerCase()) >= 0) {
				return_array.push(array[i]);
			}
		}
	}
	return return_array;
}
