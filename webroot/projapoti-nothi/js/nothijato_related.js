$(document).off('click', '.btn-rollback-to-dak').on('click', '.btn-rollback-to-dak', function () {
    bootbox.dialog({
        message: "আপনি কি নথিজাত ডাকটি পুনরায় নিজ ডেস্কে ফেরত আনতে ইচ্ছুক  ?",
        title: "নথিজাত ডাক ফেরত",
        buttons: {
            success: {
                label: "হ্যাঁ",
                className: "green",
                callback: function () {
                    Metronic.blockUI({
                        target: '#ajax-content',
                        boxed: true,
                        message: 'অপেক্ষা করুন'
                    });
                    setTimeout(nothijatBack,500);
                }
            },
            danger: {
                label: "না",
                className: "red",
                callback: function () {
                }
            }
        }
    });

});
function nothijatBack() {
    var dak_id = $("#dak_id").val();
    var dak_type = $("#dak_type").val();
    if(dak_id == '' || typeof(dak_id) == 'undefined' || dak_id == null ){
        toastr.error(' ডাক বাছাই করা হয়নি। ');
        return;
    }
    if(dak_type == '' || typeof(dak_type) == 'undefined' || dak_type == null ){
         toastr.error(' ডাকের ধরন খুঁজে পাওয়া যায়নি। ');
         return;
    }
    PROJAPOTI.ajaxSubmitDataCallback(js_wb_root + 'dakDaptoriks/rollbackFromNothijatToDak',
            {'dak_id': dak_id, 'dak_type': dak_type}, 'json', function (response) {
        if (response.status == 'success') {
            toastr.success(response.msg);
            window.location.href = js_wb_root +'dashboard/dashboard';
        } else {
            toastr.error(response.msg);
        }
        Metronic.unblockUI('#ajax-content');
    });
}
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
    