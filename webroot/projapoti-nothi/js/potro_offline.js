
function savePotroOffline() {
    if(isEmpty($('#potro-type').val())){
        return;
    }
    var data = {};
    if ($('#potrojariDraftForm').find('.fr-toolbar').length > 0) {
        data.body = $.trim($('#note').froalaEditor('html.get'));
    }
    else{
        data.body = $.trim($("#noteView").html());
    }
    data.potro_type = $('#potro-type').val();
    data.potro_priority = $('#potro-priority-level option:selected').val();
    data.potro_security = $('#potro-security-level option:selected').val();
    data.potro_language = isEmpty(potrojari_language)?'bn':potrojari_language;
    var prefixs = ['sender_','attension_','approval_','sovapoti_','onulipi_','receiver_'];
    for(var i in prefixs ){
        var prefix = prefixs[i];
        if($('.' + prefix + 'list').find('.sidebar-tags li').length > 0){
            data[prefix] = [];
            $('.' + prefix + 'list').find('.sidebar-tags li').each(function () {
                data[prefix].push($.trim($(this).html()));
                // if(prefix == 'receiver_'){
                //    console.log('save',$.trim($(this).html()));
                // }
            }) ;
        }
    }
    data.attachedment_outline = $.trim($('#fileuploadpotrojari').find('.template-download').closest('.files').html());
    data.multi_select = $('#multiselect_right').val();
    if($('#prapto-potro').val() == 21 || $('#prapto-potro').val() == 27){

        data.attachedment_crorpotro = $.trim($('#fileuploadpotrojari_crorpotro').find('.template-download').closest('.files').html());
    }

    data.subject = $.trim($('#potrojariDraftForm').find('#subject').text());

    if ($('#potro-type').val() == 13) {
        data.subject = $.trim($('#potrojariDraftForm').find('#blank-subject').val());
    }

    // data.sender_sarok_no = $.trim($('#potrojariDraftForm').find('#sharok_no').text());
    //
    // if ($('#potro-type').val() == 13) {
    //     data.sender_sarok_no = $.trim($('#potrojariDraftForm').find('#blank-sarok').val());
    // }
    var draft_name = offline_potro_draft_key+'_'+data.potro_type+'_'+data.potro_language;
    var db = ProjapotiOffline.createDatabase('offline_potro_draft');
    data._id = draft_name;
    db.get(draft_name).then(function (doc) {
        data._rev = doc._rev;
        db.put(data).then(function () {
            db.compact();
            if(typeof(setLastDraftInfo) == 'function'){
                setLastDraftInfo(offline_potro_draft_key);
            }
        }).catch(function (err) {
            // console.log(1,err);
        });
    }).catch(function (err) {
        db.put(data).catch(function (err) {
            // console.log(2,err);
        });
    });

}
function getPotroOffline(potro_type,potrojari_language){
    if(isEmpty(potro_type) || isEmpty(potrojari_language)){
        return;
    }
    if(parseInt(potro_type) == 19 ||parseInt(potro_type) == 21 || parseInt(potro_type) == 27){
        return;
    }
    var tmp_body = $('#template-body').data();
    if(tmp_body["blockUI.isBlocked"] == 1){
        return;
    }
    var draft_name = offline_potro_draft_key+'_'+potro_type+'_'+potrojari_language;
    var db = ProjapotiOffline.createDatabase('offline_potro_draft');
    db.get(draft_name).then(function (data) {
        if(!isEmpty(data.body)){
            $("#noteView").html(data.body);
        }
        if(!isEmpty(data.potro_priority)){
            $('#potro-priority-level').val(data.potro_priority).trigger('change');
        }
        if(!isEmpty(data.potro_security)){
            $('#potro-security-level').val(data.potro_security).trigger('change');
        }
        // $('#fileuploadpotrojari').find('table .files').html('');
        // if(!isEmpty(data.attachedment_outline)){
        //     $('#fileuploadpotrojari').find('table .files').html(data.attachedment_outline);
        // }
        // if(!isEmpty(data.attachedment_crorpotro)){
        //     $('#fileuploadpotrojari_crorpotro').find('table .files').html(data.attachedment_crorpotro);
        // }
        // $("#multiselect_right>option").remove();
        // if(!isEmpty(data.multi_select) && data.multi_select.length > 0){
        //     $.each(data.multi_select, function(i, v) {
        //         var cloneOption = $("#multiselect_left>option[value="+v+"]").clone();
        //         $("#multiselect_right").append(cloneOption);
        //         $("#multiselect_left>option[value="+v+"]").hide();
        //         $("#multiselect_right>option").prop('selected', true);
        //     });
        // }
        if(!isEmpty(data.subject)){
            $('#potrojariDraftForm').find('#subject').text(data.subject);
            $('#potrojariDraftForm').find('#blank-subject').val(data.subject);
        }
        // if(!isEmpty(data.sender_sarok_no)){
        //     $('#potrojariDraftForm').find('#sharok_no').text(data.sender_sarok_no);
        //     $('#potrojariDraftForm').find('#blank-sarok').val(data.sender_sarok_no);
        // }
        var prefixs = ['sender_','attension_','approval_','sovapoti_','onulipi_','receiver_'];
        for(var indx in prefixs ){
            var prefix = prefixs[indx];
            // console.log(prefix,$('.' + prefix + 'list').find('.sidebar-tags').html());
            $('.' + prefix + 'list').find('.sidebar-tags').html('');
            // console.log(prefix,$('.' + prefix + 'list').find('.sidebar-tags').html());
            if(!isEmpty(data[prefix]) && data[prefix].length > 0){
                if(prefix == 'attension_'){
                    setTimeout(setAttentionFromOffline,2*1000);
                }
                var len = data[prefix].length;
                var html = '';
                for(var i = 0;i < len; i++ ){
                    html += '<li>'+data[prefix][i]+'</li>';
                }
                // if(prefix == 'receiver_'){
                //     console.log('get Offline',html);
                // }
                $('.' + prefix + 'list').find('.sidebar-tags').html(html);
            }
        }
        if(typeof(setInformation) == 'function'){
            setTimeout(setInformation,5*1000);
        }

        if(typeof(savePotroOffline) == 'function'){
            setTimeout(setInterval(savePotroOffline,30 * 1000),60 * 1000);
        }
    }).catch(function (err) {
        // console.log(3,err);
        if(typeof(savePotroOffline) == 'function'){
            setTimeout(setInterval(savePotroOffline,30 * 1000),60 * 1000);
        }

    });
}
function deleteOfflineDraft(potro_type,potrojari_language){
    var draft_name = offline_potro_draft_key+'_'+potro_type+'_'+potrojari_language;
    var db = ProjapotiOffline.createDatabase('offline_potro_draft');
    db.get(draft_name).then(function (data) {
        db.remove(data);
    }).catch(function (err) {
        // console.log(5,err);
    });
}
function deleteAllOfflineDraftOfNote(part_no){
    if(isEmpty(part_no)){
        return;
    }
    var db = ProjapotiOffline.createDatabase('offline_potro_draft');
    db.allDocs({
        include_docs: true,
    }).then(function (result) {
        if(!isEmpty(result.total_rows) && result.total_rows > 0){
            if(!isEmpty(result.rows) && result.rows.length > 0 ){
                var data = result.rows;
                for(var indx in data){
                    if(!isEmpty(data[indx].id)){
                        var str = data[indx].id;
                        var split_data = str.split('_');
                        if(!isEmpty(split_data[3]) && split_data[3] == part_no){
                            db.get(data[indx].id).then(function (data) {
                                db.remove(data);
                            }).catch(function (err) {
                                // console.log(7,err);
                            });
                        }
                    }
                }
            }
        }
    }).catch(function (err) {
        // console.log(6,err);
    });
}
function setAttentionFromOffline(){
    $.each($('.attension_users'), function (i, data) {
        $("#attention").remove();
        $('input[name=attension_office_id_final]').val($(this).attr('ofc_id'));
        $('input[name=attension_officer_id_final]').val($(this).attr('officer_id'));
        $('input[name=attension_office_unit_id_final]').val($(this).attr('unit_id'));
        $('input[name=attension_officer_designation_id_final]').val($(this).attr('designation_id'));
        $('input[name=attension_officer_designation_label_final]').val($(this).attr('designation'));
        $('input[name=attension_officer_name_final]').val($(this).attr('officername'));
        $('input[name=attension_office_name_final]').val($(this).attr('ofc_name'));
        $('input[name=attension_office_unit_name_final]').val($(this).attr('unit_name'));
        attensionAdd($(this).attr('officer_id'), $(this).attr('officername'), $(this).attr('designation_id'), $(this).attr('designation'), $(this).attr('unit_id'), $(this).attr('unit_name'), $(this).attr('ofc_id'), $(this).attr('ofc_name'));
    });
}
function getLastDraftInfo(key){
    if(isEmpty(key)){
        return;
    }
    var db = ProjapotiOffline.createDatabase('offline_last_potro_draft');
    db.get(key).then(function (data) {
        if(!isEmpty(data.potro_language) && !isEmpty(data.potro_type)){
            $("#potro_language").val(data.potro_language);
            $("#potro-type").val(data.potro_type);
            $("#potro-type").trigger('change');
        }
    }).catch(function (err) {

    });
}
function setLastDraftInfo(key){
    if(isEmpty($('#potro-type').val())){
        return;
    }
    if(isEmpty(key)){
        return;
    }
    var db = ProjapotiOffline.createDatabase('offline_last_potro_draft');
    var data = {};
    data._id = key;
    data.potro_type = $('#potro-type').val();
    data.potro_language = isEmpty(potrojari_language)?'bn':potrojari_language;
    db.get(key).then(function (doc) {
        data._rev = doc._rev;
        db.put(data).then(function () {
            db.compact();
        }).catch(function (err) {
            // console.log(1,err);
        });
    }).catch(function (err) {
        db.put(data).catch(function (err) {
            // console.log(2,err);
        });
    });
}
