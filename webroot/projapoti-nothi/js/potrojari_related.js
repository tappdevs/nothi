var potrojari_language = 'bn';
jQuery(document).ready(function () {
//    $('.potro_language').show();
    $("#potrojari_language").bootstrapSwitch({
        state: ($("#potro_language").val() == 'eng') ? false : true,
    });
    $('#potrojari_language').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state == false) {
//            $("#template-body").attr('style','font-size: 10px!important;font-family: "Times New Roman" ');
        } else {
            $("#template-body").attr('style', 'font-size: 13pt!important;font-family: Nikosh');
        }

        potrojari_language = ($('#potrojari_language').bootstrapSwitch('state') == true) ? 'bn' : 'eng';
        $("#potro_language").val(potrojari_language);
        getTemplates();
        $("#potro-type").val(0);

//        setInformation();
    });
    if (isEmpty($("#potro_language").val())) {
        $("#potro_language").val(potrojari_language);
    } else {
        potrojari_language = $("#potro_language").val();
    }

});

function getTemplates() {
    Metronic.blockUI({
        target: '#template-body'
    });
    var promise = new Promise(function (resolve, reject) {
        var url = js_wb_root + 'PotrojariTemplates/getTemplatesByVersion/' + potrojari_language;
        PROJAPOTI.ajaxSubmitDataCallback(url, {}, 'json', function (data) {
            PROJAPOTI.projapoti_dropdown_map('#potro-type', data, 'পত্রের ধরন বাছাই করুন', 0);
            $("#potro-type").change();
        });
        resolve(1);
    }).then(function (res) {
        Metronic.unblockUI('#template-body');
        $('#template-body').html('');
        $("#potro-type").change();
    }).catch(function (err) {
        Metronic.unblockUI('#template-body');
        toastr.error(err);
        return false;
    });
}

function setInformation() {
    Metronic.blockUI({target: "#template-body", animate: true});
    $('.btn-nothiback').attr('disabled','disabled');

    var senders = $('.sender_users');
    var approvals = $('.approval_users');
    var receivers = $('.receiver_users');
    var onulipis = $('.onulipi_users');
    var sovapotis = $('.sovapoti_users');

    $('#sovapoti_signature').html("");
    $('#sovapoti_designation').html("");
    $('#sovapotiname').html("");
    $('#sovapotiname2').html("");
    $('#sovapoti_designation2').html("");
    $('#sender_signature').html("");
    $('#sender_signature2').html("");
    $('#sender_designation').html("");
    $('#sender_name').html("");
    $('#sender_designation2').html("");
    $('#sender_name2').html("");
    $('#sender_designation3').html("");
    $('#sender_name3').html("");
    $('#office_organogram_id').html("");
    $('#address').html("");
    $('#cc_list_div').html("");
    $('#office_organogram_id').html('');

    checkAttension();
    setSecrecyAndPriority();
    setSovapotiInfo(sovapotis);
    setSenderInfo(senders);
    setApprovalInfo(approvals);
    setReceiverInfo(receivers);
    setOnulipiInfo(onulipis);
    checkBitoronJoggo();
    reInitializeEditable();
    Metronic.unblockUI("#template-body");
    // if ($('[name=potro_type]').val() == 30) {
    //     $('#office_organogram_id').html("");
    //     $('#cc_list_div').html("");
    //     $('#getarthe').remove();
    // }
}
function setSecrecyAndPriority(){
        var DAK_PRIORITY_TYPE_ENG = {
        '0': ' ',
        '2': 'Emergency',
        '3': 'Immediately',
        '4': 'Highest Priority',
        '5': 'Instructions',
        "6": 'Seek Attention'
    };
    var DAK_SECRECY_TYPE_ENG = {
        '0': ' ',
        '2': 'Very Confidential',
        '3': 'Special Confidential',
        '4': 'Confidential',
        '5': 'Limited'
    };
    if (potrojari_language == 'eng') {
        $('#potro_priority').text(DAK_PRIORITY_TYPE_ENG[$('#potro-priority-level option:selected').val()]);
        $('#potro_security').text(DAK_SECRECY_TYPE_ENG[$('#potro-security-level option:selected').val()]);
        $('.potro_security').text(DAK_SECRECY_TYPE_ENG[$('#potro-security-level option:selected').val()]);
    } else {
        $('#potro_priority').text($('#potro-priority-level option:selected').text());
        $('#potro_security').text($('#potro-security-level option:selected').text());
        $('.potro_security').text($('#potro-security-level option:selected').text());
    }
}
function setSovapotiInfo(sovapotis){
    if ($('[name=potro_type]').val() == 17) {
        if (sovapotis.length != 0) {
            $.each($('.sovapoti_users'), function (i, data) {
                if (i == 0) {
                    var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
                    if (sendename.length > 0) {
                    } else {
                        sendename[0] = sendename;
                    }
                    if(!isEmpty($(this).attr('visibleName')) && $.trim($(this).attr('visibleName')) !='...'){
                        var visibleName = $.trim($(this).attr('visibleName'));
                        $('input[name=sovapoti_visibleName]').val(visibleName);
                    }else{
                        var visibleName  = $.trim(sendename[0]);
                    }
                    if(!isEmpty($(this).attr('visibleDesignation')) && $.trim($(this).attr('visibleDesignation')) !='...'){
                        var visibleDesignation = $.trim($(this).attr('visibleDesignation'));
                        $('input[name=sovapoti_visibleDesignation]').val(visibleDesignation);
                    }else{
                        var visibleDesignation  = $.trim($(this).attr('designation'));
                    }
                    $('#sender_designation').text(visibleDesignation);
                    $('#sender_name').text(visibleName);
                    $('#sovapoti_designation').text(visibleDesignation);
                    $('#sovapotiname').text($.trim(visibleName));
                    $('#sovapoti_designation2').text(visibleDesignation);
                    $('#sovapotiname2').text($.trim(visibleName));
                }
            });
        }
    }
}
function setOnulipiInfo(onulipis){
       if (onulipis.length == 0) {
        $('input[name=onulipi_group_id_final]').val('');
        $('input[name=onulipi_group_name_final]').val('');
        $('input[name=onulipi_group_member_final]').val(0);
        $('input[name=onulipi_group_designation_final]').val('');
        $('input[name=onulipi_office_id_final]').val('');
        $('input[name=onulipi_office_name_final]').val('');
        $('input[name=onulipi_officer_designation_id_final]').val('');
        $('input[name=onulipi_officer_designation_label_final]').val('');
        $('input[name=onulipi_office_unit_id_final]').val('');
        $('input[name=onulipi_office_unit_name_final]').val('');
        $('input[name=onulipi_officer_id_final]').val('');
        $('input[name=onulipi_officer_name_final]').val('');
        $('input[name=onulipi_officer_email_final]').val('');
        $('input[name=onulipi_office_head_final]').val('');
        $('input[name=onulipi_visibleName_final]').val('');
        $('input[name=onulipi_officer_mobile_final]').val('');
        $('input[name=onulipi_sms_message_final]').val('');
        $('#potrojariDraftForm').find('#cc_list_div').closest('.row').nextAll('.row').hide()
        $('#potrojariDraftForm').find('#cc_list_div').closest('.row').hide();
        $('#potrojariDraftForm').find('#sharok_no2').closest('.row').hide();
        $('#potrojariDraftForm').find('#sending_date_2').closest('.row').hide();
    } else {
        if ($('#potrojariDraftForm').find('#cc_list_div').length > 0 || $('[name=potro_type]').val() == 13) {

            var totalmember = $.map(onulipis,function(data){
                return parseInt($(data).attr('group_member'));
            }).reduce(function(total,num){return total+num; });

            var bn = replaceNumbers("'" + totalmember + "'");
            bn = bn.replace("'", '');
            bn = bn.replace("'", '');
            if (potrojari_language == 'eng') {
                $('#sharok_no2').text($('#sharok_no').text() + "/1" + (totalmember > 1 ? ("(" + totalmember + ")") : ""));
            } else {
                $('#sharok_no2').text($('#sharok_no').text() + "/" + replaceNumbers('1') + (totalmember > 1 ? ("(" + bn + ")") : ""));
            }

            $('#potrojariDraftForm').find('#cc_list_div').closest('.row').nextAll('.row').show()
            $('#potrojariDraftForm').find('#cc_list_div').closest('.row').show();
            $('#potrojariDraftForm').find('#sharok_no2').closest('.row').show();
            $('#potrojariDraftForm').find('#sending_date_2').closest('.row').show();

            $.each(onulipis, function (i, data) {
                var bn = replaceNumbers("'" + (i + 1) + "'");
                bn = bn.replace("'", '');
                bn = bn.replace("'", '');
                if (typeof ($(this).attr('group_id')) == 'undefined') {
                    if(!isEmpty($(this).attr('visibleName'))){
                        var visibleName = $(this).attr('visibleName');
                        $('input[name=onulipi_visibleName_final]').val(visibleName);
                    }else{
                        var visibleName = (($(this).attr('unit_id') == 0 && $(this).attr('officer_name').length > 0 ? ($(this).attr('officer_name') + ", ") : "" ) + $(this).attr('designation') + ($(this).attr('office_head') == 0 && ($(this).attr('unit_name').length > 0 && $(this).attr('ofc_name') != $(this).attr('unit_name')) ? (', ' + $(this).attr('unit_name')) : '') + ', ' + $(this).attr('ofc_name'));
                    }
                    if (potrojari_language == 'eng') {
                        $('#cc_list_div').append((i + 1) + ') <a data-type="text" id="cc_div_item" data-pk=1 href="javascript:;" class="editable editable-click cc_list " >' + visibleName + "</a><br/>");
                    } else {
                        $('#cc_list_div').append(bn + ') <a data-type="text" id="cc_div_item" data-pk=1 href="javascript:;" class="editable editable-click cc_list " >' + visibleName + "</a><br/>");
                    }

                } else {
                    if (potrojari_language == 'eng') {
                        $('#cc_list_div').append((i + 1) + ') <a data-type="text" id="cc_div_item" data-pk=1 href="javascript:;" class="editable editable-click cc_list " >' + $(this).attr('group_name') + "</a><br/>");
                    } else {
                        $('#cc_list_div').append(bn + ') <a data-type="text" id="cc_div_item" data-pk=1 href="javascript:;" class="editable editable-click cc_list " >' + $(this).attr('group_name') + "</a><br/>");
                    }

                }

            });
            $('#potrojariDraftForm').find('.cc_list').editable({
                name: "cc_list",
                type: 'text',
                display: function (value,d) {
                    var IndexChange = $('.cc_list').index(this);
                    if(IndexChange == 0 && typeof(d) == 'undefined' ){
                        return;
                    }
                    $(".onulipi_users").eq(IndexChange).attr('visibleName',value);
                    // console.log(IndexChange,d,typeof(d));
                    $(this).text(value);
                }
            });
        } else {
            $('input[name=onulipi_group_id_final]').val('');
            $('input[name=onulipi_group_name_final]').val('');
            $('input[name=onulipi_group_member_final]').val(0);
            $('input[name=onulipi_group_designation_final]').val('');
            $('input[name=onulipi_office_id_final]').val('');
            $('input[name=onulipi_office_name_final]').val('');
            $('input[name=onulipi_officer_designation_id_final]').val('');
            $('input[name=onulipi_officer_designation_label_final]').val('');
            $('input[name=onulipi_office_unit_id_final]').val('');
            $('input[name=onulipi_office_unit_name_final]').val('');
            $('input[name=onulipi_officer_id_final]').val('');
            $('input[name=onulipi_officer_name_final]').val('');
            $('input[name=onulipi_officer_email_final]').val('');
            $('input[name=onulipi_office_head_final]').val('');
            $('input[name=onulipi_visibleName_final]').val('');
            $('input[name=onulipi_officer_mobile_final]').val('');
            $('input[name=onulipi_sms_message_final]').val('');
            $('#potrojariDraftForm').find('#cc_list_div').closest('.row').nextAll('.row').hide()
            $('#potrojariDraftForm').find('#cc_list_div').closest('.row').hide();
            $('#potrojariDraftForm').find('#sharok_no2').closest('.row').hide();
            $('#potrojariDraftForm').find('#sending_date_2').closest('.row').hide();
            toastr.error("দুঃখিত! পত্রে অনুলিপি দেয়া যাবে না!");
            return false;
        }
    }
}
function setReceiverInfo(receivers){
    if (receivers.length == 0) {
        $('input[name=receiver_group_id_final]').val('');
        $('input[name=receiver_group_name_final]').val('');
        $('input[name=receiver_group_member_final]').val(0);
        $('input[name=receiver_group_designation_final]').val('');
        $('input[name=receiver_office_id_final]').val('');
        $('input[name=receiver_office_name_final]').val('');
        $('input[name=receiver_officer_designation_id_final]').val('');
        $('input[name=receiver_officer_designation_label_final]').val('');
        $('input[name=receiver_office_unit_id_final]').val('');
        $('input[name=receiver_office_unit_name_final]').val('');
        $('input[name=receiver_officer_id_final]').val('');
        $('input[name=receiver_officer_name_final]').val('');
        $('input[name=receiver_officer_email_final]').val('');
        $('input[name=receiver_office_head_final]').val('');
        $('input[name=receiver_visibleName_final]').val('');
        $('input[name=receiver_officer_mobile_final]').val('');
        $('input[name=receiver_sms_message_final]').val('');
    } else {

        if ($('#potrojariDraftForm').find('#office_organogram_id').length > 0) {
            if (receivers.length == 1) {
                $.each(receivers, function (i, data) {
                    if (typeof ($(this).attr('group_id')) == 'undefined') {
                        if(!isEmpty($(this).attr('visibleName'))){
                            var visibleName = $(this).attr('visibleName');
                            $('input[name=receiver_visibleName_final]').val(visibleName);
                        }else{
                            var visibleName = (($(this).attr('unit_id') == 0 && $(this).attr('officer_name').length > 0 ? ($(this).attr('officer_name') + ", ") : "" ) + $(this).attr('designation') + ($(this).attr('office_head') == 0 && ($(this).attr('unit_name').length > 0 && $(this).attr('unit_name') != '0') ? ('\n' + $(this).attr('unit_name') + '') : '') + '\n' + $(this).attr('ofc_name'));
                        }

                        $('#office_organogram_id').append('<a data-type="text" id="to_div_item" data-pk=1 href="javascript:;" class="editable editable-click to_list " >' +visibleName + "</a>");
                    } else {
                        $('#office_organogram_id').append('<a data-type="text" id="to_div_item" data-pk=1 href="javascript:;" class="editable editable-click to_list " >' + $(this).attr('group_name') + "</a>");
                    }
                });

            } else {
                $('#office_organogram_id').html("");
                $.each(receivers, function (i, data) {
                    var bn = replaceNumbers("'" + (i + 1) + "'");
                    bn = bn.replace("'", '');
                    bn = bn.replace("'", '');
                    if (typeof ($(this).attr('group_id')) == 'undefined') {
                        if(!isEmpty($(this).attr('visibleName'))){
                            var visibleName = $(this).attr('visibleName');
                            $('input[name=receiver_visibleName_final]').val(visibleName);
                        }else{
                            var visibleName = ($(this).attr('unit_id') == 0 && $(this).attr('officer_name').length > 0 ? ($(this).attr('officer_name') + ", ") : "") + $(this).attr('designation') + ($(this).attr('office_head') == 0 && ($(this).attr('unit_name').length > 0 && $(this).attr('ofc_name') != $(this).attr('unit_name')) ? (', ' + $(this).attr('unit_name')) : '') + ", " + $(this).attr('ofc_name');
                        }
                        if (potrojari_language == 'eng') {
                            $('#office_organogram_id').append((i + 1) + ') <a data-type="text" id="to_div_item" data-pk=1 href="javascript:;" class="editable editable-click to_list " >' +  visibleName + "</a><br/>");
                        } else {
                            $('#office_organogram_id').append(bn + ') <a data-type="text" id="to_div_item" data-pk=1 href="javascript:;" class="editable editable-click to_list " >' + visibleName + "</a><br/>");
                        }

                    } else {
                        if (potrojari_language == 'eng') {
                            $('#office_organogram_id').append((i + 1) + ') <a data-type="text" id="to_div_item" data-pk=1 href="javascript:;" class="editable editable-click to_list " >' + $(this).attr('group_name') + "</a><br/>");
                        } else {
                            $('#office_organogram_id').append(bn + ') <a data-type="text" id="to_div_item" data-pk=1 href="javascript:;" class="editable editable-click to_list " >' + $(this).attr('group_name') + "</a><br/>");
                        }
                    }
                });
            }
            $('#potrojariDraftForm').find('.to_list').editable({
                name: "to_list",
                type: 'text',
                display: function (value,d) {
                    var IndexChange = $('.to_list').index(this);
                    if(IndexChange == 0 && typeof(d) == 'undefined' ){
                        return;
                    }
                    $(".receiver_users").eq(IndexChange).attr('visibleName',value);
                    // console.log(IndexChange,d,typeof(d));
                    $(this).text(value);
                }
            });
        } else if ($('[name=potro_type]').val() == 13) {

        } else {
            $('input[name=receiver_group_id_final]').val('');
            $('input[name=receiver_group_name_final]').val('');
            $('input[name=receiver_group_member_final]').val(0);
            $('input[name=receiver_group_designation_final]').val('');
            $('input[name=receiver_office_id_final]').val('');
            $('input[name=receiver_office_name_final]').val('');
            $('input[name=receiver_officer_designation_id_final]').val('');
            $('input[name=receiver_officer_designation_label_final]').val('');
            $('input[name=receiver_office_unit_id_final]').val('');
            $('input[name=receiver_office_unit_name_final]').val('');
            $('input[name=receiver_officer_id_final]').val('');
            $('input[name=receiver_officer_name_final]').val('');
            $('input[name=receiver_officer_email_final]').val('');
            $('input[name=receiver_office_head_final]').val('');
            $('input[name=receiver_visibleName_final]').val('');
            $('input[name=receiver_officer_mobile_final]').val('');
            $('input[name=receiver_sms_message_final]').val('');
            toastr.error("দুঃখিত! পত্রে প্রাপক দেয়া যাবে না!");
            return false;
        }
    }
}
function setApprovalInfo(approvals){
    if (approvals.length == 0) {

    } else {

        $('#potrojariDraftForm').find('#sender_name2').closest('.row').show();
        $('#potrojariDraftForm').find('#sender_designation2').closest('.row').show();
        $('#potrojariDraftForm').find('#sender_signature2').closest('.row').show();

        $.each($('.approval_users'), function (i, data) {
            if (i > 0) {
                toastr.error("একাধিক পদবি দেয়া যাবে না। ");
                return false;
            }
            if (i > 0) {
                $('#sender_designation2').text(', ');
                $('#sender_name2').text(', ');
                $('#sender_designation3').text(', ');
                $('#sender_name3').text(', ');
            }

            var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
            if (sendename.length > 0) {
            } else {
                sendename[0] = sendename;
            }
             if(!isEmpty($(this).attr('visibleName')) && $.trim($(this).attr('visibleDesignation')) !='...'){
                var visibleName = $.trim($(this).attr('visibleName'));
                $('input[name=approval_visibleName]').val(visibleName);
            }else{
                var visibleName  = $.trim(sendename[0]);
            }
            if(!isEmpty($(this).attr('visibleDesignation')) && $.trim($(this).attr('visibleDesignation')) !='...'){
                var visibleDesignation = $.trim($(this).attr('visibleDesignation'));
                $('input[name=approval_visibleDesignation]').val(visibleDesignation);
            }else{
                var visibleDesignation  = $.trim($(this).attr('designation'));
            }
            $('#sender_designation2').text(visibleDesignation);
            $('#sender_name2').text(visibleName);
            $('#sender_designation3').text(visibleDesignation);
            $('#sender_name3').text(visibleName);

            if($('[name=potro_type]').val() == 17){

                $('#sender_designation').text(visibleDesignation);
                $('#sender_name').text(visibleName);
            }
        });
    }
}
function setSenderInfo(senders){
    if (senders.length != 0) {
         $('#sender_name').html('');
        $.each($('.sender_users'), function (i, data) {
            if (i > 0) {
                toastr.error("একাধিক পদবি দেয়া যাবে না। ");
                return false;
            }
            var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
            if(!isEmpty($(this).attr('visibleName')) && $.trim($(this).attr('visibleName')) !='...'){
                var visibleName = $.trim($(this).attr('visibleName'));
                $('input[name=sender_officer_visibleName]').val(visibleName);
            }else{
                var visibleName  = $.trim(sendename[0]);
            }
            if(!isEmpty($(this).attr('visibleDesignation')) && $.trim($(this).attr('visibleDesignation')) !='...'){
                var visibleDesignation = $.trim($(this).attr('visibleDesignation'));
                $('input[name=sender_officer_visibleDesignation]').val(visibleDesignation);
            }else{
                var visibleDesignation  = $.trim($(this).attr('designation'));
            }
            $('#sender_designation').text(visibleDesignation);
            $('#sender_name').text(visibleName);

            if($('[name=potro_type]').val() == 20){
                $('#ministry_name_noc').html("("+escapeHtml($(this).attr('unit_name'))+")");
                $('#sender_unit').html(escapeHtml($(this).attr('unit_name')));
                check_attachment();
            }
            //$('#unit_name_editable').text(($('#unit_name_editable').text().length == 0 || $('#unit_name_editable').text() == '...') ? $('.sender_users').eq(0).attr('unit_name') : $('#unit_name_editable').text());
        });
        if (!isEmpty($('.sender_users').eq(0).attr('officer_id'))) {
            var unitid = !isEmpty($('.sender_users').eq(0).attr('unit_id'))?parseInt($('.sender_users').eq(0).attr('unit_id')):0
            $.ajax({
                url: js_wb_root + 'EmployeeRecords/userDetails/' + $('.sender_users').eq(0).attr('officer_id') + '/' + unitid,
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    if (data.personal_email.length > 0) {
                        $('#sender_email').text($.trim(data.personal_email));
                    }
                    if (data.personal_mobile.length > 0) {
                        $('#sender_phone').text((potrojari_language == 'eng') ? EngFromBn(data.personal_mobile) : data.personal_mobile);
                    }
                    if (typeof(data.fax) != 'undefined') {
                        $('#sender_fax').text((potrojari_language == 'eng') ? EngFromBn(data.fax) : data.fax);
                    }
                }
            });
        }
    }
    
}
$('.savethis').off('click');
$('.savethis').on('click', function () {
    var attr = $(this).attr('refid');
    var prefix = $(this).attr('prefix');

    if (isEmpty($('#potro-type').val()) && isEmpty($('input[name=potro_type]').val())) {
        toastr.error("দুঃখিত! পত্রের ধরন বাছাই করা হয়নি");
        return false;
    }

    if ($("#potro-type").val() != 13) {
        if (prefix == 'onulipi_') {
            if ($('#potrojariDraftForm').find('#cc_list_div').length > 0) {
            } else {
                toastr.error("দুঃখিত! পত্রে অনুলিপি দেয়া যাবে না!");
                return false;
            }
        }
        if (prefix == 'receiver_') {
            if ($('#potrojariDraftForm').find('#office_organogram_id').length > 0) {
            }
            else {
                toastr.error("দুঃখিত! পত্রে প্রাপক দেয়া যাবে না!");
                return false;
            }
        }
    }
    // check multiple user add
    if(checkMultipleUserAdd(prefix) == false){
        toastr.error("দুঃখিত! একাধিক ব্যবহারকারী নির্বাচন করা যাবেনা।");
        return false;
    }
    // check multiple user add
    if (attr == 'tab_autosearch') {
        var officerid = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_id]').val();
        var officername = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_name]').val();
        var officerhead = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_head]').val();
        var officerofcid = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_id_auto]').val();
        var officerofc = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_name_auto]').val();
        var officerorgid = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_designation_id_auto]').val();
        var officerorg = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_designation_label_auto]').val();
        var officerunitid = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_unit_id_auto]').val();
        var officerunit = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_unit_name_auto]').val();

        var hasEnglish = searchForEnglish($(this), prefix);
        if (!isEmpty(hasEnglish['officername'])) {
            officername = hasEnglish['officername'];
        }
        if (!isEmpty(hasEnglish['officerofc'])) {
            officerofc = hasEnglish['officerofc'];
        }
        if (!isEmpty(hasEnglish['officerunit'])) {
            officerunit = hasEnglish['officerunit'];
        }
        if (!isEmpty(hasEnglish['officerorg'])) {
            officerorg = hasEnglish['officerorg'];
        }
        if (officerofcid.length == 0 || officerid.length == 0 || officerorgid.length == 0) {
            toastr.error("দুঃখিত! তথ্য সঠিক নয়");
            return false;
        }else if (officerid > 0 && (isEmpty(officerunitid) || isEmpty(officerunitid) || isEmpty(officerorgid))) {
            toastr.error("দুঃখিত! তথ্য সঠিক নয়");
            return false;
        } else {
            if ($(document).find('a.receiver_users[designation_id=' + officerorgid + ']').length >= 1 && prefix != 'attension_') {
                toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                return false;
            } else if ($(document).find('a.onulipi_users[designation_id=' + officerorgid + ']').length >= 1 && prefix != 'attension_') {
                toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                return false;
            }
            if(prefix == 'attension_'){
               attensionAdd(officerid,officername,officerorgid,officerorg,officerunitid,officerunit,officerofcid,officerofc);
            }
            $('.' + prefix + 'list').find('.sidebar-tags').append('<li><a class="' + prefix + 'users"  href="javascript:;" data-toggle="tooltip" title="' + officerorg + '" data-title="' + officerorg + '" designation="' + officerorg + '" designation_id="' + officerorgid + '" group_member=1 unit_name="' + officerunit + '" unit_id ="' + officerunitid + '" ofc_name="' + officerofc + '" ofc_id="' + officerofcid + '" officer_id="' + officerid + '" office_head="' + officerhead + '" officer_name="' + officername + '" >' + officername + ', ' + officerunit + ', '+ officerofc +' <i class="fs1 a2i_gn_close2 remove_list '+prefix+'remove"></i></a></li>');
            $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
//                                        $(this).closest('.open').find('.dropdown-toggle').click();
            $('.' + prefix + 'list').find('input[type=text]').val('');
        }

    } else if (attr == 'tab_dropdown') {
        var officerid = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_id]').val();
        var officername = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_name]').val();
        var officerhead = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_head]').val();
        var officerofcid = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_id] option:selected').val();
        var officerofc = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_id] option:selected').text();
        var officerorgid = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_unit_organogram_id] option:selected').val();
        var officerorg = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_unit_organogram_id] option:selected').text() + ($(this).closest('.tab-pane').find('input[name=' + prefix + 'incharge_label]').val().length > 0 ? (" (" + $(this).closest('.tab-pane').find('input[name=' + prefix + 'incharge_label]').val() + ")") : "");
        var officerunitid = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_unit_id] option:selected').val();
        var officerunit = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_unit_id] option:selected').text();

        var hasEnglish = searchForEnglish($(this), prefix);
        if (!isEmpty(hasEnglish['officername'])) {
            officername = hasEnglish['officername'];
        }
        if (!isEmpty(hasEnglish['officerofc'])) {
            officerofc = hasEnglish['officerofc'];
        }
        if (!isEmpty(hasEnglish['officerunit'])) {
            officerunit = hasEnglish['officerunit'];
        }
        if (!isEmpty(hasEnglish['officerorg'])) {
            officerorg = hasEnglish['officerorg'];
        }

        if (officerorgid.length == 0 || officerid.length == 0 || officerofcid.length == 0) {
            toastr.error("দুঃখিত! তথ্য সঠিক নয়");
            return false;
        } else if (officerid>0 && (isEmpty(officerunitid) || isEmpty(officerunitid) || isEmpty(officerorgid))) {
			toastr.error("দুঃখিত! তথ্য সঠিক নয়");
			return false;
		}else {
            if ($(document).find('a.receiver_users[designation_id=' + officerorgid + ']').length >= 1  && prefix != 'attension_') {
                toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                return false;
            } else if ($(document).find('a.onulipi_users[designation_id=' + officerorgid + ']').length >= 1 && prefix != 'attension_') {
                toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                return false;
            }
             if(prefix == 'attension_'){
                attensionAdd(officerid,officername,officerorgid,officerorg,officerunitid,officerunit,officerofcid,officerofc);
            }
            $('.' + prefix + 'list').find('.sidebar-tags').append('<li><a class="' + prefix + 'users" href="javascript:;" data-toggle="tooltip" title="' + officerorg + '" data-title="' + officerorg + '" designation="' + officerorg + '" designation_id="' + officerorgid + '" unit_name="' + officerunit + '" group_member=1 unit_id ="' + officerunitid + '" ofc_name="' + officerofc + '" ofc_id="' + officerofcid + '" officer_id="' + officerid + '"  office_head="' + officerhead + '" officer_name="' + officername + '">' + officername + ', ' + officerunit + ', ' + officerofc +' <i class="fs1 a2i_gn_close2 remove_list '+prefix+'remove"></i></a></li>');

            $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
//                                        $(this).closest('.open').find('.dropdown-toggle').click();
            $('.' + prefix + 'list').find('input[type=text]').val('');
        }

    } else if (attr == 'tab_newoffice') {
        var officername = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_name]').val();
        var officerofc = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_name]').val();
        var officerorg = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_designation_label]').val();
        var officerunit = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_unit_name]').val();
        var officeremail = typeof ($(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_email]').val() != 'undefined') ? $.trim($(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_email]').val()) : '';
        // sms to user
         var officer_mobile = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_mobile]').val();
         var sms_message = $(this).closest('.tab-pane').find('textarea[name=' + prefix + 'sms_message]').val();
         if(!isEmpty(officer_mobile)){
             var mobile_numbers_array = officer_mobile.split(',');
             officer_mobile ='';
             var count_mobile_number = 0;
             for(index_of_array in  mobile_numbers_array){
                 mobile_number = EngFromBn($.trim(mobile_numbers_array[index_of_array]));
                 if(mobile_number.length != 11 && mobile_number.length != 13  && mobile_number.length != 14){
                     toastr.error("দুঃখিত! মোবাইল নম্বর "+mobile_number+" সঠিক নয়");
                     return false;
                 }
                 if(!/[0-9]{11}|[0-9]{13}|\+[0-9]{13}/.test(mobile_number)){
                     toastr.error("দুঃখিত! মোবাইল নম্বর "+mobile_number+" সঠিক নয়");
                     return false;
                 }
                 officer_mobile +=mobile_number+'--';
                 count_mobile_number++;
                 if(count_mobile_number > 5){
                     toastr.error("দুঃখিত! মোবাইল নম্বর ৫টির বেশি গ্রহণযোগ্য নয়।");
                     return false;
                 }
             }
         }
        if (officeremail.length > 0) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(officeremail)) {

            } else {
                toastr.error("দুঃখিত! ইমেইল সঠিক নয়");
                return false;
            }
        }

        if (officerorg.length == 0 || officerofc.length == 0) {
            toastr.error("দুঃখিত! তথ্য সঠিক নয়");
            return false;
        } else {
            $('.' + prefix + 'list').find('.sidebar-tags').append('<li><a class="' + prefix + 'users" href="javascript:;" data-toggle="tooltip" title="' + officerorg + '" data-title="' + officerorg + '" designation="' + officerorg + '" designation_id="0" officer_email="' + officeremail + '" unit_name="' + officerunit + '" unit_id ="0" ofc_name="' + officerofc + '" ofc_id="0" officer_id="0" officer_name="' + officername + '" officer_mobile="'+officer_mobile+'" sms_message="'+sms_message+'" >' + (officername.length > 0 ? officername : officerorg) + (officerunit != 0 ? (', ' + officerunit) : '') + ' <i class="fs1 a2i_gn_close2 remove_list"></i></a></li>');
            $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
            $('.' + prefix + 'list').find('input[type=text]').val('');
            $('.' + prefix + 'list').find('textarea').val('');
        }
    } else if (attr == 'tab_groupuser') {
        if ($('.' + prefix + 'group_checkbox:checked').length == 0) {
            if (prefix == 'receiver_') {
                toastr.error('আপনি কোনো প্রাপক সিলেক্ট করেন নি');
                return false;
            } else {
                toastr.error('আপনি কোনো অনুলিপি সিলেক্ট করেন নি');
                return false;
            }
        }
        var that = this;
        if ($('#' + prefix + 'SeperateUserNameForGroup').is(':checked')) {
            addSeperateUserFromGroup(prefix, that);
        } else {
            var promise = new Promise(function (resolve, reject) {
                if($('#'+prefix+'customGrpName').is(':checked')){
                    var groupname = $("#"+prefix+"grpTmpName").val();
                } else {
                    var groupname = $(that).closest('.tab-pane').find('select[name=' + prefix + 'group_id]').find('option:selected').text();
                }
                var groupid = $(that).closest('.tab-pane').find('select[name=' + prefix + 'group_id]').val();
                var groupmem = $(that).closest('.tab-pane').find('.' + prefix + 'group_checkbox:checked').length;
                var group_designation = "";
                if (groupid == 0 || typeof(groupid) == 'undefined') {
                    reject(Error("দুঃখিত! তথ্য সঠিক নয়"));
                } else {
                    $.each($('.' + prefix + 'group_checkbox'), function (i, v) {
                        if ($(this).is(':checked') == true) {
                            group_designation += ($(this).data('office-unit-organogram-id') == 0 ? $.trim($(this).data('officer-email')) : $(this).data('office-unit-organogram-id')) + "~";
                        }
                    });
                    $('.' + prefix + 'list').find('.sidebar-tags').append('<li><a class="' + prefix + 'users" href="javascript:;" data-toggle="tooltip" title="' + groupname + '" data-title="' + groupname + '" group_name="' + groupname + '" group_id="' + groupid + '" group_member="'+groupmem+'" designation="" designation_id="0" officer_email="" unit_name="" unit_id ="0" ofc_name="" ofc_id="0" officer_id="0" officer_name="" group_designation="' + group_designation + '">' + groupname + ' <i class="fs1 a2i_gn_close2 remove_list"></i></a></li>');
                    resolve(1);
                }
            }).then(function (res) {
                $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
                $('.' + prefix + 'list').find('select[name=' + prefix + 'group_id]').val('');
                $('.' + prefix + 'list').find('select[name=' + prefix + 'group_id]').select2();
                $("."+ prefix +"grpTmpNameDiv").hide();
                $('.' + prefix + 'group_list').find('.list-group').html('');
            }).catch(function (err) {
                toastr.error(err);
                return false;
            });
        }
    }
    setInformation();
    if (prefix == 'sender_' || prefix == 'approval_' || prefix == 'sovapoti_' ||prefix == 'attension_' ) {
        $(this).next('.closethis').click();
    }
});

function searchForEnglish(that, prefix) {
    var error_msg = '';
    var result = [];
    if (potrojari_language == 'eng') {
        if (!isEmpty(that.closest('.tab-pane').find('input[name=' + prefix + 'name_eng]').val())) {
            result['officername'] = that.closest('.tab-pane').find('input[name=' + prefix + 'name_eng]').val();
        } else {
            error_msg = 'অফিসারের নাম';
        }
        if (!isEmpty(that.closest('.tab-pane').find('input[name=' + prefix + 'designation_name_eng]').val())) {
            result['officerorg'] = that.closest('.tab-pane').find('input[name=' + prefix + 'designation_name_eng]').val();
            if (!isEmpty(that.closest('.tab-pane').find('input[name=' + prefix + 'incharge_label_eng]').val())) {
                result['officerorg'] += (that.closest('.tab-pane').find('input[name=' + prefix + 'incharge_label_eng]').val().length > 0 ? (" (" + that.closest('.tab-pane').find('input[name=' + prefix + 'incharge_label_eng]').val() + ")") : "");
            }
        } else {
            if (!isEmpty(error_msg)) {
                error_msg += ', পদবির নাম';
            } else {
                error_msg = 'পদবির নাম';
            }
        }
        if (!isEmpty(that.closest('.tab-pane').find('input[name=' + prefix + 'unit_name_eng]').val())) {
            result['officerunit'] = that.closest('.tab-pane').find('input[name=' + prefix + 'unit_name_eng]').val();
        } else {
            if (!isEmpty(error_msg)) {
                error_msg += ', শাখার নাম';
            } else {
                error_msg = 'শাখার নাম';
            }
        }
        if (!isEmpty(that.closest('.tab-pane').find('input[name=' + prefix + 'office_name_eng]').val())) {
            result['officerofc'] = that.closest('.tab-pane').find('input[name=' + prefix + 'office_name_eng]').val();
        } else {
            if (!isEmpty(error_msg)) {
                error_msg += ', অফিসের নাম';
            } else {
                error_msg = 'অফিসের নাম';
            }
        }
        if (!isEmpty(error_msg)) {
            toastr.error('ব্যবহারকারীর যেসব তথ্য ইংরেজিতে পাওয়া যায়নিঃ ' + error_msg, {timeOut: 2000});
        }
    }
    return result;
}

function addSeperateUserFromGroup(prefix, that) {
    new Promise(function (resolve, reject) {
        var groupid = $(that).closest('.tab-pane').find('select[name=' + prefix + 'group_id]').val();
        if (groupid == 0 || typeof(groupid) == 'undefined') {
            reject(Error("দুঃখিত! তথ্য সঠিক নয়"));
        } else {
            $.each($('.' + prefix + 'group_checkbox'), function (i, v) {

                var officerid = $(this).data('office-employee-id');
                var officername = $(this).data('office-employee-name-bng');
                var officerhead = (!isEmpty($(this).data('office-head'))?$(this).data('office-head'):0);
                var officerofcid = $(this).data('office-id');
                var officerofc = $(this).data('office-name-bng');
                var officerorgid = $(this).data('office-unit-organogram-id');
                var officerorg = $(this).data('designation-name-bng');
                var officerunitid = $(this).data('office-unit-id');
                var officerunit = $(this).data('unit-name-bng');
                if (potrojari_language == 'eng') {
                    if( !isEmpty($(this).data('office-employee-name-eng'))){
                        officername = $(this).data('office-employee-name-eng');
                    }
                    if( !isEmpty($(this).data('office-name-eng'))){
                        officerofc = $(this).data('office-name-eng');
                    }
                    if( !isEmpty($(this).data('designation-name-eng'))){
                        officerorg = $(this).data('designation-name-eng');
                    }
                    if( !isEmpty($(this).data('unit-name-eng'))){
                        officerunit = $(this).data('unit-name-eng');
                    }
                }


                if ($(this).is(':checked') == true) {
                    if (typeof(officerorgid) == 'undefined' || officerorgid == '') {
                        // For Other user outside Nothi
                        var officeremail = $(this).data('officer-email');
                        // For Other user outside Nothi
                        $('.' + prefix + 'list').find('.sidebar-tags').append('<li><a class="' + prefix + 'users" href="javascript:;" data-toggle="tooltip" title="' + officerorg + '" data-title="' + officerorg + '" designation="' + officerorg + '" designation_id="0" officer_email="' + officeremail + '" group_member=1 unit_name="' + officerunit + '" unit_id ="0" ofc_name="' + officerofc + '" ofc_id="0" officer_id="0" officer_name="' + officername + '">' + (officername.length > 0 ? officername : officerorg) + (officerunit != 0 ? (', ' + officerunit+', '+officerofc) : '') + ' <i class="fs1 a2i_gn_close2 remove_list"></i></a></li>');
                    } else {
                        if ($(document).find('a.receiver_users[designation_id=' + officerorgid + ']').length >= 1  && prefix != 'attension_') {
                            toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                            return;
                        } else if ($(document).find('a.onulipi_users[designation_id=' + officerorgid + ']').length >= 1  && prefix != 'attension_') {
                            toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                            return;
                        }
                        $('.' + prefix + 'list').find('.sidebar-tags').append('<li><a class="' + prefix + 'users" href="javascript:;" data-toggle="tooltip" title="' + officerorg + '" data-title="' + officerorg + '" designation="' + officerorg + '" designation_id="' + officerorgid + '" unit_name="' + officerunit + '" group_member=1 unit_id ="' + officerunitid + '" ofc_name="' + officerofc + '" ofc_id="' + officerofcid + '" officer_id="' + officerid + '"  office_head="' + officerhead + '" officer_name="' + officername + '">' + officername + ', ' + ((officerhead==0)?officerunit+', ':'') + officerofc +' <i class="fs1 a2i_gn_close2 remove_list"></i></a></li>');
                    }
                }
            });

            resolve(1);
        }
    }).then(function (res) {
        $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
        $('.' + prefix + 'list').find('select[name=' + prefix + 'group_id]').val(0);
        $('.' + prefix + 'group_list').find('.list-group').html('');
        setInformation();
    }).catch(function (err) {
        toastr.error(err);
        return false;
    });
}
function attensionAdd(officerid,officername,officerorgid,officerorg,officerunitid,officerunit,officerofcid,officerofc){
        if ($("#attention").length == 0) {
            var office_head = $('input[name=attension_office_head]').val();
            var attension_text = (($("#potro_language").val() == 'eng') ? 'Attention:': 'দৃষ্টি আকর্ষণঃ');
            var html = '<div class="row text-left"><div class="col-md-12 col-xs-12 col-sm-12" style="font-size:13pt" id="attention"><b>'+attension_text +' </b> <a class="editable editable-click" id="attentionname" data-type="text" data-pk="1">' + officerorg +(office_head==0?( ', ' + officerunit):'') + ', ' + officerofc + '</a></div></div><br>';
            $("#sharok_no2").closest('.row').before(html);
            $('#attentionname').editable();
            $('input[name=attension_office_id_final]').val(officerofcid);
            $('input[name=attension_officer_id_final]').val(officerid);
            $('input[name=attension_office_unit_id_final]').val(officerunitid);
            $('input[name=attension_officer_designation_id_final]').val(officerorgid);
            $('input[name=attension_officer_designation_label_final]').val(officerorg);
            $('input[name=attension_officer_name_final]').val(officername);
            $('input[name=attension_office_name_final]').val(officerofc);
            $('input[name=attension_office_unit_name_final]').val(officerunit);
        } else {
             toastr.error("একাধিক পদবি দেয়া যাবে না। ");
            return false;
        }
}
function checkAttension(){
    if ($("#attention").length == 0 && !isEmpty($('input[name=attension_officer_designation_id_final]').val()) && $(".attension_users").length > 0) {
       var  officerorg = $('input[name=attension_officer_designation_label_final]').val();
       var  officerofc =   $('input[name=attension_office_name_final]').val();
         var officerunit = $('input[name=attension_office_unit_name_final]').val();
         var office_head = $('input[name=attension_office_head]').val();
         var attension_text = (($("#potro_language").val() == 'eng') ? 'Attention:': 'দৃষ্টি আকর্ষণঃ');
        var html = '<div class="row text-left"><div class="col-md-12 col-xs-12 col-sm-12" style="font-size:13pt" id="attention"><b>'+attension_text +' </b> <a class="editable editable-click" id="attentionname" data-type="text" data-pk="1">' + officerorg +(office_head==0?( ', ' + officerunit):'') + ', ' + officerofc + '</a></div></div><br>';
            $("#sharok_no2").closest('.row').before(html);
            $('#attentionname').editable();
    }else if( $(".attension_users").length == 0){
            $('input[name=attension_office_id_final]').val('');
            $('input[name=attension_officer_id_final]').val('');
            $('input[name=attension_office_unit_id_final]').val('');
            $('input[name=attension_officer_designation_id_final]').val('');
            $('input[name=attension_officer_designation_label_final]').val('');
            $('input[name=attension_officer_name_final]').val('');
            $('input[name=attension_office_name_final]').val('');
            $('input[name=attension_office_unit_name_final]').val('');
            $("#attention").remove();
    }
}
function checkBitoronJoggo(){
    if($('[name=potro_type]').val() == 1 || $('[name=potro_type]').val() == 30){
        if ($('#potrojariDraftForm').find('#office_organogram_id a').length > 1) {
            // console.log($("#bitoron").text());
            if($('#potrojari_language').bootstrapSwitch('state') == false){
                var html = '<div id="bitoron"><a class="editable editable-click" id="bitorontext" data-type="text" data-pk="1">'+(!isEmpty($("#bitoron").text())?$("#bitoron").text():' Distribution :')+ '</a> </div>';
            } else {
                var html = '<div id="bitoron"><a class="editable editable-click" id="bitorontext" data-type="text" data-pk="1">'+(!isEmpty($("#bitoron").text())?$("#bitoron").text():' বিতরণ :')+ '</a> </div>';
            }

            $("#bitoron").remove();
            $("#office_organogram_id").before(html);
            $('#potrojariDraftForm').find('#bitorontext').editable();
        }else{
             $("#bitoron").remove();
        }
    }
}
function reInitializeEditable(){
    $('#potrojariDraftForm').find('#sender_name').editable('destroy');
    $('#potrojariDraftForm').find('#sender_name').editable({
        type: 'text',
        value: (isEmpty($('#potrojariDraftForm').find('#sender_name').text()))?'...':$.trim($('#potrojariDraftForm').find('#sender_name').text()),
        display: function (value) {
            if (!value) {
               $(this).html($(".sender_users").eq(0).attr('officer_name'));
            } else {
                $(this).html(value);
                $(".sender_users").eq(0).attr('visibleName',value);
                 if($('[name=potro_type]').val() == 17){
                         $(".approval_users").eq(0).attr('visibleName',value);
                   }
            }
        }
    });
    $('#potrojariDraftForm').find('#sender_designation').editable('destroy');
    $('#potrojariDraftForm').find('#sender_designation').editable({
        type: 'text',
        value: (isEmpty($('#potrojariDraftForm').find('#sender_designation').text()))?'...':$.trim($('#potrojariDraftForm').find('#sender_designation').text()),
        display: function (value) {
            if (!value) {
               $(this).html($(".sender_users").eq(0).attr('designation'));
            } else {
                $(this).html(value);
                $(".sender_users").eq(0).attr('visibleDesignation',value);
                  if($('[name=potro_type]').val() == 17){
                         $(".approval_users").eq(0).attr('visibleDesignation',value);
                   }
            }
        }
    });
    $('#potrojariDraftForm').find('#sender_name2').editable('destroy');
    $('#potrojariDraftForm').find('#sender_name2').editable({
        type: 'text',
        value: (isEmpty($('#potrojariDraftForm').find('#sender_name2').text()))?'...':$.trim($('#potrojariDraftForm').find('#sender_name2').text()),
        display: function (value) {
            if (!value) {
               $(this).html($(".approval_users").eq(0).attr('officer_name'));
            } else {
                $(this).html(value);
                $(".approval_users").eq(0).attr('visibleName',value);
            }
        }
    });
    $('#potrojariDraftForm').find('#sender_designation2').editable('destroy');
    $('#potrojariDraftForm').find('#sender_designation2').editable({
        type: 'text',
        value: (isEmpty($('#potrojariDraftForm').find('#sender_designation2').text()))?'...':$.trim($('#potrojariDraftForm').find('#sender_designation2').text()),
        display: function (value) {
            if (!value) {
               $(this).html($(".approval_users").eq(0).attr('officer_name'));
            } else {
                $(this).html(value);
                $(".approval_users").eq(0).attr('visibleDesignation',value);
            }
        }
    });
    if($('[name=potro_type]').val() == 17){
        $('#potrojariDraftForm').find('#sovapotiname').editable('destroy');
        $('#potrojariDraftForm').find('#sovapotiname').editable({
            type: 'text',
            value: (isEmpty($('#potrojariDraftForm').find('#sovapotiname').text()))?'...':$.trim($('#potrojariDraftForm').find('#sovapotiname').text()),
            display: function (value) {
                if (!value) {
                    $(this).html($(".sovapoti_users").eq(0).attr('officer_name'));
                } else {
                    $(this).html(value);
                    $(".sovapoti_users").eq(0).attr('visibleName',value);
                    $('#sovapotiname2').text($.trim(value));
                }
            }
        });
        $('#potrojariDraftForm').find('#sovapoti_designation').editable('destroy');
        $('#potrojariDraftForm').find('#sovapoti_designation').editable({
            type: 'text',
            value: (isEmpty($('#potrojariDraftForm').find('#sovapoti_designation').text()))?'...':$.trim($('#potrojariDraftForm').find('#sovapoti_designation').text()),
            display: function (value) {
                if (!value) {
                    $(this).html($(".sovapoti_users").eq(0).attr('designation'));
                } else {
                    $(this).html(value);
                    $(".sovapoti_users").eq(0).attr('visibledesignation',value);
                    $('#sovapoti_designation2').text(value);
                }
            }
        });
    }

}
function checkMultipleUserAdd(prefix){
    // onumodon
    // 'attension_','sender_','approval_','sovapoti_'
    if($(document).find('a.sender_users').length > 0 && prefix =='sender_' ){
        return false;
    }
    if($(document).find('a.attension_users').length > 0 && prefix =='attension_' ){
        return false;
    }
    if($(document).find('a.approval_users').length > 0 && prefix =='approval_' ){
        return false;
    }
    if($(document).find('a.sovapoti_users').length > 0 && prefix =='sovapoti_' ){
        return false;
    }
    return true;
}
function removeSelections(prefix){
    var prefixs = ['sender_','attension_','approval_','sovapoti_','onulipi_','receiver_'];
    if($.inArray(prefix,prefixs)){
        $('.' + prefix + 'list').find('.sidebar-tags').html('');
    }
}

