$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
});
function addCategory(){
    var str = '<div class="col-md-12 row"><div class="col-md-2 col-sm-2"><label class="control-label pull-right">নামঃ </label></div><div class="col-md-8 col-sm-8"><input type="text" class="form-control" id="cat_name"></div></div>';
    bootbox.dialog({
        message: str,
        title: 'রিপোর্ট ক্যাটাগরি অন্তর্ভুক্তি',
        buttons: {
            success: {
                label: 'সংরক্ষণ',
                className: "green",
                callback: function () {
                    var result = $("#cat_name").val();
                    if(isEmpty(result)){
                        toastr.error('ক্যাটাগরি নাম দেওয়া হয়নি');
                        return;
                    }

                    PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'report-category-add',{'name': result},'json',function(response){
                        if(!isEmpty(response) && response.status == 'success'){
                            toastr.success(response.message);
                            window.location.reload();
                        }else{
                            if(!isEmpty(response.message)){
                                toastr.error(response.message);
                            }else{
                                toastr.error('টেকনিক্যাল ত্রুটি হয়েছে।');
                            }
                        }
                    });
                }
            },
            danger: {
                label: "বন্ধ করুন",
                className: "red",
                callback: function () {
                }
            }
        }
    });
}
function editCategory(id,cur_name){
    var str = '<div class="col-md-12 row"><div class="col-md-2 col-sm-2"><label class="control-label pull-right">নামঃ </label></div><div class="col-md-8 col-sm-8"><input type="text" class="form-control" id="cat_name" value="'+cur_name+'"></div></div>';
    bootbox.dialog({
        message: str,
        title: 'রিপোর্ট ক্যাটাগরি সংশোধন',
        buttons: {
            success: {
                label: 'সংশোধন',
                className: "green",
                callback: function () {
                    var result = $("#cat_name").val();
                    if(isEmpty(result)){
                        toastr.error('ক্যাটাগরি নাম দেওয়া হয়নি');
                        return;
                    }
                    if(isEmpty(id)){
                        toastr.error('প্রয়োজনীয় তথ্য পাওয়া যায়নি।');
                        return;
                    }
                    PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'report-category-add',{'name': result,'id' : id},'json',function(response){
                        if(!isEmpty(response) && response.status == 'success'){
                            toastr.success(response.message);
                            window.location.reload();
                        }else{
                            if(!isEmpty(response.message)){
                                toastr.error(response.message);
                            }else{
                                toastr.error('টেকনিক্যাল ত্রুটি হয়েছে।');
                            }
                        }
                    });
                }
            },
            danger: {
                label: "বন্ধ করুন",
                className: "red",
                callback: function () {
                }
            }
        }
    });
}
function deleteCategory(id) {
    var str = '<div class="col-md-12 row">ক্যাটাগরি মুছে ফেলা হলে, ক্যাটাগরির সাথে ম্যাপকৃত সকল অফিস ম্যাপিং মুছে ফেলা হবে। আপনি কি নিশ্চিত?</div>';
    bootbox.dialog({
        message: str,
        title: 'রিপোর্ট ক্যাটাগরি মুছুন',
        buttons: {
            success: {
                label: 'ক্যাটাগরি মুছুন',
                className: "green",
                callback: function () {
                    if(isEmpty(id)){
                        toastr.error('প্রয়োজনীয় তথ্য পাওয়া যায়নি।');
                        return;
                    }
                    PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'report-category-delete',{'id' : id},'json',function(response){
                        if(!isEmpty(response) && response.status == 'success'){
                            toastr.success(response.message);
                            window.location.reload();
                        }else{
                            if(!isEmpty(response.message)){
                                toastr.error(response.message);
                            }else{
                                toastr.error('টেকনিক্যাল ত্রুটি হয়েছে।');
                            }
                        }
                    });
                }
            },
            danger: {
                label: "বন্ধ করুন",
                className: "red",
                callback: function () {
                }
            }
        }
    });
}
function editCategorySerial(){
    var data =[];
    $('.category_serial').each(function(i,v){
        var ref = $(v).data('ref');
        var ref_val = !isEmpty($(v).val())?$(v).val():0;
        if(isEmpty(ref)){
            return;
        }
        data.push({ref : ref, val :ref_val});
    });
    if(data.length > 0){
        Metronic.blockUI({
            target: '.page-container',
            boxed: true,
            message: 'অপেক্ষা করুন'
        });
        PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'edit-category-serial',{data:data},'json',function(response){
            if(!isEmpty(response) && response.status == 'success'){
                toastr.success(response.message);
                window.location.reload();
            }else{
                if(!isEmpty(response.message)){
                    toastr.error(response.message);
                }else{
                    toastr.error('টেকনিক্যাল ত্রুটি হয়েছে।');
                }
            }
            Metronic.unblockUI('.page-container');
        });
    }
}