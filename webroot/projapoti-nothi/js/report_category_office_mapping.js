$(document).ready(function(){
    $("#office-ministry-id").bind('change', function () {
        EmployeeAssignment.loadMinistryWiseLayers();
    });
    $("#office-layer-id").bind('change', function () {
        EmployeeAssignment.loadMinistryAndLayerWiseOfficeOrigin();
    });
    $("#office-origin-id").bind('change', function () {
        getOfficesId($(this).val());
    });
    $("#mapped_offices").bind('DOMSubtreeModified', function () {
        $('#office_count').text(enTobn($('.selectedOffices').length));
    });
    $("#category-id").on("change",function(){
        var cat_id = $("#category-id :selected").val();
        if(!isEmpty(cat_id) && parseInt(cat_id) > 0){
            window.location.href = js_wb_root+'report-category-office-mapping/'+cat_id;
        }
    });
    $("#layer-id").on("change",function(){
        var layer_id = $("#layer-id :selected").val();
        var ministry_id = !isEmpty($("#ministry-id :selected").val())?$("#ministry-id :selected").val():0;
        if(!isEmpty(layer_id)){
            Metronic.blockUI({
                target: '#searchPanel',
                boxed: true,
                message: 'অপেক্ষা করুন'
            });
            $('#office_list').html('');
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'cron/getLayerwiseOffices',{office_layer_type : layer_id,ministry_id:ministry_id},'json',function(response){
                if(!isEmpty(response)){
                    var html = '<table class="table table-bordered"><thead><tr><th class="text-center"><input type="checkbox" class="checker"  onclick="checkAll($(this))"></th><th class="text-center">নাম</th></tr></thead><tbody>';
                    $.each(response,function (i,v) {
                        var checked = ($("#mapped_offices").find('div[data-office-id='+i+']').length > 0)?'checked':'';
                        html += '<tr><td class="text-center"><div class="form form-group" style="margin: 0 !important;"><input type="checkbox" value="'+i+'" class="checker" class="form-control" data-name="'+v+'" onclick="setOffice($(this))" '+checked+'></div></td><td class="text-center">'+v+'</td></tr>';
                    });
                    html += '</tbody></table>';
                    $('#office_list').html(html);
                    Metronic.unblockUI('#searchPanel');
                    loadDT();
                }
            });
        }
    });
    $("#ministry-id").on("change",function(){
        $("#layer-id").trigger("change");
    });

});
function setOffice(el) {
    if(isEmpty(el)){
        return;
    }
    var name = el.data('name');
    var id = el.val();
   if(el.is(':checked')){
       var html = '<div class="badge badge-primary selectedOffices" style="font-size: 11pt!important;margin-right: 2px;display: inline-block" data-office-id="'+id+'"> '+name+' <span class="delete"  onclick="unSetOffice($(this))"><i class="fa fa-times-circle"></i>  </span> </div>';
       $("#mapped_offices").append(html);
       // if($('.selectedOffices').length % 3 == 0){
       //     $("#mapped_offices").append('<br>');
       // }
    }else{
       $("#mapped_offices").find('div[data-office-id='+id+']').remove();
   }
}
function unSetOffice(el){
    var id = el.closest('.badge').data('office-id');
    $('input[value="'+id+'"]').click();
    el.closest('.badge').remove();
}
function mapOfficesWithCategory(){
    var category_id = $("#category-id :selected").val();
    if(isEmpty(category_id)){
        toastr.error('ক্যাটাগরি নির্বাচন করা হয়নি');
        return;
    }
    var offices_id ='';
    $('.selectedOffices').each(function (i,v) {
        if(!isEmpty($(this).data('office-id'))){
            if(i > 0){
                offices_id += '--';
            }
            offices_id += parseInt($(this).data('office-id'));
        }
    });
    if(isEmpty(offices_id)){
        toastr.error('কোন অফিস নির্বাচন করা হয়নি');
        return;
    }
    PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'map-category-office',{category_id : category_id,offices_id : offices_id},'json',function(response) {
        if(!isEmpty(response) && response.status == 'success'){
            toastr.success(response.message);
        }
        else if(!isEmpty(response)){
            toastr.error(response.message);
        }else{
            toastr.error('যান্ত্রিক ত্রুটি হয়েছে');
        }
    });
}
function checkAll(el){
    if(isEmpty(el)){
        return;
    }
    if(el.is(':checked')){
        $("#office_list tr").each(function (i,v) {
            var val = $(this).find('input[type=checkbox]').val();
            if(!isEmpty(val)){
                $('.badge[data-office-id="'+val+'"]').remove();
                $(this).find('input[type=checkbox]').click();
            }
            $(this).find('input[type=checkbox]').prop('checked', true);
        });
    }else{
        $("#office_list tr").each(function (i,v) {
            var val = $(this).find('input[type=checkbox]').val();
            $(this).find('input[type=checkbox]').prop('checked', false);
            if(!isEmpty(val)){
                $('.badge[data-office-id="'+val+'"]').remove();
            }
        });
    }
}
var EmployeeAssignment = {
    checked_node_ids: [],
    loadMinistryWiseLayers: function () {
        OfficeSetup.loadLayers($("#office-ministry-id").val(), function (response) {

        });
    },
    loadMinistryAndLayerWiseOfficeOrigin: function () {
        OfficeSetup.loadOffices($("#office-ministry-id").val(), $("#office-layer-id").val(), function (response) {

        });
    },
};
function getOfficesId(office_origin_id) {
    Metronic.blockUI({
        target: '#searchPanel',
        boxed: true,
        message: 'অপেক্ষা করুন'
    });
    $.ajax({
        type: 'POST',
        url: js_wb_root+"officeManagement/loadOriginOffices",
        dataType: 'json',
        data: {"office_origin_id": office_origin_id},
        success: function (data) {
            var html = '<table class="table table-bordered"><thead><tr><th class="text-center"><input type="checkbox" class="checker"  onclick="checkAll($(this))"></th><th class="text-center">নাম</th></tr></thead><tbody>';
            $.each(data,function (i,v) {
                var checked = ($("#mapped_offices").find('div[data-office-id='+i+']').length > 0)?'checked':'';
                html += '<tr><td class="text-center"><div class="form form-group" style="margin: 0 !important;"><input type="checkbox" value="'+i+'" class="checker" class="form-control" data-name="'+v+'" onclick="setOffice($(this))" '+checked+'></div></td><td class="text-center">'+v+'</td></tr>';
            });
            html += '</tbody></table>';
            $('#office_list').html(html);
            Metronic.unblockUI('#searchPanel');
            loadDT();
        }
    });
}
function loadDT() {
    $('.table').dataTable({
        "paging": false,
        'bLengthChange' : false,
        'info' : false,
        language: { search: "",searchPlaceholder : 'খুঁজুন' }
    });
}