$(document).ready(function(){
    calculateSum();
    $('input[type=number]').on('keyup change',calculateSum);
    $("#dak-nothijat").on('keyup change',setDakNisponno);
    $("#dak-nothivukto").on('keyup change',setDakNisponno);
    $("#potrojari-nisponno-internal").on('keyup change',setPotrojariNisponno);
    $("#potrojari-nisponno-external").on('keyup change',setPotrojariNisponno);
});
function calculateSum(){
    var total = 0;
    $('input[type=number]').each(function(i,v){
        if(!isEmpty($(v).val()) && isEmpty($(v).attr('readonly'))){
            total += parseInt($(v).val());
        }
    });
    if(total >= 0){
        $("#showSum").html('মোট মার্ক ধার্য করা হয়েছেঃ '+ BnFromEng(total) +'%');
        if(total > 100){
            $("#showSum").addClass('bg-red');
        }else{
            $("#showSum").removeClass('bg-red');
        }
    }
}
function setDakNisponno(){
    var nothijat = !isEmpty($("#dak-nothijat").val())?$("#dak-nothijat").val():0;
    var nothivukto = !isEmpty($("#dak-nothivukto").val())?$("#dak-nothivukto").val():0;
    $("#dak-nisponno").val(parseInt(nothijat) + parseInt(nothivukto));
}
function setPotrojariNisponno(){
    var internal = !isEmpty($("#potrojari-nisponno-internal").val())?$("#potrojari-nisponno-internal").val():0;
    var external = !isEmpty($("#potrojari-nisponno-external").val())?$("#potrojari-nisponno-external").val():0;
    $("#nothi-potrojari-niponno").val(parseInt(internal) + parseInt(external));
}