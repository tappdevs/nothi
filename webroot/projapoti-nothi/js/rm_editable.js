    // ------------------- potrojari_editable.js -------------------------------- //

var bDate = new Array("০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯");
var bMonth = new Array("জানুয়ারি", "ফেব্রুয়ারি", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর");
function callGuardFilePotro() {
    $('#responsiveModal').find('.modal-title').text('গার্ড ফাইল');
    $('#selectpage').val('');
    var grid = new Datatable();

    grid.init({
        "destroy": true,
        src: $('#filelist'),
        onSuccess: function (grid) {
            // recallDeropChangew();
            // execute some code after table records loaded
        },
        onError: function (grid) {
            // execute some code on network or other general error
        },
        onDataLoad: function (grid) {
            // execute some code on ajax data load

        },
        loadingMessage: 'লোড করা হচ্ছে...',
        dataTable: {
            "destroy": true,
            "scrollY": "500",
            "dom": "<'row'<'col-md-8 col-sm-12'li><'col-md-4 col-sm-12'<'table-group-actions pull-right'p>>r>t",
            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 50, 100, 500],
                ["১০", "২০", "৫০", "১০০", "৫০০"] // change per page values here
            ],
            "pageLength": 50, // default record count per page
            "ajax": {
                "url": js_wb_root + 'GuardFiles/officeGuardFile', // ajax source
            },
            "bSort": false,
            "ordering": false,
            "language": {// language settings
                // metronic spesific
                "metronicGroupActions": "_TOTAL_ রেকর্ড নির্বাচন করা হয়েছে:  ",
                "metronicAjaxRequestGeneralError": "অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না।",
                // data tables spesific
                "lengthMenu": "<span class='seperator'></span>দেখুন _MENU_ ধরন",
                "info": "<span class='seperator'>|</span>মোট _TOTAL_ টি পাওয়া গেছে",
                "infoEmpty": "",
                "emptyTable": "কোনো রেকর্ড  নেই",
                "zeroRecords": "কোনো রেকর্ড  নেই",
                "paginate": {
                    "previous": "প্রথমটা",
                    "next": "পরবর্তীটা",
                    "last": "শেষেরটা",
                    "first": "প্রথমটা",
                    "page": "পাতা",
                    "pageOf": "এর"
                }
            }
        }
    });
    $('#guard_file_category_potro').off('click').on('click',function () {
        grid.setAjaxParam('guard_file_category_id',$('#guard_file_category_potro').val()) ;
        $('.filter input, .filter select').each(function () {
            var $item = $(this);
            grid.setAjaxParam($item.attr('name'),$item.val()) ;
        });
        grid.getDataTable().ajax.url = js_wb_root + 'GuardFiles/officeGuardFile';
        grid.getDataTable().ajax.reload();
    });
    $(document).off('click', '.showDetailAttach').on('click', '.showDetailAttach', function (ev) {
        ev.preventDefault();
        getPopUpPotro($(this).attr('href'), $(this).attr('title'));
    });
    $(document).off('click', '.tagfile').on('click', '.tagfile', function (ev) {
        ev.preventDefault();
        var href = $(this).closest('div').find('.showDetailAttach').attr('href');
        var value = $(this).closest('div').find('.showDetailAttach').attr('data-title');

        bootbox.dialog({
            message: "বাছাইকৃত ফাইলটির কোন পেজটি রেফারেন্স করতে চান?<br/><div class='form-group'><input type='text' class='input-sm form-control' name='selectpage' id='selectpage' placeholder='সকল পেজ' /></div>",
            title: "গার্ড ফাইল রেফারেন্স",
            buttons: {
                success: {
                    label: "হ্যাঁ",
                    className: "green",
                    callback: function () {
                        var pages = $('#selectpage').val();
                        if (!isEmpty(pages)) {
                            var single_page = pages.split(',');
                            $.each(single_page, function (i,v) {
                                if(!isEmpty(v)){
                                    pasteHtmlAtCaret(" বিবেচ্য গার্ড ফাইল  <a class='showforPopup'  href='" + href + "/" + bnToen(v) + "' title=' গার্ড ফাইল'>" + value + " (পৃষ্ঠা " + enTobn(v) + ")" + "</a>, &nbsp;");
                                    $('#responsiveModal').modal('hide');
                                    $('#note').froalaEditor('events.focus', true);
                                }
                            });

                            // $('#noteComposer').froalaEditor('html.insert', " বিবেচ্য গার্ড ফাইল  <a class='showforPopup'  href='" + href + "/" + page + "' title=' গার্ড ফাইল'>" + value + "</a>, &nbsp;");

                        } else {
                            pasteHtmlAtCaret(" বিবেচ্য গার্ড ফাইল  <a class='showforPopup'  href='" + href + "' title=' গার্ড ফাইল'>" + value + "</a>, &nbsp;");
                            $('#responsiveModal').modal('hide');
                            $('#note').froalaEditor('events.focus', true);

                        }
                    }
                },
                danger: {
                    label: "না",
                    className: "red",
                    callback: function () {
                        $('#selectpage').val('');
                    }
                }
            }
        });
    });
}
$("#search").on("keyup", function() {
    var text = $(this).val().toLowerCase();

    $("table tr td").each(function() {
        var data = $(this).attr('data-search-term');
        if(typeof(data) == 'undefined'){
            return;
        }
        if ($(this).filter('[data-search-term *= ' + text + ']').length > 0 || text.length < 1) {
            $(this).closest('tr').show();
        }
        else {
            $(this).closest('tr').hide();
        }
    });
});

function  iniDictionaryPotro() {
    $('.fr-wrapper').contextmenu(function (e) {
        var data = $('#note').froalaEditor('selection.text');
        if (data != "" && data.length > 1) {
            e.preventDefault();
            if ((data[data.length - 1]) == ' ') {
                var isSpace = ' ';
            } else {
                var isSpace = '';
            }
            $("#word_potro").val(data);
            $("#space_potro").val(isSpace);
            customContextMenuCall(data);
        } else {
            var data = getWordFromEvent(e);
            if(data !="" && data.length >1){
                e.preventDefault();
                if ((data[data.length - 1])==' '){
                    var isSpace =' ';
                } else {
                    var isSpace ='';
                }
                $("#word_potro").val(data);
                $("#space_potro").val(isSpace);
                customContextMenuCall(data);
            } else {
                e.stopPropagation();
            }
        }
    });

    function customContextMenuCall(word) {
        'use strict';
        var errorItems = {"errorItem": {name: "Word Load error"},};
        var loadItems = function () {

            var dfd = jQuery.Deferred();
            setTimeout(function () {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: 'https://ovidhan.tappware.com/search.php',
                    data: {'word': $("#word_potro").val()},
                    success: function (data) {
                        if (data.length > 10) {
                            var subSubItems = {};
                        }
                        var i = 1;
                        var subItems = {};
                        $(data).each(function (j, v) {
                            if (i < 11) {
                                var name = {};
                                name['name'] = v;
                                subItems[v] = name;
                            } else {
                                var name = {};
                                name['name'] = v;
                                subSubItems[v] = name;
                            }
                            i++;
                        });

                        if (data.length > 10) {
                            var seeMoreMenu = {};
                            seeMoreMenu = {
                                name: "<strong>আরও দেখুন...</strong>",
                                isHtmlName: true,
                                items: subSubItems
                            };
                            subItems['আরও দেখুন...'] = seeMoreMenu;
                        }

                        dfd.resolve(subItems);
                    }
                });
            }, 100);
            return dfd.promise();
        };

        $.contextMenu({
            selector: '.fr-wrapper',
            build: function ($trigger, e) {
                return {
                    callback: function (key, options) {
                        if (key != "কোন তথ্য পাওয়া যায় নি।") {
                            $('#note').froalaEditor('html.insert', (key + ($("#space_potro").val())));
                        }
                    },
                    items: {
                        "status": {
                            name: "সাজেশন্স",
                            icon: "fa-check",
                            items: loadItems(),
                        },
                    }
                };
            }
        });


        var completedPromise = function (status) {
            // console.log("completed promise:", status);
        };

        var failPromise = function (status) {
            // console.log("fail promise:", status);
        };

        var notifyPromise = function (status) {
            // console.log("notify promise:", status);
        };

        $.loadItemsAsync = function () {
            // console.log("loadItemsAsync");
            var promise = loadItems();
            $.when(promise).then(completedPromise, failPromise, notifyPromise);
        };
    }
}

$(document).off('change', '#layer-ids-potro').on('change', '#layer-ids-potro', function () {
    var layer = $('#layer-ids-potro').val();

    if(!(isEmpty(layer))) {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: js_wb_root + "GuardFiles/getPortalTypesByLayer/"+layer,
        data: {},
        success: function (data) {
            var list="<option value='0'>--</option>";
            if(!(isEmpty(data))) {
                $.each(data,function (index,value) {
                    list +="<option value='"+value+"'>"+value+"</option>"
                });

            }
            $('#portal-guard-file-types').html('');
            $('#portal-guard-file-types').html(list);
            $('#portal-guard-file-types').select2();

        }
    });
}
});

var PotrojariFormEditable = function () {

    $.mockjaxSettings.responseTime = 500;

    var log = function (settings, response) {

    }

    var initAjaxMock = function () {
        //ajax mocks

        $.mockjax({
            url: '/post',
            response: function (settings) {
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/error',
            status: 400,
            statusText: 'Bad Request',
            response: function (settings) {
                this.responseText = 'Please input correct value';
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/status',
            status: 500,
            response: function (settings) {
                this.responseText = 'Internal Server Error';
                log(settings, this);
            }
        });
    }

    var initEditables = function ($headSetting, $nothino, $sarokno, $employee_office, $subject, $date, $totalPotrojari,potrojari_language,$write_unit) {

        //set editable mode based on URL parameter
        $.fn.editable.defaults.mode = 'inline';

        jQuery.uniform.update('#inline');

        //global settings
        $.fn.editable.defaults.inputclass = 'form-control';
        $.fn.editable.defaults.url = '/post';

        //global settings
        $.fn.editable.defaults.inputclass = 'form-control';
        $.fn.editable.defaults.url = '/post';

        //editables element samples
        var officeDetails = $headSetting.head_office;

		if($headSetting.remove_header_left_slogan==1){
			$('#potrojariDraftForm').find('#left_slogan').remove();
		}

		if($headSetting.remove_header_right_slogan==1){
			$('#potrojariDraftForm').find('#right_slogan').next('br').remove();
			$('#potrojariDraftForm').find('#right_slogan').remove();
		}

		if($headSetting.remove_header_head_1==1){
			$('#potrojariDraftForm').find('#gov_name').prev('br').remove();
			$('#potrojariDraftForm').find('#gov_name').remove();
		}

		if($headSetting.remove_header_head_2==1){
			$('#potrojariDraftForm').find('#office_ministry').prev('br').remove();
			$('#potrojariDraftForm').find('#office_ministry').remove();
		}

		if($headSetting.remove_header_head_3==1){
			$('#potrojariDraftForm').find('#offices').prev('br').remove();
			$('#potrojariDraftForm').find('#offices').remove();
		}

		if($headSetting.remove_header_unit==1){
			$('#potrojariDraftForm').find('#unit_name_editable').prev('br').remove();
			$('#potrojariDraftForm').find('#unit_name_editable').remove();
		}
		if($headSetting.remove_header_head_4==1){
			$('#potrojariDraftForm').find('#web_url_or_office_address').prev('br').remove();
			$('#potrojariDraftForm').find('#web_url_or_office_address').remove();
		}
		if($headSetting.remove_header_head_5==1){
			$('#potrojariDraftForm').find('#office_address').prev('br').remove();
			$('#potrojariDraftForm').find('#office_address').remove();
		}

        var k = 0;
        $('#potrojariDraftForm').find('#gov_name').editable({
            value: $.trim($('#potrojariDraftForm').find('#gov_name').text()) != '...' ? $.trim($('#potrojariDraftForm').find('#gov_name').text()) : ($headSetting.head_title != null ? $headSetting.head_title : '...'),
            name: 'office_ministry',
            title: '',
            display: function (value) {
                if (!value) {

                    $(this).html(html);
                } else {

                    $(this).html(value);
                }
            }
        });
        if($('#potro-type').val() == 21 || $('#potro-type').val() == 27){
            if($employee_office.office_id != 3260){
                $('#potrojariDraftForm').find('#form_name_noc').next('br').remove();
                $('#potrojariDraftForm').find('#form_name_noc').remove();
                $('#potrojariDraftForm').find('#office_address_rtl').text($employee_office.office_address);
            }
            $('#potrojariDraftForm').find('#office_name_rl').editable({
                value: $employee_office.office_name,
                name: 'office_name_rl',
                title: '',
                display: function (value) {
                    if (!value) {

                        $(this).html(html);
                    } else {

                        $(this).html(value);
                    }
                }
            });
            $('#potrojariDraftForm').find('#office_address_rtl').editable({
                name: 'office_address_rtl',
                title: '',
                type: 'textarea',
                display: function (value) {
                    if (!value) {

                        $(this).html(html);
                    } else {

                        $(this).html(value);
                    }
                }
            });

        }


        $('#potrojariDraftForm').find('#office_ministry').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            value: $.trim($('#potrojariDraftForm').find('#office_ministry').text()) != '...' ? $.trim($('#potrojariDraftForm').find('#office_ministry').text()) : ($headSetting.head_ministry != null ? $headSetting.head_ministry : '...'),
            name: 'office_ministry',
            title: '',
            display: function (value) {
                if (!value) {

                    $(this).html(html);
                } else {

                    $(this).html(value);
                }
            }
        });

//        $('#potrojariDraftForm').find('#sender_name').editable(
//            {
//                url: '/post',
//                type: 'text',
//            }
//        )
//        $('#potrojariDraftForm').find('#sender_designation').editable(
//            {
//                url: '/post',
//                type: 'text',
//            }
//        )
//
//        $('#potrojariDraftForm').find('#sender_name2').editable(
//            {
//                url: '/post',
//                type: 'text',
//            }
//        )
//        
//        $('#potrojariDraftForm').find('#sender_designation2').editable(
//            {
//                url: '/post',
//                type: 'text',
//            }
//        )

        $('#potrojariDraftForm').find('#unit_name_editable').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            value: $.trim($('#potrojariDraftForm').find('#unit_name_editable').text()) != '...' ? $.trim($('#potrojariDraftForm').find('#unit_name_editable').text()) : (($write_unit==1?(potrojari_language == 'eng'?$employee_office.office_unit_name_eng:$employee_office.office_unit_name):'...')),
            name: 'unit_name_editable',
            title: '',
            display: function (value) {
                if (!value) {
                    $(this).html(html);
                } else {
                    $(this).html(value);
                }
            }
        });

        $('#potrojariDraftForm').find('#offices').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            value: $.trim($('#potrojariDraftForm').find('#offices').text()) != '...' ? $.trim($('#potrojariDraftForm').find('#offices').text()) : (officeDetails != null ? officeDetails : '...'),
            name: 'offices',
            title: 'অফিসের নাম লিখুন',
            display: function (value) {
                if (!value) {
                    $(this).html(html);
                } else {

                    $(this).html(value);
                }
            }
        });


        $('#potrojariDraftForm').find('#subject').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'subject',
            title: 'বিষয় লিখুন',
            value: $subject,
            display: function (value) {
                if (!value) {
                    $(this).html(html);
                } else {
                    $(this).html(value);
                }

            }
        });

        $('#potrojariDraftForm').find('#web_url_or_office_address').editable({
            url: '/post',
            type: 'text',
            value: $.trim($('#potrojariDraftForm').find('#web_url_or_office_address').text()) != '...' ? $.trim($('#potrojariDraftForm').find('#web_url_or_office_address').text()) : $headSetting.head_other,
            pk: 1,
            name: 'web_url_or_office_address',
            title: 'ঠিকানা / ওয়েবসাইটের ঠিকানা লিখুন',
            display: function (value) {
                if (!value) {
                    $(this).html(html);
                } else {
                    $(this).html(value);
                }

            }
        });


        $('#potrojariDraftForm').find('#office_address').editable({
            url: '/post',
            type: 'text',
            value: $.trim($('#potrojariDraftForm').find('#office_address').text()) != '...' ? $.trim($('#potrojariDraftForm').find('#office_address').text()) : $headSetting.head_office_address,
            pk: 1,
            name: 'web_url_or_office_address',
            title: 'ঠিকানা / ওয়েবসাইটের ঠিকানা লিখুন',
            display: function (value) {
                if (!value) {
                    $(this).html(html);
                } else {
                    $(this).html(value);
                }

            }
        });

        $('#potrojariDraftForm').find('#sharok_no2').editable({
            inputclass: 'form-control input-medium',
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'sharok_no2',
            title: 'স্মারক নম্বর লিখুন'
        });

        $('#potrojariDraftForm').find('#sharok_no').editable({
            name: "sharok_no",
            value: $('#potrojariDraftForm').find('#sharok_no').text() != '...' ? $('#potrojariDraftForm').find('#sharok_no').text() : ((potrojari_language == 'eng')?EngFromBn($nothino + ('.' + $totalPotrojari)):($nothino + ('.' + $totalPotrojari))),
            validate: function (value) {
                if ($.trim(value) == '')
                    return 'এই তথ্য দিতে হবে';
            },
            display: function (value) {
                $(this).text(value);
                var onu = $('#potrojariDraftForm').find('.onulipi_users').length > 0 ? $.map($('#potrojariDraftForm').find('.onulipi_users'),function(data){
                    return parseInt($(data).attr('group_member'));
                }).reduce(function(total,num){return total+num; }) : 0;

                var bn = replaceNumbers("'" + (onu) + "'");
                bn = bn.replace("'", '');
                bn = bn.replace("'", '');
                if(potrojari_language == 'eng'){
                    $('#potrojariDraftForm').find("#sharok_no2").text(value + (onu > 0 ? ('/1(' + onu + ')') : ''));
                }else{
                   $('#potrojariDraftForm').find("#sharok_no2").text(value + (onu > 0 ? ('/১(' + bn + ')') : '')); 
                }
                
            }
        });

        $('#potrojariDraftForm').find('.to_list').editable({
            inputclass: 'form-control input-medium',
            name: "to_list",
            type: 'textarea',
            display: function (value,d) {
                var IndexChange = $('.to_list').index(this);
                 var nong = $(this).attr('nong');
                if(IndexChange == 0 && typeof(d) == 'undefined' ){
                    return;
                }
                value =value.replace(/(?:\r\n|\r|\n)/g, '<br />\u00a0');
                  $(this).html(value);
                if(typeof(nong) != 'undefined'){
//                   $(".receiver_users").eq(IndexChange).attr('visibleName',value);
                   $(".receiver_users").eq(nong).attr('visibleName',value);
                }
                
                // console.log(IndexChange,d,typeof(d));
//                $(this).text(value);
            }
        });

        $('#potrojariDraftForm').find('#office_organogram_id2').editable({
            inputclass: 'form-control input-medium',
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'office_organogram_id2'
        });

        $('#potrojariDraftForm').find('#getarthe').editable({
            inputclass: 'form-control input-medium',
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'getarthe'
        });

        $('#potrojariDraftForm').find('#bitorontext_routine_letter').editable({
            inputclass: 'form-control input-medium',
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'bitorontext_routine_letter'
        });
        $('#potrojariDraftForm').find('#cror_potro_text').editable({
            inputclass: 'form-control input-medium',
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'cror_potro_text'
        });
        $('#potrojariDraftForm').find('#cror_potro_body').editable({
            inputclass: 'form-control input-medium',
            url: '/post',
            type: 'textarea',
            pk: 1,
            name: 'cror_potro_body'
        });


        // for NOC,RM,Formal
        if($('#potro-type').val() == 21 || $('#potro-type').val() == 27){
            $('#potrojariDraftForm').find('#gov_name_noc').editable({
                inputclass: 'form-control input-medium',
                url: '/post',
                type: 'text',
                pk: 1,
                name: 'gov_name_noc'
            });
            $('#potrojariDraftForm').find('#ministry_name_noc').editable({
                inputclass: 'form-control input-medium',
                url: '/post',
                type: 'text',
                pk: 1,
                name: 'ministry_name_noc',
                value: $.trim($('#potrojariDraftForm').find('#ministry_name_noc').text()) != '...' ? $.trim($('#potrojariDraftForm').find('#ministry_name_noc').text()) : (($write_unit==1?(potrojari_language == 'eng'?$employee_office.office_unit_name_eng:$employee_office.office_unit_name):'...')),
                title: '',
                display: function (value) {
                    if (!value) {
                        $(this).html(html);
                    } else {
                        $(this).html(value);
                    }
                }
            });

            $('#potrojariDraftForm').find('#form_name_noc').editable({
                inputclass: 'form-control input-medium',
                url: '/post',
                type: 'text',
                pk: 1,
                name: 'form_name_noc',
                display: function (value) {
                if (!value) {
                    $(this).html(html);
                } else {
                    $(this).html(value);
                }

            }
            });
            $('#potrojariDraftForm').find('#office_website').editable({
                inputclass: 'form-control input-medium',
                url: '/post',
                type: 'text',
                pk: 1,
                name: 'office_website'
            });
            $('#potrojariDraftForm').find('#office_name_rl').editable({
                inputclass: 'form-control input-medium',
                url: '/post',
                type: 'text',
                pk: 1,
                name: 'office_name_rl'
            });
            $('#potrojariDraftForm').find('#sender_unit').editable({
                inputclass: 'form-control input-medium',
                url: '/post',
                type: 'text',
                pk: 1,
                name: 'sender_unit'
            });
            $('#potrojariDraftForm').find('#p_nong').editable({
                inputclass: 'form-control input-medium',
                url: '/post',
                type: 'text',
                pk: 1,
                name: 'p_nong'
            });
            check_attachment();
             $(".remove_div_if_empty").show();
            $(".getarthe").show();
        }

        var editor, html = '';


        $('#potrojariDraftForm').find('#pencil').click(function (e) {
            e.stopPropagation();
            e.preventDefault();

            if ($('#potrojariDraftForm').find('#note').attr('contenteditable') == "true") {
                $('#potrojariDraftForm').find('#note').removeAttr('contenteditable');
                $(this).html(' <i class="fs1 a2i_gn_edit2"></i> [সম্পাদন  করুন] <br/>');
                $('.fr-basic table').addClass('class2');
                removeSpellChecker();
                $('#note').froalaEditor('destroy');
                var preData= $('#note').val();
                $('#note').css("display","none");
                $('#noteView').html(preData);
                $('#noteView').css("display","inline");

			} else {
                $('.btn-nothiback').attr('disabled','disabled');
                $('#potrojariDraftForm').find('#note').attr('contenteditable', "true");
                $(this).html(' <i class="fs1 a2i_gn_edit2"></i> [সংরক্ষণ  করুন] <br/>');

                var preData= $('#noteView').html();
                $('#noteView').css("display","none");
                $('#note').html(preData);

                    $.FroalaEditor.DefineIconTemplate('material_design', '<i class="fs0 [NAME]"></i>');
                    $.FroalaEditor.DefineIcon('গার্ড ফাইল', {NAME: 'fs0 efile-guard_file1', template: 'material_design'});
                    $.FroalaEditor.RegisterCommand('গার্ড ফাইল', {
                        title: 'গার্ড ফাইল',
                        focus: false,
                        undo: false,
                        showOnMobile: true,
                        refreshAfterCallback: false,
                        callback: function () {
                            $('#responsiveModal').modal('show');
                            $("#guard_file_category_potro").val(0);
                            $("#s2id_guard_file_category_potro [id^=select2-chosen]").text('সকল');
                            callGuardFilePotro()
                        }
                    });

                $.FroalaEditor.DefineIcon('spell-checker', {NAME: 'check-circle'});
                $.FroalaEditor.RegisterCommand('spell-checker', {
                    title: 'বানান পরীক্ষক',
                    focus: true,
                    undo: true,
                    showOnMobile: true,
                    refreshAfterCallback: true,
                    callback: function () {
                        getWordSeperated();
                    }
                });

                $.FroalaEditor.DefineIcon('spell-checker-off', {NAME: 'minus-circle'});
                $.FroalaEditor.RegisterCommand('spell-checker-off', {
                    title: 'বানান পরীক্ষক বন্ধ করুন',
                    focus: true,
                    undo: true,
                    showOnMobile: true,
                    refreshAfterCallback: true,
                    callback: function () {
                        removeSpellChecker();
                    }
                });

                var buttons = ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertLink', 'insertTable', '|', 'insertHR', 'clearFormatting', '|', 'undo', 'redo', 'গার্ড ফাইল','spell-checker','spell-checker-off'];
                var buttonsSm = ['bold', 'underline','|', 'fontSize', 'align', '|','formatOL', 'formatUL', 'outdent', 'indent'];

                var editor = $('#note').froalaEditor({
                    key: 'xc1We1KYi1Ta1WId1CVd1F==',
                    toolbarSticky: false,
                    wordPasteModal: true,
					wordAllowedStyleProps: ['font-size', 'background', 'color', 'text-align', 'vertical-align', 'background-color', 'padding', 'margin', 'height', 'margin-top', 'margin-left', 'margin-right', 'margin-bottom', 'text-decoration', 'font-weight'],
					wordDeniedTags: ['a','form'],
					wordDeniedAttrs: ['width'],
                    tableResizerOffset: 10,
                    tableResizingLimit: 20,
                    toolbarButtons: buttons,
                    toolbarButtonsMD: buttons,
                    toolbarButtonsSM: buttonsSm,
                    toolbarButtonsXS: buttonsSm,
                    placeholderText: '',
                    height: 400,
                    enter: $.FroalaEditor.ENTER_BR,
                    fontSize: ['10','11', '12','13', '14','15','16','17', '18','19','20','21','22','23','24','25','26','27','28','29', '30'],
                    fontSizeDefaultSelection: '13',
                    tableEditButtons:['tableHeader', 'tableRemove', '|', 'tableRows', 'tableColumns','tableCells', '-',  'tableCellBackground', 'tableCellVerticalAlign', 'tableCellHorizontalAlign', 'tableCellStyle'],
                    fontFamily: {
                        "Nikosh, SolaimanLipi,'Open Sans', sans-serif": "Nikosh",
                        "Kalpurush": "Kalpurush",
                        "SolaimanLipi": "SolaimanLipi",
                        "'times new roman'": "Times New Roman",
                        "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
                    }
                });
                $('#note').on('froalaEditor.keyup', function (e, editor, keyupEvent) {
                    getCaretPosition();
                });
                $('#note').on('froalaEditor.mouseup', function (e, editor, keyupEvent) {
                    getCaretPosition();
                });
                $.contextMenu( 'destroy' );
                iniDictionaryPotro();
            }
        });

        $('#potrojariDraftForm').find('#sending_date').editable({
            rtl: Metronic.isRTL(),
            display: function (value) {
                var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                if (value != '' && value != null) {
                    var dtb, dtb1, dtb2;
                    var dt = value.getDate();
                    if (dt >= 10) {
                        dtb1 = Math.floor(dt / 10);
                        dtb2 = dt % 10;
                        dtb = bDate[dtb1] + "" + bDate[dtb2];
                    } else {
                        dtb = bDate[0] + "" + bDate[dt];
                    }

                    var mnb;
                    var mn = value.getMonth();
                    mnb = bMonth[mn];

                    var yrb = "", yr1;
                    var yr = value.getFullYear();

                    for (var i = 0; i < 3; i++) {
                        yr1 = yr % 10;
                        yrb = bDate[yr1] + yrb;
                        yr = Math.floor(yr / 10);
                    }

                    yrb = bDate[yr] + "" + yrb;
                    
                    if(potrojari_language == 'eng'){
                        var day = (value.getDate().length == 1)?'0'+value.getDate():value.getDate();
                         $(this).text(day + "/" + (mn + 1) + "/" + value.getFullYear());
                    }
                    else{
                        if($("#potro-type").val() == 27){
                            //formal letter
                        }else{
                            $(this).text(" " + dtb + " " + mnb + " " + yrb).attr("style","position: relative; top: -15px;");
                        }

                    }
                    var that = $(this);
                    $('#potrojariDraftForm').find('.bangladate').next('br').remove();

                    $('#potrojariDraftForm').find('.bangladate').remove();
                      if(potrojari_language == 'eng'){
//                        that.before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" +value.getDate() + "/" + (mn + 1) + "/" + value.getFullYear() + "</span><br/>");
//                            $('#potrojariDraftForm').find("#sending_date_2");
//                            $('#potrojariDraftForm').find("#sending_date_2").before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" +value.getDate() + "/" + (mn + 1) + "/" + value.getFullYear() + "</span><br/>");
                    }
                    else if($("#potro-type").val() == 20){
                        //LM
                        $(this).text(dtb+" "+bMonth[mn]+" "+ BnFromEng(value.getFullYear()));
                    }
                    else if($("#potro-type").val() == 27){
                          //formal
                          $(this).text(dtb+" "+bMonth[mn]+" "+ BnFromEng(value.getFullYear()));
                    }
                    else{
                        $.ajax({
                            url: js_wb_root + "banglaDate",
                            data: {date: value.getFullYear() + "-" + (mn + 1) + "-" + value.getDate()},
                            type: 'POST',
                            cache: false,
                            dataType: 'JSON',
                            async: false,
                            success: function (bangladate) {

                                     that.before("<span class=' bangladate' style='position: relative; top: -15px;border:0px!important;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");

                                $('#potrojariDraftForm').find("#sending_date_2").attr('style', 'position: relative; top: -15px;');
                                $('#potrojariDraftForm').find("#sending_date_2").before("<span class=' bangladate' style='position: relative; top: -15px;border:0px!important;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");
                                }
                        });
                    }

                    if ($('#potrojariDraftForm').find("#sending_date_2")) {
                        $('#potrojariDraftForm').find("#sending_date_2").text($(this).text()).attr('readDate', value);
                    }
                } else {
                    var dtb, dtb1, dtb2;

                    value = new Date();
                    var dt = value.getDate();
                    if (dt >= 10) {
                        dtb1 = Math.floor(dt / 10);
                        dtb2 = dt % 10;
                        dtb = bDate[dtb1] + "" + bDate[dtb2];
                    } else {
                        dtb = bDate[0] + "" + bDate[dt];
                    }

                    var mnb;
                    var mn = value.getMonth();
                    mnb = bMonth[mn];

                    var yrb = "", yr1;
                    var yr = value.getFullYear();

                    for (var i = 0; i < 3; i++) {
                        yr1 = yr % 10;
                        yrb = bDate[yr1] + yrb;
                        yr = Math.floor(yr / 10);
                    }

                    yrb = bDate[yr] + "" + yrb;
                    
                     if(potrojari_language == 'eng'){
                         $(this).text(value.getDate() + "/" + (mn + 1) + "/" + value.getFullYear() );
                     }else{
                          if($("#potro-type").val() == 20){
                            //LM
                            $(this).text(dtb+" "+bMonth[mn]+" "+ BnFromEng(value.getFullYear()));
                        }
                        else if($("#potro-type").val() == 27){
                              //formal
                              $(this).text(dtb+" "+bMonth[mn]+" "+ BnFromEng(value.getFullYear()));
                          }
                        else{
                            $(this).text(" " + dtb + " " + mnb + " " + yrb ).attr("style","position: relative; top: -15px;");
                        }
                     }
                    
//                    $(this).text(" " + dtb + " " + mnb + ", " + yrb + " খ্রিষ্টাব্দ");

                    var that = $(this);
                    $('#potrojariDraftForm').find('.bangladate').next('br').remove();
                    $('#potrojariDraftForm').find('.bangladate').remove();
                    if(potrojari_language == 'eng'){
//                        that.before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" +value.getDate() + "/" + (mn + 1) + "/" + value.getFullYear() + "</span><br/>");
//                            $('#potrojariDraftForm').find("#sending_date_2");
//                            $('#potrojariDraftForm').find("#sending_date_2").before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" +value.getDate() + "/" + (mn + 1) + "/" + value.getFullYear() + "</span><br/>");
                    }  
                    else if($("#potro-type").val() == 20){
                         //LM
                        $(this).text(dtb+" "+bMonth[mn]+" "+ BnFromEng(value.getFullYear()));
                    }
                    else if($("#potro-type").val() == 27){
                        //formal
                        $(this).text(dtb+" "+bMonth[mn]+" "+ BnFromEng(value.getFullYear()));
                    }
                    else{
                            $.ajax({
                            url: js_wb_root + "banglaDate",
                            data: {date: value.getFullYear() + "-" + (mn + 1) + "-" + value.getDate()},
                            type: 'POST',
                            cache: false,
                            dataType: 'JSON',
                            async: false,
                            success: function (bangladate) {
                                that.before("<span class=' bangladate' style='position: relative; top: -15px;border:0px!important;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");

                                $('#potrojariDraftForm').find("#sending_date_2").attr('style', '');
                                $('#potrojariDraftForm').find("#sending_date_2").before("<span class=' bangladate' style='position: relative; top: -15px;border:0px!important;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");
                            }
                        });
                    }
                    

                    if ($('#potrojariDraftForm').find("#sending_date_2")) {
                        $('#potrojariDraftForm').find("#sending_date_2").text($(this).text()).attr('readDate', value);
                    }
                }
            }
        });

        $('#potrojariDraftForm').find('.cc_list').editable({
            inputclass: 'form-control input-medium',
            type: 'textarea',
            pk: 1,
            validate: function (value) {
                if ($.trim(value) == '')
                    return 'এই তথ্য দিতে হবে';
            },
            display: function (value,d) {
                value =value.replace(/(?:\r\n|\r|\n)/g, '<br />\u00a0');
                var nong = $(this).attr('nong');
                $(this).html(value);
                var IndexChange = $('.cc_list').index(this);
                if(IndexChange == 0 && typeof(d) == 'undefined' ){
                    return;
                }
                 if(typeof(nong) != 'undefined'){
                   $(".onulipi_users").eq(nong).attr('visibleName',value);
//                   $(".onulipi_users").eq(IndexChange).attr('visibleName',value);
                }
                
                // console.log(IndexChange,d,typeof(d));

            }
        });

        $('#potrojariDraftForm').find('#reference').editable({
            inputclass: 'form-control input-medium',
            url: '/post',
            type: 'textarea',
            pk: 1,
            name: 'reference',
            value: ($.trim($('#potrojariDraftForm').find('#reference').text()).length > 0 && $.trim($('#potrojariDraftForm').find('#reference').text()).substr(0,3) != '...' && $.trim($('#potrojariDraftForm').find('#reference').html())!='(যদি থাকে) ...' ? $.trim($('#potrojariDraftForm').find('#reference').html()) : ((potrojari_language == 'Eng')?EngfromBn($sarokno):$sarokno)),
            
            title: 'সূত্র লিখুন (যদি থাকে)',
            display: function (value) {
                if ($.trim(value) == '')
                    $(this).text('...');
                else
                    $(this).html(value.replace(/(?:\r\n|\r|\n)/g, '<br />\u00a0'));
            }
        });

        $('#potrojariDraftForm').find('#potro_template_title').editable();

        $('#potrojariDraftForm').find('#i_remain_sir').editable({
            inputclass: 'form-control input-medium',
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'i_remain_sir',
            title: 'ইতি লিখুন'
        });

        $('#potrojariDraftForm').find('#sender_identitynumber').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'sender_identitynumber',
            title: 'পরিচয় পত্রের নম্বর লিখুন'
        });

        $('#potrojariDraftForm').find('#sender_phone').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            value: ($.trim($('#potrojariDraftForm').find('#sender_phone').text()).length > 0 && $.trim($('#potrojariDraftForm').find('#sender_phone').text()) != '...' ? $.trim($('#potrojariDraftForm').find('#sender_phone').text()) : (!isEmpty($employee_office.office_phone) ? ((potrojari_language == 'eng')? EngFromBn($employee_office.office_phone):$employee_office.office_phone) : '...')),
            name: 'sender_phone',
            title: 'ফোন নম্বর লিখুন',
            display: function (value) {
                if ($.trim(value) == '')
                    $(this).text('...');
                else
                    $(this).text(value);
            }
        });

        $('#potrojariDraftForm').find('#sender_fax').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            value: ($.trim($('#potrojariDraftForm').find('#sender_fax').text()).length > 0 && $.trim($('#potrojariDraftForm').find('#sender_fax').text()) != '...' ? $.trim($('#potrojariDraftForm').find('#sender_fax').text()) : (!isEmpty($employee_office.office_fax) ? ((potrojari_language == 'eng')?EngFromBn($employee_office.office_fax):$employee_office.office_fax) : '...')),
            name: 'sender_fax',
            title: 'ফ্যাক্স লিখুন',
            display: function (value) {
                if ($.trim(value) == '')
                    $(this).text('...');
                else
                    $(this).text(value);
            }
        });

        $('#potrojariDraftForm').find('#sender_email').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            value: ($.trim($('#potrojariDraftForm').find('#sender_email').text()).length > 0 && $.trim($('#potrojariDraftForm').find('#sender_email').text()) != '...' ? $.trim($('#potrojariDraftForm').find('#sender_email').text()) : (!isEmpty($employee_office.office_email) ? $employee_office.office_email : '...')),
            name: 'sender_email',
            title: 'ইমেইল লিখুন',
            display: function (value) {
                if ($.trim(value) == '')
                    $(this).text('...');
                else
                    $(this).text(value);
            }
        });


        $('#potrojariDraftForm').find('#sovapotiname').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'sovapotiname',
            title: '',
            validate: function (value) {
                if ($.trim(value) == '')
                    return 'এই তথ্য দিতে হবে';
            },
            display: function (value) {
                $(this).text(value);
                $('#sovapotiname2').text(value);
            }
        });

        $('#potrojariDraftForm').find('#sovapoti_designation').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'sovapotipodobi',
            title: '',
            validate: function (value) {
                if ($.trim(value) == '')
                    return 'এই তথ্য দিতে হবে';
            },
            display: function (value) {
                $(this).text(value);
                $('#sovapoti_designation2').text(value);
            }
        });

        $('#potrojariDraftForm').find('#sovadate').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'sovadate',
            title: '',
            validate: function (value) {
                if ($.trim(value) == '')
                    return 'এই তথ্য দিতে হবে';
            },
            display: function (value) {
                $(this).text(value);
            }
        });

        $('#potrojariDraftForm').find('#sovatime').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'sovatime',
            title: '',
            validate: function (value) {
                if ($.trim(value) == '')
                    return 'এই তথ্য দিতে হবে';
            },
            display: function (value) {
                $(this).text(value);
            }
        });

        $('#potrojariDraftForm').find('#sovaplace').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'sovaplace',
            title: '',
            validate: function (value) {
                if ($.trim(value) == '')
                    return 'এই তথ্য দিতে হবে';
            },
            display: function (value) {
                if ($.trim(value) == '')
                    $(this).text('...');
                else
                    $(this).text(value);
            }
        });

        $('#potrojariDraftForm').find('#sovapresent').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'sovapresent',
            title: '',
            validate: function (value) {
                if ($.trim(value) == '')
                    return 'এই তথ্য দিতে হবে';
            },
            display: function (value) {
                if ($.trim(value) == '')
                    $(this).text('...');
                else
                    $(this).text(value);
            }
        });
        setInformation();
    }

    return {
        //main function to initiate the module
        init: function ($head_ministry, $nothino, $sarokno, $employee_office, $subject, $date, $totalPotrojari,potrojari_language,write_unit) {

            // inii ajax simulation
            initAjaxMock();

            // init editable elements
            // if ($sarokno == '') {
            //     $('#potrojariDraftForm').find('#reference').closest('.row').remove();
            // }
            initEditables($head_ministry, $nothino, $sarokno, $employee_office, $subject, $date, $totalPotrojari,potrojari_language,write_unit);
        },
       encodeImage: function (imgurl, enc_imgurl, callback){
           $.ajax({
               url : js_wb_root + 'getSignature/'+ imgurl +'/1?token='+enc_imgurl,
               cache: false,
               type: 'post',
               async: false,
               success: function(img){
                   callback(img);
               },
               error: function (res, err){
                   return false;
               }
           });
       }
    };
}();

$('#portalGuardFileImportPotro').on('click',function(event){
    var empty_option="<option value='0'>--</option>";
    $('#portal-guard-file-types').html('');
    $('#layer-ids-potro').val(0);
    $('#portal-guard-file-types').html(empty_option);
    $('#portal-guard-file-types').select2();
    $('#layer-ids-potro').select2();

    $('#portalfilelistPotro tbody').html('<tr><td colspan="4" class="text-center"><img src="'+js_wb_root+'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span></td></tr>');

    $.ajax({
        type: 'GET',
        dataType: "json",
        url: js_wb_root + "GuardFiles/guardFileApi",
        data: {},
        success: function (data) {
            if (data.status == 'success') {
                if(!isEmpty(data.data)){
                    var list='';
                    var i =1;
                    $.each(data.data,function (index,value) {
                        if(isEmpty(value.link)){
                            return false;
                        }
                        list+='<tr><td>'+BnFromEng(i)+'</td><td>'+value.type+'</td><td data-search-term="'+value.name+'">'+value.name+'</td><td class="text-center"><a class="btn btn-success btn-sm downFile" title="ডাউনলোড করুন" href="'+value.link+'"><i class="fa fa-download"></i></a></td></tr>';
                        i++;
                    });
                    $('#portalfilelistPotro tbody').html(list);
                } else {
                    $('#portalfilelistPotro tbody').html('<tr><td colspan="4" style="color: red" class="text-center">দয়াকরে অফিস বাছাই করুন।</td></tr>');
                }
            } else {
                $('#portalfilelistPotro tbody').html('<tr><td colspan="4" style="color: red" class="text-center">দয়াকরে অফিস বাছাই করুন। </td></tr>');
            }
        }
    });
    $('#portalResponsiveModalPotro').modal('show');
});
$(document).off('click', '.downFile').on('click', '.downFile', function (ev) {
    ev.preventDefault();
    $('.WaitMsg').html('');
    $(".submitbutton").removeAttr( 'disabled' );
    var href = $(this).attr('href');
    var value = $(this).closest('td').prev().text();
    $('#name-bng').val(value);
    $('input[name="uploaded_attachments"]').val(href);
    $('#portalDownloadModal').modal('show');
});

$(document).off('change', '#portal-guard-file-types').on('change', '#portal-guard-file-types', function () {
    var type = $('#portal-guard-file-types').val();

    $('#portalfilelist tbody').html('<tr><td colspan="4" class="text-center"><img src="'+js_wb_root+'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp; লোড হচ্ছে। একটু অপেক্ষা করুন... </span></td></tr>');
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: js_wb_root + 'GuardFiles/guardFileApi/'+type, // ajax so
    data: {},
    success: function (data) {
        if (data.status == 'success' || data.status=="SUCCESS" ) {
            if(!isEmpty(data.data)){
                var list='';
                var i =1;
                $.each(data.data,function (index,value) {
                    if(isEmpty(value.link)){
                        return false;
                    }
                    list+='<tr><td>'+BnFromEng(i)+'</td><td>'+value.type+'</td><td data-search-term="'+value.name+'">'+value.name+'</td><td class="text-center"><a class="btn btn-success btn-sm downFile" title="ডাউনলোড করুন" href="'+value.link+'"><i class="fa fa-download"></i></a></td></tr>';
                    i++;
                });
                $('#portalfilelistPotro tbody').html(list);
            } else {
                $('#portalfilelistPotro tbody').html('<tr><td colspan="4" style="color: red" class="text-center">দুঃখিত! কোন ডাটা পাওয়া যায়নি।</td></tr>');
            }
        } else {
            $('#portalfilelistPotro tbody').html('<tr><td colspan="4" style="color: red" class="text-center">দুঃখিত! কোন ডাটা পাওয়া যায়নি। </td></tr>');
        }
    }
});
});


  // ------------------- potrojari_editable.js -------------------------------- //