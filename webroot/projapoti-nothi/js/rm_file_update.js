var PotroJariFileUpload = function () {
    return {
        //main function to initiate the module
        init: function () {

             // Initialize the jQuery File Upload widget:
            $('#fileuploadpotrojari').fileupload({
                disableImageResize: false,
                autoUpload: true,
                uploadTemplateId: 'template-upload2',
            // The ID of the download template:
            downloadTemplateId: 'template-downloadSonlogni',
                disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                maxFileSize: 20099999,
                acceptFileTypes: /(\.|\/)(jpe?g|png|pdf|doc|docx|xlsx|xls|ppt|pptx|bmp)$/i,
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},                
            });

            // Enable iframe cross-domain access via redirect option:
            $('#fileuploadpotrojari').fileupload(
                'option',
                'redirect',
                window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                )
            );

            // Upload server status check for browsers with CORS support:
            if ($.support.cors) {
                $.ajax({
                    type: 'HEAD'
                }).fail(function () {
                    $('<div class="alert alert-danger"/>')
                        .text('Upload server currently unavailable - ' +
                                new Date())
                        .appendTo('#fileuploadpotrojari');
                });
            }

            // Load & display existing files:
            $('#fileuploadpotrojari').addClass('fileupload-processing');
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: $('#fileuploadpotrojari').attr("action"),
                dataType: 'json',
                context: $('#fileuploadpotrojari')[0]
            }).always(function () {
                $(this).removeClass('fileupload-processing');
            }).done(function (result) {
                $(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});
            });
            
            // Initialize the jQuery File Upload widget:
            $('#fileuploadpotrojari_crorpotro').fileupload({
                disableImageResize: false,
                autoUpload: true,
                uploadTemplateId: 'template-upload2',
            // The ID of the download template:
            downloadTemplateId: 'template-downloadCrorPotro',
                disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                maxFileSize: 20099999,
                acceptFileTypes: /(\.|\/)(jpe?g|png|pdf|doc|docx|xlsx|xls|ppt|pptx|bmp)$/i,
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},                
            });

            // Enable iframe cross-domain access via redirect option:
            $('#fileuploadpotrojari_crorpotro').fileupload(
                'option',
                'redirect',
                window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                )
            );


            // Load & display existing files:
            $('#fileuploadpotrojari_crorpotro').addClass('fileupload-processing');
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: $('#fileuploadpotrojari_crorpotro').attr("action"),
                dataType: 'json',
                context: $('#fileuploadpotrojari_crorpotro')[0]
            }).always(function () {
                $(this).removeClass('fileupload-processing');
            }).done(function (result) {
                $(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});
            });
        }

    };

}();