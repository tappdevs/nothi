$(document).ready(function ($) {
    // delegate calls to data-toggle="lightbox"
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
    $('.approval_list').hide();
    $('.attension_list').hide();
    $('.sender_list').find('a').closest('.potrojariOptions').html('প্রেরক <i class="glyphicon glyphicon-chevron-down pull-right"></i>');
});
$(function () {
    $('#prapto-potro').multiSelect({
        dblClick: true, afterSelect: function (values) {
            var title = "";
            var id = values;
            var part_id =$("#part_id").val();
            var token =$("#token").val();
            $('#responsiveOnuccedModal').find('.modal-title').text('');
            $('#responsiveOnuccedModal').find('.modal-body').html('');
            PROJAPOTI.ajaxSubmitDataCallback(js_wb_root+'NothiMasters/showPopUp/' + id + '/potro/' + $nothimasterid+'?nothi_part='+part_id+'&token='+token, {'nothi_office': $nothi_office}, 'html', function (response) {
                $('#responsiveOnuccedModal').modal('show');
                $('#responsiveOnuccedModal').find('.modal-title').text(title);
                $('#responsiveOnuccedModal').find('.modal-body').html(response);
            });
        }
    });

})


function getPopUpPotro(href, title) {
    $('#responsiveOnuccedModal').find('.modal-title').text('');
    $('#responsiveOnuccedModal').find('.modal-body').html('');
    var part_id =$("#part_id").val();
    var token =$("#token").val();
    $.ajax({
        url: js_wb_root+'NothiMasters/showPopUp/' + href+'?nothi_part='+part_id+'&token='+token,
        dataType: 'html',
        data: {'nothi_office': $nothi_office},
        type: 'post',
        success: function (response) {
            $('#responsiveOnuccedModal').modal('show');
            $('#responsiveOnuccedModal').find('.modal-title').text(title);

            $('#responsiveOnuccedModal').find('.modal-body').html(response);
        }
    });
}

$(document).off('click', '.showforPopup').on('click', '.showforPopup', function (e) {
    e.preventDefault();
    var title = $(this).attr('title').length > 0 ? $(this).attr('title') : $(this).attr('data-original-title');
    getPopUpPotro($(this).attr('href'), title);
})

$(document).on('click', '.btn-print', function () {
    $('#template-body').printThis({
        importCSS: true,
        debug: false,
        importStyle: true,
        printContainer: true,
        pageTitle: "",
        removeInline: false,
        header: null
    });
});

$(document).ready(function () {
    var potro_type = $('[name=potro_type]').val();
    if (potro_type == 4) {
        $('.onulipi_list').hide();
    } else if (potro_type == 5) {
        $('.receiver_list').hide();
    } else if (potro_type == 10) {
        $('.receiver_list').hide();
    } else if (potro_type == 11) {
        $('.receiver_list').hide();
    } else if (potro_type == 12) {
        $('.receiver_list').hide();
    } else if ($('#potro-type').val() == 20) {
        $('.approval_list').hide();
        $('.attension_list').hide();
        $('.sender_list').find('a').closest('.potrojariOptions').html('প্রেরক <i class="glyphicon glyphicon-chevron-down pull-right"></i>');
        $("a[href=#tab_other_potro]").text("সংলগ্নী ");
    } else if ($('#potro-type').val() == 22) {
        $('.approval_list').hide();
        $('.onulipi_list').hide();
        $('.attension_list').hide();
        $('.sender_list').find('a').closest('.potrojariOptions').html('এন.ও.সি প্রদানকারী <i class="glyphicon glyphicon-chevron-down pull-right"></i>');
    }
});
function check_attachment() {
    if ($('#potro-type').val() == 21 || $('#potro-type').val() == 27) {
        checkSonlogni();
        checkCrorpotro();
    }
}
function checkSonlogni() {
    if ($('#fileuploadpotrojari .potro-attachment-input').length > 0) {
        $(".sologni_div").show();
    } else {
        $(".sologni_div").hide();
    }

    var i = 1;
    var songlogni_text = '';
    $('#fileuploadpotrojari .potro-attachment-input').each(function () {
        var tx = $(this).val();
        var songlogni_set_text = $("#rm_songlogni_set_" + i).text();
        if (isEmpty(songlogni_set_text)) {
            songlogni_set_text = '...';
        }
        songlogni_text += "<tr style='text-align: left!important'></td><td style='width:50%'>"+ BnFromEng(i) + "। " + tx + "</td><td style='width:50%'><a class='rm_songlogni_set' id='rm_songlogni_set_" + i + "'>" + songlogni_set_text + "</a></td></tr>";
        i++;
    });
    $('.sologni_div .attachment_list').html('<table style="width:100%">' + songlogni_text + '</table>');
    $('#potrojariDraftForm').find('.rm_songlogni_set').editable('destroy');
    $('#potrojariDraftForm').find('.rm_songlogni_set').editable({
        inputclass: 'form-control input-medium',
        url: '/post',
        type: 'text',
        pk: 1,
        name: 'lm_songlogni_set'
    });
}
function checkCrorpotro() {
    if ($('#fileuploadpotrojari_crorpotro .potro-attachment-input').length > 0) {
        $(".crorpotro_div").show();
    } else {
        $(".crorpotro_div").hide();
    }

    var i = 1;
    var songlogni_text = '';
    $('#fileuploadpotrojari_crorpotro .potro-attachment-input').each(function () {
        var tx = $(this).val();
        var songlogni_set_text = $("#rm_crorpotro_set_" + i).text();
        if (isEmpty(songlogni_set_text)) {
            songlogni_set_text = '...';
        }
        songlogni_text += "<tr style='text-align: left!important'><td style='width:50%'>"+ BnFromEng(i) + "। " + tx + "</td><td style='width:50%'><a class='rm_crorpotro_set' id='rm_crorpotro_set_" + i + "'>" + songlogni_set_text + "</a></td></tr>";
        i++;
    });
    $('.crorpotro_div .attachment_list').html('<table style="width:100%">' + songlogni_text + '</table>');
     $('#potrojariDraftForm').find('.rm_crorpotro_set').editable('destroy');
    $('#potrojariDraftForm').find('.rm_crorpotro_set').editable({
        inputclass: 'form-control input-medium',
        url: '/post',
        type: 'text',
        pk: 1,
        name: 'lm_songlogni_set'
    });
}
// >>>>>>>>>>>>>>>>>>>>>>>>> potrojari_related.js >>>>>>>>>>>>>>>>>>>>>> //

var potrojari_language = 'bn';
jQuery(document).ready(function () {
//    $('.potro_language').show();
    $("#potrojari_language").bootstrapSwitch({
        state: ($("#potro_language").val() == 'eng') ? false : true,
    });
    $('#potrojari_language').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state == false) {
//            $("#template-body").attr('style','font-size: 10px!important;font-family: "Times New Roman" ');
        } else {
            $("#template-body").attr('style', 'font-size: 13pt!important;font-family: Nikosh');
        }

        potrojari_language = ($('#potrojari_language').bootstrapSwitch('state') == true) ? 'bn' : 'eng';
        $("#potro_language").val(potrojari_language);
        // getTemplates();

//        setInformation();
    });
    if (isEmpty($("#potro_language").val())) {
        $("#potro_language").val(potrojari_language);
    } else {
        potrojari_language = $("#potro_language").val();
    }

});

function getTemplates() {
    Metronic.blockUI({
        target: '#template-body'
    });
    var promise = new Promise(function (resolve, reject) {
        var url = js_wb_root + 'PotrojariTemplates/getTemplatesByVersion/' + potrojari_language;
        PROJAPOTI.ajaxSubmitDataCallback(url, {}, 'json', function (data) {
            PROJAPOTI.projapoti_dropdown_map('#potro-type', data, 'পত্রের ধরন বাছাই করুন', 0);
        });
        resolve(1);
    }).then(function (res) {
        Metronic.unblockUI('#template-body');
        $('#template-body').html('');
        $("#potro-type").trigger('change');
    }).catch(function (err) {
        Metronic.unblockUI('#template-body');
        toastr.error(err);
        return false;
    });
}
var totalOntosthoReceiver = '';
var totalBohisthoReceiver = '';
var totalOntosthoOnulipi = '';
var totalBohisthoOnulipi = '';
var indexOntosthoReceiver = 0;
var indexBohisthoReceiver = 0;
var indexOntosthoOnulipi = 0;
var indexBohisthoOnulipi = 0;
function setInformation() {
    totalOntosthoReceiver = '';
    totalBohisthoReceiver = '';
    totalOntosthoOnulipi = '';
    totalBohisthoOnulipi = '';
    indexOntosthoReceiver = 0;
    indexBohisthoReceiver = 0;
    indexOntosthoOnulipi = 0;
    indexBohisthoOnulipi = 0; 
    var senders = $('.sender_users');
    var approvals = $('.approval_users');
    var receivers = $('.receiver_users');
    var onulipis = $('.onulipi_users');
    var sovapotis = $('.sovapoti_users');

    $('#sovapoti_signature').html("");
    $('#sovapoti_designation').html("");
    $('#sovapotiname').html("");
    $('#sovapotiname2').html("");
    $('#sovapoti_designation2').html("");
    $('#sender_signature').html("");
    $('#sender_signature2').html("");
    $('#sender_designation').html("");
    $('#sender_name').html("");
    $('#sender_designation2').html("");
    $('#sender_name2').html("");
    $('#sender_designation3').html("");
    $('#sender_name3').html("");
    $('#office_organogram_id').html("");
    $('#address').html("");
    $('#cc_list_div').html("");
    $('#office_organogram_id').html('');

    checkAttension();
    setSecrecyAndPriority();
    setSovapotiInfo(sovapotis);
    setSenderInfo(senders);
    setApprovalInfo(approvals);
    setReceiverInfo(receivers);
    setOnulipiInfo(onulipis);
    checkBitoronJoggo();
    setInternalExternalUsers();
    reInitializeEditable();
}
function setSecrecyAndPriority() {
    var DAK_PRIORITY_TYPE_ENG = {
        '0': ' ',
        '2': 'Emergency',
        '3': 'Immediately',
        '4': 'Highest Priority',
        '5': 'Instructions',
        "6": 'Seek Attention'
    };
    var DAK_SECRECY_TYPE_ENG = {
        '0': ' ',
        '2': 'Very Confidential',
        '3': 'Special Confidential',
        '4': 'Confidential',
        '5': 'Limited'
    };
    if (potrojari_language == 'eng') {
        $('#potro_priority').text(DAK_PRIORITY_TYPE_ENG[$('#potro-priority-level option:selected').val()]);
        $('#potro_security').text(DAK_SECRECY_TYPE_ENG[$('#potro-security-level option:selected').val()]);
        $('.potro_security').text(DAK_SECRECY_TYPE_ENG[$('#potro-security-level option:selected').val()]);
    } else {
        $('#potro_priority').text($('#potro-priority-level option:selected').text());
        $('#potro_security').text($('#potro-security-level option:selected').text());
        $('.potro_security').text($('#potro-security-level option:selected').text());
    }
}
function setSovapotiInfo(sovapotis) {
    if ($('[name=potro_type]').val() == 17) {
        if (sovapotis.length != 0) {
            $.each($('.sovapoti_users'), function (i, data) {
                if (i == 0) {
                    var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
                    if (sendename.length > 0) {
                    } else {
                        sendename[0] = sendename;
                    }
                    $('#sovapoti_designation').append($(this).attr('designation'));
                    $('#sovapotiname').append(sendename[0]);
                    $('#sovapoti_designation2').append($(this).attr('designation'));
                    $('#sovapotiname2').append(sendename[0]);
                }
            });
        }
    }
}
function setOnulipiInfo(onulipis) {
    if (onulipis.length == 0) {
        $('input[name=onulipi_group_id_final]').val('');
        $('input[name=onulipi_group_name_final]').val('');
        $('input[name=onulipi_group_member_final]').val(0);
        $('input[name=onulipi_group_designation_final]').val('');
        $('input[name=onulipi_office_id_final]').val('');
        $('input[name=onulipi_office_name_final]').val('');
        $('input[name=onulipi_officer_designation_id_final]').val('');
        $('input[name=onulipi_officer_designation_label_final]').val('');
        $('input[name=onulipi_office_unit_id_final]').val('');
        $('input[name=onulipi_office_unit_name_final]').val('');
        $('input[name=onulipi_officer_id_final]').val('');
        $('input[name=onulipi_officer_name_final]').val('');
        $('input[name=onulipi_officer_email_final]').val('');
        $('input[name=onulipi_office_head_final]').val('');
        $('input[name=onulipi_visibleName_final]').val('');
        $('input[name=onulipi_officer_mobile_final]').val('');
        $('input[name=onulipi_sms_message_final]').val('');
        $('input[name=onulipi_options_final]').val('');
        $('#potrojariDraftForm').find('#cc_list_div').closest('.row').nextAll('.row').hide()
        $('#potrojariDraftForm').find('#cc_list_div').closest('.row').hide();
        $('#potrojariDraftForm').find('#sharok_no2').closest('.row').hide();
        $('#potrojariDraftForm').find('#sending_date_2').closest('.row').hide();
    } else {
        if ($('#potrojariDraftForm').find('#cc_list_div').length > 0 || $('[name=potro_type]').val() == 13) {

            var totalmember = $.map(onulipis, function (data) {
                return parseInt($(data).attr('group_member'));
            }).reduce(function (total, num) {
                return total + num;
            });

            var bn = replaceNumbers("'" + totalmember + "'");
            bn = bn.replace("'", '');
            bn = bn.replace("'", '');
            if (potrojari_language == 'eng') {
                $('#sharok_no2').text($('#sharok_no').text() + "/1" + (totalmember > 1 ? ("(" + totalmember + ")") : ""));
            } else {
                $('#sharok_no2').text($('#sharok_no').text() + "/" + replaceNumbers('1') + (totalmember > 1 ? ("(" + bn + ")") : ""));
            }

            $('#potrojariDraftForm').find('#cc_list_div').closest('.row').nextAll('.row').show()
            $('#potrojariDraftForm').find('#cc_list_div').closest('.row').show();
            $('#potrojariDraftForm').find('#sharok_no2').closest('.row').show();
            $('#potrojariDraftForm').find('#sending_date_2').closest('.row').show();

            $.each(onulipis, function (i, data) {
                var bn = replaceNumbers("'" + (i + 1) + "'");
                bn = bn.replace("'", '');
                bn = bn.replace("'", '');
                if (typeof ($(this).attr('group_id')) == 'undefined') {
                    if (!isEmpty($(this).attr('visibleName'))) {
                        var visibleName = $(this).attr('visibleName');
                        visibleName =visibleName.replace(/<br\s*[\/]?>/gi, '\n');
                        $('input[name=onulipi_visibleName_final]').val(visibleName);
                    } else {
                         if(!isEmpty($(this).attr('user_type')) && $(this).attr('user_type') == 1 ){
                                // internal receiver
                                if(!isEmpty($(this).attr('office_selection'))){
                                    // internal receiver office selection
                                  var visibleName =  setUserInfoInTemplateForInternalOfficeSelection(this,0);
                                }else{
                                    // internal receiver direct selection 
                                   var visibleName =  setUserInfoInTemplateForInternalDirectSelection(this,0);
                                }
                            }else{
                                    // external receiver
                                if(!isEmpty($(this).attr('office_selection'))){
                                    // external receiver office selection
                                  var visibleName =  setUserInfoInTemplateForExternalOfficeSelection(this,0);
                                }else{
                                    // external receiver direct selection 
                                   var visibleName =  setUserInfoInTemplateForExternalDirectSelection(this,0);
                                }
                            }
                    }
                     if(!isEmpty($(this).attr('user_type')) && $(this).attr('user_type') == 1 ){
//                        $('#cc_list_div').append((i + 1) + ') <a data-type="text" id="cc_div_item" data-pk=1 href="javascript:;" class="editable editable-click cc_list " >' + visibleName + "</a><br/>");
                        totalOntosthoOnulipi += '<a nong='+i+' data-type="textarea" data-pk=1 href="javascript:;" class="editable editable-click cc_list cc_div_item">' + visibleName.toString() + "</a><br/><br/>";
                    } else {
//                        $('#cc_list_div').append(bn + ') <a data-type="text" id="cc_div_item" data-pk=1 href="javascript:;" class="editable editable-click cc_list " >' + visibleName + "</a><br/>");
                        totalBohisthoOnulipi += '<a nong='+i+' data-type="textarea" data-pk=1 href="javascript:;" class="editable editable-click cc_list cc_div_item">' + visibleName.toString() + '</a><br/><br/>';
                    }
                }
            });

        } else {
            $('input[name=onulipi_group_id_final]').val('');
            $('input[name=onulipi_group_name_final]').val('');
            $('input[name=onulipi_group_member_final]').val(0);
            $('input[name=onulipi_group_designation_final]').val('');
            $('input[name=onulipi_office_id_final]').val('');
            $('input[name=onulipi_office_name_final]').val('');
            $('input[name=onulipi_officer_designation_id_final]').val('');
            $('input[name=onulipi_officer_designation_label_final]').val('');
            $('input[name=onulipi_office_unit_id_final]').val('');
            $('input[name=onulipi_office_unit_name_final]').val('');
            $('input[name=onulipi_officer_id_final]').val('');
            $('input[name=onulipi_officer_name_final]').val('');
            $('input[name=onulipi_officer_email_final]').val('');
            $('input[name=onulipi_office_head_final]').val('');
            $('input[name=onulipi_visibleName_final]').val('');
            $('input[name=onulipi_officer_mobile_final]').val('');
            $('input[name=onulipi_sms_message_final]').val('');
            $('input[name=onulipi_options_final]').val('');
            $('#potrojariDraftForm').find('#cc_list_div').closest('.row').nextAll('.row').hide()
            $('#potrojariDraftForm').find('#cc_list_div').closest('.row').hide();
            $('#potrojariDraftForm').find('#sharok_no2').closest('.row').hide();
            $('#potrojariDraftForm').find('#sending_date_2').closest('.row').hide();
            toastr.error("দুঃখিত! পত্রে অনুলিপি দেয়া যাবে না!");
            return false;
        }
    }
}
function setReceiverInfo(receivers) {

    if (receivers.length == 0) {
        $('input[name=receiver_group_id_final]').val('');
        $('input[name=receiver_group_name_final]').val('');
        $('input[name=receiver_group_member_final]').val(0);
        $('input[name=receiver_group_designation_final]').val('');
        $('input[name=receiver_office_id_final]').val('');
        $('input[name=receiver_office_name_final]').val('');
        $('input[name=receiver_officer_designation_id_final]').val('');
        $('input[name=receiver_officer_designation_label_final]').val('');
        $('input[name=receiver_office_unit_id_final]').val('');
        $('input[name=receiver_office_unit_name_final]').val('');
        $('input[name=receiver_officer_id_final]').val('');
        $('input[name=receiver_officer_name_final]').val('');
        $('input[name=receiver_officer_email_final]').val('');
        $('input[name=receiver_office_head_final]').val('');
        $('input[name=receiver_visibleName_final]').val('');
        $('input[name=receiver_officer_mobile_final]').val('');
        $('input[name=receiver_sms_message_final]').val('');
        $('input[name=receiver_options_final]').val('');
    } else {
        if ($('#potrojariDraftForm').find('#office_organogram_id').length > 0) {
                $('#office_organogram_id').html("");
                $.each(receivers, function (i, data) {
                    var bn = replaceNumbers("'" + (i + 1) + "'");
                    bn = bn.replace("'", '');
                    bn = bn.replace("'", '');
                    if (typeof ($(this).attr('group_id')) == 'undefined') {
                        if (!isEmpty($(this).attr('visibleName'))) {
                            var visibleName = $(this).attr('visibleName');
                             visibleName =visibleName.replace(/<br\/>/g, '\n');
                            $('input[name=receiver_visibleName_final]').val(visibleName);
                        } else {
                            if(!isEmpty($(this).attr('user_type')) && $(this).attr('user_type') == 1 ){
                                // internal receiver
                                if(!isEmpty($(this).attr('office_selection'))){
                                    // internal receiver office selection
                                  var visibleName =  setUserInfoInTemplateForInternalOfficeSelection(this,1);
                                }else{
                                    // internal receiver direct selection 
                                   var visibleName =  setUserInfoInTemplateForInternalDirectSelection(this,1);
                                }
                            }else{
                                    // external receiver
                                if(!isEmpty($(this).attr('office_selection'))){
                                    // external receiver office selection
                                  var visibleName =  setUserInfoInTemplateForExternalOfficeSelection(this,1);
                                }else{
                                    // external receiver direct selection 
                                   var visibleName =  setUserInfoInTemplateForExternalDirectSelection(this,1);
                                }
                            }
                        }
                         if(!isEmpty($(this).attr('user_type')) && $(this).attr('user_type') == 1 ){
                            totalOntosthoReceiver += '<a nong='+i+' data-type="textarea" data-pk=1 href="javascript:;" class="editable editable-click to_list to_div_item"  >' + visibleName +'</a><br/><br/>';
//                            $('#office_organogram_id').append((i + 1) + ') <a data-type="text" id="to_div_item" data-pk=1 href="javascript:;" class="editable editable-click to_list " >' + visibleName + "</a><br/>");
                        } else {
                            totalBohisthoReceiver += '<a nong='+i+' data-type="textarea" data-pk=1 href="javascript:;" class="editable editable-click to_list to_div_item">' + visibleName + "</a><br/><br/>";
//                            $('#office_organogram_id').append(bn + ') <a data-type="text" id="to_div_item" data-pk=1 href="javascript:;" class="editable editable-click to_list " >' + visibleName + "</a><br/>");
                        }

                    }
                });
        } else if ($('[name=potro_type]').val() == 13) {

        } else {
            $('input[name=receiver_group_id_final]').val('');
            $('input[name=receiver_group_name_final]').val('');
            $('input[name=receiver_group_member_final]').val(0);
            $('input[name=receiver_group_designation_final]').val('');
            $('input[name=receiver_office_id_final]').val('');
            $('input[name=receiver_office_name_final]').val('');
            $('input[name=receiver_officer_designation_id_final]').val('');
            $('input[name=receiver_officer_designation_label_final]').val('');
            $('input[name=receiver_office_unit_id_final]').val('');
            $('input[name=receiver_office_unit_name_final]').val('');
            $('input[name=receiver_officer_id_final]').val('');
            $('input[name=receiver_officer_name_final]').val('');
            $('input[name=receiver_officer_email_final]').val('');
            $('input[name=receiver_office_head_final]').val('');
            $('input[name=receiver_visibleName_final]').val('');
            $('input[name=receiver_officer_mobile_final]').val('');
            $('input[name=receiver_sms_message_final]').val('');
            $('input[name=receiver_options_final]').val('');
            toastr.error("দুঃখিত! পত্রে প্রাপক দেয়া যাবে না!");
            return false;
        }
    }
}
function setApprovalInfo(approvals) {
    if (approvals.length == 0) {

    } else {

        $('#potrojariDraftForm').find('#sender_name2').closest('.row').show();
        $('#potrojariDraftForm').find('#sender_designation2').closest('.row').show();
        $('#potrojariDraftForm').find('#sender_signature2').closest('.row').show();

        $.each($('.approval_users'), function (i, data) {
            if (i > 0) {
                toastr.error("একাধিক পদবি দেয়া যাবে না। ");
                return false;
            }
            if (i > 0) {
                $('#sender_designation2').text(', ');
                $('#sender_name2').text(', ');
                $('#sender_designation3').text(', ');
                $('#sender_name3').text(', ');
            }

            var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
            if (sendename.length > 0) {
            } else {
                sendename[0] = sendename;
            }
            if(!isEmpty($(this).attr('visibleName')) && $.trim($(this).attr('visibleName')) !='...'){
                var visibleName = $.trim($(this).attr('visibleName'));
                $('input[name=approval_visibleName]').val(visibleName);
            }else{
                var visibleName  = $.trim(sendename[0]);
            }
            if(!isEmpty($(this).attr('visibleDesignation')) && $.trim($(this).attr('visibleDesignation')) !='...'){
                var visibleDesignation = $.trim($(this).attr('visibleDesignation'));
                $('input[name=approval_visibleDesignation]').val(visibleDesignation);
            }else{
                var visibleDesignation  = $.trim($(this).attr('designation'));
            }
            $('#sender_designation2').text(visibleDesignation);
            $('#sender_name2').text(visibleName);
            $('#sender_designation3').text(visibleDesignation);
            $('#sender_name3').text(visibleName);

            if($('[name=potro_type]').val() == 17){

                $('#sender_designation').text(visibleDesignation);
                $('#sender_name').text(visibleName);
            }
        });
    }
}
function setSenderInfo(senders) {
    if (senders.length != 0) {
        $('#sender_name').text('');
        $.each($('.sender_users'), function (i, data) {
            if (i > 0) {
                toastr.error("একাধিক পদবি দেয়া যাবে না। ");
                return false;
            }
            var sendename = $(this).text().split(', ' + $(this).attr('unit_name'));
           if(!isEmpty($(this).attr('visibleName')) && $.trim($(this).attr('visibleName')) !='...'){
                var visibleName =$.trim($(this).attr('visibleName'));
                $('input[name=sender_officer_visibleName]').val(visibleName);
            }else{
                var visibleName  = $.trim(sendename[0]);
            }
            if(!isEmpty($(this).attr('visibleDesignation')) && $.trim($(this).attr('visibleDesignation')) !='...'){
                var visibleDesignation = $.trim($(this).attr('visibleDesignation'));
                $('input[name=sender_officer_visibleDesignation]').val(visibleDesignation);
            }else{
                var visibleDesignation  = $.trim($(this).attr('designation'));
            }
            $('#sender_designation').text(visibleDesignation);
            $('#sender_name').text(visibleName);

            if ($('[name=potro_type]').val() == 21 || $('[name=potro_type]').val() == 27) {
                $('#ministry_name_noc').text($(this).attr('unit_name'));
                check_attachment();
            }
            if ($('[name=potro_type]').val() == 21) {
                $('#sender_unit').text($(this).attr('unit_name'));
            }
            //$('#unit_name_editable').text(($('#unit_name_editable').text().length == 0 || $('#unit_name_editable').text() == '...') ? $('.sender_users').eq(0).attr('unit_name') : $('#unit_name_editable').text());
        });
        if (!isEmpty($('.sender_users').eq(0).attr('officer_id'))) {
            $.ajax({
                url: js_wb_root + 'EmployeeRecords/userDetails/' + $('.sender_users').eq(0).attr('officer_id'),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    if (data.personal_email.length > 0) {
                        $('#sender_email').text($.trim(data.personal_email));
                    }
                    if (data.personal_mobile.length > 0) {
                        $('#sender_phone').text((potrojari_language == 'eng') ? EngFromBn(data.personal_mobile) : data.personal_mobile);
                    }
                    if (typeof (data.fax) != 'undefined') {
                        $('#sender_fax').text((potrojari_language == 'eng') ? EngFromBn(data.fax) : data.fax);
                    }
                }
            });
        }
    }

}
$('.savethis').off('click');
$('.savethis').on('click', function () {
    var attr = $(this).attr('refid');
    var prefix = $(this).attr('prefix');
    var style2apply = '';
    
    if (isEmpty($('#potro-type').val()) && isEmpty($('input[name=potro_type]').val())) {
        toastr.error("দুঃখিত! পত্রের ধরন বাছাই করা হয়নি");
        return false;
    }

    if ($("#potro-type").val() != 13) {
        if (prefix == 'onulipi_') {
            if ($('#potrojariDraftForm').find('#cc_list_div').length > 0) {
            } else {
                toastr.error("দুঃখিত! পত্রে অনুলিপি দেয়া যাবে না!");
                return false;
            }
        }
        if (prefix == 'receiver_') {
            if ($('#potrojariDraftForm').find('#office_organogram_id').length > 0) {
            } else {
                toastr.error("দুঃখিত! পত্রে প্রাপক দেয়া যাবে না!");
                return false;
            }
        }
    }
    if (attr == 'tab_autosearch') {
        var officerid = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_id]').val();
        var officername = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_name]').val();
        var officerhead = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_head]').val();
        var officerofcid = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_id_auto]').val();
        var officerofc = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_name_auto]').val();
        var officerorgid = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_designation_id_auto]').val();
        var officerorg = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_designation_label_auto]').val();
        var officerunitid = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_unit_id_auto]').val();
        var officerunit = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_unit_name_auto]').val();
        var personal_mobile = $(this).closest('.tab-pane').find('input[name=' + prefix + 'personal_mobile]').val();

        var user_type = '';
        var isAttention = '';
        var office_address = '';
        if (prefix == 'receiver_' || prefix == 'onulipi_') {
            var res = checkUserSettings(prefix, attr, officerofcid);
           $.ajax({
                    type: 'POST',
                    async:false,
                    url: js_wb_root + 'officeSettings/getOfficeAddress',
                    data: {'office_id': officerofcid},
                    success: function(res){
                        if (res.status == 'success') {
                            office_address = res.data;
                        }
                    }
            });
            if (res.status == 'success') {
                user_type = res.user_type;
                isAttention = res.isAttention;
                if(user_type == 1){
                    style2apply += 'color:#2196f3';
                }else{
                    style2apply += 'color:#009688';
                }
            } else {
                return;
            }
        }

        var hasEnglish = searchForEnglish($(this), prefix);
        if (!isEmpty(hasEnglish['officername'])) {
            officername = hasEnglish['officername'];
        }
        if (!isEmpty(hasEnglish['officerofc'])) {
            officerofc = hasEnglish['officerofc'];
        }
        if (!isEmpty(hasEnglish['officerunit'])) {
            officerunit = hasEnglish['officerunit'];
        }
        if (!isEmpty(hasEnglish['officerorg'])) {
            officerorg = hasEnglish['officerorg'];
        }
        if (officerofcid.length == 0 || officerid.length == 0 || officerorgid.length == 0) {
            toastr.error("দুঃখিত! তথ্য সঠিক নয়");
            return false;
        } else {
            if ($(document).find('a.receiver_users[designation_id=' + officerorgid + ']').length >= 1 && prefix != 'attension_') {
                toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                return false;
            } else if ($(document).find('a.onulipi_users[designation_id=' + officerorgid + ']').length >= 1 && prefix != 'attension_') {
                toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                return false;
            }
            if (prefix == 'attension_') {
                attensionAdd(officerid, officername, officerorgid, officerorg, officerunitid, officerunit, officerofcid, officerofc);
            }
            $('.' + prefix + 'list').find('.sidebar-tags').append('<li><a style="'+style2apply+'" class="' + prefix + 'users"  href="javascript:;" data-toggle="tooltip" title="' + officerorg + '" data-title="' + officerorg + '" designation="' + officerorg + '" designation_id="' + officerorgid + '" group_member=1 unit_name="' + officerunit + '" unit_id ="' + officerunitid + '" ofc_name="' + officerofc + '" ofc_id="' + officerofcid + '" officer_id="' + officerid + '" office_head="' + officerhead + '" officer_name="' + officername + '" user_type="' + user_type + '" is_attention="' + isAttention + '" office_address="' + office_address + '" personal_mobile ="'+personal_mobile+'">' + officername + ', ' + officerunit + ', ' + officerofc + ' <i class="fs1 a2i_gn_close2 remove_list ' + prefix + 'remove"></i></a></li>');
            $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
//                                        $(this).closest('.open').find('.dropdown-toggle').click();
            $('.' + prefix + 'list').find('input[type=text]').val('');
        }

    } else if (attr == 'tab_dropdown') {
        var officerid = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_id]').val();
        var officername = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_name]').val();
        var officerhead = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_head]').val();
        var officerofcid = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_id] option:selected').val();
        var officerofc = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_id] option:selected').text();
        var officerorgid = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_unit_organogram_id] option:selected').val();
        var officerorg = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_unit_organogram_id] option:selected').text() + ($(this).closest('.tab-pane').find('input[name=' + prefix + 'incharge_label]').val().length > 0 ? (" (" + $(this).closest('.tab-pane').find('input[name=' + prefix + 'incharge_label]').val() + ")") : "");
        var officerunitid = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_unit_id] option:selected').val();
        var officerunit = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_unit_id] option:selected').text();
        var personal_mobile = $(this).closest('.tab-pane').find('input[name=' + prefix + 'personal_mobile]').val();

        var user_type = '';
        var isAttention = '';
        var office_address = '';
        if (prefix == 'receiver_' || prefix == 'onulipi_') {
            var res = checkUserSettings(prefix, attr, officerofcid);
             $.ajax({
                    type: 'POST',
                    async:false,
                    url: js_wb_root + 'officeSettings/getOfficeAddress',
                    data: {'office_id': officerofcid},
                    success: function(res){
                        if (res.status == 'success') {
                            office_address = res.data;
                        }
                    }
            });
            if (res.status == 'success') {
                user_type = res.user_type;
                isAttention = res.isAttention;
                 if(user_type == 1){
                    style2apply += 'color:#2196f3';
                }else{
                    style2apply += 'color:#009688';
                }
            } else {
                return;
            }
        }

        var hasEnglish = searchForEnglish($(this), prefix);
        if (!isEmpty(hasEnglish['officername'])) {
            officername = hasEnglish['officername'];
        }
        if (!isEmpty(hasEnglish['officerofc'])) {
            officerofc = hasEnglish['officerofc'];
        }
        if (!isEmpty(hasEnglish['officerunit'])) {
            officerunit = hasEnglish['officerunit'];
        }
        if (!isEmpty(hasEnglish['officerorg'])) {
            officerorg = hasEnglish['officerorg'];
        }

        if (officerorgid.length == 0 || officerid.length == 0 || officerofcid.length == 0) {
            toastr.error("দুঃখিত! তথ্য সঠিক নয়");
            return false;
        } else {
            if ($(document).find('a.receiver_users[designation_id=' + officerorgid + ']').length >= 1 && prefix != 'attension_') {
                toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                return false;
            } else if ($(document).find('a.onulipi_users[designation_id=' + officerorgid + ']').length >= 1 && prefix != 'attension_') {
                toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                return false;
            }
            if (prefix == 'attension_') {
                attensionAdd(officerid, officername, officerorgid, officerorg, officerunitid, officerunit, officerofcid, officerofc);
            }
            $('.' + prefix + 'list').find('.sidebar-tags').append('<li><a  style="'+style2apply+'" class="' + prefix + 'users" href="javascript:;" data-toggle="tooltip" title="' + officerorg + '" data-title="' + officerorg + '" designation="' + officerorg + '" designation_id="' + officerorgid + '" unit_name="' + officerunit + '" group_member=1 unit_id ="' + officerunitid + '" ofc_name="' + officerofc + '" ofc_id="' + officerofcid + '" officer_id="' + officerid + '"  office_head="' + officerhead + '" officer_name="' + officername + '" user_type="' + user_type + '" is_attention="' + isAttention + '"  office_address="' + office_address + '" personal_mobile="'+personal_mobile+'">' + officername + ', ' + officerunit + ', ' + officerofc + ' <i class="fs1 a2i_gn_close2 remove_list ' + prefix + 'remove"></i></a></li>');

            $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
//                                        $(this).closest('.open').find('.dropdown-toggle').click();
            $('.' + prefix + 'list').find('input[type=text]').val('');
        }

    } else if (attr == 'tab_newoffice') {
        var officername = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_name]').val();
        var officerofc = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_name]').val();
        var officerorg = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_designation_label]').val();
        var officerunit = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_unit_name]').val();
        var officeremail = typeof ($(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_email]').val() != 'undefined') ? $.trim($(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_email]').val()) : '';
        // sms to user
        var officer_mobile = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_mobile]').val();
        var sms_message = $(this).closest('.tab-pane').find('textarea[name=' + prefix + 'sms_message]').val();
        if(!isEmpty(officer_mobile)){
            var mobile_numbers_array = officer_mobile.split(',');
            officer_mobile ='';
            var count_mobile_number = 0;
            for(index_of_array in  mobile_numbers_array){
               var mobile_number = EngFromBn($.trim(mobile_numbers_array[index_of_array]));
                if(mobile_number.length != 11 && mobile_number.length != 13  && mobile_number.length != 14){
                    toastr.error("দুঃখিত! মোবাইল নম্বর "+mobile_number+" সঠিক নয়");
                    return false;
                }
                if(!/[0-9]{11}|[0-9]{13}|\+[0-9]{13}/.test(mobile_number)){
                    toastr.error("দুঃখিত! মোবাইল নম্বর "+mobile_number+" সঠিক নয়");
                    return false;
                }
                officer_mobile +=mobile_number+'--';
                count_mobile_number++;
                if(count_mobile_number > 5){
                    toastr.error("দুঃখিত! মোবাইল নম্বর ৫টির বেশি গ্রহণযোগ্য নয়।");
                    return false;
                }
            }
        }
        if (officeremail.length > 0) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(officeremail)) {

            } else {
                toastr.error("দুঃখিত! ইমেইল সঠিক নয়");
                return false;
            }
        }
        var user_type = '';
        var isAttention = '';
        var office_address = '';
        if (prefix == 'receiver_' || prefix == 'onulipi_') {
            var res = checkUserSettings(prefix, attr);
            if (res.status == 'success') {
                user_type = res.user_type;
                isAttention = res.isAttention;
                office_address = '';
                 if(user_type == 1){
                    style2apply += 'color:#2196f3';
                }else{
                    style2apply += 'color:#009688';
                }
            } else {
                return;
            }
        }

        if (officerorg.length == 0 || officerofc.length == 0) {
            toastr.error("দুঃখিত! তথ্য সঠিক নয়");
            return false;
        } else {
            $('.' + prefix + 'list').find('.sidebar-tags').append('<li><a  style="'+style2apply+'" class="' + prefix + 'users" href="javascript:;" data-toggle="tooltip" title="' + officerorg + '" data-title="' + officerorg + '" designation="' + officerorg + '" designation_id="0" officer_email="' + officeremail + '" unit_name="' + officerunit + '" unit_id ="0" ofc_name="' + officerofc + '" ofc_id="0" officer_id="0" officer_name="' + officername + '" officer_mobile="' + officer_mobile + '" sms_message="' + sms_message + '" user_type="' + user_type + '" is_attention="' + isAttention + '"  office_address="' + office_address + '" personal_mobile = "'+officer_mobile+'">' + (officername.length > 0 ? officername : officerorg) + (officerunit != 0 ? (', ' + officerunit) : '') + ' <i class="fs1 a2i_gn_close2 remove_list"></i></a></li>');
            $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
            $('.' + prefix + 'list').find('input[type=text]').val('');
            $('.' + prefix + 'list').find('textarea').val('');
        }
    } else if (attr == 'tab_groupuser') {
        if ($('.' + prefix + 'group_checkbox:checked').length == 0) {
            if (prefix == 'receiver_') {
                toastr.error('আপনি কোনো প্রাপক সিলেক্ট করেন নি');
                return false;
            } else {
                toastr.error('আপনি কোনো অনুলিপি সিলেক্ট করেন নি');
                return false;
            }
        }
        var that = this;
        if ($('#' + prefix + 'SeperateUserNameForGroup').is(':checked')) {
            addSeperateUserFromGroup(prefix, that);
        } else {
            var promise = new Promise(function (resolve, reject) {
                var groupname = $(that).closest('.tab-pane').find('select[name=' + prefix + 'group_id]').find('option:selected').text();
                var groupid = $(that).closest('.tab-pane').find('select[name=' + prefix + 'group_id]').val();
                var groupmem = $(that).closest('.tab-pane').find('.' + prefix + 'group_checkbox:checked').length;
                var group_designation = "";
                if (groupid == 0 || typeof (groupid) == 'undefined') {
                    reject(Error("দুঃখিত! তথ্য সঠিক নয়"));
                } else {
                    $.each($('.' + prefix + 'group_checkbox'), function (i, v) {
                        if ($(this).is(':checked') == true) {
                            group_designation += ($(this).data('office-unit-organogram-id') == 0 ? $.trim($(this).data('officer-email')) : $(this).data('office-unit-organogram-id')) + "~";
                        }
                    });
                    $('.' + prefix + 'list').find('.sidebar-tags').append('<li><a class="' + prefix + 'users" href="javascript:;" data-toggle="tooltip" title="' + groupname + '" data-title="' + groupname + '" group_name="' + groupname + '" group_id="' + groupid + '" group_member="' + groupmem + '" designation="" designation_id="0" officer_email="" unit_name="" unit_id ="0" ofc_name="" ofc_id="0" officer_id="0" officer_name="" group_designation="' + group_designation + '">' + groupname + ' <i class="fs1 a2i_gn_close2 remove_list"></i></a></li>');
                    resolve(1);
                }
            }).then(function (res) {
                $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
                $('.' + prefix + 'list').find('select[name=' + prefix + 'group_id]').val(0);
                $('.' + prefix + 'group_list').find('.list-group').html('');
            }).catch(function (err) {
                toastr.error(err);
                return false;
            });
        }
    } else if (attr == 'tab_office_show') {
        var prefixMain = prefix;
        var prefix = 'officeselection' + prefix;
        var officerid = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_id]').val();
        var officername = $(this).closest('.tab-pane').find('input[name=' + prefix + 'officer_name]').val();
        var officerhead = $(this).closest('.tab-pane').find('input[name=' + prefix + 'office_head]').val();
        var officerofcid = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_id] option:selected').val();
        var officerofc = $(this).closest('.tab-pane').find('select[name=' + prefix + 'office_id] option:selected').text();
        var officerorgid = $(this).closest('.tab-pane').find('input[name=' + prefix + 'designation_id]').val();
        var officerorg = $(this).closest('.tab-pane').find('input[name=' + prefix + 'designation_name_bng]').val() + ($(this).closest('.tab-pane').find('input[name=' + prefix + 'incharge_label]').val().length > 0 ? (" (" + $(this).closest('.tab-pane').find('input[name=' + prefix + 'incharge_label]').val() + ")") : "");
        var officerunitid = $(this).closest('.tab-pane').find('input[name=' + prefix + 'unit_id]').val();
        var officerunit = $(this).closest('.tab-pane').find('input[name=' + prefix + 'unit_name_bng]').val();
        var personal_mobile = $(this).closest('.tab-pane').find('input[name=' + prefix + 'personal_mobile]').val();

        var user_type = '';
        var isAttention = '';
        var office_address = ''
        if (prefixMain == 'receiver_' || prefixMain == 'onulipi_') {
            var res = checkUserSettings(prefixMain, attr, officerofcid);
            $.ajax({
                    type: 'POST',
                    async:false,
                    url: js_wb_root + 'officeSettings/getOfficeAddress',
                    data: {'office_id': officerofcid},
                    success: function(res){
                        if (res.status == 'success') {
                            office_address = res.data;
                        }
                    }
            });
            if (res.status == 'success') {
                user_type = res.user_type;
                isAttention = res.isAttention;
                 if(user_type == 1){
                    style2apply += 'color:#2196f3';
                }else{
                    style2apply += 'color:#009688';
                }
            } else {
                return;
            }
        }

        var hasEnglish = searchForEnglish($(this), prefix);
        if (!isEmpty(hasEnglish['officername'])) {
            officername = hasEnglish['officername'];
        }
        if (!isEmpty(hasEnglish['officerofc'])) {
            officerofc = hasEnglish['officerofc'];
        }
        if (!isEmpty(hasEnglish['officerunit'])) {
            officerunit = hasEnglish['officerunit'];
        }
        if (!isEmpty(hasEnglish['officerorg'])) {
            officerorg = hasEnglish['officerorg'];
        }
        if (officerorgid.length == 0 || officerid.length == 0 || officerofcid.length == 0) {
            toastr.error("দুঃখিত! তথ্য সঠিক নয়");
            return false;
        } else {
            if ($(document).find('a.receiver_users[designation_id=' + officerorgid + ']').length >= 1 && prefixMain != 'attension_') {
                toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                return false;
            } else if ($(document).find('a.onulipi_users[designation_id=' + officerorgid + ']').length >= 1 && prefixMain != 'attension_') {
                toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                return false;
            }
            if (prefixMain == 'attension_') {
                attensionAdd(officerid, officername, officerorgid, officerorg, officerunitid, officerunit, officerofcid, officerofc);
            }
            $('.' + prefixMain + 'list').find('.sidebar-tags').append('<li><a  style="'+style2apply+'" class="' + prefixMain + 'users" href="javascript:;" data-toggle="tooltip" title="' + officerorg + '" data-title="' + officerorg + '" designation="' + officerorg + '" designation_id="' + officerorgid + '" unit_name="' + officerunit + '" group_member=1 unit_id ="' + officerunitid + '" ofc_name="' + officerofc + '" ofc_id="' + officerofcid + '" officer_id="' + officerid + '"  office_head="' + officerhead + '" officer_name="' + officername + '" user_type="' + user_type + '" is_attention="' + isAttention + '" office_address="' + office_address + '" office_selection="' + 1 + '" personal_mobile = "'+personal_mobile+'">' + officername + ', ' + officerunit + ', ' + officerofc + ' <i class="fs1 a2i_gn_close2 remove_list ' + prefixMain + 'remove"></i></a></li>');

            $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
//            $(this).closest('.open').find('.dropdown-toggle').click();
            $('.' + prefixMain + 'list').find('input[type=text]').val('');
        }
    }
    setInformation();
    if (prefix == 'sender_' || prefix == 'approval_' || prefix == 'sovapoti_' || prefix == 'attension_') {
        $(this).next('.closethis').click();
    }
});

function searchForEnglish(that, prefix) {
    var error_msg = '';
    var result = [];
    if (potrojari_language == 'eng') {
        if (!isEmpty(that.closest('.tab-pane').find('input[name=' + prefix + 'name_eng]').val())) {
            result['officername'] = that.closest('.tab-pane').find('input[name=' + prefix + 'name_eng]').val();
        } else {
            error_msg = 'অফিসারের নাম';
        }
        if (!isEmpty(that.closest('.tab-pane').find('input[name=' + prefix + 'designation_name_eng]').val())) {
            result['officerorg'] = that.closest('.tab-pane').find('input[name=' + prefix + 'designation_name_eng]').val();
            if (!isEmpty(that.closest('.tab-pane').find('input[name=' + prefix + 'incharge_label_eng]').val())) {
                result['officerorg'] += (that.closest('.tab-pane').find('input[name=' + prefix + 'incharge_label_eng]').val().length > 0 ? (" (" + that.closest('.tab-pane').find('input[name=' + prefix + 'incharge_label_eng]').val() + ")") : "");
            }
        } else {
            if (!isEmpty(error_msg)) {
                error_msg += ', পদবির নাম';
            } else {
                error_msg = 'পদবির নাম';
            }
        }
        if (!isEmpty(that.closest('.tab-pane').find('input[name=' + prefix + 'unit_name_eng]').val())) {
            result['officerunit'] = that.closest('.tab-pane').find('input[name=' + prefix + 'unit_name_eng]').val();
        } else {
            if (!isEmpty(error_msg)) {
                error_msg += ', শাখার নাম';
            } else {
                error_msg = 'শাখার নাম';
            }
        }
        if (!isEmpty(that.closest('.tab-pane').find('input[name=' + prefix + 'office_name_eng]').val())) {
            result['officerofc'] = that.closest('.tab-pane').find('input[name=' + prefix + 'office_name_eng]').val();
        } else {
            if (!isEmpty(error_msg)) {
                error_msg += ', অফিসের নাম';
            } else {
                error_msg = 'অফিসের নাম';
            }
        }
        if (!isEmpty(error_msg)) {
            toastr.error('ব্যবহারকারীর যেসব তথ্য ইংরেজিতে পাওয়া যায়নিঃ ' + error_msg, {timeOut: 2000});
        }
    }
    return result;
}

function addSeperateUserFromGroup(prefix, that) {
    new Promise(function (resolve, reject) {
        var groupid = $(that).closest('.tab-pane').find('select[name=' + prefix + 'group_id]').val();
        if (groupid == 0 || typeof (groupid) == 'undefined') {
            reject(Error("দুঃখিত! তথ্য সঠিক নয়"));
        } else {
            $.each($('.' + prefix + 'group_checkbox'), function (i, v) {

                var officerid = $(this).data('office-employee-id');
                var officername = $(this).data('office-employee-name-bng');
                var officerhead = (!isEmpty($(this).data('office-head')) ? $(this).data('office-head') : 0);
                var officerofcid = $(this).data('office-id');
                var officerofc = $(this).data('office-name-bng');
                var officerorgid = $(this).data('office-unit-organogram-id');
                var officerorg = $(this).data('designation-name-bng');
                var officerunitid = $(this).data('office-unit-id');
                var officerunit = $(this).data('unit-name-bng');
                if (potrojari_language == 'eng') {
                    if (!isEmpty($(this).data('office-employee-name-eng'))) {
                        officername = $(this).data('office-employee-name-eng');
                    }
                    if (!isEmpty($(this).data('office-name-eng'))) {
                        officerofc = $(this).data('office-name-eng');
                    }
                    if (!isEmpty($(this).data('designation-name-eng'))) {
                        officerorg = $(this).data('designation-name-eng');
                    }
                    if (!isEmpty($(this).data('unit-name-eng'))) {
                        officerunit = $(this).data('unit-name-eng');
                    }
                }


                if ($(this).is(':checked') == true) {
                    if (typeof (officerorgid) == 'undefined' || officerorgid == '') {
                        // For Other user outside Nothi
                        var officeremail = $(this).data('officer-email');
                        // For Other user outside Nothi
                        $('.' + prefix + 'list').find('.sidebar-tags').append('<li><a class="' + prefix + 'users" href="javascript:;" data-toggle="tooltip" title="' + officerorg + '" data-title="' + officerorg + '" designation="' + officerorg + '" designation_id="0" officer_email="' + officeremail + '" group_member=1 unit_name="' + officerunit + '" unit_id ="0" ofc_name="' + officerofc + '" ofc_id="0" officer_id="0" officer_name="' + officername + '">' + (officername.length > 0 ? officername : officerorg) + (officerunit != 0 ? (', ' + officerunit + ', ' + officerofc) : '') + ' <i class="fs1 a2i_gn_close2 remove_list"></i></a></li>');
                    } else {
                        if ($(document).find('a.receiver_users[designation_id=' + officerorgid + ']').length >= 1 && prefix != 'attension_') {
                            toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                            return;
                        } else if ($(document).find('a.onulipi_users[designation_id=' + officerorgid + ']').length >= 1 && prefix != 'attension_') {
                            toastr.error("দুঃখিত! পূর্বে নির্বাচিত করা হয়েছে");
                            return;
                        }
                        $('.' + prefix + 'list').find('.sidebar-tags').append('<li><a class="' + prefix + 'users" href="javascript:;" data-toggle="tooltip" title="' + officerorg + '" data-title="' + officerorg + '" designation="' + officerorg + '" designation_id="' + officerorgid + '" unit_name="' + officerunit + '" group_member=1 unit_id ="' + officerunitid + '" ofc_name="' + officerofc + '" ofc_id="' + officerofcid + '" officer_id="' + officerid + '"  office_head="' + officerhead + '" officer_name="' + officername + '">' + officername + ', ' + officerunit + ', ' + officerofc + ' <i class="fs1 a2i_gn_close2 remove_list"></i></a></li>');
                    }
                }
            });

            resolve(1);
        }
    }).then(function (res) {
        $('[data-toggle="tooltip"]').tooltip({'placement':'bottom'});
        $('.' + prefix + 'list').find('select[name=' + prefix + 'group_id]').val(0);
        $('.' + prefix + 'group_list').find('.list-group').html('');
        setInformation();
    }).catch(function (err) {
        toastr.error(err);
        return false;
    });
}
function attensionAdd(officerid, officername, officerorgid, officerorg, officerunitid, officerunit, officerofcid, officerofc) {
    if ($("#attention").length == 0) {
        var attension_text = (($("#potro_language").val() == 'eng') ? 'Attention:' : 'দৃষ্টি আকর্ষণঃ');
        var html = '<div class="row"><div class="col-md-10 col-xs-10 col-sm-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 " style="font-size:13pt" id="attention"><b>' + attension_text + ' </b> <a class="editable editable-click" id="attentionname" data-type="text" data-pk="1">' + officerorg + ', ' + officerunit + ', ' + officerofc + '</a></div></div><br>';
        $("#sharok_no2").closest('.row').before(html);
        $('#attentionname').editable();
        $('input[name=attension_office_id_final]').val(officerofcid);
        $('input[name=attension_officer_id_final]').val(officerid);
        $('input[name=attension_office_unit_id_final]').val(officerunitid);
        $('input[name=attension_officer_designation_id_final]').val(officerorgid);
        $('input[name=attension_officer_designation_label_final]').val(officerorg);
        $('input[name=attension_officer_name_final]').val(officername);
        $('input[name=attension_office_name_final]').val(officerofc);
        $('input[name=attension_office_unit_name_final]').val(officerunit);
    } else {
        toastr.error("একাধিক পদবি দেয়া যাবে না। ");
        return false;
    }
}
function checkAttension() {
    if ($("#attention").length == 0 && !isEmpty($('input[name=attension_officer_designation_id_final]').val()) && $(".attension_users").length > 0) {
        var officerorg = $('input[name=attension_officer_designation_label_final]').val();
        var officerofc = $('input[name=attension_office_name_final]').val();
        var officerunit = $('input[name=attension_office_unit_name_final]').val();
        var attension_text = (($("#potro_language").val() == 'eng') ? 'Attension:' : 'দৃষ্টি আকর্ষণঃ');
        var html = '<div class="row"><div class="col-md-10 col-xs-10 col-sm-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 " style="font-size:13pt" id="attention"><b>' + attension_text + ' </b> <a class="editable editable-click" id="attentionname" data-type="text" data-pk="1">' + officerorg + ', ' + officerunit + ', ' + officerofc + '</a></div></div><br>';
        $("#sharok_no2").closest('.row').before(html);
        $('#attentionname').editable();
    } else if ($(".attension_users").length == 0) {
        $('input[name=attension_office_id_final]').val('');
        $('input[name=attension_officer_id_final]').val('');
        $('input[name=attension_office_unit_id_final]').val('');
        $('input[name=attension_officer_designation_id_final]').val('');
        $('input[name=attension_officer_designation_label_final]').val('');
        $('input[name=attension_officer_name_final]').val('');
        $('input[name=attension_office_name_final]').val('');
        $('input[name=attension_office_unit_name_final]').val('');
        $("#attention").remove();
    }
}
function checkBitoronJoggo() {
    if ($('[name=potro_type]').val() == 1) {
        if ($('#potrojariDraftForm').find('#office_organogram_id a').length > 1) {
            // console.log($("#bitoron").text());
            var html = '<div id="bitoron"><a class="editable editable-click" id="bitorontext" data-type="text" data-pk="1">' + (!isEmpty($("#bitoron").text()) ? $("#bitoron").text() : ' বিতরণ :') + '</a> </div>';
            $("#bitoron").remove();
            $("#office_organogram_id").before(html);
            $('#potrojariDraftForm').find('#bitorontext').editable();
        } else {
            $("#bitoron").remove();
        }
    }
}
function checkUserSettings(prefix, attr) {
    var response = {'status': 'error'};
    var office_address = '';
     var isAttention = '';
     var user_type = '';
     if (prefix == 'receiver_' || prefix == 'onulipi_') {
                user_type = $('#' + prefix + attr).closest('.tab-pane').find('input[name=' + prefix + 'internal_selection' + attr + ']').closest('.checked').find('input[name=' + prefix + 'internal_selection' + attr + ']').val();
                if (isEmpty(user_type)) {
                    toastr.error(((prefix == 'receiver_') ? 'প্রাপকের' : 'অনুলিপির ' )+ ' ধরন বাছাই করা হয়নি।');
                    return response;
                }else{
                    isAttention = ($('#' + prefix + attr).closest('.tab-pane').find('input[name=' + prefix + 'attention_selection' + attr + ']').closest('span').hasClass('checked') == true) ? 1 : 0;
                    response = {'status': 'success', 'user_type': user_type, 'isAttention': isAttention, 'office_address': office_address};
                    return response;
                }
        }
        return response;
}

function setUserInfoInTemplateForExternalOfficeSelection(that){
        var visibleName = (( !isEmpty($(that).attr('ofc_name')) && $(that).attr('ofc_name').length > 0 ? ($(that).attr('ofc_name') + "\n") : "") + ((!isEmpty($(that).attr('office_address')) && $(that).attr('office_address').length > 0 ) ? ( $(that).attr('office_address')+ "\n") : ''));
        if(!isEmpty($(that).attr('is_attention'))){
            visibleName +='(দৃষ্টি আকর্ষণঃ '+(( (!isEmpty($(that).attr('officer_name')) && $(that).attr('officer_name').length > 0) ? ($(that).attr('officer_name')) : "") + (( !isEmpty($(that).attr('designation')) && $(that).attr('designation').length > 0) ? ( ', ' + $(that).attr('designation')) : '')+ (( !isEmpty($(that).attr('ofc_name')) && $(that).attr('ofc_name').length > 0) ? ( ', ' + $(that).attr('ofc_name')) : '') + (( !isEmpty($(that).attr('personal_mobile')) && $(that).attr('personal_mobile').length > 0) ? ( ', মোবাইল নং - ' + $(that).attr('personal_mobile')) : ''))+')';
        }
   return visibleName;
}
function setUserInfoInTemplateForExternalDirectSelection(that){
        var visibleName = (( (!isEmpty($(that).attr('designation')) && $(that).attr('designation').length > 0) ? ($(that).attr('designation') + ", ") : "") +( (!isEmpty($(that).attr('unit_name')) && $(that).attr('unit_name').length > 0) ? ($(that).attr('unit_name') + ", ") : "") +( (!isEmpty($(that).attr('ofc_name')) && $(that).attr('ofc_name').length > 0) ? ($(that).attr('ofc_name') + "\n") : "") + (( !isEmpty($(that).attr('office_address')) && $(that).attr('office_address').length > 0) ? ( $(that).attr('office_address')+ "\n") : ''));
        if(!isEmpty($(that).attr('is_attention'))){
            visibleName +='(দৃষ্টি আকর্ষণঃ '+(( (!isEmpty($(that).attr('officer_name')) && $(that).attr('officer_name').length > 0) ? ($(that).attr('officer_name')) : "") + (( !isEmpty($(that).attr('designation')) && $(that).attr('designation').length > 0) ? ( ', ' + $(that).attr('designation')) : '') + (( !isEmpty($(that).attr('personal_mobile')) && $(that).attr('personal_mobile').length > 0) ? ( ', মোবাইল নং - ' + $(that).attr('personal_mobile')) : ''))+')';
        }
    return visibleName;
}
function setUserInfoInTemplateForInternalOfficeSelection(that, main){
    // RM
    if($('#potro-type').val() == 21){
        var visibleName = (( (!isEmpty($(that).attr('ofc_name'))) ?  $(that).attr('ofc_name')  : "") + ((main == 0)? "  -    সদয় অবগতির জন্য\n":'\n' ) + (( !isEmpty($(that).attr('office_address')) ) ? ( $(that).attr('office_address')+ "\n") : ''));
        if(!isEmpty($(that).attr('is_attention'))){
            visibleName +='(দৃষ্টি আকর্ষণঃ '+((( !isEmpty($(that).attr('officer_name')) ) ? ($(that).attr('officer_name')) : "") + (( !isEmpty($(that).attr('designation')) ) ? ( ', ' + $(that).attr('designation')) : '')+ (( !isEmpty($(that).attr('personal_mobile')) ) ? ( ', মোবাইল নং - ' + $(that).attr('personal_mobile')) : ''))+')';
        }
    }else if($('#potro-type').val() == 27){
        //Formal
        var visibleName = (( (!isEmpty($(that).attr('ofc_name'))) ?  $(that).attr('ofc_name')  : "") + "\n");
    }

   return visibleName;
}
function setUserInfoInTemplateForInternalDirectSelection(that){
    // RM
    if($('#potro-type').val() == 21){
        var visibleName = (((!isEmpty($(that).attr('designation')) && $(that).attr('designation').length > 0) ? ($(that).attr('designation') + ", ") : "") +(( !isEmpty($(that).attr('unit_name')) &&  $(that).attr('unit_name').length > 0) ? ($(that).attr('unit_name') + ", ") : "") +(( !isEmpty($(that).attr('ofc_name')) &&  $(that).attr('ofc_name').length > 0) ? ($(that).attr('ofc_name') + "\n") : "") + (( !isEmpty($(that).attr('office_address')) &&  $(that).attr('office_address').length > 0) ? ( $(that).attr('office_address')+ "\n") : ''));
        if(!isEmpty($(that).attr('is_attention'))){
            visibleName +='(দৃষ্টি আকর্ষণঃ '+(((!isEmpty($(that).attr('officer_name')) && $(that).attr('officer_name').length > 0) ? ($(that).attr('officer_name')) : "") + ((!isEmpty($(that).attr('designation')) && $(that).attr('designation').length > 0) ? ( ', ' + $(that).attr('designation')) : '') + ((!isEmpty($(that).attr('personal_mobile')) && $(that).attr('personal_mobile').length > 0) ? ( ', মোবাইল নং - ' + $(that).attr('personal_mobile')) : ''))+')';
        }
    }else if($('#potro-type').val() == 27){
        //Formal
        var visibleName = (((!isEmpty($(that).attr('designation')) && $(that).attr('designation').length > 0) ? ($(that).attr('designation') + "\n") : ""));
    }

    return visibleName;
}
function setInternalExternalUsers(){
    // RM
    if($('#potro-type').val() == 21){
        if(totalBohisthoReceiver.length > 0 || totalBohisthoOnulipi.length > 0){
            $('#office_organogram_id').append('বহিস্থঃ <br>');
        }
        if(totalBohisthoReceiver.length > 0){
            $('#office_organogram_id').append('কার্যঃ <br><br>');
            $('#office_organogram_id').append(totalBohisthoReceiver);
        }
        if(totalBohisthoOnulipi.length > 0){
            $('#office_organogram_id').append('অবগতিঃ <br><br>');
            $('#office_organogram_id').append(totalBohisthoOnulipi);
        }
        if(totalOntosthoReceiver.length > 0 || totalOntosthoOnulipi.length > 0){
            $('#office_organogram_id').append('অন্তস্থঃ <br>');
        }
        if(totalOntosthoReceiver.length > 0){
            $('#office_organogram_id').append('কার্যঃ <br><br>');
            $('#office_organogram_id').append(totalOntosthoReceiver);
        }
        if(totalOntosthoOnulipi.length > 0){
            $('#office_organogram_id').append('অবগতিঃ <br><br>');
            $('#office_organogram_id').append(totalOntosthoOnulipi);
        }
    }else if($('#potro-type').val() == 27){
        if(totalOntosthoReceiver.length > 0){
            $('#office_organogram_id').append('প্রতিঃ <br><br>');
            $('#office_organogram_id').append(totalOntosthoReceiver);
        }
        if(totalOntosthoOnulipi.length > 0){
            $('#office_organogram_id').append('অবগতিঃ <br><br>');
            $('#office_organogram_id').append(totalOntosthoOnulipi);
        }
    }

     $('#potrojariDraftForm').find('.to_list').editable('destroy');
    $('#potrojariDraftForm').find('.to_list').editable({
         inputclass: 'form-control input-medium',
        name: "to_list",
        type: 'textarea',
        display: function (value, d) {
            var IndexChange = $('.to_list').index(this);
            var nong = $(this).attr('nong');
//            if (IndexChange == 0 && typeof (d) == 'undefined') {
//                return;
//            }
            value =value.replace(/(?:\r\n|\r|\n)/g, '<br/>');
             $(this).html(value);
               if(typeof(nong) != 'undefined'){
                $(".receiver_users").eq(nong).attr('visibleName', value);
            }
           
            // console.log(IndexChange,d,typeof(d));
//            $(this).text(value);
        }
    });
    $('#potrojariDraftForm').find('.cc_list').editable('destroy');
    $('#potrojariDraftForm').find('.cc_list').editable({
         inputclass: 'form-control input-medium',
        name: "cc_list",
        type: 'textarea',
        display: function (value, d) {
            var IndexChange = $('.cc_list').index(this);
            var nong = $(this).attr('nong');
//            if (IndexChange == 0 && typeof (d) == 'undefined') {
//                return;
//            }
            value =value.replace(/(?:\r\n|\r|\n)/g, '<br/>');
            $(this).html(value);
            if(typeof(nong) != 'undefined'){
                $(".onulipi_users").eq(nong).attr('visibleName', value);
            }
            
            // console.log(IndexChange,d,typeof(d));
            
//            $(this).text(value);
        }
    });
}
function reInitializeEditable(){
    $('#sender_name').editable('destroy');
    $('#sender_name').editable({
        type: 'text',
        value: (isEmpty($('#sender_name').text()))?'...':$.trim($('#sender_name').text()),
        display: function (value) {
            if (!value) {
               $(this).html($(".sender_users").eq(0).attr('officer_name'));
            } else {
                $(this).html(value);
                $(".sender_users").eq(0).attr('visibleName',value);
            }
        }
    });
    $('#sender_designation').editable('destroy');
    $('#sender_designation').editable({
        type: 'text',
        value: (isEmpty($('#sender_designation').text()))?'...':$.trim($('#sender_designation').text()),
        display: function (value) {
            if (!value) {
               $(this).html($(".sender_users").eq(0).attr('designation'));
            } else {
                $(this).html(value);
                $(".sender_users").eq(0).attr('visibleDesignation',value);
            }
        }
    });
    $('#sender_name2').editable('destroy');
    $('#sender_name2').editable({
        type: 'text',
        value: (isEmpty($('#sender_name2').text()))?'...':$.trim($('#sender_name2').text()),
        display: function (value) {
            if (!value) {
               $(this).html($(".approval_users").eq(0).attr('officer_name'));
            } else {
                $(this).html(value);
                $(".approval_users").eq(0).attr('visibleName',value);
            }
        }
    });
    $('#sender_designation2').editable('destroy');
    $('#sender_designation2').editable({
        type: 'text',
        value: (isEmpty($('#sender_designation2').text()))?'...':$.trim($('#sender_designation2').text()),
        display: function (value) {
            if (!value) {
               $(this).html($(".approval_users").eq(0).attr('officer_name'));
            } else {
                $(this).html(value);
                $(".approval_users").eq(0).attr('visibleDesignation',value);
            }
        }
    });
}
// >>>>>>>>>>>>>>>>>>>>>>>>> potrojari_related.js >>>>>>>>>>>>>>>>>>>>>> //



