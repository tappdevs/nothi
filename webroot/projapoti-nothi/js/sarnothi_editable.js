var SarNothiFormEditable = function () {

    $.mockjaxSettings.responseTime = 500;

    var log = function (settings, response) {

    }

    var initAjaxMock = function () {
        //ajax mocks

        $.mockjax({
            url: '/post',
            response: function (settings) {
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/error',
            status: 400,
            statusText: 'Bad Request',
            response: function (settings) {
                this.responseText = 'Please input correct value';
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/status',
            status: 500,
            response: function (settings) {
                this.responseText = 'Internal Server Error';
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/groups',
            response: function (settings) {
                this.responseText = [{
                        value: 0,
                        text: 'Guest'
                    }, {
                        value: 1,
                        text: 'Service'
                    }, {
                        value: 2,
                        text: 'Customer'
                    }, {
                        value: 3,
                        text: 'Operator'
                    }, {
                        value: 4,
                        text: 'Support'
                    }, {
                        value: 5,
                        text: 'Admin'
                    }
                ];
                log(settings, this);
            }
        });

    }

    var initEditables = function ($nothino, $subject, $date) {


        //set editable mode based on URL parameter

        //set editable mode based on URL parameter
        $.fn.editable.defaults.mode = 'inline';
        /* if (Metronic.getURLParameter('mode') == 'inline') {
         $('#inline').attr("checked", true);
         } else {
         $('#inline').attr("checked", false);
         jQuery.uniform.update('#inline');
         }*/
        jQuery.uniform.update('#inline');

        //global settings
        $.fn.editable.defaults.inputclass = 'form-control';
        $.fn.editable.defaults.url = '/post';



        //global settings 
        $.fn.editable.defaults.inputclass = 'form-control';
        $.fn.editable.defaults.url = '/post';


        $('#potrojariDraftForm').find('#subject').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'subject',
            title: 'বিষয় লিখুন',
            value: $subject,
            display: function (value) {
                if (!value) {
                    var html = $subject;

                    $(this).html(html);
                    $('#potrojariDraftForm').find('#subject2').html(html);
                } else {
                    $(this).html(value);
                    $('#potrojariDraftForm').find('#subject2').html(value);
                }

            }
        });

        $('#potrojariDraftForm').find('#sharok_no2').editable({
            name: 'sharok_no2',
            value: $nothino,
            display: function (value) {
                $(this).text(value);
            }
        });


        $('#potrojariDraftForm').find('#sharok_no').editable({
            name: "sharok_no",
            value: $nothino,
            validate: function (value) {
                if ($.trim(value) == '')
                    return 'This field is required';
            },
            display: function (value) {
                $(this).text(value);
                $('#potrojariDraftForm').find("#sharok_no2").text(value);
            }
        });

        $('#potrojariDraftForm').find('#sarnothi_for').editable({
            name: "sarnothi_for",
            validate: function (value) {
                if ($.trim(value) == '')
                    return 'This field is required';
            },
            display: function (value) {
                $(this).text(value);
                $('#potrojariDraftForm').find("#sarnothi_for2").text(value);
                $('#potrojariDraftForm').find("#sarnothi_for3").text(value);
            }
        });

        $('#potrojariDraftForm').find('#sarnothi_sarsonkhep').editable({
            inputclass: 'form-control input-medium',
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'sarnothi_sarsonkhep',
            title: 'স্মারক নম্বর লিখুন',
            display: function (value) {
                $(this).text(value);
                $('#potrojariDraftForm').find("#sarnothi_sarsonkhep2").text(value);
            }
        });

        $('#potrojariDraftForm').find('#sarnothi_nothititle').editable({
            inputclass: 'form-control input-medium',
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'sarnothi_nothititle',
            display: function (value) {
                $(this).text(value);
                $('#potrojariDraftForm').find("#sarnothi_nothititle2").text(value);
            }
        });

        $('#potrojariDraftForm').find('#sarnothi_ministry').editable({
            inputclass: 'form-control input-medium',
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'sarnothi_ministry',
            display: function (value) {
                $(this).text(value);
                $('#potrojariDraftForm').find("#sarnothi_ministry2").text(value);
            }
        });

        $('#potrojariDraftForm').find('#office_organogram_id2').editable({
            inputclass: 'form-control input-medium',
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'office_organogram_id2',
            title: 'স্মারক নম্বর লিখুন'
        });

        var editor, html = '';

        function createEditor() {
            if (editor)
                return;

            editor = CKEDITOR.replace('note', {
                toolbarGroups: [
                    {name: 'clipboard', groups: ['undo', 'redo']},
                    {"name": "links", "groups": ["links"]},
                    {name: 'paragraph', groups: ['list', 'indent', 'align']},
                    {name: 'insert'},
                    {"name": "basicstyles", "groups": ["basicstyles"]},
                    {name: 'editing', groups: ['find', 'selection']},
                    {"name": "styles", "groups": ["styles"]}
                ],
                removeButtons: 'Anchor,Subscript,Superscript,Image,Flash,Smiley,IFrame,Iframe,Copy,Cut,Paste'
            });
        }

        function removeEditor() {
            if (!editor)
                return;

            // Retrieve the editor contents. In an Ajax application, this data would be
            // sent to the server or used in any other way.
            document.getElementById('note').innerHTML = html = editor.getData();

            // Destroy the editor.
            editor.destroy();
            editor = null;
        }


        $('#potrojariDraftForm').find('#pencil').click(function (e) {
            e.stopPropagation();
            e.preventDefault();

            if ($('#potrojariDraftForm').find('#note').attr('contenteditable') == "true") {
                $('#potrojariDraftForm').find('#note').removeAttr('contenteditable');
                $(this).html(' <i class="fs1 a2i_gn_edit2"></i> [সম্পাদন  করুন] <br/>');
                var editor = $('#note').froalaEditor();
                if (editor != null) {
                    // editor.destroy();
                    removeSpellChecker();
                    $('#note').froalaEditor('destroy');

                }
                var preData= $('#note').val();
                $('#note').css("display","none");
                $('#noteView').html(preData);
                $('#noteView').css("display","inline");



            } else {
                $('#potrojariDraftForm').find('#note').attr('contenteditable', "true");
                $(this).html(' <i class="fs1 a2i_gn_edit2"></i> [সংরক্ষণ  করুন] <br/>');

                    var preData= $('#noteView').html();
                    $('#noteView').css("display","none");
                    $('#note').html(preData);

                $.FroalaEditor.DefineIcon('summary-potro-attachment-ref', {NAME: 'envelope'});
                $.FroalaEditor.RegisterCommand('summary-potro-attachment-ref', {
                    title: 'প্রাপ্ত পত্র সংলাগ বাছাই করুন',
                    type: 'dropdown',
                    focus: false,
                    undo: false,
                    refreshAfterCallback: true,
                    options: {'--':'--'},
                    callback: function (cmd, val) {

                        if (val != '--') {

                            val = val.toString();
                            var songlap_title = $('[id^=dropdown-menu-summary-potro-attachment-ref] ul li a[data-param1="'+val+'"]').attr('title');
                            var office_id = $('#fileuploadpotrojari input[name="office_id"]').val();

                            $('#note').froalaEditor('html.insert', '<input type="button"  style="font-style: italic; text-decoration: underline;padding:1px; color: blue;cursor: pointer; background: transparent;"  class="btn btn-xs link songlapbutton" data-nothi-office="'+office_id+'" id="' + val + '" value="' + songlap_title + '" />,&nbsp;');

                        }
                    }
                });

                $.FroalaEditor.DefineIcon('summary-attachment-ref', {NAME: 'paperclip'});
                $.FroalaEditor.RegisterCommand('summary-attachment-ref', {
                    title: 'সংযুক্ত সংলাগ বাছাই করুন',
                    type: 'dropdown',
                    focus: false,
                    undo: false,
                    refreshAfterCallback: true,
                    options: {
                        '--': '--'
                    },
                    callback: function (cmd, val) {

                        if (val.length > 0 && val != '--') {

                            var value_select = $('[id^=dropdown-menu-summary-attachment-ref] ul li a[data-param1="'+val+'"]').text();
                            var file_type = $('[id^=dropdown-menu-summary-attachment-ref] ul li a[data-param1="'+val+'"]').attr('ft');
                            var office_id = $('#fileuploadpotrojari input[name="office_id"]').val();

                            $('#note').froalaEditor('html.insert', " <a  style='font-style: italic; text-decoration: underline;padding:1px; color: blue;cursor: pointer; background: transparent;' class='link showAttachmentOfContent' data-nothi-office=''"+office_id+"' data-url="+val+" data-type="+file_type+" value="+value_select+">"+value_select+"</a>, &nbsp;");

                        }
                    }
                });

                $.FroalaEditor.DefineIcon('spell-checker', {NAME: 'check-circle'});
                $.FroalaEditor.RegisterCommand('spell-checker', {
                    title: 'বানান পরীক্ষক',
                    focus: true,
                    undo: true,
                    showOnMobile: true,
                    refreshAfterCallback: true,
                    callback: function () {
                        getWordSeperated();
                    }
                });

                $.FroalaEditor.DefineIcon('spell-checker-off', {NAME: 'minus-circle'});
                $.FroalaEditor.RegisterCommand('spell-checker-off', {
                    title: 'বানান পরীক্ষক বন্ধ করুন',
                    focus: true,
                    undo: true,
                    showOnMobile: true,
                    refreshAfterCallback: true,
                    callback: function () {
                        removeSpellChecker();
                    }
                });

                var buttons = ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertLink', 'insertTable', '|', 'insertHR', 'clearFormatting', '|', 'undo', 'redo','|','summary-potro-attachment-ref','summary-attachment-ref','spell-checker','spell-checker-off'];

                var editor = $('#note').froalaEditor({
                    key: 'xc1We1KYi1Ta1WId1CVd1F==',
                    toolbarSticky: true,
                    wordPasteModal: true,
					wordAllowedStyleProps: ['font-size', 'background', 'color', 'text-align', 'vertical-align', 'background-color', 'padding', 'margin', 'height', 'margin-top', 'margin-left', 'margin-right', 'margin-bottom', 'text-decoration', 'font-weight'],
					wordDeniedTags: ['a','form'],
					wordDeniedAttrs: ['width'],
                    tableResizerOffset: 10,
                    tableResizingLimit: 20,
                    toolbarButtons: buttons,
                    toolbarButtonsMD: buttons,
                    toolbarButtonsSM: buttons,
                    toolbarButtonsXS: buttons,
                    placeholderText: '',
                    height: 300,
                    fontSize: ['10','11', '12','13', '14','15','16','17', '18','19','20','21','22','23','24','25','26','27','28','29', '30'],
                    fontSizeDefaultSelection: '13',
                    tableEditButtons:['tableHeader', 'tableRemove', '|', 'tableRows', 'tableColumns','tableCells', '-',  'tableCellBackground', 'tableCellVerticalAlign', 'tableCellHorizontalAlign', 'tableCellStyle'],
                    fontFamily: {
                        "Nikosh,SolaimanLipi,'Open Sans', sans-serif": "Nikosh",
                        "Arial": "Arial",
                        "Kalpurush": "Kalpurush",
                        "SolaimanLipi": "SolaimanLipi",
                        "'times new roman'": "Times New Roman",
                        "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
                    }
                });
                check_title();
                check_potro_title();
                $.contextMenu( 'destroy' );
                iniDictionarySarNothi();
            }
        });

        $('#potrojariDraftForm').find('#sending_date').editable({
            rtl: Metronic.isRTL(),
            display: function (value) {

                var bDate = new Array("০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯");
//                var bMonth = new Array("০১", "০২", "০৩", "০৪", "০৫", "০৬", "০৭", "০৮", "০৯", "১০", "১১", "১২");
                var bMonth = new Array("জানুয়ারি", "ফেব্রুয়ারি", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর");
                var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

                if (value != '' && value != null) {
                    var dtb, dtb1, dtb2;
                    var dt = value.getDate();
                    if (dt >= 10) {
                        dtb1 = Math.floor(dt / 10);
                        dtb2 = dt % 10;
                        dtb = bDate[dtb1] + "" + bDate[dtb2];
                    } else {
                        dtb = bDate[0] + "" + bDate[dt];
                    }

                    var mnb;
                    var mn = value.getMonth();
                    mnb = bMonth[mn];

                    var yrb = "", yr1;
                    var yr = value.getFullYear();

                    for (var i = 0; i < 3; i++) {
                        yr1 = yr % 10;
                        yrb = bDate[yr1] + yrb;
                        yr = Math.floor(yr / 10);
                    }

                    yrb = bDate[yr] + "" + yrb;

                    $(this).text(dtb + " " + mnb + " " + yrb );

                    var that = $(this);
                    $('#potrojariDraftForm').find('.bangladate').next('br').remove();
                    $('#potrojariDraftForm').find('.bangladate').remove();
                    $.ajax({
                        url: js_wb_root + "banglaDate",
                        data: {date: value.getFullYear() + "-" + (mn + 1) + "-" + value.getDate()},
                        type: 'POST',
                        cache: false,
                        dataType: 'JSON',
                        success: function (bangladate) {
                            that.before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");

                            $('#potrojariDraftForm').find("#sending_date_2").attr('style', 'padding-top: 1px;');
                            $('#potrojariDraftForm').find("#sending_date_2").before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");
                        }
                    });

                    if ($('#potrojariDraftForm').find("#sending_date_2")) {
                        $('#potrojariDraftForm').find("#sending_date_2").text($(this).text()).attr('readDate', value);
                    }
                } else {
                    var dtb, dtb1, dtb2;

                    value = new Date();
                    var dt = value.getDate();
                    if (dt >= 10) {
                        dtb1 = Math.floor(dt / 10);
                        dtb2 = dt % 10;
                        dtb = bDate[dtb1] + "" + bDate[dtb2];
                    } else {
                        dtb = bDate[0] + "" + bDate[dt];
                    }

                    var mnb;
                    var mn = value.getMonth();
                    mnb = bMonth[mn];

                    var yrb = "", yr1;
                    var yr = value.getFullYear();

                    for (var i = 0; i < 3; i++) {
                        yr1 = yr % 10;
                        yrb = bDate[yr1] + yrb;
                        yr = Math.floor(yr / 10);
                    }

                    yrb = bDate[yr] + "" + yrb;

                    $(this).text(dtb + " " + mnb + " " + yrb );

                    var that = $(this);
                    $('#potrojariDraftForm').find('.bangladate').next('br').remove();
                    $('#potrojariDraftForm').find('.bangladate').remove();
                    $.ajax({
                        url: js_wb_root + "banglaDate",
                        data: {date: value.getFullYear() + "-" + (mn + 1) + "-" + value.getDate()},
                        type: 'POST',
                        cache: false,
                        dataType: 'JSON',
                        success: function (bangladate) {
                            that.before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");

                            $('#potrojariDraftForm').find("#sending_date_2").attr('style', 'padding-top: 1px;');
                            $('#potrojariDraftForm').find("#sending_date_2").before("<span class=' bangladate' style='border-bottom:1px solid #000!important; margin-left: 3px; position: relative; top: -5px;'>" + bangladate.date + " " + bangladate.month + " " + bangladate.year + "</span><br/>");
                        }
                    });

                    if ($('#potrojariDraftForm').find("#sending_date_2")) {
                        $('#potrojariDraftForm').find("#sending_date_2").text($(this).text()).attr('readDate', value);
                    }
                }
            }
        });
    }

    return {
        //main function to initiate the module
        init: function ($nothino, $subject, $date) {

            // inii ajax simulation
            initAjaxMock();

            // init editable elements
            initEditables($nothino, $subject, $date);

        },
        encodeImage: function (imgurl, enc_imgurl, callback){
           
           $.ajax({
               url : js_wb_root + 'getSignature/'+ imgurl +'/1?token='+enc_imgurl,
               cache: true,
               type: 'post',
               async: false,
               success: function(img){
                   callback(img);
               }
           })
       }

    };

}();
function  iniDictionarySarNothi() {
    $('.fr-wrapper').contextmenu(function (e) {
        var data = $('#note').froalaEditor('selection.text');
        if (data != "" && data.length > 1) {
            e.preventDefault();
            if ((data[data.length - 1]) == ' ') {
                var isSpace = ' ';
            } else {
                var isSpace = '';
            }
            $("#word").val(data);
            $("#space").val(isSpace);
            customContextMenuCall(data);
        } else {
            var data = getWordFromEvent(e);
            if(data !="" && data.length >1){
                e.preventDefault();
                if ((data[data.length - 1])==' '){
                    var isSpace =' ';
                } else {
                    var isSpace ='';
                }
                $("#word").val(data);
                $("#space").val(isSpace);
                customContextMenuCall(data);
            } else {
                e.stopPropagation();
            }
        }
    });

    function customContextMenuCall(word) {
        'use strict';
        var errorItems = {"errorItem": {name: "Word Load error"},};
        var loadItems = function () {

            var dfd = jQuery.Deferred();
            setTimeout(function () {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: 'https://ovidhan.tappware.com/search.php',
                    data: {'word': $("#word").val()},
                    success: function (data) {
                        if (data.length > 10) {
                            var subSubItems = {};
                        }
                        var i = 1;
                        var subItems = {};
                        $(data).each(function (j, v) {
                            if (i < 11) {
                                var name = {};
                                name['name'] = v;
                                subItems[v] = name;
                            } else {
                                var name = {};
                                name['name'] = v;
                                subSubItems[v] = name;
                            }
                            i++;
                        });

                        if (data.length > 10) {
                            var seeMoreMenu = {};
                            seeMoreMenu = {
                                name: "<strong>আরও দেখুন...</strong>",
                                isHtmlName: true,
                                items: subSubItems
                            };
                            subItems['আরও দেখুন...'] = seeMoreMenu;
                        }

                        dfd.resolve(subItems);
                    }
                });
            }, 100);
            return dfd.promise();
        };

        $.contextMenu({
            selector: '.fr-wrapper',
            build: function ($trigger, e) {
                return {
                    callback: function (key, options) {
                        if (key != "কোন তথ্য পাওয়া যায় নি।") {
                            $('#note').froalaEditor('html.insert', (key + ($("#space").val())));
                        }
                    },
                    items: {
                        "status": {
                            name: "সাজেশন্স",
                            icon: "fa-check",
                            items: loadItems(),
                        },
                    }
                };
            }
        });


        var completedPromise = function (status) {
            console.log("completed promise:", status);
        };

        var failPromise = function (status) {
            console.log("fail promise:", status);
        };

        var notifyPromise = function (status) {
            console.log("notify promise:", status);
        };

        $.loadItemsAsync = function () {
            console.log("loadItemsAsync");
            var promise = loadItems();
            $.when(promise).then(completedPromise, failPromise, notifyPromise);
        };
    }
}

$(document).off('click', '.showAttachmentOfContent').on('click', '.showAttachmentOfContent', function () {
    $('#responsiveOnuccedModal').modal('show');
    var file = $(this).text();
    $('#responsiveOnuccedModal').find('.modal-title').html(file);

    var url = $(this).data('url');
    var attachment_type = $(this).data('type');

    if(typeof(url)!='undefined'){

                if (attachment_type == 'text') {
                    $('#responsiveOnuccedModal').find('.modal-body').html('<div style="background-color: #fff; max-width:950px; min-height:815px; max-height: 815px; margin:0 auto; page-break-inside: auto;">"'+file+'<br/>" + file + "</div>"');
                } else if ((attachment_type.substring(0, 15)) == 'application/vnd' || (attachment_type.substring(0, 15)) == 'application/ms'  || (attachment_type.substring(0, 15)) == 'application/pdf') {
                    $('#responsiveOnuccedModal').find('.modal-body').html('<iframe style=" width:100%; height: 700px;" src="https://docs.google.com/gview?url='  + url + '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>');

                } else if ((attachment_type.substring(0, 5)) == 'image') {
                    $('#responsiveOnuccedModal').find('.modal-body').html('<embed src="'+ url + '" style=" width:100%; height: 700px;" type="' + attachment_type + '"></embed>');
                } else {
                    $('#responsiveOnuccedModal').find('.modal-body').html('দুঃখিত! ডাটা পাওয়া যায়নি');
                }
    }

});
function check_potro_title() {
    $('[id^=dropdown-menu-summary-potro-attachment-ref] ul').html('');
    $('[id^=dropdown-menu-summary-potro-attachment-ref] ul').append($('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="summary-potro-attachment-ref" data-param1="--" title="" aria-selected="false">--</a></li>'));

    $('#summary_potro_attachment_input_form .summary-potro-attachment-input').each(function () {
        var text = $(this).val();
        var potro_id = $(this).attr('potro_id');

        $('[id^=dropdown-menu-summary-potro-attachment-ref] ul').append('<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="summary-potro-attachment-ref" data-param1="'+ potro_id + '" title="'+text+'" aria-selected="false">'+text+'</a></li>');

    });
}