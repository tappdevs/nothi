var TableAjax = function () {
    var handleRecords = function () {

        var grid = new Datatable();

        grid.init({
            "destroy": true,
            src: $('#filelist'),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function (grid) {
                // execute some code on ajax data load

            },
            loadingMessage: 'লোড করা হচ্ছে...',
            dataTable: {
                "destroy": true,
                "scrollY": "500",
                "dom": "<'row'<'col-md-8 col-sm-12'li><'col-md-4 col-sm-12'<'table-group-actions pull-right'p>>r>t",
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 500],
                    ["১০", "২০", "৫০", "১০০", "৫০০"] // change per page values here
                ],
                "pageLength": (typeof ($.cookie("page_limit_guardfile")) != 'undefined'?$.cookie("page_limit_guardfile"):50), // default record count per page
                "ajax": {
                    "url": js_wb_root + 'GuardFiles/officeGuardFile?attachment=0&guardFilePage=1', // ajax source
                },
                "bSort": false,
                "ordering": false,
                "language": {// language settings
                    // metronic spesific
                    "metronicGroupActions": "_TOTAL_ রেকর্ড নির্বাচন করা হয়েছে:  ",
                    "metronicAjaxRequestGeneralError": "অনুরোধ সম্মন্ন করা সম্ভব হচ্ছে না।",
                    // data tables spesific
                    "lengthMenu": "<span class='seperator'></span>দেখুন _MENU_ ধরন",
                    "info": "<span class='seperator'>|</span>মোট _TOTAL_ টি পাওয়া গেছে",
                    "infoEmpty": "",
                    "emptyTable": "কোনো রেকর্ড  নেই",
                    "zeroRecords": "কোনো রেকর্ড  নেই",
                    "paginate": {
                        "previous": "প্রথমটা",
                        "next": "পরবর্তীটা",
                        "last": "শেষেরটা",
                        "first": "প্রথমটা",
                        "page": "পাতা",
                        "pageOf": "এর"
                    }
                }
            }
        });

        $(document).off('click', '.showDetailAttach').on('click', '.showDetailAttach', function (ev) {
            ev.preventDefault();
            getPopUpPotro($(this).attr('href'), $(this).attr('title'));
        });

        $('#guard-file-category-id').off('change').on('change', function () {
            grid.setAjaxParam('guard_file_category_id', $(this).val());
            grid.getDataTable().ajax.url = js_wb_root + 'GuardFiles/officeGuardFile?attachment=0&guardFilePage=1';
            grid.getDataTable().ajax.reload();
        });

        $('[name=filelist_length]').on('change', function () {
            $.cookie('page_limit_guardfile',$(this).val());
        });
    };


    return {

        //main function to initiate the module
        init: function (url) {

            handleRecords(url);
        }

    };

}();