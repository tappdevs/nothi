var TableAjax = function () {
    var handleRecords = function (url, name) {
        var grid = new Datatable();
        var url = js_wb_root + url;

        grid.init({
            src: $("#"+name+"-tab #datatable-nothi-masters"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
                $('[data-title]').tooltip({placement: 'bottom'});
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function (grid) {
                // execute some code on ajax data load

                $('[data-title]').tooltip({placement: 'bottom'});
            },
            loadingMessage: 'লোড করা হচ্ছে...',
            dataTable: {
                "dom": "<'row'<'col-md-8 col-sm-12'li><'col-md-4 col-sm-12'<'table-group-actions pull-right'p>>r>t",
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 500],
                    ["১০", "২০", "৫০", "১০০", "৫০০"] // change per page values here
                ],
                "pageLength": (typeof ($.cookie("page_limit_nothi")) != 'undefined'?$.cookie("page_limit_nothi"):10), // default record count per page
                "ajax": {
                    "url": url, // ajax source
                },
//                "bSort":false,
                "order": [
                    [1, "desc"]
                ]// set first column as a default sort by asc
                ,
                "language": {// language settings
                    // metronic spesific
                    "metronicGroupActions": "_TOTAL_ রেকর্ড নির্বাচন করা হয়েছে:  ",
                    "metronicAjaxRequestGeneralError": "অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না।",
                    // data tables spesific
                    "lengthMenu": "<span class='seperator'></span> _MENU_ ",
                    "info": "<span class='seperator'> </span> মোট _TOTAL_ টি নথি আছে",
                    "infoEmpty": "",
                    "emptyTable": "কোনো নথি  নেই",
                    "zeroRecords": "কোনো নথি  নেই",
                    "paginate": {
                        "previous": "প্রথমটা",
                        "next": "পরবর্তীটা",
                        "last": "শেষেরটা",
                        "first": "প্রথমটা",
                        "page": "পাতা",
                        "pageOf": "এর"
                    }
                }
            }
        });
        $('.filter-cancel').click(function () {
            $('#filter_input').val('');
            $('#filter_input_4').val('');
            $('#search_date_from').val('');
            $('#search_date_to').val('');
            $('#select-office-unit-id').val('');
            $('#show-only-important').val('');
            grid.resetFilter();
        });

        $('#filter_submit_btn1').off('click').on('click', function () {
            grid.setAjaxParam('subject', $('#filter_input').val());
            grid.setAjaxParam('unit', $('#select-office-unit-id').val());
            grid.setAjaxParam('show_only_important', $('#show-only-important').val());
            $('.filter input').each(function () {
                var $item = $(this);
                grid.setAjaxParam($item.attr('name'), $item.val());
            });
            grid.getDataTable().ajax.url = url;
            grid.getDataTable().ajax.reload();
        });

        $('[name=datatable-nothi-masters_length]').on('change', function () {
            $.cookie('page_limit_nothi',$(this).val());
        });

    };

    var handleFolderRecords = function (url, condition) {

        if (typeof condition == 'undefined') {
            condition = '';
        }

        $('.folder-content').html('<img src="' + js_wb_root + 'assets/global/img/loading-spinner-grey.gif" alt="" class="loading"> <span>&nbsp;&nbsp;লোড হচ্ছে। একটু অপেক্ষা করুন... </span>');

        $.ajax({
            type: "POST",
            cache: true,
            url: js_wb_root + url,
            dataType: 'JSON',
            data: condition,
            success: function (res) {
                $('.folder-content').html(res.data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    }

    return {
        //main function to initiate the module
        init: function (url, name) {
            handleRecords(url, name);
        },
        folderInit: function (url, condition) {

            handleFolderRecords(url, condition);
        }

    };

}();