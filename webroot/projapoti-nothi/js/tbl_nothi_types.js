var TableAjax = function () {
    var handleRecords = function (url, per_page) {

        var grid = new Datatable();
        var url = js_wb_root + url;
        grid.init({
            src: $("#datatable-nothitype"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function (grid) {
                // execute some code on ajax data load


            },
            loadingMessage: 'লোড করা হচ্ছে...',
            dataTable: {
                "dom": "<'row'<'col-md-8 col-sm-12'li><'col-md-4 col-sm-12'<'table-group-actions pull-right'p>>r>t",

                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 500],
                    ["১০", "২০", "৫০", "১০০", "৫০০"] // change per page values here
                ],
                "pageLength": per_page, // default record count per page
                "ajax": {
                    "url": url, // ajax source
                },
//                "bSort":false,
                "order": [
                    [1, "desc"]
                ]// set first column as a default sort by asc
                ,
                "language": { // language settings
                    // metronic spesific
                    "metronicGroupActions": "_TOTAL_ রেকর্ড নির্বাচন করা হয়েছে:  ",
                    "metronicAjaxRequestGeneralError": "অনুরোধ সম্পন্ন করা সম্ভব হচ্ছে না।",

                    // data tables spesific
                    "lengthMenu": "<span class='seperator'></span>দেখুন _MENU_ ধরন",
                    "info": "<span class='seperator'>|</span>মোট _TOTAL_ টি পাওয়া গেছে",
                    "infoEmpty": "",
                    "emptyTable": "কোনো রেকর্ড  নেই",
                    "zeroRecords": "কোনো রেকর্ড  নেই",
                    "paginate": {
                        "previous": "প্রথমটা",
                        "next": "পরবর্তীটা",
                        "last": "শেষেরটা",
                        "first": "প্রথমটা",
                        "page": "পাতা",
                        "pageOf": "এর"
                    }
                }
            }
        });

        var pageLengthFromCookie = getCookie('NothiTypeList_PageLimit');
        if (pageLengthFromCookie != $("select[name=datatable-nothitype_length]").val()) {
            $("select[name=datatable-nothitype_length]").val(pageLengthFromCookie).trigger('change');
        }
        $('#filter_submit_btn').click(function () {
            grid.getDataTable().ajax.reload(); //reload the datatable;
        });

    };


    return {

        //main function to initiate the module
        init: function (url, per_page) {

            handleRecords(url, per_page);
        }

    };

}();