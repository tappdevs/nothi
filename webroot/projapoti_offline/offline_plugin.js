/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Iftekhar Ahmed Eather
 * Offline Plugin to maintain data save in offline and fetch
 * read for details http://pouchdb.com/guides/
 */


var ProjapotiOffline = function () {

    return {
        init: function (debug) {
            if(typeof(debug) != 'undefined'){
                if(debug == true){
                    PouchDB.debug.enable('*');
                }else{
                    PouchDB.debug.disable();
                }
            }else{
                PouchDB.debug.disable();
            }
        },
        createDatabase: function (dbName) {
            return new PouchDB(dbName);
        },
        deleteDatabase: function (dbName) {
            return dbName.destroy();
        },
        saveData: function (database, doc) {
            database.put(doc).then(function (response) {
                // handle response
            }).catch(function (err) {
                
            });
        },
        postData: function (database, doc) {
            database.post(doc).then(function (response) {
                // handle response
               // console.log(response);
            }).catch(function (err) {
                
            });
        },
        updateData: function (database, newDoc, id) {
            database.get(id).then(function (doc) {
                database.put(newDoc, id, doc._rev);
            }).then(function (response) {
            }).catch(function (err) {
                database.put(newDoc);
            });
        },
        fetchData: function (database, id) {
            database.get(id).then(function (doc) {
                // handle doc
                return doc;
            }).catch(function (err) {
                
            });
        },
        deleteData: function (database, id) {
            database.get(id).then(function (doc) {
                return database.remove(doc);
            }).then(function (result) {
                // handle result
            }).catch(function (err) {
                
            });
        },
        manageBulkData: function (database, bulkDoc) {
            database.bulkDocs(bulkDoc).then(function (result) {
                // handle result
                //console.log(result);
            }).catch(function (err) {
                
            });
        },
        fetchBulkData: function (database, options) {
            database.allDocs({
                include_docs: true,
                attachments: true
              }).then(function (result) {
                return result;
              }).catch(function (err) {
                
              });
        },
        //http://pouchdb.com/2014/04/14/pagination-strategies-with-pouchdb.html
        fetchNextPage: function (database, options) {
            database.allDocs(options, function (err, response) {
                if (response && response.rows.length > 0) {
                    options.startkey = response.rows[response.rows.length - 1];
                    options.skip = 1;
                }
                // handle err or response
            })
        },
        //https://github.com/nolanlawson/pouchdb-find
        createIndex: function (database, fields) {
            database.createIndex({
                index: {
                    fields: [fields]
                }
            });
        },
        deleteIndex: function (database, index) {
            database.deleteIndex(index);
        },
        findByIndex: function (database, options) {
            return database.find(options);
        }
    }
}();